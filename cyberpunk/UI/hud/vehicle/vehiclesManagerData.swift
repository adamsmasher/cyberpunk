
public class VehiclesManagerDataHelper extends IScriptable {

  public final static func GetVehicles(player: ref<GameObject>) -> [ref<IScriptable>] {
    let currentData: ref<VehicleListItemData>;
    let i: Int32;
    let repairElapsedTime: Float;
    let result: array<ref<IScriptable>>;
    let vehicle: PlayerVehicle;
    let vehicleRecord: ref<Vehicle_Record>;
    let vehiclesList: array<PlayerVehicle>;
    let repairTime: Float = TweakDBInterface.GetFloat(t"Vehicle.summon_setup.repairCooldownMax", 0.00);
    let currentSimTime: EngineTime = GameInstance.GetSimTime(player.GetGame());
    GameInstance.GetVehicleSystem(player.GetGame()).GetPlayerUnlockedVehicles(vehiclesList);
    i = 0;
    while i < ArraySize(vehiclesList) {
      vehicle = vehiclesList[i];
      if TDBID.IsValid(vehicle.recordID) {
        vehicleRecord = TweakDBInterface.GetVehicleRecord(vehicle.recordID);
        currentData = new VehicleListItemData();
        currentData.m_displayName = vehicleRecord.DisplayName();
        currentData.m_icon = vehicleRecord.Icon();
        currentData.m_data = vehicle;
        repairElapsedTime = EngineTime.ToFloat(currentSimTime - vehicle.destructionTimeStamp);
        currentData.m_repairTimeRemaining = repairElapsedTime > repairTime ? 0.00 : repairTime - repairElapsedTime;
        ArrayPush(result, currentData);
      };
      i += 1;
    };
    return result;
  }

  public final static func GetRadioStations(player: ref<GameObject>) -> [ref<IScriptable>] {
    let res: array<ref<IScriptable>>;
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.NoStation"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.Downtempo"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.AggroIndie"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.GrowlFM"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.Jazz"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.ElectroIndie"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.MinimTech"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.Metal"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.Pop"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.Impulse"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.HipHop"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.AggroTechno"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.Latino"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.AttRock"));
    VehiclesManagerDataHelper.PushRadioStationData(res, TweakDBInterface.GetRadioStationRecord(t"RadioStation.DarkStar"));
    return res;
  }

  private final static func PushRadioStationData(result: script_ref<[ref<IScriptable>]>, record: ref<RadioStation_Record>) -> Void {
    let stationDataObj: ref<RadioListItemData> = new RadioListItemData();
    stationDataObj.m_record = record;
    ArrayPush(Deref(result), stationDataObj);
  }
}
