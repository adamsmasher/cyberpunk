
public class InteractiveSignController extends ScriptableDeviceComponent {

  public const func GetPS() -> ref<InteractiveSignControllerPS> {
    return this.GetBasePS() as InteractiveSignControllerPS;
  }
}
