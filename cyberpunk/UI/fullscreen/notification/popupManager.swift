
public native class PopupsManager extends inkGameController {

  private let m_blackboard: wref<IBlackboard>;

  private let m_bbDefinition: ref<UIGameDataDef>;

  private let m_journalManager: wref<JournalManager>;

  private let m_uiSystem: ref<UISystem>;

  private let m_uiSystemBB: wref<IBlackboard>;

  private let m_uiSystemBBDef: ref<UI_SystemDef>;

  private let m_uiSystemId: ref<CallbackHandle>;

  private let m_isShownBbId: ref<CallbackHandle>;

  private let m_dataBbId: ref<CallbackHandle>;

  private let m_photomodeActiveId: ref<CallbackHandle>;

  private let m_phoneActiveId: ref<CallbackHandle>;

  private let m_tutorialOnHold: Bool;

  private let m_tutorialData: PopupData;

  private let m_tutorialSettings: PopupSettings;

  private let m_phoneMessageOnHold: Bool;

  private let m_phoneMessageData: ref<JournalNotificationData>;

  private let m_shardReadOnHold: Bool;

  private let m_shardReadData: ref<NotifyShardRead>;

  private let m_smartFrameData: ref<inkFrameNotificationData>;

  private let m_vehicleColorSelectorData: ref<inkGameNotificationData>;

  private let m_tutorialToken: ref<inkGameNotificationToken>;

  private let m_phoneMessageToken: ref<inkGameNotificationToken>;

  private let m_shardToken: ref<inkGameNotificationToken>;

  private let m_vehiclesManagerToken: ref<inkGameNotificationToken>;

  private let m_vehicleRadioToken: ref<inkGameNotificationToken>;

  private let m_codexToken: ref<inkGameNotificationToken>;

  private let m_ponrToken: ref<inkGameNotificationToken>;

  private let m_twintoneOverride: ref<inkGameNotificationToken>;

  private let m_expansionToken: ref<inkGameNotificationToken>;

  private let m_expansionErrorToken: ref<inkGameNotificationToken>;

  private let m_patchNotesToken: ref<inkGameNotificationToken>;

  private let m_marketingConsentToken: ref<inkGameNotificationToken>;

  private let m_signInToken: ref<inkGameNotificationToken>;

  private let m_expansionStateToken: ref<inkGameNotificationToken>;

  private let m_vehicleVisualCustomizationSelectorToken: ref<inkGameNotificationToken>;

  private let m_frameSwitcherToken: ref<inkGameNotificationToken>;

  private let m_isInMenu: Bool;

  private let m_isInPhotoMode: Bool;

  private let m_isOnPhone: Bool;

  private let m_isBlockingPopupOpened: Bool;

  protected cb func OnInitialize() -> Bool {
    let requestHandler: wref<inkISystemRequestsHandler> = this.GetSystemRequestsHandler();
    requestHandler.RegisterToCallback(n"OnAdditionalContentInstallationRequestResult", this, n"OnAdditionalContentInstallationRequestResult");
    requestHandler.RegisterToCallback(n"OnAdditionalContentInstallationResult", this, n"OnAdditionalContentInstallationResult");
    requestHandler.RegisterToCallback(n"OnAdditionalContentDataReloadProgressCallback", this, n"OnAdditionalContentDataReloadProgress");
  }

  protected cb func OnUninitialize() -> Bool {
    let requestHandler: wref<inkISystemRequestsHandler> = this.GetSystemRequestsHandler();
    requestHandler.UnregisterFromCallback(n"OnAdditionalContentInstallationRequestResult", this, n"OnAdditionalContentInstallationRequestResult");
    requestHandler.UnregisterFromCallback(n"OnAdditionalContentInstallationResult", this, n"OnAdditionalContentInstallationResult");
  }

  protected cb func OnPlayerAttach(playerPuppet: ref<GameObject>) -> Bool {
    this.m_blackboard = this.GetUIBlackboard();
    this.m_bbDefinition = GetAllBlackboardDefs().UIGameData;
    this.m_journalManager = GameInstance.GetJournalManager(playerPuppet.GetGame());
    this.m_uiSystem = GameInstance.GetUISystem(playerPuppet.GetGame());
    this.m_uiSystemBB = this.GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_System);
    this.m_uiSystemBBDef = GetAllBlackboardDefs().UI_System;
    this.m_uiSystemId = this.m_uiSystemBB.RegisterListenerBool(this.m_uiSystemBBDef.IsInMenu, this, n"OnMenuUpdate");
    this.m_isShownBbId = this.m_blackboard.RegisterDelayedListenerBool(this.m_bbDefinition.Popup_IsShown, this, n"OnUpdateVisibility");
    this.m_dataBbId = this.m_blackboard.RegisterDelayedListenerVariant(this.m_bbDefinition.Popup_Data, this, n"OnUpdateData");
    this.m_photomodeActiveId = this.GetBlackboardSystem().Get(GetAllBlackboardDefs().PhotoMode).RegisterListenerBool(GetAllBlackboardDefs().PhotoMode.IsActive, this, n"OnPhotomodeUpdate");
    this.m_phoneActiveId = this.GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_ComDevice).RegisterListenerBool(GetAllBlackboardDefs().UI_ComDevice.ContactsActive, this, n"OnPhoneUpdate");
  }

  protected cb func OnPlayerDetach(playerPuppet: ref<GameObject>) -> Bool {
    this.m_uiSystemBB.UnregisterListenerBool(this.m_uiSystemBBDef.IsInMenu, this.m_uiSystemId);
    this.m_blackboard.UnregisterDelayedListener(this.m_bbDefinition.Popup_IsShown, this.m_isShownBbId);
    this.m_blackboard.UnregisterDelayedListener(this.m_bbDefinition.Popup_Data, this.m_dataBbId);
    this.GetBlackboardSystem().Get(GetAllBlackboardDefs().PhotoMode).UnregisterListenerBool(GetAllBlackboardDefs().PhotoMode.IsActive, this.m_photomodeActiveId);
    this.GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_ComDevice).UnregisterListenerBool(GetAllBlackboardDefs().UI_ComDevice.ContactsActive, this.m_phoneActiveId);
  }

  protected cb func OnMenuUpdate(isInMenu: Bool) -> Bool {
    this.m_isInMenu = isInMenu;
    this.SetPhoneMessageVisibility(isInMenu);
    this.SetTutorialTokenVisibility(isInMenu);
    this.SetShardReadVisibility(isInMenu);
    this.ChangeTutorialVisibilityInMenu(isInMenu);
    if this.m_isInMenu {
      this.HideExclusivePopUps();
    };
  }

  protected cb func OnPhotomodeUpdate(isInPhotomode: Bool) -> Bool {
    this.m_isInPhotoMode = isInPhotomode;
    this.SetPhoneMessageVisibility(isInPhotomode);
    this.SetTutorialTokenVisibility(isInPhotomode);
    this.ChangeTutorialVisibility(isInPhotomode, gameuiTutorialHiddenReason.InPhotomode);
    if this.m_isInPhotoMode {
      this.HideExclusivePopUps();
    };
  }

  protected cb func OnPhoneUpdate(isOnPhone: Bool) -> Bool {
    this.m_isOnPhone = isOnPhone;
    if this.m_isOnPhone {
      this.HideExclusivePopUps();
    };
  }

  protected cb func OnUpdateVisibility(value: Bool) -> Bool {
    if !value && IsDefined(this.m_tutorialToken) {
      this.m_tutorialToken.TriggerCallback(null);
    };
  }

  private final func SetPhoneMessageVisibility(hideToken: Bool) -> Void {
    if hideToken && this.m_phoneMessageToken != null {
      this.m_phoneMessageOnHold = true;
      this.m_phoneMessageToken = null;
      return;
    };
    if !hideToken && this.m_phoneMessageToken == null && this.m_phoneMessageOnHold {
      this.m_phoneMessageOnHold = false;
      this.ShowPhoneMessage();
    };
  }

  private final func SetTutorialTokenVisibility(hideToken: Bool) -> Void {
    if hideToken {
      if this.m_tutorialToken != null && this.m_tutorialSettings.hideInMenu {
        this.m_tutorialOnHold = true;
        this.m_tutorialToken = null;
      };
    } else {
      if this.m_tutorialToken == null && this.m_tutorialOnHold {
        this.m_tutorialOnHold = false;
        this.ShowTutorial();
      };
    };
  }

  private final func HideExclusivePopUps() -> Void {
    if IsDefined(this.m_vehicleRadioToken) {
      this.m_vehicleRadioToken.TriggerCallback(null);
    };
    if IsDefined(this.m_vehiclesManagerToken) {
      this.m_vehiclesManagerToken.TriggerCallback(null);
    };
    if IsDefined(this.m_vehicleVisualCustomizationSelectorToken) {
      this.m_vehicleVisualCustomizationSelectorToken.TriggerCallback(this.m_vehicleColorSelectorData);
    };
    if IsDefined(this.m_twintoneOverride) {
      this.m_twintoneOverride.TriggerCallback(null);
    };
    if IsDefined(this.m_frameSwitcherToken) {
      this.m_smartFrameData.shouldApply = false;
      this.m_frameSwitcherToken.TriggerCallback(this.m_smartFrameData);
    };
    this.SendPSMRadialCloseRequest();
    this.m_isBlockingPopupOpened = false;
  }

  private final func SendPSMRadialCloseRequest() -> Void {
    let psmEvent: ref<PSMPostponedParameterBool> = new PSMPostponedParameterBool();
    psmEvent.id = n"RadialWheelCloseRequest";
    psmEvent.value = true;
    this.GetPlayerControlledObject().QueueEvent(psmEvent);
  }

  private final func CanShowExclusivePopUp() -> Bool {
    return !this.m_isInMenu && !this.m_isInPhotoMode && !this.m_isBlockingPopupOpened && !this.m_isOnPhone;
  }

  private final func SetShardReadVisibility(hideToken: Bool) -> Void {
    if hideToken && this.m_shardToken != null {
      this.m_shardReadOnHold = true;
      this.m_shardToken = null;
      return;
    };
    if !hideToken && this.m_shardToken == null && this.m_shardReadOnHold {
      this.m_shardReadOnHold = false;
      this.ShardRead();
    };
  }

  protected cb func OnUpdateData(value: Variant) -> Bool {
    this.m_tutorialOnHold = false;
    this.m_tutorialData = FromVariant<PopupData>(value);
    this.m_tutorialSettings = FromVariant<PopupSettings>(this.m_blackboard.GetVariant(this.m_bbDefinition.Popup_Settings));
    this.ShowTutorial();
  }

  protected cb func OnPopupCloseRequest(data: ref<inkGameNotificationData>) -> Bool {
    this.m_tutorialToken = null;
    this.m_blackboard.SetBool(this.m_bbDefinition.Popup_IsShown, false);
  }

  private final func ShowTutorial() -> Void {
    let notificationData: ref<TutorialPopupData> = new TutorialPopupData();
    notificationData.notificationName = n"base\\gameplay\\gui\\widgets\\notifications\\tutorial.inkwidget";
    notificationData.queueName = n"tutorial";
    notificationData.closeAtInput = this.m_tutorialSettings.closeAtInput;
    notificationData.pauseGame = this.m_tutorialSettings.pauseGame;
    notificationData.position = this.m_tutorialSettings.position;
    notificationData.isModal = this.m_tutorialSettings.fullscreen;
    notificationData.margin = this.m_tutorialSettings.margin;
    notificationData.title = this.m_tutorialData.title;
    notificationData.message = this.m_tutorialData.message;
    notificationData.messageOverrideDataList = this.m_tutorialData.messageOverrideDataList;
    notificationData.imageId = this.m_tutorialData.iconID;
    notificationData.videoType = this.m_tutorialData.videoType;
    notificationData.video = PopupData.GetVideo(this.m_tutorialData);
    notificationData.isBlocking = this.m_tutorialSettings.closeAtInput;
    this.m_tutorialToken = this.ShowGameNotification(notificationData);
    this.m_tutorialToken.RegisterListener(this, n"OnPopupCloseRequest");
  }

  public final native func ChangeTutorialVisibilityInMenu(isInMenu: Bool) -> Void;

  public final native func ChangeTutorialVisibility(hideTutorial: Bool, reason: gameuiTutorialHiddenReason) -> Void;

  protected cb func OnExpansionPopupRequest(evt: ref<OpenExpansionPopupEvent>) -> Bool {
    let uiSystem: ref<UISystem> = GameInstance.GetUISystem(this.GetPlayerControlledObject().GetGame());
    let isPatchIntroNeeded: Bool = uiSystem.IsPatchIntroNeeded(gameuiPatchIntro.Patch2000_EP1);
    let isDownloaded: Bool = Equals(evt.m_state, ExpansionStatus.Downloaded);
    if !evt.m_forcibly && (!isPatchIntroNeeded || !isDownloaded) {
      return true;
    };
    this.ShowExpansionPopup(evt.m_state, evt.m_type);
  }

  public final func ShowExpansionPopup(state: ExpansionStatus, type: ExpansionPopupType) -> Void {
    let expansionPopupData: ref<ExpansionPopupData> = new ExpansionPopupData();
    expansionPopupData.m_type = type;
    expansionPopupData.m_state = state;
    expansionPopupData.isBlocking = true;
    expansionPopupData.useCursor = true;
    expansionPopupData.queueName = n"expansion";
    expansionPopupData.notificationName = n"base\\gameplay\\gui\\fullscreen\\dlc\\expansion_popup.inkwidget";
    this.m_expansionToken = this.ShowGameNotification(expansionPopupData);
    this.m_expansionToken.RegisterListener(this, n"OnExpansionPopupCloseRequest");
  }

  protected cb func OnExpansionPopupCloseRequest(data: ref<inkGameNotificationData>) -> Bool {
    let uiSystem: ref<UISystem>;
    let expansionPopupData: ref<ExpansionPopupData> = data as ExpansionPopupData;
    if Equals(expansionPopupData.m_state, ExpansionStatus.Downloaded) {
      uiSystem = GameInstance.GetUISystem(this.GetPlayerControlledObject().GetGame());
      uiSystem.MarkPatchIntroAsSeen(gameuiPatchIntro.Patch2000_EP1);
    };
    this.m_expansionToken = null;
  }

  protected cb func OnExpansionErrorPopupRequest(evt: ref<OpenExpansionErrorPopupEvent>) -> Bool {
    let expansionErrorData: ref<ExpansionErrorPopuppData> = new ExpansionErrorPopuppData();
    expansionErrorData.title = evt.m_title;
    expansionErrorData.description = evt.m_description;
    expansionErrorData.errorCode = evt.m_errorCode;
    expansionErrorData.isBlocking = true;
    expansionErrorData.useCursor = true;
    expansionErrorData.queueName = n"ExpansionError";
    expansionErrorData.notificationName = n"base\\gameplay\\gui\\fullscreen\\dlc\\expansion_error_popup.inkwidget";
    this.m_expansionErrorToken = this.ShowGameNotification(expansionErrorData);
    this.m_expansionErrorToken.RegisterListener(this, n"OnExpansionErrorPopupCloseRequest");
  }

  protected cb func OnExpansionErrorPopupCloseRequest(data: ref<inkGameNotificationData>) -> Bool {
    this.m_expansionErrorToken = null;
  }

  protected cb func OnPatchNotesPopupRequest(evt: ref<OpenPatchNotesPopupEvent>) -> Bool {
    this.ShowPatchNotesPopup();
  }

  public final func ShowPatchNotesPopup() -> Void {
    let patchNotesPopupData: ref<PatchNotesPopupData> = new PatchNotesPopupData();
    patchNotesPopupData.isBlocking = true;
    patchNotesPopupData.useCursor = true;
    patchNotesPopupData.queueName = n"patchnotes";
    patchNotesPopupData.notificationName = n"base\\gameplay\\gui\\fullscreen\\dlc\\patch_notes.inkwidget";
    this.m_patchNotesToken = this.ShowGameNotification(patchNotesPopupData);
    this.m_patchNotesToken.RegisterListener(this, n"OnPatchNotesPopupCloseRequest");
  }

  protected cb func OnPatchNotesPopupCloseRequest(data: ref<inkGameNotificationData>) -> Bool {
    this.m_patchNotesToken = null;
  }

  protected cb func OnMarketingConsentPopupRequest(evt: ref<MarketingConsentPopupEvent>) -> Bool {
    this.ShowMarketingConsentPopup(evt.m_type);
  }

  public final func ShowMarketingConsentPopup(type: inkMarketingConsentPopupType) -> Void {
    let marketingConsentPopupData: ref<MarketingConsentPopupData> = new MarketingConsentPopupData();
    marketingConsentPopupData.m_type = type;
    marketingConsentPopupData.isBlocking = true;
    marketingConsentPopupData.useCursor = true;
    marketingConsentPopupData.queueName = n"marketingConsent";
    marketingConsentPopupData.notificationName = n"base\\gameplay\\gui\\fullscreen\\main_menu\\marketing_consent_popup.inkwidget";
    this.m_marketingConsentToken = this.ShowGameNotification(marketingConsentPopupData);
    this.m_marketingConsentToken.RegisterListener(this, n"OnMarketingConsentPopupCloseRequest");
  }

  protected cb func OnMarketingConsentPopupCloseRequest(data: ref<inkGameNotificationData>) -> Bool {
    this.m_marketingConsentToken = null;
  }

  protected cb func OnSignInPopupRequest(evt: ref<SignInPopupEvent>) -> Bool {
    this.ShowSignInPopup();
  }

  public final func ShowSignInPopup() -> Void {
    let signInPopupData: ref<SignInPopupData> = new SignInPopupData();
    signInPopupData.isBlocking = true;
    signInPopupData.useCursor = true;
    signInPopupData.queueName = n"signIn";
    signInPopupData.notificationName = n"base\\gameplay\\gui\\fullscreen\\main_menu\\sign_in_popup.inkwidget";
    this.m_signInToken = this.ShowGameNotification(signInPopupData);
    this.m_signInToken.RegisterListener(this, n"OnSignInPopupCloseRequest");
  }

  protected cb func OnSignInPopupCloseRequest(data: ref<inkGameNotificationData>) -> Bool {
    this.m_signInToken = null;
  }

  protected cb func OnCodexPopupRequest(evt: ref<OpenCodexPopupEvent>) -> Bool {
    let codexPopupData: ref<CodexPopupData> = new CodexPopupData();
    codexPopupData.m_entry = evt.m_entry;
    codexPopupData.isBlocking = true;
    codexPopupData.useCursor = true;
    codexPopupData.queueName = n"codex";
    codexPopupData.notificationName = n"base\\gameplay\\gui\\widgets\\notifications\\codex_popup.inkwidget";
    this.m_codexToken = this.ShowGameNotification(codexPopupData);
    this.m_codexToken.RegisterListener(this, n"OnCodexPopupCloseRequest");
  }

  protected cb func OnCodexPopupCloseRequest(data: ref<inkGameNotificationData>) -> Bool {
    let evt: ref<CodexPopupClosedEvent> = new CodexPopupClosedEvent();
    this.QueueBroadcastEvent(evt);
    this.m_codexToken = null;
  }

  public final func ShowPhoneMessage() -> Void {
    this.m_phoneMessageToken = this.ShowGameNotification(this.m_phoneMessageData);
    this.m_phoneMessageToken.RegisterListener(this, n"OnMessagePopupUseCloseRequest");
  }

  protected cb func OnPhoneMessageShowRequest(evt: ref<PhoneMessagePopupEvent>) -> Bool {
    this.m_phoneMessageData = evt.m_data;
    this.ShowPhoneMessage();
  }

  protected cb func OnPhoneMessageHideRequest(evt: ref<PhoneMessageHidePopupEvent>) -> Bool {
    this.m_phoneMessageToken = null;
  }

  protected cb func OnMessagePopupUseCloseRequest(data: ref<inkGameNotificationData>) -> Bool {
    this.m_phoneMessageToken = null;
  }

  protected cb func OnShardRead(evt: ref<NotifyShardRead>) -> Bool {
    this.m_shardReadData = evt;
    this.ShardRead();
  }

  public final func ShardRead() -> Void {
    let notificationData: ref<ShardReadPopupData> = new ShardReadPopupData();
    notificationData.notificationName = n"base\\gameplay\\gui\\widgets\\notifications\\shard_notification.inkwidget";
    notificationData.queueName = n"shards";
    notificationData.requiredGameState = n"inkGameState";
    notificationData.isBlocking = true;
    notificationData.useCursor = false;
    notificationData.title = this.m_shardReadData.title;
    notificationData.text = this.m_shardReadData.text;
    notificationData.isCrypted = this.m_shardReadData.isCrypted;
    notificationData.itemID = this.m_shardReadData.itemID;
    notificationData.m_imageId = this.m_shardReadData.m_imageId;
    this.m_journalManager.SetEntryVisited(this.m_shardReadData.entry, true);
    this.m_shardToken = this.ShowGameNotification(notificationData);
    this.m_shardToken.RegisterListener(this, n"OnShardReadClosed");
    if notificationData.isCrypted {
      this.ProcessCrackableShardTutorial();
    };
  }

  public final func ProcessCrackableShardTutorial() -> Void {
    let questSystem: ref<QuestsSystem> = GameInstance.GetQuestsSystem(this.GetPlayerControlledObject().GetGame());
    if questSystem.GetFact(n"encoded_shard_tutorial") == 0 && questSystem.GetFact(n"disable_tutorials") == 0 {
      questSystem.SetFact(n"encoded_shard_tutorial", 1);
    };
  }

  protected cb func OnShardReadClosed(data: ref<inkGameNotificationData>) -> Bool {
    this.m_shardToken = null;
    this.PlaySound(n"Button", n"OnPress");
  }

  protected cb func OnQuickSlotButtonHoldStartEvent(evt: ref<QuickSlotButtonHoldStartEvent>) -> Bool {
    switch evt.dPadItemDirection {
      case EDPadSlot.VehicleWheel:
        this.SpawnVehiclesManagerPopup();
        break;
      case EDPadSlot.VehicleInsideWheel:
        this.TrySpawnVehicleRadioPopup();
        break;
      case EDPadSlot.PocketRadio:
        this.TrySpawnPocketRadioPopup();
        break;
      case EDPadSlot.VehicleVisualCustomization:
        this.TrySpawnVehicleVisualCustomizationSelectorPopup();
        break;
      default:
    };
  }

  private final func SpawnVehiclesManagerPopup() -> Void {
    let data: ref<inkGameNotificationData>;
    if !this.CanShowExclusivePopUp() {
      return;
    };
    this.m_isBlockingPopupOpened = true;
    data = new inkGameNotificationData();
    data.notificationName = n"base\\gameplay\\gui\\widgets\\vehicle_control\\vehicles_manager.inkwidget";
    data.queueName = n"VehiclesManager";
    data.isBlocking = false;
    this.m_vehiclesManagerToken = this.ShowGameNotification(data);
    this.m_vehiclesManagerToken.RegisterListener(this, n"OnVehiclesManagerCloseRequest");
    this.m_blackboard.SetBool(this.m_bbDefinition.Popup_VehiclesManager_IsShown, true);
  }

  protected cb func OnVehiclesManagerCloseRequest(data: ref<inkGameNotificationData>) -> Bool {
    this.m_vehiclesManagerToken = null;
    this.m_isBlockingPopupOpened = false;
    this.m_blackboard.SetBool(this.m_bbDefinition.Popup_VehiclesManager_IsShown, false);
  }

  private final func TrySpawnVehicleRadioPopup() -> Void {
    if this.m_blackboard.GetBool(this.m_bbDefinition.Popup_Radio_Enabled) && !this.m_blackboard.GetBool(GetAllBlackboardDefs().UIGameData.Popup_Radio_IsShown) {
      this.SpawnVehicleRadioPopup();
    } else {
      this.ShowActionBlockedNotification();
    };
  }

  private final func TrySpawnPocketRadioPopup() -> Void {
    if !this.m_blackboard.GetBool(GetAllBlackboardDefs().UIGameData.Popup_Radio_IsShown) {
      this.SpawnVehicleRadioPopup();
    };
  }

  private final func SpawnVehicleRadioPopup() -> Void {
    let data: ref<inkGameNotificationData>;
    if !this.CanShowExclusivePopUp() {
      return;
    };
    this.m_isBlockingPopupOpened = true;
    data = new inkGameNotificationData();
    data.notificationName = n"base\\gameplay\\gui\\widgets\\vehicle_control\\vehicles_radio.inkwidget";
    data.queueName = n"VehiclesRadio";
    data.isBlocking = false;
    this.m_vehicleRadioToken = this.ShowGameNotification(data);
    this.m_vehicleRadioToken.RegisterListener(this, n"OnVehicleRadioCloseRequest");
    this.m_blackboard.SetBool(this.m_bbDefinition.Popup_Radio_IsShown, true);
  }

  protected cb func OnVehicleRadioCloseRequest(data: ref<inkGameNotificationData>) -> Bool {
    this.m_vehicleRadioToken = null;
    this.m_isBlockingPopupOpened = false;
    this.m_blackboard.SetBool(this.m_bbDefinition.Popup_Radio_IsShown, false);
  }

  private final func TrySpawnVehicleVisualCustomizationSelectorPopup() -> Void {
    if !this.m_blackboard.GetBool(GetAllBlackboardDefs().UIGameData.Popup_CarColorPicker_IsShown) {
      this.SpawnVehicleVisualCustomizationSelectorPopup();
    };
  }

  private final func SpawnVehicleVisualCustomizationSelectorPopup() -> Void {
    if !this.CanShowExclusivePopUp() {
      return;
    };
    this.m_isBlockingPopupOpened = true;
    this.m_vehicleColorSelectorData = new inkGameNotificationData();
    this.m_vehicleColorSelectorData.notificationName = n"base\\gameplay\\gui\\widgets\\notifications\\vehicle_visual_customization.inkwidget";
    this.m_vehicleColorSelectorData.queueName = n"VehicleVisualCustomization";
    this.m_vehicleColorSelectorData.isBlocking = true;
    this.m_vehicleColorSelectorData.useCursor = true;
    this.m_vehicleVisualCustomizationSelectorToken = this.ShowGameNotification(this.m_vehicleColorSelectorData);
    this.m_vehicleVisualCustomizationSelectorToken.RegisterListener(this, n"OnVehicleVisualCustomizationCloseRequest");
    this.m_blackboard.SetBool(this.m_bbDefinition.Popup_CarColorPicker_IsShown, true);
  }

  protected cb func OnVehicleVisualCustomizationCloseRequest(data: ref<inkGameNotificationData>) -> Bool {
    this.m_vehicleVisualCustomizationSelectorToken = null;
    this.m_isBlockingPopupOpened = false;
    this.m_blackboard.SetBool(this.m_bbDefinition.Popup_CarColorPicker_IsShown, false);
  }

  protected cb func OnFrameSwitcherEvent(evt: ref<FrameSwitcherEvent>) -> Bool {
    if !this.CanShowExclusivePopUp() {
      return false;
    };
    this.m_isBlockingPopupOpened = true;
    this.m_smartFrameData = new inkFrameNotificationData();
    this.m_smartFrameData.notificationName = n"base\\gameplay\\devices\\frames\\base\\frame_switcher.inkwidget";
    this.m_smartFrameData.queueName = n"FrameSwitcher";
    this.m_smartFrameData.isBlocking = true;
    this.m_smartFrameData.useCursor = true;
    this.m_smartFrameData.frame = evt.frame;
    this.m_smartFrameData.hash = evt.hash;
    this.m_smartFrameData.index = evt.index;
    this.m_smartFrameData.uv = evt.uv;
    this.m_smartFrameData.shouldApply = false;
    this.m_frameSwitcherToken = this.ShowGameNotification(this.m_smartFrameData);
    this.m_frameSwitcherToken.RegisterListener(this, n"OnFrameSwitcherCloseRequest");
    this.m_uiSystem.PushGameContext(UIGameContext.QuickHack);
  }

  protected cb func OnFrameSwitcherCloseRequest(data: ref<inkFrameNotificationData>) -> Bool {
    this.m_frameSwitcherToken = null;
    if data == null {
      this.NotifyFrameCancel(this.m_smartFrameData);
    } else {
      if data.shouldApply {
        this.NotifyFrameSwitch(data);
      } else {
        this.NotifyFrameCancel(data);
      };
    };
    this.m_smartFrameData = null;
    this.m_isBlockingPopupOpened = false;
    this.m_uiSystem.PopGameContext(UIGameContext.QuickHack);
  }

  private final func NotifyFrameSwitch(data: ref<inkFrameNotificationData>) -> Void {
    let evt: ref<FrameSwitch> = new FrameSwitch();
    evt.hash = data.hash;
    evt.index = data.index;
    evt.uv = data.uv;
    data.frame.QueueEvent(evt);
  }

  private final func NotifyFrameCancel(data: ref<inkFrameNotificationData>) -> Void {
    let evt: ref<FrameCancel> = new FrameCancel();
    data.frame.QueueEvent(evt);
  }

  private final func ShowActionBlockedNotification() -> Void {
    let notificationEvent: ref<UIInGameNotificationEvent> = new UIInGameNotificationEvent();
    notificationEvent.m_notificationType = UIInGameNotificationType.CombatRestriction;
    let uiSystem: ref<UISystem> = GameInstance.GetUISystem(this.GetPlayerControlledObject().GetGame());
    if IsDefined(uiSystem) {
      uiSystem.QueueEvent(new UIInGameNotificationRemoveEvent());
      uiSystem.QueueEvent(notificationEvent);
    };
  }

  protected cb func OnSpawnPonRRewardsScreen(evt: ref<ShowPointOfNoReturnPromptEvent>) -> Bool {
    let notificationData: ref<inkGameNotificationData> = new inkGameNotificationData();
    notificationData.notificationName = n"base\\gameplay\\gui\\widgets\\ponr\\ponr_rewards.inkwidget";
    notificationData.queueName = n"PonR";
    notificationData.isBlocking = true;
    notificationData.useCursor = true;
    this.m_ponrToken = this.ShowGameNotification(notificationData);
    this.m_ponrToken.RegisterListener(this, n"OnClosePonRRewardsScreen");
  }

  protected cb func OnClosePonRRewardsScreen(data: ref<inkGameNotificationData>) -> Bool {
    this.m_ponrToken = null;
  }

  protected cb func OnSpawnTwintoneOverride(evt: ref<ShowTwintoneOverrideEvent>) -> Bool {
    let notificationData: ref<TwintoneOverrideData>;
    if !this.CanShowExclusivePopUp() {
      return false;
    };
    this.m_isBlockingPopupOpened = true;
    notificationData = new TwintoneOverrideData();
    notificationData.notificationName = n"base\\gameplay\\gui\\widgets\\scanning\\twintone\\twintoneoverride.inkwidget";
    notificationData.queueName = n"twintoneOverride";
    notificationData.isBlocking = true;
    notificationData.useCursor = false;
    notificationData.templateType = evt.templateType;
    notificationData.modelName = evt.modelName;
    this.m_twintoneOverride = this.ShowGameNotification(notificationData);
    this.m_twintoneOverride.RegisterListener(this, n"OnCloseTwintoneOverrideScreen");
  }

  protected cb func OnCloseTwintoneOverrideScreen(data: ref<inkGameNotificationData>) -> Bool {
    let evt: ref<OnTwintoneOverrideClosedEvent> = new OnTwintoneOverrideClosedEvent();
    this.m_twintoneOverride = null;
    this.m_isBlockingPopupOpened = false;
    this.m_blackboard.SetBool(this.m_bbDefinition.Popup_TwintoneOverride_IsShown, false);
    this.GetPlayerControlledObject().QueueEvent(evt);
  }

  protected cb func OnAdditionalContentInstallationRequestResult(id: CName, success: Bool) -> Bool {
    if success {
      this.ShowExpansionStatePopupRequest(ExpansionStatus.Owned);
    };
  }

  protected cb func OnAdditionalContentInstallationResult(id: CName, success: Bool) -> Bool {
    if success {
      if !this.GetSystemRequestsHandler().IsPreGame() {
        GameInstance.GetUISystem(this.GetPlayerControlledObject().GetGame()).ResetPatchIntro(gameuiPatchIntro.Patch2000_EP1);
        this.ShowExpansionStateGameNotificationRequest(ExpansionStatus.Downloaded);
      } else {
        this.ShowExpansionStatePopupRequest(ExpansionStatus.Reloading);
      };
    };
  }

  protected cb func OnAdditionalContentDataReloadProgress(progress: Float) -> Bool {
    if progress >= 1.00 {
      this.m_expansionStateToken = null;
    };
  }

  public final func ShowExpansionStatePopupRequest(state: ExpansionStatus) -> Void {
    let expansionPopupData: ref<ExpansionPopupData>;
    if !IsFinal() {
      expansionPopupData = new ExpansionPopupData();
      expansionPopupData.m_state = state;
      expansionPopupData.isBlocking = false;
      expansionPopupData.useCursor = false;
      expansionPopupData.queueName = n"ExpansionStatePopup";
      expansionPopupData.notificationName = n"base\\gameplay\\gui\\fullscreen\\dlc\\expansion_state_popup.inkwidget";
      this.m_expansionStateToken = this.ShowGameNotification(expansionPopupData);
      this.m_expansionStateToken.RegisterListener(this, n"OnExpansionStatePopupCloseRequest");
    };
  }

  protected cb func OnExpansionStatePopupCloseRequest(data: ref<inkGameNotificationData>) -> Bool {
    this.m_expansionStateToken = null;
  }

  public final func ShowExpansionStateGameNotificationRequest(state: ExpansionStatus) -> Void {
    let notificationEvent: ref<UIInGameNotificationEvent>;
    if Equals(state, ExpansionStatus.Downloaded) {
      notificationEvent = new UIInGameNotificationEvent();
      notificationEvent.m_notificationType = UIInGameNotificationType.ExpansionInstalled;
      notificationEvent.m_overrideCurrentNotification = false;
      GameInstance.GetUISystem(this.GetPlayerControlledObject().GetGame()).QueueEvent(notificationEvent);
    };
  }
}
