
public class ConsumeAction extends BaseItemAction {

  public func CompleteAction(gameInstance: GameInstance) -> Void {
    let removeConsumableEvent: ref<RemoveConsumableDelayedEvent>;
    let removePoint: Float;
    super.CompleteAction(gameInstance);
    if this.ShouldRemoveAfterUse() {
      this.RemoveConsumableItem(gameInstance);
    } else {
      removePoint = TweakDBInterface.GetConsumableItemRecord(ItemID.GetTDBID(this.GetItemData().GetID())).RemovePoint();
      removeConsumableEvent = new RemoveConsumableDelayedEvent();
      removeConsumableEvent.consumeAction = this;
      GameInstance.GetDelaySystem(gameInstance).DelayEvent(this.GetExecutor(), removeConsumableEvent, removePoint);
    };
    if this.GetItemData().HasTag(n"IllegalFood") {
      this.ProcessPermanentFood(gameInstance);
    };
    this.NotifyAutocraftSystem(gameInstance);
  }

  private final func ProcessPermanentFood(gameInstance: GameInstance) -> Void {
    let arrayValues: array<Float>;
    let currentBonus: Int32;
    let tooltipValues: array<Float>;
    let player: ref<PlayerPuppet> = GetPlayer(gameInstance);
    let modifier: ref<gameConstantStatModifierData> = new gameConstantStatModifierData();
    modifier.modifierType = gameStatModifierType.Additive;
    if this.GetItemData().GetID() == t"Items.PermanentHealthFood" {
      arrayValues = TDB.GetFloatArray(t"Items.PermanentHealthFood.healthIncreasePerUse");
      currentBonus = Cast<Int32>(player.GetPermanentFoodBonus(gamedataStatType.HealthBonusBlackmarket));
      modifier.statType = gamedataStatType.Health;
      modifier.value = arrayValues[currentBonus];
      GameInstance.GetStatsSystem(gameInstance).AddSavedModifier(Cast<StatsObjectID>(player.GetEntityID()), modifier);
      if currentBonus + 1 < ArraySize(arrayValues) {
        player.SetPermanentFoodBonus(gamedataStatType.HealthBonusBlackmarket, Cast<Float>(currentBonus) + 1.00);
        GameInstance.GetStatsSystem(gameInstance).RemoveAllModifiers(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.HealthBonusBlackmarket, true);
        tooltipValues = TDB.GetFloatArray(t"Items.PermanentHealthFood.healthIncreasePerUse");
        modifier.statType = gamedataStatType.HealthBonusBlackmarket;
        modifier.value = tooltipValues[currentBonus + 1];
        GameInstance.GetStatsSystem(gameInstance).AddSavedModifier(Cast<StatsObjectID>(player.GetEntityID()), modifier);
      };
    } else {
      if this.GetItemData().GetID() == t"Items.PermanentStaminaRegenFood" {
        arrayValues = TDB.GetFloatArray(t"Items.PermanentStaminaRegenFood.staminaRegenRateIncreasePerUse");
        currentBonus = Cast<Int32>(player.GetPermanentFoodBonus(gamedataStatType.StaminaRegenBonusBlackmarket));
        modifier.statType = gamedataStatType.StaminaRegenRateAdd;
        modifier.value = arrayValues[currentBonus];
        GameInstance.GetStatsSystem(gameInstance).AddSavedModifier(Cast<StatsObjectID>(player.GetEntityID()), modifier);
        if currentBonus + 1 < ArraySize(arrayValues) {
          player.SetPermanentFoodBonus(gamedataStatType.StaminaRegenBonusBlackmarket, Cast<Float>(currentBonus) + 1.00);
          GameInstance.GetStatsSystem(gameInstance).RemoveAllModifiers(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.StaminaRegenBonusBlackmarket, true);
          tooltipValues = TDB.GetFloatArray(t"Items.PermanentStaminaRegenFood.staminaNotificationValues");
          modifier.statType = gamedataStatType.StaminaRegenBonusBlackmarket;
          modifier.value = tooltipValues[currentBonus + 1];
          GameInstance.GetStatsSystem(gameInstance).AddSavedModifier(Cast<StatsObjectID>(player.GetEntityID()), modifier);
        };
      } else {
        if this.GetItemData().GetID() == t"Items.PermanentMemoryRegenFood" {
          arrayValues = TDB.GetFloatArray(t"Items.PermanentMemoryRegenFood.memoryRegenRateIncreasePerUse");
          currentBonus = Cast<Int32>(player.GetPermanentFoodBonus(gamedataStatType.MemoryRegenBonusBlackmarket));
          modifier.statType = gamedataStatType.MemoryRegenRateMult;
          modifier.value = arrayValues[currentBonus];
          GameInstance.GetStatsSystem(gameInstance).AddSavedModifier(Cast<StatsObjectID>(player.GetEntityID()), modifier);
          if currentBonus + 1 < ArraySize(arrayValues) {
            player.SetPermanentFoodBonus(gamedataStatType.MemoryRegenBonusBlackmarket, Cast<Float>(currentBonus) + 1.00);
            GameInstance.GetStatsSystem(gameInstance).RemoveAllModifiers(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.MemoryRegenBonusBlackmarket, true);
            tooltipValues = TDB.GetFloatArray(t"Items.PermanentMemoryRegenFood.memoryNotificationValues");
            modifier.statType = gamedataStatType.MemoryRegenBonusBlackmarket;
            modifier.value = tooltipValues[currentBonus + 1];
            GameInstance.GetStatsSystem(gameInstance).AddSavedModifier(Cast<StatsObjectID>(player.GetEntityID()), modifier);
          };
        };
      };
    };
  }

  public final func RemoveConsumableItem(gameInstance: GameInstance) -> Void {
    let blackboard: ref<IBlackboard>;
    let blackboardSystem: ref<BlackboardSystem>;
    let eqs: ref<EquipmentSystem>;
    GameInstance.GetTransactionSystem(gameInstance).RemoveItem(this.GetExecutor(), this.GetItemData().GetID(), 1);
    eqs = GameInstance.GetScriptableSystemsContainer(gameInstance).Get(n"EquipmentSystem") as EquipmentSystem;
    if IsDefined(eqs) {
    };
    if this.ShouldEquipAnotherConsumable() {
      this.TryToEquipSameTypeConsumable();
    };
    blackboardSystem = GameInstance.GetBlackboardSystem(gameInstance);
    blackboard = blackboardSystem.Get(GetAllBlackboardDefs().UI_QuickSlotsData);
    blackboard.SetVariant(GetAllBlackboardDefs().UI_QuickSlotsData.consumableBeingUsed, ToVariant(ItemID.None()));
  }

  private final func ShouldEquipAnotherConsumable() -> Bool {
    if this.GetItemData().GetQuantity() > 0 || this.ShouldRemoveAfterUse() {
      return false;
    };
    return true;
  }

  private final func TryToEquipSameTypeConsumable() -> Void {
    let bestQuality: Int32;
    let consumableQuality: Int32;
    let consumableRecord: ref<ConsumableItem_Record>;
    let consumableToEquip: InventoryItemData;
    let consumableType: gamedataConsumableType;
    let currentConsumable: InventoryItemData;
    let i: Int32;
    let inventoryItems: array<InventoryItemData>;
    let inventoryManager: ref<InventoryDataManagerV2> = new InventoryDataManagerV2();
    inventoryManager.Initialize(this.GetExecutor() as PlayerPuppet);
    currentConsumable = inventoryManager.GetInventoryItemData(this.GetItemData());
    consumableType = TweakDBInterface.GetConsumableItemRecord(ItemID.GetTDBID(this.GetItemData().GetID())).ConsumableType().Type();
    inventoryItems = inventoryManager.GetPlayerInventoryData(InventoryItemData.GetEquipmentArea(currentConsumable), true);
    if ArraySize(inventoryItems) == 0 {
      return;
    };
    i = 0;
    while i < ArraySize(inventoryItems) {
      consumableRecord = TweakDBInterface.GetConsumableItemRecord(ItemID.GetTDBID(InventoryItemData.GetID(inventoryItems[i])));
      consumableQuality = consumableRecord.Quality().Value();
      if Equals(consumableRecord.ConsumableType().Type(), consumableType) && consumableQuality >= bestQuality {
        bestQuality = consumableQuality;
        consumableToEquip = inventoryItems[i];
      };
      i += 1;
    };
    if !InventoryItemData.IsEmpty(consumableToEquip) {
      inventoryManager.EquipItem(InventoryItemData.GetID(consumableToEquip), InventoryItemData.GetSlotIndex(currentConsumable));
    };
  }

  protected func ProcessStatusEffects(const actionEffects: script_ref<[wref<ObjectActionEffect_Record>]>, gameInstance: GameInstance) -> Void {
    let actionEffectStatusEffectType: String;
    let appliedStatusEffectType: String;
    let effectInstigator: TweakDBID;
    let usedConsumableName: gamedataConsumableBaseName;
    let appliedEffects: array<ref<StatusEffect>> = StatusEffectHelper.GetAppliedEffects(this.GetExecutor());
    let newConsumableTDBID: TweakDBID = ItemID.GetTDBID(this.GetItemData().GetID());
    let newConsumableName: gamedataConsumableBaseName = TweakDBInterface.GetConsumableItemRecord(newConsumableTDBID).ConsumableBaseName().Type();
    let i: Int32 = 0;
    while i < ArraySize(appliedEffects) {
      effectInstigator = appliedEffects[i].GetInstigatorStaticDataID();
      usedConsumableName = TweakDBInterface.GetConsumableItemRecord(effectInstigator).ConsumableBaseName().Type();
      actionEffectStatusEffectType = Deref(actionEffects)[i].StatusEffect().StatusEffectType().EnumName();
      appliedStatusEffectType = appliedEffects[i].GetRecord().StatusEffectType().EnumName();
      if Equals(newConsumableName, usedConsumableName) && Equals(appliedStatusEffectType, actionEffectStatusEffectType) && Cast<Int32>(appliedEffects[i].GetMaxStacks()) == 1 {
        StatusEffectHelper.RemoveStatusEffect(this.GetExecutor(), appliedEffects[i]);
        break;
      };
      i += 1;
    };
    i = 0;
    while i < ArraySize(Deref(actionEffects)) {
      StatusEffectHelper.ApplyStatusEffect(this.GetExecutor(), Deref(actionEffects)[i].StatusEffect().GetID(), ItemID.GetTDBID(this.GetItemData().GetID()));
      i += 1;
    };
  }

  protected final func NotifyAutocraftSystem(gameInstance: GameInstance) -> Void {
    let autocraftSystem: ref<AutocraftSystem> = GameInstance.GetScriptableSystemsContainer(gameInstance).Get(n"AutocraftSystem") as AutocraftSystem;
    let autocraftItemUsedRequest: ref<RegisterItemUsedRequest> = new RegisterItemUsedRequest();
    autocraftItemUsedRequest.itemUsed = this.GetItemData().GetID();
    autocraftSystem.QueueRequest(autocraftItemUsedRequest);
  }

  public func IsVisible(const context: script_ref<GetActionsContext>, objectActionsCallbackController: wref<gameObjectActionsCallbackController>) -> Bool {
    if (this.GetExecutor() as PlayerPuppet).IsInCombat() && NotEquals(this.GetItemData().GetItemType(), gamedataItemType.Con_Skillbook) {
      return false;
    };
    return true;
  }
}
