
public class QuestOpen extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestOpen";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}

public class QuestClose extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestClose";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}

public class QuestExplode extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestExplode";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}

public class QuestStartHacking extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"StartHacking";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}

public class QuestStopHacking extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"StopHacking";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}

public class ServerOverload extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ServerOverload";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}
