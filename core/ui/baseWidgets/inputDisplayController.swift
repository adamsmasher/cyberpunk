
public class LabelInputDisplayController extends inkInputDisplayController {

  private edit let m_inputLabel: inkTextRef;

  public final func SetInputActionLabel(actionName: CName, const label: script_ref<String>) -> Void {
    inkTextRef.SetText(this.m_inputLabel, Deref(label));
    this.SetInputAction(actionName);
  }
}
