
public static func OperatorAdd(s: String, mode: gamecheatsystemFlag) -> String {
  return s + EnumValueToString("gamecheatsystemFlag", EnumInt(mode));
}

public static func OperatorAdd(mode: gamecheatsystemFlag, s: String) -> String {
  return EnumValueToString("gamecheatsystemFlag", EnumInt(mode)) + s;
}
