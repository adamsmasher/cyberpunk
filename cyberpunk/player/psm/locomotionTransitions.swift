
public abstract class LocomotionTransition extends DefaultTransition {

  public let m_ownerRecordId: TweakDBID;

  public let m_statModifierGroupId: Uint64;

  public let m_statModifierTDBNameDefault: String;

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_statModifierTDBNameDefault = NameToString(this.GetStateName());
    DefaultTransition.UppercaseFirstChar(this.m_statModifierTDBNameDefault);
    this.m_statModifierTDBNameDefault = "player_locomotion_data_" + this.m_statModifierTDBNameDefault;
    this.m_statModifierTDBNameDefault = "Player" + NameToString(this.GetStateMachineName()) + "." + this.m_statModifierTDBNameDefault;
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
  }

  protected final const func InternalEnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let capsuleHeight: Float = this.GetStaticFloatParameterDefault("capsuleHeight", 1.00);
    let capsuleRadius: Float = this.GetStaticFloatParameterDefault("capsuleRadius", 1.00);
    return scriptInterface.CanCapsuleFit(capsuleHeight, capsuleRadius);
  }

  protected final func SetModifierGroupForState(scriptInterface: ref<StateGameScriptInterface>, opt statModifierTDBName: String) -> Void {
    let statSystem: ref<StatsSystem> = scriptInterface.GetStatsSystem();
    let ownerRecordId: TweakDBID = (scriptInterface.owner as gamePuppetBase).GetRecordID();
    if this.m_ownerRecordId != ownerRecordId {
      this.m_ownerRecordId = ownerRecordId;
      TDBID.Append(ownerRecordId, t"PlayerLocomotion");
      this.m_statModifierGroupId = TDBID.ToNumber(ownerRecordId);
    };
    statSystem.RemoveModifierGroup(Cast<StatsObjectID>(scriptInterface.ownerEntityID), this.m_statModifierGroupId);
    statSystem.UndefineModifierGroup(this.m_statModifierGroupId);
    if Equals(statModifierTDBName, "") {
      statModifierTDBName = this.m_statModifierTDBNameDefault;
    };
    statSystem.DefineModifierGroupFromRecord(this.m_statModifierGroupId, TDBID.Create(statModifierTDBName));
    statSystem.ApplyModifierGroup(Cast<StatsObjectID>(scriptInterface.ownerEntityID), this.m_statModifierGroupId);
  }

  protected func EnableMovementDecelerationStatModifier(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, decelerationModifier: ref<gameStatModifierData>, movementDecelerationModifierVal: Float) -> ref<gameStatModifierData> {
    return this.SetMovementDecelerationStatModifier(stateContext, scriptInterface, true, decelerationModifier, movementDecelerationModifierVal);
  }

  protected func DisableMovementDecelerationStatModifier(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, decelerationModifier: ref<gameStatModifierData>) -> ref<gameStatModifierData> {
    return this.SetMovementDecelerationStatModifier(stateContext, scriptInterface, false, decelerationModifier);
  }

  protected func SetMovementDecelerationStatModifier(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, enable: Bool, decelerationModifier: ref<gameStatModifierData>, opt movementDecelerationModifierVal: Float) -> ref<gameStatModifierData> {
    let currDeceleration: Float;
    let ownerID: StatsObjectID = Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID);
    if enable && !IsDefined(decelerationModifier) {
      currDeceleration = this.GetStatFloatValue(scriptInterface, gamedataStatType.Deceleration, scriptInterface.GetStatsSystem());
      decelerationModifier = RPGManager.CreateStatModifier(gamedataStatType.Deceleration, gameStatModifierType.Additive, currDeceleration * movementDecelerationModifierVal);
      scriptInterface.GetStatsSystem().AddModifier(ownerID, decelerationModifier);
    } else {
      if !enable && IsDefined(decelerationModifier) {
        scriptInterface.GetStatsSystem().RemoveModifier(ownerID, decelerationModifier);
        decelerationModifier = null;
      };
    };
    return decelerationModifier;
  }

  protected final func ShowDebugText(const text: script_ref<String>, scriptInterface: ref<StateGameScriptInterface>, out layerId: Uint32) -> Void {
    layerId = GameInstance.GetDebugVisualizerSystem(scriptInterface.owner.GetGame()).DrawText(new Vector4(650.00, 100.00, 0.00, 0.00), Deref(text), gameDebugViewETextAlignment.Center, new Color(0u, 240u, 148u, 100u));
    GameInstance.GetDebugVisualizerSystem(scriptInterface.owner.GetGame()).SetScale(layerId, new Vector4(1.50, 1.50, 0.00, 0.00));
  }

  protected final func ClearDebugText(layerId: Uint32, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    GameInstance.GetDebugVisualizerSystem(scriptInterface.owner.GetGame()).ClearLayer(layerId);
  }

  protected final func ResetFallingParameters(stateContext: ref<StateContext>) -> Void {
    stateContext.SetPermanentIntParameter(n"LandingType", 0, true);
    stateContext.SetPermanentFloatParameter(n"ImpactSpeed", 0.00, true);
    stateContext.SetPermanentFloatParameter(n"InAirDuration", 0.00, true);
  }

  protected final func PlayHardLandingEffects(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.StartEffect(scriptInterface, n"landing_hard");
    this.PrepareGameEffectAoEAttack(stateContext, scriptInterface, TweakDBInterface.GetAttackRecord(t"Attacks.HardLanding"));
    this.SpawnLandingFxGameEffect(t"Attacks.HardLanding", scriptInterface);
    GameObject.PlayVoiceOver(scriptInterface.owner, n"hardLanding", n"Scripts:HardLandEvents");
    DefaultTransition.PlayRumble(scriptInterface, "medium_pulse");
  }

  protected final func PlayVeryHardLandingEffects(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.StartEffect(scriptInterface, n"landing_very_hard");
    this.PrepareGameEffectAoEAttack(stateContext, scriptInterface, TweakDBInterface.GetAttackRecord(t"Attacks.VeryHardLanding"));
    this.SpawnLandingFxGameEffect(t"Attacks.VeryHardLanding", scriptInterface);
    GameObject.PlayVoiceOver(scriptInterface.owner, n"veryhardLanding", n"Scripts:VeryHardLandEvents");
    DefaultTransition.PlayRumble(scriptInterface, "heavy_fast");
  }

  protected final func PlayDeathLandingEffects(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.StartEffect(scriptInterface, n"landing_death");
    this.PrepareGameEffectAoEAttack(stateContext, scriptInterface, TweakDBInterface.GetAttackRecord(t"Attacks.DeathLanding"));
    this.SpawnLandingFxGameEffect(t"Attacks.DeathLanding", scriptInterface);
    GameObject.PlayVoiceOver(scriptInterface.owner, n"veryhardLanding", n"Scripts:DeathLandEvents");
  }

  protected final func SendSuperHeroLandAnimFeature(scriptInterface: ref<StateGameScriptInterface>, state: Int32) -> Void {
    let animFeature: ref<AnimFeature_SuperheroLand> = new AnimFeature_SuperheroLand();
    animFeature.state = state;
    scriptInterface.SetAnimationParameterFeature(n"SuperheroLand", animFeature);
  }

  protected final func AddImpulseInMovingDirection(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, impulse: Float) -> Void {
    let direction: Vector4;
    if impulse == 0.00 {
      return;
    };
    direction = scriptInterface.GetOwnerMovingDirection();
    this.AddImpulse(stateContext, direction * impulse);
  }

  protected final func AddImpulse(stateContext: ref<StateContext>, impulse: Vector4) -> Void {
    stateContext.SetTemporaryVectorParameter(n"impulse", impulse, true);
  }

  protected final func AddVerticalImpulse(stateContext: ref<StateContext>, force: Float) -> Void {
    let impulse: Vector4;
    impulse.Z = force;
    this.AddImpulse(stateContext, impulse);
  }

  public final func SetCollisionFilter(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let simulationFilter: SimulationFilter;
    let zero: Bool = this.GetStaticBoolParameterDefault("collisionFilterPresetIsZero", false);
    if zero {
      simulationFilter = SimulationFilter.ZERO();
    } else {
      SimulationFilter.SimulationFilter_BuildFromPreset(simulationFilter, n"Player Collision");
    };
    scriptInterface.SetStateVectorParameter(physicsStateValue.SimulationFilter, ToVariant(simulationFilter));
  }

  public func SetLocomotionParameters(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> ref<LocomotionParameters> {
    let locomotionParameters: ref<LocomotionParameters>;
    this.SetModifierGroupForState(scriptInterface);
    locomotionParameters = new LocomotionParameters();
    this.GetStateDefaultLocomotionParameters(locomotionParameters);
    stateContext.SetTemporaryScriptableParameter(n"locomotionParameters", locomotionParameters, true);
    return locomotionParameters;
  }

  protected final func SetLocomotionCameraParameters(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let param: StateResultCName = this.GetStaticCNameParameter("onEnterCameraParamsName");
    if param.valid {
      stateContext.SetPermanentCNameParameter(n"LocomotionCameraParams", param.value, true);
      this.UpdateCameraParams(stateContext, scriptInterface);
    };
  }

  protected final const func GetStateDefaultLocomotionParameters(out locomotionParameters: ref<LocomotionParameters>) -> Void {
    locomotionParameters.SetUpwardsGravity(this.GetStaticFloatParameterDefault("upwardsGravity", -16.00));
    locomotionParameters.SetDownwardsGravity(this.GetStaticFloatParameterDefault("downwardsGravity", -16.00));
    locomotionParameters.SetImperfectTurn(this.GetStaticBoolParameterDefault("imperfectTurn", false));
    locomotionParameters.SetSpeedBoostInputRequired(this.GetStaticBoolParameterDefault("speedBoostInputRequired", false));
    locomotionParameters.SetSpeedBoostMultiplyByDot(this.GetStaticBoolParameterDefault("speedBoostMultiplyByDot", false));
    locomotionParameters.SetUseCameraHeadingForMovement(this.GetStaticBoolParameterDefault("useCameraHeadingForMovement", false));
    locomotionParameters.SetCapsuleHeight(this.GetStaticFloatParameterDefault("capsuleHeight", 1.80));
    locomotionParameters.SetCapsuleRadius(this.GetStaticFloatParameterDefault("capsuleRadius", 0.40));
    locomotionParameters.SetIgnoreSlope(this.GetStaticBoolParameterDefault("ignoreSlope", false));
  }

  protected final func BroadcastStimuliFootstepSprint(context: ref<StateGameScriptInterface>) -> Void {
    let broadcaster: ref<StimBroadcasterComponent>;
    let broadcastFootstepStim: Bool = GameInstance.GetStatsSystem(context.owner.GetGame()).GetStatValue(Cast<StatsObjectID>(context.owner.GetEntityID()), gamedataStatType.CanRunSilently) < 1.00;
    if broadcastFootstepStim {
      broadcaster = context.executionOwner.GetStimBroadcasterComponent();
      if IsDefined(broadcaster) {
        broadcaster.TriggerSingleBroadcast(context.executionOwner, gamedataStimType.FootStepSprint);
      };
    };
  }

  protected final func BroadcastStimuliFootstepRegular(context: ref<StateGameScriptInterface>) -> Void {
    let broadcaster: ref<StimBroadcasterComponent>;
    let broadcastFootstepStim: Bool = GameInstance.GetStatsSystem(context.owner.GetGame()).GetStatValue(Cast<StatsObjectID>(context.owner.GetEntityID()), gamedataStatType.CanWalkSilently) < 1.00;
    if broadcastFootstepStim {
      broadcaster = context.executionOwner.GetStimBroadcasterComponent();
      if IsDefined(broadcaster) {
        broadcaster.TriggerSingleBroadcast(context.executionOwner, gamedataStimType.FootStepRegular);
      };
    };
  }

  protected final func SetDetailedState(scriptInterface: ref<StateGameScriptInterface>, state: gamePSMDetailedLocomotionStates) -> Void {
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.LocomotionDetailed, EnumInt(state));
  }

  public final const func IsTouchingGround(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let onGround: Bool = scriptInterface.IsOnGround();
    return onGround;
  }

  public final const func HasSecureFooting(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.CheckSecureFootingFailure(this.HasSecureFootingDetailedResult(scriptInterface));
  }

  public final const func CheckSecureFootingFailure(const result: script_ref<SecureFootingResult>) -> Bool {
    return Equals(Deref(result).type, moveSecureFootingFailureType.Invalid);
  }

  public final const func HasSecureFootingDetailedResult(const scriptInterface: ref<StateGameScriptInterface>) -> SecureFootingResult {
    return scriptInterface.HasSecureFooting();
  }

  protected final const func GetFallingSpeedBasedOnHeight(const scriptInterface: ref<StateGameScriptInterface>, height: Float) -> Float {
    let acc: Float;
    let speed: Float;
    if height <= 0.00 {
      return 0.00;
    };
    acc = AbsF(this.GetStaticFloatParameterDefault("upwardsGravity", this.GetStaticFloatParameterDefault("defaultGravity", -16.00)));
    speed = 0.00;
    if acc != 0.00 {
      speed = acc * SqrtF((2.00 * height) / acc);
    };
    return speed * -1.00;
  }

  protected final const func GetInitialHeightBasedOnFallingSpeed(const scriptInterface: ref<StateGameScriptInterface>, fallSpeed: Float) -> Float {
    let acc: Float;
    let height: Float;
    if fallSpeed == 0.00 {
      return 0.00;
    };
    acc = AbsF(this.GetStaticFloatParameterDefault("upwardsGravity", this.GetStaticFloatParameterDefault("defaultGravity", -16.00)));
    height = acc * 0.50 * PowF(fallSpeed / acc, 2.00);
    return height;
  }

  protected final func GetSpeedBasedOnDistance(scriptInterface: ref<StateGameScriptInterface>, desiredDistance: Float) -> Float {
    let statSystem: ref<StatsSystem> = scriptInterface.GetStatsSystem();
    let deceleration: Float = this.GetStatFloatValue(scriptInterface, gamedataStatType.Deceleration, statSystem);
    return deceleration * SqrtF((2.00 * desiredDistance) / deceleration);
  }

  protected final const func IsCurrentFallSpeedTooFastToEnter(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let playerFallingTooFast: Float;
    let verticalSpeed: Float;
    if !scriptInterface.IsOnGround() {
      verticalSpeed = this.GetVerticalSpeed(scriptInterface);
      playerFallingTooFast = stateContext.GetFloatParameter(n"VeryHardLandingFallingSpeed", true);
      if verticalSpeed <= playerFallingTooFast {
        return true;
      };
    };
    return false;
  }

  protected final const func IsAiming(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.GetActionValue(n"CameraAim") > 0.00;
  }

  protected final const func WantsToDodge(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let disableAirDash: StateResultBool;
    let isAirDashPerkBought: Bool;
    let isInCooldown: Bool;
    let isStaminaPositive: Bool;
    if !scriptInterface.HasStatFlag(gamedataStatType.HasDodge) {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HealFood3") {
      return false;
    };
    isInCooldown = StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeCooldown") || StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeAirCooldown");
    if isInCooldown {
      return false;
    };
    disableAirDash = stateContext.GetPermanentBoolParameter(n"disableAirDash");
    isAirDashPerkBought = PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Reflexes_Central_Milestone_3) == 3;
    isStaminaPositive = GameInstance.GetStatPoolsSystem(scriptInterface.executionOwner.GetGame()).GetStatPoolValue(Cast<StatsObjectID>(scriptInterface.executionOwner.GetEntityID()), gamedataStatPoolType.Stamina, true) > 0.00;
    if !this.IsTouchingGround(scriptInterface) && (!isAirDashPerkBought || !isStaminaPositive || disableAirDash.valid && disableAirDash.value) {
      return false;
    };
    if this.IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface) {
      return false;
    };
    if scriptInterface.IsActionJustTapped(n"Dodge") {
      if scriptInterface.IsMoveInputConsiderable() {
        stateContext.SetConditionFloatParameter(n"DodgeDirection", scriptInterface.GetInputHeading(), true);
        return true;
      };
      if this.GetStaticBoolParameterDefault("dodgeWithNoMovementInput", false) {
        stateContext.SetConditionFloatParameter(n"DodgeDirection", -180.00, true);
        return true;
      };
    };
    if this.WantsToDodgeFromMovementInput(stateContext, scriptInterface) && GameplaySettingsSystem.GetMovementDodgeEnabled(scriptInterface.executionOwner) {
      return true;
    };
    return false;
  }

  protected final const func WantsToDodgeFromMovementInput(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let dodgeDirection: Float;
    if scriptInterface.IsActionJustPressed(n"DodgeForward") {
      dodgeDirection = 0.00;
      if scriptInterface.GetActionValue(n"Left") > 0.00 {
        dodgeDirection += 45.00;
      };
      if scriptInterface.GetActionValue(n"Right") > 0.00 {
        dodgeDirection -= 45.00;
      };
    } else {
      if scriptInterface.IsActionJustPressed(n"DodgeRight") {
        dodgeDirection = -90.00;
        if scriptInterface.GetActionValue(n"Forward") > 0.00 {
          dodgeDirection += 45.00;
        };
        if scriptInterface.GetActionValue(n"Back") > 0.00 {
          dodgeDirection -= 45.00;
        };
      } else {
        if scriptInterface.IsActionJustPressed(n"DodgeLeft") {
          dodgeDirection = 90.00;
          if scriptInterface.GetActionValue(n"Forward") > 0.00 {
            dodgeDirection -= 45.00;
          };
          if scriptInterface.GetActionValue(n"Back") > 0.00 {
            dodgeDirection += 45.00;
          };
        } else {
          if scriptInterface.IsActionJustPressed(n"DodgeBack") {
            dodgeDirection = -180.00;
            if scriptInterface.GetActionValue(n"Left") > 0.00 {
              dodgeDirection = 135.00;
            };
            if scriptInterface.GetActionValue(n"Right") > 0.00 {
              dodgeDirection += 45.00;
            };
          } else {
            return false;
          };
        };
      };
    };
    stateContext.SetConditionFloatParameter(n"DodgeDirection", dodgeDirection, true);
    return true;
  }

  protected final const func IsIdleForced(const stateContext: ref<StateContext>) -> Bool {
    return stateContext.GetBoolParameter(n"ForceIdle", true);
  }

  protected final const func IsWalkForced(const stateContext: ref<StateContext>) -> Bool {
    return stateContext.GetBoolParameter(n"ForceWalk", true);
  }

  protected final const func IsFreezeForced(const stateContext: ref<StateContext>) -> Bool {
    return stateContext.GetBoolParameter(n"ForceFreeze", true);
  }

  protected final const func GetLandingType(const stateContext: ref<StateContext>) -> Int32 {
    return stateContext.GetIntParameter(n"LandingType", true);
  }

  protected final func PlayRumbleBasedOnDodgeDirection(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, opt isHeavy: Bool) -> Void {
    let presetName: String;
    let movementDirection: EPlayerMovementDirection = DefaultTransition.GetMovementDirection(stateContext, scriptInterface);
    if Equals(movementDirection, EPlayerMovementDirection.Right) {
      presetName = isHeavy ? "heavy_pulse_right" : "medium_pulse_right";
    } else {
      if Equals(movementDirection, EPlayerMovementDirection.Left) {
        presetName = isHeavy ? "heavy_pulse_left" : "medium_pulse_left";
      } else {
        presetName = isHeavy ? "heavy_pulse" : "medium_pulse";
      };
    };
    DefaultTransition.PlayRumble(scriptInterface, presetName);
  }

  protected final const func IsStatusEffectType(statusEffectRecord: ref<StatusEffect_Record>, type: gamedataStatusEffectType) -> Bool {
    let effectType: gamedataStatusEffectType = statusEffectRecord.StatusEffectType().Type();
    return Equals(effectType, type);
  }

  protected final func SpawnLandingFxGameEffect(attackId: TweakDBID, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let effect: ref<EffectInstance> = scriptInterface.GetGameEffectSystem().CreateEffectStatic(n"landing", n"fx", scriptInterface.executionOwner);
    let position: Vector4 = scriptInterface.executionOwner.GetWorldPosition();
    position.Z += 0.50;
    EffectData.SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.range, 2.00);
    EffectData.SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, position);
    EffectData.SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.forward, new Vector4(0.00, 0.00, -1.00, 0.00));
    EffectData.SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attackId, ToVariant(attackId));
    effect.Run();
  }

  protected final const func ProcessSprintInputLock(stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if stateContext.GetBoolParameter(n"sprintInputLock", true) && scriptInterface.GetActionValue(n"Sprint") == 0.00 && scriptInterface.GetActionValue(n"ToggleSprint") == 0.00 {
      stateContext.RemovePermanentBoolParameter(n"sprintInputLock");
    };
  }

  protected final const func SetupSprintInputLock(stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if scriptInterface.GetActionValue(n"Sprint") != 0.00 || scriptInterface.GetActionValue(n"ToggleSprint") != 0.00 {
      stateContext.SetPermanentBoolParameter(n"sprintInputLock", true, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", true, true);
    };
  }

  protected final const func LogSpecialMovementToTelemetry(const scriptInterface: ref<StateGameScriptInterface>, mvtType: telemetryMovementType) -> Void {
    GameInstance.GetTelemetrySystem(scriptInterface.executionOwner.GetGame()).LogSpecialMovementPerformed(mvtType);
  }

  protected final const func IsPlayerAboveLadderTop(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let ladderTopPosition: Vector4;
    let playerPosition: Vector4;
    let ladderParameter: ref<LadderDescription> = stateContext.GetConditionScriptableParameter(n"usingLadder") as LadderDescription;
    if !IsDefined(ladderParameter) {
      return false;
    };
    playerPosition = DefaultTransition.GetPlayerPosition(scriptInterface);
    ladderTopPosition = ladderParameter.position + ladderParameter.up * ladderParameter.topHeightFromPosition;
    if playerPosition.Z < ladderTopPosition.Z {
      return false;
    };
    return true;
  }
}

public abstract class LocomotionEventsTransition extends LocomotionTransition {

  public let m_causeContactDestruction: Bool;

  public let m_activatedDestructionComponent: Bool;

  public let m_ignoreBarbedWire: Bool;

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnAttach(stateContext, scriptInterface);
    this.m_causeContactDestruction = this.GetStaticBoolParameterDefault("causeContactDestruction", false);
    this.m_ignoreBarbedWire = this.GetStaticBoolParameterDefault("ignoreBarbedWire", false);
    this.m_activatedDestructionComponent = false;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let activateEvent: ref<ActivateTriggerDestructionComponentEvent>;
    let prevStateIgnoredBarbedWire: Bool;
    let blockAimingFor: Float = this.GetStaticFloatParameterDefault("softBlockAimingOnEnterFor", -1.00);
    if blockAimingFor > 0.00 {
      this.SoftBlockAimingForTime(stateContext, scriptInterface, blockAimingFor);
    };
    this.SetLocomotionParameters(stateContext, scriptInterface);
    this.ResetGravityParametersForChargedJump(stateContext, scriptInterface);
    this.SetCollisionFilter(scriptInterface);
    this.SetLocomotionCameraParameters(stateContext, scriptInterface);
    if this.m_causeContactDestruction {
      activateEvent = new ActivateTriggerDestructionComponentEvent();
      scriptInterface.executionOwner.QueueEvent(activateEvent);
      this.m_activatedDestructionComponent = true;
    };
    prevStateIgnoredBarbedWire = scriptInterface.localBlackboard.GetFloat(GetAllBlackboardDefs().PlayerStateMachine.IgnoreBarbedWireStateEnterTime) > 0.00;
    if NotEquals(prevStateIgnoredBarbedWire, this.m_ignoreBarbedWire) {
      if this.m_ignoreBarbedWire {
        this.SetBlackboardFloatVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IgnoreBarbedWireStateEnterTime, EngineTime.ToFloat(GameInstance.GetSimTime(scriptInterface.GetGame())));
      } else {
        this.SetBlackboardFloatVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IgnoreBarbedWireStateEnterTime, -1.00);
      };
    };
  }

  private final const func ResetGravityParametersForChargedJump(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let defaultLocoParams: ref<LocomotionParameters>;
    let downwardsGravity: StateResultFloat;
    let upwardsGravity: StateResultFloat;
    let isGravityAffectedByChargedJump: StateResultBool = stateContext.GetPermanentBoolParameter(n"isGravityAffectedByChargedJump");
    if !this.IsTouchingGround(scriptInterface) && isGravityAffectedByChargedJump.valid && isGravityAffectedByChargedJump.value {
      upwardsGravity = stateContext.GetPermanentFloatParameter(n"chargedJumpUpwardsGravity");
      downwardsGravity = stateContext.GetPermanentFloatParameter(n"chargedJumpDownwardsGravity");
      defaultLocoParams = stateContext.GetTemporaryScriptableParameter(n"locomotionParameters") as LocomotionParameters;
      if IsDefined(defaultLocoParams) && upwardsGravity.valid && downwardsGravity.valid {
        defaultLocoParams.SetUpwardsGravity(upwardsGravity.value);
        defaultLocoParams.SetDownwardsGravity(downwardsGravity.value);
      };
    };
  }

  protected final func CleanupTriggerDestructionComponent(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let deactivateEvent: ref<DeactivateTriggerDestructionComponentEvent>;
    if this.m_activatedDestructionComponent {
      deactivateEvent = new DeactivateTriggerDestructionComponentEvent();
      scriptInterface.executionOwner.QueueEvent(deactivateEvent);
      this.m_activatedDestructionComponent = false;
    };
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.CleanupTriggerDestructionComponent(scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    this.CleanupTriggerDestructionComponent(scriptInterface);
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.ProcessSprintInputLock(stateContext, scriptInterface);
  }

  protected final func ConsumeStaminaBasedOnLocomotionState(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let staminaReduction: Float = 0.00;
    let staminaReductionMultiplier: Float = 1.00;
    let stateName: CName = this.GetStateName();
    switch stateName {
      case n"sprint":
        if !RPGManager.HasStatFlag(scriptInterface.executionOwner, gamedataStatType.IsSprintStaminaFree) {
          staminaReduction = PlayerStaminaHelpers.GetSprintStaminaCost();
        };
        break;
      case n"crouchSprint":
        staminaReduction = PlayerStaminaHelpers.GetCrouchSprintStaminaCost();
        if PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Cool_Central_Perk_3_4) == 1 && scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Combat) == 1 {
          staminaReduction *= TweakDBInterface.GetFloat(t"NewPerks.Cool_Central_Perk_3_4.crouchSprintStaminaCostMultiplier", 1.00);
        };
        staminaReductionMultiplier = scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.executionOwner.GetEntityID()), gamedataStatType.CrouchSprintStaminaCostReduction);
        if staminaReductionMultiplier > 0.00 {
          staminaReduction *= 1.00 - staminaReductionMultiplier;
        };
        break;
      case n"swimmingFastDiving":
      case n"swimmingSurfaceFast":
        staminaReduction = PlayerStaminaHelpers.GetSprintStaminaCost();
        break;
      case n"slide":
        staminaReduction = PlayerStaminaHelpers.GetSlideStaminaCost();
        break;
      case n"hoverJump":
      case n"chargeJump":
      case n"doubleJump":
      case n"jump":
        staminaReduction = PlayerStaminaHelpers.GetJumpStaminaCost();
        break;
      case n"dodge":
        if !RPGManager.HasStatFlag(scriptInterface.executionOwner, gamedataStatType.IsDodgeStaminaFree) {
          if this.IsTouchingGround(scriptInterface) {
            staminaReduction = PlayerStaminaHelpers.GetDodgeStaminaCost();
          } else {
            staminaReduction = PlayerStaminaHelpers.GetAirDodgeStaminaCost();
          };
          staminaReductionMultiplier = scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.executionOwner.GetEntityID()), gamedataStatType.DodgeStaminaCostReduction);
          if staminaReductionMultiplier > 0.00 {
            staminaReduction *= 1.00 - staminaReductionMultiplier;
          };
        };
        break;
      case n"dodgeAir":
        if !RPGManager.HasStatFlag(scriptInterface.executionOwner, gamedataStatType.IsDodgeStaminaFree) {
          staminaReduction = PlayerStaminaHelpers.GetAirDodgeStaminaCost();
          staminaReductionMultiplier = scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.executionOwner.GetEntityID()), gamedataStatType.DodgeStaminaCostReduction);
          if staminaReductionMultiplier > 0.00 {
            staminaReduction *= 1.00 - staminaReductionMultiplier;
          };
        };
        break;
      default:
        staminaReduction = 0.10;
    };
    if staminaReduction > 0.00 {
      PlayerStaminaHelpers.ModifyStamina(scriptInterface.executionOwner as PlayerPuppet, -staminaReduction);
    };
  }

  protected final func RemoveOpticalCamoEffect(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, currentState: Int32, opt timeInState: Float) -> Void {
    let state: Int32;
    let time: Float;
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.OpticalCamoCoolPerkShort") {
      state = scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LocomotionDetailed);
      if state == currentState {
        time = this.GetInStateTime();
        if timeInState != 0.00 {
          if time > timeInState {
            StatusEffectHelper.RemoveStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.OpticalCamoCoolPerkShort");
          };
        } else {
          if time > 1.35 {
            StatusEffectHelper.RemoveStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.OpticalCamoCoolPerkShort");
          };
        };
      };
    };
  }

  protected final func UpdateInputToggles(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, minTimeBeforeCrouch: Float, dontToggleCrouchOffOnSprint: Bool, out toggledSprint: Bool, out toggledCrouch: Bool) -> Void {
    if scriptInterface.IsActionJustPressed(n"ToggleSprint") || scriptInterface.IsActionJustPressed(n"Sprint") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", true, true);
      if !dontToggleCrouchOffOnSprint {
        stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
      };
      toggledSprint = true;
      return;
    };
    if this.GetInStateTime() > minTimeBeforeCrouch && (scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustHeld(n"ToggleCrouch")) {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", true, true);
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      toggledCrouch = true;
      return;
    };
  }

  protected final func UpdateInputToggles(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, opt minTimeBeforeCrouch: Float, opt dontToggleCrouchOffOnSprint: Bool) -> Void {
    let toggledCrouch: Bool;
    let toggledSprint: Bool;
    this.UpdateInputToggles(stateContext, scriptInterface, minTimeBeforeCrouch, dontToggleCrouchOffOnSprint, toggledSprint, toggledCrouch);
  }
}

public abstract class LocomotionGroundDecisions extends LocomotionTransition {

  public const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return true;
  }

  public final static func CheckCrouchEnterCondition(const stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustHeld(n"ToggleCrouch") || stateContext.GetConditionBool(n"CrouchToggled") {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", true, true);
      return true;
    };
    return scriptInterface.GetActionValue(n"Crouch") > 0.00;
  }

  protected final const func CrouchEnterCondition(const stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, isFFByLine: Bool) -> Bool {
    let puppetPS: ref<ScriptedPuppetPS>;
    let test: Bool = test;
    let paramName: CName = isFFByLine ? n"FFhintActive" : n"FFHoldLock";
    let puppet: ref<ScriptedPuppet> = scriptInterface.owner as ScriptedPuppet;
    if IsDefined(puppet) {
      puppetPS = puppet.GetPuppetPS();
    };
    test = stateContext.GetConditionBool(n"CrouchToggled");
    test = !stateContext.GetBoolParameter(paramName, true);
    test = IsDefined(puppetPS) && puppetPS.IsCrouch();
    if (scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustHeld(n"ToggleCrouch") || stateContext.GetConditionBool(n"CrouchToggled")) && !stateContext.GetBoolParameter(paramName, true) || IsDefined(puppetPS) && puppetPS.IsCrouch() {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", true, true);
      return true;
    };
    return scriptInterface.GetActionValue(n"Crouch") > 0.00;
  }

  protected final const func CrouchExitCondition(const stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, isFFByLine: Bool) -> Bool {
    let paramName: CName = isFFByLine ? n"FFhintActive" : n"FFHoldLock";
    if (scriptInterface.IsActionJustReleased(n"Crouch") || scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustHeld(n"ToggleCrouch")) && !stateContext.GetBoolParameter(paramName, true) {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
      return true;
    };
    if !stateContext.GetConditionBool(n"CrouchToggled") && scriptInterface.GetActionValue(n"Crouch") == 0.00 {
      return true;
    };
    return false;
  }
}

public abstract class LocomotionGroundEvents extends LocomotionEventsTransition {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let animFeature: ref<AnimFeature_PlayerLocomotionStateMachine>;
    super.OnEnter(stateContext, scriptInterface);
    stateContext.RemovePermanentBoolParameter(n"enteredFallFromAirDodge");
    stateContext.SetPermanentIntParameter(n"currentNumberOfJumps", 0, true);
    stateContext.SetPermanentIntParameter(n"currentNumberOfAirDodges", 0, true);
    this.SetAudioParameter(n"RTPC_Vertical_Velocity", 0.00, scriptInterface);
    animFeature = new AnimFeature_PlayerLocomotionStateMachine();
    animFeature.inAirState = false;
    scriptInterface.SetAnimationParameterFeature(n"LocomotionStateMachine", animFeature);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, 0);
    scriptInterface.GetAudioSystem().NotifyGameTone(n"EnterOnGround");
    this.StopEffect(scriptInterface, n"falling");
    stateContext.SetConditionBoolParameter(n"JumpPressed", false, true);
    stateContext.SetPermanentBoolParameter(n"disableAirDash", false, true);
    stateContext.SetPermanentBoolParameter(n"isGravityAffectedByChargedJump", false, true);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    scriptInterface.GetAudioSystem().NotifyGameTone(n"LeaveOnGround");
  }
}

public class ForceIdleDecisions extends LocomotionGroundDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsIdleForced(stateContext) || scriptInterface.IsSceneAnimationActive();
  }

  protected final const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let mountingInfo: MountingInfo = scriptInterface.GetMountingInfo(scriptInterface.executionOwner);
    return !this.IsIdleForced(stateContext) && !scriptInterface.IsSceneAnimationActive() && !IMountingFacility.InfoIsComplete(mountingInfo) && !scriptInterface.localBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsControllingDevice);
  }
}

public class ForceIdleEvents extends LocomotionGroundEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().EnableQueriesForOwner(scriptInterface.owner, gamePlayerObstacleSystemQueryType.AverageNormal);
    scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().DisableQueriesForOwner(scriptInterface.owner, gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
    scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().EnableQueriesForOwner(scriptInterface.owner, gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
  }
}

public class WorkspotDecisions extends LocomotionGroundDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return DefaultTransition.IsInWorkspot(scriptInterface);
  }

  protected final const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !DefaultTransition.IsInWorkspot(scriptInterface) || this.IsInMinigame(scriptInterface) {
      return true;
    };
    return false;
  }

  protected final const func ToCrouch(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return true;
  }

  protected final const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return true;
  }

  protected final const func ToSlide(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let forceSlide: StateResultBool = stateContext.GetTemporaryBoolParameter(n"forceSlide");
    return forceSlide.value;
  }
}

public class WorkspotEvents extends LocomotionGroundEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if DefaultTransition.GetPlayerPuppet(scriptInterface).HasWorkspotTag(n"DisableCameraControl") {
      this.SetWorkspotAnimFeature(scriptInterface);
    };
    if DefaultTransition.GetPlayerPuppet(scriptInterface).HasWorkspotTag(n"Grab") {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
    };
    if !DefaultTransition.GetPlayerPuppet(scriptInterface).PlayerContainsWorkspotTag(n"FinisherWorkspot") {
      stateContext.SetTemporaryBoolParameter(n"requestSandevistanDeactivation", true, true);
      stateContext.SetTemporaryBoolParameter(n"requestKerenzikovDeactivation", true, true);
    };
    super.OnEnter(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsInWorkspot, 1);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
    scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().DisableQueriesForOwner(scriptInterface.owner, gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 9);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsInWorkspot, 0);
    this.ResetWorkspotAnimFeature(scriptInterface);
    this.ResetParameters(stateContext, scriptInterface);
    scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().EnableQueriesForOwner(scriptInterface.owner, gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.IsInWorkspot, 0);
    this.ResetWorkspotAnimFeature(scriptInterface);
    this.ResetParameters(stateContext, scriptInterface);
  }

  protected final func SetWorkspotAnimFeature(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let animFeature: ref<AnimFeature_AerialTakedown> = new AnimFeature_AerialTakedown();
    animFeature.state = 1;
    scriptInterface.SetAnimationParameterFeature(n"AerialTakedown", animFeature);
  }

  protected final func ResetWorkspotAnimFeature(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let animFeature: ref<AnimFeature_AerialTakedown> = new AnimFeature_AerialTakedown();
    animFeature.state = 0;
    scriptInterface.SetAnimationParameterFeature(n"AerialTakedown", animFeature);
  }

  protected final func ResetParameters(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    scriptInterface.localBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.MeleeLeap, false);
    stateContext.SetPermanentBoolParameter(n"enableVaultFromleapAttack", false, true);
  }
}

public class ForceWalkDecisions extends LocomotionGroundDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsWalkForced(stateContext);
  }

  protected final const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return !this.IsWalkForced(stateContext);
  }
}

public class ForceWalkEvents extends LocomotionGroundEvents {

  public let m_storedSpeedValue: Float;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
    this.ProcessPermanentBoolParameterToggle(n"WalkToggled", false, stateContext);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
  }
}

public class ForceFreezeDecisions extends LocomotionGroundDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsFreezeForced(stateContext);
  }

  protected final const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return !this.IsFreezeForced(stateContext);
  }

  protected final const func ToWorkspot(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return !this.IsInMinigame(scriptInterface) && DefaultTransition.IsInWorkspot(scriptInterface);
  }
}

public class ForceFreezeEvents extends LocomotionGroundEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
    scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().DisableQueriesForOwner(scriptInterface.owner, gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().EnableQueriesForOwner(scriptInterface.owner, gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
  }
}

public class CoolExitJumpDecisions extends LocomotionAirDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let coolExitJumpRequested: StateResultBool = stateContext.GetTemporaryBoolParameter(n"requestCoolExitJump");
    return coolExitJumpRequested.value;
  }
}

public class CoolExitJumpEvents extends LocomotionAirEvents {

  protected func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let coolExitImpulseLevelResult: StateResultInt;
    let exitMomentum: StateResultVector;
    let legCW: ItemID;
    let impulse: Vector4 = new Vector4(0.00, 0.00, 0.00, 0.00);
    let coolExitImpulseLevel: vehicleCoolExitImpulseLevel = vehicleCoolExitImpulseLevel.NoImpulse;
    super.OnEnter(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 5);
    StatusEffectHelper.ApplyStatusEffect(scriptInterface.owner, t"BaseStatusEffect.VehicleExitKnockdownProtection", scriptInterface.executionOwnerEntityID);
    coolExitImpulseLevelResult = stateContext.GetTemporaryIntParameter(n"coolExitMagnitude");
    if coolExitImpulseLevelResult.valid {
      coolExitImpulseLevel = IntEnum<vehicleCoolExitImpulseLevel>(coolExitImpulseLevelResult.value);
    };
    this.BlockAimingForTime(stateContext, scriptInterface, 0.33);
    if Equals(coolExitImpulseLevel, vehicleCoolExitImpulseLevel.NoImpulse) || Equals(coolExitImpulseLevel, vehicleCoolExitImpulseLevel.NoExit) {
      return;
    };
    exitMomentum = stateContext.GetTemporaryVectorParameter(n"exitMomentum");
    if exitMomentum.valid {
      impulse += exitMomentum.value;
    };
    if Equals(coolExitImpulseLevel, vehicleCoolExitImpulseLevel.MaxImpulse) {
      legCW = EquipmentSystem.GetData(scriptInterface.executionOwner).GetActiveItem(gamedataEquipmentArea.LegsCW);
      if ItemID.IsOfTDBID(legCW, t"Items.ReinforcedMusclesRare") {
        impulse.Z += this.GetStaticFloatParameterDefault("chargeJumpHeight", 10.00);
      } else {
        impulse.Z += this.GetStaticFloatParameterDefault("defaultJumpHeight", 7.00);
      };
    } else {
      if Equals(coolExitImpulseLevel, vehicleCoolExitImpulseLevel.LowImpulse) {
        impulse.Z = this.GetStaticFloatParameterDefault("lowImpulseJumpHeight", 2.50);
      };
    };
    scriptInterface.PushAnimationEvent(n"Jump");
    stateContext.SetTemporaryVectorParameter(n"impulse", impulse, true);
  }
}

public class InitialDecisions extends LocomotionGroundDecisions {

  protected final const func ToCrouch(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return true;
  }
}

public class StandDecisions extends LocomotionGroundDecisions {

  public const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsTouchingGround(scriptInterface) && this.GetVerticalSpeed(scriptInterface) <= 0.50;
  }

  protected final const func ToSlide(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsTouchingGround(scriptInterface) && StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"JustDodgedLocomotion");
  }
}

public class StandEvents extends LocomotionGroundEvents {

  private let m_enteredAfterSprintWithNoInput: Bool;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let sprintToggledOffWithNoInput: StateResultBool;
    scriptInterface.PushAnimationEvent(n"StandEnter");
    stateContext.SetConditionBoolParameter(n"blockEnteringSlide", false, true);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 0);
    this.PlaySound(n"lcm_falling_wind_loop_end", scriptInterface);
    super.OnEnter(stateContext, scriptInterface);
    if stateContext.GetBoolParameter(n"WalkToggled", true) && !stateContext.GetBoolParameter(n"ForceDisableToggleWalk", true) {
      this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Crouch);
      this.SetModifierGroupForState(scriptInterface, "PlayerLocomotion.player_locomotion_data_Crouch");
    } else {
      this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
    };
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"ForceStand") {
      if StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"KeepState") {
        stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
      };
    };
    sprintToggledOffWithNoInput = stateContext.GetTemporaryBoolParameter(n"SprintToggledOffWithNoInput");
    this.m_enteredAfterSprintWithNoInput = sprintToggledOffWithNoInput.valid && sprintToggledOffWithNoInput.value;
  }

  protected final func OnTick(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let footstepStimuliSpeedThreshold: Float;
    let playerSpeed: Float;
    if this.IsTouchingGround(scriptInterface) {
      playerSpeed = scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed);
      footstepStimuliSpeedThreshold = this.GetStaticFloatParameterDefault("footstepStimuliSpeedThreshold", 2.50);
      if playerSpeed > footstepStimuliSpeedThreshold {
        this.BroadcastStimuliFootstepRegular(scriptInterface);
      };
    };
    if stateContext.GetBoolParameter(n"ForceDisableToggleWalk", true) {
      this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
      this.SetModifierGroupForState(scriptInterface, "PlayerLocomotion.player_locomotion_data_Stand");
      this.ProcessPermanentBoolParameterToggle(n"WalkToggled", false, stateContext);
      stateContext.RemovePermanentBoolParameter(n"ForceDisableToggleWalk");
      stateContext.RemovePermanentBoolParameter(n"ToggleWalkInputRegistered");
    } else {
      if stateContext.GetBoolParameter(n"ToggleWalkInputRegistered", true) {
        if stateContext.GetBoolParameter(n"WalkToggled", true) {
          this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
          this.SetModifierGroupForState(scriptInterface, "PlayerLocomotion.player_locomotion_data_Stand");
          this.ProcessPermanentBoolParameterToggle(n"WalkToggled", false, stateContext);
        } else {
          this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Crouch);
          this.SetModifierGroupForState(scriptInterface, "PlayerLocomotion.player_locomotion_data_Crouch");
          this.ProcessPermanentBoolParameterToggle(n"WalkToggled", true, stateContext);
        };
        stateContext.RemovePermanentBoolParameter(n"ToggleWalkInputRegistered");
      };
    };
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    this.RemoveOpticalCamoEffect(stateContext, scriptInterface, 1, 0.80);
  }

  public final func OnExitToDodge(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExit(stateContext, scriptInterface);
    if this.m_enteredAfterSprintWithNoInput && scriptInterface.IsActionJustPressed(n"DodgeForward") && this.GetInStateTime() <= 0.30 {
      stateContext.SetConditionBoolParameter(n"SprintToggled", true, true);
    };
  }
}

public class AimWalkDecisions extends LocomotionGroundDecisions {

  public let m_callbackIDs: [ref<CallbackHandle>];

  private let m_isBlocking: Bool;

  private let m_isAiming: Bool;

  private let m_inFocusMode: Bool;

  private let m_isLeftHandChanging: Bool;

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let allBlackboardDef: ref<AllBlackboardDefinitions>;
    super.OnAttach(stateContext, scriptInterface);
    if IsDefined(scriptInterface.localBlackboard) {
      allBlackboardDef = GetAllBlackboardDefs();
      ArrayPush(this.m_callbackIDs, scriptInterface.localBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.Melee, this, n"OnMeleeChanged", true));
      ArrayPush(this.m_callbackIDs, scriptInterface.localBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.UpperBody, this, n"OnUpperBodyChanged", true));
      ArrayPush(this.m_callbackIDs, scriptInterface.localBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.Vision, this, n"OnVisionChanged", true));
      ArrayPush(this.m_callbackIDs, scriptInterface.localBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.LeftHandCyberware, this, n"OnLeftHandCyberwareChanged", true));
      this.m_isBlocking = scriptInterface.localBlackboard.GetInt(allBlackboardDef.PlayerStateMachine.Melee) == 2;
      this.m_isAiming = scriptInterface.localBlackboard.GetInt(allBlackboardDef.PlayerStateMachine.UpperBody) == 6;
      this.m_inFocusMode = scriptInterface.localBlackboard.GetInt(allBlackboardDef.PlayerStateMachine.Vision) == 1;
      this.m_isLeftHandChanging = scriptInterface.localBlackboard.GetInt(allBlackboardDef.PlayerStateMachine.LeftHandCyberware) == 5;
      this.UpdateEnterConditionEnabled();
    };
  }

  protected final func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    ArrayClear(this.m_callbackIDs);
  }

  protected final func UpdateEnterConditionEnabled() -> Void {
    this.EnableOnEnterCondition(this.m_isBlocking || this.m_isAiming || this.m_inFocusMode || this.m_isLeftHandChanging);
  }

  protected cb func OnMeleeChanged(value: Int32) -> Bool {
    this.m_isBlocking = value == 2;
    this.UpdateEnterConditionEnabled();
  }

  protected cb func OnUpperBodyChanged(value: Int32) -> Bool {
    this.m_isAiming = value == 6;
    this.UpdateEnterConditionEnabled();
  }

  protected cb func OnVisionChanged(value: Int32) -> Bool {
    this.m_inFocusMode = value == 1;
    this.UpdateEnterConditionEnabled();
  }

  protected cb func OnLeftHandCyberwareChanged(value: Int32) -> Bool {
    this.m_isLeftHandChanging = value == 5;
    this.UpdateEnterConditionEnabled();
  }

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.m_isBlocking && DefaultTransition.HasMeleeWeaponEquipped(scriptInterface) && scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.IsNotSlowedDuringBlock) > 0.00 {
      return false;
    };
    return true;
  }

  protected final const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return !this.IsOnEnterConditionEnabled() || !this.EnterCondition(stateContext, scriptInterface);
  }

  protected const func ToDodge(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let isDashPerkBought: Bool = PlayerDevelopmentSystem.GetData(scriptInterface.executionOwner).IsNewPerkBought(gamedataNewPerkType.Reflexes_Central_Milestone_2) == 2;
    if isDashPerkBought && this.WantsToDodge(stateContext, scriptInterface) {
      return true;
    };
    return false;
  }
}

public class AimWalkEvents extends LocomotionGroundEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.AimWalk);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    this.RemoveOpticalCamoEffect(stateContext, scriptInterface, 2, 0.80);
  }
}

public class CrouchDecisions extends LocomotionGroundDecisions {

  public let m_gameplaySettings: wref<GameplaySettingsSystem>;

  public let m_executionOwner: wref<GameObject>;

  public let m_callbackID: ref<CallbackHandle>;

  private let m_statusEffectListener: ref<DefaultTransitionStatusEffectListener>;

  private let m_crouchPressed: Bool;

  private let m_toggleCrouchPressed: Bool;

  private let m_forcedCrouch: Bool;

  private let m_controllingDevice: Bool;

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let allBlackboardDef: ref<AllBlackboardDefinitions>;
    this.m_gameplaySettings = GameplaySettingsSystem.GetGameplaySettingsSystemInstance(scriptInterface.executionOwner);
    if IsDefined(scriptInterface.localBlackboard) {
      allBlackboardDef = GetAllBlackboardDefs();
      this.m_callbackID = scriptInterface.localBlackboard.RegisterListenerBool(allBlackboardDef.PlayerStateMachine.IsControllingDevice, this, n"OnControllingDeviceChange");
      this.OnControllingDeviceChange(scriptInterface.localBlackboard.GetBool(allBlackboardDef.PlayerStateMachine.IsControllingDevice));
    };
    scriptInterface.executionOwner.RegisterInputListener(this, n"Crouch");
    scriptInterface.executionOwner.RegisterInputListener(this, n"ToggleCrouch");
    this.m_crouchPressed = scriptInterface.GetActionValue(n"Crouch") > 0.00;
    if scriptInterface.GetActionValue(n"ToggleCrouch") > 0.00 {
      this.m_toggleCrouchPressed = true;
    };
    this.m_statusEffectListener = new DefaultTransitionStatusEffectListener();
    this.m_statusEffectListener.m_transitionOwner = this;
    scriptInterface.GetStatusEffectSystem().RegisterListener(scriptInterface.owner.GetEntityID(), this.m_statusEffectListener);
    this.m_executionOwner = scriptInterface.executionOwner;
    this.m_forcedCrouch = StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_executionOwner, n"ForceCrouch");
  }

  protected final func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_callbackID = null;
    this.m_statusEffectListener = null;
    scriptInterface.executionOwner.UnregisterInputListener(this);
  }

  public func OnStatusEffectApplied(statusEffect: wref<StatusEffect_Record>) -> Void {
    if !this.m_forcedCrouch {
      if statusEffect.GameplayTagsContains(n"ForceCrouch") {
        this.m_forcedCrouch = true;
        this.EnableOnEnterCondition(true);
      };
    };
  }

  public func OnStatusEffectRemoved(statusEffect: wref<StatusEffect_Record>) -> Void {
    if this.m_forcedCrouch {
      if statusEffect.GameplayTagsContains(n"ForceCrouch") {
        this.m_forcedCrouch = StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_executionOwner, n"ForceCrouch");
      };
    };
  }

  protected cb func OnAction(action: ListenerAction, consumer: ListenerActionConsumer) -> Bool {
    if Equals(ListenerAction.GetName(action), n"Crouch") {
      this.m_crouchPressed = ListenerAction.GetValue(action) > 0.00;
      if this.m_crouchPressed && !StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_executionOwner, n"ForceStand") && !StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_executionOwner, n"KeepState") {
        this.EnableOnEnterCondition(true);
      };
    } else {
      if Equals(ListenerAction.GetName(action), n"ToggleCrouch") {
        this.m_toggleCrouchPressed = ListenerAction.GetValue(action) > 0.00;
        if this.m_toggleCrouchPressed && !StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_executionOwner, n"ForceStand") && !StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_executionOwner, n"KeepState") {
          this.EnableOnEnterCondition(true);
        };
      };
    };
  }

  protected cb func OnControllingDeviceChange(value: Bool) -> Bool {
    this.m_controllingDevice = value;
  }

  protected const func EnterCondition(stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let isFFByLine: Bool;
    let shouldCrouch: Bool;
    let superResult: Bool;
    if this.m_controllingDevice {
      return true;
    };
    isFFByLine = this.m_gameplaySettings.GetIsFastForwardByLine();
    if isFFByLine && !StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"ForceStand") {
      if !scriptInterface.HasStatFlag(gamedataStatType.CanCrouch) || stateContext.GetBoolParameter(n"FFhintActive", true) || !scriptInterface.HasStatFlag(gamedataStatType.FFInputLock) {
        if scriptInterface.IsActionJustPressed(n"Crouch") || scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustHeld(n"ToggleCrouch") || stateContext.GetConditionBool(n"CrouchToggled") {
          stateContext.SetConditionBoolParameter(n"CrouchToggled", true, true);
          stateContext.SetPermanentBoolParameter(n"HoldInputFastForwardLock", true, true);
          return true;
        };
        return false;
      };
    };
    superResult = super.EnterCondition(stateContext, scriptInterface) && scriptInterface.HasStatFlag(gamedataStatType.CanCrouch);
    shouldCrouch = this.CrouchEnterCondition(stateContext, scriptInterface, isFFByLine) || this.m_forcedCrouch;
    if !this.m_crouchPressed && !this.m_toggleCrouchPressed && !this.m_forcedCrouch && !stateContext.GetConditionBool(n"CrouchToggled") {
      this.EnableOnEnterCondition(false);
    };
    return shouldCrouch && superResult && this.IsTouchingGround(scriptInterface);
  }

  protected const func ToCrouch(stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
    stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
    return true;
  }

  protected const func ToStand(stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let isFFByLine: Bool;
    if this.m_controllingDevice {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"ForceStand") {
      if StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"KeepState") {
        stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
      };
      return true;
    };
    isFFByLine = this.m_gameplaySettings.GetIsFastForwardByLine();
    if isFFByLine {
      if StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"FastForwardCrouchLock") || stateContext.GetBoolParameter(n"FFhintActive", true) || !scriptInterface.HasStatFlag(gamedataStatType.FFInputLock) {
        if scriptInterface.IsActionJustReleased(n"Crouch") || scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustHeld(n"ToggleCrouch") {
          stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
          stateContext.SetPermanentBoolParameter(n"HoldInputFastForwardLock", true, true);
          return true;
        };
        return false;
      };
      if !scriptInterface.HasStatFlag(gamedataStatType.CanCrouch) && !stateContext.GetBoolParameter(n"FFhintActive", true) {
        return true;
      };
    } else {
      if !scriptInterface.HasStatFlag(gamedataStatType.CanCrouch) && !StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"FastForward") && !stateContext.GetBoolParameter(n"FFRestriction", true) && !stateContext.GetBoolParameter(n"TriggerFF", true) {
        return true;
      };
    };
    if this.WantsToDodgeFromCrouch(stateContext, scriptInterface) {
      return false;
    };
    if this.CrouchExitCondition(stateContext, scriptInterface, isFFByLine) && !this.m_forcedCrouch {
      return true;
    };
    return false;
  }

  protected const func ToSprint(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let visionToggled: StateResultBool = stateContext.GetPermanentBoolParameter(n"VisionToggled");
    if PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Cool_Central_Milestone_3) == 3 {
      return false;
    };
    if this.m_controllingDevice {
      return false;
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.CanSprint) {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HealFood3") {
      return false;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().CoverAction.coverActionStateId) == 3 {
      return false;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == 1 || visionToggled.valid && visionToggled.value {
      return false;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LeftHandCyberware) == 5 {
      return false;
    };
    if scriptInterface.GetActionValue(n"AttackA") > 0.00 {
      return false;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == 6 {
      return false;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Melee) == 2 || this.IsInMeleeState(stateContext, n"meleeChargedHold") {
      return false;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon) == 7 && !stateContext.GetBoolParameter(n"canSprintWhileCharging", true) {
      return false;
    };
    if !stateContext.GetConditionBool(n"SprintToggled") && scriptInterface.IsActionJustReleased(n"ToggleSprint") && !StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"NoSlide") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", true, true);
    };
    if scriptInterface.GetActionValue(n"Crouch") == 0.00 && scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) >= 1.00 && !StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"NoSlide") && (scriptInterface.GetActionValue(n"Sprint") > 0.00 && (!stateContext.GetBoolParameter(n"sprintInputLock", true) || stateContext.GetConditionBool(n"SprintHoldCanStartWithoutNewInput")) || scriptInterface.GetActionValue(n"ToggleSprint") > 0.00 || stateContext.GetConditionBool(n"SprintToggled")) {
      return true;
    };
    return false;
  }

  protected const func ToCrouchSprint(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let visionToggled: StateResultBool = stateContext.GetPermanentBoolParameter(n"VisionToggled");
    if PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Cool_Central_Milestone_3) != 3 {
      return false;
    };
    if this.GetInStateTime() < this.GetStaticFloatParameterDefault("minTimeToCrouchSprint", 0.40) {
      return false;
    };
    if this.m_controllingDevice {
      return false;
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.CanSprint) {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HealFood3") {
      return false;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().CoverAction.coverActionStateId) == 3 {
      return false;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == 1 || visionToggled.valid && visionToggled.value {
      return false;
    };
    if !stateContext.GetConditionBool(n"SprintToggled") && scriptInterface.IsActionJustReleased(n"ToggleSprint") && !StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"NoSlide") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", true, true);
    };
    if scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) >= 1.00 && !StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"NoSlide") && (scriptInterface.GetActionValue(n"Sprint") > 0.00 && (!stateContext.GetBoolParameter(n"sprintInputLock", true) || stateContext.GetConditionBool(n"SprintHoldCanStartWithoutNewInput")) || scriptInterface.GetActionValue(n"ToggleSprint") > 0.00 || stateContext.GetConditionBool(n"SprintToggled")) {
      return true;
    };
    return false;
  }

  protected const func ToDodge(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.WantsToDodgeFromCrouch(stateContext, scriptInterface);
  }

  protected final const func WantsToDodgeFromCrouch(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let isPlayerAiming: Bool;
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HealFood3") {
      return false;
    };
    isPlayerAiming = this.IsInUpperBodyState(stateContext, n"aimingState");
    if (!isPlayerAiming || scriptInterface.HasStatFlag(gamedataStatType.CanAimWhileDodging)) && this.WantsToDodge(stateContext, scriptInterface) {
      return true;
    };
    return false;
  }
}

public class CrouchEvents extends LocomotionGroundEvents {

  protected func CancelSprintOnHoldWithoutNinjutsu() -> Bool {
    return true;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let puppet: ref<ScriptedPuppet> = scriptInterface.owner as ScriptedPuppet;
    if IsDefined(puppet) {
      puppet.GetPuppetPS().SetCrouch(true);
    };
    super.OnEnter(stateContext, scriptInterface);
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"NoSlide") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
    };
    if this.CancelSprintOnHoldWithoutNinjutsu() && scriptInterface.GetActionValue(n"Crouch") > 0.00 && PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Cool_Central_Milestone_3) != 3 {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
    };
    scriptInterface.GetAudioSystem().NotifyGameTone(n"EnterCrouch");
    scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().OnEnterCrouch(scriptInterface.owner);
    scriptInterface.SetAnimationParameterFloat(n"crouch", 1.00);
    if DefaultTransition.HasMeleeWeaponEquipped(scriptInterface) {
      scriptInterface.GetTargetingSystem().AimSnap(scriptInterface.owner);
    };
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 1);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Crouch);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    this.RemoveOpticalCamoEffect(stateContext, scriptInterface, 3, 0.80);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let puppet: ref<ScriptedPuppet> = scriptInterface.owner as ScriptedPuppet;
    if IsDefined(puppet) {
      puppet.GetPuppetPS().SetCrouch(false);
    };
    super.OnExit(stateContext, scriptInterface);
    scriptInterface.GetAudioSystem().NotifyGameTone(n"LeaveCrouch");
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 0);
    scriptInterface.SetAnimationParameterFloat(n"crouch", 0.00);
    if DefaultTransition.HasMeleeWeaponEquipped(scriptInterface) {
      scriptInterface.GetTargetingSystem().AimSnap(scriptInterface.owner);
    };
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    this.OnExit(stateContext, scriptInterface);
  }
}

public class CrouchSprintDecisions extends CrouchDecisions {

  protected const func ToCrouch(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let minLinearVelocityThreshold: Float;
    let minStickInputThreshold: Float;
    if !this.IsTouchingGround(scriptInterface) {
      return false;
    };
    if this.GetInStateTime() < this.GetStaticFloatParameterDefault("minTimeToExit", 1.00) {
      return false;
    };
    if scriptInterface.IsActionJustPressed(n"Crouch") || scriptInterface.IsActionJustPressed(n"Jump") || scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustHeld(n"ToggleCrouch") {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
      return true;
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.CanSprint) || scriptInterface.IsActionJustReleased(n"Sprint") || scriptInterface.IsActionJustPressed(n"ToggleSprint") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return true;
    };
    if stateContext.GetBoolParameter(n"WalkToggled", true) {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return true;
    };
    if this.GetInStateTime() >= 0.60 {
      minLinearVelocityThreshold = this.GetStaticFloatParameterDefault("minLinearVelocityThreshold", 0.50);
      if scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) < minLinearVelocityThreshold {
        stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
        stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
        return true;
      };
    };
    if !scriptInterface.IsMoveInputConsiderable() {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return true;
    };
    minStickInputThreshold = this.GetStaticFloatParameterDefault("minStickInputThreshold", 0.80);
    if stateContext.GetConditionBool(n"SprintToggled") && DefaultTransition.GetMovementInputActionValue(stateContext, scriptInterface) <= minStickInputThreshold {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      return true;
    };
    if (scriptInterface.executionOwner as PlayerPuppet).GetStaminaValueUnsafe() <= 1.00 {
      StatusEffectHelper.RemoveStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.OpticalCamoCoolPerkShort");
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return true;
    };
    if scriptInterface.IsActionJustReleased(n"Sprint") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return true;
    };
    return false;
  }
}

public class CrouchSprintEvents extends CrouchEvents {

  protected func CancelSprintOnHoldWithoutNinjutsu() -> Bool {
    return false;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    stateContext.SetConditionBoolParameter(n"SprintToggled", true, true);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 1);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.CrouchSprint);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    this.ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
  }
}

public class SprintDecisions extends LocomotionGroundDecisions {

  private let m_sprintPressed: Bool;

  private let m_toggleSprintPressed: Bool;

  private let m_dodgeForwardPressed: Bool;

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnAttach(stateContext, scriptInterface);
    scriptInterface.executionOwner.RegisterInputListener(this, n"Sprint");
    scriptInterface.executionOwner.RegisterInputListener(this, n"ToggleSprint");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeForward");
    this.m_sprintPressed = scriptInterface.GetActionValue(n"Sprint") > 0.00;
    this.m_toggleSprintPressed = scriptInterface.GetActionValue(n"ToggleSprint") > 0.00;
    this.m_dodgeForwardPressed = scriptInterface.GetActionValue(n"DodgeForward") > 0.00;
  }

  protected final func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    scriptInterface.executionOwner.UnregisterInputListener(this);
  }

  protected cb func OnAction(action: ListenerAction, consumer: ListenerActionConsumer) -> Bool {
    if Equals(ListenerAction.GetName(action), n"Sprint") {
      this.m_sprintPressed = ListenerAction.GetValue(action) > 0.00;
      if this.m_sprintPressed {
        this.EnableOnEnterCondition(true);
      };
    } else {
      if Equals(ListenerAction.GetName(action), n"ToggleSprint") {
        this.m_toggleSprintPressed = ListenerAction.GetValue(action) > 0.00;
        if this.m_toggleSprintPressed {
          this.EnableOnEnterCondition(true);
        };
      } else {
        if Equals(ListenerAction.GetName(action), n"DodgeForward") {
          this.m_dodgeForwardPressed = ListenerAction.IsButtonJustPressed(action);
          if this.m_dodgeForwardPressed {
            this.EnableOnEnterCondition(true);
          };
        };
      };
    };
  }

  private final static func IsWreckingBallAllowed(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Body_Right_Milestone_2) >= 2 && WeaponObject.IsBlunt(GameObject.GetActiveWeapon(scriptInterface.executionOwner).GetItemID()) && scriptInterface.GetStatPoolsSystem().GetStatPoolValue(Cast<StatsObjectID>(scriptInterface.executionOwner.GetEntityID()), gamedataStatPoolType.Stamina, true) > 0.00;
  }

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let enterAngleThreshold: Float;
    let isAiming: Bool;
    let isChargingCyberware: Bool;
    let lastShotTime: StateResultFloat;
    let linearVelocity: Float;
    let minLinearVelocityThreshold: Float;
    let minStickInputThreshold: Float;
    let superResult: Bool;
    let timeSinceLastShot: Float;
    if !this.m_sprintPressed && !this.m_toggleSprintPressed && !stateContext.GetConditionBool(n"SprintToggled") {
      this.EnableOnEnterCondition(false);
      return false;
    };
    superResult = super.EnterCondition(stateContext, scriptInterface) && this.IsTouchingGround(scriptInterface);
    minLinearVelocityThreshold = this.GetStaticFloatParameterDefault("minLinearVelocityThreshold", 0.50);
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"JustDodgedLocomotion") {
      minLinearVelocityThreshold = this.GetStaticFloatParameterDefault("minLinearVelocityAfterDodgeThreshold", 0.50);
    };
    minStickInputThreshold = this.GetStaticFloatParameterDefault("minStickInputThreshold", 0.90);
    enterAngleThreshold = this.GetStaticFloatParameterDefault("enterAngleThreshold", -180.00);
    if !scriptInterface.HasStatFlag(gamedataStatType.CanSprint) {
      return false;
    };
    if !scriptInterface.IsMoveInputConsiderable() || AbsF(scriptInterface.GetInputHeading()) > enterAngleThreshold || DefaultTransition.GetMovementInputActionValue(stateContext, scriptInterface) <= minStickInputThreshold {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return false;
    };
    linearVelocity = scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed);
    if linearVelocity < minLinearVelocityThreshold {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return false;
    };
    if !SprintDecisions.IsWreckingBallAllowed(scriptInterface) {
      isAiming = scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.UpperBody) == 6;
      if isAiming {
        stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
        stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
        return false;
      };
    };
    isChargingCyberware = scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LeftHandCyberware) == 5;
    if isChargingCyberware {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return false;
    };
    if !MeleeTransition.MeleeSprintStateCondition(stateContext, scriptInterface) {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return false;
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.CanWeaponShootWhileSprinting) {
      lastShotTime = stateContext.GetPermanentFloatParameter(n"LastShotTime");
      if lastShotTime.valid {
        timeSinceLastShot = EngineTime.ToFloat(GameInstance.GetSimTime(scriptInterface.GetGame())) - lastShotTime.value;
        if timeSinceLastShot < this.GetStaticFloatParameterDefault("sprintDisableTimeAfterShoot", -2.00) {
          return false;
        };
      };
      if scriptInterface.GetActionValue(n"RangedAttack") > 0.00 {
        stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
        stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
        return false;
      };
    };
    if stateContext.GetConditionBool(n"OnInterruptSprintFail_BlockSprintStartOnce") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      stateContext.SetConditionBoolParameter(n"OnInterruptSprintFail_BlockSprintStartOnce", false, true);
      return false;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().CoverAction.coverActionStateId) == 3 {
      return false;
    };
    if this.m_toggleSprintPressed && !stateContext.GetBoolParameter(n"sprintInputLock", true) {
      stateContext.SetConditionBoolParameter(n"SprintToggled", true, true);
    };
    if !superResult {
      return false;
    };
    if stateContext.GetConditionBool(n"SprintToggled") && !stateContext.GetBoolParameter(n"sprintInputLock", true) {
      return true;
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.CanWeaponReloadWhileSprinting) && scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Weapon) == 2 {
      if scriptInterface.IsActionJustPressed(n"Sprint") && !stateContext.GetBoolParameter(n"sprintInputLock", true) {
        return true;
      };
    } else {
      if this.m_sprintPressed && (!stateContext.GetBoolParameter(n"sprintInputLock", true) || stateContext.GetConditionBool(n"SprintHoldCanStartWithoutNewInput")) {
        return true;
      };
    };
    if stateContext.IsStateActive(n"Melee", n"meleeBodySlamAttack") && (stateContext.IsStateActive(n"Locomotion", n"fall") || stateContext.IsStateActive(n"Locomotion", n"regularLand")) {
      return true;
    };
    return false;
  }

  protected const func ToStand(stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let enterAngleThreshold: Float;
    let minLinearVelocityThreshold: Float;
    let minStickInputThreshold: Float;
    if !scriptInterface.HasStatFlag(gamedataStatType.CanSprint) {
      return true;
    };
    if stateContext.GetBoolParameter(n"InterruptSprint") && !(stateContext.GetBoolParameter(n"InterruptSprintByAiming") && SprintDecisions.IsWreckingBallAllowed(scriptInterface)) {
      return true;
    };
    if stateContext.GetBoolParameter(n"WalkToggled", true) {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return true;
    };
    if this.GetInStateTime() >= 0.60 {
      minLinearVelocityThreshold = this.GetStaticFloatParameterDefault("minLinearVelocityThreshold", 0.50);
      if scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) < minLinearVelocityThreshold {
        stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
        stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
        return true;
      };
    };
    enterAngleThreshold = this.GetStaticFloatParameterDefault("enterAngleThreshold", 45.00);
    if !scriptInterface.IsMoveInputConsiderable() {
      if stateContext.GetConditionBool(n"SprintToggled") {
        stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
        stateContext.SetTemporaryBoolParameter(n"SprintToggledOffWithNoInput", true, true);
      };
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return true;
    };
    if AbsF(scriptInterface.GetInputHeading()) > enterAngleThreshold {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return true;
    };
    minStickInputThreshold = this.GetStaticFloatParameterDefault("minStickInputThreshold", 0.90);
    if DefaultTransition.GetMovementInputActionValue(stateContext, scriptInterface) <= minStickInputThreshold {
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      if stateContext.GetConditionBool(n"SprintToggled") {
        stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
        return true;
      };
    };
    if scriptInterface.IsActionJustReleased(n"Sprint") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return true;
    };
    return false;
  }
}

public class SprintEvents extends LocomotionGroundEvents {

  public let m_previousStimTimeStamp: Float;

  public let m_reloadModifier: ref<gameStatModifierData>;

  public let m_isInSecondSprint: Bool;

  public let m_sprintModifier: ref<gameStatModifierData>;

  public let m_sprintAnimBlocked: Bool;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_previousStimTimeStamp = -1.00;
    this.m_isInSecondSprint = false;
    this.m_sprintAnimBlocked = false;
    super.OnEnter(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 2);
    this.ProcessPermanentBoolParameterToggle(n"WalkToggled", false, stateContext);
    this.SetupSprintInputLock(stateContext, scriptInterface);
    stateContext.SetConditionBoolParameter(n"blockEnteringSlide", false, true);
    stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
    stateContext.SetTemporaryBoolParameter(n"CancelGrenadeAction", true, true);
    this.ForceDisableVisionModeWithInput(stateContext, scriptInterface);
    this.StartEffect(scriptInterface, n"locomotion_sprint");
    if !scriptInterface.HasStatFlag(gamedataStatType.CanWeaponReloadWhileSprinting) && scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Weapon) == 2 {
      stateContext.SetTemporaryBoolParameter(n"TryInterruptReload", true, true);
    };
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Sprint);
  }

  private final func ApplySprintAnimBlock(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let sprintAnimFeature: ref<AnimFeature_WeaponSprintBlock> = new AnimFeature_WeaponSprintBlock();
    sprintAnimFeature.active = this.m_sprintAnimBlocked;
    AnimationControllerComponent.ApplyFeatureToReplicate(scriptInterface.executionOwner, n"WeaponSprintBlock", sprintAnimFeature);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let blockSprintAnim: Bool;
    let isReloading: Bool;
    let isShooting: Bool;
    let lastShotTime: StateResultFloat;
    let timeSinceLastShot: Float;
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    this.ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    this.UpdateFootstepSprintStim(stateContext, scriptInterface);
    isReloading = scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Weapon) == 2;
    isShooting = scriptInterface.GetActionValue(n"RangedAttack") > 0.00;
    blockSprintAnim = false;
    if scriptInterface.HasStatFlag(gamedataStatType.CanWeaponShootWhileSprinting) {
      lastShotTime = stateContext.GetPermanentFloatParameter(n"LastShotTime");
      if lastShotTime.valid {
        timeSinceLastShot = EngineTime.ToFloat(GameInstance.GetSimTime(scriptInterface.GetGame())) - lastShotTime.value;
        blockSprintAnim = timeSinceLastShot < 2.00;
      };
    };
    if NotEquals(blockSprintAnim, this.m_sprintAnimBlocked) {
      this.m_sprintAnimBlocked = blockSprintAnim;
      this.ApplySprintAnimBlock(scriptInterface);
    };
    if (isReloading && !scriptInterface.HasStatFlag(gamedataStatType.CanWeaponReloadWhileSprinting) || isShooting && !scriptInterface.HasStatFlag(gamedataStatType.CanWeaponShootWhileSprinting)) && !IsDefined(this.m_reloadModifier) {
      AnimationControllerComponent.SetInputFloat(scriptInterface.executionOwner, n"sprint", 0.00);
      this.EnableReloadStatModifier(true, stateContext, scriptInterface);
    } else {
      if !(isReloading || isShooting) && IsDefined(this.m_reloadModifier) {
        AnimationControllerComponent.SetInputFloat(scriptInterface.executionOwner, n"sprint", 1.00);
        this.EnableReloadStatModifier(false, stateContext, scriptInterface);
      };
    };
    this.RemoveOpticalCamoEffect(stateContext, scriptInterface, 4, 0.50);
  }

  private final func EvaluateTwoStepSprint(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if this.GetStaticBoolParameterDefault("enableTwoStepSprint_EXPERIMENTAL", false) {
      if this.ShouldEnterSecondSprint(stateContext, scriptInterface) {
        this.m_isInSecondSprint = true;
        this.AddMaxSpeedModifier(stateContext, scriptInterface);
      } else {
        if this.m_isInSecondSprint && this.GetInStateTime() >= 0.50 && scriptInterface.IsActionJustPressed(n"ToggleSprint") {
          this.m_isInSecondSprint = false;
          this.RemoveMaxSpeedModifier(stateContext, scriptInterface);
        };
      };
    };
  }

  private final func AddMaxSpeedModifier(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let statSystem: ref<StatsSystem> = scriptInterface.GetStatsSystem();
    this.m_sprintModifier = RPGManager.CreateStatModifierUsingCurve(gamedataStatType.MaxSpeed, gameStatModifierType.Additive, gamedataStatType.Reflexes, n"locomotion_stats", n"max_speed_in_sprint");
    statSystem.AddModifier(Cast<StatsObjectID>(scriptInterface.ownerEntityID), this.m_sprintModifier);
  }

  private final func RemoveMaxSpeedModifier(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let statSystem: ref<StatsSystem> = scriptInterface.GetStatsSystem();
    if IsDefined(this.m_sprintModifier) {
      statSystem.RemoveModifier(Cast<StatsObjectID>(scriptInterface.ownerEntityID), this.m_sprintModifier);
      this.m_sprintModifier = null;
    };
  }

  private final func ShouldEnterSecondSprint(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return !this.m_isInSecondSprint && this.GetInStateTime() >= this.GetStaticFloatParameterDefault("minTimeToEnterTwoStepSprint", 0.00) && scriptInterface.IsActionJustPressed(n"ToggleSprint");
  }

  private final func CleanupTwoStepSprint(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_isInSecondSprint = false;
    this.RemoveMaxSpeedModifier(stateContext, scriptInterface);
  }

  protected final const func GetReloadModifier(const scriptInterface: ref<StateGameScriptInterface>) -> Float {
    let modifierStart: Float = this.GetStaticFloatParameterDefault("reloadModifierStart", -2.00);
    let modifierEnd: Float = this.GetStaticFloatParameterDefault("reloadModifierEnd", -2.00);
    let modifierRange: Float = modifierEnd - modifierStart;
    let statValue: Float = scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.Reflexes);
    let lerp: Float = (statValue - 1.00) / 19.00;
    let result: Float = modifierStart + lerp * modifierRange;
    return result;
  }

  protected func EnableReloadStatModifier(enable: Bool, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let reloadModifierAmount: Float;
    let ownerID: StatsObjectID = Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID);
    if enable && !IsDefined(this.m_reloadModifier) {
      reloadModifierAmount = this.GetReloadModifier(scriptInterface);
      this.m_reloadModifier = RPGManager.CreateStatModifier(gamedataStatType.MaxSpeed, gameStatModifierType.Additive, reloadModifierAmount);
      scriptInterface.GetStatsSystem().AddModifier(ownerID, this.m_reloadModifier);
    } else {
      if !enable && IsDefined(this.m_reloadModifier) {
        scriptInterface.GetStatsSystem().RemoveModifier(ownerID, this.m_reloadModifier);
        this.m_reloadModifier = null;
      };
    };
  }

  protected final func UpdateFootstepSprintStim(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if this.GetInStateTime() >= this.m_previousStimTimeStamp + 0.20 {
      this.m_previousStimTimeStamp = this.GetInStateTime();
      this.BroadcastStimuliFootstepSprint(scriptInterface);
    };
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.CleanupTwoStepSprint(stateContext, scriptInterface);
    this.EnableReloadStatModifier(false, stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 0);
    stateContext.SetPermanentFloatParameter(n"SprintingStoppedTimeStamp", scriptInterface.GetNow(), true);
    this.StopEffect(scriptInterface, n"locomotion_sprint");
    stateContext.SetPermanentBoolParameter(n"TemporarySpeedLimitIgnore", false, true);
    if stateContext.GetBoolParameter(n"InterruptSprint") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"OnInterruptSprintFail_BlockSprintStartOnce", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
    };
    this.m_sprintAnimBlocked = false;
    this.ApplySprintAnimBlock(scriptInterface);
    super.OnExit(stateContext, scriptInterface);
  }

  protected func OnExitToJump(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.CleanupTwoStepSprint(stateContext, scriptInterface);
    this.EnableReloadStatModifier(false, stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 0);
    stateContext.SetPermanentFloatParameter(n"SprintingStoppedTimeStamp", scriptInterface.GetNow(), true);
    this.StopEffect(scriptInterface, n"locomotion_sprint");
    this.OnExit(stateContext, scriptInterface);
  }

  protected func OnExitToChargeJump(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.CleanupTwoStepSprint(stateContext, scriptInterface);
    this.EnableReloadStatModifier(false, stateContext, scriptInterface);
    this.StopEffect(scriptInterface, n"locomotion_sprint");
    this.OnExit(stateContext, scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    this.CleanupTwoStepSprint(stateContext, scriptInterface);
    this.EnableReloadStatModifier(false, stateContext, scriptInterface);
    this.StopEffect(scriptInterface, n"locomotion_sprint");
    if stateContext.GetBoolParameter(n"InterruptSprint") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"OnInterruptSprintFail_BlockSprintStartOnce", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
    };
    this.m_sprintAnimBlocked = false;
    this.ApplySprintAnimBlock(scriptInterface);
  }
}

public class SlideFallDecisions extends LocomotionAirDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.ShouldFall(stateContext, scriptInterface);
  }

  protected final const func ToSlide(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.IsTouchingGround(scriptInterface) {
      return false;
    };
    return true;
  }

  protected final const func ToFall(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let fallingSpeedThreshold: Float;
    let height: Float;
    let verticalSpeed: Float;
    if AbsF(this.GetCameraYaw(stateContext, scriptInterface)) >= this.GetStaticFloatParameterDefault("maxCameraYawToExit", 95.00) {
      return true;
    };
    if this.GetStaticBoolParameterDefault("backInputExitsSlide", false) && scriptInterface.GetActionValue(n"MoveY") < -0.50 {
      return true;
    };
    if scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) <= this.GetStaticFloatParameterDefault("minSpeedToExit", 2.00) && NotEquals(stateContext.GetStateMachineCurrentState(n"TimeDilation"), n"kerenzikov") {
      return true;
    };
    height = this.GetStaticFloatParameterDefault("heightToEnterFall", 0.00);
    if height > 0.00 {
      fallingSpeedThreshold = this.GetFallingSpeedBasedOnHeight(scriptInterface, height);
      verticalSpeed = this.GetVerticalSpeed(scriptInterface);
      if verticalSpeed <= fallingSpeedThreshold {
        return true;
      };
    };
    return false;
  }
}

public class SlideFallEvents extends LocomotionAirEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.SlideFall);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 11);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    this.ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    this.RemoveOpticalCamoEffect(stateContext, scriptInterface, 6, 0.85);
  }
}

public class SlideDecisions extends CrouchDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let angle: Float;
    let currentSpeed: Float;
    let enterAngleThreshold: Float;
    let minSpeedToEnter: Float;
    let secureFootingResult: SecureFootingResult;
    let velocity: Vector4;
    let superResult: Bool = super.EnterCondition(stateContext, scriptInterface);
    if !superResult {
      return false;
    };
    if stateContext.GetConditionBool(n"blockEnteringSlide") {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"NoSlide") {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HealFood3") {
      return false;
    };
    velocity = DefaultTransition.GetLinearVelocity(scriptInterface);
    enterAngleThreshold = this.GetStaticFloatParameterDefault("enterAngleThreshold", 45.00);
    angle = Vector4.GetAngleBetween(scriptInterface.executionOwner.GetWorldForward(), velocity);
    if AbsF(angle) > enterAngleThreshold {
      return false;
    };
    currentSpeed = Vector4.Length2D(velocity);
    secureFootingResult = scriptInterface.HasSecureFooting();
    if Equals(secureFootingResult.type, moveSecureFootingFailureType.Slope) {
      return true;
    };
    minSpeedToEnter = this.GetStaticFloatParameterDefault("minSpeedToEnter", 4.50);
    if currentSpeed < minSpeedToEnter {
      return false;
    };
    if !scriptInterface.IsMoveInputConsiderable() {
      return false;
    };
    return true;
  }

  protected const func ToCrouch(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let angle: Float;
    let horizontalDirection: Vector4;
    let secureFootingResult: SecureFootingResult;
    let slidingUphill: Bool;
    let velocity: Vector4;
    if !this.IsTouchingGround(scriptInterface) {
      return false;
    };
    secureFootingResult = scriptInterface.HasSecureFooting();
    if !StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"JustVaultedLocomotion") && Equals(secureFootingResult.type, moveSecureFootingFailureType.Invalid) {
      horizontalDirection = secureFootingResult.slidingDirection;
      horizontalDirection.Z = 0.00;
      velocity = DefaultTransition.GetLinearVelocity(scriptInterface);
      angle = Vector4.GetAngleBetween(secureFootingResult.slidingDirection, horizontalDirection);
      slidingUphill = Vector4.Dot(secureFootingResult.slidingDirection, velocity) < 0.00;
      if slidingUphill && angle >= this.GetStaticFloatParameterDefault("minAngleToStopUphillSlide", 10.00) && angle < 89.50 {
        return true;
      };
    };
    if PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Cool_Central_Milestone_3) == 3 && this.GetInStateTime() >= this.GetStaticFloatParameterDefault("minTimeToExit", 1.00) && (scriptInterface.IsActionJustPressed(n"Sprint") || scriptInterface.IsActionJustPressed(n"ToggleSprint") || stateContext.GetConditionBool(n"SprintToggled")) {
      stateContext.SetConditionBoolParameter(n"SprintToggled", scriptInterface.GetActionValue(n"Sprint") == 0.00, true);
      return true;
    };
    if this.ShouldExit(stateContext, scriptInterface) {
      return scriptInterface.GetActionValue(n"Crouch") > 0.00 || stateContext.GetConditionBool(n"CrouchToggled") || scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) <= this.GetStaticFloatParameterDefault("minSpeedToExit", 3.00);
    };
    return false;
  }

  protected const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let forceSlide: StateResultBool;
    if !this.IsTouchingGround(scriptInterface) {
      return false;
    };
    if this.GetInStateTime() < this.GetStaticFloatParameterDefault("minTimeToExit", 1.00) {
      return this.ShouldExit(stateContext, scriptInterface);
    };
    forceSlide = stateContext.GetPermanentBoolParameter(n"forceSlide");
    if !stateContext.GetConditionBool(n"CrouchToggled") && scriptInterface.GetActionValue(n"Crouch") <= 0.00 && !forceSlide.value {
      return true;
    };
    if scriptInterface.IsActionJustReleased(n"Crouch") || scriptInterface.IsActionJustPressed(n"Sprint") || scriptInterface.IsActionJustPressed(n"ToggleSprint") || scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustHeld(n"ToggleCrouch") {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
      return true;
    };
    return this.ShouldExit(stateContext, scriptInterface);
  }

  protected const func ToDodge(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.WantsToDodgeFromCrouch(stateContext, scriptInterface) && this.HasValidDodgeAngle(stateContext, scriptInterface);
  }

  private final const func HasValidDodgeAngle(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let dodgeHeading: Float = stateContext.GetConditionFloat(n"DodgeDirection");
    let cameraYaw: Float = this.GetCameraYaw(stateContext, scriptInterface);
    let dodgeWithCamera: Float = AngleNormalize180(cameraYaw + dodgeHeading);
    return AbsF(dodgeWithCamera) < this.GetStaticFloatParameterDefault("maxCameraYawToExit", 95.00);
  }

  protected const func ShouldExit(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let isKerenzikovStateActive: Bool;
    if AbsF(this.GetCameraYaw(stateContext, scriptInterface)) >= this.GetStaticFloatParameterDefault("maxCameraYawToExit", 95.00) {
      return true;
    };
    if this.WantsToDodgeFromCrouch(stateContext, scriptInterface) && !this.HasValidDodgeAngle(stateContext, scriptInterface) {
      return true;
    };
    if this.GetInStateTime() < this.GetStaticFloatParameterDefault("minTimeToExit", 0.70) {
      return false;
    };
    isKerenzikovStateActive = Equals(stateContext.GetStateMachineCurrentState(n"TimeDilation"), n"kerenzikov");
    if this.GetStaticBoolParameterDefault("backInputExitsSlide", false) && scriptInterface.GetActionValue(n"MoveY") < -0.50 {
      return true;
    };
    if !isKerenzikovStateActive && scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) <= this.GetStaticFloatParameterDefault("minSpeedToExit", 3.00) {
      return true;
    };
    return false;
  }
}

public class SlideEvents extends CrouchEvents {

  public let m_rumblePlayed: Bool;

  public let m_enteredWithSprint: Bool;

  public let m_decelerating: Bool;

  public let m_perkDecelerationMultiplier: Float;

  public let m_addDecelerationModifier: ref<gameStatModifierData>;

  public let m_multiplyDecelerationModifier: ref<gameStatModifierData>;

  protected func CancelSprintOnHoldWithoutNinjutsu() -> Bool {
    return false;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    this.m_enteredWithSprint = stateContext.GetConditionBool(n"SprintToggled");
    stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
    stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
    this.m_decelerating = false;
    this.m_perkDecelerationMultiplier = TweakDBInterface.GetFloat(t"NewPerks.Reflexes_Central_Perk_1_1.decelerationMultiplier", 0.00);
    this.m_rumblePlayed = false;
    if this.GetStaticBoolParameterDefault("pushAnimEventOnEnter", false) {
      scriptInterface.PushAnimationEvent(n"Slide");
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.CanWeaponReloadWhileSliding) && scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Weapon) == 2 {
      stateContext.SetTemporaryBoolParameter(n"TryInterruptReload", true, true);
    };
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 10);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Slide);
    RPGManager.AwardExperienceFromLocomotion(scriptInterface.owner as PlayerPuppet, 3.00);
  }

  public final func OnEnterFromSprint(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.BroadcastStimuliFootstepSprint(scriptInterface);
    this.OnEnter(stateContext, scriptInterface);
  }

  public final func OnEnterFromWorkspot(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    stateContext.SetPermanentBoolParameter(n"forceSlide", true, true);
    this.OnEnter(stateContext, scriptInterface);
  }

  protected func AddDecelerationStatModifier(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, enable: Bool) -> Void {
    let ownerID: StatsObjectID = Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID);
    if Equals(enable, this.m_decelerating) {
      return;
    };
    this.m_decelerating = enable;
    if enable {
      this.m_addDecelerationModifier = RPGManager.CreateStatModifier(gamedataStatType.Deceleration, gameStatModifierType.Additive, this.GetStaticFloatParameterDefault("backInputDecelerationModifier", 8.00));
      scriptInterface.GetStatsSystem().AddModifier(ownerID, this.m_addDecelerationModifier);
      if this.m_perkDecelerationMultiplier > 0.00 && PlayerDevelopmentSystem.GetData(scriptInterface.executionOwner).IsNewPerkBoughtAnyLevel(gamedataNewPerkType.Reflexes_Central_Perk_1_1) {
        this.m_multiplyDecelerationModifier = RPGManager.CreateStatModifier(gamedataStatType.Deceleration, gameStatModifierType.Multiplier, 1.00 / this.m_perkDecelerationMultiplier);
        scriptInterface.GetStatsSystem().AddModifier(ownerID, this.m_multiplyDecelerationModifier);
      };
      return;
    };
    if IsDefined(this.m_addDecelerationModifier) {
      scriptInterface.GetStatsSystem().RemoveModifier(ownerID, this.m_addDecelerationModifier);
      this.m_addDecelerationModifier = null;
    };
    if IsDefined(this.m_multiplyDecelerationModifier) {
      scriptInterface.GetStatsSystem().RemoveModifier(ownerID, this.m_multiplyDecelerationModifier);
      this.m_multiplyDecelerationModifier = null;
    };
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let toggleCrouchOffOnSprint: Bool;
    let toggledCrouch: Bool;
    let toggledSprint: Bool;
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    this.ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    if !this.m_rumblePlayed && this.GetInStateTime() >= this.GetStaticFloatParameterDefault("rumbleDelay", 0.50) {
      this.m_rumblePlayed = true;
      DefaultTransition.PlayRumble(scriptInterface, this.GetStaticStringParameterDefault("rumbleName", "medium_slow"));
    };
    toggleCrouchOffOnSprint = PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Cool_Central_Milestone_3) != 3;
    this.UpdateInputToggles(stateContext, scriptInterface, 0.05, !toggleCrouchOffOnSprint, toggledSprint, toggledCrouch);
    if toggledCrouch {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
    };
    if this.GetStaticBoolParameterDefault("backInputDeceleratesSlide", false) {
      this.EvaluateSlideDeceleration(stateContext, scriptInterface);
    };
  }

  private final func EvaluateSlideDeceleration(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if this.GetInStateTime() >= this.GetStaticFloatParameterDefault("minTimeToAllowDeceleration", 0.10) && scriptInterface.GetActionValue(n"MoveY") < -0.50 {
      this.AddDecelerationStatModifier(stateContext, scriptInterface, true);
    };
  }

  public final func OnExitHelper(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, dontStopRumble: Bool) -> Void {
    this.OnExit(stateContext, scriptInterface);
    this.CleanUpOnExit(stateContext, scriptInterface);
    if !dontStopRumble {
      SlideEvents.StopSlideSoundEffect(scriptInterface);
    };
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitHelper(stateContext, scriptInterface, false);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitHelper(stateContext, scriptInterface, false);
  }

  public final func OnExitToCrouch(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitHelper(stateContext, scriptInterface, false);
    scriptInterface.SetAnimationParameterFloat(n"crouch", 1.00);
  }

  public final func OnExitNoCrouch(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, keepSprint: Bool, opt grounded: Bool) -> Void {
    this.OnExitHelper(stateContext, scriptInterface, grounded);
    if !grounded || PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Cool_Central_Milestone_3) != 3 {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
    };
    if keepSprint && this.m_enteredWithSprint {
      stateContext.SetConditionBoolParameter(n"SprintToggled", true, true);
    };
  }

  public final func OnExitToJump(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitNoCrouch(stateContext, scriptInterface, true);
  }

  public final func OnExitToChargeJump(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitNoCrouch(stateContext, scriptInterface, true);
  }

  public final func OnExitToHoverJump(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitNoCrouch(stateContext, scriptInterface, true);
  }

  public final func OnExitToClimb(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitNoCrouch(stateContext, scriptInterface, false);
  }

  public final func OnExitToVault(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitNoCrouch(stateContext, scriptInterface, true);
  }

  public final func OnExitToDodge(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitNoCrouch(stateContext, scriptInterface, true, true);
  }

  private final func CleanUpOnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.AddDecelerationStatModifier(stateContext, scriptInterface, false);
    stateContext.RemovePermanentBoolParameter(n"forceSlide");
  }

  public final static func StopSlideSoundEffect(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    GameObject.PlaySoundEvent(scriptInterface.owner, n"lcm_fs_additional_slide_stop");
  }
}

public class DodgeDecisions extends LocomotionGroundDecisions {

  public let m_shouldDisableEnterCondition: Bool;

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnAttach(stateContext, scriptInterface);
    scriptInterface.executionOwner.RegisterInputListener(this, n"Dodge");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeDirection");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeForward");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeRight");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeLeft");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeBack");
    this.EnableOnEnterCondition(false);
  }

  protected final func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    scriptInterface.executionOwner.UnregisterInputListener(this);
  }

  protected cb func OnAction(action: ListenerAction, consumer: ListenerActionConsumer) -> Bool {
    if ListenerAction.IsButtonJustPressed(action) {
      this.m_shouldDisableEnterCondition = false;
      this.EnableOnEnterCondition(true);
    };
    if ListenerAction.IsButtonJustReleased(action) {
      this.m_shouldDisableEnterCondition = true;
    };
  }

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.m_shouldDisableEnterCondition {
      this.EnableOnEnterCondition(false);
    };
    if this.WantsToDodge(stateContext, scriptInterface) {
      if !scriptInterface.HasStatFlag(gamedataStatType.HasDodge) {
        return false;
      };
      if this.IsTimeDilationActive(stateContext, scriptInterface, n"kereznikov") {
        return false;
      };
      if !scriptInterface.HasStatFlag(gamedataStatType.CanAimWhileDodging) && stateContext.IsStateActive(n"UpperBody", n"aimingState") && DefaultTransition.IsRangedWeaponEquipped(scriptInterface) {
        return false;
      };
      return true;
    };
    return false;
  }

  protected const func ToSlide(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsTouchingGround(scriptInterface) && !StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeBuff") && !stateContext.GetConditionBool(n"DodgeWhileCrouching") && (stateContext.GetConditionBool(n"CrouchToggled") || scriptInterface.GetActionValue(n"Crouch") > 0.00);
  }

  protected const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsTouchingGround(scriptInterface) && !StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeBuff") && !stateContext.GetConditionBool(n"DodgeWhileCrouching") && !stateContext.GetConditionBool(n"CrouchToggled") && scriptInterface.GetActionValue(n"Crouch") == 0.00;
  }

  protected const func ToFall(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let hasDodgeBuff: Bool = StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeBuff");
    let hasDodgeAirBuff: Bool = StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeAirBuff");
    let isKerenzikovActive: Bool = Equals(stateContext.GetStateMachineCurrentState(n"TimeDilation"), n"kerenzikov");
    if isKerenzikovActive && !this.IsTouchingGround(scriptInterface) && this.IsInUpperBodyState(stateContext, n"aimingState") && PlayerDevelopmentSystem.GetData(scriptInterface.executionOwner).IsNewPerkBoughtAnyLevel(gamedataNewPerkType.Reflexes_Inbetween_Left_3) {
      return false;
    };
    return !this.IsTouchingGround(scriptInterface) && !hasDodgeBuff && !hasDodgeAirBuff;
  }

  protected const func ToCrouch(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsTouchingGround(scriptInterface) && !StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeBuff") && (stateContext.GetConditionBool(n"DodgeWhileCrouching") || stateContext.GetConditionBool(n"CrouchToggled") || scriptInterface.GetActionValue(n"Crouch") > 0.00);
  }
}

public class DodgeEvents extends LocomotionGroundEvents {

  public let m_blockStatFlag: ref<gameStatModifierData>;

  public let m_dashDecelerationModifier: ref<gameStatModifierData>;

  public let m_airDashDecelerationModifier: ref<gameStatModifierData>;

  public let m_currentNumberOfJumps: Int32;

  @default(DodgeEvents, false)
  public let m_pressureWaveCreated: Bool;

  public let m_crouching: Bool;

  public let m_enteredFromSlide: Bool;

  @default(DodgeEvents, false)
  public let m_isAirDashSaveLockTriggered: Bool;

  protected func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let crossHairNPC: NPCNextToTheCrosshair;
    let fierceDashPerkIsBought: Bool;
    let inputSchemeNum: Uint32;
    let inputSchemesBB: ref<IBlackboard>;
    let isAirDash: Bool;
    let isPlayerInElevator: Bool;
    let isPlayerInTheAir: Bool;
    let maxDistToTarget: Float;
    let npcGObj: wref<GameObject>;
    let npcdata: ref<IBlackboard>;
    let puppet: ref<ScriptedPuppet>;
    let shouldLeapToTarget: Bool;
    let targetObjectMax: ref<GameObject>;
    let targetObjectMin: ref<GameObject>;
    let ownerID: StatsObjectID = Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID);
    let questSystem: ref<QuestsSystem> = scriptInterface.GetQuestsSystem();
    let dodgeHeading: Float = stateContext.GetConditionFloat(n"DodgeDirection");
    let playerDevelopmentData: ref<PlayerDevelopmentData> = PlayerDevelopmentSystem.GetData(scriptInterface.executionOwner);
    let isExhausted: Bool = GameInstance.GetStatPoolsSystem(scriptInterface.GetGame()).GetStatPoolValue(Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID), gamedataStatPoolType.Stamina) == 0.00;
    let locomotionState: CName = stateContext.GetStateMachineCurrentState(n"Locomotion");
    scriptInterface.localBlackboard.SetFloat(GetAllBlackboardDefs().PlayerStateMachine.DodgeTimeStamp, EngineTime.ToFloat(GameInstance.GetEngineTime(scriptInterface.GetGame())));
    this.m_currentNumberOfJumps = stateContext.GetIntParameter(n"currentNumberOfJumps", true);
    super.OnEnter(stateContext, scriptInterface);
    if Equals(locomotionState, n"chargeJump") {
      stateContext.SetPermanentBoolParameter(n"isGravityAffectedByChargedJump", true, true);
    };
    inputSchemesBB = GameInstance.GetBlackboardSystem(scriptInterface.GetGame()).Get(GetAllBlackboardDefs().InputSchemes);
    if IsDefined(inputSchemesBB) {
      inputSchemeNum = inputSchemesBB.GetUint(GetAllBlackboardDefs().InputSchemes.Scheme);
    };
    this.m_crouching = inputSchemeNum == 0u && !scriptInterface.executionOwner.PlayerLastUsedKBM() || (Equals(locomotionState, n"crouch") || Equals(locomotionState, n"crouchSprint") || Equals(locomotionState, n"slide")) && (inputSchemeNum == 0u || !scriptInterface.IsActionJustTapped(n"ToggleCrouch") || !scriptInterface.IsActionJustPressed(n"Crouch"));
    if this.m_crouching {
      puppet = scriptInterface.owner as ScriptedPuppet;
      if IsDefined(puppet) {
        puppet.GetPuppetPS().SetCrouch(true);
      };
      scriptInterface.GetAudioSystem().NotifyGameTone(n"EnterCrouch");
      scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().OnEnterCrouch(scriptInterface.owner);
      this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 13);
      scriptInterface.SetAnimationParameterFloat(n"crouch", 1.00);
    } else {
      if Equals(locomotionState, n"crouch") || Equals(locomotionState, n"crouchSprint") {
        stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
      };
      this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 7);
    };
    stateContext.SetConditionBoolParameter(n"DodgeWhileCrouching", this.m_crouching, true);
    npcdata = GameInstance.GetBlackboardSystem(scriptInterface.GetGame()).Get(GetAllBlackboardDefs().UI_NPCNextToTheCrosshair);
    crossHairNPC = FromVariant<NPCNextToTheCrosshair>(npcdata.GetVariant(GetAllBlackboardDefs().UI_NPCNextToTheCrosshair.NameplateData));
    npcGObj = crossHairNPC.npc as ScriptedPuppet;
    shouldLeapToTarget = npcGObj.IsHostile();
    isPlayerInTheAir = !this.IsTouchingGround(scriptInterface);
    isPlayerInElevator = GameInstance.GetBlackboardSystem(scriptInterface.GetGame()).GetLocalInstanced(scriptInterface.ownerEntityID, GetAllBlackboardDefs().PlayerStateMachine).GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideElevator);
    if !isPlayerInElevator && playerDevelopmentData.IsNewPerkBought(gamedataNewPerkType.Reflexes_Central_Milestone_2) == 2 {
      if isPlayerInTheAir && playerDevelopmentData.IsNewPerkBought(gamedataNewPerkType.Reflexes_Central_Milestone_3) != 3 {
        return;
      };
      stateContext.SetPermanentBoolParameter(n"TemporarySpeedLimitIgnore", true, true);
      fierceDashPerkIsBought = playerDevelopmentData.IsNewPerkBoughtAnyLevel(gamedataNewPerkType.Reflexes_Central_Perk_2_1);
      maxDistToTarget = this.GetStaticFloatParameterDefault("maxDistToTarget", 5.00);
      maxDistToTarget = fierceDashPerkIsBought ? maxDistToTarget * 2.00 : maxDistToTarget;
      targetObjectMax = DefaultTransition.GetTargetObject(scriptInterface, maxDistToTarget, true);
      targetObjectMin = DefaultTransition.GetTargetObject(scriptInterface, this.GetStaticFloatParameterDefault("minDistToTarget", 1.00), true);
      if shouldLeapToTarget && AbsF(dodgeHeading) < 60.00 && (IsDefined(targetObjectMin) || IsDefined(targetObjectMax)) {
        StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.PlayerMadDashLocomotionBuffer");
        this.LeapToTarget(stateContext, scriptInterface, npcGObj);
        this.PlayRumbleBasedOnDodgeDirection(stateContext, scriptInterface);
      } else {
        if isPlayerInTheAir {
          isAirDash = this.TreatDashAsAirDash(scriptInterface);
        };
        this.Dash(stateContext, scriptInterface, isExhausted, isAirDash);
        this.PlayRumbleBasedOnDodgeDirection(stateContext, scriptInterface, isExhausted);
      };
      if !isExhausted {
        this.PlaySound(n"lcm_dash", scriptInterface);
        GameObjectEffectHelper.StartEffectEvent(scriptInterface.executionOwner, n"dash");
        StatusEffectHelper.ApplyStatusEffect(scriptInterface.owner, t"BaseStatusEffect.PlayerJustDashed", scriptInterface.owner.GetEntityID());
      };
      scriptInterface.PushAnimationEvent(n"Dodge");
    } else {
      this.Dodge(stateContext, scriptInterface, isExhausted);
      if !this.m_crouching {
        scriptInterface.PushAnimationEvent(n"Dodge");
      };
      if StatusEffectHelper.HasStatusEffectWithTagConst(scriptInterface.executionOwner as PlayerPuppet, n"SecondChancePerkTimeDilation") {
        this.PlaySound(n"lcm_dash", scriptInterface);
      };
      this.PlayRumbleBasedOnDodgeDirection(stateContext, scriptInterface);
    };
    questSystem.SetFact(n"gmpl_player_dodged", questSystem.GetFact(n"gmpl_player_dodged") + 1);
    this.m_blockStatFlag = RPGManager.CreateStatModifier(gamedataStatType.IsDodging, gameStatModifierType.Additive, 1.00);
    StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeBuff");
    StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.JustDodgedBuffer");
    StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.PlayerJustDodgedLocomotionBuffer");
    if !isExhausted && (dodgeHeading < -45.00 || dodgeHeading > 45.00) {
      StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeInvulnerability");
    };
    this.ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    scriptInterface.GetStatsSystem().AddModifier(ownerID, this.m_blockStatFlag);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Dodge);
    this.LogSpecialMovementToTelemetry(scriptInterface, telemetryMovementType.Dodge);
  }

  protected final func OnEnterFromSlide(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_enteredFromSlide = true;
    this.OnEnter(stateContext, scriptInterface);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    if !this.m_pressureWaveCreated && this.GetInStateTime() >= 0.15 {
      this.m_pressureWaveCreated = true;
      this.BroadcastStimuliFootstepSprint(scriptInterface);
    };
    if !this.m_isAirDashSaveLockTriggered && !this.IsTouchingGround(scriptInterface) {
      SaveLocksManager.RequestSaveLockAdd(scriptInterface.owner.GetGame(), n"DisableSaveWhileAirDashing");
      this.m_isAirDashSaveLockTriggered = true;
    };
    if scriptInterface.IsActionJustPressed(n"Jump") {
      stateContext.SetConditionBoolParameter(n"JumpPressed", true, true);
    };
    if scriptInterface.IsActionJustPressed(n"ToggleSprint") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", true, true);
    };
    if scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustHeld(n"ToggleCrouch") {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", !this.m_crouching, true);
    };
    this.RemoveOpticalCamoEffect(stateContext, scriptInterface, 7, 0.85);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.CleanUpOnExit(stateContext, scriptInterface);
    super.OnExit(stateContext, scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    this.CleanUpOnExit(stateContext, scriptInterface);
  }

  private final func CleanUpOnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let puppet: ref<ScriptedPuppet>;
    let ownerID: StatsObjectID = Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID);
    this.m_pressureWaveCreated = false;
    if this.m_enteredFromSlide {
      SlideEvents.StopSlideSoundEffect(scriptInterface);
      this.m_enteredFromSlide = false;
    };
    if IsDefined(this.m_blockStatFlag) {
      scriptInterface.GetStatsSystem().RemoveModifier(ownerID, this.m_blockStatFlag);
    };
    StatusEffectHelper.RemoveStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeBuff");
    StatusEffectHelper.RemoveStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeAirBuff");
    stateContext.SetPermanentBoolParameter(n"TemporarySpeedLimitIgnore", false, true);
    this.m_dashDecelerationModifier = this.DisableMovementDecelerationStatModifier(stateContext, scriptInterface, this.m_dashDecelerationModifier);
    this.m_airDashDecelerationModifier = this.DisableMovementDecelerationStatModifier(stateContext, scriptInterface, this.m_airDashDecelerationModifier);
    if this.m_crouching {
      puppet = scriptInterface.owner as ScriptedPuppet;
      if IsDefined(puppet) {
        puppet.GetPuppetPS().SetCrouch(false);
      };
      scriptInterface.GetAudioSystem().NotifyGameTone(n"LeaveCrouch");
      this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 0);
      scriptInterface.SetAnimationParameterFloat(n"crouch", 0.00);
    };
    if this.m_isAirDashSaveLockTriggered {
      SaveLocksManager.RequestSaveLockRemove(scriptInterface.owner.GetGame(), n"DisableSaveWhileAirDashing");
      this.m_isAirDashSaveLockTriggered = false;
    };
  }

  protected final func LeapToTarget(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, target: wref<GameObject>) -> Void {
    let adjustPosition: Vector4;
    let slideDuration: Float;
    let vecToTarget: Vector4;
    let cameraWorldTransform: Transform = scriptInterface.GetCameraWorldTransform();
    let leapAngle: EulerAngles = Transform.ToEulerAngles(cameraWorldTransform);
    if leapAngle.Pitch > this.GetStaticFloatParameterDefault("noTargetMaxPitch", 45.00) {
      leapAngle.Pitch = this.GetStaticFloatParameterDefault("noTargetMaxPitch", 45.00);
      Transform.SetOrientationEuler(cameraWorldTransform, leapAngle);
    };
    vecToTarget = target.GetWorldPosition() - cameraWorldTransform.position;
    vecToTarget -= Vector4.Normalize(vecToTarget) * 1.25;
    adjustPosition = scriptInterface.executionOwner.GetWorldPosition() + vecToTarget;
    slideDuration = this.CalculateAdjustmentDuration(Vector4.Length(vecToTarget));
    this.RequestPlayerPositionAdjustment(stateContext, scriptInterface, null, slideDuration, 0.00, -1.00, adjustPosition);
  }

  private final func CalculateAdjustmentDuration(distance: Float) -> Float {
    let duration: Float;
    let minDist: Float = this.GetStaticFloatParameterDefault("minDistToTarget", 1.00);
    let maxDist: Float = this.GetStaticFloatParameterDefault("maxDistToTarget", 5.00);
    let minDur: Float = this.GetStaticFloatParameterDefault("minAdjustmentDuration", 0.05);
    let maxDur: Float = this.GetStaticFloatParameterDefault("maxAdjustmentDuration", 0.25);
    distance -= minDist;
    maxDist -= minDist;
    duration = LerpF(distance / maxDist, minDur, maxDur, true);
    return duration;
  }

  private final func TreatDashAsAirDash(scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let psmBlackboard: ref<IBlackboard> = GameInstance.GetBlackboardSystem(scriptInterface.GetGame()).GetLocalInstanced(scriptInterface.ownerEntityID, GetAllBlackboardDefs().PlayerStateMachine);
    let minElevation: Float = psmBlackboard.GetFloat(GetAllBlackboardDefs().PlayerStateMachine.MinElevation);
    let maxElevation: Float = psmBlackboard.GetFloat(GetAllBlackboardDefs().PlayerStateMachine.MaxElevation);
    let elevationThreshold: Float = TDB.GetFloat(t"Perks.IsPlayerInAirDashElevation.elevationThreshold");
    return AbsF(minElevation) > elevationThreshold || AbsF(maxElevation) > elevationThreshold;
  }

  protected final func Dash(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, isExhausted: Bool, treatDashAsAirDash: Bool) -> Void {
    let dodgeHeading: Float;
    let impulse: Vector4;
    let impulseValue: Float;
    let dashMovementDecelerationModifier: Float = this.GetStaticFloatParameterDefault("dashMovementDecelerationModifier", -0.71);
    let airDashMovementDecelerationModifier: Float = this.GetStaticFloatParameterDefault("airDashMovementDecelerationModifier", -0.71);
    if !this.IsTouchingGround(scriptInterface) {
      stateContext.SetPermanentBoolParameter(n"disableAirDash", true, true);
      if this.m_currentNumberOfJumps > 0 {
        this.m_currentNumberOfJumps += 1;
        stateContext.SetPermanentIntParameter(n"currentNumberOfJumps", this.m_currentNumberOfJumps, true);
      };
    };
    if treatDashAsAirDash {
      this.m_airDashDecelerationModifier = this.EnableMovementDecelerationStatModifier(stateContext, scriptInterface, this.m_airDashDecelerationModifier, airDashMovementDecelerationModifier);
      if isExhausted {
        impulseValue = this.GetStaticFloatParameterDefault("airDashImpulseNoStamina", 6.50);
      } else {
        impulseValue = this.GetStaticFloatParameterDefault("airDashImpulse", 10.00);
        StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeAirBuff");
      };
      if PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Reflexes_Master_Perk_3) == 1 {
        scriptInterface.executionOwner.QueueEvent(new ReflexesMasterPerk3Triggerd());
      };
      StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.PlayerJustAirDashed");
    } else {
      this.m_dashDecelerationModifier = this.EnableMovementDecelerationStatModifier(stateContext, scriptInterface, this.m_dashDecelerationModifier, dashMovementDecelerationModifier);
      if isExhausted {
        impulseValue = this.GetStaticFloatParameterDefault("dashImpulseNoStamina", 7.80);
      } else {
        impulseValue = this.GetStaticFloatParameterDefault("dashImpulse", 10.00);
      };
    };
    dodgeHeading = stateContext.GetConditionFloat(n"DodgeDirection");
    impulse = Vector4.FromHeading(AngleNormalize180(Transform.GetYaw(scriptInterface.GetCameraWorldTransform()) + dodgeHeading)) * impulseValue;
    this.AddImpulse(stateContext, impulse);
    if !isExhausted {
      RPGManager.AwardExperienceFromLocomotion(scriptInterface.owner as PlayerPuppet, 5.00);
    };
  }

  protected final func Dodge(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, isExhausted: Bool) -> Void {
    let dodgeHeading: Float;
    let impulse: Vector4;
    let impulseValue: Float;
    let dashMovementDecelerationModifier: Float = this.GetStaticFloatParameterDefault("dashMovementDecelerationModifier", -0.71);
    this.m_dashDecelerationModifier = this.EnableMovementDecelerationStatModifier(stateContext, scriptInterface, this.m_dashDecelerationModifier, dashMovementDecelerationModifier);
    if isExhausted {
      impulseValue = this.GetStaticFloatParameterDefault("impulseNoStamina", 5.50);
    } else {
      impulseValue = this.GetStaticFloatParameterDefault("impulse", 7.80);
      StatusEffectHelper.ApplyStatusEffect(scriptInterface.owner, t"BaseStatusEffect.PlayerJustDodged", scriptInterface.owner.GetEntityID());
    };
    dodgeHeading = stateContext.GetConditionFloat(n"DodgeDirection");
    impulse = Vector4.FromHeading(AngleNormalize180(Transform.GetYaw(scriptInterface.GetCameraWorldTransform()) + dodgeHeading)) * impulseValue;
    this.AddImpulse(stateContext, impulse);
    if !isExhausted {
      RPGManager.AwardExperienceFromLocomotion(scriptInterface.owner as PlayerPuppet, 3.00);
    };
  }
}

public class ClimbDecisions extends LocomotionGroundDecisions {

  public const let stateBodyDone: Bool;

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let climbInfo: ref<PlayerClimbInfo>;
    let enterAngleThreshold: Float;
    let isObstacleSuitable: Bool;
    let preClimbAnimFeature: ref<AnimFeature_PreClimbing>;
    let isPathClear: Bool = false;
    let isInAcceptableAerialState: Bool = scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Locomotion) == 5 || this.IsInLocomotionState(stateContext, n"dodgeAir") || stateContext.GetBoolParameter(n"enteredFallFromAirDodge", true);
    let isInAutoClimbState: Bool = PlayerDevelopmentSystem.GetData(scriptInterface.executionOwner).IsNewPerkBought(gamedataNewPerkType.Reflexes_Central_Milestone_2) == 2 && this.IsTouchingGround(scriptInterface) && (this.IsInLocomotionState(stateContext, n"dodge") || StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"JustDodgedLocomotion"));
    let isClimbInputActive: Bool = stateContext.GetConditionBool(n"JumpPressed") || scriptInterface.IsActionJustPressed(n"Jump");
    if !isInAcceptableAerialState && !isClimbInputActive {
      if !isInAutoClimbState {
        return false;
      };
      if !scriptInterface.IsMoveInputConsiderable() {
        return false;
      };
    };
    climbInfo = scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().GetCurrentClimbInfo(scriptInterface.owner);
    isObstacleSuitable = climbInfo.climbValid && this.OverlapFitTest(scriptInterface, climbInfo);
    if isObstacleSuitable {
      isPathClear = this.TestClimbingPath(scriptInterface, climbInfo, DefaultTransition.GetPlayerPosition(scriptInterface));
      isObstacleSuitable = isObstacleSuitable && isPathClear;
    };
    preClimbAnimFeature = new AnimFeature_PreClimbing();
    preClimbAnimFeature.valid = 0.00;
    if isObstacleSuitable {
      preClimbAnimFeature.edgePositionLS = scriptInterface.TransformInvPointFromObject(climbInfo.descResult.topPoint);
      preClimbAnimFeature.valid = 1.00;
    };
    stateContext.SetConditionScriptableParameter(n"PreClimbAnimFeature", preClimbAnimFeature, true);
    if isObstacleSuitable {
      if this.IsVaultingClimbingRestricted(scriptInterface) {
        return false;
      };
      if !this.ForwardAngleTest(stateContext, scriptInterface, climbInfo) {
        return false;
      };
      if this.IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface) {
        return false;
      };
      if AbsF(scriptInterface.GetInputHeading()) > 90.00 {
        return false;
      };
      if this.IsCameraPitchAcceptable(stateContext, scriptInterface, this.GetStaticFloatParameterDefault("cameraPitchThreshold", -30.00)) {
        return false;
      };
      if stateContext.IsStateActive(n"Locomotion", n"chargeJump") && this.GetVerticalSpeed(scriptInterface) > 0.00 {
        return false;
      };
      enterAngleThreshold = this.GetStaticFloatParameterDefault("inputAngleThreshold", -180.00);
      if !(AbsF(scriptInterface.GetInputHeading()) <= enterAngleThreshold) {
        return false;
      };
      if !stateContext.GetBoolParameter(n"enableVaultFromleapAttack", true) && !MeleeTransition.MeleeUseExplorationCondition(stateContext, scriptInterface) {
        return false;
      };
      return isObstacleSuitable;
    };
    return false;
  }

  public final const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.stateBodyDone;
  }

  public final const func ToCrouch(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.stateBodyDone;
  }

  private final const func ForwardAngleTest(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>, climbInfo: ref<PlayerClimbInfo>) -> Bool {
    let playerForward: Vector4 = scriptInterface.GetOwnerForward();
    let obstaclePosition: Vector4 = climbInfo.descResult.collisionNormal;
    let forwardAngleDifference: Float = Vector4.GetAngleBetween(-obstaclePosition, playerForward);
    let enterAngleThreshold: Float = this.GetStaticFloatParameterDefault("obstacleEnterAngleThreshold", -180.00);
    if forwardAngleDifference < enterAngleThreshold && forwardAngleDifference - 180.00 < enterAngleThreshold {
      return true;
    };
    return false;
  }

  private final const func TestClimbingPath(const scriptInterface: ref<StateGameScriptInterface>, climbInfo: ref<PlayerClimbInfo>, playerPosition: Vector4) -> Bool {
    let fitTestOvelap1: TraceResult;
    let fitTestOvelap2: TraceResult;
    let overlapPosition2: Vector4;
    let overlapResult1: Bool;
    let overlapResult2: Bool;
    let playerCapsuleDimensions: Vector4;
    let rayCastDestinationPosition1: Vector4;
    let rayCastDestinationPosition2: Vector4;
    let rayCastResult1: Bool;
    let rayCastResult2: Bool;
    let rayCastSourcePosition2: Vector4;
    let rayCastTraceResult1: TraceResult;
    let rayCastTraceResult2: TraceResult;
    let rotation1: EulerAngles;
    let rotation2: EulerAngles;
    let groundTolerance: Float = 0.05;
    let tolerance: Float = 0.15;
    playerCapsuleDimensions.X = this.GetStaticFloatParameterDefault("capsuleRadius", 0.40);
    playerCapsuleDimensions.Y = -1.00;
    playerCapsuleDimensions.Z = -1.00;
    let climbDestination: Vector4 = climbInfo.descResult.topPoint + DefaultTransition.GetUpVector() * (playerCapsuleDimensions.X + tolerance);
    let overlapPosition1: Vector4 = playerPosition;
    overlapPosition1.Z = climbDestination.Z;
    let rayCastSourcePosition1: Vector4 = playerPosition;
    rayCastSourcePosition1.Z += groundTolerance;
    rayCastDestinationPosition1 = overlapPosition1;
    rayCastTraceResult1 = scriptInterface.LocomotionRaycastTest(rayCastSourcePosition1, rayCastDestinationPosition1);
    rayCastResult1 = TraceResult.IsValid(rayCastTraceResult1);
    if !rayCastResult1 {
      overlapResult1 = scriptInterface.LocomotionOverlapTest(playerCapsuleDimensions, overlapPosition1, rotation1, fitTestOvelap1);
    };
    if !rayCastResult1 && !overlapResult1 {
      overlapPosition2 = climbDestination;
      rayCastSourcePosition2 = overlapPosition1;
      rayCastDestinationPosition2 = overlapPosition2;
      rayCastTraceResult2 = scriptInterface.LocomotionRaycastTest(rayCastSourcePosition2, rayCastDestinationPosition2);
      rayCastResult2 = TraceResult.IsValid(rayCastTraceResult2);
      if !rayCastResult2 {
        overlapResult2 = scriptInterface.LocomotionOverlapTest(playerCapsuleDimensions, overlapPosition2, rotation2, fitTestOvelap2);
      };
    };
    return !rayCastResult1 && !overlapResult1 && !rayCastResult2 && !overlapResult2;
  }

  private final const func OverlapFitTest(const scriptInterface: ref<StateGameScriptInterface>, climbInfo: ref<PlayerClimbInfo>) -> Bool {
    let fitTestOvelap: TraceResult;
    let playerCapsuleDimensions: Vector4;
    let rotation: EulerAngles;
    let tolerance: Float = 0.15;
    playerCapsuleDimensions.X = this.GetStaticFloatParameterDefault("capsuleRadius", 0.40);
    playerCapsuleDimensions.Y = -1.00;
    playerCapsuleDimensions.Z = -1.00;
    let queryPosition: Vector4 = climbInfo.descResult.topPoint + DefaultTransition.GetUpVector() * (playerCapsuleDimensions.X + tolerance);
    let crouchOverlap: Bool = scriptInterface.LocomotionOverlapTest(playerCapsuleDimensions, queryPosition, rotation, fitTestOvelap);
    return !crouchOverlap;
  }
}

public class ClimbEvents extends LocomotionGroundEvents {

  public let m_ikHandEvents: [ref<IKTargetAddEvent>];

  public let m_shouldIkHands: Bool;

  public let m_framesDelayingAnimStart: Int32;

  public let m_climbedEntity: wref<Entity>;

  public let m_playerCapsuleDimensions: Vector4;

  private final func GetClimbParameter(scriptInterface: ref<StateGameScriptInterface>) -> ref<ClimbParameters> {
    let climbSpeed: Float;
    let climbTypeKey: String;
    let obstacleHeight: Float;
    let climbParameters: ref<ClimbParameters> = new ClimbParameters();
    let statSystem: ref<StatsSystem> = scriptInterface.GetStatsSystem();
    let direction: Vector4 = scriptInterface.GetOwnerForward();
    let directionOffset: Vector4 = direction * this.GetStaticFloatParameterDefault("capsuleRadius", 0.00);
    let climbInfo: ref<PlayerClimbInfo> = scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().GetCurrentClimbInfo(scriptInterface.owner);
    let playerPosition: Vector4 = DefaultTransition.GetPlayerPosition(scriptInterface);
    climbParameters.SetObstacleFrontEdgePosition(climbInfo.descResult.topPoint);
    climbParameters.SetObstacleFrontEdgeNormal(climbInfo.descResult.collisionNormal);
    climbParameters.SetObstacleVerticalDestination(climbInfo.descResult.topPoint - directionOffset);
    climbParameters.SetObstacleSurfaceNormal(climbInfo.descResult.topNormal);
    climbParameters.SetObstacleHorizontalDestination(climbInfo.descResult.topPoint + direction * this.GetStaticFloatParameterDefault("forwardStep", 0.50));
    this.m_climbedEntity = climbInfo.descResult.climbedEntity;
    climbParameters.SetClimbedEntity(this.m_climbedEntity);
    obstacleHeight = climbInfo.descResult.topPoint.Z - playerPosition.Z;
    if obstacleHeight > this.GetStaticFloatParameterDefault("highThreshold", 1.00) {
      climbParameters.SetClimbType(0);
      climbTypeKey = "High";
      this.m_shouldIkHands = true;
    } else {
      if obstacleHeight > this.GetStaticFloatParameterDefault("midThreshold", 1.00) {
        climbParameters.SetClimbType(1);
        climbTypeKey = "Mid";
        this.m_shouldIkHands = true;
      } else {
        climbParameters.SetClimbType(2);
        climbTypeKey = "Low";
        this.m_shouldIkHands = false;
      };
    };
    climbSpeed = this.GetStatFloatValue(scriptInterface, gamedataStatType.ClimbSpeedModifier, statSystem);
    if climbSpeed <= 0.00 {
      climbSpeed = 1.00;
    };
    climbParameters.SetHorizontalDuration(climbSpeed * this.GetStaticFloatParameterDefault("horizontalDuration" + climbTypeKey, 10.00));
    climbParameters.SetVerticalDuration(climbSpeed * this.GetStaticFloatParameterDefault("verticalDuration" + climbTypeKey, 10.00));
    climbParameters.SetAnimationNameApproach(this.GetStaticCNameParameterDefault("animationNameApproach", n"None"));
    return climbParameters;
  }

  private final func CreateIKConstraint(scriptInterface: ref<StateGameScriptInterface>, const handData: HandIKDescriptionResult, const refUpVector: Vector4, const ikChainName: CName, climbedEntity: ref<Entity>) -> Void {
    let climbedEntityTransform: Transform;
    let ikEvent: ref<IKTargetAddEvent> = new IKTargetAddEvent();
    let edgeSlop: Vector4 = handData.grabPointStart - handData.grabPointEnd;
    let handNormal: Vector4 = Vector4.Cross(edgeSlop, refUpVector);
    handNormal = Vector4.Normalize(handNormal);
    handNormal.Z = 0.30;
    handNormal = Vector4.Normalize(handNormal);
    let handOrientation: Matrix = Matrix.BuildFromDirectionVector(handNormal, edgeSlop);
    if IsDefined(climbedEntity) {
      climbedEntityTransform = WorldTransform._ToXForm(climbedEntity.GetWorldTransform());
      ikEvent.targetPositionProvider = IPositionProvider.CreateEntityPositionProvider(climbedEntity, Cast<Vector3>(Transform.TransformPoint(Transform.GetInverse(climbedEntityTransform), handData.grabPointEnd + edgeSlop * 0.50)));
      ikEvent.orientationProvider = IOrientationProvider.CreateEntityOrientationProvider(null, n"None", climbedEntity, Quaternion.Conjugate(Transform.GetOrientation(climbedEntityTransform)) * Matrix.ToQuat(handOrientation));
    } else {
      ikEvent.SetStaticTarget(handData.grabPointEnd + edgeSlop * 0.50);
      ikEvent.SetStaticOrientationTarget(Matrix.ToQuat(handOrientation));
    };
    ikEvent.request.transitionIn = 0.00;
    ikEvent.request.priority = -100;
    ikEvent.bodyPart = ikChainName;
    scriptInterface.owner.QueueEvent(ikEvent);
    ArrayPush(this.m_ikHandEvents, ikEvent);
  }

  private final func AddHandIK(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let climbInfo: ref<PlayerClimbInfo> = scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().GetCurrentClimbInfo(scriptInterface.owner);
    this.CreateIKConstraint(scriptInterface, climbInfo.descResult.leftHandData, new Vector4(0.00, 0.00, 1.00, 0.00), n"ikLeftArm", this.m_climbedEntity);
    this.CreateIKConstraint(scriptInterface, climbInfo.descResult.rightHandData, new Vector4(0.00, 0.00, -1.00, 0.00), n"ikRightArm", this.m_climbedEntity);
  }

  private final func RemoveHandIK(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let ikEvent: ref<IKTargetAddEvent>;
    let i: Int32 = 0;
    while i < ArraySize(this.m_ikHandEvents) {
      ikEvent = this.m_ikHandEvents[i];
      if !IsDefined(ikEvent) {
      } else {
        IKTargetRemoveEvent.QueueRemoveIkTargetRemoveEvent(scriptInterface.owner, ikEvent);
      };
      i += 1;
    };
    ArrayClear(this.m_ikHandEvents);
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    GameObject.PlayVoiceOver(scriptInterface.owner, n"climbStart", n"Scripts:ClimbEvents");
    stateContext.SetTemporaryScriptableParameter(n"climbInfo", this.GetClimbParameter(scriptInterface), true);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Climb);
    DefaultTransition.PlayRumble(scriptInterface, "light_fast");
    this.m_framesDelayingAnimStart = 0;
    this.m_playerCapsuleDimensions.X = this.GetStaticFloatParameterDefault("capsuleRadius", 0.40);
    this.m_playerCapsuleDimensions.Y = this.GetStaticFloatParameterDefault("capsuleHeight", 1.00) * 0.50 - this.m_playerCapsuleDimensions.X;
    this.m_playerCapsuleDimensions.Z = -1.00;
    RPGManager.AwardExperienceFromLocomotion(scriptInterface.owner as PlayerPuppet, 3.00);
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let capsuleOrientation: EulerAngles;
    let fitTestOverlap: TraceResult;
    let hitDetected: Bool;
    let knockdownStatusEffect: wref<StatusEffect_Record>;
    let playerPosition: Vector4;
    let shouldCheckClimbingKnockdown: Bool;
    let vehicleClimbed: ref<VehicleObject>;
    let tolerance: Float = 0.15;
    if IsDefined(this.m_climbedEntity) {
      shouldCheckClimbingKnockdown = true;
      vehicleClimbed = this.m_climbedEntity as VehicleObject;
      if IsDefined(vehicleClimbed) {
        if Vector4.LengthSquared(vehicleClimbed.GetLinearVelocity()) == 0.00 {
          shouldCheckClimbingKnockdown = false;
        };
      };
      if shouldCheckClimbingKnockdown {
        playerPosition = scriptInterface.owner.GetWorldPosition();
        playerPosition.Z += this.m_playerCapsuleDimensions.X + this.m_playerCapsuleDimensions.Y + tolerance;
        capsuleOrientation.Roll = 90.00;
        hitDetected = scriptInterface.LocomotionOverlapTestExcludeEntity(this.m_playerCapsuleDimensions, playerPosition, capsuleOrientation, this.m_climbedEntity, fitTestOverlap);
        if hitDetected && TraceResult.IsValid(fitTestOverlap) {
          knockdownStatusEffect = TweakDBInterface.GetStatusEffectRecord(t"BaseStatusEffect.ClimbingKnockdown");
          scriptInterface.GetStatusEffectSystem().ApplyStatusEffect(scriptInterface.owner.GetEntityID(), t"BaseStatusEffect.ClimbingKnockdown");
          stateContext.SetPermanentScriptableParameter(n"StatusEffect_ForceKnockdown", knockdownStatusEffect, true);
        };
      };
    };
    this.m_framesDelayingAnimStart = this.m_framesDelayingAnimStart + 1;
    if this.m_framesDelayingAnimStart == 3 {
      scriptInterface.SetAnimationParameterFeature(n"PreClimb", stateContext.GetConditionScriptableParameter(n"PreClimbAnimFeature") as AnimFeature_PreClimbing);
      stateContext.RemoveConditionScriptableParameter(n"PreClimbAnimFeature");
      if this.m_shouldIkHands {
        this.AddHandIK(scriptInterface);
      };
    };
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    this.RemoveHandIK(scriptInterface);
    this.m_climbedEntity = null;
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    this.RemoveHandIK(scriptInterface);
    this.m_climbedEntity = null;
  }
}

public class VaultDecisions extends LocomotionGroundDecisions {

  public let m_callbackIDs: [ref<CallbackHandle>];

  public const let stateBodyDone: Bool;

  public let m_shouldDisableEnterCondition: Bool;

  private final const func ObstacleLengthCheck(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>, vaultInfo: ref<PlayerClimbInfo>) -> Bool {
    let minDetectionRange: Float = this.GetStaticFloatParameterDefault("minDetectionRange", 0.40);
    let maxSpeedNormalizer: Float = this.GetStaticFloatParameterDefault("maxSpeedNormalizer", 8.50);
    let detectionRange: Float = this.GetStaticFloatParameterDefault("detectionRange", 2.00);
    let linearVelocity: Vector4 = DefaultTransition.GetLinearVelocity(scriptInterface);
    let normalizedSpeed: Float = MinF(1.00, Vector4.Length(linearVelocity) / maxSpeedNormalizer);
    let playerPosition: Vector4 = DefaultTransition.GetPlayerPosition(scriptInterface);
    let offsetFromObstacle: Vector4 = vaultInfo.descResult.topPoint - playerPosition;
    let playerForward: Vector4 = vaultInfo.inputDirection;
    let offsetFromObstacleInVelocityVector: Vector4 = playerForward * Vector4.Dot(playerForward, offsetFromObstacle);
    let offsetFromObstacleInVelocityVectorMag: Float = Vector4.Length(offsetFromObstacleInVelocityVector);
    let maxExtent: Float = this.GetStaticFloatParameterDefault("maxExtent", 2.10);
    let obstacleExtent: Float = vaultInfo.descResult.topExtent;
    let maxClimbableDistanceFromCurve: Float = minDetectionRange + (detectionRange - minDetectionRange) * normalizedSpeed + 0.05;
    let resVelocity: Bool = offsetFromObstacleInVelocityVectorMag < maxClimbableDistanceFromCurve;
    let resDepth: Bool = obstacleExtent < maxExtent;
    return resVelocity && resDepth;
  }

  protected final const func FitTest(const scriptInterface: ref<StateGameScriptInterface>, playerCapsuleDimensions: Vector4, vaultInfo: ref<PlayerClimbInfo>) -> Bool {
    let direction: Vector4;
    let distance: Float;
    let fitTest: TraceResult;
    let fitTestOverlap: TraceResult;
    let overlapPosition: Vector4;
    let queryPosition: Vector4;
    let rotation: EulerAngles;
    let playerPosition: Vector4 = DefaultTransition.GetPlayerPosition(scriptInterface);
    if vaultInfo.descResult.behindPoint.Z - playerPosition.Z > 0.40 {
      return false;
    };
    distance = vaultInfo.descResult.topExtent;
    direction = vaultInfo.inputDirection;
    queryPosition = vaultInfo.descResult.topPoint + DefaultTransition.GetUpVector() * (playerCapsuleDimensions.X + MaxF(playerCapsuleDimensions.Y, 0.00) + 0.01);
    if scriptInterface.LocomotionSweepTest(playerCapsuleDimensions, queryPosition, rotation, direction, distance, false, fitTest) {
      return false;
    };
    playerCapsuleDimensions.X = 0.10;
    overlapPosition = vaultInfo.descResult.behindPoint;
    overlapPosition.Z += 0.40;
    if scriptInterface.OverlapWithASingleGroup(playerCapsuleDimensions, overlapPosition, rotation, n"VehicleBlocker", fitTestOverlap) {
      return false;
    };
    return true;
  }

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let allBlackboardDef: ref<AllBlackboardDefinitions> = GetAllBlackboardDefs();
    super.OnAttach(stateContext, scriptInterface);
    scriptInterface.executionOwner.RegisterInputListener(this, n"Jump");
    scriptInterface.executionOwner.RegisterInputListener(this, n"Dodge");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeDirection");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeForward");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeRight");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeLeft");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeBack");
    ArrayPush(this.m_callbackIDs, scriptInterface.localBlackboard.RegisterListenerBool(allBlackboardDef.PlayerStateMachine.MeleeLeap, this, n"OnMeleeLeapChanged"));
    this.EnableOnEnterCondition(false);
  }

  protected cb func OnMeleeLeapChanged(value: Bool) -> Bool {
    this.EnableOnEnterCondition(value);
  }

  protected final func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    scriptInterface.executionOwner.UnregisterInputListener(this);
    ArrayClear(this.m_callbackIDs);
  }

  protected cb func OnAction(action: ListenerAction, consumer: ListenerActionConsumer) -> Bool {
    if ListenerAction.IsButtonJustPressed(action) {
      this.m_shouldDisableEnterCondition = false;
      this.EnableOnEnterCondition(true);
    };
    if ListenerAction.IsButtonJustReleased(action) {
      this.m_shouldDisableEnterCondition = true;
    };
  }

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let angle: Float;
    let dashVaultAttempt: Bool;
    let detailedLocomotionState: Int32;
    let enableVaultFromleapAttack: Bool;
    let enterAngleThreshold: Float;
    let playerCapsuleDimensions: Vector4;
    let vaultInfo: ref<PlayerClimbInfo>;
    let velocity: Vector4;
    let playerDevelopmentData: ref<PlayerDevelopmentData> = PlayerDevelopmentSystem.GetData(scriptInterface.executionOwner);
    if playerDevelopmentData.IsNewPerkBought(gamedataNewPerkType.Reflexes_Central_Milestone_2) == 2 && this.IsTouchingGround(scriptInterface) {
      detailedLocomotionState = scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LocomotionDetailed);
      if detailedLocomotionState == 7 || StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"JustDodgedLocomotion") || !this.m_shouldDisableEnterCondition && (scriptInterface.GetActionValue(n"Dodge") > 0.00 || scriptInterface.GetActionValue(n"DodgeDirection") > 0.00 || scriptInterface.GetActionValue(n"DodgeForward") > 0.00 || scriptInterface.GetActionValue(n"DodgeRight") > 0.00 || scriptInterface.GetActionValue(n"DodgeLeft") > 0.00 || scriptInterface.GetActionValue(n"DodgeBack") > 0.00) {
        dashVaultAttempt = true;
      };
    };
    if dashVaultAttempt && scriptInterface.GetActionValue(n"Jump") <= 0.00 && scriptInterface.GetActionValue(n"Dodge") <= 0.00 {
      velocity = DefaultTransition.GetLinearVelocity(scriptInterface);
      angle = Vector4.GetAngleBetween(scriptInterface.executionOwner.GetWorldForward(), velocity);
      if AbsF(angle) > 50.00 {
        this.EnableOnEnterCondition(false);
        return false;
      };
    };
    enableVaultFromleapAttack = stateContext.GetBoolParameter(n"enableVaultFromleapAttack", true);
    if !dashVaultAttempt && !enableVaultFromleapAttack && this.m_shouldDisableEnterCondition {
      this.EnableOnEnterCondition(false);
      return false;
    };
    if this.IsVaultingClimbingRestricted(scriptInterface) {
      this.EnableOnEnterCondition(false);
      return false;
    };
    if !enableVaultFromleapAttack && this.GetStaticBoolParameterDefault("requireDirectionalInputToVault", false) && !scriptInterface.IsMoveInputConsiderable() {
      this.EnableOnEnterCondition(false);
      return false;
    };
    enterAngleThreshold = this.GetStaticFloatParameterDefault("enterAngleThreshold", -180.00);
    if AbsF(scriptInterface.GetInputHeading()) > enterAngleThreshold {
      if !enableVaultFromleapAttack {
        this.EnableOnEnterCondition(false);
      };
      return false;
    };
    if !enableVaultFromleapAttack && !MeleeTransition.MeleeUseExplorationCondition(stateContext, scriptInterface) {
      this.EnableOnEnterCondition(false);
      return false;
    };
    vaultInfo = scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().GetCurrentClimbInfo(scriptInterface.owner);
    if !vaultInfo.vaultValid {
      if !dashVaultAttempt && !enableVaultFromleapAttack {
        this.EnableOnEnterCondition(false);
      };
      return false;
    };
    playerCapsuleDimensions.X = this.GetStaticFloatParameterDefault("capsuleRadius", 0.40);
    playerCapsuleDimensions.Y = dashVaultAttempt ? this.GetStaticFloatParameterDefault("capsuleHeight", 0.90) * 0.50 - playerCapsuleDimensions.X : -1.00;
    playerCapsuleDimensions.Z = -1.00;
    if !this.FitTest(scriptInterface, playerCapsuleDimensions, vaultInfo) {
      if !enableVaultFromleapAttack {
        this.EnableOnEnterCondition(false);
      };
      return false;
    };
    if !this.ObstacleLengthCheck(stateContext, scriptInterface, vaultInfo) {
      if !dashVaultAttempt && !enableVaultFromleapAttack {
        this.EnableOnEnterCondition(false);
      };
      return false;
    };
    if !dashVaultAttempt && !enableVaultFromleapAttack {
      this.EnableOnEnterCondition(false);
    };
    return true;
  }

  public final const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.stateBodyDone;
  }

  public final const func ToCrouch(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.stateBodyDone;
  }

  public final const func ToSlide(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if stateContext.GetConditionBool(n"VaultingDuringCrouchSprint") {
      return false;
    };
    return this.stateBodyDone;
  }
}

public class VaultEvents extends LocomotionGroundEvents {

  protected final func GetVaultParameter(scriptInterface: ref<StateGameScriptInterface>) -> ref<VaultParameters> {
    let behindZ: Float;
    let landingPoint: Vector4;
    let obstacleEnd: Vector4;
    let playerPosition: Vector4 = DefaultTransition.GetPlayerPosition(scriptInterface);
    let vaultParameters: ref<VaultParameters> = new VaultParameters();
    let climbInfo: ref<PlayerClimbInfo> = scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().GetCurrentClimbInfo(scriptInterface.owner);
    let direction: Vector4 = climbInfo.inputDirection;
    vaultParameters.SetObstacleFrontEdgePosition(climbInfo.descResult.topPoint);
    vaultParameters.SetObstacleFrontEdgeNormal(climbInfo.descResult.collisionNormal);
    vaultParameters.SetObstacleVerticalDestination(climbInfo.descResult.topPoint);
    vaultParameters.SetObstacleSurfaceNormal(climbInfo.descResult.topNormal);
    obstacleEnd = climbInfo.obstacleEnd;
    behindZ = MaxF(climbInfo.descResult.behindPoint.Z, playerPosition.Z);
    landingPoint.X = obstacleEnd.X;
    landingPoint.Y = obstacleEnd.Y;
    landingPoint.Z = behindZ;
    vaultParameters.SetObstacleDestination(landingPoint + direction * this.GetStaticFloatParameterDefault("forwardStep", 0.50));
    vaultParameters.SetObstacleDepth(climbInfo.descResult.topExtent);
    vaultParameters.SetMinSpeed(this.GetStaticFloatParameterDefault("minSpeed", 3.50));
    return vaultParameters;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    stateContext.SetTemporaryScriptableParameter(n"vaultInfo", this.GetVaultParameter(scriptInterface), true);
    scriptInterface.PushAnimationEvent(n"Vault");
    GameObject.PlayVoiceOver(scriptInterface.owner, n"Vault", n"Scripts:VaultEvents");
    if !scriptInterface.HasStatFlag(gamedataStatType.CanWeaponReloadWhileVaulting) {
      stateContext.SetTemporaryBoolParameter(n"TryInterruptReload", true, true);
    };
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Vault);
    DefaultTransition.PlayRumble(scriptInterface, "medium_pulse");
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 6);
    RPGManager.AwardExperienceFromLocomotion(scriptInterface.owner as PlayerPuppet, 3.00);
  }

  public final func OnEnterFromCrouchSprint(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnEnter(stateContext, scriptInterface);
    stateContext.SetConditionBoolParameter(n"VaultingDuringCrouchSprint", true, true);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    stateContext.SetPermanentBoolParameter(n"ForceSafeState", false, true);
    stateContext.SetConditionBoolParameter(n"VaultingDuringCrouchSprint", false, true);
    StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.PlayerJustVaultedLocomotionBuffer");
  }
}

public class LadderDecisions extends LocomotionGroundDecisions {

  private final const func TestParameters(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>, out ladderParameter: ref<LadderDescription>) -> Bool {
    let ladderFinishedParameter: StateResultBool;
    ladderParameter = stateContext.GetTemporaryScriptableParameter(n"usingLadder") as LadderDescription;
    if !IsDefined(ladderParameter) {
      ladderParameter = stateContext.GetConditionScriptableParameter(n"usingLadder") as LadderDescription;
      ladderFinishedParameter = stateContext.GetTemporaryBoolParameter(n"exitLadder");
      if ladderFinishedParameter.valid && ladderFinishedParameter.value {
        stateContext.RemoveConditionScriptableParameter(n"usingLadder");
        return false;
      };
      if !IsDefined(ladderParameter) {
        return false;
      };
    } else {
      stateContext.SetConditionScriptableParameter(n"usingLadder", ladderParameter, true);
    };
    return true;
  }

  private final const func TestLadderMath(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>, ladderParameter: ref<LadderDescription>) -> Bool {
    let directionToLadder: Vector4;
    let enterAngleThreshold: Float;
    let fromBottomFactor: Float;
    let isPlayerMovingForward: Bool;
    let ladderEntityAngle: Float;
    let ladderPosition: Vector4;
    let playerInputDirection: Vector4;
    let playerMoveDirection: Float;
    let playerForward: Vector4 = scriptInterface.GetOwnerForward();
    if SgnF(Vector4.Dot(ladderParameter.normal, playerForward)) > 0.00 {
      return false;
    };
    ladderPosition = ladderParameter.position + (ladderParameter.up * (ladderParameter.verticalStepBottom + ladderParameter.topHeightFromPosition)) / 2.00;
    directionToLadder = ladderPosition - DefaultTransition.GetPlayerPosition(scriptInterface);
    directionToLadder = Vector4.Normalize2D(directionToLadder);
    ladderEntityAngle = Rad2Deg(AcosF(ClampF(Vector4.Dot(playerForward, directionToLadder), -1.00, 1.00)));
    enterAngleThreshold = this.GetStaticFloatParameterDefault("enterAngleThreshold", 35.00);
    isPlayerMovingForward = !this.IsPlayerMovingBackwards(stateContext, scriptInterface);
    if !this.IsTouchingGround(scriptInterface) && isPlayerMovingForward {
      if AbsF(ladderEntityAngle) < enterAngleThreshold {
        if this.GetLandingType(stateContext) < 3 {
          if !this.IsPlayerAboveLadderTop(stateContext, scriptInterface) {
            return true;
          };
        };
      };
    } else {
      playerInputDirection = Vector4.Normalize2D(Vector4.RotByAngleXY(playerForward, scriptInterface.GetInputHeading()));
      playerMoveDirection = Rad2Deg(AcosF(ClampF(Vector4.Dot(playerInputDirection, -ladderParameter.normal), -1.00, 1.00)));
      fromBottomFactor = SgnF(Vector4.Dot(ladderParameter.up, directionToLadder));
      if scriptInterface.IsMoveInputConsiderable() && isPlayerMovingForward && fromBottomFactor > 0.00 && AbsF(ladderEntityAngle) < enterAngleThreshold && AbsF(playerMoveDirection) < enterAngleThreshold {
        return true;
      };
    };
    return false;
  }

  protected final const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let finishedLadder: StateResultBool = stateContext.GetTemporaryBoolParameter(n"finishedLadderAction");
    return finishedLadder.valid && finishedLadder.value;
  }

  protected final const func ToLadderCrouch(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.IsActionJustPressed(n"Crouch") || scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustHeld(n"ToggleCrouch") {
      return true;
    };
    return false;
  }

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let isActionEnterLadder: Bool;
    let isActionEnterLadderParam: StateResultBool;
    let ladderParameter: ref<LadderDescription>;
    let testParameters: Bool = this.TestParameters(stateContext, scriptInterface, ladderParameter);
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"NoLadder") {
      return false;
    };
    isActionEnterLadderParam = stateContext.GetTemporaryBoolParameter(n"actionEnterLadder");
    isActionEnterLadder = isActionEnterLadderParam.valid && isActionEnterLadderParam.value;
    if ladderParameter == null && !isActionEnterLadder {
      return false;
    };
    if !MeleeTransition.MeleeUseExplorationCondition(stateContext, scriptInterface) {
      return false;
    };
    if isActionEnterLadder {
      return true;
    };
    return testParameters && this.TestLadderMath(stateContext, scriptInterface, ladderParameter);
  }

  protected final const func CommonEnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let ladderEntryDuration: StateResultFloat = stateContext.GetPermanentFloatParameter(n"ladderEntryDuration");
    if ladderEntryDuration.valid {
      return false;
    };
    if !scriptInterface.IsMoveInputConsiderable() {
      return false;
    };
    if scriptInterface.IsActionJustPressed(n"ToggleSprint") || stateContext.GetConditionBool(n"SprintToggled") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", true, true);
      return true;
    };
    if scriptInterface.GetActionValue(n"Sprint") > 0.00 || scriptInterface.GetActionValue(n"ToggleSprint") > 0.00 {
      return true;
    };
    return false;
  }

  protected final const func CommonToLadder(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>, const isVerticalSpeedValid: Bool) -> Bool {
    if stateContext.GetBoolParameter(n"InterruptSprint") {
      return true;
    };
    if !scriptInterface.IsMoveInputConsiderable() || !isVerticalSpeedValid {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return true;
    };
    if !stateContext.GetConditionBool(n"SprintToggled") && scriptInterface.GetActionValue(n"Sprint") == 0.00 {
      return true;
    };
    if scriptInterface.IsActionJustReleased(n"Sprint") || scriptInterface.IsActionJustPressed(n"AttackA") {
      stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
      stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
      return true;
    };
    return false;
  }
}

public class LadderEvents extends LocomotionGroundEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let locomotionState: CName = stateContext.GetStateMachineCurrentState(n"Locomotion");
    if NotEquals(locomotionState, n"ladderSprint") && NotEquals(locomotionState, n"ladderSlide") {
      super.OnEnter(stateContext, scriptInterface);
      this.SetLadderEntryDuration(stateContext, scriptInterface);
      this.UseLadderCameraParams(stateContext, scriptInterface, LadderCameraParams.Enter);
    };
    this.SetLocomotionParameters(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Ladder);
  }

  public final func OnEnterFromJump(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SendLadderEnterStyleToGraph(scriptInterface, 1);
    this.OnEnter(stateContext, scriptInterface);
  }

  public final func OnEnterFromDoubleJump(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SendLadderEnterStyleToGraph(scriptInterface, 2);
    this.OnEnter(stateContext, scriptInterface);
  }

  public final func OnEnterFromChargeJump(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SendLadderEnterStyleToGraph(scriptInterface, 3);
    this.OnEnter(stateContext, scriptInterface);
  }

  public final func OnEnterFromDodgeAir(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SendLadderEnterStyleToGraph(scriptInterface, 4);
    this.OnEnter(stateContext, scriptInterface);
  }

  public final func OnEnterFromFall(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SendLadderEnterStyleToGraph(scriptInterface, 5);
    this.OnEnter(stateContext, scriptInterface);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let detailedLocomotionState: Int32;
    let ladderCameraParams: LadderCameraParams;
    let ladderEntryDuration: StateResultFloat;
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    detailedLocomotionState = scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LocomotionDetailed);
    if detailedLocomotionState == 13 {
      return;
    };
    ladderEntryDuration = stateContext.GetPermanentFloatParameter(n"ladderEntryDuration");
    if ladderEntryDuration.valid {
      if this.GetInStateTime() > ladderEntryDuration.value {
        stateContext.RemovePermanentFloatParameter(n"ladderEntryDuration");
        this.UseLadderCameraParams(stateContext, scriptInterface, LadderCameraParams.Default);
      };
    } else {
      ladderCameraParams = IntEnum<LadderCameraParams>(scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LadderCameraParams));
      if this.IsPlayerAboveLadderTop(stateContext, scriptInterface) {
        this.UseLadderCameraParams(stateContext, scriptInterface, LadderCameraParams.Exit);
      } else {
        if Equals(ladderCameraParams, LadderCameraParams.CameraReset) {
          if this.IsPlayerLookingDirectlyAtLadder(stateContext, scriptInterface) {
            this.UseLadderCameraParams(stateContext, scriptInterface, LadderCameraParams.Default);
          };
        } else {
          if Equals(ladderCameraParams, LadderCameraParams.Default) {
            if this.ShouldResetCamera(stateContext, scriptInterface) {
              this.UseLadderCameraParams(stateContext, scriptInterface, LadderCameraParams.CameraReset);
            };
          };
        };
      };
    };
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    this.CleanUpLadderState(stateContext, scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.HighLevel) == 6 {
      return;
    };
    this.CleanUpLadderState(stateContext, scriptInterface);
    super.OnForcedExit(stateContext, scriptInterface);
  }

  protected final func OnExitToStand(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let direction: Vector4;
    let impulse: Vector4 = direction * this.GetStaticFloatParameterDefault("exitToStandPushMagnitude", 3.00);
    this.OnExit(stateContext, scriptInterface);
    this.AddImpulse(stateContext, impulse);
    this.CleanUpLadderState(stateContext, scriptInterface);
  }

  protected final func OnExitToLadderSprint(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void;

  protected final func OnExitToLadderSlide(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void;

  protected final func OnExitToLadderJump(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExit(stateContext, scriptInterface);
    this.CleanUpLadderState(stateContext, scriptInterface);
  }

  protected final func OnExitToLadderCrouch(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExit(stateContext, scriptInterface);
    this.CleanUpLadderState(stateContext, scriptInterface);
  }

  protected final func OnExitToKnockdown(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExit(stateContext, scriptInterface);
    this.CleanUpLadderState(stateContext, scriptInterface);
  }

  protected final func OnExitToStunned(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExit(stateContext, scriptInterface);
    this.CleanUpLadderState(stateContext, scriptInterface);
  }

  private final const func SendLadderEnterStyleToGraph(scriptInterface: ref<StateGameScriptInterface>, enterStyle: Int32) -> Void {
    let animFeature: ref<AnimFeature_LadderEnterStyleData> = new AnimFeature_LadderEnterStyleData();
    animFeature.enterStyle = enterStyle;
    scriptInterface.SetAnimationParameterFeature(n"LadderEnterStyleData", animFeature);
  }

  private final const func SetLadderEntryDuration(stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let ladderEntryDuration: Float;
    let isActionEnterLadder: StateResultBool = stateContext.GetTemporaryBoolParameter(n"actionEnterLadder");
    if isActionEnterLadder.valid && isActionEnterLadder.value || this.IsPlayerAboveLadderTop(stateContext, scriptInterface) {
      ladderEntryDuration = this.GetStaticFloatParameterDefault("ladderEntryDurationFromTop", 0.00);
    } else {
      ladderEntryDuration = this.GetStaticFloatParameterDefault("ladderEntryDuration", 0.00);
    };
    stateContext.SetPermanentFloatParameter(n"ladderEntryDuration", ladderEntryDuration, true);
  }

  private final func CleanUpLadderState(stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SendLadderEnterStyleToGraph(scriptInterface, 0);
    this.UseLadderCameraParams(stateContext, scriptInterface, LadderCameraParams.None);
    stateContext.RemovePermanentFloatParameter(n"ladderEntryDuration");
    stateContext.RemoveConditionScriptableParameter(n"usingLadder");
  }

  private final const func IsPlayerLookingDirectlyAtLadder(stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let cameraForwardXY: Vector4;
    let cameraToLadderAngle: Float;
    let ladderNormalXY: Vector4 = stateContext.GetConditionScriptableParameter(n"usingLadder") as LadderDescription.normal;
    ladderNormalXY.Z = 0.00;
    Vector4.Normalize(ladderNormalXY);
    cameraForwardXY = Transform.GetForward(scriptInterface.GetCameraWorldTransform());
    cameraForwardXY.Z = 0.00;
    Vector4.Normalize(cameraForwardXY);
    cameraToLadderAngle = Rad2Deg(AcosF(Vector4.Dot(cameraForwardXY, ladderNormalXY)));
    if 180.00 - cameraToLadderAngle > 2.00 {
      return false;
    };
    return true;
  }

  private final func UseLadderCameraParams(stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>, const params: LadderCameraParams) -> Void {
    let paramsName: CName = n"None";
    let ladderCameraParams: LadderCameraParams = IntEnum<LadderCameraParams>(scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LadderCameraParams));
    if Equals(ladderCameraParams, params) {
      return;
    };
    switch params {
      case LadderCameraParams.Enter:
        paramsName = n"LadderEnter";
        break;
      case LadderCameraParams.Default:
        paramsName = n"LadderDefault";
        break;
      case LadderCameraParams.CameraReset:
        paramsName = n"LadderCameraReset";
        break;
      case LadderCameraParams.Exit:
        paramsName = n"LadderExit";
    };
    stateContext.SetPermanentCNameParameter(n"LadderCameraParams", paramsName, true);
    scriptInterface.localBlackboard.SetInt(GetAllBlackboardDefs().PlayerStateMachine.LadderCameraParams, EnumInt(params));
    this.UpdateCameraParams(stateContext, scriptInterface);
  }

  private final const func ShouldResetCamera(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let cameraWorldTransform: Transform;
    let geometryDescription: ref<GeometryDescriptionQuery>;
    let geometryDescriptionResult: ref<GeometryDescriptionResult>;
    let staticQueryFilter: QueryFilter;
    if !scriptInterface.IsMoveInputConsiderable() {
      return false;
    };
    if DefaultTransition.GetMovementInputActionValue(stateContext, scriptInterface) < this.GetStaticFloatParameterDefault("minStickInputToSwapToClimbCamera", 0.00) {
      return false;
    };
    if AbsF(this.GetVerticalSpeed(scriptInterface)) <= 0.01 {
      return false;
    };
    cameraWorldTransform = scriptInterface.GetCameraWorldTransform();
    QueryFilter.AddGroup(staticQueryFilter, n"Interaction");
    geometryDescription = new GeometryDescriptionQuery();
    geometryDescription.refPosition = Transform.GetPosition(cameraWorldTransform);
    geometryDescription.refDirection = Transform.GetForward(cameraWorldTransform);
    geometryDescription.filter = staticQueryFilter;
    geometryDescription.primitiveDimension = new Vector4(0.10, 0.10, 0.10, 0.00);
    geometryDescription.maxDistance = 2.00;
    geometryDescription.maxExtent = 0.50;
    geometryDescription.probingPrecision = 0.05;
    geometryDescription.probingMaxDistanceDiff = 2.00;
    geometryDescription.AddFlag(worldgeometryDescriptionQueryFlags.DistanceVector);
    geometryDescriptionResult = scriptInterface.GetSpatialQueriesSystem().GetGeometryDescriptionSystem().QueryExtents(geometryDescription);
    if NotEquals(geometryDescriptionResult.queryStatus, worldgeometryDescriptionQueryStatus.NoGeometry) {
      return false;
    };
    return true;
  }
}

public class LadderSprintDecisions extends LadderDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.GetVerticalSpeed(scriptInterface) <= 0.00 {
      return false;
    };
    return this.CommonEnterCondition(stateContext, scriptInterface);
  }

  protected final const func ToLadder(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.CommonToLadder(stateContext, scriptInterface, this.GetVerticalSpeed(scriptInterface) > 0.00);
  }
}

public class LadderSprintEvents extends LadderEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.LadderSprint);
    this.SetLocomotionParameters(stateContext, scriptInterface);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    this.ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
  }

  protected final func OnExitToLadder(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void;
}

public class LadderSlideDecisions extends LadderDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.GetVerticalSpeed(scriptInterface) >= 0.00 {
      return false;
    };
    return this.CommonEnterCondition(stateContext, scriptInterface);
  }

  protected final const func ToLadder(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.CommonToLadder(stateContext, scriptInterface, this.GetVerticalSpeed(scriptInterface) < 0.00);
  }
}

public class LadderSlideEvents extends LadderEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.LadderSlide);
    this.SetLocomotionParameters(stateContext, scriptInterface);
  }

  protected final func OnExitToLadder(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void;
}

public class LadderJumpEvents extends LocomotionAirEvents {

  protected func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let camerForwardXY: Vector4;
    let cameraAngle: Float;
    let jumpDirection: Vector4;
    let ownerForwardXY: Vector4;
    let ownerRightXY: Vector4;
    let ownerTransform: Transform = scriptInterface.GetOwnerTransform();
    super.OnEnter(stateContext, scriptInterface);
    camerForwardXY = Transform.GetForward(scriptInterface.GetCameraWorldTransform());
    camerForwardXY.Z = 0.00;
    camerForwardXY = Vector4.Normalize(camerForwardXY);
    ownerForwardXY = Transform.GetForward(ownerTransform);
    ownerForwardXY.Z = 0.00;
    ownerForwardXY = Vector4.Normalize(ownerForwardXY);
    cameraAngle = Rad2Deg(AcosF(Vector4.Dot(camerForwardXY, ownerForwardXY)));
    if cameraAngle < this.GetStaticFloatParameterDefault("angleToleranceForLateralJump", 30.00) {
      jumpDirection = -ownerForwardXY;
    } else {
      if cameraAngle < 90.00 {
        ownerRightXY = Transform.GetRight(ownerTransform);
        ownerRightXY.Z = 0.00;
        ownerRightXY = Vector4.Normalize(ownerRightXY);
        if Vector4.Dot(camerForwardXY, ownerRightXY) > 0.00 {
          jumpDirection = ownerRightXY;
        } else {
          jumpDirection = -ownerRightXY;
        };
      } else {
        jumpDirection = camerForwardXY;
      };
    };
    jumpDirection.Z = SinF(Deg2Rad(this.GetStaticFloatParameterDefault("pitchAngle", 45.00)));
    jumpDirection = Vector4.Normalize(jumpDirection);
    this.AddImpulse(stateContext, jumpDirection * this.GetStaticFloatParameterDefault("impulseStrength", 4.00));
    stateContext.SetTemporaryBoolParameter(n"finishedLadderAction", true, true);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.LadderJump);
  }
}

public abstract class LocomotionAirDecisions extends LocomotionTransition {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return true;
  }

  protected final const func ShouldFall(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let regularLandingFallingSpeed: Float;
    let verticalSpeed: Float;
    if this.IsTouchingGround(scriptInterface) {
      return false;
    };
    this.IsTouchingGround(scriptInterface);
    if scriptInterface.IsOnMovingPlatform() {
      return false;
    };
    if stateContext.GetBoolParameter(n"isAttacking", true) {
      return true;
    };
    regularLandingFallingSpeed = this.GetFallingSpeedBasedOnHeight(scriptInterface, this.GetStaticFloatParameterDefault("regularLandingHeight", 0.10));
    verticalSpeed = this.GetVerticalSpeed(scriptInterface);
    return verticalSpeed < regularLandingFallingSpeed;
  }

  protected const func ToRegularLand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let landingType: Int32 = this.GetLandingType(stateContext);
    if !this.IsTouchingGround(scriptInterface) || this.GetVerticalSpeed(scriptInterface) > 0.00 {
      return false;
    };
    if landingType <= 1 {
      return true;
    };
    return false;
  }

  protected final const func ToHardLand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let landingType: Int32;
    if !this.IsTouchingGround(scriptInterface) || this.GetVerticalSpeed(scriptInterface) > 0.00 {
      return false;
    };
    landingType = this.GetLandingType(stateContext);
    if landingType == 2 {
      return true;
    };
    return false;
  }

  protected final const func ToVeryHardLand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let landingType: Int32;
    if !this.IsTouchingGround(scriptInterface) {
      return false;
    };
    landingType = this.GetLandingType(stateContext);
    if landingType == 3 {
      return true;
    };
    return false;
  }

  protected final const func ToSuperheroLand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.IsTouchingGround(scriptInterface) {
      return true;
    };
    return false;
  }

  protected const func ToDeathLand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let landingType: Int32;
    if !this.IsTouchingGround(scriptInterface) {
      return false;
    };
    landingType = this.GetLandingType(stateContext);
    if landingType == 5 {
      return true;
    };
    return false;
  }
}

public abstract class LocomotionAirEvents extends LocomotionEventsTransition {

  @default(LocomotionAirEvents, false)
  public let m_maxSuperheroFallHeight: Bool;

  @default(AirThrustersEvents, false)
  @default(DodgeAirEvents, false)
  @default(LocomotionAirEvents, true)
  public let m_updateInputToggles: Bool;

  @default(LocomotionAirEvents, false)
  public let m_resetFallingParametersOnExit: Bool;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let animFeature: ref<AnimFeature_PlayerLocomotionStateMachine>;
    let deathLandingFallingSpeed: Float;
    let hardLandingFallingSpeed: Float;
    let regularLandingFallingSpeed: Float;
    let safeLandingFallingSpeed: Float;
    let veryHardLandingFallingSpeed: Float;
    super.OnEnter(stateContext, scriptInterface);
    regularLandingFallingSpeed = this.GetFallingSpeedBasedOnHeight(scriptInterface, this.GetStaticFloatParameterDefault("regularLandingHeight", 0.10));
    safeLandingFallingSpeed = this.GetFallingSpeedBasedOnHeight(scriptInterface, this.GetStaticFloatParameterDefault("safeLandingHeight", 0.10));
    hardLandingFallingSpeed = this.GetFallingSpeedBasedOnHeight(scriptInterface, this.GetStaticFloatParameterDefault("hardLandingHeight", 1.00));
    veryHardLandingFallingSpeed = this.GetFallingSpeedBasedOnHeight(scriptInterface, this.GetStaticFloatParameterDefault("veryHardLandingHeight", 1.00));
    deathLandingFallingSpeed = this.GetFallingSpeedBasedOnHeight(scriptInterface, this.GetStaticFloatParameterDefault("deathLanding", 1.00));
    stateContext.SetPermanentFloatParameter(n"RegularLandingFallingSpeed", regularLandingFallingSpeed, true);
    stateContext.SetPermanentFloatParameter(n"SafeLandingFallingSpeed", safeLandingFallingSpeed, true);
    stateContext.SetPermanentFloatParameter(n"HardLandingFallingSpeed", hardLandingFallingSpeed, true);
    stateContext.SetPermanentFloatParameter(n"VeryHardLandingFallingSpeed", veryHardLandingFallingSpeed, true);
    stateContext.SetPermanentFloatParameter(n"DeathLandingFallingSpeed", deathLandingFallingSpeed, true);
    animFeature = new AnimFeature_PlayerLocomotionStateMachine();
    animFeature.inAirState = true;
    scriptInterface.SetAnimationParameterFeature(n"LocomotionStateMachine", animFeature);
    scriptInterface.PushAnimationEvent(n"InAir");
    scriptInterface.GetTargetingSystem().SetIsMovingFast(scriptInterface.owner, true);
    this.m_maxSuperheroFallHeight = false;
    this.m_resetFallingParametersOnExit = false;
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let deathLandingFallingSpeed: Float;
    let hardLandingFallingSpeed: Float;
    let horizontalSpeed: Float;
    let isInSuperheroFall: Bool;
    let isLeaping: Bool;
    let landingAnimFeature: ref<AnimFeature_Landing>;
    let landingType: LandingType;
    let maxAllowedDistanceToGround: Float;
    let playerVelocity: Vector4;
    let regularLandingFallingSpeed: Float;
    let safeLandingFallingSpeed: Float;
    let verticalSpeed: Float;
    let veryHardLandingFallingSpeed: Float;
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    if this.IsTouchingGround(scriptInterface) {
      this.m_resetFallingParametersOnExit = true;
      return;
    };
    this.m_resetFallingParametersOnExit = false;
    verticalSpeed = this.GetVerticalSpeed(scriptInterface);
    if this.m_updateInputToggles && verticalSpeed < this.GetFallingSpeedBasedOnHeight(scriptInterface, this.GetStaticFloatParameterDefault("minFallHeightToConsiderInputToggles", 0.00)) {
      this.UpdateInputToggles(stateContext, scriptInterface);
    };
    if scriptInterface.IsActionJustPressed(n"Jump") {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
      return;
    };
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.BerserkPlayerBuff") && verticalSpeed < this.GetFallingSpeedBasedOnHeight(scriptInterface, this.GetStaticFloatParameterDefault("minFallHeightToEnterSuperheroFall", 0.00)) {
      stateContext.SetTemporaryBoolParameter(n"requestSuperheroLandActivation", true, true);
    };
    regularLandingFallingSpeed = stateContext.GetFloatParameter(n"RegularLandingFallingSpeed", true);
    safeLandingFallingSpeed = stateContext.GetFloatParameter(n"SafeLandingFallingSpeed", true);
    hardLandingFallingSpeed = stateContext.GetFloatParameter(n"HardLandingFallingSpeed", true);
    veryHardLandingFallingSpeed = stateContext.GetFloatParameter(n"VeryHardLandingFallingSpeed", true);
    deathLandingFallingSpeed = stateContext.GetFloatParameter(n"DeathLandingFallingSpeed", true);
    isLeaping = scriptInterface.localBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.MeleeLeap) || StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.PlayerMadDashLocomotionBuffer");
    isInSuperheroFall = stateContext.IsStateActive(n"Locomotion", n"superheroFall");
    maxAllowedDistanceToGround = this.GetStaticFloatParameterDefault("maxDistToGroundFromSuperheroFall", 20.00);
    if isInSuperheroFall && !this.m_maxSuperheroFallHeight {
      this.StartEffect(scriptInterface, n"falling");
      this.PlaySound(n"lcm_falling_wind_loop", scriptInterface);
      if DefaultTransition.GetDistanceToGround(scriptInterface) >= maxAllowedDistanceToGround {
        this.m_maxSuperheroFallHeight = true;
        return;
      };
      landingType = LandingType.Superhero;
    } else {
      if isLeaping {
        return;
      };
      if verticalSpeed <= deathLandingFallingSpeed {
        landingType = LandingType.Death;
        this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, 5);
      } else {
        if verticalSpeed <= veryHardLandingFallingSpeed {
          landingType = LandingType.VeryHard;
          this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, 4);
        } else {
          if verticalSpeed <= hardLandingFallingSpeed {
            landingType = LandingType.Hard;
            if this.GetLandingType(stateContext) != 2 {
              this.StartEffect(scriptInterface, n"falling");
            };
            this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, 3);
          } else {
            if verticalSpeed <= safeLandingFallingSpeed {
              landingType = LandingType.Regular;
              this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, 1);
              playerVelocity = DefaultTransition.GetLinearVelocity(scriptInterface);
              horizontalSpeed = Vector4.Length2D(playerVelocity);
              if horizontalSpeed <= this.GetStaticFloatParameterDefault("maxHorizontalSpeedToAerialTakedown", 0.00) {
                this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, 2);
              };
            } else {
              if verticalSpeed <= regularLandingFallingSpeed {
                if this.GetLandingType(stateContext) != 1 {
                  this.PlaySound(n"lcm_falling_wind_loop", scriptInterface);
                };
                landingType = LandingType.Regular;
              } else {
                landingType = LandingType.Off;
              };
            };
          };
        };
      };
    };
    stateContext.SetPermanentIntParameter(n"LandingType", EnumInt(landingType), true);
    stateContext.SetPermanentFloatParameter(n"ImpactSpeed", verticalSpeed, true);
    stateContext.SetPermanentFloatParameter(n"InAirDuration", this.GetInStateTime(), true);
    landingAnimFeature = new AnimFeature_Landing();
    landingAnimFeature.impactSpeed = verticalSpeed;
    landingAnimFeature.type = EnumInt(landingType);
    scriptInterface.SetAnimationParameterFeature(n"Landing", landingAnimFeature);
    this.SetAudioParameter(n"RTPC_Vertical_Velocity", verticalSpeed, scriptInterface);
    this.SetAudioParameter(n"RTPC_Landing_Type", Cast<Float>(EnumInt(landingType)), scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if this.m_resetFallingParametersOnExit {
      this.ResetFallingParameters(stateContext);
    };
    super.OnExit(stateContext, scriptInterface);
    this.StopEffect(scriptInterface, n"falling");
    this.PlaySound(n"lcm_falling_wind_loop_end", scriptInterface);
    scriptInterface.GetTargetingSystem().SetIsMovingFast(scriptInterface.owner, false);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if this.m_resetFallingParametersOnExit {
      this.ResetFallingParameters(stateContext);
    };
    super.OnForcedExit(stateContext, scriptInterface);
  }

  protected final func OnExitToRegularLand(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_resetFallingParametersOnExit = false;
    this.OnExit(stateContext, scriptInterface);
  }

  protected final func OnExitToHardLand(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_resetFallingParametersOnExit = false;
    this.OnExit(stateContext, scriptInterface);
  }

  protected final func OnExitToVeryHardLand(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_resetFallingParametersOnExit = false;
    this.OnExit(stateContext, scriptInterface);
  }

  protected final func OnExitToSuperheroLand(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_resetFallingParametersOnExit = false;
    this.OnExit(stateContext, scriptInterface);
  }

  protected final func OnExitToDeathLand(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_resetFallingParametersOnExit = false;
    this.OnExit(stateContext, scriptInterface);
  }

  protected final func OnExitToUnsecureFootingFall(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    switch IntEnum<LandingType>(this.GetLandingType(stateContext)) {
      case LandingType.Hard:
        this.PlayHardLandingEffects(stateContext, scriptInterface);
        break;
      case LandingType.VeryHard:
        this.PlayVeryHardLandingEffects(stateContext, scriptInterface);
        break;
      case LandingType.Death:
        this.PlayDeathLandingEffects(stateContext, scriptInterface);
    };
    this.OnExit(stateContext, scriptInterface);
  }
}

public class FallDecisions extends LocomotionAirDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let shouldFall: Bool = this.ShouldFall(stateContext, scriptInterface);
    if shouldFall {
      scriptInterface.GetAudioSystem().NotifyGameTone(n"StartFalling");
    };
    return shouldFall;
  }
}

public class FallEvents extends LocomotionAirEvents {

  public final func OnEnterFromDodge(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if !this.IsTouchingGround(scriptInterface) {
      stateContext.SetPermanentBoolParameter(n"enteredFallFromAirDodge", true, true);
      this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Fall, 1);
    };
    this.OnEnter(stateContext, scriptInterface);
  }

  public final func OnEnterFromDodgeAir(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    stateContext.SetPermanentBoolParameter(n"enteredFallFromAirDodge", true, true);
    this.OnEnter(stateContext, scriptInterface);
  }

  protected func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    this.PlaySound(n"lcm_falling_wind_loop", scriptInterface);
    scriptInterface.PushAnimationEvent(n"Fall");
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Fall);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    this.RemoveOpticalCamoEffect(stateContext, scriptInterface, 14, 1.00);
  }
}

public class UnsecureFootingFallDecisions extends FallDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let linearVelocity: Vector4;
    let secureFootingResult: SecureFootingResult;
    if this.IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface) {
      return false;
    };
    secureFootingResult = scriptInterface.HasSecureFooting();
    linearVelocity = DefaultTransition.GetLinearVelocity(scriptInterface);
    return Equals(secureFootingResult.type, moveSecureFootingFailureType.Edge) && linearVelocity.Z < this.GetStaticFloatParameterDefault("minVerticalVelocityToEnter", -0.30);
  }
}

public class UnsecureFootingFallEvents extends FallEvents {

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
  }

  protected func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
  }
}

public class AirThrustersDecisions extends LocomotionAirDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let autoActivationNearGround: Bool;
    let minInputHoldTime: Float;
    let shouldFall: Bool = this.ShouldFall(stateContext, scriptInterface);
    if shouldFall {
      scriptInterface.GetAudioSystem().NotifyGameTone(n"StartFalling");
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.HasAirThrusters) && !this.GetStaticBoolParameterDefault("debug_Enable_Air_Thrusters", false) {
      return false;
    };
    minInputHoldTime = this.GetStaticFloatParameterDefault("minInputHoldTime", 0.15);
    if scriptInterface.GetActionValue(n"Jump") > 0.00 && scriptInterface.GetActionStateTime(n"Jump") > minInputHoldTime {
      return DefaultTransition.GetDistanceToGround(scriptInterface) >= this.GetStaticFloatParameterDefault("minDistanceToGround", 0.00);
    };
    autoActivationNearGround = this.GetStaticBoolParameterDefault("autoActivationAboutToHitGround", true);
    if autoActivationNearGround && this.IsFallHeightAcceptable(stateContext, scriptInterface) {
      return DefaultTransition.GetDistanceToGround(scriptInterface) <= this.GetStaticFloatParameterDefault("autoActivationDistanceToGround", 0.00);
    };
    return false;
  }

  protected final const func IsFallHeightAcceptable(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let acceptableFallingSpeed: Float = this.GetFallingSpeedBasedOnHeight(scriptInterface, this.GetStaticFloatParameterDefault("minFallHeight", 3.00));
    let verticalSpeed: Float = this.GetVerticalSpeed(scriptInterface);
    if verticalSpeed <= acceptableFallingSpeed {
      return true;
    };
    return false;
  }

  protected final const func ToFall(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !scriptInterface.HasStatFlag(gamedataStatType.HasAirThrusters) {
      return true;
    };
    if this.GetStaticBoolParameterDefault("autoTransitionToFall", true) && this.GetInStateTime() >= this.GetStaticFloatParameterDefault("stateDuration", 0.00) {
      return true;
    };
    if !this.GetStaticBoolParameterDefault("autoTransitionToFall", true) && this.GetInStateTime() >= this.GetStaticFloatParameterDefault("stateDuration", 0.00) && (scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustPressed(n"Crouch")) {
      return true;
    };
    if this.GetStaticBoolParameterDefault("allowCancelingWithCrouchAction", true) && scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustHeld(n"ToggleCrouch") || scriptInterface.IsActionJustPressed(n"Crouch") {
      return true;
    };
    if DefaultTransition.GetDistanceToGround(scriptInterface) <= this.GetStaticFloatParameterDefault("minDistanceToGround", 0.00) {
      return true;
    };
    return false;
  }

  protected final const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.IsTouchingGround(scriptInterface) {
      return true;
    };
    return false;
  }

  protected final const func ToDoubleJump(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !scriptInterface.HasStatFlag(gamedataStatType.HasDoubleJump) {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HealFood3") {
      return false;
    };
    if stateContext.GetIntParameter(n"currentNumberOfJumps", true) >= 2 {
      return false;
    };
    if stateContext.GetConditionBool(n"JumpPressed") || scriptInterface.IsActionJustPressed(n"Jump") {
      return true;
    };
    return false;
  }
}

public class AirThrustersEvents extends LocomotionAirEvents {

  protected func SendAnimFeatureDataToGraph(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, state: Int32) -> Void {
    let animFeature: ref<AnimFeature_AirThrusterData> = new AnimFeature_AirThrusterData();
    animFeature.state = state;
    scriptInterface.SetAnimationParameterFeature(n"AirThrusterData", animFeature);
  }

  protected func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    this.SendAnimFeatureDataToGraph(stateContext, scriptInterface, 1);
    scriptInterface.SetAnimationParameterFloat(n"crouch", 0.00);
    stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
    this.StopEffect(scriptInterface, n"falling");
    this.PlaySound(n"q115_thruster_start", scriptInterface);
    this.PlayEffectOnItem(scriptInterface, n"thrusters");
    this.SetUpwardsThrustStats(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.AirThrusters);
    DefaultTransition.PlayRumbleLoop(scriptInterface, "light");
  }

  private final func GetActiveFeetAreaItem(scriptInterface: ref<StateGameScriptInterface>) -> ref<ItemObject> {
    let es: ref<EquipmentSystem> = scriptInterface.GetScriptableSystem(n"EquipmentSystem") as EquipmentSystem;
    let feetItem: ref<ItemObject> = es.GetActiveWeaponObject(scriptInterface.executionOwner, gamedataEquipmentArea.Feet);
    return feetItem;
  }

  private final func PlayEffectOnItem(scriptInterface: ref<StateGameScriptInterface>, effectName: CName) -> Void {
    let spawnEffectEvent: ref<entSpawnEffectEvent>;
    if IsDefined(this.GetActiveFeetAreaItem(scriptInterface)) {
      spawnEffectEvent = new entSpawnEffectEvent();
      spawnEffectEvent.effectName = effectName;
      this.GetActiveFeetAreaItem(scriptInterface).GetOwner().QueueEvent(spawnEffectEvent);
    };
  }

  protected final func StopEffectOnItem(scriptInterface: ref<StateGameScriptInterface>, effectName: CName) -> Void {
    let killEffectEvent: ref<entKillEffectEvent>;
    if IsDefined(this.GetActiveFeetAreaItem(scriptInterface)) {
      killEffectEvent = new entKillEffectEvent();
      killEffectEvent.effectName = effectName;
      this.GetActiveFeetAreaItem(scriptInterface).GetOwner().QueueEvent(killEffectEvent);
    };
  }

  private final func SetUpwardsThrustStats(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let locomotionParameters: ref<LocomotionParameters> = new LocomotionParameters();
    this.SetModifierGroupForState(scriptInterface);
    this.GetStateDefaultLocomotionParameters(locomotionParameters);
    locomotionParameters.SetUpwardsGravity(this.GetStaticFloatParameterDefault("upwardsGravity", -16.00));
    locomotionParameters.SetDownwardsGravity(this.GetStaticFloatParameterDefault("downwardsGravity", -4.00));
    locomotionParameters.SetDoJump(true);
    stateContext.SetTemporaryScriptableParameter(n"locomotionParameters", locomotionParameters, true);
  }

  protected func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    this.SendAnimFeatureDataToGraph(stateContext, scriptInterface, 0);
    this.PlaySound(n"q115_thruster_stop", scriptInterface);
    this.StopEffectOnItem(scriptInterface, n"thrusters");
    DefaultTransition.StopRumbleLoop(scriptInterface, "light");
  }
}

public class AirHoverDecisions extends LocomotionAirDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let shouldEnter: StateResultBool;
    let shouldFall: Bool = this.ShouldFall(stateContext, scriptInterface);
    if shouldFall {
      scriptInterface.GetAudioSystem().NotifyGameTone(n"StartFalling");
    };
    shouldEnter = stateContext.GetTemporaryBoolParameter(n"groundSlam");
    if shouldEnter.value {
      return true;
    };
    return false;
  }

  protected final const func ToSuperheroFall(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.GetStaticBoolParameterDefault("autoTransitionToSuperheroFall", true) && this.GetInStateTime() >= this.GetStaticFloatParameterDefault("maxAirHoverTime", 0.00) {
      return true;
    };
    if !this.GetStaticBoolParameterDefault("autoTransitionToSuperheroFall", true) && this.GetInStateTime() >= this.GetStaticFloatParameterDefault("maxAirHoverTime", 0.00) && (scriptInterface.IsActionJustTapped(n"ToggleCrouch") || scriptInterface.IsActionJustHeld(n"ToggleCrouch") || scriptInterface.IsActionJustPressed(n"Crouch")) {
      return true;
    };
    if scriptInterface.IsOnGround() {
      return true;
    };
    return false;
  }
}

public class AirHoverEvents extends LocomotionAirEvents {

  protected final func SetDeathFallHeight(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, verticalSpeed: Float) -> Void {
    let deathLandingHeight: Float;
    let currentPosition: Vector4 = DefaultTransition.GetPlayerPosition(scriptInterface);
    if verticalSpeed < 0.00 {
      currentPosition.Z += this.GetInitialHeightBasedOnFallingSpeed(scriptInterface, verticalSpeed);
    };
    deathLandingHeight = currentPosition.Z - this.GetStaticFloatParameterDefault("deathLanding", 1.00);
    stateContext.SetPermanentFloatParameter(n"DeathLandingHeight", deathLandingHeight, true);
  }

  protected func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let verticalSpeed: Float;
    this.SendSuperHeroLandAnimFeature(scriptInterface, 1);
    if scriptInterface.IsOnGround() {
      this.SetDeathFallHeight(stateContext, scriptInterface, 0.00);
      stateContext.SetPermanentBoolParameter(n"groundedGroundSlam", true, true);
      return;
    };
    super.OnEnter(stateContext, scriptInterface);
    verticalSpeed = this.GetVerticalSpeed(scriptInterface);
    this.SetDeathFallHeight(stateContext, scriptInterface, verticalSpeed);
    scriptInterface.PushAnimationEvent(n"AirHover");
    this.PlaySound(n"lcm_wallrun_out", scriptInterface);
    this.AddUpwardsImpulse(stateContext, scriptInterface, verticalSpeed);
    stateContext.SetPermanentBoolParameter(n"groundedGroundSlam", false, true);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.AirHover);
  }

  private final func AddUpwardsImpulse(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, verticalSpeed: Float) -> Void {
    let verticalImpulse: Float;
    if verticalSpeed <= -0.50 {
      verticalImpulse = this.GetStaticFloatParameterDefault("verticalUpwardsImpulse", 4.00);
      this.AddVerticalImpulse(stateContext, verticalImpulse);
    };
  }

  protected func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
  }

  protected func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    this.SendSuperHeroLandAnimFeature(scriptInterface, 0);
  }
}

public class SuperheroFallDecisions extends LocomotionAirDecisions {

  protected const func ToDeathLand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let deathLandingHeight: StateResultFloat;
    let playerPosition: Vector4;
    if !this.IsTouchingGround(scriptInterface) {
      return false;
    };
    deathLandingHeight = stateContext.GetPermanentFloatParameter(n"DeathLandingHeight");
    playerPosition = DefaultTransition.GetPlayerPosition(scriptInterface);
    if deathLandingHeight.valid {
      if deathLandingHeight.value > playerPosition.Z {
        return true;
      };
    } else {
      return true;
    };
    return false;
  }
}

public class SuperheroFallEvents extends LocomotionAirEvents {

  protected func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let grounded: StateResultBool;
    this.SendSuperHeroLandAnimFeature(scriptInterface, 2);
    grounded = stateContext.GetPermanentBoolParameter(n"groundedGroundSlam");
    if grounded.value {
      return;
    };
    super.OnEnter(stateContext, scriptInterface);
    this.PlaySound(n"Player_double_jump", scriptInterface);
    scriptInterface.PushAnimationEvent(n"SuperHeroFall");
    this.AddVerticalImpulse(stateContext, this.GetStaticFloatParameterDefault("downwardsImpulseStrength", 0.00));
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.SuperheroFall);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    stateContext.SetTemporaryFloatParameter(n"SuperheroFallTime", this.GetInStateTime(), true);
  }

  protected func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    this.SendSuperHeroLandAnimFeature(scriptInterface, 0);
  }
}

public class JumpDecisions extends LocomotionAirDecisions {

  protected let m_jumpPressed: Bool;

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnAttach(stateContext, scriptInterface);
    scriptInterface.executionOwner.RegisterInputListener(this, n"Jump");
    this.EnableOnEnterCondition(false);
  }

  protected final func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    scriptInterface.executionOwner.UnregisterInputListener(this);
  }

  protected cb func OnAction(action: ListenerAction, consumer: ListenerActionConsumer) -> Bool {
    this.m_jumpPressed = ListenerAction.GetValue(action) > 0.00;
    this.EnableOnEnterCondition(true);
  }

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let disableAirDash: StateResultBool;
    let jumpPressedFlag: Bool = stateContext.GetConditionBool(n"JumpPressed");
    if !jumpPressedFlag && !this.m_jumpPressed {
      this.EnableOnEnterCondition(false);
    };
    if scriptInterface.localBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideElevator) {
      return false;
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.CanJump) {
      return false;
    };
    disableAirDash = stateContext.GetPermanentBoolParameter(n"disableAirDash");
    if !this.IsTouchingGround(scriptInterface) && disableAirDash.valid && disableAirDash.value {
      return false;
    };
    if scriptInterface.HasStatFlag(gamedataStatType.HasChargeJump) || scriptInterface.HasStatFlag(gamedataStatType.HasAirHover) {
      if this.GetActionHoldTime(stateContext, scriptInterface, n"Jump") < 0.15 && stateContext.GetConditionFloat(n"InputHoldTime") < 0.20 && scriptInterface.IsActionJustReleased(n"Jump") {
        return true;
      };
    } else {
      if jumpPressedFlag || scriptInterface.IsActionJustPressed(n"Jump") {
        return true;
      };
    };
    return false;
  }
}

public class JumpEvents extends LocomotionAirEvents {

  protected func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    if !DefaultTransition.IsInRpgContext(scriptInterface) {
      stateContext.SetPermanentBoolParameter(n"VisionToggled", false, true);
    };
    scriptInterface.PushAnimationEvent(n"Jump");
    this.ModifyJumpSpeed(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 5);
    this.ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Jump);
    this.LogSpecialMovementToTelemetry(scriptInterface, telemetryMovementType.Jump);
  }

  private final func ModifyJumpSpeed(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let defaultLocoParams: ref<LocomotionParameters>;
    let defaultUpwardsGravity: Float;
    let jumpSpeedModifier: Float = this.GetStatFloatValue(scriptInterface, gamedataStatType.JumpSpeedModifier, scriptInterface.GetStatsSystem());
    if jumpSpeedModifier > 0.00 && jumpSpeedModifier < 1.00 {
      jumpSpeedModifier = 2.00 - jumpSpeedModifier;
      defaultLocoParams = stateContext.GetTemporaryScriptableParameter(n"locomotionParameters") as LocomotionParameters;
      if IsDefined(defaultLocoParams) {
        defaultUpwardsGravity = this.GetStaticFloatParameterDefault("upwardsGravity", -16.00);
        defaultLocoParams.SetUpwardsGravity(jumpSpeedModifier * defaultLocoParams.GetUpwardsGravity(defaultUpwardsGravity));
      };
    };
  }

  protected func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 0);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    this.RemoveOpticalCamoEffect(stateContext, scriptInterface, 18, 0.50);
  }
}

public class DoubleJumpDecisions extends JumpDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let currentNumberOfJumps: Int32;
    let jumpPressedFlag: Bool = stateContext.GetConditionBool(n"JumpPressed");
    if !jumpPressedFlag && !this.m_jumpPressed {
      this.EnableOnEnterCondition(false);
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.HasDoubleJump) {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HealFood3") {
      return false;
    };
    if scriptInterface.HasStatFlag(gamedataStatType.HasChargeJump) || scriptInterface.HasStatFlag(gamedataStatType.HasAirHover) {
      return false;
    };
    if this.IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface) {
      return false;
    };
    if scriptInterface.localBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideMovingElevator) {
      return false;
    };
    currentNumberOfJumps = stateContext.GetIntParameter(n"currentNumberOfJumps", true);
    if currentNumberOfJumps >= this.GetStaticIntParameterDefault("numberOfMultiJumps", 1) {
      return false;
    };
    if jumpPressedFlag || scriptInterface.IsActionJustPressed(n"Jump") {
      return true;
    };
    return false;
  }
}

public class DoubleJumpEvents extends LocomotionAirEvents {

  public final func OnEnterFromAirThrusters(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    stateContext.SetPermanentIntParameter(n"currentNumberOfJumps", 1, true);
    this.OnEnter(stateContext, scriptInterface);
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let currentNumberOfJumps: Int32;
    super.OnEnter(stateContext, scriptInterface);
    this.ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    currentNumberOfJumps = stateContext.GetIntParameter(n"currentNumberOfJumps", true);
    currentNumberOfJumps += 1;
    stateContext.SetPermanentIntParameter(n"currentNumberOfJumps", currentNumberOfJumps, true);
    this.PlaySound(n"lcm_player_double_jump", scriptInterface);
    DefaultTransition.PlayRumble(scriptInterface, this.GetStaticStringParameterDefault("rumbleOnEnter", "medium_fast"));
    scriptInterface.PushAnimationEvent(n"DoubleJump");
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 5);
    stateContext.SetConditionBoolParameter(n"JumpPressed", false, true);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.DoubleJump);
    this.LogSpecialMovementToTelemetry(scriptInterface, telemetryMovementType.DoubleJump);
    if PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Reflexes_Master_Perk_3) == 1 {
      scriptInterface.executionOwner.QueueEvent(new ReflexesMasterPerk3Triggerd());
    };
  }

  protected func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 0);
  }
}

public class ChargeJumpDecisions extends LocomotionAirDecisions {

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnAttach(stateContext, scriptInterface);
    scriptInterface.executionOwner.RegisterInputListener(this, n"Jump");
  }

  protected final func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    scriptInterface.executionOwner.UnregisterInputListener(this);
  }

  protected cb func OnAction(action: ListenerAction, consumer: ListenerActionConsumer) -> Bool {
    if ListenerAction.IsButtonJustReleased(action) {
      this.EnableOnEnterCondition(true);
    };
  }

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let disableAirDash: StateResultBool = stateContext.GetPermanentBoolParameter(n"disableAirDash");
    this.EnableOnEnterCondition(false);
    if stateContext.GetConditionFloat(n"InputHoldTime") > 0.15 && scriptInterface.IsActionJustReleased(n"Jump") && scriptInterface.HasStatFlag(gamedataStatType.HasChargeJump) && !(disableAirDash.valid && disableAirDash.value) {
      if scriptInterface.HasStatFlag(gamedataStatType.HasAirHover) {
        return false;
      };
      if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HealFood3") {
        return false;
      };
      if this.IsPlayerInAnyMenu(scriptInterface) || this.IsRadialWheelOpen(scriptInterface) {
        return false;
      };
      if this.IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface) {
        return false;
      };
      if scriptInterface.localBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideMovingElevator) {
        return false;
      };
      return true;
    };
    return false;
  }
}

public class ChargeJumpEvents extends LocomotionAirEvents {

  protected func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let inputHoldTime: Float;
    super.OnEnter(stateContext, scriptInterface);
    inputHoldTime = stateContext.GetConditionFloat(n"InputHoldTime");
    scriptInterface.PushAnimationEvent(n"ChargeJump");
    this.PlaySound(n"lcm_player_double_jump", scriptInterface);
    DefaultTransition.PlayRumble(scriptInterface, this.GetStaticStringParameterDefault("rumbleOnEnter", "medium_fast"));
    this.StartEffect(scriptInterface, n"charged_jump");
    this.SpawnLandingFxGameEffect(t"Attacks.PressureWave", scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 5);
    this.SetChargeJumpParameters(stateContext, scriptInterface, inputHoldTime);
    this.ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.ChargeJump);
    this.LogSpecialMovementToTelemetry(scriptInterface, telemetryMovementType.ChargedJump);
  }

  private final func SetChargeJumpParameters(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, inputHoldTime: Float) -> Void {
    let downwardsGravity: Float;
    let nameSuffix: String;
    let upwardsGravity: Float;
    if inputHoldTime >= this.GetStaticFloatParameterDefault("minHoldTime", 0.10) && inputHoldTime <= this.GetStaticFloatParameterDefault("medChargeHoldTime", 0.20) {
      upwardsGravity = this.GetStaticFloatParameterDefault("upwardsGravityMinCharge", -16.00);
      downwardsGravity = this.GetStaticFloatParameterDefault("downwardsGravityMinCharge", -16.00);
      nameSuffix = "Low";
    } else {
      if inputHoldTime > this.GetStaticFloatParameterDefault("medChargeHoldTime", 0.20) && inputHoldTime <= this.GetStaticFloatParameterDefault("maxChargeHoldTime", 0.30) {
        upwardsGravity = this.GetStaticFloatParameterDefault("upwardsGravityMedCharge", -16.00);
        downwardsGravity = this.GetStaticFloatParameterDefault("downwardsGravityMedCharge", -16.00);
        nameSuffix = "Medium";
      } else {
        if inputHoldTime >= this.GetStaticFloatParameterDefault("maxChargeHoldTime", 0.30) {
          upwardsGravity = this.GetStaticFloatParameterDefault("upwardsGravityMaxCharge", -16.00);
          downwardsGravity = this.GetStaticFloatParameterDefault("downwardsGravityMaxCharge", -20.00);
          nameSuffix = "High";
          this.AddVerticalImpulse(stateContext, this.GetStaticFloatParameterDefault("verticalImpulse", 2.00));
        };
      };
    };
    this.UpdateChargeJumpStats(stateContext, scriptInterface, upwardsGravity, downwardsGravity, nameSuffix);
  }

  private final func UpdateChargeJumpStats(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, upwardsGravity: Float, downwardsGravity: Float, const nameSuffix: script_ref<String>) -> Void {
    let locomotionParameters: ref<LocomotionParameters> = new LocomotionParameters();
    let statModifierTDBName: String = this.m_statModifierTDBNameDefault + nameSuffix;
    this.SetModifierGroupForState(scriptInterface, statModifierTDBName);
    this.GetStateDefaultLocomotionParameters(locomotionParameters);
    locomotionParameters.SetUpwardsGravity(upwardsGravity);
    locomotionParameters.SetDownwardsGravity(downwardsGravity);
    locomotionParameters.SetDoJump(true);
    stateContext.SetPermanentBoolParameter(n"isGravityAffectedByChargedJump", true, true);
    stateContext.SetPermanentFloatParameter(n"chargedJumpUpwardsGravity", upwardsGravity, true);
    stateContext.SetPermanentFloatParameter(n"chargedJumpDownwardsGravity", downwardsGravity, true);
    stateContext.SetTemporaryScriptableParameter(n"locomotionParameters", locomotionParameters, true);
  }

  protected func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 0);
  }

  protected final func OnExitToKnockdown(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let currentVelocity: Vector4;
    this.OnExit(stateContext, scriptInterface);
    currentVelocity = DefaultTransition.GetLinearVelocity(scriptInterface);
    if currentVelocity.Z > 0.00 {
      this.AddVerticalImpulse(stateContext, -currentVelocity.Z * 0.50);
    };
  }

  protected final func OnExitToVehicleKnockdown(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitToKnockdown(stateContext, scriptInterface);
  }

  protected final func OnExitToBarbedWireKnockdown(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitToKnockdown(stateContext, scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 0);
  }
}

public class HoverJumpDecisions extends LocomotionAirDecisions {

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnAttach(stateContext, scriptInterface);
    scriptInterface.executionOwner.RegisterInputListener(this, n"Jump");
    this.EnableOnEnterCondition(false);
  }

  protected final func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    scriptInterface.executionOwner.UnregisterInputListener(this);
  }

  protected cb func OnAction(action: ListenerAction, consumer: ListenerActionConsumer) -> Bool {
    this.EnableOnEnterCondition(ListenerAction.GetValue(action) > 0.00);
  }

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.IsActionJustHeld(n"Jump") {
      if !scriptInterface.HasStatFlag(gamedataStatType.HasAirHover) {
        return false;
      };
      if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HealFood3") {
        return false;
      };
      if this.IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface) {
        return false;
      };
      if scriptInterface.localBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideMovingElevator) {
        return false;
      };
      return true;
    };
    return false;
  }
}

public class HoverJumpEvents extends LocomotionAirEvents {

  protected func SendHoverJumpStateToGraph(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, state: Int32) -> Void {
    let animFeature: ref<AnimFeature_HoverJumpData> = new AnimFeature_HoverJumpData();
    animFeature.state = state;
    scriptInterface.SetAnimationParameterFeature(n"HoverJumpData", animFeature);
  }

  protected func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    this.SendHoverJumpStateToGraph(stateContext, scriptInterface, 1);
    this.PlaySound(n"lcm_player_double_jump", scriptInterface);
    DefaultTransition.PlayRumble(scriptInterface, this.GetStaticStringParameterDefault("rumbleOnEnter", "medium_fast"));
    this.StartEffect(scriptInterface, n"charged_jump");
    this.SpawnLandingFxGameEffect(t"Attacks.PressureWave", scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 5);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.HoverJump);
    StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HoverJumpPlayerBuff");
    this.ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let verticalSpeed: Float = this.GetVerticalSpeed(scriptInterface);
    if !stateContext.GetBoolParameter(n"isAboutToLand", true) && verticalSpeed <= -1.00 && DefaultTransition.GetDistanceToGround(scriptInterface) <= 1.00 {
      this.SendHoverJumpStateToGraph(stateContext, scriptInterface, 2);
      stateContext.SetPermanentBoolParameter(n"isAboutToLand", true, true);
    };
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HoverJumpPlayerBuff") {
      if scriptInterface.GetActionValue(n"CameraAim") == 0.00 && scriptInterface.GetActionValue(n"Jump") > 0.00 {
        this.AddUpwardsThrust(stateContext, scriptInterface, this.GetStaticFloatParameterDefault("verticalImpulse", 4.00) * (scriptInterface.owner as TimeDilatable).GetTimeDilationValue());
        if !stateContext.GetBoolParameter(n"isHovering", true) {
          stateContext.RemovePermanentBoolParameter(n"isStabilising");
          stateContext.SetPermanentBoolParameter(n"isHovering", true, true);
          this.UpdateHoverJumpStats(stateContext, scriptInterface, this.GetStaticFloatParameterDefault("upwardsGravityOnThrust", -10.00), this.GetStaticFloatParameterDefault("downwardsGravityOnThrust", -5.00), "");
        };
      } else {
        if this.GetStaticBoolParameterDefault("stabilizeOnAim", false) && scriptInterface.GetActionValue(n"CameraAim") > 0.00 {
          if !stateContext.GetBoolParameter(n"isStabilising", true) {
            if verticalSpeed <= -0.50 {
              this.AddUpwardsThrust(stateContext, scriptInterface, this.GetStaticFloatParameterDefault("verticalImpulseStabilize", 4.00) * (scriptInterface.owner as TimeDilatable).GetTimeDilationValue());
            };
            stateContext.RemovePermanentBoolParameter(n"isHovering");
            stateContext.SetPermanentBoolParameter(n"isStabilising", true, true);
            this.UpdateHoverJumpStats(stateContext, scriptInterface, this.GetStaticFloatParameterDefault("upwardsGravityOnStabilize", -10.00), this.GetStaticFloatParameterDefault("downwardsGravityOnStabilize", -3.00), "Thrust");
            this.PlaySound(n"lcm_wallrun_in", scriptInterface);
          };
        };
      };
    } else {
      this.UpdateHoverJumpStats(stateContext, scriptInterface, this.GetStaticFloatParameterDefault("upwardsGravity", -16.00), this.GetStaticFloatParameterDefault("downwardsGravity", -16.00), "");
    };
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
  }

  private final func UpdateHoverJumpStats(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, upwardsGravity: Float, downwardsGravity: Float, const nameSuffix: script_ref<String>) -> Void {
    let locomotionParameters: ref<LocomotionParameters> = new LocomotionParameters();
    let statModifierTDBName: String = this.m_statModifierTDBNameDefault + nameSuffix;
    this.SetModifierGroupForState(scriptInterface, statModifierTDBName);
    this.GetStateDefaultLocomotionParameters(locomotionParameters);
    locomotionParameters.SetUpwardsGravity(upwardsGravity);
    locomotionParameters.SetDownwardsGravity(downwardsGravity);
    locomotionParameters.SetDoJump(true);
    stateContext.SetTemporaryScriptableParameter(n"locomotionParameters", locomotionParameters, true);
  }

  private final func AddUpwardsThrust(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, verticalImpulse: Float) -> Void {
    if verticalImpulse > 0.00 {
      this.AddVerticalImpulse(stateContext, verticalImpulse);
    };
  }

  protected func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    this.CleanUpOnExit(stateContext, scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    this.CleanUpOnExit(stateContext, scriptInterface);
  }

  private final func CleanUpOnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SendHoverJumpStateToGraph(stateContext, scriptInterface, 0);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 0);
    StatusEffectHelper.RemoveStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HoverJumpPlayerBuff");
    this.UpdateHoverJumpStats(stateContext, scriptInterface, -16.00, -16.00, "");
    stateContext.RemovePermanentBoolParameter(n"isStabilising");
    stateContext.RemovePermanentBoolParameter(n"isHovering");
    stateContext.RemovePermanentBoolParameter(n"isAboutToLand");
  }
}

public class BodySlamJumpEvents extends LocomotionAirEvents {

  protected func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 0);
  }
}

public class DodgeAirDecisions extends LocomotionAirDecisions {

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnAttach(stateContext, scriptInterface);
    scriptInterface.executionOwner.RegisterInputListener(this, n"Dodge");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeDirection");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeForward");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeRight");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeLeft");
    scriptInterface.executionOwner.RegisterInputListener(this, n"DodgeBack");
    this.EnableOnEnterCondition(false);
  }

  protected final func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    scriptInterface.executionOwner.UnregisterInputListener(this);
  }

  protected cb func OnAction(action: ListenerAction, consumer: ListenerActionConsumer) -> Bool {
    if ListenerAction.IsButtonJustPressed(action) {
      this.EnableOnEnterCondition(true);
    };
  }

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let currentNumberOfAirDodges: Int32;
    this.EnableOnEnterCondition(false);
    if !scriptInterface.HasStatFlag(gamedataStatType.HasDodgeAir) {
      return false;
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.CanAimWhileDodging) && this.IsInUpperBodyState(stateContext, n"aimingState") {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.HealFood3") {
      return false;
    };
    if this.IsCurrentFallSpeedTooFastToEnter(stateContext, scriptInterface) {
      return false;
    };
    currentNumberOfAirDodges = stateContext.GetIntParameter(n"currentNumberOfAirDodges", true);
    if currentNumberOfAirDodges >= this.GetStaticIntParameterDefault("numberOfAirDodges", 1) {
      return false;
    };
    if this.WantsToDodge(stateContext, scriptInterface) {
      return true;
    };
    return false;
  }

  protected const func ToFall(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let isKerenzikovEnd: Bool;
    let isKerenzikovStateActive: Bool = Equals(stateContext.GetStateMachineCurrentState(n"TimeDilation"), n"kerenzikov");
    if !StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeAirBuff") {
      return !isKerenzikovStateActive;
    };
    isKerenzikovEnd = isKerenzikovStateActive && !StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.KerenzikovPlayerBuff");
    if isKerenzikovEnd {
      return true;
    };
    return false;
  }
}

public class DodgeAirEvents extends LocomotionAirEvents {

  protected func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let currentNumberOfAirDodges: Int32;
    super.OnEnter(stateContext, scriptInterface);
    currentNumberOfAirDodges = stateContext.GetIntParameter(n"currentNumberOfAirDodges", true);
    currentNumberOfAirDodges += 1;
    stateContext.SetPermanentIntParameter(n"currentNumberOfAirDodges", currentNumberOfAirDodges, true);
    this.Dodge(stateContext, scriptInterface);
    this.PlayRumbleBasedOnDodgeDirection(stateContext, scriptInterface);
    scriptInterface.PushAnimationEvent(n"Dodge");
    stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
    stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
    stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
    StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeAirBuff");
    this.ConsumeStaminaBasedOnLocomotionState(stateContext, scriptInterface);
    this.LogSpecialMovementToTelemetry(scriptInterface, telemetryMovementType.AirDodge);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Locomotion, 8);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if scriptInterface.IsActionJustPressed(n"Jump") {
      stateContext.SetConditionBoolParameter(n"JumpPressed", true, true);
    };
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
  }

  protected func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    StatusEffectHelper.RemoveStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DodgeAirBuff");
  }

  protected final func Dodge(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let impulseValue: Float = this.GetStaticFloatParameterDefault("impulse", 10.00);
    let dodgeHeading: Float = stateContext.GetConditionFloat(n"DodgeDirection");
    let impulse: Vector4 = Vector4.FromHeading(AngleNormalize180(Transform.GetYaw(scriptInterface.GetCameraWorldTransform()) + dodgeHeading)) * impulseValue;
    this.AddImpulse(stateContext, impulse);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.DodgeAir);
  }
}

public abstract class AbstractLandEvents extends LocomotionGroundEvents {

  @default(AbstractLandEvents, false)
  public let m_blockLandingStimBroadcasting: Bool;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let bottomCollisionFound: Bool;
    let bottomCollisionNormal: Vector4;
    let capsuleRadius: Float;
    let collisionIndex: Int32;
    let collisionReport: array<ControllerHit>;
    let hit: ControllerHit;
    let playerPosition: Vector4;
    let playerPositionCentreOfSphere: Vector4;
    let touchNormal: Vector4;
    let up: Vector4 = DefaultTransition.GetUpVector();
    super.OnEnter(stateContext, scriptInterface);
    this.SetAudioParameter(n"RTPC_Landing_Type", 0.00, scriptInterface);
    scriptInterface.PushAnimationEvent(n"Land");
    capsuleRadius = FromVariant<Float>(scriptInterface.GetStateVectorParameter(physicsStateValue.Radius));
    playerPosition = DefaultTransition.GetPlayerPosition(scriptInterface);
    collisionReport = scriptInterface.GetCollisionReport();
    playerPositionCentreOfSphere = playerPosition + up * capsuleRadius;
    bottomCollisionFound = false;
    collisionIndex = 0;
    while collisionIndex < ArraySize(collisionReport) && !bottomCollisionFound {
      hit = collisionReport[collisionIndex];
      touchNormal = Vector4.Normalize(playerPositionCentreOfSphere - hit.worldPos);
      if touchNormal.Z > 0.00 && bottomCollisionNormal.Z < touchNormal.Z {
        bottomCollisionNormal = touchNormal;
        if bottomCollisionNormal.Z < 1.00 {
          bottomCollisionFound = true;
        };
      };
      collisionIndex += 1;
    };
    this.ResetFallingParameters(stateContext);
  }

  protected final func BroadcastLandingStim(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, stimType: gamedataStimType) -> Void {
    let broadcaster: ref<StimBroadcasterComponent>;
    let impactSpeed: StateResultFloat;
    let speedThresholdToSendStim: Float;
    let broadcastLandingStim: Bool = scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.CanLandSilently) < 1.00;
    if !broadcastLandingStim || this.m_blockLandingStimBroadcasting {
      this.m_blockLandingStimBroadcasting = false;
      return;
    };
    if LocomotionGroundDecisions.CheckCrouchEnterCondition(stateContext, scriptInterface) && Equals(stimType, gamedataStimType.LandingRegular) {
      return;
    };
    impactSpeed = stateContext.GetPermanentFloatParameter(n"ImpactSpeed");
    speedThresholdToSendStim = this.GetFallingSpeedBasedOnHeight(scriptInterface, 1.20);
    if impactSpeed.value < speedThresholdToSendStim {
      broadcaster = scriptInterface.executionOwner.GetStimBroadcasterComponent();
      if IsDefined(broadcaster) {
        broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, stimType);
      };
    };
  }

  protected final func EvaluatePlayingLandingVFX(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let impactSpeed: StateResultFloat = stateContext.GetPermanentFloatParameter(n"ImpactSpeed");
    let minFallSpeed: Float = this.GetFallingSpeedBasedOnHeight(scriptInterface, 2.00);
    if impactSpeed.value < minFallSpeed {
      this.StartEffect(scriptInterface, n"landing_regular");
    };
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, 0);
  }
}

public abstract class FailedLandingAbstractDecisions extends AbstractLandDecisions {

  public final const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.GetInStateTime() >= this.GetStaticFloatParameterDefault("duration", 2.50);
  }
}

public abstract class FailedLandingAbstractEvents extends AbstractLandEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
  }
}

public class RegularLandEvents extends AbstractLandEvents {

  public final func OnEnterFromLadderCrouch(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_blockLandingStimBroadcasting = true;
    this.OnEnter(stateContext, scriptInterface);
  }

  public final func OnEnterFromUnsecureFootingFall(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    stateContext.SetConditionBoolParameter(n"blockEnteringSlide", true, true);
    this.OnEnter(stateContext, scriptInterface);
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    GameObject.PlayVoiceOver(scriptInterface.owner, n"regularLanding", n"Scripts:RegularLandEvents");
    this.EvaluateTransitioningToSlideAfterLanding(stateContext, scriptInterface);
    this.ShouldTriggerDestruction(stateContext, scriptInterface);
    this.EvaluatePlayingLandingVFX(stateContext, scriptInterface);
    this.BroadcastLandingStim(stateContext, scriptInterface, gamedataStimType.LandingRegular);
    super.OnEnter(stateContext, scriptInterface);
    this.TryPlayRumble(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.RegularLand);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, 1);
  }

  protected final func EvaluateTransitioningToSlideAfterLanding(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let currentSpeed: Float;
    let inAirTime: StateResultFloat;
    let velocity: Vector4;
    if !stateContext.GetConditionBool(n"CrouchToggled") {
      return;
    };
    inAirTime = stateContext.GetPermanentFloatParameter(n"InAirDuration");
    velocity = DefaultTransition.GetLinearVelocity(scriptInterface);
    currentSpeed = Vector4.Length2D(velocity);
    if inAirTime.valid && inAirTime.value > 0.70 && currentSpeed < 5.00 || inAirTime.valid && inAirTime.value < 0.50 {
      stateContext.SetConditionBoolParameter(n"blockEnteringSlide", true, true);
    };
  }

  protected final func TryPlayRumble(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let impactSpeed: StateResultFloat = stateContext.GetPermanentFloatParameter(n"ImpactSpeed");
    let inAirTime: StateResultFloat = stateContext.GetPermanentFloatParameter(n"InAirDuration");
    if scriptInterface.localBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsControllingDevice) {
      return;
    };
    if stateContext.GetConditionBool(n"CrouchToggled") && impactSpeed.valid && impactSpeed.value > this.GetFallingSpeedBasedOnHeight(scriptInterface, 1.20) {
      return;
    };
    if impactSpeed.valid && impactSpeed.value < this.GetFallingSpeedBasedOnHeight(scriptInterface, 0.66) {
      DefaultTransition.PlayRumble(scriptInterface, "light_pulse");
    } else {
      if inAirTime.valid && inAirTime.value > 0.33 {
        DefaultTransition.PlayRumble(scriptInterface, "light_pulse");
      };
    };
  }

  protected final func ShouldTriggerDestruction(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let impactSpeed: StateResultFloat = stateContext.GetPermanentFloatParameter(n"ImpactSpeed");
    if impactSpeed.value < this.GetFallingSpeedBasedOnHeight(scriptInterface, 2.50) {
      this.PrepareGameEffectAoEAttack(stateContext, scriptInterface, TweakDBInterface.GetAttackRecord(t"Attacks.PressureWave"));
      this.SpawnLandingFxGameEffect(t"Attacks.PressureWave", scriptInterface);
    };
  }
}

public class HardLandEvents extends FailedLandingAbstractEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.PlayHardLandingEffects(stateContext, scriptInterface);
    this.BroadcastLandingStim(stateContext, scriptInterface, gamedataStimType.LandingHard);
    super.OnEnter(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.HardLand);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, 2);
  }
}

public class VeryHardLandEvents extends FailedLandingAbstractEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.PlayVeryHardLandingEffects(stateContext, scriptInterface);
    this.BroadcastLandingStim(stateContext, scriptInterface, gamedataStimType.LandingVeryHard);
    super.OnEnter(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.VeryHardLand);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, 3);
    scriptInterface.SetStateVectorParameter(physicsStateValue.LinearVelocity, ToVariant(new Vector3()));
  }
}

public class DeathLandEvents extends FailedLandingAbstractEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.PlayDeathLandingEffects(stateContext, scriptInterface);
    this.BroadcastLandingStim(stateContext, scriptInterface, gamedataStimType.LandingVeryHard);
    super.OnEnter(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.DeathLand);
    this.ProcessPermanentBoolParameterToggle(n"WalkToggled", false, stateContext);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, 4);
  }
}

public class SuperheroLandDecisions extends AbstractLandDecisions {

  public final const func ToSuperheroLandRecovery(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let wasGrounded: StateResultBool = stateContext.GetPermanentBoolParameter(n"groundedGroundSlam");
    if wasGrounded.value {
      return this.GetInStateTime() >= this.GetStaticFloatParameterDefault("groundedStateDuration", 0.77);
    };
    return this.GetInStateTime() >= this.GetStaticFloatParameterDefault("aerialStateDuration", 0.77);
  }
}

public class SuperheroLandEvents extends AbstractLandEvents {

  public let m_spawnedLandingAttack: Bool;

  @default(SuperheroLandEvents, 0.0f)
  public let m_superheroFallTime: Float;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let superheroFallTime: StateResultFloat;
    let wasGrounded: StateResultBool;
    super.OnEnter(stateContext, scriptInterface);
    this.m_spawnedLandingAttack = false;
    wasGrounded = stateContext.GetPermanentBoolParameter(n"groundedGroundSlam");
    if wasGrounded.value {
      scriptInterface.PushAnimationEvent(n"GroundSlam");
      this.m_superheroFallTime = 0.00;
    } else {
      scriptInterface.PushAnimationEvent(n"SuperheroLand");
      superheroFallTime = stateContext.GetTemporaryFloatParameter(n"SuperheroFallTime");
      if superheroFallTime.valid {
        this.m_superheroFallTime = superheroFallTime.value;
      };
    };
    this.SendSuperHeroLandAnimFeature(scriptInterface, 3);
    this.PlaySound(n"lcm_wallrun_in", scriptInterface);
    stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.SuperheroLand);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, 5);
  }

  protected final func SpawmGroundSlamAoEAttack(scriptInterface: ref<StateGameScriptInterface>, record: TweakDBID, propagationRate: Float, rangeModifier: Float, height: Float, positionOffset: Vector4) -> Void {
    let attackContext: AttackInitContext;
    let attackRadius: Float;
    let effect: ref<EffectInstance>;
    let statMods: array<ref<gameStatModifierData>>;
    let attackRecord: ref<Attack_Record> = TweakDBInterface.GetAttackRecord(record);
    attackContext.record = attackRecord;
    attackContext.instigator = scriptInterface.executionOwner;
    attackContext.source = scriptInterface.executionOwner;
    let attack: ref<Attack_GameEffect> = IAttack.Create(attackContext) as Attack_GameEffect;
    if !IsDefined(attack) {
      return;
    };
    attack.GetStatModList(statMods);
    effect = attack.PrepareAttack(scriptInterface.executionOwner);
    attackRadius = attackRecord.Range();
    attackRadius = (TweakDBInterface.GetFloat(record + t".maxRange", attackRadius) - attackRadius) * rangeModifier + attackRadius;
    EffectData.SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.duration, attackRadius / propagationRate);
    EffectData.SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.range, attackRadius);
    EffectData.SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.radius, attackRadius);
    EffectData.SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.height, height);
    EffectData.SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.charge, rangeModifier);
    EffectData.SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, scriptInterface.executionOwner.GetWorldPosition() + positionOffset);
    EffectData.SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attack, ToVariant(attack));
    EffectData.SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attackStatModList, ToVariant(statMods));
    attack.StartAttack();
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let broadcaster: ref<StimBroadcasterComponent>;
    let height: Float;
    let positionOffset: Vector4;
    let propagationRate: Float;
    let wasGrounded: StateResultBool;
    let shouldSpawnPressureWave: Bool = false;
    let rangeModifier: Float = 0.00;
    if this.m_spawnedLandingAttack {
      return;
    };
    wasGrounded = stateContext.GetPermanentBoolParameter(n"groundedGroundSlam");
    if wasGrounded.value {
      shouldSpawnPressureWave = this.GetInStateTime() > this.GetStaticFloatParameterDefault("groundedPressureWaveSpawnDelay", 0.30);
    } else {
      shouldSpawnPressureWave = this.GetInStateTime() > this.GetStaticFloatParameterDefault("aerialPressureWaveSpawnDelay", 0.30);
    };
    if shouldSpawnPressureWave {
      this.m_spawnedLandingAttack = true;
      propagationRate = this.GetStaticFloatParameterDefault("propagationRate", 15.00);
      positionOffset = scriptInterface.GetOwnerForward() * this.GetStaticFloatParameterDefault("forwardOffset", 0.50);
      positionOffset.Z = positionOffset.Z + this.GetStaticFloatParameterDefault("zOffset", 0.00);
      if PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Body_Right_Perk_3_1) == 1 {
        rangeModifier = MinF(this.m_superheroFallTime / this.GetStaticFloatParameterDefault("superheroFallTimeToMaxEffect", 2.00), 1.00);
      };
      height = this.GetStaticFloatParameterDefault("height", 0.30);
      this.SpawmGroundSlamAoEAttack(scriptInterface, t"Attacks.GroundSlamFar", propagationRate, rangeModifier, height, positionOffset);
      this.SpawmGroundSlamAoEAttack(scriptInterface, t"Attacks.GroundSlamNear", propagationRate, rangeModifier, height, positionOffset);
      stateContext.SetTemporaryBoolParameter(n"spawnGroundSlamSwing", true, true);
      broadcaster = scriptInterface.executionOwner.GetStimBroadcasterComponent();
      if IsDefined(broadcaster) {
        broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.Explosion);
      };
      DefaultTransition.PlayRumble(scriptInterface, "heavy_fast");
      this.StartEffect(scriptInterface, n"stagger_effect");
      StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.GroundSlamCooldown");
    };
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, 0);
  }

  protected func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SendSuperHeroLandAnimFeature(scriptInterface, 0);
  }
}

public class SuperheroLandRecoveryDecisions extends AbstractLandDecisions {

  public final const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.GetInStateTime() >= this.GetStaticFloatParameterDefault("stateDuration", 0.40);
  }
}

public class SuperheroLandRecoveryEvents extends AbstractLandEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    stateContext.SetTemporaryBoolParameter(n"ClearVelocity", true, true);
    this.SendSuperHeroLandAnimFeature(scriptInterface, 4);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.SuperheroLandRecovery);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, 6);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SendSuperHeroLandAnimFeature(scriptInterface, 0);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Landing, 0);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SendSuperHeroLandAnimFeature(scriptInterface, 0);
  }
}

public abstract class WallCollisionHelpers extends IScriptable {

  public final static func GetWallCollision(const scriptInterface: ref<StateGameScriptInterface>, playerPosition: Vector4, up: Vector4, capsuleRadius: Float, out wallCollision: ControllerHit) -> Bool {
    let hit: ControllerHit;
    let touchDirection: Vector4;
    let collisionReport: array<ControllerHit> = scriptInterface.GetCollisionReport();
    let playerPositionCentreOfSphere: Vector4 = playerPosition + up * capsuleRadius;
    let sideCollisionFound: Bool = false;
    let collisionIndex: Int32 = 0;
    while collisionIndex < ArraySize(collisionReport) && !sideCollisionFound {
      hit = collisionReport[collisionIndex];
      touchDirection = Vector4.Normalize(hit.worldPos - playerPositionCentreOfSphere);
      if touchDirection.Z >= 0.00 {
        wallCollision = hit;
        return true;
      };
      collisionIndex += 1;
    };
    return false;
  }
}

public class StatusEffectDecisions extends LocomotionGroundDecisions {

  private let m_executionOwner: wref<GameObject>;

  private let m_statusEffectListener: ref<DefaultTransitionStatusEffectListener>;

  private let m_statusEffectEnumName: String;

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnAttach(stateContext, scriptInterface);
    this.m_statusEffectEnumName = this.GetStaticStringParameterDefault("statusEffectEnumName", "");
    this.m_statusEffectListener = new DefaultTransitionStatusEffectListener();
    this.m_statusEffectListener.m_transitionOwner = this;
    scriptInterface.GetStatusEffectSystem().RegisterListener(scriptInterface.owner.GetEntityID(), this.m_statusEffectListener);
    this.m_executionOwner = scriptInterface.executionOwner;
    this.EnableOnEnterCondition(StatusEffectSystem.ObjectHasStatusEffectOfTypeName(this.m_executionOwner, this.m_statusEffectEnumName));
  }

  protected final func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_statusEffectListener = null;
  }

  public func OnStatusEffectApplied(statusEffect: wref<StatusEffect_Record>) -> Void {
    if Equals(this.m_statusEffectEnumName, statusEffect.StatusEffectType().EnumName()) {
      this.EnableOnEnterCondition(true);
    };
  }

  public func OnStatusEffectRemoved(statusEffect: wref<StatusEffect_Record>) -> Void {
    if Equals(this.m_statusEffectEnumName, statusEffect.StatusEffectType().EnumName()) {
      this.EnableOnEnterCondition(StatusEffectSystem.ObjectHasStatusEffectOfTypeName(this.m_executionOwner, this.m_statusEffectEnumName));
    };
  }

  public const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return true;
  }

  protected const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return !this.HasMovementAffiliatedStatusEffect(stateContext, scriptInterface);
  }

  protected const func ToRegularFall(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return !this.HasMovementAffiliatedStatusEffect(stateContext, scriptInterface) && !this.IsTouchingGround(scriptInterface);
  }

  private final const func HasMovementAffiliatedStatusEffect(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let statusEffectRecord: wref<StatusEffect_Record> = stateContext.GetConditionScriptableParameter(n"AffectMovementStatusEffectRecord") as StatusEffect_Record;
    return StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.owner, statusEffectRecord.GetID());
  }
}

public class StatusEffectEvents extends LocomotionGroundEvents {

  public let m_statusEffectRecord: wref<StatusEffect_Record>;

  public let m_playerStatusEffectRecordData: wref<StatusEffectPlayerData_Record>;

  public let m_animFeatureStatusEffect: ref<AnimFeature_StatusEffect>;

  private let m_statusEffectEnumName: String;

  private let m_timeInAnimState: Float;

  private let m_timeInEffect: Float;

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnAttach(stateContext, scriptInterface);
    this.m_statusEffectEnumName = this.GetStaticStringParameterDefault("statusEffectEnumName", "");
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let appliedStatusEffects: array<ref<StatusEffect>>;
    super.OnEnter(stateContext, scriptInterface);
    this.m_timeInAnimState = 0.00;
    this.m_timeInEffect = 0.00;
    scriptInterface.GetStatusEffectSystem().GetAppliedEffectsOfTypeName(scriptInterface.executionOwnerEntityID, this.m_statusEffectEnumName, appliedStatusEffects);
    if ArraySize(appliedStatusEffects) > 0 {
      this.m_statusEffectRecord = appliedStatusEffects[0].GetRecord();
      this.m_playerStatusEffectRecordData = TweakDBInterface.GetStatusEffectRecord(this.m_statusEffectRecord.GetID()).PlayerData();
    };
    stateContext.SetConditionScriptableParameter(n"AffectMovementStatusEffectRecord", this.m_statusEffectRecord, true);
    stateContext.SetConditionScriptableParameter(n"PlayerStatusEffectRecordData", this.m_playerStatusEffectRecordData, true);
    if this.ShouldForceUnequipWeapon() {
      stateContext.SetPermanentBoolParameter(n"forcedTemporaryUnequip", true, true);
    };
    this.ProcessStatusEffectBasedOnType(scriptInterface, stateContext, this.GetStatusEffectType(scriptInterface, stateContext));
    if this.ShouldRotateToSource() {
      this.RotateToKnockdownSource(stateContext, scriptInterface);
    };
  }

  protected final func RotateToKnockdownSource(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let adjustRequest: ref<AdjustTransformWithDurations>;
    let direction: Vector4 = this.GetStatusEffectHitDirection(scriptInterface);
    if Vector4.IsZero(direction) {
      return;
    };
    adjustRequest = new AdjustTransformWithDurations();
    adjustRequest.SetPosition(new Vector4(0.00, 0.00, 0.00, 0.00));
    adjustRequest.SetSlideDuration(-1.00);
    adjustRequest.SetRotation(Quaternion.BuildFromDirectionVector(-direction, new Vector4(0.00, 0.00, 1.00, 0.00)));
    adjustRequest.SetRotationDuration(0.50);
    stateContext.SetTemporaryScriptableParameter(n"adjustTransform", adjustRequest, true);
  }

  private final func ProcessStatusEffectBasedOnType(scriptInterface: ref<StateGameScriptInterface>, stateContext: ref<StateContext>, type: gamedataStatusEffectType) -> Void {
    if !IsDefined(this.m_statusEffectRecord) {
      return;
    };
    if !this.ShouldUseCustomAdditives(scriptInterface, type) {
      if Equals(type, gamedataStatusEffectType.Stunned) {
        scriptInterface.PushAnimationEvent(n"StaggerHit");
        if !StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.Parry") {
          stateContext.SetPermanentBoolParameter(n"InterruptMelee", this.m_playerStatusEffectRecordData.ForceSafeWeapon(), true);
        };
      };
      this.SendCameraShakeDataToGraph(scriptInterface, stateContext, this.GetCameraShakeStrength());
      this.SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.Start);
    };
    this.ApplyCounterForce(scriptInterface, stateContext, this.GetImpulseDistance(), this.GetScaleImpulseDistance());
  }

  private final func SendCameraShakeDataToGraph(scriptInterface: ref<StateGameScriptInterface>, stateContext: ref<StateContext>, camShakeStrength: Float) -> Void {
    let animFeatureHitReaction: ref<AnimFeature_PlayerHitReactionData> = new AnimFeature_PlayerHitReactionData();
    animFeatureHitReaction.hitStrength = camShakeStrength;
    scriptInterface.SetAnimationParameterFeature(n"HitReactionData", animFeatureHitReaction);
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    this.UpdateTimes(timeDelta, scriptInterface);
  }

  private final func UpdateTimes(timeDelta: Float, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let sandevistanVSTimeDilation: Float = AbsF(GameInstance.GetTimeSystem(scriptInterface.owner.GetGame()).GetActiveTimeDilation(n"sandevistanVersusSandevistan"));
    let correctedTimeDelta: Float = (timeDelta * 1.00) / sandevistanVSTimeDilation;
    this.m_timeInAnimState += correctedTimeDelta;
    this.m_timeInEffect += correctedTimeDelta;
  }

  protected final const func GetTimeInAnimState() -> Float {
    return this.m_timeInAnimState;
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    this.DefaultOnExit(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.DefaultOnExit(stateContext, scriptInterface);
  }

  protected final func OnExitToFall(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.DefaultOnExit(stateContext, scriptInterface);
    scriptInterface.PushAnimationEvent(n"StraightToFall");
  }

  protected func CommonOnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.RemoveStatusEffect(scriptInterface, stateContext);
    if this.ShouldForceUnequipWeapon() {
      stateContext.SetPermanentBoolParameter(n"forcedTemporaryUnequip", false, true);
    };
    stateContext.RemovePermanentFloatParameter(n"SatusEffectStateStartTime");
  }

  protected final func DefaultOnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if IsDefined(this.m_animFeatureStatusEffect) {
      this.m_animFeatureStatusEffect.Clear();
    };
    scriptInterface.SetAnimationParameterFeature(n"StatusEffect", this.m_animFeatureStatusEffect);
    if this.GetStaticBoolParameterDefault("forceExitToStand", false) {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
    };
    this.CommonOnExit(stateContext, scriptInterface);
  }

  protected func SendStatusEffectAnimDataToGraph(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, state: EKnockdownStates) -> Void {
    if !IsDefined(this.m_animFeatureStatusEffect) {
      this.m_animFeatureStatusEffect = new AnimFeature_StatusEffect();
    };
    stateContext.SetPermanentFloatParameter(n"SatusEffectStateStartTime", EngineTime.ToFloat(GameInstance.GetTimeSystem(scriptInterface.owner.GetGame()).GetSimTime()), true);
    this.m_timeInAnimState = 0.00;
    StatusEffectHelper.PopulateStatusEffectAnimData(scriptInterface.owner, this.m_statusEffectRecord, state, this.GetStatusEffectHitDirection(scriptInterface), this.m_animFeatureStatusEffect);
    scriptInterface.SetAnimationParameterFeature(n"StatusEffect", this.m_animFeatureStatusEffect);
  }

  protected final func ApplyCounterForce(scriptInterface: ref<StateGameScriptInterface>, stateContext: ref<StateContext>, desiredDistance: Float, scaleDistance: Bool) -> Void {
    let direction: Vector4;
    let ev: ref<PSMImpulse>;
    let impulseDir: Vector4;
    let speed: Float;
    if desiredDistance <= 0.00 {
      return;
    };
    direction = this.GetStatusEffectHitDirection(scriptInterface);
    direction.Z = 0.00;
    if scaleDistance {
      desiredDistance *= Vector4.Length2D(direction);
    };
    if Vector4.IsZero(direction) {
      direction = scriptInterface.owner.GetWorldForward() * -1.00;
    } else {
      Vector4.Normalize2D(direction);
    };
    speed = this.GetSpeedBasedOnDistance(scriptInterface, desiredDistance);
    impulseDir = direction * speed;
    ev = new PSMImpulse();
    ev.id = n"impulse";
    ev.impulse = impulseDir;
    scriptInterface.owner.QueueEvent(ev);
  }

  private final const func RemoveStatusEffect(const scriptInterface: ref<StateGameScriptInterface>, const stateContext: ref<StateContext>) -> Void {
    StatusEffectHelper.RemoveStatusEffect(scriptInterface.owner, this.m_statusEffectRecord.GetID());
    stateContext.RemoveConditionScriptableParameter(n"PlayerStatusEffectRecordData");
  }

  private final const func GetStatusEffectType(const scriptInterface: ref<StateGameScriptInterface>, const stateContext: ref<StateContext>) -> gamedataStatusEffectType {
    return this.m_statusEffectRecord.StatusEffectType().Type();
  }

  protected final const func GetStatusEffectRemainingDuration(const scriptInterface: ref<StateGameScriptInterface>, const stateContext: ref<StateContext>) -> Float {
    let duration: Float = StatusEffectHelper.GetStatusEffectByID(scriptInterface.owner, this.m_statusEffectRecord.GetID()).GetTotalDuration();
    return duration - this.m_timeInEffect;
  }

  protected final const func GetStatusEffectHitDirection(const scriptInterface: ref<StateGameScriptInterface>) -> Vector4 {
    return StatusEffectHelper.GetStatusEffectByID(scriptInterface.owner, this.m_statusEffectRecord.GetID()).GetDirection();
  }

  protected final const func GetStartupAnimDuration() -> Float {
    return this.m_playerStatusEffectRecordData.StartupAnimDuration();
  }

  protected final const func ShouldRotateToSource() -> Bool {
    return this.m_playerStatusEffectRecordData.RotateToSource();
  }

  protected final const func GetAirRecoveryAnimDuration() -> Float {
    return this.m_playerStatusEffectRecordData.AirRecoveryAnimDuration();
  }

  protected final const func GetRecoveryAnimDuration() -> Float {
    return this.m_playerStatusEffectRecordData.RecoveryAnimDuration();
  }

  protected final const func GetLandAnimDuration() -> Float {
    return this.m_playerStatusEffectRecordData.LandAnimDuration();
  }

  private final const func GetImpulseDistance() -> Float {
    return this.m_playerStatusEffectRecordData.ImpulseDistance();
  }

  private final const func GetScaleImpulseDistance() -> Bool {
    return this.m_playerStatusEffectRecordData.ScaleImpulseDistance();
  }

  private final const func GetCameraShakeStrength() -> Float {
    return this.m_playerStatusEffectRecordData.CameraShakeStrength();
  }

  private final const func ShouldForceUnequipWeapon() -> Bool {
    return this.m_playerStatusEffectRecordData.ForceUnequipWeapon();
  }

  protected final const func ShouldUseCustomAdditives(const scriptInterface: ref<StateGameScriptInterface>, type: gamedataStatusEffectType) -> Bool {
    return Equals(type, gamedataStatusEffectType.Stunned) && StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"UseCustomAdditives");
  }
}

public class KnockdownDecisions extends StatusEffectDecisions {

  protected const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let canExit: StateResultBool = stateContext.GetTemporaryBoolParameter(n"StatusEffect_CanExitKnockdown");
    if canExit.valid {
      return canExit.value;
    };
    return false;
  }

  protected const func ToRegularFall(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let canExit: StateResultBool = stateContext.GetTemporaryBoolParameter(n"StatusEffect_CanExitKnockdown");
    if canExit.valid {
      return super.ToRegularFall(stateContext, scriptInterface);
    };
    return false;
  }

  protected const func ToSecondaryKnockdown(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let canTriggerSecondaryKnockdown: StateResultBool = stateContext.GetPermanentBoolParameter(n"StatusEffect_TriggerSecondaryKnockdown");
    if canTriggerSecondaryKnockdown.valid {
      return this.EnterCondition(stateContext, scriptInterface);
    };
    return false;
  }
}

public class KnockdownEvents extends StatusEffectEvents {

  public let m_cachedPlayerVelocity: Vector4;

  public let m_secondaryKnockdownDir: Vector4;

  public let m_secondaryKnockdownTimer: Float;

  public let m_playedImpactAnim: Bool;

  public let m_frictionForceApplied: Bool;

  public let m_frictionForceAppliedLastFrame: Bool;

  public let m_delayDamageFrame: Bool;

  public let m_bikeKnockdown: Bool;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Knockdown);
    super.OnEnter(stateContext, scriptInterface);
    this.m_playedImpactAnim = false;
    this.m_frictionForceApplied = false;
    this.m_frictionForceAppliedLastFrame = false;
    this.m_delayDamageFrame = false;
    this.m_secondaryKnockdownTimer = -1.00;
    this.m_cachedPlayerVelocity = DefaultTransition.GetLinearVelocity(scriptInterface);
    this.m_bikeKnockdown = StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.BikeKnockdown");
  }

  protected func CommonOnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.CommonOnExit(stateContext, scriptInterface);
    stateContext.RemovePermanentBoolParameter(n"StatusEffect_TriggerSecondaryKnockdown");
  }

  protected func SendStatusEffectAnimDataToGraph(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, state: EKnockdownStates) -> Void {
    if Equals(state, EKnockdownStates.Land) && this.m_animFeatureStatusEffect.state != EnumInt(state) {
      this.SetModifierGroupForState(scriptInterface, "PlayerLocomotion.player_locomotion_data_KnockdownLand");
    };
    super.SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, state);
  }

  private final func UpdateStatusEffectAnimStates(timeDelta: Float, scriptInterface: ref<StateGameScriptInterface>, stateContext: ref<StateContext>) -> Void {
    switch IntEnum<EKnockdownStates>(this.m_animFeatureStatusEffect.state) {
      case EKnockdownStates.Start:
        if this.GetTimeInAnimState() >= this.GetStartupAnimDuration() {
          if this.IsTouchingGround(scriptInterface) {
            this.SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.Land);
          } else {
            this.SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.FallLoop);
          };
        };
        break;
      case EKnockdownStates.FallLoop:
        if this.IsTouchingGround(scriptInterface) {
          this.SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.Land);
        } else {
          if this.GetStatusEffectRemainingDuration(scriptInterface, stateContext) <= this.GetAirRecoveryAnimDuration() {
            this.SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.AirRecovery);
          };
        };
        break;
      case EKnockdownStates.Land:
        if this.GetTimeInAnimState() >= this.GetLandAnimDuration() && this.GetStatusEffectRemainingDuration(scriptInterface, stateContext) <= this.GetRecoveryAnimDuration() {
          this.SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.Recovery);
        };
        break;
      case EKnockdownStates.Recovery:
        if this.GetTimeInAnimState() >= this.GetRecoveryAnimDuration() {
          stateContext.SetTemporaryBoolParameter(n"StatusEffect_CanExitKnockdown", true, true);
        };
        break;
      case EKnockdownStates.AirRecovery:
        if this.IsTouchingGround(scriptInterface) {
          this.SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.Land);
        } else {
          if this.GetTimeInAnimState() >= this.GetAirRecoveryAnimDuration() {
            stateContext.SetTemporaryBoolParameter(n"StatusEffect_CanExitKnockdown", true, true);
          };
        };
        break;
      default:
    };
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let collisionFrictionForce: Vector4;
    let currentVelocity: Vector4;
    let frictionForceScale: Float;
    let impulseEvent: ref<PSMImpulse>;
    let startupInterruptPoint: Float;
    let velocityChangeDir: Vector4;
    let velocityChangeMag: Float;
    let impactDirection: Int32 = -1;
    let playImpact: Bool = false;
    let triggerSecondaryKnockdown: Bool = false;
    this.UpdateStatusEffectAnimStates(timeDelta, scriptInterface, stateContext);
    this.UpdateQueuedSecondaryKnockdown(stateContext, scriptInterface, timeDelta);
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    if !this.m_bikeKnockdown {
      return;
    };
    if this.m_frictionForceAppliedLastFrame {
      this.m_frictionForceAppliedLastFrame = false;
      return;
    };
    currentVelocity = DefaultTransition.GetLinearVelocity(scriptInterface);
    velocityChangeDir = currentVelocity - this.m_cachedPlayerVelocity;
    velocityChangeMag = Vector4.Length(velocityChangeDir);
    if velocityChangeMag > 0.00 {
      velocityChangeDir *= 1.00 / velocityChangeMag;
    };
    if velocityChangeMag > 7.00 && Vector4.Dot(velocityChangeDir, this.m_cachedPlayerVelocity) < 0.00 {
      if !this.m_delayDamageFrame {
        this.m_delayDamageFrame = true;
        playImpact = true;
        impactDirection = GameObject.GetLocalAngleForDirectionInInt(velocityChangeDir, scriptInterface.owner);
        currentVelocity = this.m_cachedPlayerVelocity;
      } else {
        this.m_delayDamageFrame = false;
        frictionForceScale = 0.00;
        if velocityChangeMag > 25.00 {
          frictionForceScale = 0.90;
          this.PrepareGameEffectAoEAttack(stateContext, scriptInterface, TweakDBInterface.GetAttackRecord(t"Attacks.HardWallImpact"));
          triggerSecondaryKnockdown = true;
        } else {
          if velocityChangeMag > 15.00 {
            frictionForceScale = 0.60;
            this.PrepareGameEffectAoEAttack(stateContext, scriptInterface, TweakDBInterface.GetAttackRecord(t"Attacks.MediumWallImpact"));
            triggerSecondaryKnockdown = true;
          } else {
            frictionForceScale = 0.30;
            this.PrepareGameEffectAoEAttack(stateContext, scriptInterface, TweakDBInterface.GetAttackRecord(t"Attacks.LightWallImpact"));
            triggerSecondaryKnockdown = Vector4.Length2D(collisionFrictionForce) < 1.00;
          };
        };
        if frictionForceScale > 0.00 {
          if !this.m_frictionForceApplied {
            this.m_frictionForceApplied = true;
            this.m_frictionForceAppliedLastFrame = true;
            impulseEvent = new PSMImpulse();
            impulseEvent.id = n"impulse";
            impulseEvent.impulse = currentVelocity * -frictionForceScale;
            currentVelocity += impulseEvent.impulse;
            scriptInterface.owner.QueueEvent(impulseEvent);
          };
        };
      };
    } else {
      this.m_delayDamageFrame = false;
    };
    if NotEquals(playImpact, this.m_animFeatureStatusEffect.playImpact) {
      if !this.m_playedImpactAnim {
        this.m_playedImpactAnim = playImpact;
      };
      if playImpact {
        this.m_animFeatureStatusEffect.playImpact = true;
        this.m_animFeatureStatusEffect.impactDirection = impactDirection;
      } else {
        this.m_animFeatureStatusEffect.playImpact = false;
      };
      scriptInterface.SetAnimationParameterFeature(n"StatusEffect", this.m_animFeatureStatusEffect);
    };
    if this.m_playedImpactAnim && this.m_animFeatureStatusEffect.state == 1 {
      startupInterruptPoint = this.m_playerStatusEffectRecordData.StartupAnimInterruptPoint();
      if startupInterruptPoint >= 0.00 {
        if this.GetTimeInAnimState() >= startupInterruptPoint {
          this.SendStatusEffectAnimDataToGraph(stateContext, scriptInterface, EKnockdownStates.FallLoop);
        };
      };
    };
    if triggerSecondaryKnockdown {
      this.QueueSecondaryKnockdown(stateContext, scriptInterface, velocityChangeDir);
    };
    this.m_cachedPlayerVelocity = currentVelocity;
  }

  protected final func QueueSecondaryKnockdown(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, knockdownDir: Vector4) -> Void {
    let startupInterruptPoint: Float;
    if this.m_secondaryKnockdownTimer <= 0.00 {
      this.m_secondaryKnockdownTimer = 0.10;
      this.m_secondaryKnockdownDir = knockdownDir;
      if this.m_animFeatureStatusEffect.state == 1 {
        startupInterruptPoint = this.m_playerStatusEffectRecordData.StartupAnimInterruptPoint();
        if startupInterruptPoint < 0.00 {
          startupInterruptPoint = this.m_playerStatusEffectRecordData.StartupAnimDuration();
        };
        this.m_secondaryKnockdownTimer += startupInterruptPoint - this.GetTimeInAnimState();
      };
    };
  }

  protected final func UpdateQueuedSecondaryKnockdown(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, deltaTime: Float) -> Void {
    let statusEffectRecord: wref<StatusEffect_Record>;
    let stackcount: Uint32 = 1u;
    if this.m_secondaryKnockdownTimer > 0.00 && this.m_animFeatureStatusEffect.state < 3 {
      this.m_secondaryKnockdownTimer -= deltaTime;
      if this.m_secondaryKnockdownTimer <= 0.00 {
        stateContext.SetPermanentBoolParameter(n"StatusEffect_TriggerSecondaryKnockdown", true, true);
        statusEffectRecord = TweakDBInterface.GetStatusEffectRecord(t"BaseStatusEffect.SecondaryKnockdown");
        GameInstance.GetStatusEffectSystem(scriptInterface.owner.GetGame()).ApplyStatusEffect(scriptInterface.executionOwnerEntityID, statusEffectRecord.GetID(), GameObject.GetTDBID(scriptInterface.owner), scriptInterface.ownerEntityID, stackcount, this.m_secondaryKnockdownDir);
      };
    };
  }

  protected final func DidPlayerCollideWithWall(scriptInterface: ref<StateGameScriptInterface>, out wallCollision: ControllerHit) -> Bool {
    let playerPosition: Vector4 = DefaultTransition.GetPlayerPosition(scriptInterface);
    let capsuleRadius: Float = FromVariant<Float>(scriptInterface.GetStateVectorParameter(physicsStateValue.Radius));
    return WallCollisionHelpers.GetWallCollision(scriptInterface, playerPosition, DefaultTransition.GetUpVector(), capsuleRadius, wallCollision);
  }
}

public class ForcedKnockdownDecisions extends KnockdownDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.HasForcedStatusEffect(stateContext, scriptInterface);
  }

  private final const func HasForcedStatusEffect(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return Equals(this.GetStaticStringParameterDefault("statusEffectEnumName", ""), this.GetForcedStatusEffectName(stateContext, scriptInterface));
  }

  private final const func GetForcedStatusEffectName(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> String {
    let statusEffectName: String;
    let statusEffectRecord: wref<StatusEffect_Record> = stateContext.GetPermanentScriptableParameter(n"StatusEffect_ForceKnockdown") as StatusEffect_Record;
    if IsDefined(statusEffectRecord) {
      statusEffectName = statusEffectRecord.StatusEffectType().EnumName();
    };
    return statusEffectName;
  }
}

public class ForcedKnockdownEvents extends KnockdownEvents {

  public let m_firstUpdate: Bool;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let initialVelocity: StateResultVector;
    let originalStartTime: StateResultFloat;
    let statusEffectRecord: wref<StatusEffect_Record> = stateContext.GetPermanentScriptableParameter(n"StatusEffect_ForceKnockdown") as StatusEffect_Record;
    stateContext.RemovePermanentScriptableParameter(n"StatusEffect_ForceKnockdown");
    initialVelocity = stateContext.GetPermanentVectorParameter(n"StatusEffect_ForceKnockdownImpulse");
    stateContext.RemovePermanentVectorParameter(n"StatusEffect_ForceKnockdownImpulse");
    stateContext.SetTemporaryScriptableParameter(n"StatusEffect", statusEffectRecord, true);
    originalStartTime = stateContext.GetPermanentFloatParameter(n"SatusEffectStateStartTime");
    super.OnEnter(stateContext, scriptInterface);
    if initialVelocity.valid {
      this.m_cachedPlayerVelocity = initialVelocity.value;
    };
    if originalStartTime.valid {
      stateContext.SetPermanentFloatParameter(n"SatusEffectStateStartTime", originalStartTime.value, true);
    };
    this.m_firstUpdate = true;
  }

  protected func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if this.m_firstUpdate {
      this.m_firstUpdate = false;
    } else {
      super.OnUpdate(timeDelta, stateContext, scriptInterface);
    };
  }
}

public class FelledDecisions extends LocomotionGroundDecisions {

  private let m_felled: Bool;

  private let m_callbackIDs: [ref<CallbackHandle>];

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let allBlackboardDef: ref<AllBlackboardDefinitions> = GetAllBlackboardDefs();
    super.OnAttach(stateContext, scriptInterface);
    ArrayPush(this.m_callbackIDs, scriptInterface.localBlackboard.RegisterListenerBool(allBlackboardDef.PlayerStateMachine.Felled, this, n"OnFelledChanged", true));
  }

  protected final func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnAttach(stateContext, scriptInterface);
    ArrayClear(this.m_callbackIDs);
  }

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.m_felled;
  }

  protected final const func ToStand(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return !this.m_felled;
  }

  protected cb func OnFelledChanged(value: Bool) -> Bool {
    this.m_felled = value;
  }
}

public class FelledEvents extends LocomotionGroundEvents {

  private let m_animFeatureFelled: ref<AnimFeature_Felled>;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnEnter(stateContext, scriptInterface);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Felled);
    scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().DisableQueriesForOwner(scriptInterface.owner, gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
    this.m_animFeatureFelled = new AnimFeature_Felled();
    this.m_animFeatureFelled.active = true;
    scriptInterface.SetAnimationParameterFeature(n"Felled", this.m_animFeatureFelled);
    stateContext.SetPermanentCNameParameter(n"FelledCameraParams", n"Felled", true);
    this.UpdateCameraParams(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    scriptInterface.GetSpatialQueriesSystem().GetPlayerObstacleSystem().EnableQueriesForOwner(scriptInterface.owner, gamePlayerObstacleSystemQueryType.Climb_Vault, gamePlayerObstacleSystemQueryType.Covers, gamePlayerObstacleSystemQueryType.AverageNormal);
    this.SetDetailedState(scriptInterface, gamePSMDetailedLocomotionStates.Stand);
    this.m_animFeatureFelled.active = false;
    scriptInterface.SetAnimationParameterFeature(n"Felled", this.m_animFeatureFelled);
    stateContext.SetPermanentCNameParameter(n"FelledCameraParams", n"None", true);
  }
}
