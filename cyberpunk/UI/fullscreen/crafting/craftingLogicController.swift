
public class CraftingMainLogicController extends inkLogicController {

  @default(CraftingMainLogicController, 0.3f)
  private const let TIME_UNTIL_SELECTION: Float;

  protected edit let m_root: inkWidgetRef;

  protected edit let m_itemDetailsContainer: inkWidgetRef;

  protected edit let m_leftListScrollHolder: inkWidgetRef;

  protected edit let m_virtualListContainer: inkVirtualCompoundRef;

  protected edit let m_filterGroup: inkWidgetRef;

  protected edit let m_sortingButton: inkWidgetRef;

  protected edit let m_sortingDropdown: inkWidgetRef;

  protected edit let m_tooltipContainer: inkWidgetRef;

  protected edit let m_itemName: inkTextRef;

  protected edit let m_itemQuality: inkTextRef;

  protected edit let m_progressBarContainer: inkCompoundRef;

  protected edit let m_progressButtonContainer: inkCompoundRef;

  protected edit let m_blockedText: inkTextRef;

  protected edit let m_ingredientsListContainer: inkCompoundRef;

  protected let m_notificationType: UIMenuNotificationType;

  protected let m_classifier: ref<CraftingItemTemplateClassifier>;

  protected let m_dataView: ref<CraftingDataView>;

  protected let m_dataSource: ref<ScriptableDataSource>;

  protected let m_virtualListController: wref<inkVirtualGridController>;

  protected let m_leftListScrollController: wref<inkScrollController>;

  protected let m_ingredientsControllerList: [wref<IngredientListItemLogicController>];

  @default(CraftingLogicController, 5)
  @default(UpgradingScreenController, 8)
  protected let m_maxIngredientCount: Int32;

  protected let m_selectedRecipe: ref<RecipeData>;

  protected let m_selectedItemData: InventoryItemData;

  protected let m_isCraftable: Bool;

  protected let m_filters: [Int32];

  protected let m_progressButtonController: wref<ProgressBarButton>;

  protected let m_itemWeaponController: wref<InventoryItemDisplayController>;

  protected let m_itemIngredientController: wref<InventoryItemDisplayController>;

  protected let m_doPlayFilterSounds: Bool;

  protected let m_craftingGameController: wref<CraftingMainGameController>;

  protected let m_craftingSystem: wref<CraftingSystem>;

  protected let m_tooltipsManager: wref<gameuiTooltipsManager>;

  protected let m_buttonHintsController: wref<ButtonHints>;

  protected let m_inventoryManager: wref<InventoryDataManagerV2>;

  protected let m_sortingController: wref<DropdownListController>;

  protected let m_sortingButtonController: wref<DropdownButtonController>;

  protected let m_isPanelOpen: Bool;

  protected let m_hasSpawnedTooltip: Bool;

  private let m_currentSelected: wref<CraftableItemLogicController>;

  private let m_itemTooltipPath: ResRef;

  private let m_isProcessing: Bool;

  protected let m_DelaySystem: wref<DelaySystem>;

  protected let m_StatsSystem: wref<StatsSystem>;

  protected let m_Player: wref<PlayerPuppet>;

  protected let m_Game: GameInstance;

  private let m_firstClicked: Bool;

  private let m_progress: Float;

  @default(CraftingMainLogicController, true)
  private let m_isFirstOpen: Bool;

  public func Init(craftingGameController: wref<CraftingMainGameController>) -> Void {
    this.m_craftingGameController = craftingGameController;
    this.m_craftingSystem = craftingGameController.GetCraftingSystem();
    this.m_tooltipsManager = craftingGameController.GetTooltipManager();
    this.m_buttonHintsController = craftingGameController.GetButtonHintsController();
    this.m_inventoryManager = craftingGameController.GetInventoryManager();
    this.InitVirtualList();
    this.SetupIngredientWidgets();
    this.SetupFilters();
    this.SetupSortingDropdown();
    inkWidgetRef.SetVisible(this.m_itemDetailsContainer, false);
    this.m_leftListScrollController = inkWidgetRef.GetController(this.m_leftListScrollHolder) as inkScrollController;
    this.m_itemTooltipPath = r"base\\gameplay\\gui\\common\\tooltip\\new_itemtooltip.inkwidget";
    this.m_Player = this.m_craftingGameController.GetPlayer();
    this.m_Game = this.m_Player.GetGame();
    this.m_DelaySystem = GameInstance.GetDelaySystem(this.m_Game);
    this.m_StatsSystem = GameInstance.GetStatsSystem(this.m_Game);
  }

  protected final func InitVirtualList() -> Void {
    this.m_virtualListController = inkWidgetRef.GetControllerByType(this.m_virtualListContainer, n"inkVirtualGridController") as inkVirtualGridController;
    this.m_classifier = new CraftingItemTemplateClassifier();
    this.m_virtualListController.SetClassifier(this.m_classifier);
    this.m_dataSource = new ScriptableDataSource();
    this.m_dataView = new CraftingDataView();
    this.m_dataView.SetSource(this.m_dataSource);
    this.m_dataView.EnableSorting();
    this.m_dataView.BindUIScriptableSystem(this.m_craftingGameController.GetScriptableSystem());
    this.m_virtualListController.SetSource(this.m_dataView);
    this.m_virtualListController.RegisterToCallback(n"OnItemSelected", this, n"OnItemSelect");
  }

  public final func OpenPanel() -> Void {
    let currentIndex: Uint32;
    inkWidgetRef.SetVisible(this.m_root, true);
    this.RefreshListViewContent();
    this.SetItemButtonHintsHoverOut(null);
    if !this.m_isFirstOpen {
      currentIndex = this.m_virtualListController.GetToggledIndex();
      this.DispatchSelectDelayed(currentIndex);
    } else {
      currentIndex = 0u;
      this.DispatchSelectDelayed(currentIndex);
      this.m_isFirstOpen = false;
    };
    this.m_isPanelOpen = true;
  }

  public func RefreshListViewContent(opt inventoryItemData: InventoryItemData) -> Void;

  public func OnChangeTab(isCurrent: Bool) -> Void {
    if !isCurrent {
      this.m_currentSelected = null;
    };
  }

  public final func ClosePanel() -> Void {
    inkWidgetRef.SetVisible(this.m_root, false);
    this.m_dataSource.Clear();
    this.m_isPanelOpen = false;
  }

  protected func SetupIngredientWidgets() -> Void {
    let i: Int32;
    if ArraySize(this.m_ingredientsControllerList) < this.m_maxIngredientCount {
      i = 0;
      while i < this.m_maxIngredientCount {
        this.AsyncSpawnFromLocal(inkWidgetRef.Get(this.m_ingredientsListContainer), n"ingredientsListItem", this, n"OnIngedientControllerSpawned");
        i += 1;
      };
    };
  }

  protected cb func OnIngedientControllerSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    let controller: wref<IngredientListItemLogicController> = widget.GetController() as IngredientListItemLogicController;
    controller.SetUnusedState();
    ArrayPush(this.m_ingredientsControllerList, controller);
  }

  protected func SetupFilters() -> Void {
    let radioGroup: ref<FilterRadioGroup> = inkWidgetRef.GetControllerByType(this.m_filterGroup, n"FilterRadioGroup") as FilterRadioGroup;
    radioGroup.SetData(this.m_filters, this.m_tooltipsManager, 0);
    radioGroup.RegisterToCallback(n"OnValueChanged", this, n"OnFilterChange");
    radioGroup.Toggle(0);
    this.OnFilterChange(null, 0);
  }

  protected cb func OnFilterChange(controller: wref<inkRadioGroupController>, selectedIndex: Int32) -> Bool {
    let filter: ItemFilterCategory = IntEnum<ItemFilterCategory>(this.m_filters[selectedIndex]);
    this.m_dataView.SetFilterType(filter);
    this.PlayLibraryAnimation(n"player_grid_show");
    this.m_leftListScrollController.SetScrollPosition(0.00);
    if this.m_doPlayFilterSounds {
      this.PlaySound(n"Button", n"OnPress");
    };
    inkWidgetRef.SetVisible(this.m_itemDetailsContainer, false);
  }

  protected final func SetupSortingDropdown() -> Void {
    inkWidgetRef.RegisterToCallback(this.m_sortingButton, n"OnRelease", this, n"OnSortingButtonClicked");
    this.m_sortingController = inkWidgetRef.GetController(this.m_sortingDropdown) as DropdownListController;
    this.m_sortingButtonController = inkWidgetRef.GetController(this.m_sortingButton) as DropdownButtonController;
    this.m_sortingController.Setup(this, SortingDropdownData.GetDefaultDropdownOptions(), this.m_sortingButtonController);
    this.m_sortingButtonController.SetData(CraftingMainLogicController.GetDropdownOption(ItemSortMode.Default));
  }

  protected cb func OnSortingButtonClicked(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"click") {
      this.m_sortingController.Toggle();
      this.PlaySound(n"Button", n"OnPress");
    };
  }

  protected cb func OnDropdownItemClickedEvent(evt: ref<DropdownItemClickedEvent>) -> Bool {
    let identifier: ItemSortMode = FromVariant<ItemSortMode>(evt.identifier);
    let data: ref<DropdownItemData> = CraftingMainLogicController.GetDropdownOption(identifier);
    if IsDefined(data) {
      this.m_sortingButtonController.SetData(data);
      this.m_dataView.SetSortMode(identifier);
      this.PlaySound(n"Button", n"OnPress");
    };
  }

  protected final func SetCraftingButton(const label: script_ref<String>) -> Void {
    this.m_progressButtonController = inkWidgetRef.GetControllerByType(this.m_progressButtonContainer, n"ProgressBarButton") as ProgressBarButton;
    this.m_progressButtonController.SetupProgressButton(label, inkWidgetRef.GetControllerByType(this.m_progressBarContainer, n"ProgressBarsController") as ProgressBarsController);
    this.m_progressButtonController.ButtonController.RegisterToCallback(n"OnPress", this, n"OnButtonClick");
    this.m_progressButtonController.ButtonController.RegisterToCallback(n"OnHoverOver", this, n"SetItemButtonHintsHoverOver");
    this.m_progressButtonController.ButtonController.RegisterToCallback(n"OnHoverOut", this, n"SetItemButtonHintsHoverOut");
    inkWidgetRef.SetVisible(this.m_progressBarContainer, false);
  }

  protected cb func OnButtonClick(evt: ref<inkPointerEvent>) -> Bool {
    let craftingNotification: ref<UIMenuNotificationEvent>;
    if evt.IsAction(n"craft_item") {
      if !this.m_isCraftable {
        if NotEquals(this.m_notificationType, UIMenuNotificationType.CraftingNoPerks) {
          craftingNotification = new UIMenuNotificationEvent();
          craftingNotification.m_notificationType = this.m_notificationType;
          this.QueueEvent(craftingNotification);
        };
      } else {
        this.StartProcess();
      };
    };
    this.m_craftingGameController.DisableTabs();
  }

  protected cb func OnItemSelect(previous: ref<inkVirtualCompoundItemController>, next: ref<inkVirtualCompoundItemController>) -> Bool {
    if next == this.m_currentSelected {
      return true;
    };
    this.UpdateSelection(next as CraftableItemLogicController);
    this.ResetProcess();
    this.SetItemButtonHintsHoverOver(null);
  }

  private final func UpdateSelection(selection: ref<CraftableItemLogicController>) -> Void {
    if IsDefined(this.m_currentSelected) {
      this.m_currentSelected.UnregisterFromCallback(n"OnPress", this, n"OnPressSelectedItem");
      this.m_currentSelected.UnregisterFromCallback(n"OnHold", this, n"OnHoldSelectedItem");
      this.m_currentSelected.UnregisterFromCallback(n"OnHoverOut", this, n"OnHoverOutSelectedItem");
      this.m_currentSelected.UnregisterFromCallback(n"OnRelease", this, n"OnReleaseSelectedItem");
      this.m_firstClicked = false;
    };
    this.m_currentSelected = selection;
    this.m_currentSelected.RegisterToCallback(n"OnPress", this, n"OnPressSelectedItem");
    this.m_currentSelected.RegisterToCallback(n"OnHold", this, n"OnHoldSelectedItem");
    this.m_currentSelected.RegisterToCallback(n"OnHoverOut", this, n"OnHoverOutSelectedItem");
    this.m_currentSelected.RegisterToCallback(n"OnRelease", this, n"OnReleaseSelectedItem");
  }

  private final func OnPressSelectedItem(evt: ref<inkPointerEvent>) -> Void {
    if evt.IsAction(n"craft_item") {
      this.UpdateItemPreview(this.m_currentSelected);
      this.PlaySound(n"Button", n"OnPress");
      if this.m_isCraftable {
        this.StartProcess();
        this.m_craftingGameController.DisableTabs();
      };
    } else {
      if evt.IsAction(n"click") && !evt.IsAction(n"mouse_left") {
        this.UpdateItemPreview(this.m_currentSelected);
        this.PlaySound(n"Button", n"OnPress");
      };
    };
  }

  protected cb func OnHoverOutSelectedItem(evt: ref<inkPointerEvent>) -> Bool {
    this.m_progressButtonController.Lock();
    if this.m_firstClicked {
      this.PlaySound(n"Item", n"OnCraftFailed");
    };
    if this.m_isProcessing {
      this.m_craftingGameController.EnableTabs();
    };
    this.ResetProcess();
  }

  protected cb func OnHoldSelectedItem(evt: ref<inkPointerEvent>) -> Bool {
    this.m_progress = evt.GetHoldProgress();
    if evt.IsAction(n"craft_item") {
      if this.m_progress > 0.20 && this.m_isProcessing {
        this.m_progressButtonController.UpdateCraftProcess(evt);
        if this.m_isCraftable && !this.m_firstClicked {
          this.m_firstClicked = true;
          this.PlaySound(n"Item", n"OnCraftStarted");
        };
      };
    };
  }

  protected cb func OnReleaseSelectedItem(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"craft_item") {
      this.m_craftingGameController.EnableTabs();
      this.ResetProcess();
      if this.m_firstClicked {
        this.m_firstClicked = false;
        this.PlaySound(n"Item", n"OnCraftFailed");
      };
    };
  }

  protected func UpdateItemPreview(craftableController: ref<CraftableItemLogicController>) -> Void;

  protected cb func OnUninitialize() -> Bool {
    this.m_virtualListController.SetSource(null);
    this.m_virtualListController.SetClassifier(null);
    this.m_dataView.SetSource(null);
    this.m_dataView = null;
    this.m_dataSource = null;
    this.m_classifier = null;
    if IsDefined(this.m_currentSelected) {
      this.m_currentSelected.UnregisterFromCallback(n"OnPress", this, n"OnPressSelectedItem");
      this.m_currentSelected.UnregisterFromCallback(n"OnHold", this, n"OnHoldSelectedItem");
      this.m_currentSelected.UnregisterFromCallback(n"OnHoverOut", this, n"OnHoverOutSelectedItem");
      this.m_currentSelected.UnregisterFromCallback(n"OnRelease", this, n"OnReleaseSelectedItem");
    };
    this.m_virtualListController.UnregisterFromCallback(n"OnItemSelected", this, n"OnItemSelect");
    this.m_progressButtonController.ButtonController.UnregisterFromCallback(n"OnPress", this, n"OnButtonClick");
    this.m_progressButtonController.ButtonController.UnregisterFromCallback(n"OnHoverOver", this, n"SetItemButtonHintsHoverOver");
    this.m_progressButtonController.ButtonController.UnregisterFromCallback(n"OnHoverOut", this, n"SetItemButtonHintsHoverOut");
    this.m_doPlayFilterSounds = false;
    if this.m_firstClicked {
      this.PlaySound(n"Item", n"OnCraftFailed");
    };
  }

  protected final func SpawnItemTooltipAsync(parentWidget: wref<inkWidget>, opt callbackObject: ref<IScriptable>, opt callbackFunctionName: CName) -> Void {
    this.AsyncSpawnFromExternal(parentWidget, this.m_itemTooltipPath, n"itemTooltip", callbackObject, callbackFunctionName);
  }

  protected func SetItemButtonHintsHoverOver(evt: ref<inkPointerEvent>) -> Void;

  protected final func SetItemButtonHintsHoverOut(evt: ref<inkPointerEvent>) -> Void {
    this.m_buttonHintsController.RemoveButtonHint(n"craft_item");
    this.m_craftingGameController.EnableTabs();
    this.ResetProcess();
  }

  public final static func IsWeapon(type: gamedataEquipmentArea) -> Bool {
    return Equals(type, gamedataEquipmentArea.Weapon) || Equals(type, gamedataEquipmentArea.WeaponHeavy) || Equals(type, gamedataEquipmentArea.WeaponWheel) || Equals(type, gamedataEquipmentArea.WeaponLeft);
  }

  public final static func GetDropdownOption(identifier: ItemSortMode) -> ref<DropdownItemData> {
    let options: array<ref<DropdownItemData>> = SortingDropdownData.GetDefaultDropdownOptions();
    let i: Int32 = 0;
    while i < ArraySize(options) {
      if Equals(FromVariant<ItemSortMode>(options[i].identifier), identifier) {
        return options[i];
      };
      i += 1;
    };
    return null;
  }

  private final func ResetProcess() -> Void {
    this.m_progressButtonController.Reset();
    this.m_isProcessing = false;
  }

  private final func StartProcess() -> Void {
    if this.m_isProcessing {
      return;
    };
    this.m_isProcessing = true;
    this.m_progressButtonController.Unlock();
    this.m_progressButtonController.Start();
  }

  protected final func DispatchSelectDelayed(index: Uint32) -> Void {
    let delayedSelect: ref<DelayedSelect> = new DelayedSelect();
    delayedSelect.m_controller = this;
    delayedSelect.m_index = index;
    this.m_DelaySystem.DelayCallback(delayedSelect, this.TIME_UNTIL_SELECTION, false);
  }

  public final func Select(index: Uint32) -> Void {
    this.m_virtualListController.ToggleItem(index);
    this.m_virtualListController.SelectItem(index);
    this.m_virtualListController.ScrollToIndex(index);
    this.UpdateItemPreview(this.m_virtualListController.GetToggledItem() as CraftableItemLogicController);
  }

  protected final func IsEmptyData() -> Bool {
    return this.m_dataSource.Size() < 1u;
  }
}

public class DelayedSelect extends DelayCallback {

  public let m_controller: wref<CraftingMainLogicController>;

  public let m_index: Uint32;

  public func Call() -> Void {
    if IsDefined(this.m_controller) {
      this.m_controller.Select(this.m_index);
    };
  }
}
