
public class VehicleComponent extends ScriptableDeviceComponent {

  public let m_interaction: ref<InteractionComponent>;

  public let m_scanningComponent: ref<ScanningComponent>;

  public let m_customizationComponent: ref<VehicleCustomizationComponent>;

  @default(VehicleComponent, 0)
  public let m_damageLevel: Int32;

  public let m_coolerDestro: Bool;

  public let m_bumperFrontState: Int32;

  public let m_bumperBackState: Int32;

  public let m_visualDestructionSet: Bool;

  public let m_immuneInDecay: Bool;

  public let m_healthDecayThreshold: Float;

  @default(VehicleComponent, false)
  public let m_isBroadcastingHazardStims: Bool;

  public let m_healthStatPoolListener: ref<VehicleHealthStatPoolListener>;

  public let m_vehicleBlackboard: wref<IBlackboard>;

  public let m_radioState: Bool;

  public let m_mounted: Bool;

  public let m_enterTime: Float;

  public let m_mappinID: NewMappinID;

  public let m_quickhackMappinID: NewMappinID;

  public let m_ignoreAutoDoorClose: Bool;

  public let m_timeSystemCallbackID: Uint32;

  public let m_vehicleTPPCallbackID: ref<CallbackHandle>;

  public let m_vehicleSpeedCallbackID: ref<CallbackHandle>;

  public let m_carAlarmCallbackID: ref<CallbackHandle>;

  public let m_vehicleRPMCallbackID: ref<CallbackHandle>;

  public let m_vehicleDisableAlarmDelayID: DelayID;

  public let m_vehicleExitDelayId: DelayID;

  public let m_broadcasting: Bool;

  public let m_hasSpoiler: Bool;

  public let m_spoilerUp: Float;

  public let m_spoilerDown: Float;

  public let m_spoilerDeployed: Bool;

  public let m_hasTurboCharger: Bool;

  public let m_overheatEffectBlackboard: ref<worldEffectBlackboard>;

  public let m_overheatActive: Bool;

  public let m_hornOn: Bool;

  public let m_useAuxiliary: Bool;

  public let m_sirenPressTime: Float;

  public let m_radioPressTime: Float;

  public let m_raceClockTickID: DelayID;

  public let m_objectActionsCallbackCtrl: ref<gameObjectActionsCallbackController>;

  public let m_trunkNpcBody: wref<GameObject>;

  public let m_mountedPlayer: wref<PlayerPuppet>;

  @default(VehicleComponent, false)
  public let m_isIgnoredInTargetingSystem: Bool;

  @default(VehicleComponent, true)
  public let m_arePlayerHitShapesEnabled: Bool;

  public let m_uiWantedBarBB: wref<IBlackboard>;

  public let m_currentWantedLevelCallback: ref<CallbackHandle>;

  @default(VehicleComponent, 0)
  public let m_preventionPassengers: Int32;

  @default(VehicleComponent, 0)
  public let m_timeSinceLastHit: Float;

  @default(VehicleComponent, 0)
  public let m_dragTime: Float;

  public let effectBlackboard: ref<worldEffectBlackboard>;

  private let m_vehicleController: ref<vehicleController>;

  public final static func GetImmobilizedName() -> CName {
    return n"immobilized";
  }

  private final func OnGameAttach() -> Void {
    this.m_interaction = this.FindComponentByName(n"interaction") as InteractionComponent;
    this.m_scanningComponent = this.FindComponentByName(n"scanning") as ScanningComponent;
    this.m_customizationComponent = this.GetVehicle().GetCustomizationComponent();
    this.m_customizationComponent.SetComponentExcludedFromCustomization(this.GetVehicle().GetRecord().ExcludedComponentsCustomization());
    this.m_ignoreAutoDoorClose = false;
    this.m_radioState = false;
    this.SetImmortalityMode();
    this.m_healthStatPoolListener = new VehicleHealthStatPoolListener();
    this.m_healthStatPoolListener.m_owner = this.GetVehicle();
    GameInstance.GetStatPoolsSystem(this.GetVehicle().GetGame()).RequestRegisteringListener(Cast<StatsObjectID>(this.GetVehicle().GetEntityID()), gamedataStatPoolType.Health, this.m_healthStatPoolListener);
    this.m_vehicleBlackboard = this.GetVehicle().GetBlackboard();
    this.InitialVehcileSetup();
    this.RegisterToHUDManager(true);
    this.IsPlayerVehicle();
    this.LoadExplodedState();
    this.SetupThrusterFX();
    if this.GetVehicle().IsPlayerVehicle() && !this.GetPS().GetIsDestroyed() {
      this.CreateMappin();
    };
    this.m_healthDecayThreshold = TweakDBInterface.GetPoolValueModifierRecord(t"BaseStatPools.VehicleHealthDecay").RangeEnd();
    this.SetupCarAlarmHonkListener();
  }

  private final func OnGameDetach() -> Void {
    this.DestroyObjectActionsCallbackController();
    this.ClearImmortalityMode();
    GameInstance.GetStatPoolsSystem(this.GetVehicle().GetGame()).RequestUnregisteringListener(Cast<StatsObjectID>(this.GetVehicle().GetEntityID()), gamedataStatPoolType.Health, this.m_healthStatPoolListener);
    this.DestroyMappin();
    this.RegisterToHUDManager(false);
    this.UnregisterListeners();
    this.UnregisterCarAlarmHonkListener();
    this.UnregisterInputListener();
    this.DoPanzerCleanup();
    this.DoPreventionVehicleCleanup();
  }

  public final static func IsDestroyed(gi: GameInstance, vehicleID: EntityID) -> Bool {
    let vehicle: wref<VehicleObject>;
    VehicleComponent.GetVehicleFromID(gi, vehicleID, vehicle);
    if !IsDefined(vehicle) {
      return false;
    };
    return vehicle.IsDestroyed();
  }

  public final static func HasFlatTire(gi: GameInstance, vehicleID: EntityID) -> Bool {
    let vehicle: wref<VehicleObject>;
    let wheeledObject: wref<WheeledObject>;
    VehicleComponent.GetVehicleFromID(gi, vehicleID, vehicle);
    if !IsDefined(vehicle) {
      return false;
    };
    wheeledObject = vehicle as WheeledObject;
    if !IsDefined(wheeledObject) {
      return false;
    };
    return wheeledObject.GetFlatTireIndex() != -1;
  }

  public final static func GetDriverSlotName() -> CName {
    return n"seat_front_left";
  }

  public final static func GetFrontPassengerSlotName() -> CName {
    return n"seat_front_right";
  }

  public final static func GetBackLeftPassengerSlotName() -> CName {
    return n"seat_back_left";
  }

  public final static func GetBackRightPassengerSlotName() -> CName {
    return n"seat_back_right";
  }

  public final static func GetPassengersSlotNames(slotNames: script_ref<[CName]>) -> Void {
    ArrayResize(Deref(slotNames), 4);
    Deref(slotNames)[0] = n"seat_front_left";
    Deref(slotNames)[1] = n"seat_front_right";
    Deref(slotNames)[2] = n"seat_back_left";
    Deref(slotNames)[3] = n"seat_back_right";
  }

  public final static func IsDriverSlot(slotId: CName) -> Bool {
    let slotName: String = NameToString(slotId);
    let driveSlotName: String = NameToString(n"seat_front_left");
    return StrContains(slotName, driveSlotName);
  }

  public final static func IsSameSlot(slotId1: CName, slotId2: CName) -> Bool {
    let slotName1: String = NameToString(slotId1);
    let slotName2: String = NameToString(slotId2);
    if StrLen(slotName1) > StrLen(slotName2) {
      return StrContains(slotName1, slotName2);
    };
    return StrContains(slotName2, slotName1);
  }

  public final static func GetDriverSlotID() -> MountingSlotId {
    let slotID: MountingSlotId;
    slotID.id = n"seat_front_left";
    return slotID;
  }

  public final static func GetFrontPassengerSlotID() -> MountingSlotId {
    let slotID: MountingSlotId;
    slotID.id = n"seat_front_right";
    return slotID;
  }

  public final static func GetOwnerVehicleSpeed(gi: GameInstance, owner: wref<GameObject>) -> Float {
    let vehicle: wref<VehicleObject>;
    if !IsDefined(owner) {
      return 0.00;
    };
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(owner.GetEntityID()) {
      return 0.00;
    };
    if !VehicleComponent.GetVehicle(gi, owner.GetEntityID(), vehicle) {
      return 0.00;
    };
    return vehicle.GetCurrentSpeed();
  }

  public final static func IsMountedToVehicle(gi: GameInstance, owner: wref<GameObject>) -> Bool {
    if !IsDefined(owner) {
      return false;
    };
    return VehicleComponent.IsMountedToVehicle(gi, owner.GetEntityID());
  }

  public final static func IsMountedToVehicle(gi: GameInstance, ownerID: EntityID) -> Bool {
    let vehicle: wref<VehicleObject>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(ownerID) {
      return false;
    };
    if !VehicleComponent.GetVehicle(gi, ownerID, vehicle) {
      return false;
    };
    return true;
  }

  public final static func IsMountedToProvidedVehicle(gi: GameInstance, ownerID: EntityID, vehicle: wref<VehicleObject>) -> Bool {
    let mountInfo: MountingInfo;
    let mountedVeh: EntityID;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(ownerID) || !IsDefined(vehicle) {
      return false;
    };
    if VehicleComponent.IsMountedToVehicle(gi, ownerID) {
      mountInfo = GameInstance.GetMountingFacility(gi).GetMountingInfoSingleWithIds(ownerID);
      mountedVeh = mountInfo.parentId;
      if mountedVeh == vehicle.GetEntityID() {
        return true;
      };
    };
    return false;
  }

  public final static func IsDriver(gi: GameInstance, owner: wref<GameObject>) -> Bool {
    if !IsDefined(owner) {
      return false;
    };
    return VehicleComponent.IsDriver(gi, owner.GetEntityID());
  }

  public final static func IsDriver(gi: GameInstance, ownerID: EntityID) -> Bool {
    let mountInfo: MountingInfo;
    let vehicle: wref<VehicleObject>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(ownerID) {
      return false;
    };
    mountInfo = GameInstance.GetMountingFacility(gi).GetMountingInfoSingleWithIds(ownerID);
    if !EntityID.IsDefined(mountInfo.parentId) {
      return false;
    };
    if !VehicleComponent.IsDriverSlot(mountInfo.slotId.id) {
      return false;
    };
    if !VehicleComponent.GetVehicle(gi, ownerID, vehicle) {
      return false;
    };
    if vehicle == (vehicle as AVObject) {
      return false;
    };
    return true;
  }

  public final static func GetVehicle(gi: GameInstance, owner: wref<GameObject>, out vehicle: wref<GameObject>) -> Bool {
    let vehicleObj: wref<VehicleObject>;
    if !IsDefined(owner) {
      return false;
    };
    if !VehicleComponent.GetVehicle(gi, owner.GetEntityID(), vehicleObj) {
      return false;
    };
    vehicle = vehicleObj;
    return vehicle != null;
  }

  public final static func GetVehicle(gi: GameInstance, owner: wref<GameObject>, out vehicle: wref<VehicleObject>) -> Bool {
    if !IsDefined(owner) {
      return false;
    };
    return VehicleComponent.GetVehicle(gi, owner.GetEntityID(), vehicle);
  }

  public final static func GetVehicle(gi: GameInstance, ownerID: EntityID, out vehicle: wref<VehicleObject>) -> Bool {
    let mountInfo: MountingInfo;
    let vehicleID: EntityID;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(ownerID) {
      return false;
    };
    mountInfo = GameInstance.GetMountingFacility(gi).GetMountingInfoSingleWithIds(ownerID);
    vehicleID = mountInfo.parentId;
    if !EntityID.IsDefined(vehicleID) {
      return false;
    };
    return VehicleComponent.GetVehicleFromID(gi, vehicleID, vehicle);
  }

  public final static func GetVehicleFromID(gi: GameInstance, vehicleID: EntityID, out vehicle: wref<VehicleObject>) -> Bool {
    if !EntityID.IsDefined(vehicleID) {
      return false;
    };
    vehicle = GameInstance.FindEntityByID(gi, vehicleID) as VehicleObject;
    if IsDefined(vehicle) {
      return true;
    };
    return false;
  }

  public final static func GetVehicleID(gi: GameInstance, owner: wref<GameObject>, out vehicleID: EntityID) -> Bool {
    if !IsDefined(owner) {
      return false;
    };
    return VehicleComponent.GetVehicleID(gi, owner.GetEntityID(), vehicleID);
  }

  public final static func GetVehicleID(gi: GameInstance, ownerID: EntityID, out vehicleID: EntityID) -> Bool {
    let vehicle: wref<VehicleObject>;
    if !VehicleComponent.GetVehicle(gi, ownerID, vehicle) {
      return false;
    };
    vehicleID = vehicle.GetEntityID();
    if !EntityID.IsDefined(vehicleID) {
      return false;
    };
    return true;
  }

  public final static func GetVehicleRecord(gi: GameInstance, owner: wref<GameObject>, out vehicleRecord: ref<Vehicle_Record>) -> Bool {
    if !IsDefined(owner) {
      return false;
    };
    return VehicleComponent.GetVehicleRecord(gi, owner.GetEntityID(), vehicleRecord);
  }

  public final static func GetVehicleRecord(gi: GameInstance, ownerID: EntityID, out vehicleRecord: ref<Vehicle_Record>) -> Bool {
    let vehicle: wref<VehicleObject>;
    if !VehicleComponent.GetVehicle(gi, ownerID, vehicle) {
      return false;
    };
    vehicleRecord = TweakDBInterface.GetVehicleRecord(vehicle.GetRecordID());
    if !IsDefined(vehicleRecord) {
      return false;
    };
    return true;
  }

  public final static func GetVehicleRecord(vehicle: wref<VehicleObject>, out vehicleRecord: ref<Vehicle_Record>) -> Bool {
    if !IsDefined(vehicle) {
      return false;
    };
    vehicleRecord = TweakDBInterface.GetVehicleRecord(vehicle.GetRecordID());
    if !IsDefined(vehicleRecord) {
      return false;
    };
    return true;
  }

  public final static func GetVehicleRecordFromID(gi: GameInstance, vehicleID: EntityID, out vehicleRecord: ref<Vehicle_Record>) -> Bool {
    let vehicle: wref<VehicleObject>;
    if !VehicleComponent.GetVehicleFromID(gi, vehicleID, vehicle) {
      return false;
    };
    vehicleRecord = TweakDBInterface.GetVehicleRecord(vehicle.GetRecordID());
    if !IsDefined(vehicleRecord) {
      return false;
    };
    return true;
  }

  public final static func GetDriver(gi: GameInstance, vehicle: wref<VehicleObject>, vehicleID: EntityID) -> wref<GameObject> {
    let playerObj: wref<GameObject>;
    let playerSystem: ref<PlayerSystem>;
    if vehicle.IsVehicleRemoteControlled() {
      playerSystem = GameInstance.GetPlayerSystem(gi);
      playerObj = playerSystem.GetLocalPlayerMainGameObject();
      if IsDefined(playerObj) {
        return playerObj;
      };
      return null;
    };
    return VehicleComponent.GetDriverMounted(gi, vehicleID);
  }

  public final static func IsDriverSeatOccupiedByDeadNPC(gi: GameInstance, vehicleID: EntityID) -> Bool {
    let driver: ref<ScriptedPuppet> = VehicleComponent.GetDriverMounted(gi, vehicleID) as ScriptedPuppet;
    return IsDefined(driver) && !driver.IsActive();
  }

  public final static func IsMountedToVehicleWithDriverSeatOccupiedByDeadNPC(gi: GameInstance, passengerID: EntityID) -> Bool {
    let driver: ref<ScriptedPuppet>;
    let i: Int32;
    let mountInfos: array<MountingInfo>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(passengerID) {
      return false;
    };
    mountInfos = GameInstance.GetMountingFacility(gi).GetMountingInfoMultipleWithIds(passengerID);
    i = 0;
    while i < ArraySize(mountInfos) {
      if VehicleComponent.IsDriverSlot(mountInfos[i].slotId.id) {
        driver = GameInstance.FindEntityByID(gi, mountInfos[i].childId) as ScriptedPuppet;
        return IsDefined(driver) && !driver.IsActive();
      };
      i += 1;
    };
    return false;
  }

  public final static func GetDriverMounted(gi: GameInstance, vehicleID: EntityID) -> wref<GameObject> {
    let i: Int32;
    let mountInfos: array<MountingInfo>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) {
      return null;
    };
    mountInfos = GameInstance.GetMountingFacility(gi).GetMountingInfoMultipleWithIds(vehicleID);
    i = 0;
    while i < ArraySize(mountInfos) {
      if VehicleComponent.IsDriverSlot(mountInfos[i].slotId.id) {
        return GameInstance.FindEntityByID(gi, mountInfos[i].childId) as GameObject;
      };
      i += 1;
    };
    return null;
  }

  public final static func PlayerPassengerHasGodModeFromCheatSystem(gi: GameInstance, vehicleID: EntityID, gmType: gameGodModeType) -> Bool {
    let godModeSources: array<CName>;
    let godModeSystem: ref<GodModeSystem>;
    let mountingInfo: MountingInfo;
    let playerObj: wref<GameObject>;
    let playerSystem: ref<PlayerSystem>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) {
      return false;
    };
    godModeSystem = GameInstance.GetGodModeSystem(gi);
    playerSystem = GameInstance.GetPlayerSystem(gi);
    playerObj = playerSystem.GetLocalPlayerMainGameObject();
    if !IsDefined(playerObj) || !godModeSystem.HasGodMode(playerObj.GetEntityID(), gmType) {
      return false;
    };
    mountingInfo = GameInstance.GetMountingFacility(gi).GetMountingInfoSingleWithIds(playerObj.GetEntityID());
    if mountingInfo.parentId != vehicleID {
      return false;
    };
    godModeSources = godModeSystem.GetGodModeSources(playerObj.GetEntityID(), gmType);
    return ArrayContains(godModeSources, n"cheat system");
  }

  public final static func CanBeDriven(gi: GameInstance, ownerID: EntityID) -> Bool {
    let vehicle: wref<VehicleObject>;
    if !VehicleComponent.GetVehicle(gi, ownerID, vehicle) {
      return false;
    };
    return VehicleComponent.CanBeDriven(gi, vehicle);
  }

  public final static func CanBeDriven(gi: GameInstance, vehicle: wref<VehicleObject>) -> Bool {
    if !IsDefined(vehicle) || vehicle.IsDestroyed() || vehicle.GetAIComponent().GetReservedSeatsCount() > 0u || VehicleComponent.HasActiveAutopilot(gi, vehicle) || VehicleComponent.IsExecutingAnyCommand(gi, vehicle) || vehicle.IsPerformingSceneAnimation() || !vehicle.CommandsFromDriverEnabled() {
      return false;
    };
    return true;
  }

  public final static func HasActiveAutopilot(gi: GameInstance, vehicle: wref<VehicleObject>) -> Bool {
    if IsDefined(vehicle) {
      return vehicle.GetBlackboard().GetBool(GetAllBlackboardDefs().Vehicle.IsAutopilotOn);
    };
    return false;
  }

  public final static func HasActiveAutopilot(gi: GameInstance, ownerID: EntityID) -> Bool {
    let vehicle: wref<VehicleObject>;
    if !VehicleComponent.GetVehicle(gi, ownerID, vehicle) {
      return false;
    };
    return VehicleComponent.HasActiveAutopilot(gi, vehicle);
  }

  public final static func IsExecutingAnyCommand(gi: GameInstance, vehicle: wref<VehicleObject>) -> Bool {
    if IsDefined(vehicle) {
      return vehicle.IsExecutingAnyCommand();
    };
    return false;
  }

  public final static func HasActiveDriver(gi: GameInstance, vehicle: wref<VehicleObject>, vehicleID: EntityID) -> Bool {
    let driver: wref<GameObject> = VehicleComponent.GetDriver(gi, vehicle, vehicleID);
    if IsDefined(driver) && ScriptedPuppet.IsActive(driver) {
      return true;
    };
    return false;
  }

  public final static func HasActiveDriverMounted(gi: GameInstance, vehicleID: EntityID) -> Bool {
    let driver: wref<GameObject> = VehicleComponent.GetDriverMounted(gi, vehicleID);
    if IsDefined(driver) && ScriptedPuppet.IsActive(driver) {
      return true;
    };
    return false;
  }

  public final static func QueueEventToAllPassengersRandomly(gi: GameInstance, id: EntityID, evt: ref<Event>, min: Float, max: Float) -> Bool {
    let delayTime: Float;
    let i: Int32;
    let passenger: wref<GameObject>;
    let passengers: array<wref<GameObject>>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(id) {
      return false;
    };
    VehicleComponent.GetAllPassengers(gi, id, false, passengers);
    i = 0;
    while i < ArraySize(passengers) {
      passenger = passengers[i];
      if IsDefined(passenger) {
        delayTime = delayTime + RandRangeF(min, max);
        GameInstance.GetDelaySystem(gi).DelayEvent(passenger, evt, delayTime);
      };
      i += 1;
    };
    if !IsDefined(passenger) {
      return false;
    };
    return true;
  }

  public final static func QueueEventToPassenger(gi: GameInstance, vehicleID: EntityID, slotID: MountingSlotId, evt: ref<Event>, opt delay: Float, opt randomDelay: Bool) -> Bool {
    let delayTime: Float;
    let i: Int32;
    let passenger: wref<GameObject>;
    let passengers: array<wref<GameObject>>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) {
      return false;
    };
    VehicleComponent.GetAllPassengers(gi, vehicleID, false, passengers);
    i = 0;
    while i < ArraySize(passengers) {
      passenger = passengers[i];
      if IsDefined(passenger) {
        if delay > 0.00 {
          GameInstance.GetDelaySystem(gi).DelayEvent(passenger, evt, delay);
        } else {
          if randomDelay {
            delayTime = delayTime + RandRangeF(0.10, 2.00);
            GameInstance.GetDelaySystem(gi).DelayEvent(passenger, evt, delayTime);
          } else {
            passenger.QueueEvent(evt);
          };
        };
      };
      i += 1;
    };
    if !IsDefined(passenger) {
      return false;
    };
    return true;
  }

  public final static func QueueEventToPassenger(gi: GameInstance, vehicle: wref<VehicleObject>, slotID: MountingSlotId, evt: ref<Event>, opt delay: Float, opt randomDelay: Bool) -> Bool {
    if !IsDefined(vehicle) {
      return false;
    };
    return VehicleComponent.QueueEventToPassenger(gi, vehicle.GetEntityID(), slotID, evt, delay, randomDelay);
  }

  public final static func HasPassengersWithThreatOnPlayer(gi: GameInstance, vehicleID: EntityID) -> Bool {
    let i: Int32;
    let passengerThreats: array<TrackedLocation>;
    let passengers: array<wref<GameObject>>;
    let player: wref<GameObject>;
    VehicleComponent.GetAllPassengers(gi, vehicleID, false, passengers);
    i = 0;
    while i < ArraySize(passengers) {
      passengerThreats = (passengers[i] as ScriptedPuppet).GetTargetTrackerComponent().GetThreats(false);
      if ArraySize(passengerThreats) > 0 && TargetTrackingExtension.GetPlayerFromThreats(passengerThreats, player) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final static func GetAllPassengers(gi: GameInstance, vehicleID: EntityID, includeTrunkBody: Bool, passengers: script_ref<[wref<GameObject>]>) -> Void {
    let i: Int32;
    let mountInfos: array<MountingInfo>;
    let passenger: wref<GameObject>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) {
      return;
    };
    mountInfos = GameInstance.GetMountingFacility(gi).GetMountingInfoMultipleWithIds(vehicleID);
    i = 0;
    while i < ArraySize(mountInfos) {
      if !includeTrunkBody {
        if Equals(mountInfos[i].slotId.id, n"trunk_body") {
        } else {
          passenger = GameInstance.FindEntityByID(gi, mountInfos[i].childId) as GameObject;
          if IsDefined(passenger) {
            ArrayPush(Deref(passengers), passenger);
          };
        };
      };
      passenger = GameInstance.FindEntityByID(gi, mountInfos[i].childId) as GameObject;
      if IsDefined(passenger) {
        ArrayPush(Deref(passengers), passenger);
      };
      i += 1;
    };
    return;
  }

  public final static func QueueEventToAllPassengers(gi: GameInstance, vehicleID: EntityID, evt: ref<Event>, opt delay: Float, opt randomDelay: Bool) -> Bool {
    let delayTime: Float;
    let i: Int32;
    let passenger: wref<GameObject>;
    let passengers: array<wref<GameObject>>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) {
      return false;
    };
    VehicleComponent.GetAllPassengers(gi, vehicleID, false, passengers);
    i = 0;
    while i < ArraySize(passengers) {
      passenger = passengers[i];
      if IsDefined(passenger) {
        if delay > 0.00 {
          GameInstance.GetDelaySystem(gi).DelayEvent(passenger, evt, delay);
        } else {
          if randomDelay {
            delayTime = delayTime + RandRangeF(0.10, 2.00);
            GameInstance.GetDelaySystem(gi).DelayEvent(passenger, evt, delayTime);
          } else {
            passenger.QueueEvent(evt);
          };
        };
      };
      i += 1;
    };
    if !IsDefined(passenger) {
      return false;
    };
    return true;
  }

  public final static func QueueEventToAllPassengers(gi: GameInstance, vehicle: wref<VehicleObject>, evt: ref<Event>, opt delay: Float, opt randomDelay: Bool) -> Bool {
    if !IsDefined(vehicle) {
      return false;
    };
    return VehicleComponent.QueueEventToAllPassengers(gi, vehicle.GetEntityID(), evt, delay, randomDelay);
  }

  public final static func QueueEventToAllNonFriendlyAggressivePassengers(gi: GameInstance, vehicleID: EntityID, evt: ref<Event>, opt delay: Float) -> Bool {
    let i: Int32;
    let passenger: wref<ScriptedPuppet>;
    let passengers: array<wref<GameObject>>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) {
      return false;
    };
    VehicleComponent.GetAllPassengers(gi, vehicleID, false, passengers);
    i = 0;
    while i < ArraySize(passengers) {
      passenger = passengers[i] as ScriptedPuppet;
      if IsDefined(passenger) && !IsFriendlyTowardsPlayer(passenger) && passenger.IsAggressive() {
        if delay > 0.00 {
          GameInstance.GetDelaySystem(gi).DelayEvent(passenger, evt, delay);
        } else {
          passenger.QueueEvent(evt);
        };
      };
      i += 1;
    };
    if !IsDefined(passenger) {
      return false;
    };
    return true;
  }

  public final static func QueueEventToPassengers(gi: GameInstance, vehicleID: EntityID, evt: ref<Event>, const passengers: script_ref<[wref<GameObject>]>, opt delay: Bool, opt maxDelayAmount: Float) -> Bool {
    let delayTime: Float;
    let i: Int32;
    let maxDelay: Float;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) || ArraySize(Deref(passengers)) == 0 {
      return false;
    };
    i = 0;
    while i < ArraySize(Deref(passengers)) {
      if IsDefined(Deref(passengers)[i]) {
        if delay {
          maxDelay = 2.00;
          if maxDelayAmount > 0.00 {
            maxDelay = maxDelayAmount;
          };
          delayTime = delayTime + RandRangeF(0.10, maxDelay);
          GameInstance.GetDelaySystem(gi).DelayEvent(Deref(passengers)[i], evt, delayTime);
        } else {
          Deref(passengers)[i].QueueEvent(evt);
        };
      };
      i += 1;
    };
    return true;
  }

  public final static func QueueExitEventToAllNonFriendlyActivePassengers(gi: GameInstance, vehicleID: EntityID, executionOwner: ref<GameObject>, opt broadcastHijack: Bool, opt delay: Bool) -> Bool {
    let active: Bool;
    let attitude: EAIAttitude;
    let broadcaster: ref<StimBroadcasterComponent>;
    let delayTime: Float;
    let evt: ref<AIEvent>;
    let exitEvent: ref<AIEvent>;
    let exitInPanicEvent: ref<AIEvent>;
    let i: Int32;
    let mountInfos: array<MountingInfo>;
    let passenger: wref<GameObject>;
    let playerMountInfo: MountingInfo;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) {
      return false;
    };
    playerMountInfo = GameInstance.GetMountingFacility(gi).GetMountingInfoSingleWithObjects(GameInstance.GetPlayerSystem(gi).GetLocalPlayerMainGameObject());
    mountInfos = GameInstance.GetMountingFacility(gi).GetMountingInfoMultipleWithIds(vehicleID);
    i = 0;
    while i < ArraySize(mountInfos) {
      if Equals(mountInfos[i].slotId.id, n"trunk_body") {
        ArrayErase(mountInfos, i);
        break;
      };
      i += 1;
    };
    exitEvent = new AIEvent();
    exitEvent.name = n"ExitVehicle";
    exitInPanicEvent = new AIEvent();
    exitInPanicEvent.name = n"ExitVehicleInPanic";
    delayTime = RandRangeF(0.20, 0.40);
    broadcaster = GameInstance.GetPlayerSystem(gi).GetLocalPlayerMainGameObject().GetStimBroadcasterComponent();
    i = 0;
    while i < ArraySize(mountInfos) {
      passenger = GameInstance.FindEntityByID(gi, mountInfos[i].childId) as GameObject;
      active = VehicleComponent.IsSlotOccupiedByActivePassenger(gi, vehicleID, mountInfos[i].slotId.id);
      VehicleComponent.GetAttitudeOfPassenger(gi, vehicleID, mountInfos[i].slotId, attitude);
      if NotEquals(playerMountInfo.slotId.id, mountInfos[i].slotId.id) {
        if IsDefined(passenger) && active && NotEquals(attitude, EAIAttitude.AIA_Friendly) {
          evt = (passenger as ScriptedPuppet).IsEnemy() ? exitEvent : exitInPanicEvent;
          if delay {
            delayTime = delayTime + RandRangeF(0.20, 0.40);
            GameInstance.GetDelaySystem(gi).DelayEvent(passenger, evt, delayTime);
          } else {
            passenger.QueueEvent(evt);
          };
        };
      };
      if IsDefined(broadcaster) && broadcastHijack {
        broadcaster.SendDrirectStimuliToTarget(executionOwner, gamedataStimType.HijackVehicle, passenger);
      };
      i += 1;
    };
    if !IsDefined(passenger) {
      return false;
    };
    return true;
  }

  public final static func QueueHijackExitEventToInactiveDriver(gi: GameInstance, vehicle: wref<VehicleObject>) -> Bool {
    let driver: wref<GameObject>;
    if !GameInstance.IsValid(gi) || !IsDefined(vehicle) {
      return false;
    };
    driver = VehicleComponent.GetDriverMounted(gi, vehicle.GetEntityID());
    if driver == null || ScriptedPuppet.IsActive(driver) {
      return false;
    };
    VehicleComponent.OpenDoor(vehicle, VehicleComponent.GetDriverSlotID(), 1.00);
    GameInstance.GetWorkspotSystem(vehicle.GetGame()).UnmountFromVehicle(vehicle, driver, false, n"deadstealing");
    return true;
  }

  public final static func CheckIfPassengersCanLeaveCar(gi: GameInstance, vehicleID: EntityID, passengersCanLeaveCar: script_ref<[wref<GameObject>]>, passengersCantLeaveCar: script_ref<[wref<GameObject>]>) -> Void {
    let active: Bool;
    let passenger: wref<GameObject>;
    let mountInfos: array<MountingInfo> = GameInstance.GetMountingFacility(gi).GetMountingInfoMultipleWithIds(vehicleID);
    let workspotSystem: ref<WorkspotGameSystem> = GameInstance.GetWorkspotSystem(gi);
    let i: Int32 = 0;
    while i < ArraySize(mountInfos) {
      passenger = GameInstance.FindEntityByID(gi, mountInfos[i].childId) as GameObject;
      active = VehicleComponent.IsSlotOccupiedByActivePassenger(gi, vehicleID, mountInfos[i].slotId.id);
      if IsDefined(passenger) && active {
        if workspotSystem.HasExitNodes(passenger, true, false, true) {
          ArrayPush(Deref(passengersCanLeaveCar), passenger);
        } else {
          ArrayPush(Deref(passengersCantLeaveCar), passenger);
        };
      };
      i += 1;
    };
  }

  public final static func IsAnyPassengerCrowd(gi: GameInstance, vehicle: wref<VehicleObject>) -> Bool {
    let i: Int32;
    let mountInfos: array<MountingInfo>;
    let passenger: wref<NPCPuppet>;
    if !GameInstance.IsValid(gi) || !IsDefined(vehicle) {
      return false;
    };
    mountInfos = GameInstance.GetMountingFacility(gi).GetMountingInfoMultipleWithObjects(vehicle);
    i = 0;
    while i < ArraySize(mountInfos) {
      passenger = GameInstance.FindEntityByID(gi, mountInfos[i].childId) as NPCPuppet;
      if passenger.IsCrowd() {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final static func IsSlotAvailable(gi: GameInstance, vehicle: wref<VehicleObject>, slotName: CName) -> Bool {
    if !IsDefined(vehicle) || !IsNameValid(slotName) {
      return false;
    };
    if !VehicleComponent.HasSlot(gi, vehicle, slotName) {
      return false;
    };
    if VehicleComponent.IsSlotOccupied(gi, vehicle.GetEntityID(), slotName) {
      return false;
    };
    return true;
  }

  public final static func IsSlotOccupied(gi: GameInstance, vehicleID: EntityID, slotName: CName) -> Bool {
    let vehicleSlotID: MountingSlotId;
    if !IsNameValid(slotName) {
      return false;
    };
    vehicleSlotID.id = slotName;
    if !VehicleComponent.IsSlotOccupied(gi, vehicleID, vehicleSlotID) {
      return false;
    };
    return true;
  }

  public final static func IsSlotOccupied(gi: GameInstance, vehicleID: EntityID, vehicleSlotID: MountingSlotId) -> Bool {
    let mountInfo: MountingInfo;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) {
      return false;
    };
    mountInfo = GameInstance.GetMountingFacility(gi).GetMountingInfoSingleWithIds(vehicleID, vehicleSlotID);
    if !EntityID.IsDefined(mountInfo.childId) {
      return false;
    };
    return true;
  }

  public final static func IsSlotOccupiedByOtherEntity(gi: GameInstance, vehicleID: EntityID, vehicleSlotID: MountingSlotId, expectedEntity: EntityID) -> Bool {
    let mountInfo: MountingInfo;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) || !EntityID.IsDefined(expectedEntity) {
      return false;
    };
    mountInfo = GameInstance.GetMountingFacility(gi).GetMountingInfoSingleWithIds(vehicleID, vehicleSlotID);
    if !EntityID.IsDefined(mountInfo.childId) {
      return false;
    };
    return expectedEntity != mountInfo.childId;
  }

  public final static func IsSlotOccupiedByActivePassenger(gi: GameInstance, vehicleID: EntityID, slotName: CName) -> Bool {
    let vehicleSlotID: MountingSlotId;
    if !IsNameValid(slotName) {
      return false;
    };
    vehicleSlotID.id = slotName;
    if !VehicleComponent.IsSlotOccupiedByActivePassenger(gi, vehicleID, vehicleSlotID) {
      return false;
    };
    return true;
  }

  public final static func IsSlotOccupiedByActivePassenger(gi: GameInstance, vehicleID: EntityID, vehicleSlotID: MountingSlotId) -> Bool {
    let mountInfo: MountingInfo;
    let passanger: wref<GameObject>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) {
      return false;
    };
    mountInfo = GameInstance.GetMountingFacility(gi).GetMountingInfoSingleWithIds(vehicleID, vehicleSlotID);
    if !EntityID.IsDefined(mountInfo.childId) {
      return false;
    };
    passanger = GameInstance.FindEntityByID(gi, mountInfo.childId) as GameObject;
    if !IsDefined(passanger) || !ScriptedPuppet.IsActive(passanger) {
      return false;
    };
    return true;
  }

  public final static func HasOnlyOneActivePassenger(gi: GameInstance, vehicleID: EntityID) -> Bool {
    let activePassangers: Int32;
    if !VehicleComponent.GetNumberOfActivePassengers(gi, vehicleID, activePassangers) {
      return false;
    };
    if activePassangers == 1 {
      return true;
    };
    return false;
  }

  public final static func GetNumberOfActivePassengers(gi: GameInstance, vehicleID: EntityID, out activePassangers: Int32) -> Bool {
    let i: Int32;
    let mountInfos: array<MountingInfo>;
    let passanger: wref<GameObject>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) {
      return false;
    };
    mountInfos = GameInstance.GetMountingFacility(gi).GetMountingInfoMultipleWithIds(vehicleID);
    i = 0;
    while i < ArraySize(mountInfos) {
      passanger = GameInstance.FindEntityByID(gi, mountInfos[i].childId) as GameObject;
      if IsDefined(passanger) && ScriptedPuppet.IsActive(passanger) {
        activePassangers += 1;
      };
      i += 1;
    };
    return true;
  }

  public final static func HasAnyPreventionPassengers(vehicle: wref<VehicleObject>) -> Bool {
    let vehicleComp: ref<VehicleComponent>;
    if IsDefined(vehicle) {
      vehicleComp = vehicle.GetVehicleComponent();
      if IsDefined(vehicleComp) {
        return vehicleComp.HasPreventionPassenger();
      };
    };
    return false;
  }

  public final static func HasAnyActivePassengers(gi: GameInstance, vehicleID: EntityID) -> Bool {
    let i: Int32;
    let mountInfos: array<MountingInfo>;
    let passanger: wref<GameObject>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) {
      return false;
    };
    mountInfos = GameInstance.GetMountingFacility(gi).GetMountingInfoMultipleWithIds(vehicleID);
    i = 0;
    while i < ArraySize(mountInfos) {
      passanger = GameInstance.FindEntityByID(gi, mountInfos[i].childId) as GameObject;
      if IsDefined(passanger) && ScriptedPuppet.IsActive(passanger) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final static func IsVehicleOccupied(gi: GameInstance, vehicle: wref<VehicleObject>) -> Bool {
    let mountInfo: MountingInfo;
    if !GameInstance.IsValid(gi) || !IsDefined(vehicle) {
      return false;
    };
    mountInfo = GameInstance.GetMountingFacility(gi).GetMountingInfoSingleWithObjects(vehicle);
    return IMountingFacility.InfoHasChild(mountInfo);
  }

  public final static func IsVehicleOccupiedByHostile(vehicleID: EntityID, passenger: wref<GameObject>) -> Bool {
    let attitudeOwner: ref<AttitudeAgent>;
    let attitudeTarget: ref<AttitudeAgent>;
    let i: Int32;
    let target: wref<GameObject>;
    let targets: array<wref<GameObject>>;
    if !IsDefined(passenger) {
      return false;
    };
    attitudeOwner = passenger.GetAttitudeAgent();
    VehicleComponent.GetAllPassengers(passenger.GetGame(), vehicleID, false, targets);
    i = 0;
    while i < ArraySize(targets) {
      target = targets[i];
      if !IsDefined(target) || !ScriptedPuppet.IsActive(target) {
      } else {
        attitudeTarget = target.GetAttitudeAgent();
        if Equals(attitudeOwner.GetAttitudeTowards(attitudeTarget), EAIAttitude.AIA_Hostile) {
          return true;
        };
      };
      i += 1;
    };
    return false;
  }

  public final func IsVehicleParked() -> Bool {
    return this.GetVehicle().IsVehicleParked();
  }

  public final static func SetAnimsetOverrideForPassenger(passenger: wref<GameObject>, value: Float) -> [CName] {
    let animsetOverides: array<CName>;
    let mountInfo: MountingInfo;
    if !IsDefined(passenger) {
      return animsetOverides;
    };
    mountInfo = GameInstance.GetMountingFacility(passenger.GetGame()).GetMountingInfoSingleWithIds(passenger.GetEntityID());
    if !EntityID.IsDefined(mountInfo.parentId) {
      return animsetOverides;
    };
    return VehicleComponent.SetAnimsetOverrideForPassenger(passenger, mountInfo.parentId, mountInfo.slotId.id, value);
  }

  public final static func SetAnimsetOverrideForPassenger(passenger: wref<GameObject>, vehicleID: EntityID, slotName: CName, value: Float) -> [CName] {
    let animsetOverides: array<CName>;
    let boneName: CName;
    let evt: ref<AnimWrapperWeightSetter>;
    let i: Int32;
    let vehicle: wref<VehicleObject>;
    if !IsDefined(passenger) {
      return animsetOverides;
    };
    if !EntityID.IsDefined(vehicleID) || !IsNameValid(slotName) {
      return animsetOverides;
    };
    vehicle = GameInstance.FindEntityByID(passenger.GetGame(), vehicleID) as VehicleObject;
    if !IsDefined(vehicle) {
      return animsetOverides;
    };
    if vehicle == (vehicle as BikeObject) {
      boneName = slotName;
    } else {
      boneName = vehicle.GetBoneNameFromSlot(slotName);
    };
    ArrayPush(animsetOverides, vehicle.GetAnimsetOverrideForPassenger(boneName));
    ArrayPush(animsetOverides, boneName);
    i = 0;
    while i < ArraySize(animsetOverides) {
      evt = new AnimWrapperWeightSetter();
      evt.key = animsetOverides[i];
      evt.value = value;
      passenger.QueueEvent(evt);
      i += 1;
    };
    return animsetOverides;
  }

  public final static func CheckVehicleDesiredTag(vehicle: wref<VehicleObject>, desiredTag: CName) -> Bool {
    let tags: array<CName>;
    let vehicleRecord: ref<Vehicle_Record>;
    if !VehicleComponent.GetVehicleRecord(vehicle, vehicleRecord) {
      return false;
    };
    tags = vehicleRecord.Tags();
    if ArrayContains(tags, desiredTag) {
      return true;
    };
    return false;
  }

  public final static func CheckVehicleDesiredTag(gi: GameInstance, owner: wref<GameObject>, desiredTag: CName) -> Bool {
    let vehicleRecord: ref<Vehicle_Record>;
    if !VehicleComponent.GetVehicleRecord(gi, owner, vehicleRecord) {
      return false;
    };
    if vehicleRecord.TagsContains(desiredTag) {
      return true;
    };
    return false;
  }

  public final static func GetVehicleType(gi: GameInstance, owner: wref<GameObject>, type: script_ref<String>) -> Bool {
    let vehTypeRecord: ref<VehicleType_Record>;
    let vehicleRecord: ref<Vehicle_Record>;
    if !VehicleComponent.GetVehicleRecord(gi, owner, vehicleRecord) {
      return false;
    };
    vehTypeRecord = vehicleRecord.Type();
    type = vehTypeRecord.EnumName();
    return true;
  }

  public final static func GetAttitudeOfPassenger(gi: GameInstance, ownerID: EntityID, slotID: MountingSlotId, out attitude: EAIAttitude) -> Bool {
    let mountInfo: MountingInfo;
    let npcObject: wref<GameObject>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(ownerID) {
      return false;
    };
    mountInfo = GameInstance.GetMountingFacility(gi).GetMountingInfoSingleWithIds(ownerID, slotID);
    npcObject = GameInstance.FindEntityByID(gi, mountInfo.childId) as GameObject;
    attitude = GameObject.GetAttitudeTowards(GameInstance.GetPlayerSystem(gi).GetLocalPlayerMainGameObject(), npcObject);
    return true;
  }

  private final static func GetVehicleNPCData(gi: GameInstance, owner: ref<GameObject>, out vehicleNPCData: ref<AnimFeature_VehicleNPCData>) -> Bool {
    let mountInfo: MountingInfo;
    let slotName: CName;
    let vehicle: wref<VehicleObject>;
    if !GameInstance.IsValid(gi) || !IsDefined(owner) {
      return false;
    };
    vehicleNPCData = new AnimFeature_VehicleNPCData();
    mountInfo = GameInstance.GetMountingFacility(gi).GetMountingInfoSingleWithObjects(owner);
    slotName = mountInfo.slotId.id;
    if Equals(slotName, n"seat_front_left") || Equals(slotName, n"seat_back_left") {
      vehicleNPCData.side = 1;
    } else {
      if Equals(slotName, n"seat_front_right") || Equals(slotName, n"seat_back_right") {
        vehicleNPCData.side = 2;
      } else {
        vehicleNPCData.side = 0;
      };
    };
    if VehicleComponent.IsDriverSlot(slotName) {
      vehicleNPCData.isDriver = true;
    };
    VehicleComponent.GetVehicle(gi, owner, vehicle);
    if vehicle.IsVehicleRemoteControlled() || vehicle.IsVehicleAccelerateQuickhackActive() || vehicle.IsVehicleForceBrakesQuickhackActive() {
      vehicleNPCData.forcePanic = true;
    };
    return true;
  }

  private final func PushVehicleNPCDataToAllPassengers(gi: GameInstance, id: EntityID) -> Void {
    let i: Int32;
    let passenger: wref<GameObject>;
    let passengers: array<wref<GameObject>>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(id) {
      return;
    };
    VehicleComponent.GetAllPassengers(gi, id, false, passengers);
    i = 0;
    while i < ArraySize(passengers) {
      passenger = passengers[i];
      if IsDefined(passenger) {
        VehicleComponent.PushVehicleNPCData(gi, passenger);
      };
      i += 1;
    };
  }

  private final static func PushVehicleNPCData(gi: GameInstance, passenger: wref<GameObject>) -> Void {
    let vehicleNPCData: ref<AnimFeature_VehicleNPCData>;
    VehicleComponent.GetVehicleNPCData(gi, passenger, vehicleNPCData);
    AnimationControllerComponent.ApplyFeatureToReplicate(passenger, n"VehicleNPCData", vehicleNPCData);
    AnimationControllerComponent.PushEventToReplicate(passenger, n"VehicleNPCData");
  }

  public final static func HasSlot(gi: GameInstance, vehicle: wref<VehicleObject>, slotName: CName) -> Bool {
    let i: Int32;
    let seats: array<wref<VehicleSeat_Record>>;
    if !IsNameValid(slotName) || !VehicleComponent.GetSeats(gi, vehicle, seats) {
      return false;
    };
    i = 0;
    while i < ArraySize(seats) {
      if Equals(slotName, seats[i].SeatName()) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final static func GetMountedSlotName(gi: GameInstance, owner: wref<GameObject>, out slotName: CName) -> Bool {
    let mountInfo: MountingInfo;
    if !IsDefined(owner) {
      return false;
    };
    mountInfo = GameInstance.GetMountingFacility(gi).GetMountingInfoSingleWithObjects(owner);
    slotName = mountInfo.slotId.id;
    return true;
  }

  public final static func GetSeats(gi: GameInstance, vehicle: wref<VehicleObject>, out seats: [wref<VehicleSeat_Record>]) -> Bool {
    let seatSet: wref<VehicleSeatSet_Record>;
    let vehicleDataPackage: wref<VehicleDataPackage_Record>;
    let vehicleRecord: ref<Vehicle_Record>;
    if !VehicleComponent.GetVehicleRecord(vehicle, vehicleRecord) {
      return false;
    };
    vehicleDataPackage = vehicleRecord.VehDataPackage();
    if !IsDefined(vehicleDataPackage) {
      return false;
    };
    seatSet = vehicleDataPackage.VehSeatSet();
    if !IsDefined(seatSet) {
      return false;
    };
    seatSet.VehSeats(seats);
    if ArraySize(seats) == 0 {
      return false;
    };
    return true;
  }

  public final static func GetFirstAvailableSlot(gi: GameInstance, vehicle: wref<VehicleObject>, out slotName: CName) -> Bool {
    let i: Int32;
    let seats: array<wref<VehicleSeat_Record>>;
    if !VehicleComponent.GetSeats(gi, vehicle, seats) {
      return false;
    };
    i = 0;
    while i < ArraySize(seats) {
      slotName = seats[i].SeatName();
      if !(VehicleComponent.IsSlotOccupied(gi, vehicle.GetEntityID(), slotName) || IsDefined(vehicle.GetAIComponent()) && vehicle.GetAIComponent().IsSeatReserved(slotName)) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final static func GetSeatsStatus(gi: GameInstance, vehicle: wref<VehicleObject>, out totalSeatSlots: Int32, out occupiedSeatSlots: Int32, out reservedSlots: Int32) -> Void {
    let i: Int32;
    let seats: array<wref<VehicleSeat_Record>>;
    let slotName: CName;
    if !VehicleComponent.GetSeats(gi, vehicle, seats) {
      return;
    };
    i = 0;
    while i < ArraySize(seats) {
      slotName = seats[i].SeatName();
      if VehicleComponent.IsSlotOccupied(gi, vehicle.GetEntityID(), slotName) {
        occupiedSeatSlots += 1;
      } else {
        if IsDefined(vehicle.GetAIComponent()) && vehicle.GetAIComponent().IsSeatReserved(slotName) {
          reservedSlots += 1;
        };
      };
      i += 1;
    };
    totalSeatSlots = ArraySize(seats);
  }

  public final static func GetNumberOfOccupiedSlots(gi: GameInstance, vehicle: wref<VehicleObject>) -> Int32 {
    let i: Int32;
    let occupiedSlots: Int32;
    let seats: array<wref<VehicleSeat_Record>>;
    let slotName: CName;
    if !VehicleComponent.GetSeats(gi, vehicle, seats) {
      return occupiedSlots;
    };
    i = 0;
    while i < ArraySize(seats) {
      slotName = seats[i].SeatName();
      if VehicleComponent.IsSlotOccupied(gi, vehicle.GetEntityID(), slotName) {
        occupiedSlots += 1;
      };
      i += 1;
    };
    return occupiedSlots;
  }

  public final static func GetVehicleDataPackage(gi: GameInstance, vehicle: wref<VehicleObject>, out vehDataPackage: wref<VehicleDataPackage_Record>) -> Bool {
    let vehicleRecord: ref<Vehicle_Record>;
    if !GameInstance.IsValid(gi) || !IsDefined(vehicle) {
      return false;
    };
    VehicleComponent.GetVehicleRecord(vehicle, vehicleRecord);
    vehDataPackage = vehicleRecord.VehDataPackage();
    return true;
  }

  public final static func GetVehicleAllowsCombat(gi: GameInstance, vehicle: wref<VehicleObject>) -> Bool {
    let vehDataPackage: wref<VehicleDataPackage_Record>;
    if !GameInstance.IsValid(gi) || !IsDefined(vehicle) {
      return false;
    };
    VehicleComponent.GetVehicleDataPackage(gi, vehicle, vehDataPackage);
    return NotEquals(vehDataPackage.DriverCombat().Type(), gamedataDriverCombatType.Disabled);
  }

  public final static func ToggleVehicleWindow(gi: GameInstance, vehicle: wref<VehicleObject>, slotID: MountingSlotId, toggle: Bool, opt speed: CName) -> Bool {
    let windowToggleEvent: ref<VehicleExternalWindowRequestEvent>;
    if !GameInstance.IsValid(gi) || !IsDefined(vehicle) {
      return false;
    };
    windowToggleEvent = new VehicleExternalWindowRequestEvent();
    windowToggleEvent.slotName = slotID.id;
    windowToggleEvent.shouldOpen = toggle;
    windowToggleEvent.speed = speed;
    vehicle.QueueEvent(windowToggleEvent);
    return true;
  }

  protected final const func GetVehicle() -> wref<VehicleObject> {
    return this.GetEntity() as VehicleObject;
  }

  private final func GetVehicleController() -> ref<vehicleController> {
    if this.m_vehicleController == null {
      this.m_vehicleController = (this.GetEntity() as VehicleObject).GetAccessoryController();
    };
    return this.m_vehicleController;
  }

  private final func GetVehicleControllerPS() -> ref<vehicleControllerPS> {
    let persistentId: PersistentID = CreatePersistentID(this.GetEntity().GetEntityID(), n"VehicleController");
    let vehicleControllerPS: ref<vehicleControllerPS> = GameInstance.GetPersistencySystem((this.GetEntity() as VehicleObject).GetGame()).GetConstAccessToPSObject(persistentId, n"gamevehicleControllerPS") as vehicleControllerPS;
    return vehicleControllerPS;
  }

  public const func GetPS() -> ref<VehicleComponentPS> {
    return this.GetBasePS() as VehicleComponentPS;
  }

  public final static func OpenDoor(vehicle: wref<VehicleObject>, vehicleSlotID: MountingSlotId, opt delay: Float) -> Bool {
    let doorOpenRequest: ref<VehicleDoorOpen>;
    let persistentId: PersistentID;
    if !IsDefined(vehicle) {
      return false;
    };
    persistentId = CreatePersistentID(vehicle.GetEntityID(), n"controller");
    doorOpenRequest = new VehicleDoorOpen();
    if Equals(vehicleSlotID.id, n"trunk") || Equals(vehicleSlotID.id, n"hood") {
      doorOpenRequest.slotID = vehicleSlotID.id;
    } else {
      doorOpenRequest.slotID = vehicle.GetBoneNameFromSlot(vehicleSlotID.id);
    };
    doorOpenRequest.shouldAutoClose = false;
    if delay > 0.00 {
      GameInstance.GetDelaySystem(vehicle.GetGame()).DelayPSEvent(persistentId, n"VehicleComponentPS", doorOpenRequest, delay);
    } else {
      GameInstance.GetPersistencySystem(vehicle.GetGame()).QueuePSEvent(persistentId, n"VehicleComponentPS", doorOpenRequest);
    };
    return true;
  }

  public final static func CloseDoor(vehicle: wref<VehicleObject>, vehicleSlotID: MountingSlotId) -> Bool {
    let doorOpenRequest: ref<VehicleDoorClose>;
    let persistentId: PersistentID;
    if !IsDefined(vehicle) {
      return false;
    };
    persistentId = CreatePersistentID(vehicle.GetEntityID(), n"controller");
    doorOpenRequest = new VehicleDoorClose();
    if Equals(vehicleSlotID.id, n"trunk") || Equals(vehicleSlotID.id, n"hood") {
      doorOpenRequest.slotID = vehicleSlotID.id;
    } else {
      doorOpenRequest.slotID = vehicle.GetBoneNameFromSlot(vehicleSlotID.id);
    };
    GameInstance.GetPersistencySystem(vehicle.GetGame()).QueuePSEvent(persistentId, n"VehicleComponentPS", doorOpenRequest);
    return true;
  }

  protected cb func OnWeaponShootEvent(evt: ref<VehicleMountedWeaponShootEvent>) -> Bool {
    let vehicle: ref<VehicleObject> = this.GetVehicle();
    let broadcaster: ref<StimBroadcasterComponent> = vehicle.GetStimBroadcasterComponent();
    broadcaster.TriggerSingleBroadcast(vehicle, gamedataStimType.Gunshot, 50.00, true);
    if IsDefined(this.m_mountedPlayer) {
      broadcaster.TriggerSingleBroadcast(this.m_mountedPlayer, gamedataStimType.Gunshot, this.m_mountedPlayer.GetGunshotRange());
    };
  }

  protected cb func OnMountingEvent(evt: ref<MountingEvent>) -> Bool {
    let PSvehicleDooropenRequest: ref<VehicleDoorOpen>;
    let isDriverSlot: Bool;
    let vehicleDataPackage: wref<VehicleDataPackage_Record>;
    let vehicleRecord: ref<Vehicle_Record>;
    let shouldAutoClose: Bool = true;
    let gameInstance: GameInstance = this.GetVehicle().GetGame();
    let mountChild: ref<GameObject> = GameInstance.FindEntityByID(gameInstance, evt.request.lowLevelMountingInfo.childId) as GameObject;
    VehicleComponent.GetVehicleDataPackage(gameInstance, this.GetVehicle(), vehicleDataPackage);
    if mountChild.IsPlayer() {
      this.m_mountedPlayer = mountChild as PlayerPuppet;
      isDriverSlot = VehicleComponent.IsDriverSlot(evt.request.lowLevelMountingInfo.slotId.id);
      if isDriverSlot {
        PreventionSystem.SetPlayerMounted(gameInstance, true);
        PreventionSystemHackerLoop.UpdatePlayerVehicle(gameInstance, this.GetVehicle());
        PoliceRadioScriptSystem.UpdatePoliceRadioOnVehicleEntrance(gameInstance);
      };
      this.m_mountedPlayer.GetPlayerStateMachineBlackboard().SetBool(GetAllBlackboardDefs().PlayerStateMachine.MountedToVehicleInDriverSeat, isDriverSlot);
      VehicleComponent.GetVehicleRecord(gameInstance, this.m_mountedPlayer, vehicleRecord);
      VehicleComponent.QueueEventToAllPassengers(this.m_mountedPlayer.GetGame(), this.GetVehicle().GetEntityID(), PlayerMuntedToMyVehicle.Create(this.m_mountedPlayer));
      PlayerPuppet.ReevaluateAllBreathingEffects(mountChild as PlayerPuppet);
      if !this.GetVehicle().IsCrowdVehicle() {
        this.GetVehicle().GetDeviceLink().TriggerSecuritySystemNotification(this.GetVehicle().GetWorldPosition(), mountChild, ESecurityNotificationType.ALARM);
      };
      this.ToggleScanningComponent(false);
      if this.GetVehicle().GetHudManager().IsRegistered(this.GetVehicle().GetEntityID()) {
        this.RegisterToHUDManager(false);
      };
      if this.GetVehicle().IsPlayerVehicle() && !this.GetPS().GetIsDestroyed() {
        this.CreateMappin();
      };
      this.RegisterInputListener();
      FastTravelSystem.AddFastTravelLock(n"InVehicle", gameInstance);
      this.m_mounted = true;
      this.m_ignoreAutoDoorClose = true;
      this.SetupListeners();
      this.DisableTargetingComponents();
      if EntityID.IsDefined(evt.request.mountData.mountEventOptions.entityID) {
        this.m_enterTime = vehicleDataPackage.Stealing() + vehicleDataPackage.SlideDuration();
      } else {
        if this.m_mountedPlayer.IsInCombat() && Equals(vehicleRecord.Type().Type(), gamedataVehicleType.Car) && isDriverSlot {
          this.m_enterTime = vehicleDataPackage.CombatEntering() + vehicleDataPackage.SlideDuration();
          if Equals(vehicleDataPackage.DriverCombat().Type(), gamedataDriverCombatType.Doors) && (IsDefined(GameInstance.GetTransactionSystem(this.m_mountedPlayer.GetGame()).GetItemInSlot(this.m_mountedPlayer, t"AttachmentSlots.WeaponLeft") as WeaponObject) || IsDefined(GameInstance.GetTransactionSystem(this.m_mountedPlayer.GetGame()).GetItemInSlot(this.m_mountedPlayer, t"AttachmentSlots.WeaponRight") as WeaponObject)) {
            shouldAutoClose = false;
          } else {
            shouldAutoClose = true;
          };
        } else {
          this.m_enterTime = vehicleDataPackage.Entering() + vehicleDataPackage.SlideDuration();
        };
      };
      this.DrivingStimuli(true);
      if Equals(evt.request.lowLevelMountingInfo.slotId.id, n"seat_front_left") {
        if IsDefined(this.GetVehicle() as TankObject) {
          this.TogglePlayerHitShapesForPanzer(this.m_mountedPlayer, false);
          this.ToggleTargetingSystemForPanzer(this.m_mountedPlayer, true);
        };
        this.SetSteeringLimitAnimFeature(1);
      };
      GameInstance.GetStatPoolsSystem(this.GetVehicle().GetGame()).RequestSettingStatPoolValueCustomLimit(Cast<StatsObjectID>(this.GetVehicle().GetEntityID()), gamedataStatPoolType.Health, this.m_healthDecayThreshold, this.GetVehicle());
    };
    if !mountChild.IsPlayer() {
      if evt.request.mountData.isInstant {
        mountChild.QueueEvent(CreateDisableRagdollEvent(n"VehicleComponentOnMountingEvent"));
      };
      VehicleComponent.PushVehicleNPCData(gameInstance, mountChild);
      if mountChild.IsPuppet() && !this.GetVehicle().IsPlayerVehicle() && (IsHostileTowardsPlayer(mountChild) || (mountChild as ScriptedPuppet).IsAggressive()) {
        this.EnableTargetingComponents();
      };
    };
    if !evt.request.mountData.isInstant {
      PSvehicleDooropenRequest = new VehicleDoorOpen();
      PSvehicleDooropenRequest.slotID = this.GetVehicle().GetBoneNameFromSlot(evt.request.lowLevelMountingInfo.slotId.id);
      if EntityID.IsDefined(evt.request.mountData.mountEventOptions.entityID) {
        PSvehicleDooropenRequest.autoCloseTime = vehicleDataPackage.Stealing_open();
      } else {
        PSvehicleDooropenRequest.autoCloseTime = vehicleDataPackage.Normal_open();
      };
      if !this.GetPS().GetIsDestroyed() && shouldAutoClose {
        PSvehicleDooropenRequest.shouldAutoClose = true;
      };
      GameInstance.GetPersistencySystem(gameInstance).QueuePSEvent(this.GetPS().GetID(), this.GetPS().GetClassName(), PSvehicleDooropenRequest);
    };
    this.ManageAdditionalAnimFeatures(mountChild, true);
    if mountChild.IsPrevention() {
      this.m_preventionPassengers += 1;
      this.RegisterWantedLevelListener();
      GameInstance.GetPreventionSpawnSystem(gameInstance).RegisterEntityDeathCallback(this, "OnPreventionPassengerDeath", mountChild.GetEntityID());
      this.CreateMappin();
      this.GetVehicle().GetPreventionSystem().UpdateVehiclePassengerCount(this.GetVehicle().GetEntityID(), this.m_preventionPassengers);
    };
  }

  protected cb func OnUnmountingEvent(evt: ref<UnmountingEvent>) -> Bool {
    let activePassengers: Int32;
    let gameInstance: GameInstance = this.GetVehicle().GetGame();
    let mountChild: ref<GameObject> = GameInstance.FindEntityByID(gameInstance, evt.request.lowLevelMountingInfo.childId) as GameObject;
    let mountChildIsPrevention: Bool = IsDefined(mountChild) && mountChild.IsPrevention();
    VehicleComponent.SetAnimsetOverrideForPassenger(mountChild, evt.request.lowLevelMountingInfo.parentId, evt.request.lowLevelMountingInfo.slotId.id, 0.00);
    if IsDefined(mountChild) && mountChild.IsPlayer() {
      PreventionSystem.SetPlayerMounted(gameInstance, false);
      PlayerPuppet.ReevaluateAllBreathingEffects(mountChild as PlayerPuppet);
      this.ToggleScanningComponent(true);
      if this.GetVehicle().ShouldRegisterToHUD() {
        this.RegisterToHUDManager(true);
      };
      this.UnregisterInputListener();
      FastTravelSystem.RemoveFastTravelLock(n"InVehicle", gameInstance);
      this.m_mounted = false;
      this.UnregisterListeners();
      this.ToggleSiren(false, false);
      if this.m_broadcasting {
        this.DrivingStimuli(false);
      };
      if Equals(evt.request.lowLevelMountingInfo.slotId.id, n"seat_front_left") {
        PreventionSystemHackerLoop.UpdatePlayerVehicle(gameInstance, null);
        this.ToggleCrystalDome(false, true, false, 0.00, 0.00, true);
        this.SetSteeringLimitAnimFeature(0);
        this.m_ignoreAutoDoorClose = false;
      };
      this.DoPanzerCleanup();
      this.m_mountedPlayer = null;
      this.CleanUpRace();
      GameInstance.GetStatPoolsSystem(this.GetVehicle().GetGame()).RequestSettingStatPoolValueCustomLimit(Cast<StatsObjectID>(this.GetVehicle().GetEntityID()), gamedataStatPoolType.Health, 0.00, this.GetVehicle());
      GameInstance.GetAudioSystem(this.GetVehicle().GetGame()).RemoveTriggerEffect(n"te_vehicle_car_disabled");
    };
    if IsDefined(mountChild) {
      if mountChildIsPrevention {
        GameInstance.GetPreventionSpawnSystem(gameInstance).UnregisterEntityDeathCallback(this, "OnPreventionPassengerDeath", mountChild.GetEntityID());
        if mountChild.IsActive() {
          this.m_preventionPassengers = Max(0, this.m_preventionPassengers - 1);
        };
        this.GetVehicle().GetPreventionSystem().UpdateVehiclePassengerCount(this.GetVehicle().GetEntityID(), this.m_preventionPassengers);
        if this.m_preventionPassengers == 0 {
          this.DestroyMappin();
        };
      };
      if VehicleComponent.GetNumberOfActivePassengers(mountChild.GetGame(), this.GetVehicle().GetEntityID(), activePassengers) {
        if activePassengers <= 0 {
          this.DisableTargetingComponents();
        };
      };
    };
    this.GetPS().ToggleReserveSeatDuringUnmounting(false, evt.request.lowLevelMountingInfo.slotId.id);
    this.ManageAdditionalAnimFeatures(mountChild, false);
  }

  protected cb func OnCurrentWantedLevelChanged(value: Int32) -> Bool {
    if value == 0 {
      this.ToggleSiren(false, false);
    };
  }

  public final func OnPreventionPassengerDeath(deadEntityID: EntityID) -> Void {
    this.m_preventionPassengers = Max(0, this.m_preventionPassengers - 1);
    if this.m_preventionPassengers == 0 {
      this.UnregisterWantedLevelListener();
      this.DestroyMappin();
    };
  }

  protected cb func OnVehicleFinishedMountingEvent(evt: ref<VehicleFinishedMountingEvent>) -> Bool {
    let environmentalHazardStimEvent: ref<RepeatDirectEnvironmentalHazardStimEvent>;
    let isDestroyed: Bool;
    let vehDataPackage: wref<VehicleDataPackage_Record>;
    if evt.isMounting {
      isDestroyed = this.GetPS().GetIsDestroyed();
      if !isDestroyed {
        if VehicleComponent.IsDriverSlot(evt.slotID) {
          this.ToggleVehicleSystems(true, true, true);
        };
      };
    } else {
      if this.m_isBroadcastingHazardStims {
        environmentalHazardStimEvent = new RepeatDirectEnvironmentalHazardStimEvent();
        environmentalHazardStimEvent.target = evt.character;
        this.GetVehicle().QueueEvent(environmentalHazardStimEvent);
      };
    };
    this.GetVehicle().GetAIComponent().ReleaseSeat(evt.slotID);
    VehicleComponent.GetVehicleDataPackage(this.GetVehicle().GetGame(), this.GetVehicle(), vehDataPackage);
    if NotEquals(vehDataPackage.DriverCombat().Type(), gamedataDriverCombatType.Doors) && NotEquals(IntEnum<gamePSMVehicle>(this.m_mountedPlayer.GetPlayerStateMachineBlackboard().GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vehicle)), gamePSMVehicle.DriverCombat) {
      this.m_ignoreAutoDoorClose = false;
    };
  }

  protected cb func OnVehicleStartedUnmountingEvent(evt: ref<VehicleStartedUnmountingEvent>) -> Bool;

  protected cb func OnVehicleQuestNodeActivateRemoteVehicleControl(evt: ref<VehicleQuestNodeSetVehicleRemoteControlled>) -> Bool {
    let triggerVehicleRemoteControlEvent: ref<TriggerVehicleRemoteControlEvent> = new TriggerVehicleRemoteControlEvent();
    triggerVehicleRemoteControlEvent.enable = evt.enable;
    triggerVehicleRemoteControlEvent.shouldUnseatPassengers = evt.shouldUnseatPassengers;
    triggerVehicleRemoteControlEvent.shouldModifyInteractionState = evt.shouldModifyInteractionState;
    TakeOverControlSystem.ReleaseControl(this.GetVehicle().GetGame(), triggerVehicleRemoteControlEvent, this.GetVehicle().GetEntityID());
  }

  protected cb func OnTriggerVehicleRemoteControlEvent(evt: ref<TriggerVehicleRemoteControlEvent>) -> Bool {
    let invalidateEvent: ref<PlayerVisionModeControllerInvalidateEvent> = new PlayerVisionModeControllerInvalidateEvent();
    let playerPuppet: ref<PlayerPuppet> = GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerMainGameObject() as PlayerPuppet;
    this.GetVehicle().SetVehicleRemoteControlled(evt.enable, evt.shouldUnseatPassengers, evt.shouldModifyInteractionState);
    invalidateEvent.m_active = false;
    playerPuppet.QueueEvent(invalidateEvent);
  }

  protected cb func OnVehicleStartedMountingEvent(evt: ref<VehicleStartedMountingEvent>) -> Bool {
    let passengers: array<wref<GameObject>>;
    if !evt.isMounting {
      this.SendVehicleStartedUnmountingEventToPS(evt.isMounting, evt.slotID, evt.character);
      VehicleComponent.GetAllPassengers(this.GetVehicle().GetGame(), this.GetVehicle().GetEntityID(), false, passengers);
      if (Equals(evt.slotID, n"seat_front_left") || ArraySize(passengers) == 1) && !this.GetVehicle().IsAbandoned() && !this.GetVehicle().IsVehicleAccelerateQuickhackActive() && !(Equals(evt.animationSlotName, n"cool") || Equals(evt.animationSlotName, n"combat") || Equals(evt.animationSlotName, n"speed")) {
        this.ToggleVehicleSystems(false, true, true);
      };
    } else {
      this.GetVehicle().SendDelayedFinishedMountingEventToPS(evt.isMounting, evt.slotID, evt.character, evt.instant ? 0.00 : this.m_enterTime);
    };
  }

  protected final func SendVehicleStartedUnmountingEventToPS(isMounting: Bool, slotID: CName, character: ref<GameObject>) -> Void {
    let evt: ref<VehicleStartedUnmountingEvent> = new VehicleStartedUnmountingEvent();
    evt.slotID = slotID;
    evt.isMounting = isMounting;
    evt.character = character;
    GameInstance.GetPersistencySystem(this.GetVehicle().GetGame()).QueuePSEvent(this.GetPS().GetID(), this.GetPS().GetClassName(), evt);
  }

  protected final func SetSteeringLimitAnimFeature(limit: Int32) -> Void {
    let steeringLimitAnimFeature: ref<AnimFeature_VehicleSteeringLimit> = new AnimFeature_VehicleSteeringLimit();
    steeringLimitAnimFeature.state = limit;
    AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), n"SteeringLimit", steeringLimitAnimFeature);
  }

  protected final func ManageAdditionalAnimFeatures(object: ref<GameObject>, value: Bool) -> Void {
    let animFeature: ref<AnimFeature_NPCVehicleAdditionalFeatures>;
    let animFeatureName: CName;
    let animFeatures: array<CName>;
    let i: Int32;
    let vehicleDataPackage: wref<VehicleDataPackage_Record>;
    VehicleComponent.GetVehicleDataPackage(this.GetVehicle().GetGame(), this.GetVehicle(), vehicleDataPackage);
    animFeatures = vehicleDataPackage.AdditionalAnimFeatures();
    if ArraySize(animFeatures) == 0 {
      return;
    };
    i = 0;
    while i < ArraySize(animFeatures) {
      animFeature = new AnimFeature_NPCVehicleAdditionalFeatures();
      animFeatureName = animFeatures[i];
      animFeature.state = value;
      AnimationControllerComponent.ApplyFeatureToReplicate(object, animFeatureName, animFeature);
      i += 1;
    };
  }

  protected cb func OnVehicleSeatReservationEvent(evt: ref<VehicleSeatReservationEvent>) -> Bool;

  protected cb func OnVehicleBodyDisposalPerformedEvent(evt: ref<VehicleBodyDisposalPerformedEvent>) -> Bool {
    this.DetermineInteractionState(n"trunk");
  }

  protected final func DetermineInteractionState() -> Void {
    let activeInteractions: array<gameinteractionsActiveLayerData>;
    let activeLayerData: gameinteractionsActiveLayerData;
    let context: VehicleActionsContext;
    let i: Int32;
    this.m_interaction.GetActiveInputLayers(activeInteractions);
    i = 0;
    while i < ArraySize(activeInteractions) {
      activeLayerData = activeInteractions[i];
      context.requestorID = this.GetVehicle().GetEntityID();
      context.processInitiatorObject = activeLayerData.activator;
      context.interactionLayerTag = activeLayerData.layerName;
      context.eventType = gameinteractionsEInteractionEventType.EIET_activate;
      this.GetPS().DetermineActionsToPush(this.m_interaction, context, this.m_objectActionsCallbackCtrl, true);
      i += 1;
    };
  }

  protected final func DetermineInteractionState(layerName: CName) -> Void {
    let activeInteractions: array<gameinteractionsActiveLayerData>;
    let activeLayerData: gameinteractionsActiveLayerData;
    let context: VehicleActionsContext;
    let i: Int32;
    this.m_interaction.GetActivatorsForLayer(layerName, activeInteractions);
    i = 0;
    while i < ArraySize(activeInteractions) {
      activeLayerData = activeInteractions[i];
      context.requestorID = this.GetVehicle().GetEntityID();
      context.processInitiatorObject = activeLayerData.activator;
      context.interactionLayerTag = layerName;
      context.eventType = gameinteractionsEInteractionEventType.EIET_activate;
      this.GetPS().DetermineActionsToPush(this.m_interaction, context, this.m_objectActionsCallbackCtrl, true);
      i += 1;
    };
  }

  protected final func GetIsMounted() -> Bool {
    return this.m_mounted;
  }

  private final func InitialVehcileSetup() -> Void {
    let lightEvent: ref<VehicleLightSetupEvent>;
    this.m_overheatActive = false;
    this.SetupAuxillary();
    this.VehicleDefaultStateSetup();
    this.EvaluateInteractions();
    this.EvaluateDoorState();
    this.EvaluateWindowState();
    this.SendParkEvent(true);
    lightEvent = new VehicleLightSetupEvent();
    this.GetVehicle().QueueEvent(lightEvent);
    this.SetupCrystalDome();
    this.SetupWheels();
    this.ShouldVisualDestructionBeSet();
  }

  private final func VehicleDefaultStateSetup() -> Void {
    let door: EVehicleDoor;
    let i: Int32;
    let seatStateRecord: ref<SeatState_Record>;
    let sirenDelayEvent: ref<VehicleSirenDelayEvent>;
    let sirenLight: Bool;
    let sirenSound: Bool;
    let spawnDestroyed: Bool;
    let state: EQuestVehicleDoorState;
    let thrusters: Bool;
    let recordID: TweakDBID = this.GetVehicle().GetRecordID();
    let record: ref<Vehicle_Record> = TweakDBInterface.GetVehicleRecord(recordID);
    let defaultState: ref<VehicleDefaultState_Record> = record.VehDefaultState();
    let size: Int32 = 6;
    if !this.GetPS().GetHasDefaultStateBeenSet() {
      if defaultState.DisableAllInteractions() {
        state = EQuestVehicleDoorState.DisableAllInteractions;
        this.CreateAndSendDefaultStateEvent(door, state);
      };
      if defaultState.LockAll() || this.IsVehicleParked() {
        state = EQuestVehicleDoorState.LockAll;
        this.CreateAndSendDefaultStateEvent(door, state);
      };
      if defaultState.OpenAll() {
        state = EQuestVehicleDoorState.OpenAll;
        this.CreateAndSendDefaultStateEvent(door, state);
      };
      if defaultState.CloseAll() {
        state = EQuestVehicleDoorState.CloseAll;
        this.CreateAndSendDefaultStateEvent(door, state);
      };
      if defaultState.QuestLockAll() {
        state = EQuestVehicleDoorState.QuestLockAll;
        this.CreateAndSendDefaultStateEvent(door, state);
      };
      i = 0;
      while i < size {
        switch IntEnum<EVehicleDoor>(i) {
          case EVehicleDoor.seat_front_left:
            seatStateRecord = defaultState.Seat_front_left();
            if IsDefined(seatStateRecord) {
              door = EVehicleDoor.seat_front_left;
              if seatStateRecord.EnableInteraction() {
                state = EQuestVehicleDoorState.EnableInteraction;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.DisableInteraction() {
                state = EQuestVehicleDoorState.DisableInteraction;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceOpen() {
                state = EQuestVehicleDoorState.ForceOpen;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceClose() {
                state = EQuestVehicleDoorState.ForceClose;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceLock() {
                state = EQuestVehicleDoorState.ForceLock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceUnlock() {
                state = EQuestVehicleDoorState.ForceUnlock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.QuestLock() {
                state = EQuestVehicleDoorState.QuestLock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
            };
            break;
          case EVehicleDoor.seat_front_right:
            seatStateRecord = defaultState.Seat_front_right();
            if IsDefined(seatStateRecord) {
              door = EVehicleDoor.seat_front_right;
              if seatStateRecord.EnableInteraction() {
                state = EQuestVehicleDoorState.EnableInteraction;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.DisableInteraction() {
                state = EQuestVehicleDoorState.DisableInteraction;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceOpen() {
                state = EQuestVehicleDoorState.ForceOpen;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceClose() {
                state = EQuestVehicleDoorState.ForceClose;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceLock() {
                state = EQuestVehicleDoorState.ForceLock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceUnlock() {
                state = EQuestVehicleDoorState.ForceUnlock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.QuestLock() {
                state = EQuestVehicleDoorState.QuestLock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
            };
            break;
          case EVehicleDoor.seat_back_left:
            seatStateRecord = defaultState.Seat_back_left();
            if IsDefined(seatStateRecord) {
              door = EVehicleDoor.seat_back_left;
              if seatStateRecord.EnableInteraction() {
                state = EQuestVehicleDoorState.EnableInteraction;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.DisableInteraction() {
                state = EQuestVehicleDoorState.DisableInteraction;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceOpen() {
                state = EQuestVehicleDoorState.ForceOpen;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceClose() {
                state = EQuestVehicleDoorState.ForceClose;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceLock() {
                state = EQuestVehicleDoorState.ForceLock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceUnlock() {
                state = EQuestVehicleDoorState.ForceUnlock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.QuestLock() {
                state = EQuestVehicleDoorState.QuestLock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
            };
            break;
          case EVehicleDoor.seat_back_right:
            seatStateRecord = defaultState.Seat_back_right();
            if IsDefined(seatStateRecord) {
              door = EVehicleDoor.seat_back_right;
              if seatStateRecord.EnableInteraction() {
                state = EQuestVehicleDoorState.EnableInteraction;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.DisableInteraction() {
                state = EQuestVehicleDoorState.DisableInteraction;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceOpen() {
                state = EQuestVehicleDoorState.ForceOpen;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceClose() {
                state = EQuestVehicleDoorState.ForceClose;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceLock() {
                state = EQuestVehicleDoorState.ForceLock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceUnlock() {
                state = EQuestVehicleDoorState.ForceUnlock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.QuestLock() {
                state = EQuestVehicleDoorState.QuestLock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
            };
            break;
          case EVehicleDoor.trunk:
            seatStateRecord = defaultState.Trunk();
            if IsDefined(seatStateRecord) {
              door = EVehicleDoor.trunk;
              if seatStateRecord.EnableInteraction() {
                state = EQuestVehicleDoorState.EnableInteraction;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.DisableInteraction() {
                state = EQuestVehicleDoorState.DisableInteraction;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceOpen() {
                state = EQuestVehicleDoorState.ForceOpen;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceClose() {
                state = EQuestVehicleDoorState.ForceClose;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceLock() {
                state = EQuestVehicleDoorState.ForceLock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceUnlock() {
                state = EQuestVehicleDoorState.ForceUnlock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.QuestLock() {
                state = EQuestVehicleDoorState.QuestLock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
            };
            break;
          case EVehicleDoor.hood:
            seatStateRecord = defaultState.Hood();
            if IsDefined(seatStateRecord) {
              door = EVehicleDoor.hood;
              if seatStateRecord.EnableInteraction() {
                state = EQuestVehicleDoorState.EnableInteraction;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.DisableInteraction() {
                state = EQuestVehicleDoorState.DisableInteraction;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceOpen() {
                state = EQuestVehicleDoorState.ForceOpen;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceClose() {
                state = EQuestVehicleDoorState.ForceClose;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceLock() {
                state = EQuestVehicleDoorState.ForceLock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.ForceUnlock() {
                state = EQuestVehicleDoorState.ForceUnlock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
              if seatStateRecord.QuestLock() {
                state = EQuestVehicleDoorState.QuestLock;
                this.CreateAndSendDefaultStateEvent(door, state);
              };
            };
            break;
          default:
        };
        i += 1;
      };
      sirenLight = defaultState.SirenLight();
      sirenSound = defaultState.SirenSounds();
      if sirenLight || sirenSound {
        sirenDelayEvent = new VehicleSirenDelayEvent();
        sirenDelayEvent.lights = sirenLight;
        sirenDelayEvent.sounds = sirenSound;
        GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), sirenDelayEvent, 0.50);
      };
      thrusters = defaultState.Thrusters();
      if thrusters {
        this.GetPS().SetThrusterState(true);
      };
    };
    spawnDestroyed = defaultState.SpawnDestroyed();
    if spawnDestroyed {
      this.GetPS().SetIsDestroyed(true);
      if !this.GetPS().GetIsSubmerged() {
        this.GetPS().SetHasExploded(true);
      };
    };
  }

  private final func CreateAndSendDefaultStateEvent(door: EVehicleDoor, state: EQuestVehicleDoorState) -> Void {
    let vehicleQuestEvent: ref<VehicleQuestChangeDoorStateEvent> = new VehicleQuestChangeDoorStateEvent();
    vehicleQuestEvent.door = door;
    vehicleQuestEvent.newState = state;
    GameInstance.GetPersistencySystem(this.GetVehicle().GetGame()).QueuePSEvent(this.GetPS().GetID(), this.GetPS().GetClassName(), vehicleQuestEvent);
    this.GetPS().SetHasDefaultStateBeenSet(true);
  }

  protected cb func OnRemoteControlEvent(evt: ref<VehicleRemoteControlEvent>) -> Bool {
    let vehicleQuestEvent: ref<VehicleQuestChangeDoorStateEvent> = new VehicleQuestChangeDoorStateEvent();
    let maxDelayToUnseatPassengers: Float = 5.00;
    if evt.shouldModifyInteractionState {
      if evt.remoteControl {
        vehicleQuestEvent.newState = EQuestVehicleDoorState.DisableAllInteractions;
      } else {
        vehicleQuestEvent.newState = EQuestVehicleDoorState.ResetInteractions;
      };
      GameInstance.GetPersistencySystem(this.GetVehicle().GetGame()).QueuePSEvent(this.GetPS().GetID(), this.GetPS().GetClassName(), vehicleQuestEvent);
    };
    if evt.shouldUnseatPassengers {
      this.GetVehicle().TriggerExitBehavior(maxDelayToUnseatPassengers);
    };
    if !evt.remoteControl {
      this.GetPS().EndStimsOnVehicleQuickhack();
    };
    this.PushVehicleNPCDataToAllPassengers(this.GetVehicle().GetGame(), this.GetVehicle().GetEntityID());
  }

  protected cb func OnForceBrakesQuickhackEvent(evt: ref<VehicleForceBrakesQuickhackEvent>) -> Bool {
    if evt.active {
      this.SetDelayDisableCarAlarm(evt.alarmDuration);
    };
    this.PushVehicleNPCDataToAllPassengers(this.GetVehicle().GetGame(), this.GetVehicle().GetEntityID());
  }

  protected cb func OnAccelerateQuickhackEvent(evt: ref<VehicleAccelerateQuickhackEvent>) -> Bool {
    this.PushVehicleNPCDataToAllPassengers(this.GetVehicle().GetGame(), this.GetVehicle().GetEntityID());
  }

  private final func QueueVehicleImpactLethalHitEvent(target: wref<GameObject>, instigator: wref<GameObject>, sourceName: CName) -> Void {
    let attack: ref<IAttack>;
    let attackContext: AttackInitContext;
    let hitEvent: ref<gameHitEvent>;
    if target == null {
      return;
    };
    hitEvent = new gameHitEvent();
    hitEvent.attackData = new AttackData();
    attackContext.record = TweakDBInterface.GetAttackRecord(t"Attacks.VehicleImpact");
    attackContext.instigator = instigator;
    attackContext.source = this.GetVehicle();
    attack = IAttack.Create(attackContext);
    hitEvent.target = target;
    hitEvent.attackData.AddFlag(hitFlag.Kill, sourceName);
    if target == instigator {
      hitEvent.attackData.AddFlag(hitFlag.CanDamageSelf, sourceName);
    };
    hitEvent.attackData.SetSource(this.GetVehicle());
    hitEvent.attackData.SetInstigator(instigator);
    hitEvent.attackData.SetAttackDefinition(attack);
    GameInstance.GetDamageSystem(this.GetVehicle().GetGame()).QueueHitEvent(hitEvent, target);
  }

  protected cb func OnExplodeEvent(evt: ref<VehicleExplodeEvent>) -> Bool {
    let instigator: wref<GameObject> = GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerControlledGameObject();
    this.QueueVehicleImpactLethalHitEvent(this.GetVehicle(), instigator, n"Explode Quickhack");
  }

  protected cb func OnVehicleNotifyPassengersOfCollision(evt: ref<VehicleNotifyPassengersOfCollision>) -> Bool {
    let i: Int32;
    let passenger: wref<GameObject>;
    let passengers: array<wref<GameObject>>;
    let gi: GameInstance = this.GetVehicle().GetGame();
    let vehicleID: EntityID = this.GetVehicle().GetEntityID();
    VehicleComponent.GetAllPassengers(gi, vehicleID, false, passengers);
    i = 0;
    while i < ArraySize(passengers) {
      passenger = passengers[i];
      if IsDefined(passenger) {
        this.DamagePassengerInCollision(passenger, evt.instigator, evt.hitDirection);
      };
      i += 1;
    };
  }

  private final func DamagePassengerInCollision(passenger: wref<GameObject>, instigator: wref<GameObject>, hitDirection: Vector4) -> Void {
    let attack: ref<IAttack>;
    let attackContext: AttackInitContext;
    let hitEvent: ref<gameHitEvent>;
    let player: ref<GameObject> = GetPlayer(this.GetVehicle().GetGame());
    if player == passenger {
      if !PlayerDevelopmentSystem.GetData(player).IsNewPerkBoughtAnyLevel(gamedataNewPerkType.Body_Right_Milestone_1) {
        hitEvent = new gameHitEvent();
        hitEvent.attackData = new AttackData();
        hitEvent.target = passenger;
        attackContext.record = TweakDBInterface.GetAttackRecord(t"Attacks.PlayerCarInCollision");
        attackContext.instigator = instigator;
        attackContext.source = instigator;
        attack = IAttack.Create(attackContext);
        hitEvent.attackData.SetAttackDefinition(attack);
        hitEvent.attackData.AddFlag(hitFlag.Nonlethal, n"Player Passenger in vehicle in collision");
        hitEvent.attackData.AddFlag(hitFlag.CanDamageSelf, n"Player Passenger in vehicle in collision");
        hitEvent.attackData.SetSource(instigator);
        hitEvent.attackData.SetInstigator(instigator);
        hitEvent.attackData.SetAttackDefinition(attack);
        hitEvent.hitPosition = passenger.GetWorldPosition();
        hitEvent.hitDirection = hitDirection;
        GameInstance.GetDamageSystem(this.GetVehicle().GetGame()).QueueHitEvent(hitEvent, this.GetVehicle());
      };
    } else {
      hitEvent = new gameHitEvent();
      hitEvent.attackData = new AttackData();
      hitEvent.target = passenger;
      attackContext.record = TweakDBInterface.GetAttackRecord(t"Attacks.NPCCarInCollision");
      attackContext.instigator = instigator;
      attackContext.source = instigator;
      attack = IAttack.Create(attackContext);
      hitEvent.attackData.SetAttackDefinition(attack);
      hitEvent.attackData.AddFlag(hitFlag.CanDamageSelf, n"npc Passenger in vehicle in collision");
      hitEvent.attackData.AddFlag(hitFlag.NPCPassengerVehicleCollision, n"npc Passenger in vehicle in collision");
      hitEvent.attackData.AddFlag(hitFlag.ForceNoCrit, n"npc Passenger in vehicle in collision");
      hitEvent.attackData.SetSource(instigator);
      hitEvent.attackData.SetInstigator(instigator);
      hitEvent.attackData.SetAttackDefinition(attack);
      hitEvent.hitPosition = passenger.GetWorldPosition();
      hitEvent.hitDirection = hitDirection;
      GameInstance.GetDamageSystem(this.GetVehicle().GetGame()).QueueHitEvent(hitEvent, this.GetVehicle());
      if VehicleComponent.GetDriver(this.GetVehicle().GetGame(), this.GetVehicle(), this.GetVehicle().GetEntityID()) == this {
        this.GetVehicle().ActivateTemporaryLossOfControl();
      };
      passenger.QueueEvent(new VehicleRammedEvent());
    };
  }

  protected cb func OnRemoteControlCameraToggleEvent(evt: ref<VehicleRemoteControlCameraToggleEvent>) -> Bool {
    GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"camera_transition_effect_stop");
  }

  protected cb func OnToggleBrokenTireEvent(evt: ref<VehicleToggleBrokenTireEvent>) -> Bool {
    let panicDrivingEvent: ref<TriggerPanicDrivingEvent>;
    let vehicle: wref<VehicleObject> = this.GetVehicle();
    let driver: wref<ScriptedPuppet> = VehicleComponent.GetDriverMounted(this.GetVehicle().GetGame(), this.GetVehicle().GetEntityID()) as ScriptedPuppet;
    if IsDefined(vehicle) && IsDefined(driver) {
      if driver.IsCrowd() && driver.IsAggressive() && !driver.IsPrevention() && !this.GetVehicle().IsQuest() {
        panicDrivingEvent = new TriggerPanicDrivingEvent();
        this.GetVehicle().QueueEvent(panicDrivingEvent);
      };
    };
  }

  protected cb func OnAIVehicleDisabledEvent(evt: ref<AIVehicleDisabledEvent>) -> Bool {
    if !this.GetPS().GetIsDestroyed() && !GameInstance.GetGodModeSystem(this.GetVehicle().GetGame()).HasGodMode(this.GetVehicle().GetEntityID(), gameGodModeType.Immortal) {
      this.DestroyMappin();
      this.DisableRadio();
      this.GetVehicle().TriggerExitBehavior();
    };
  }

  private final func ShouldVisualDestructionBeSet() -> Void {
    let vehVisualDestroRecord: wref<VehicleVisualDestruction_Record> = this.GetVehicle().GetRecord().VisualDestruction();
    if IsDefined(vehVisualDestroRecord) && vehVisualDestroRecord.SetVisualDestruction() {
      this.VehicleVisualDestructionSetup();
    };
  }

  private final func VehicleVisualDestructionSetup() -> Void {
    let i: Int32;
    let pointValues: [Float; 15];
    let vehVisualDestroRecord: wref<VehicleVisualDestruction_Record> = this.GetVehicle().GetRecord().VisualDestruction();
    pointValues[0] = vehVisualDestroRecord.BackLeft();
    pointValues[1] = vehVisualDestroRecord.Back();
    pointValues[2] = vehVisualDestroRecord.BackRight();
    pointValues[3] = vehVisualDestroRecord.Left();
    pointValues[4] = 0.00;
    pointValues[5] = vehVisualDestroRecord.Right();
    pointValues[6] = vehVisualDestroRecord.Left();
    pointValues[7] = 0.00;
    pointValues[8] = vehVisualDestroRecord.Right();
    pointValues[9] = vehVisualDestroRecord.Left();
    pointValues[10] = 0.00;
    pointValues[11] = vehVisualDestroRecord.Right();
    pointValues[12] = vehVisualDestroRecord.FrontLeft();
    pointValues[13] = vehVisualDestroRecord.Front();
    pointValues[14] = vehVisualDestroRecord.FrontRight();
    this.GetVehicle().SetDestructionGridPointValues(0u, pointValues, false);
    i = 0;
    while i < ArraySize(pointValues) {
      pointValues[i] = vehVisualDestroRecord.Roof();
      i += 1;
    };
    this.GetVehicle().SetDestructionGridPointValues(1u, pointValues, false);
  }

  protected cb func OnVehicleQuestVisualDestructionEvent(evt: ref<VehicleQuestVisualDestructionEvent>) -> Bool {
    let i: Int32;
    let pointValues: [Float; 15];
    pointValues[0] = evt.backLeft;
    pointValues[1] = evt.back;
    pointValues[2] = evt.backRight;
    pointValues[3] = evt.left;
    pointValues[4] = 0.00;
    pointValues[5] = evt.right;
    pointValues[6] = evt.left;
    pointValues[7] = 0.00;
    pointValues[8] = evt.right;
    pointValues[9] = evt.left;
    pointValues[10] = 0.00;
    pointValues[11] = evt.right;
    pointValues[12] = evt.frontLeft;
    pointValues[13] = evt.front;
    pointValues[14] = evt.frontRight;
    this.GetVehicle().SetDestructionGridPointValues(0u, pointValues, evt.accumulate);
    i = 0;
    while i < ArraySize(pointValues) {
      pointValues[i] = evt.roof;
      i += 1;
    };
    this.GetVehicle().SetDestructionGridPointValues(1u, pointValues, evt.accumulate);
  }

  private final func EvaluateInteractions() -> Void {
    this.EvaluateTrunkAndHoodInteractions();
    if VehicleComponent.CheckVehicleDesiredTag(this.GetVehicle(), n"player_bike") {
      this.ToggleVehReadyInteractions(true);
    };
    this.ToggleInitialVehDoorInteractions();
  }

  private final func ToggleInitialVehDoorInteractions() -> Void {
    let i: Int32;
    let seats: array<wref<VehicleSeat_Record>>;
    let evt: ref<InteractionMultipleSetEnableEvent> = new InteractionMultipleSetEnableEvent();
    VehicleComponent.GetSeats(this.GetVehicle().GetGame(), this.GetVehicle(), seats);
    i = 0;
    while i < ArraySize(seats) {
      evt.PushBack(true, seats[i].SeatName());
      i += 1;
    };
    this.GetVehicle().QueueEvent(evt);
  }

  private final func EvaluateTrunkAndHoodInteractions() -> Void {
    let evt: ref<InteractionMultipleSetEnableEvent> = new InteractionMultipleSetEnableEvent();
    evt.PushBack(true, n"trunk");
    evt.PushBack(true, n"hood");
    this.GetVehicle().QueueEvent(evt);
  }

  private final func EvaluateTrunkInteractions() -> Void {
    this.ToggleInteraction(n"trunk", true);
  }

  private final func EvaluateHoodInteractions() -> Void {
    this.ToggleInteraction(n"hood", true);
  }

  protected final func ToggleVehReadyInteractions(toggle: Bool, opt layer: CName) -> Void {
    let evt: ref<InteractionMultipleSetEnableEvent> = new InteractionMultipleSetEnableEvent();
    if IsNameValid(layer) {
      evt.PushBack(toggle, layer);
    } else {
      evt.PushBack(toggle, n"PuppetClose");
      evt.PushBack(toggle, n"PuppetFar");
    };
    this.GetVehicle().QueueEvent(evt);
  }

  private final func EvaluateDoorState() -> Void {
    let state: VehicleDoorState;
    let size: Int32 = 6;
    let i: Int32 = 0;
    while i < size {
      state = this.GetPS().GetDoorState(IntEnum<EVehicleDoor>(i));
      this.SetDoorAnimFeatureData(IntEnum<EVehicleDoor>(i), state);
      i += 1;
    };
  }

  protected final func SetDoorAnimFeatureData(door: EVehicleDoor, state: VehicleDoorState) -> Void {
    let animFeature: ref<AnimFeature_PartData>;
    let animFeatureName: CName;
    if Equals(state, VehicleDoorState.Open) {
      animFeature = new AnimFeature_PartData();
      animFeature.state = 2;
      animFeatureName = EnumValueToName(n"EVehicleDoor", EnumInt(door));
      AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), animFeatureName, animFeature);
    };
  }

  protected cb func OnVehicleLightSetupEvent(evt: ref<VehicleLightSetupEvent>) -> Bool {
    let vehicleRecord: ref<Vehicle_Record> = this.GetVehicle().GetRecord();
    let headlightCount: Int32 = vehicleRecord.GetHeadlightColorCount();
    let interiorlightCount: Int32 = vehicleRecord.GetInteriorColorCount();
    let brakelightCount: Int32 = vehicleRecord.GetBrakelightColorCount();
    let leftBlinkerlightCount: Int32 = vehicleRecord.GetLeftBlinkerlightColorCount();
    let rightBLinkerlightCount: Int32 = vehicleRecord.GetRightBLinkerlightColorCount();
    let reverselightCount: Int32 = vehicleRecord.GetReverselightColorCount();
    let utilityLightCount: Int32 = vehicleRecord.GetUtilityLightColorCount();
    if headlightCount == 4 {
      this.GetVehicleController().SetLightColor(vehicleELightType.Head, new Color(Cast<Uint8>(vehicleRecord.GetHeadlightColorItem(0)), Cast<Uint8>(vehicleRecord.GetHeadlightColorItem(1)), Cast<Uint8>(vehicleRecord.GetHeadlightColorItem(2)), Cast<Uint8>(vehicleRecord.GetHeadlightColorItem(3))));
    };
    if interiorlightCount == 4 {
      this.GetVehicleController().SetLightColor(vehicleELightType.Interior, new Color(Cast<Uint8>(vehicleRecord.GetInteriorColorItem(0)), Cast<Uint8>(vehicleRecord.GetInteriorColorItem(1)), Cast<Uint8>(vehicleRecord.GetInteriorColorItem(2)), Cast<Uint8>(vehicleRecord.GetInteriorColorItem(3))));
    };
    if brakelightCount == 4 {
      this.GetVehicleController().SetLightColor(vehicleELightType.Brake, new Color(Cast<Uint8>(vehicleRecord.GetBrakelightColorItem(0)), Cast<Uint8>(vehicleRecord.GetBrakelightColorItem(1)), Cast<Uint8>(vehicleRecord.GetBrakelightColorItem(2)), Cast<Uint8>(vehicleRecord.GetBrakelightColorItem(3))));
    };
    if leftBlinkerlightCount == 4 {
      this.GetVehicleController().SetLightColor(vehicleELightType.LeftBlinker, new Color(Cast<Uint8>(vehicleRecord.GetLeftBlinkerlightColorItem(0)), Cast<Uint8>(vehicleRecord.GetLeftBlinkerlightColorItem(1)), Cast<Uint8>(vehicleRecord.GetLeftBlinkerlightColorItem(2)), Cast<Uint8>(vehicleRecord.GetLeftBlinkerlightColorItem(3))));
    };
    if rightBLinkerlightCount == 4 {
      this.GetVehicleController().SetLightColor(vehicleELightType.RightBlinker, new Color(Cast<Uint8>(vehicleRecord.GetRightBLinkerlightColorItem(0)), Cast<Uint8>(vehicleRecord.GetRightBLinkerlightColorItem(1)), Cast<Uint8>(vehicleRecord.GetRightBLinkerlightColorItem(2)), Cast<Uint8>(vehicleRecord.GetRightBLinkerlightColorItem(3))));
    };
    if reverselightCount == 4 {
      this.GetVehicleController().SetLightColor(vehicleELightType.Reverse, new Color(Cast<Uint8>(vehicleRecord.GetReverselightColorItem(0)), Cast<Uint8>(vehicleRecord.GetReverselightColorItem(1)), Cast<Uint8>(vehicleRecord.GetReverselightColorItem(2)), Cast<Uint8>(vehicleRecord.GetReverselightColorItem(3))));
    };
    if utilityLightCount == 4 {
      this.GetVehicleController().SetLightColor(vehicleELightType.Utility, new Color(Cast<Uint8>(vehicleRecord.GetUtilityLightColorItem(0)), Cast<Uint8>(vehicleRecord.GetUtilityLightColorItem(1)), Cast<Uint8>(vehicleRecord.GetUtilityLightColorItem(2)), Cast<Uint8>(vehicleRecord.GetUtilityLightColorItem(3))));
    };
  }

  private final func RegisterInputListener() -> Void {
    let playerPuppet: ref<PlayerPuppet> = GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerMainGameObject() as PlayerPuppet;
    playerPuppet.RegisterInputListener(this, n"VehicleInsideWheel");
    playerPuppet.RegisterInputListener(this, n"VehicleHorn");
    playerPuppet.RegisterInputListener(this, n"VehicleHornHold");
    playerPuppet.RegisterInputListener(this, n"VehicleSiren");
  }

  private final func UnregisterInputListener() -> Void {
    let playerPuppet: ref<PlayerPuppet> = GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerMainGameObject() as PlayerPuppet;
    if IsDefined(playerPuppet) {
      playerPuppet.UnregisterInputListener(this);
    };
  }

  private final func LoadExplodedState() -> Void {
    let destroyedAppearanceName: CName;
    let vehicleRecord: ref<Vehicle_Record>;
    if this.GetPS().GetHasExploded() {
      vehicleRecord = this.GetVehicle().GetRecord();
      destroyedAppearanceName = vehicleRecord.DestroyedAppearance();
      if IsNameValid(destroyedAppearanceName) {
        this.GetVehicle().ScheduleAppearanceChange(destroyedAppearanceName);
      };
      GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"fire", false);
      this.GetVehicle().DetachAllParts();
      this.GetVehicle().SetHasExploded();
    };
  }

  private final func SetupThrusterFX() -> Void {
    let toggle: Bool = this.GetPS().GetThrusterState();
    if toggle {
      GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"thrusters", true);
    } else {
      GameObjectEffectHelper.BreakEffectLoopEvent(this.GetVehicle(), n"thrusters");
    };
  }

  protected final func ToggleScanningComponent(toggle: Bool) -> Void {
    this.m_scanningComponent.Toggle(toggle);
  }

  private final func EnableTargetingComponents() -> Void {
    this.ToggleTargetingComponents(true);
  }

  private final func DisableTargetingComponents() -> Void {
    this.ToggleTargetingComponents(false);
  }

  private final func ToggleTargetingComponents(on: Bool) -> Void {
    let front_left_tire: ref<TargetingComponent> = this.FindComponentByName(n"front_left_tire") as TargetingComponent;
    let front_right_tire: ref<TargetingComponent> = this.FindComponentByName(n"front_right_tire") as TargetingComponent;
    let back_left_tire: ref<TargetingComponent> = this.FindComponentByName(n"back_left_tire") as TargetingComponent;
    let back_right_tire: ref<TargetingComponent> = this.FindComponentByName(n"back_right_tire") as TargetingComponent;
    let gas_tank: ref<TargetingComponent> = this.FindComponentByName(n"gas_tank") as TargetingComponent;
    if IsDefined(front_left_tire) {
      front_left_tire.Toggle(on);
    };
    if IsDefined(front_right_tire) {
      front_right_tire.Toggle(on);
    };
    if IsDefined(back_left_tire) {
      back_left_tire.Toggle(on);
    };
    if IsDefined(back_right_tire) {
      back_right_tire.Toggle(on);
    };
    if IsDefined(gas_tank) {
      gas_tank.Toggle(on);
    };
  }

  private final func BroadcastEnvironmentalHazardStimuli() -> Void {
    let broadcaster: ref<StimBroadcasterComponent> = this.GetVehicle().GetStimBroadcasterComponent();
    if IsDefined(broadcaster) {
      broadcaster.AddActiveStimuli(this.GetVehicle(), gamedataStimType.EnvironmentalHazard, -1.00);
      this.m_isBroadcastingHazardStims = true;
    };
  }

  private final func RemoveEnvironmentalHazardStimuli() -> Void {
    let broadcaster: ref<StimBroadcasterComponent> = this.GetVehicle().GetStimBroadcasterComponent();
    if IsDefined(broadcaster) {
      broadcaster.RemoveActiveStimuliByName(this.GetVehicle(), gamedataStimType.EnvironmentalHazard);
      this.m_isBroadcastingHazardStims = false;
    };
  }

  private final func RemoveQuickhackQueue() -> Void {
    let currentlyUploadingAction: ref<ScriptableDeviceAction> = this.GetVehicle().GetCurrentlyUploadingAction();
    QuickHackableQueueHelper.RemoveQuickhackQueue(null, currentlyUploadingAction);
  }

  protected cb func OnDeath(evt: ref<gameDeathEvent>) -> Bool {
    this.ToggleSiren(false, false);
    this.m_interaction.Toggle(false);
    this.ExplodeVehicle(evt.instigator);
    this.GetVehicle().FindAndRewardKiller(gameKillType.Normal);
    this.DisableTargetingComponents();
    this.UnmountTrunkBody();
    this.ToggleInteraction(n"None", false);
    this.TryToKnockDownBike();
    this.DestroyMappin();
    this.RemoveEnvironmentalHazardStimuli();
    this.RemoveQuickhackQueue();
    if this.GetVehicle().ShouldPreventionReactToExplosion() {
      PreventionSystem.NotifyVehicleExplosion(this.GetVehicle());
    };
  }

  protected cb func OnVehicleWaterEvent(evt: ref<VehicleWaterEvent>) -> Bool {
    let vehicle: wref<VehicleObject>;
    if evt.isInWater && !this.GetPS().GetIsSubmerged() {
      vehicle = this.GetVehicle();
      this.GetPS().SetIsSubmerged(true);
      this.BreakAllDamageStageFX(true);
      GameObjectEffectHelper.BreakEffectLoopEvent(vehicle, n"fire");
      this.GetPS().SetIsDestroyed(true);
      this.DisableVehicle();
      this.DestroyRandomWindow();
      this.RemoveVehicleDOT();
      GameObjectEffectHelper.StartEffectEvent(vehicle, n"underwater_bubbles");
      GameObjectEffectHelper.StopEffectEvent(vehicle, n"immobilized");
      this.QueueLethalVehicleImpactToAllNonFriendlyAggressivePassengers(n"OnVehicleWaterEvent");
    };
    ScriptedPuppet.ReevaluateOxygenConsumption(this.m_mountedPlayer);
  }

  protected cb func OnInteractionActivated(evt: ref<InteractionActivationEvent>) -> Bool {
    let context: VehicleActionsContext;
    if this.GetVehicle().IsCrowdVehicle() && StatusEffectSystem.ObjectHasStatusEffectWithTag(evt.activator, n"BlockTrafficInteractions") {
      return false;
    };
    if Equals(evt.eventType, gameinteractionsEInteractionEventType.EIET_activate) {
      if evt.IsInputLayerEvent() {
        this.CreateObjectActionsCallbackController(evt.activator);
      };
    } else {
      if Equals(evt.eventType, gameinteractionsEInteractionEventType.EIET_deactivate) {
        if evt.IsInputLayerEvent() {
          this.DestroyObjectActionsCallbackController();
        };
      };
    };
    context.requestorID = this.GetVehicle().GetEntityID();
    context.processInitiatorObject = evt.activator;
    context.interactionLayerTag = evt.layerData.tag;
    context.eventType = evt.eventType;
    this.GetPS().DetermineActionsToPush(this.m_interaction, context, this.m_objectActionsCallbackCtrl, false);
    if this.GetVehicle() == (this.GetVehicle() as TankObject) {
      this.EvaluatePanzerInteractions();
    };
  }

  protected cb func OnInteractionUsed(evt: ref<InteractionChoiceEvent>) -> Bool {
    this.ExecuteAction(evt.choice, evt.activator);
    this.m_interaction.ResetChoices();
  }

  protected final func EvaluatePanzerInteractions() -> Void {
    let state: VehicleDoorInteractionState = this.GetPS().GetDoorInteractionState(EVehicleDoor.seat_front_left);
    if Equals(state, VehicleDoorInteractionState.Locked) || Equals(state, VehicleDoorInteractionState.Reserved) || Equals(state, VehicleDoorInteractionState.QuestLocked) {
      this.GetPS().SetDoorInteractionState(EVehicleDoor.seat_front_left, VehicleDoorInteractionState.Available, "PanzerTankFailsafe");
    };
  }

  private final func RegisterToHUDManager(shouldRegister: Bool) -> Void {
    let registration: ref<HUDManagerRegistrationRequest>;
    let hudManager: ref<HUDManager> = GameInstance.GetScriptableSystemsContainer(this.GetVehicle().GetGame()).Get(n"HUDManager") as HUDManager;
    if IsDefined(hudManager) {
      registration = new HUDManagerRegistrationRequest();
      registration.SetProperties(this.GetVehicle(), shouldRegister);
      hudManager.QueueRequest(registration);
    };
  }

  protected cb func OnHUDInstruction(evt: ref<HUDInstruction>) -> Bool {
    if Equals(evt.highlightInstructions.GetState(), InstanceState.ON) {
      this.GetPS().SetFocusModeData(true);
    } else {
      if evt.highlightInstructions.WasProcessed() {
        this.GetPS().SetFocusModeData(false);
      };
    };
  }

  public final func GetVehicleStateForScanner() -> String {
    if this.GetPS().GetIsDestroyed() {
      return "LocKey#49082";
    };
    if this.GetAnyDoorAvailable() && VehicleComponent.IsAnyPassengerCrowd(this.GetVehicle().GetGame(), this.GetVehicle()) {
      return "LocKey#49085";
    };
    if !this.GetAnySlotAvailable() {
      return "LocKey#49083";
    };
    return "LocKey#49084";
  }

  private final func GetAnySlotAvailable(opt checkOccupied: Bool) -> Bool {
    let door: EVehicleDoor;
    let i: Int32;
    let seatSet: array<wref<VehicleSeat_Record>>;
    let slotName: CName;
    VehicleComponent.GetSeats(this.GetVehicle().GetGame(), this.GetVehicle(), seatSet);
    i = 0;
    while i < ArraySize(seatSet) {
      slotName = seatSet[i].SeatName();
      this.GetPS().GetVehicleDoorEnum(door, slotName);
      if Equals(this.GetPS().GetDoorInteractionState(door), VehicleDoorInteractionState.Available) {
        if !VehicleComponent.IsSlotOccupiedByActivePassenger(this.GetVehicle().GetGame(), this.GetVehicle().GetEntityID(), slotName) {
          return true;
        };
      };
      i += 1;
    };
    return false;
  }

  private final func GetAnyDoorAvailable(opt checkOccupied: Bool) -> Bool {
    let door: EVehicleDoor;
    let i: Int32;
    let seatSet: array<wref<VehicleSeat_Record>>;
    let slotName: CName;
    VehicleComponent.GetSeats(this.GetVehicle().GetGame(), this.GetVehicle(), seatSet);
    i = 0;
    while i < ArraySize(seatSet) {
      slotName = seatSet[i].SeatName();
      this.GetPS().GetVehicleDoorEnum(door, slotName);
      if Equals(this.GetPS().GetDoorInteractionState(door), VehicleDoorInteractionState.Available) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final func SetVehicleScannerDirty() -> Void {
    this.GetVehicle().SetScannerDirty(true);
  }

  protected cb func OnQuickSlotCommandUsed(evt: ref<QuickSlotCommandUsed>) -> Bool {
    this.ExecuteAction(evt.action, GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerControlledGameObject());
  }

  private final func ExecuteAction(const choice: script_ref<InteractionChoice>, executor: wref<GameObject>) -> Void {
    let action: ref<DeviceAction>;
    let i: Int32 = 0;
    while i < ArraySize(Deref(choice).data) {
      action = FromVariant<ref<DeviceAction>>(Deref(choice).data[i]);
      if IsDefined(action) {
        this.ExecuteAction(action, executor);
      };
      i += 1;
    };
  }

  private final func ExecuteAction(action: ref<DeviceAction>, opt executor: wref<GameObject>) -> Void {
    let sAction: ref<ScriptableDeviceAction> = action as ScriptableDeviceAction;
    if sAction != null {
      sAction.RegisterAsRequester(this.GetEntity().GetEntityID());
      if executor != null {
        sAction.SetExecutor(executor);
      };
      sAction.ProcessRPGAction((this.GetEntity() as VehicleObject).GetGame());
    };
  }

  private final func ToggleInteraction(layer: CName, toggle: Bool) -> Void {
    let interactionEvent: ref<InteractionSetEnableEvent> = new InteractionSetEnableEvent();
    interactionEvent.enable = toggle;
    interactionEvent.layer = layer;
    this.GetVehicle().QueueEvent(interactionEvent);
  }

  private final func ProcessExplosionEffects() -> Void {
    this.BreakAllDamageStageFX(true);
    GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"explosion", false);
    GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"fire", false);
    GameObject.PlaySoundEvent(this.GetVehicle(), n"v_car_test_explosion");
    GameObjectEffectHelper.StopEffectEvent(this.GetVehicle(), n"immobilized");
  }

  private final func ExplodeVehicle(instigator: wref<GameObject>) -> Void {
    let attackID: TweakDBID;
    let broadcaster: ref<StimBroadcasterComponent>;
    let explosionAttack: ref<Attack_GameEffect>;
    let hitFlags: array<SHitFlag>;
    let playerPuppet: ref<PlayerPuppet> = instigator as PlayerPuppet;
    let isPlayerDead: Bool = GetPlayer(this.GetVehicle().GetGame()).IsDead();
    let vehicleRecord: ref<Vehicle_Record> = this.GetVehicle().GetRecord();
    let destroyedAppearanceName: CName = vehicleRecord.DestroyedAppearance();
    if this.GetPS().GetIsSubmerged() {
      return;
    };
    if !isPlayerDead {
      this.GetPS().SetIsDestroyed(true);
    };
    this.RemoveVehicleDOT();
    if this.GetVehicle().IsPlayerMounted() {
      GameInstance.GetTelemetrySystem(this.GetVehicle().GetGame()).LogPlayerVehicleExploded();
    };
    attackID = t"Attacks.CarMediumKill";
    if IsDefined(playerPuppet) {
      playerPuppet.OnExplosiveDeviceExploded();
    };
    if !IsDefined(instigator) {
      instigator = this.GetVehicle();
    };
    if TDBID.IsValid(attackID) {
      explosionAttack = RPGManager.PrepareGameEffectAttack(this.GetVehicle().GetGame(), instigator, this.GetVehicle(), attackID, hitFlags);
      if IsDefined(explosionAttack) {
        explosionAttack.StartAttack();
        this.GetVehicle().DetachAllParts();
        this.ProcessExplosionEffects();
        if !isPlayerDead {
          this.GetPS().SetHasExploded(true);
        };
        if IsNameValid(destroyedAppearanceName) {
          this.GetVehicle().ScheduleAppearanceChange(destroyedAppearanceName);
        };
        this.CancelVehicleExitDelayedEvent();
        this.KillPassengers(instigator);
        broadcaster = this.GetVehicle().GetStimBroadcasterComponent();
        if IsDefined(broadcaster) {
          broadcaster.TriggerSingleBroadcast(this.GetVehicle(), gamedataStimType.DeviceExplosion);
        };
      };
    };
    if this.GetVehicle().IsInTrafficPhysicsState() {
      this.SendExplodedAIEvent();
    };
    GameInstance.GetAudioSystem(this.GetVehicle().GetGame()).PlayOnEmitter(n"v_car_explosion_warning_stop", this.GetVehicle().GetEntityID(), n"vehicle_engine_emitter");
  }

  private final func SendExplodedAIEvent() -> Void {
    let evt: ref<AIEvent> = new AIEvent();
    evt.name = n"VehicleExploded";
    this.GetVehicle().QueueEvent(evt);
  }

  protected final func KillPassengers(instigator: wref<GameObject>) -> Void {
    let i: Int32;
    let playerID: EntityID;
    let seSystem: ref<StatusEffectSystem>;
    let godModeSystem: ref<GodModeSystem> = GameInstance.GetGodModeSystem(this.GetVehicle().GetGame());
    let mountInfos: array<MountingInfo> = GameInstance.GetMountingFacility(this.GetVehicle().GetGame()).GetMountingInfoMultipleWithIds(this.GetVehicle().GetEntityID());
    if ArraySize(mountInfos) <= 0 {
      return;
    };
    seSystem = GameInstance.GetStatusEffectSystem(this.GetVehicle().GetGame());
    if !IsDefined(seSystem) {
      return;
    };
    playerID = GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerMainGameObject().GetEntityID();
    i = 0;
    while i < ArraySize(mountInfos) {
      if EntityID.IsDefined(mountInfos[i].childId) {
        if !godModeSystem.HasGodMode(mountInfos[i].childId, gameGodModeType.Invulnerable) {
          if playerID == mountInfos[i].childId {
            seSystem.ApplyStatusEffect(mountInfos[i].childId, t"BaseStatusEffect.ForceKill", GameObject.GetTDBID(instigator), instigator.GetEntityID());
          } else {
            if !GameInstance.GetGodModeSystem(this.GetVehicle().GetGame()).HasGodMode(mountInfos[i].childId, gameGodModeType.Immortal) {
              seSystem.ApplyStatusEffect(mountInfos[i].childId, t"BaseStatusEffect.ForceKill", GameObject.GetTDBID(instigator), instigator.GetEntityID());
            };
          };
        };
      };
      i += 1;
    };
  }

  private final func SetImmortalityMode() -> Void {
    let vehicleID: EntityID = this.GetVehicle().GetEntityID();
    let recordID: TweakDBID = this.GetVehicle().GetRecordID();
    let record: ref<Vehicle_Record> = TweakDBInterface.GetVehicleRecord(recordID);
    if IsDefined(record) {
      if record.TagsContains(n"Invulnerable") {
        GameInstance.GetGodModeSystem(this.GetVehicle().GetGame()).AddGodMode(vehicleID, gameGodModeType.Invulnerable, n"Default");
        return;
      };
      if record.TagsContains(n"Immortal") {
        GameInstance.GetGodModeSystem(this.GetVehicle().GetGame()).AddGodMode(vehicleID, gameGodModeType.Immortal, n"Default");
        return;
      };
    };
  }

  private final func ClearImmortalityMode() -> Void {
    GameInstance.GetGodModeSystem(this.GetVehicle().GetGame()).ClearGodMode(this.GetVehicle().GetEntityID(), n"Default");
  }

  private final func StealVehicle(opt slotID: MountingSlotId) -> Void {
    let stealEvent: ref<StealVehicleEvent>;
    let vehicleHijackEvent: ref<VehicleHijackEvent>;
    let vehicle: wref<VehicleObject> = this.GetVehicle();
    if !IsDefined(vehicle) {
      return;
    };
    if IsNameValid(slotID.id) {
      vehicleHijackEvent = new VehicleHijackEvent();
      vehicleHijackEvent.driverAllowedToGetAggressive = Equals(slotID.id, n"seat_front_left");
      VehicleComponent.QueueEventToPassenger(vehicle.GetGame(), vehicle, slotID, vehicleHijackEvent);
    };
    stealEvent = new StealVehicleEvent();
    vehicle.QueueEvent(stealEvent);
  }

  protected final func ToggleVehicleSystems(toggle: Bool, vehicle: Bool, engine: Bool, opt lockState: VehicleQuestEngineLockState) -> Void {
    if this.GetVehicle().IsVehicleOnStateLocked() {
      if Equals(lockState, VehicleQuestEngineLockState.DontToggleIfLocked) {
        return;
      };
      this.GetVehicle().LockVehicleOnState(false);
    };
    if vehicle {
      this.GetVehicle().TurnVehicleOn(toggle);
    };
    if engine {
      this.GetVehicle().TurnEngineOn(toggle);
    };
    if Equals(lockState, VehicleQuestEngineLockState.Lock) {
      this.GetVehicle().LockVehicleOnState(true);
    };
  }

  public final func SetDelayDisableCarAlarm(evtActivationTime: Float) -> Void {
    let delayEvt: ref<DisableAlarmEvent>;
    let duration: Float;
    if this.m_vehicleDisableAlarmDelayID == GetInvalidDelayID() && this.GetVehicleControllerPS().IsAlarmOn() {
      duration = evtActivationTime;
      delayEvt = new DisableAlarmEvent();
      this.m_vehicleDisableAlarmDelayID = GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), delayEvt, duration);
    };
  }

  protected cb func OnForceCarAlarm(evt: ref<ForceCarAlarm>) -> Bool {
    this.SetDelayDisableCarAlarm(evt.GetActivationTime());
  }

  protected cb func OnDisableAlarm(evt: ref<DisableAlarmEvent>) -> Bool {
    this.m_vehicleDisableAlarmDelayID = GetInvalidDelayID();
    this.GetPS().DisableAlarm();
  }

  protected cb func OnChangeState(evt: ref<vehicleChangeStateEvent>) -> Bool {
    let defaultState: ref<VehicleDefaultState_Record>;
    let evnt: ref<VehicleLightQuestToggleEvent>;
    let i: Int32;
    let record: ref<Vehicle_Record>;
    let recordID: TweakDBID;
    let size: Int32;
    let crystalDomeQuestModified: Bool = this.GetPS().GetIsCrystalDomeQuestModified();
    if Equals(evt.newState, vehicleEState.On) {
      if !crystalDomeQuestModified {
        if this.GetVehicle() != (this.GetVehicle() as AVObject) {
          this.ToggleCrystalDome(true);
        };
      };
      if this.m_mounted {
        this.DrivingStimuli(true);
      };
    };
    if NotEquals(evt.newState, vehicleEState.On) {
      if !crystalDomeQuestModified {
        if this.GetVehicle() != (this.GetVehicle() as AVObject) {
          this.ToggleCrystalDome(false);
        };
      };
      this.DrivingStimuli(false);
    };
    if !this.GetPS().GetIsDefaultLightToggleSet() {
      recordID = this.GetVehicle().GetRecordID();
      record = TweakDBInterface.GetVehicleRecord(recordID);
      defaultState = record.VehDefaultState();
      size = defaultState.GetLightToggleComponentsCount();
      i = 0;
      while i < size {
        evnt = new VehicleLightQuestToggleEvent();
        evnt.lightType = IntEnum<vehicleELightType>(defaultState.GetLightToggleComponentsItem(i));
        evnt.toggle = defaultState.DefaultLightToggle();
        this.GetVehicle().QueueEvent(evnt);
        i += 1;
      };
      this.GetPS().SetIsDefaultLightToggleSet(true);
    };
  }

  protected cb func OnVehicleQuestCrystalDomeEvent(evt: ref<VehicleQuestCrystalDomeEvent>) -> Bool {
    let toggle: Bool = this.GetPS().GetCrystalDomeQuestState();
    this.ToggleCrystalDome(toggle, true);
  }

  private final func DrivingStimuli(broadcast: Bool) -> Void {
    let broadcaster: ref<StimBroadcasterComponent> = this.GetVehicle().GetStimBroadcasterComponent();
    if !IsDefined(broadcaster) {
      return;
    };
    if broadcast && !this.m_broadcasting {
      broadcaster.SetSingleActiveStimuli(this.GetVehicle(), gamedataStimType.Driving, -1.00);
      this.m_broadcasting = true;
    } else {
      if !broadcast && this.m_broadcasting {
        broadcaster.RemoveActiveStimuliByName(this.GetVehicle(), gamedataStimType.Driving);
        this.m_broadcasting = false;
      };
    };
  }

  protected cb func OnRepeatDirectEnvironmentalHazardStimuli(evt: ref<RepeatDirectEnvironmentalHazardStimEvent>) -> Bool {
    if this.m_isBroadcastingHazardStims {
      this.SendDirectEnvironmentalHazardStimuli(evt.target);
      GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), evt, 1.00);
    };
  }

  private final func SendDirectEnvironmentalHazardStimuli(target: ref<GameObject>) -> Void {
    let broadcaster: ref<StimBroadcasterComponent> = this.GetVehicle().GetStimBroadcasterComponent();
    if IsDefined(broadcaster) {
      broadcaster.SendDrirectStimuliToTarget(this.GetVehicle(), gamedataStimType.EnvironmentalHazard, target, true, true);
    };
  }

  private final func SetupCrystalDome() -> Void {
    if this.GetPS().GetCrystalDomeState() {
      this.ToggleCrystalDome(true, false, true, 0.50);
    };
  }

  private final func ToggleCrystalDome(toggle: Bool, opt force: Bool, opt instant: Bool, opt instantDelay: Float, opt meshVisibilityDelay: Float, opt fastStop: Bool) -> Void {
    let animFeature: ref<AnimFeature_VehicleState>;
    let crystalDomeMeshDelayEvent: ref<VehicleCrystalDomeMeshVisibilityDelayEvent>;
    let crystalDomeOffDelayEvent: ref<VehicleCrystalDomeOffDelayEvent>;
    let crystalDomeOnDelayEvent: ref<VehicleCrystalDomeOnDelayEvent>;
    let vehicle: ref<VehicleObject> = this.GetVehicle();
    let gameInstance: GameInstance = vehicle.GetGame();
    let player: ref<PlayerPuppet> = GetPlayer(gameInstance);
    if !force && !VehicleComponent.IsMountedToProvidedVehicle(gameInstance, player.GetEntityID(), vehicle) && !instant {
      return;
    };
    animFeature = new AnimFeature_VehicleState();
    if toggle {
      this.ToggleTargetingSystemForPanzer(this.m_mountedPlayer, true);
      if instant {
        if instantDelay == 0.00 {
          GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"crystal_dome_instant_on", true);
        } else {
          crystalDomeOnDelayEvent = new VehicleCrystalDomeOnDelayEvent();
          GameInstance.GetDelaySystem(gameInstance).DelayEvent(vehicle, crystalDomeOnDelayEvent, instantDelay);
        };
        if meshVisibilityDelay == 0.00 {
          animFeature.tppEnabled = !vehicle.GetCameraManager().IsTPPActive();
          AnimationControllerComponent.ApplyFeatureToReplicate(vehicle, n"VehicleState", animFeature);
          this.TogglePanzerShadowMeshes(vehicle.GetCameraManager().IsTPPActive());
        } else {
          crystalDomeMeshDelayEvent = new VehicleCrystalDomeMeshVisibilityDelayEvent();
          GameInstance.GetDelaySystem(gameInstance).DelayEvent(vehicle, crystalDomeMeshDelayEvent, meshVisibilityDelay);
        };
      } else {
        GameObjectEffectHelper.StartEffectEvent(vehicle, n"crystal_dome_start", true);
        animFeature.tppEnabled = !vehicle.GetCameraManager().IsTPPActive();
        AnimationControllerComponent.ApplyFeatureToReplicate(vehicle, n"VehicleState", animFeature);
        this.TogglePanzerShadowMeshes(vehicle.GetCameraManager().IsTPPActive());
      };
      this.GetPS().SetCrystalDomeState(true);
    } else {
      this.ToggleTargetingSystemForPanzer(this.m_mountedPlayer, false);
      if fastStop {
        GameObjectEffectHelper.StartEffectEvent(vehicle, n"crystal_dome_stop_fast", true);
      } else {
        GameObjectEffectHelper.StartEffectEvent(vehicle, n"crystal_dome_stop", true);
      };
      crystalDomeOffDelayEvent = new VehicleCrystalDomeOffDelayEvent();
      GameInstance.GetDelaySystem(gameInstance).DelayEvent(vehicle, crystalDomeOffDelayEvent, 0.60);
    };
  }

  private final func TogglePanzerShadowMeshes(toggle: Bool) -> Void {
    let shadowMesh1: ref<IComponent>;
    if this.GetVehicle() != (this.GetVehicle() as TankObject) {
      return;
    };
    shadowMesh1 = this.FindComponentByName(n"av_militech_basilisk__ext01_canopy_a_shadow");
    shadowMesh1.Toggle(toggle);
  }

  protected cb func OnVehicleCrystalDomeOffDelayEvent(evt: ref<VehicleCrystalDomeOffDelayEvent>) -> Bool {
    let animFeature: ref<AnimFeature_VehicleState> = new AnimFeature_VehicleState();
    this.GetPS().SetCrystalDomeState(false);
    animFeature.tppEnabled = false;
    AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), n"VehicleState", animFeature);
  }

  protected cb func OnVehicleCrystalDomeOnDelayEvent(evt: ref<VehicleCrystalDomeOnDelayEvent>) -> Bool {
    GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"crystal_dome_instant_on", true);
  }

  protected cb func OnVehicleCrystalDomeMeshVisibilityDelayEvent(evt: ref<VehicleCrystalDomeMeshVisibilityDelayEvent>) -> Bool {
    let animFeature: ref<AnimFeature_VehicleState> = new AnimFeature_VehicleState();
    animFeature.tppEnabled = !this.GetVehicle().GetCameraManager().IsTPPActive();
    AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), n"VehicleState", animFeature);
    this.TogglePanzerShadowMeshes(!this.GetVehicle().GetCameraManager().IsTPPActive());
  }

  private final func ToggleTargetingSystemForPanzer(mountedPlayer: ref<PlayerPuppet>, enable: Bool) -> Void {
    let targetingSystem: ref<TargetingSystem>;
    let vehicle: ref<VehicleObject> = this.GetVehicle();
    if IsDefined(mountedPlayer) {
      if enable {
        if !this.m_isIgnoredInTargetingSystem {
          targetingSystem = GameInstance.GetTargetingSystem(vehicle.GetGame());
          targetingSystem.AddIgnoredCollisionEntities(vehicle);
          targetingSystem.AddIgnoredLookAtEntity(mountedPlayer, vehicle.GetEntityID());
          this.m_isIgnoredInTargetingSystem = true;
        };
      } else {
        if this.m_isIgnoredInTargetingSystem {
          targetingSystem = GameInstance.GetTargetingSystem(vehicle.GetGame());
          targetingSystem.RemoveIgnoredCollisionEntities(vehicle);
          targetingSystem.RemoveIgnoredLookAtEntity(mountedPlayer, vehicle.GetEntityID());
          this.m_isIgnoredInTargetingSystem = false;
        };
      };
    };
  }

  private final func TogglePlayerHitShapesForPanzer(mountedPlayer: ref<PlayerPuppet>, enable: Bool) -> Void {
    if IsDefined(mountedPlayer) {
      if enable && !this.m_arePlayerHitShapesEnabled {
        HitShapeUserDataBase.EnableHitShape(mountedPlayer, n"head", false);
        HitShapeUserDataBase.EnableHitShape(mountedPlayer, n"chest", false);
        HitShapeUserDataBase.EnableHitShape(mountedPlayer, n"legs", false);
        this.m_arePlayerHitShapesEnabled = true;
      } else {
        if this.m_arePlayerHitShapesEnabled {
          HitShapeUserDataBase.DisableHitShape(mountedPlayer, n"head", false);
          HitShapeUserDataBase.DisableHitShape(mountedPlayer, n"chest", false);
          HitShapeUserDataBase.DisableHitShape(mountedPlayer, n"legs", false);
          this.m_arePlayerHitShapesEnabled = false;
        };
      };
    };
  }

  private final func DoPanzerCleanup() -> Void {
    if !this.m_arePlayerHitShapesEnabled {
      this.TogglePlayerHitShapesForPanzer(this.m_mountedPlayer, true);
    };
    if this.m_isIgnoredInTargetingSystem {
      this.ToggleTargetingSystemForPanzer(this.m_mountedPlayer, false);
    };
  }

  public final const func HasPreventionPassenger() -> Bool {
    return this.m_preventionPassengers > 0;
  }

  private final func RegisterWantedLevelListener() -> Void {
    if !IsDefined(this.m_currentWantedLevelCallback) {
      this.m_uiWantedBarBB = GameInstance.GetBlackboardSystem(this.GetVehicle().GetGame()).Get(GetAllBlackboardDefs().UI_WantedBar);
      if IsDefined(this.m_uiWantedBarBB) {
        this.m_currentWantedLevelCallback = this.m_uiWantedBarBB.RegisterListenerInt(GetAllBlackboardDefs().UI_WantedBar.CurrentWantedLevel, this, n"OnCurrentWantedLevelChanged");
      };
    };
  }

  private final func DoPreventionVehicleCleanup() -> Void {
    if this.m_preventionPassengers > 0 {
      this.UnregisterPreventionPassengerCallbacks(this.GetVehicle().GetGame(), this.GetVehicle().GetEntityID());
    };
    this.m_preventionPassengers = 0;
    this.UnregisterWantedLevelListener();
  }

  private final func UnregisterWantedLevelListener() -> Void {
    if IsDefined(this.m_currentWantedLevelCallback) {
      if IsDefined(this.m_uiWantedBarBB) {
        this.m_uiWantedBarBB.UnregisterListenerInt(GetAllBlackboardDefs().UI_WantedBar.CurrentWantedLevel, this.m_currentWantedLevelCallback);
        this.m_uiWantedBarBB = null;
      };
    };
  }

  private final func UnregisterPreventionPassengerCallbacks(gi: GameInstance, vehicleID: EntityID) -> Void {
    let i: Int32;
    let mountInfos: array<MountingInfo>;
    if !GameInstance.IsValid(gi) || !EntityID.IsDefined(vehicleID) {
      return;
    };
    mountInfos = GameInstance.GetMountingFacility(gi).GetMountingInfoMultipleWithIds(vehicleID);
    i = 0;
    while i < ArraySize(mountInfos) {
      GameInstance.GetPreventionSpawnSystem(gi).UnregisterEntityDeathCallback(this, "OnPreventionPassengerDeath", mountInfos[i].childId);
      i += 1;
    };
  }

  public final func InjectThreat(instigator: wref<GameObject>) -> Void {
    let i: Int32;
    let mountedObj: wref<ScriptedPuppet>;
    let mountInfos: array<MountingInfo> = GameInstance.GetMountingFacility(this.GetVehicle().GetGame()).GetMountingInfoMultipleWithIds(this.GetVehicle().GetEntityID());
    if IsDefined(instigator) {
      i = 0;
      while i < ArraySize(mountInfos) {
        mountedObj = GameInstance.FindEntityByID(this.GetVehicle().GetGame(), mountInfos[i].childId) as ScriptedPuppet;
        if IsDefined(mountedObj) && AIActionHelper.TryChangingAttitudeToHostile(mountedObj, instigator) {
          TargetTrackingExtension.InjectThreat(mountedObj, instigator);
        };
        i += 1;
      };
    };
  }

  protected cb func OnVehicleForceOccupantOut(evt: ref<VehicleForceOccupantOut>) -> Bool {
    this.StealVehicle();
  }

  protected cb func OnActionDemolition(evt: ref<ActionDemolition>) -> Bool {
    this.StealVehicle(evt.slotID);
  }

  protected cb func OnActionEngineering(evt: ref<ActionEngineering>) -> Bool {
    this.StealVehicle();
  }

  protected cb func OnVehicleQuestDoorLocked(evt: ref<VehicleQuestDoorLocked>) -> Bool;

  protected cb func OnVehicleDoorInteraction(evt: ref<VehicleDoorInteraction>) -> Bool {
    let door: EVehicleDoor;
    let doorID: CName = evt.slotID;
    if this.GetVehicleDoorEnum(door, doorID) {
      this.EvaluateDoorReaction(doorID, false, this.GetPS().GetDoorState(door));
    };
  }

  protected cb func OnVehicleDoorOpen(evt: ref<VehicleDoorOpen>) -> Bool {
    let PSVehicleDoorCloseRequest: ref<VehicleDoorClose>;
    let autoCloseDelay: Float;
    this.EvaluateDoorReaction(evt.slotID, evt.forceScene, VehicleDoorState.Open);
    if evt.shouldAutoClose {
      PSVehicleDoorCloseRequest = new VehicleDoorClose();
      PSVehicleDoorCloseRequest.slotID = evt.slotID;
      autoCloseDelay = evt.autoCloseTime;
      if autoCloseDelay == 0.00 {
        autoCloseDelay = 1.50;
      };
      GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayPSEvent(this.GetPS().GetID(), this.GetPS().GetClassName(), PSVehicleDoorCloseRequest, autoCloseDelay, true);
    };
    this.GetPS().SetHasAnyDoorOpen(true);
  }

  protected cb func OnVehicleDoorClose(evt: ref<VehicleDoorClose>) -> Bool {
    this.EvaluateDoorReaction(evt.slotID, evt.forceScene, VehicleDoorState.Closed);
  }

  protected final func GetVehicleDoorEnum(out door: EVehicleDoor, doorName: CName) -> Bool {
    let res: Int32 = Cast<Int32>(EnumValueFromName(n"EVehicleDoor", doorName));
    if res < 0 {
      return false;
    };
    door = IntEnum<EVehicleDoor>(res);
    return true;
  }

  protected cb func OnVehicleDoorInteractionStateChange(evt: ref<VehicleDoorInteractionStateChange>) -> Bool {
    let layerName: CName = EnumValueToName(n"EVehicleDoor", EnumInt(evt.door));
    this.DetermineInteractionState(layerName);
  }

  protected final func EvaluateDoorReaction(doorID: CName, immediate: Bool, doorState: VehicleDoorState) -> Void {
    let animFeature: ref<AnimFeature_PartData>;
    let animFeatureName: CName;
    let door: EVehicleDoor;
    let vehDataPackage: wref<VehicleDataPackage_Record>;
    VehicleComponent.GetVehicleDataPackage(this.GetVehicle().GetGame(), this.GetVehicle(), vehDataPackage);
    animFeature = new AnimFeature_PartData();
    animFeatureName = doorID;
    if !this.GetVehicleDoorEnum(door, doorID) {
      return;
    };
    if Equals(doorState, VehicleDoorState.Open) {
      animFeature.state = 1;
      animFeature.duration = immediate ? vehDataPackage.Immediate_open_close_duration() : vehDataPackage.Open_close_duration();
      AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), animFeatureName, animFeature);
      AnimationControllerComponent.PushEvent(this.GetVehicle(), this.GetAnimEventName(doorState, door));
    };
    if Equals(doorState, VehicleDoorState.Closed) {
      animFeature.state = 3;
      animFeature.duration = immediate ? vehDataPackage.Immediate_open_close_duration() : vehDataPackage.Open_close_duration();
      AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), animFeatureName, animFeature);
      AnimationControllerComponent.PushEvent(this.GetVehicle(), this.GetAnimEventName(doorState, door));
    };
  }

  private final func GetAnimEventName(doorState: VehicleDoorState, door: EVehicleDoor) -> CName {
    if Equals(door, EVehicleDoor.seat_front_left) {
      if Equals(doorState, VehicleDoorState.Open) {
        return n"doorOpenFrontLeft";
      };
      if Equals(doorState, VehicleDoorState.Closed) {
        return n"doorCloseFrontLeft";
      };
    };
    if Equals(door, EVehicleDoor.seat_front_right) {
      if Equals(doorState, VehicleDoorState.Open) {
        return n"doorOpenFrontRight";
      };
      if Equals(doorState, VehicleDoorState.Closed) {
        return n"doorCloseFrontRight";
      };
    };
    if Equals(door, EVehicleDoor.seat_back_left) {
      if Equals(doorState, VehicleDoorState.Open) {
        return n"doorOpenBackLeft";
      };
      if Equals(doorState, VehicleDoorState.Closed) {
        return n"doorCloseBackLeft";
      };
    };
    if Equals(door, EVehicleDoor.seat_back_right) {
      if Equals(doorState, VehicleDoorState.Open) {
        return n"doorOpenBackRight";
      };
      if Equals(doorState, VehicleDoorState.Closed) {
        return n"doorCloseBackRight";
      };
    };
    return n"None";
  }

  protected cb func OnVehicleExternalDoorRequestEvent(evt: ref<VehicleExternalDoorRequestEvent>) -> Bool {
    let PSvehicleDooropenRequest: ref<VehicleDoorOpen> = new VehicleDoorOpen();
    PSvehicleDooropenRequest.slotID = evt.slotName;
    PSvehicleDooropenRequest.shouldAutoClose = evt.autoClose;
    PSvehicleDooropenRequest.autoCloseTime = evt.autoCloseTime;
    GameInstance.GetPersistencySystem(this.GetVehicle().GetGame()).QueuePSEvent(this.GetPS().GetID(), this.GetPS().GetClassName(), PSvehicleDooropenRequest);
  }

  protected cb func OnVehicleExternalWindowRequestEvent(evt: ref<VehicleExternalWindowRequestEvent>) -> Bool {
    let PSvehicleWindowcloseRequest: ref<VehicleWindowClose>;
    let PSvehicleWindowopenRequest: ref<VehicleWindowOpen>;
    if evt.shouldOpen {
      PSvehicleWindowopenRequest = new VehicleWindowOpen();
      PSvehicleWindowopenRequest.slotID = evt.slotName;
      PSvehicleWindowopenRequest.speed = evt.speed;
      GameInstance.GetPersistencySystem(this.GetVehicle().GetGame()).QueuePSEvent(this.GetPS().GetID(), this.GetPS().GetClassName(), PSvehicleWindowopenRequest);
      this.GetVehicle().NotifyWindowChange(evt.slotName, true);
    } else {
      PSvehicleWindowcloseRequest = new VehicleWindowClose();
      PSvehicleWindowcloseRequest.slotID = evt.slotName;
      PSvehicleWindowcloseRequest.speed = evt.speed;
      GameInstance.GetPersistencySystem(this.GetVehicle().GetGame()).QueuePSEvent(this.GetPS().GetID(), this.GetPS().GetClassName(), PSvehicleWindowcloseRequest);
      this.GetVehicle().NotifyWindowChange(evt.slotName, false);
    };
  }

  protected cb func OnVehicleWindowOpen(evt: ref<VehicleWindowOpen>) -> Bool {
    this.EvaluateWindowReaction(evt.slotID, evt.speed);
  }

  protected cb func OnVehicleWindowClose(evt: ref<VehicleWindowClose>) -> Bool {
    this.EvaluateWindowReaction(evt.slotID, evt.speed);
  }

  protected final func EvaluateWindowReaction(doorID: CName, speed: CName) -> Void {
    let door: EVehicleDoor;
    let windowState: EVehicleWindowState;
    let animFeature: ref<AnimFeature_PartData> = new AnimFeature_PartData();
    let animFeatureName: CName = StringToName(NameToString(doorID) + "_window");
    if !this.GetVehicleDoorEnum(door, doorID) {
      return;
    };
    windowState = this.GetPS().GetWindowState(door);
    if Equals(speed, n"Fast") {
      animFeature.duration = 0.20;
    } else {
      animFeature.duration = -1.00;
    };
    if Equals(windowState, EVehicleWindowState.Open) {
      animFeature.state = 1;
      AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), animFeatureName, animFeature);
    };
    if Equals(windowState, EVehicleWindowState.Closed) {
      animFeature.state = 3;
      AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), animFeatureName, animFeature);
    };
  }

  private final func EvaluateWindowState() -> Void {
    let state: EVehicleWindowState;
    let size: Int32 = 6;
    let i: Int32 = 0;
    while i < size {
      state = this.GetPS().GetWindowState(IntEnum<EVehicleDoor>(i));
      this.SetWindowAnimFeatureData(IntEnum<EVehicleDoor>(i), state);
      i += 1;
    };
  }

  protected final func SetWindowAnimFeatureData(door: EVehicleDoor, state: EVehicleWindowState) -> Void {
    let animFeature: ref<AnimFeature_PartData>;
    let animFeatureName: CName;
    if Equals(state, EVehicleWindowState.Open) {
      animFeature = new AnimFeature_PartData();
      animFeature.state = 2;
      animFeatureName = StringToName(NameToString(EnumValueToName(n"EVehicleDoor", EnumInt(door))) + "_window");
      AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), animFeatureName, animFeature);
    };
  }

  protected cb func OnToggleDoorInteractionEvent(evt: ref<ToggleDoorInteractionEvent>) -> Bool {
    this.EvaluateTrunkAndHoodInteractions();
  }

  protected cb func OnOpenTrunk(evt: ref<VehicleOpenTrunk>) -> Bool {
    let delayEvt: ref<ToggleDoorInteractionEvent> = new ToggleDoorInteractionEvent();
    let vehicle: wref<VehicleObject> = this.GetEntity() as VehicleObject;
    AnimationControllerComponent.PushEvent(vehicle, n"doorOpenTrunk");
    this.EvaluateDoorReaction(n"trunk", false, VehicleDoorState.Open);
    this.ToggleInteraction(n"trunk", false);
    GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetEntity() as VehicleObject, delayEvt, 1.00);
    this.GetPS().SetHasAnyDoorOpen(true);
    if this.GetVehicle().IsPrevention() {
      this.InjectThreat(GetPlayer(this.GetVehicle().GetGame()));
    };
  }

  protected cb func OnCloseTrunk(evt: ref<VehicleCloseTrunk>) -> Bool {
    let delayEvt: ref<ToggleDoorInteractionEvent> = new ToggleDoorInteractionEvent();
    let vehicle: wref<VehicleObject> = this.GetEntity() as VehicleObject;
    AnimationControllerComponent.PushEvent(vehicle, n"doorCloseTrunk");
    this.EvaluateDoorReaction(n"trunk", false, VehicleDoorState.Closed);
    this.ToggleInteraction(n"trunk", false);
    GameInstance.GetDelaySystem((this.GetEntity() as VehicleObject).GetGame()).DelayEvent(this.GetVehicle(), delayEvt, 1.00);
  }

  protected cb func OnVehicleDumpBody(evt: ref<VehicleDumpBody>) -> Bool {
    let mountingInfo: MountingInfo;
    let playerPuppet: ref<PlayerPuppet>;
    let playerStateMachineBlackboard: ref<IBlackboard>;
    let slotID: MountingSlotId;
    let vehDataPackage: wref<VehicleDataPackage_Record>;
    let unmountEvent: ref<UnmountingRequest> = new UnmountingRequest();
    let dumpBodyWorkspotEvent: ref<DumpBodyWorkspotDelayEvent> = new DumpBodyWorkspotDelayEvent();
    let vehicleDumpBodyCloseTrunkEvent: ref<VehicleDumpBodyCloseTrunkEvent> = new VehicleDumpBodyCloseTrunkEvent();
    slotID.id = n"trunk";
    VehicleComponent.OpenDoor(this.GetVehicle(), slotID);
    this.ToggleInteraction(n"trunk", false);
    playerPuppet = GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerControlledGameObject() as PlayerPuppet;
    playerStateMachineBlackboard = GameInstance.GetBlackboardSystem(this.GetVehicle().GetGame()).GetLocalInstanced(playerPuppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    mountingInfo = GameInstance.GetMountingFacility(this.GetVehicle().GetGame()).GetMountingInfoSingleWithObjects(playerPuppet);
    this.m_trunkNpcBody = GameInstance.FindEntityByID(this.GetVehicle().GetGame(), mountingInfo.childId) as GameObject;
    unmountEvent.lowLevelMountingInfo = mountingInfo;
    playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.CarryingDisposal, true);
    playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.IsInteractingWithDevice, true);
    playerStateMachineBlackboard.SetInt(GetAllBlackboardDefs().PlayerStateMachine.BodyDisposalDetailed, 1);
    GameInstance.GetMountingFacility(this.GetVehicle().GetGame()).Unmount(unmountEvent);
    VehicleComponent.GetVehicleDataPackage(this.GetVehicle().GetGame(), this.GetVehicle(), vehDataPackage);
    GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), dumpBodyWorkspotEvent, 0.00);
    GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), vehicleDumpBodyCloseTrunkEvent, TweakDBInterface.GetFloat(vehDataPackage.GetID() + t".body_dump_close_trunk_delay", 2.00));
  }

  protected cb func OnDumpBodyWorkspotDelayEvent(evt: ref<DumpBodyWorkspotDelayEvent>) -> Bool {
    let workspotSystem: ref<WorkspotGameSystem> = GameInstance.GetWorkspotSystem(this.GetVehicle().GetGame());
    workspotSystem.PlayNpcInWorkspot(this.m_trunkNpcBody, GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerControlledGameObject(), this.GetVehicle(), n"trunkBodyDisposalNpc", n"None", 0.00);
    workspotSystem.PlayInDevice(this.GetVehicle(), GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerControlledGameObject(), n"lockedCamera", n"trunkBodyDisposalPlayer", n"None", n"bodyDisposalSync", 0.10, WorkspotSlidingBehaviour.DontPlayAtResourcePosition);
  }

  public final func MountNpcBodyToTrunk() -> Void {
    let playerStateMachineBlackboard: ref<IBlackboard> = GameInstance.GetBlackboardSystem(this.GetVehicle().GetGame()).GetLocalInstanced(GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerControlledGameObject().GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.CarryingDisposal, false);
    playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.IsInteractingWithDevice, false);
    playerStateMachineBlackboard.SetInt(GetAllBlackboardDefs().PlayerStateMachine.BodyDisposalDetailed, 0);
    this.MountEntityToSlot(this.GetVehicle().GetEntityID(), this.m_trunkNpcBody.GetEntityID(), n"trunk_body");
    if !RPGManager.IsInventoryEmpty(this.m_trunkNpcBody) {
      this.m_trunkNpcBody as NPCPuppet.DropLootBag();
    };
  }

  protected cb func OnVehicleDumpBodyCloseTrunkEvent(evt: ref<VehicleDumpBodyCloseTrunkEvent>) -> Bool {
    let slotID: MountingSlotId;
    slotID.id = n"trunk";
    VehicleComponent.CloseDoor(this.GetVehicle(), slotID);
    this.EvaluateTrunkInteractions();
  }

  protected cb func OnVehicleTakeBody(evt: ref<VehicleTakeBody>) -> Bool {
    let slotID: MountingSlotId;
    let pickupBodyWorkspotEvent: ref<PickupBodyWorkspotDelayEvent> = new PickupBodyWorkspotDelayEvent();
    slotID.id = n"trunk_body";
    let mountingInfo: MountingInfo = GameInstance.GetMountingFacility(this.GetVehicle().GetGame()).GetMountingInfoSingleWithObjects(this.GetVehicle(), slotID);
    this.m_trunkNpcBody = GameInstance.FindEntityByID(this.GetVehicle().GetGame(), mountingInfo.childId) as GameObject;
    let trunkPickUpAIevent: ref<AIEvent> = new AIEvent();
    trunkPickUpAIevent.name = n"InstantUnmount";
    this.m_trunkNpcBody.QueueEvent(trunkPickUpAIevent);
    (this.m_trunkNpcBody as NPCPuppet).SetDisableRagdoll(true);
    GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), pickupBodyWorkspotEvent, 0.00);
  }

  protected cb func OnPickupBodyWorkspotDelayEvent(evt: ref<PickupBodyWorkspotDelayEvent>) -> Bool {
    let workspotSystem: ref<WorkspotGameSystem> = GameInstance.GetWorkspotSystem(this.GetVehicle().GetGame());
    workspotSystem.PlayNpcInWorkspot(this.m_trunkNpcBody, GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerControlledGameObject(), this.GetVehicle(), n"trunkBodyPickupNpc", n"None", 0.00);
    workspotSystem.PlayInDevice(this.GetVehicle(), GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerControlledGameObject(), n"lockedCamera", n"trunkBodyPickupPlayer", n"None", n"bodyPickupSync", 0.10, WorkspotSlidingBehaviour.DontPlayAtResourcePosition);
  }

  public final func FinishTrunkBodyPickup() -> Void {
    this.MountBodyToPlayer(this.m_trunkNpcBody);
    this.ToggleInteraction(n"trunk", false);
    this.EvaluateTrunkInteractions();
    (this.m_trunkNpcBody as NPCPuppet).SetDisableRagdoll(false);
  }

  private final func MountBodyToPlayer(npcBody: wref<GameObject>) -> Void {
    let addCarriedObjectSM: ref<PSMAddOnDemandStateMachine>;
    if !IsDefined(npcBody) {
      return;
    };
    GameInstance.GetStatusEffectSystem(this.GetVehicle().GetGame()).ApplyStatusEffect(npcBody.GetEntityID(), t"BaseStatusEffect.VehicleTrunkBodyPickup", this.GetVehicle().GetRecordID(), this.GetVehicle().GetEntityID());
    addCarriedObjectSM = new PSMAddOnDemandStateMachine();
    addCarriedObjectSM.owner = npcBody;
    addCarriedObjectSM.stateMachineName = n"CarriedObject";
    GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerControlledGameObject().QueueEvent(addCarriedObjectSM);
  }

  protected final func MountEntityToSlot(parentID: EntityID, childId: EntityID, slot: CName) -> Void {
    let lowLevelMountingInfo: MountingInfo;
    let mountingRequest: ref<MountingRequest> = new MountingRequest();
    let mountData: ref<MountEventData> = new MountEventData();
    let mountOptions: ref<MountEventOptions> = new MountEventOptions();
    lowLevelMountingInfo.parentId = parentID;
    lowLevelMountingInfo.childId = childId;
    lowLevelMountingInfo.slotId.id = slot;
    mountingRequest.lowLevelMountingInfo = lowLevelMountingInfo;
    mountingRequest.preservePositionAfterMounting = true;
    mountingRequest.mountData = mountData;
    mountOptions.alive = false;
    mountOptions.occupiedByNonFriendly = false;
    mountingRequest.mountData.mountEventOptions = mountOptions;
    GameInstance.GetMountingFacility(this.GetEntity() as GameObject.GetGame()).Mount(mountingRequest);
  }

  private final func UnmountTrunkBody() -> Void {
    let instantUnmount: ref<AIEvent>;
    let slotID: MountingSlotId;
    slotID.id = n"trunk_body";
    let mountingInfo: MountingInfo = GameInstance.GetMountingFacility(this.GetVehicle().GetGame()).GetMountingInfoSingleWithObjects(this.GetVehicle(), slotID);
    let npcBody: wref<GameObject> = GameInstance.FindEntityByID(this.GetVehicle().GetGame(), mountingInfo.childId) as GameObject;
    if !IsDefined(npcBody) {
      return;
    };
    instantUnmount = new AIEvent();
    instantUnmount.name = n"InstantUnmount";
    npcBody.QueueEvent(instantUnmount);
  }

  protected cb func OnVehiclePlayerTrunk(evt: ref<VehiclePlayerTrunk>) -> Bool {
    let storageBB: ref<IBlackboard>;
    let storageData: ref<StorageUserData>;
    let player: ref<GameObject> = GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerControlledGameObject();
    Stash.ProcessStashRetroFixes(this.GetVehicle());
    storageData = new StorageUserData();
    storageData.storageObject = this.GetVehicle();
    storageBB = GameInstance.GetBlackboardSystem(player.GetGame()).Get(GetAllBlackboardDefs().StorageBlackboard);
    if IsDefined(storageBB) {
      storageBB.SetVariant(GetAllBlackboardDefs().StorageBlackboard.StorageData, ToVariant(storageData), true);
    };
  }

  protected cb func OnOpenHood(evt: ref<VehicleOpenHood>) -> Bool {
    let delayEvt: ref<ToggleDoorInteractionEvent> = new ToggleDoorInteractionEvent();
    this.EvaluateDoorReaction(n"hood", false, VehicleDoorState.Open);
    this.ToggleInteraction(n"hood", false);
    GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetEntity() as VehicleObject, delayEvt, 1.00);
    this.GetPS().SetHasAnyDoorOpen(true);
    this.UpdateDamageEngineEffects();
    if this.GetVehicle().IsPrevention() {
      this.InjectThreat(GetPlayer(this.GetVehicle().GetGame()));
    };
  }

  protected cb func OnCloseHood(evt: ref<VehicleCloseHood>) -> Bool {
    let delayEvt: ref<ToggleDoorInteractionEvent> = new ToggleDoorInteractionEvent();
    this.EvaluateDoorReaction(n"hood", false, VehicleDoorState.Closed);
    this.ToggleInteraction(n"hood", false);
    GameInstance.GetDelaySystem((this.GetEntity() as VehicleObject).GetGame()).DelayEvent(this.GetVehicle(), delayEvt, 1.00);
    this.UpdateDamageEngineEffects();
  }

  protected cb func OnSummonStartedEvent(evt: ref<SummonStartedEvent>) -> Bool {
    if Equals(evt.state, vehicleSummonState.EnRoute) || Equals(evt.state, vehicleSummonState.AlreadySummoned) {
      this.CreateMappin();
      if Equals(evt.state, vehicleSummonState.EnRoute) {
        this.SendParkEvent(false);
      };
      if Equals(evt.state, vehicleSummonState.AlreadySummoned) {
        this.HonkAndFlash();
      };
    };
  }

  protected cb func OnSummonFinishedEvent(evt: ref<SummonFinishedEvent>) -> Bool {
    if Equals(evt.state, vehicleSummonState.Arrived) {
      this.CreateMappin();
      this.HonkAndFlash();
      this.SendParkEvent(true);
      this.PlaySummonArrivalSFX();
    };
  }

  private final func PlaySummonArrivalSFX() -> Void {
    let audioEvent: ref<SoundPlayEvent> = new SoundPlayEvent();
    audioEvent.soundName = n"ui_jingle_vehicle_arrive";
    GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerControlledGameObject().QueueEvent(audioEvent);
  }

  public final func PlayDelayedHonk(honkTime: Float, delayTime: Float) -> Void {
    let hornEvt: ref<VehicleQuestDelayedHornEvent> = new VehicleQuestDelayedHornEvent();
    hornEvt.honkTime = honkTime;
    hornEvt.delayTime = delayTime;
    this.GetVehicle().QueueEvent(hornEvt);
  }

  public final func PlayHonkForDuration(honkTime: Float) -> Void {
    let hornEvt: ref<VehicleQuestHornEvent> = new VehicleQuestHornEvent();
    hornEvt.honkTime = honkTime;
    this.GetVehicle().QueueEvent(hornEvt);
  }

  private final func HonkAndFlash() -> Void {
    let hornEvt: ref<VehicleQuestHornEvent>;
    let toggle: Bool = !this.GetVehicleController().AreDefaultLightsToggled();
    let lightsEvt: ref<VehicleLightQuestToggleEvent> = new VehicleLightQuestToggleEvent();
    lightsEvt.toggle = toggle;
    lightsEvt.lightType = vehicleELightType.Default;
    this.OnVehicleLightQuestToggleEvent(lightsEvt);
    lightsEvt = new VehicleLightQuestToggleEvent();
    lightsEvt.toggle = !toggle;
    lightsEvt.lightType = vehicleELightType.Default;
    GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), lightsEvt, 0.33);
    hornEvt = new VehicleQuestHornEvent();
    hornEvt.honkTime = 2.00;
    this.GetVehicle().QueueEvent(hornEvt);
  }

  private final func IsPlayerVehicle() -> Void {
    if this.GetVehicle().IsPlayerVehicle() {
      this.GetPS().SetIsPlayerVehicle(true);
    };
  }

  private final func SetupAuxillary() -> Void {
    let vehDataPackage: wref<VehicleDataPackage_Record>;
    VehicleComponent.GetVehicleDataPackage(this.GetVehicle().GetGame(), this.GetVehicle(), vehDataPackage);
    this.m_hasSpoiler = vehDataPackage.HasSpoiler();
    this.m_useAuxiliary = vehDataPackage.UseAuxiliary();
    if this.m_hasSpoiler {
      this.m_spoilerUp = vehDataPackage.SpoilerSpeedToDeploy();
      this.m_spoilerDown = vehDataPackage.SpoilerSpeedToRetract();
    };
    this.m_hasTurboCharger = vehDataPackage.HasTurboCharger();
    if this.m_hasTurboCharger {
      this.m_overheatEffectBlackboard = new worldEffectBlackboard();
      this.m_overheatEffectBlackboard.SetValue(n"overheatValue", 1.00);
    };
  }

  private final func SetupWheels() -> Void {
    let record: ref<Vehicle_Record> = this.GetVehicle().GetRecord();
    let wheelAnimFeature: ref<AnimFeature_CamberData> = new AnimFeature_CamberData();
    let animFeatureName: CName = n"wheel_data";
    wheelAnimFeature.rightFrontCamber = record.RightFrontCamber();
    wheelAnimFeature.leftFrontCamber = record.LeftFrontCamber();
    wheelAnimFeature.rightBackCamber = record.RightBackCamber();
    wheelAnimFeature.leftBackCamber = record.LeftBackCamber();
    wheelAnimFeature.rightFrontCamberOffset = Vector4.Vector3To4(record.RightFrontCamberOffset());
    wheelAnimFeature.leftFrontCamberOffset = Vector4.Vector3To4(record.LeftFrontCamberOffset());
    wheelAnimFeature.rightBackCamberOffset = Vector4.Vector3To4(record.RightBackCamberOffset());
    wheelAnimFeature.leftBackCamberOffset = Vector4.Vector3To4(record.LeftBackCamberOffset());
    AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), animFeatureName, wheelAnimFeature);
  }

  protected cb func OnGridDestruction(evt: ref<VehicleGridDestructionEvent>) -> Bool {
    let biggestImpact: Float;
    let broadcaster: ref<StimBroadcasterComponent>;
    let desiredChange: Float;
    let gridState: Float;
    let owner: wref<VehicleObject>;
    let bike: ref<BikeObject> = this.GetVehicle() as BikeObject;
    let i: Int32 = 0;
    while i < ArraySize(evt.rawChange) {
      gridState = evt.state[i];
      desiredChange = evt.desiredChange[i];
      this.SendDestructionDataToGraph(i, gridState);
      this.DetermineAdditionalEngineFX(i, gridState);
      if desiredChange > biggestImpact {
        biggestImpact = desiredChange;
      };
      i += 1;
    };
    this.PlayVehicleVisialCustomisationCollisionGlitch();
    if evt.damageMultiplier > 0.00 {
      this.CreateHitEventOnSelf(evt.damageMultiplier, evt.impactPoint, evt.otherVehicle, evt.rammedOtherVehicle, evt.otherVehicleRammed);
    };
    if IsDefined(bike) && bike.IsTiltControlEnabled() && evt.damageMultiplier >= 0.00 && !VehicleComponent.HasActiveDriver(this.GetVehicle().GetGame(), this.GetVehicle(), this.GetVehicle().GetEntityID()) {
      this.TryToKnockDownBike();
    };
    owner = this.GetVehicle();
    broadcaster = owner.GetStimBroadcasterComponent();
    if IsDefined(broadcaster) {
      if biggestImpact < 0.03 {
        broadcaster.TriggerSingleBroadcast(owner, gamedataStimType.CrowdIllegalAction, 10.00);
      } else {
        broadcaster.TriggerSingleBroadcast(owner, gamedataStimType.VehicleHit, 5.00);
      };
    };
  }

  private final func CreateHitEventOnSelf(damageMultiplier: Float, impactPoint: Vector3, otherVehicle: wref<GameObject>, rammedOtherVehicle: Bool, otherVehicleRammed: Bool) -> Void {
    let attack: ref<IAttack>;
    let attackContext: AttackInitContext;
    let hasDriver: Bool = VehicleComponent.HasActiveDriver(this.GetVehicle().GetGame(), this.GetVehicle(), this.GetVehicle().GetEntityID());
    let otherVehicleObject: wref<VehicleObject> = otherVehicle as VehicleObject;
    let otherCarIsPlayers: Bool = IsDefined(otherVehicleObject) ? otherVehicleObject.IsPlayerDriver() : false;
    let evt: ref<gameHitEvent> = new gameHitEvent();
    evt.attackData = new AttackData();
    if hasDriver && !otherVehicleRammed {
      attackContext.instigator = VehicleComponent.GetDriver(this.GetVehicle().GetGame(), this.GetVehicle(), this.GetVehicle().GetEntityID());
      attackContext.source = this.GetVehicle();
    } else {
      if IsDefined(otherVehicle) && (otherVehicleRammed || !hasDriver) && VehicleComponent.HasActiveDriver(this.GetVehicle().GetGame(), otherVehicleObject, otherVehicle.GetEntityID()) {
        attackContext.instigator = VehicleComponent.GetDriver(otherVehicleObject.GetGame(), otherVehicleObject, otherVehicleObject.GetEntityID());
        attackContext.source = otherVehicle;
      } else {
        if IsDefined(otherVehicle) {
          attackContext.instigator = otherVehicle;
          attackContext.source = otherVehicle;
        } else {
          attackContext.instigator = this.GetVehicle();
          attackContext.source = this.GetVehicle();
        };
      };
    };
    attackContext.record = TweakDBInterface.GetAttackRecord(t"Attacks.VehicleImpact");
    attack = IAttack.Create(attackContext);
    evt.target = this.GetVehicle();
    evt.attackData.SetVehicleImpactForce(damageMultiplier);
    evt.attackData.SetAttackPosition(Cast<Vector4>(impactPoint));
    evt.hitPosition = Cast<Vector4>(impactPoint);
    if otherVehicleRammed {
      evt.attackData.AddFlag(hitFlag.CriticalHit, n"VehicleRamming");
    };
    if otherCarIsPlayers && !otherVehicleRammed {
      evt.attackData.AddFlag(hitFlag.VehicleImpactWithPlayer, n"VehicleImpact");
    };
    evt.attackData.AddFlag(hitFlag.VehicleImpact, n"VehicleImpact");
    evt.attackData.AddFlag(hitFlag.CanDamageSelf, n"VehicleImpact");
    evt.attackData.AddFlag(hitFlag.FriendlyFire, n"VehicleImpact");
    evt.attackData.SetAttackType(attackContext.record.AttackType().Type());
    evt.attackData.SetSource(attackContext.source);
    evt.attackData.SetInstigator(attackContext.instigator);
    evt.attackData.SetAttackTime(EngineTime.ToFloat(GameInstance.GetTimeSystem(this.GetVehicle().GetGame()).GetSimTime()));
    evt.attackData.SetAttackDefinition(attack);
    evt.attackData.PreAttack();
    GameInstance.GetDamageSystem(this.GetVehicle().GetGame()).QueueHitEvent(evt, this.GetVehicle());
  }

  public final func HandleBikeCollisionReaction(impactVelocityChange: Float, impactHitNormal: Vector4) -> Void {
    let animFeatureData: ref<AnimFeature_KnockOffData>;
    let delayedKnockOffEvent: ref<DelayedBikeKnockOffEvent>;
    let driver: ref<GameObject>;
    let knockDownModifier: Float;
    let knockOffForce: Float;
    let recordID: TweakDBID;
    let vehicleDataPackage: wref<VehicleDataPackage_Record>;
    let vehicleRecord: ref<Vehicle_Record>;
    let bike: ref<BikeObject> = this.GetVehicle() as BikeObject;
    if IsDefined(bike) && GameInstance.GetCameraSystem(bike.GetGame()).IsInCameraFrustum(bike, 2.00, 0.75) {
      driver = VehicleComponent.GetDriverMounted(bike.GetGame(), this.GetVehicle().GetEntityID());
      recordID = bike.GetRecordID();
      vehicleRecord = TweakDBInterface.GetVehicleRecord(recordID);
      vehicleDataPackage = vehicleRecord.VehDataPackage();
      knockDownModifier = TweakDBInterface.GetFloat(t"AIGeneralSettings.aiBikeKnockOffModifier", 1.00);
      knockOffForce = vehicleDataPackage.KnockOffForce() * IsDefined(driver as NPCPuppet) ? knockDownModifier : 1.00;
      if impactVelocityChange > knockOffForce || this.IsBeingDragged() {
        if VehicleComponent.IsMountedToVehicle(bike.GetGame(), driver) {
          bike.EnableAirControl(false);
          delayedKnockOffEvent = new DelayedBikeKnockOffEvent();
          GameInstance.GetDelaySystem(bike.GetGame()).DelayEvent(this.GetVehicle(), delayedKnockOffEvent, 0.40);
          if IsDefined(driver as NPCPuppet) {
            GameInstance.GetDelaySystem(driver.GetGame()).DelayEvent(driver, CreateForceRagdollEvent(n"BikeknockDownEvent"), 0.30);
            this.m_timeSinceLastHit = 0.00;
            animFeatureData = new AnimFeature_KnockOffData();
            animFeatureData.knockedOff = true;
            animFeatureData.direction = GameObject.GetLocalAngleForDirectionInInt(impactHitNormal, driver);
            animFeatureData.force = impactVelocityChange;
            animFeatureData.draggedOff = this.IsBeingDragged();
            AnimationControllerComponent.ApplyFeatureToReplicate(driver, n"KnockOffData", animFeatureData);
            AnimationControllerComponent.PushEventToReplicate(driver, n"KnockOffData");
          };
        };
      };
    };
  }

  protected cb func OnDelayedBikeKnockOffEvent(evt: ref<DelayedBikeKnockOffEvent>) -> Bool {
    let aiEvent: ref<AIEvent>;
    let collisionForce: Vector4;
    let knockOverBike: ref<KnockOverBikeEvent>;
    let recordID: TweakDBID;
    let vehicleDataPackage: wref<VehicleDataPackage_Record>;
    let vehicleRecord: ref<Vehicle_Record>;
    let bike: ref<BikeObject> = this.GetVehicle() as BikeObject;
    let driver: ref<GameObject> = VehicleComponent.GetDriverMounted(bike.GetGame(), this.GetVehicle().GetEntityID());
    if driver != null {
      if IsDefined(driver as NPCPuppet) {
        GameInstance.GetWorkspotSystem(bike.GetGame()).UnmountFromVehicle(this.GetVehicle(), driver, true, n"Bumped");
        knockOverBike = new KnockOverBikeEvent();
        knockOverBike.forceKnockdown = true;
        knockOverBike.applyDirectionalForce = true;
        bike.QueueEvent(knockOverBike);
        aiEvent = new AIEvent();
        aiEvent.name = n"NoDriver";
        bike.QueueEvent(aiEvent);
      } else {
        recordID = this.GetVehicle().GetRecordID();
        vehicleRecord = TweakDBInterface.GetVehicleRecord(recordID);
        vehicleDataPackage = vehicleRecord.VehDataPackage();
        collisionForce = -bike.GetLinearVelocity();
        if Vector4.Length(collisionForce) <= vehicleDataPackage.KnockOffForce() {
          bike.EnableAirControl(true);
          bike.EnableTiltControl(true);
          return false;
        };
        bike.AddCollisionForce(collisionForce);
      };
    };
    this.ToggleVehicleSystems(false, false, false);
  }

  public final func CheckForDrag(impactImpulse: Float) -> Void {
    let currentTimeStamp: Float = EngineTime.ToFloat(GameInstance.GetSimTime(this.GetVehicle().GetGame()));
    let deltaTime: Float = currentTimeStamp - this.m_timeSinceLastHit;
    let isValidData: Bool = this.m_timeSinceLastHit > 0.00 && deltaTime > 0.00;
    if this.m_timeSinceLastHit != 0.00 && isValidData && deltaTime < 1.00 {
      this.m_dragTime += deltaTime;
    } else {
      this.m_dragTime = 0.00;
    };
    this.m_timeSinceLastHit = currentTimeStamp;
  }

  public final func IsBeingDragged() -> Bool {
    return this.m_dragTime > 1.50;
  }

  private final func TryToKnockDownBike() -> Void {
    let knockOverBike: ref<KnockOverBikeEvent>;
    let recordID: TweakDBID = this.GetVehicle().GetRecordID();
    let record: ref<Vehicle_Record> = TweakDBInterface.GetVehicleRecord(recordID);
    if IsDefined(record) {
      if record.TagsContains(n"IgnoreImpulses") {
        return;
      };
    };
    knockOverBike = new KnockOverBikeEvent();
    this.GetVehicle().QueueEvent(knockOverBike);
  }

  public final func ReactToHPChange(destruction: Float) -> Void {
    let damageStageTurnOffDelayEvent: ref<VehicleDamageStageTurnOffEvent>;
    let destroyedAppearanceName: CName;
    let vehicleRecord: wref<Vehicle_Record>;
    let currDmgLevel: Int32 = this.m_damageLevel;
    this.m_damageLevel = this.EvaluateDamageLevel(destruction);
    if this.GetPS().GetIsSubmerged() {
      return;
    };
    if this.m_damageLevel > currDmgLevel {
      if this.m_damageLevel == 1 {
        this.UpdateDamageEngineEffects();
      } else {
        if this.m_damageLevel == 2 {
          this.UpdateDamageEngineEffects();
          this.TutorialCarDamageFact();
          damageStageTurnOffDelayEvent = new VehicleDamageStageTurnOffEvent();
          GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), damageStageTurnOffDelayEvent, RandRangeF(35.00, 55.00));
        } else {
          if this.m_damageLevel == 3 {
            vehicleRecord = this.GetVehicle().GetRecord();
            this.UpdateDamageEngineEffects();
            destroyedAppearanceName = vehicleRecord.DestroyedAppearance();
            this.BroadcastEnvironmentalHazardStimuli();
            this.DisableVehicle();
            if IsNameValid(destroyedAppearanceName) {
              this.GetVehicle().PrefetchAppearanceChange(destroyedAppearanceName);
            };
            if IsDefined(this.m_mountedPlayer) {
              GameInstance.GetStatPoolsSystem(this.GetVehicle().GetGame()).RequestSettingStatPoolValueCustomLimit(Cast<StatsObjectID>(this.GetVehicle().GetEntityID()), gamedataStatPoolType.Health, 0.00, this.GetVehicle());
              this.m_immuneInDecay = true;
              GamepadLightScriptableSystem.TriggerVehicleExplosionWarningSiren(this.GetVehicle().GetGame());
              GameInstance.GetAudioSystem(this.GetVehicle().GetGame()).AddTriggerEffect(n"te_vehicle_car_disabled", n"VehicleDisabled");
              GameInstance.GetAudioSystem(this.GetVehicle().GetGame()).PlayOnEmitter(n"v_car_explosion_warning_start", this.GetVehicle().GetEntityID(), n"vehicle_engine_emitter");
              GameInstance.GetAudioSystem(this.GetVehicle().GetGame()).PlayOnEmitter(n"v_car_malfunction", this.GetVehicle().GetEntityID(), n"vehicle_engine_emitter");
            } else {
              this.GetPS().SetIsDestroyed(true);
            };
          };
        };
      };
      this.m_vehicleBlackboard.SetInt(GetAllBlackboardDefs().Vehicle.DamageState, this.m_damageLevel);
    };
  }

  public final func ForceAboutToExplodeState() -> Void {
    let vehicle: ref<VehicleObject> = this.GetVehicle();
    let statPoolSys: ref<StatPoolsSystem> = GameInstance.GetStatPoolsSystem(vehicle.GetGame());
    let vehicleEntityID: EntityID = vehicle.GetEntityID();
    let wantedTreshold: Float = this.m_healthDecayThreshold * 0.15;
    if statPoolSys.GetStatPoolValue(Cast<StatsObjectID>(vehicleEntityID), gamedataStatPoolType.Health) > wantedTreshold {
      statPoolSys.RequestSettingStatPoolValue(Cast<StatsObjectID>(vehicleEntityID), gamedataStatPoolType.Health, wantedTreshold, null);
    };
  }

  private final func UpdateDamageEngineEffects() -> Void {
    let vehicleRecord: wref<Vehicle_Record> = this.GetVehicle().GetRecord();
    this.BreakAllDamageStageFX();
    if this.m_damageLevel == 1 {
      if !vehicleRecord.UsesSecondaryHoodEmitter() || Equals(this.GetPS().GetDoorState(EVehicleDoor.hood), VehicleDoorState.Open) || Equals(this.GetPS().GetDoorState(EVehicleDoor.hood), VehicleDoorState.Detached) {
        GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"damage_engine_stage1", false);
      } else {
        GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"damage_engine_stage1b", false);
      };
    } else {
      if this.m_damageLevel == 2 {
        if !vehicleRecord.UsesSecondaryHoodEmitter() || Equals(this.GetPS().GetDoorState(EVehicleDoor.hood), VehicleDoorState.Open) || Equals(this.GetPS().GetDoorState(EVehicleDoor.hood), VehicleDoorState.Detached) {
          GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"damage_engine_stage2", false);
        } else {
          GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"damage_engine_stage2b", false);
        };
      } else {
        if this.m_damageLevel == 3 {
          GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"damage_engine_stage3", false);
        };
      };
    };
  }

  private final func DisableVehicle() -> Void {
    let delayedExitEvent: ref<VehicleExitDelayed>;
    let vehicleExitDelay: Float;
    if !this.GetPS().GetIsDestroyed() && !GameInstance.GetGodModeSystem(this.GetVehicle().GetGame()).HasGodMode(this.GetVehicle().GetEntityID(), gameGodModeType.Immortal) {
      this.ToggleVehicleSystems(false, false, true);
      this.DestroyMappin();
      this.ToggleInteraction(n"None", false);
      this.DisableRadio();
      if this.m_vehicleExitDelayId == GetInvalidDelayID() {
        delayedExitEvent = new VehicleExitDelayed();
        delayedExitEvent.isEmergencyExit = this.m_isBroadcastingHazardStims;
        vehicleExitDelay = delayedExitEvent.isEmergencyExit ? 1.00 : 2.00;
        this.m_vehicleExitDelayId = GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), delayedExitEvent, vehicleExitDelay);
      };
    };
  }

  private final func CancelVehicleExitDelayedEvent() -> Void {
    if this.m_vehicleExitDelayId != GetInvalidDelayID() {
      GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).CancelDelay(this.m_vehicleExitDelayId);
      this.m_vehicleExitDelayId = GetInvalidDelayID();
    };
  }

  protected cb func OnVehicleExitDelayedEvent(evt: ref<VehicleExitDelayed>) -> Bool {
    let exitVehicleEvent: ref<AIEvent>;
    if evt.isEmergencyExit {
      this.GetVehicle().ForceBrakesUntilStoppedOrFor(4.00);
    };
    exitVehicleEvent = new AIEvent();
    exitVehicleEvent.name = n"ExitVehicle";
    this.m_vehicleExitDelayId = GetInvalidDelayID();
    VehicleComponent.QueueEventToAllPassengers(this.GetVehicle().GetGame(), this.GetVehicle(), exitVehicleEvent);
  }

  private final func RepairVehicle() -> Void {
    this.RemoveVehicleDOT();
    GameInstance.GetStatPoolsSystem(this.GetVehicle().GetGame()).RequestSettingStatPoolMaxValue(Cast<StatsObjectID>(this.GetVehicle().GetEntityID()), gamedataStatPoolType.Health, this.GetVehicle());
    this.GetPS().SetIsDestroyed(false);
    this.CreateMappin();
    this.ToggleInteraction(n"None", true);
    this.ToggleVehicleSystems(true, true, true);
    this.GetPS().SetIsSubmerged(false);
    GameObjectEffectHelper.StopEffectEvent(this.GetVehicle(), n"underwater_bubbles");
  }

  protected cb func OnVehicleRepairEvent(re: ref<VehicleRepairEvent>) -> Bool {
    this.RepairVehicle();
  }

  private final func EnableRadio() -> Void {
    let radioEvent: ref<VehicleRadioEvent> = new VehicleRadioEvent();
    radioEvent.toggle = true;
    this.GetVehicle().QueueEvent(radioEvent);
  }

  private final func DisableRadio() -> Void {
    let radioEvent: ref<VehicleRadioEvent> = new VehicleRadioEvent();
    radioEvent.toggle = false;
    this.GetVehicle().QueueEvent(radioEvent);
  }

  private final func DestroyRandomWindow() -> Void {
    let destructionEvent: ref<VehicleGlassDestructionEvent>;
    let glassArray: array<wref<VehicleDestructibleGlass_Record>>;
    let vehicleRecord: ref<Vehicle_Record>;
    if VehicleComponent.GetVehicleRecord(this.GetVehicle(), vehicleRecord) && IsDefined(vehicleRecord.Destruction()) && vehicleRecord.Destruction().GetGlassCount() > 1 {
      vehicleRecord.Destruction().Glass(glassArray);
      destructionEvent = new VehicleGlassDestructionEvent();
      destructionEvent.glassName = glassArray[RandRange(1, ArraySize(glassArray))].Component();
      this.GetVehicle().QueueEvent(destructionEvent);
    };
  }

  private final func TutorialCarDamageFact() -> Void {
    let questSystem: ref<QuestsSystem> = GameInstance.GetQuestsSystem(this.GetVehicle().GetGame());
    if VehicleComponent.IsMountedToVehicle(this.GetVehicle().GetGame(), GameInstance.GetPlayerSystem(this.GetVehicle().GetGame()).GetLocalPlayerControlledGameObject()) && questSystem.GetFact(n"car_damage_tutorial") == 0 && questSystem.GetFact(n"disable_tutorials") == 0 {
      questSystem.SetFact(n"car_damage_tutorial", 1);
    };
  }

  private final func BreakAllDamageStageFX(opt auxillaryFX: Bool) -> Void {
    let vehicle: ref<GameObject> = this.GetVehicle();
    GameObjectEffectHelper.BreakEffectLoopEvent(vehicle, n"damage_engine_stage1");
    GameObjectEffectHelper.BreakEffectLoopEvent(vehicle, n"damage_engine_stage1b");
    GameObjectEffectHelper.BreakEffectLoopEvent(vehicle, n"damage_engine_stage2");
    GameObjectEffectHelper.BreakEffectLoopEvent(vehicle, n"damage_engine_stage2b");
    GameObjectEffectHelper.BreakEffectLoopEvent(vehicle, n"damage_engine_stage3");
    if auxillaryFX {
      GameObjectEffectHelper.BreakEffectLoopEvent(vehicle, n"cooler_destro_fx");
    };
  }

  private final func SendDestructionDataToGraph(gridID: Int32, gridState: Float) -> Void {
    if gridState >= 0.15 {
      this.PlayCrystalDomeGlitchEffect();
      this.m_vehicleBlackboard.SetBool(GetAllBlackboardDefs().Vehicle.Collision, true);
      this.m_vehicleBlackboard.SignalBool(GetAllBlackboardDefs().Vehicle.Collision);
    };
    if gridState >= 0.40 {
      if gridID == 6 {
        if gridState >= 0.80 {
          AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"wheel_f_l_destruction", 1.00);
        };
        AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"bumper_f_destruction_side_2", 1.00);
        AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"hood_destruction", 1.00);
        AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"destruction", 1.00);
      } else {
        if gridID == 7 {
          if gridState >= 0.80 {
            AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"wheel_f_r_destruction", 1.00);
          };
          AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"bumper_f_destruction", 1.00);
          AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"hood_destruction", 1.00);
          AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"destruction", 1.00);
        } else {
          if gridID == 5 {
            AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"door_f_r_destruction", 1.00);
            AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"destruction", 1.00);
          } else {
            if gridID == 3 {
            } else {
              if gridID == 1 {
                AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"bumper_b_destruction", 1.00);
                AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"trunk_destruction", 1.00);
                AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"destruction", 1.00);
              } else {
                if gridID == 0 {
                  AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"bumper_b_destruction_side_2", 1.00);
                  AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"trunk_destruction", 1.00);
                  AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"destruction", 1.00);
                } else {
                  if gridID == 2 {
                  } else {
                    if gridID == 4 {
                      AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"door_f_l_destruction", 1.00);
                      AnimationControllerComponent.SetInputFloat(this.GetVehicle(), n"destruction", 1.00);
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
  }

  private final func DetermineAdditionalEngineFX(gridID: Int32, gridState: Float) -> Void {
    if !this.m_coolerDestro {
      if gridID == 6 || gridID == 7 {
        if gridState >= 0.80 {
          GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"cooler_destro_fx", true);
          this.m_coolerDestro = true;
        };
      };
    };
  }

  public final func IsVehicleInDecay() -> Bool {
    return this.m_damageLevel == 3;
  }

  public final func IsVehicleImmuneInDecay() -> Bool {
    return this.m_immuneInDecay;
  }

  public final func GetVehicleDecayThreshold() -> Float {
    return this.m_healthDecayThreshold;
  }

  private final func EvaluateDamageLevel(destruction: Float) -> Int32 {
    if destruction <= 50.00 && destruction > 25.00 {
      this.m_damageLevel = 1;
    } else {
      if destruction <= 25.00 && destruction > this.m_healthDecayThreshold {
        this.m_damageLevel = 2;
      } else {
        if destruction <= this.m_healthDecayThreshold && destruction > 0.00 {
          this.m_damageLevel = 3;
        };
      };
    };
    if GameInstance.GetGodModeSystem(this.GetVehicle().GetGame()).HasGodMode(this.GetVehicle().GetEntityID(), gameGodModeType.Immortal) {
      if this.m_damageLevel > 2 {
        this.m_damageLevel = 2;
      };
    };
    return this.m_damageLevel;
  }

  protected cb func OnVehicleDamageStageTurnOffEvent(evt: ref<VehicleDamageStageTurnOffEvent>) -> Bool {
    if this.m_damageLevel == 2 {
      this.m_damageLevel = 1;
      this.UpdateDamageEngineEffects();
    };
  }

  private final func PlayCrystalDomeGlitchEffect() -> Void {
    if this.GetPS().GetCrystalDomeState() {
      GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"crystal_dome_fl_b", true);
      GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"crystal_dome_fl_f", true);
      GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"crystal_dome_fr_b", true);
      GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"crystal_dome_fr_f", true);
      GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"crystal_dome_ml", true);
      GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"crystal_dome_mr", true);
      GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"crystal_dome_ol", true);
      GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"crystal_dome_or", true);
    };
  }

  protected cb func OnVehicleOnPartDetached(evt: ref<VehicleOnPartDetachedEvent>) -> Bool {
    let partName: CName = evt.partName;
    if Equals(partName, n"Trunk") {
      this.GetPS().SetDoorState(EVehicleDoor.trunk, VehicleDoorState.Detached, false);
      this.SignalDamageToVehicleVisualCustomization();
    } else {
      if Equals(partName, n"Hood") {
        this.GetPS().SetDoorState(EVehicleDoor.hood, VehicleDoorState.Detached, false);
        this.SignalDamageToVehicleVisualCustomization();
      } else {
        if Equals(partName, n"HoodLeft") || Equals(partName, n"HoodRight") {
          this.SignalDamageToVehicleVisualCustomization();
        } else {
          if Equals(partName, n"DoorFrontLeft") || Equals(partName, n"DoorFrontLeft_A") || Equals(partName, n"DoorFrontLeft_B") || Equals(partName, n"DoorFrontLeft_C") {
            this.GetPS().SetDoorState(EVehicleDoor.seat_front_left, VehicleDoorState.Detached, false);
            this.SignalDamageToVehicleVisualCustomization();
          } else {
            if Equals(partName, n"DoorFrontRight") || Equals(partName, n"DoorFrontRight_A") || Equals(partName, n"DoorFrontRight_B") || Equals(partName, n"DoorFrontRight_C") {
              this.GetPS().SetDoorState(EVehicleDoor.seat_front_right, VehicleDoorState.Detached, false);
              this.SignalDamageToVehicleVisualCustomization();
            } else {
              if Equals(partName, n"DoorBackLeft") {
                this.GetPS().SetDoorState(EVehicleDoor.seat_back_left, VehicleDoorState.Detached, false);
                this.SignalDamageToVehicleVisualCustomization();
              } else {
                if Equals(partName, n"DoorBackRight") {
                  this.GetPS().SetDoorState(EVehicleDoor.seat_back_right, VehicleDoorState.Detached, false);
                  this.SignalDamageToVehicleVisualCustomization();
                } else {
                  if Equals(partName, n"BumperFront") {
                    this.SignalDamageToVehicleVisualCustomization();
                  } else {
                    if Equals(partName, n"BumperBack") {
                      this.SignalDamageToVehicleVisualCustomization();
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
  }

  protected cb func OnVehicleRadioStationInitialized(evt: ref<VehicleRadioStationInitialized>) -> Bool {
    this.m_radioState = true;
    this.m_vehicleBlackboard.SetBool(GetAllBlackboardDefs().Vehicle.VehRadioState, true);
    this.m_vehicleBlackboard.SetName(GetAllBlackboardDefs().Vehicle.VehRadioStationName, this.GetVehicle().GetRadioReceiverStationName());
  }

  protected cb func OnRadioToggleEvent(evt: ref<RadioToggleEvent>) -> Bool {
    let uiRadioEvent: ref<UIVehicleRadioEvent>;
    let vehicle: wref<VehicleObject> = this.GetVehicle();
    if vehicle.IsRadioReceiverActive() {
      this.GetVehicle().ToggleRadioReceiver(false);
      if this.m_radioState {
        this.m_radioState = false;
        this.m_vehicleBlackboard.SetBool(GetAllBlackboardDefs().Vehicle.VehRadioState, false);
      };
    } else {
      this.GetVehicle().ToggleRadioReceiver(true);
      if !this.m_radioState {
        this.m_radioState = true;
        this.m_vehicleBlackboard.SetBool(GetAllBlackboardDefs().Vehicle.VehRadioState, true);
      };
    };
    uiRadioEvent = new UIVehicleRadioEvent();
    GameInstance.GetUISystem(vehicle.GetGame()).QueueEvent(uiRadioEvent);
  }

  protected cb func OnVehicleRadioEvent(evt: ref<VehicleRadioEvent>) -> Bool {
    let uiRadioSongEvent: ref<VehicleRadioSongChanged>;
    let toggle: Bool = evt.toggle;
    let setStation: Bool = evt.setStation;
    let station: Int32 = evt.station;
    let uiSystem: ref<UISystem> = GameInstance.GetUISystem(this.GetVehicle().GetGame());
    let uiRadioEvent: ref<UIVehicleRadioEvent> = new UIVehicleRadioEvent();
    if toggle {
      if this.m_radioState {
        this.GetVehicle().NextRadioReceiverStation();
      } else {
        this.GetVehicle().ToggleRadioReceiver(true);
        this.m_radioState = true;
        this.m_vehicleBlackboard.SetBool(GetAllBlackboardDefs().Vehicle.VehRadioState, true);
        if !setStation {
          uiRadioSongEvent = new VehicleRadioSongChanged();
          uiSystem.QueueEvent(uiRadioSongEvent);
        };
      };
      this.m_vehicleBlackboard.SetName(GetAllBlackboardDefs().Vehicle.VehRadioStationName, this.GetVehicle().GetRadioReceiverStationName());
      uiSystem.QueueEvent(uiRadioEvent);
    } else {
      this.GetVehicle().ToggleRadioReceiver(false);
      if this.m_radioState {
        this.m_radioState = false;
        this.m_vehicleBlackboard.SetBool(GetAllBlackboardDefs().Vehicle.VehRadioState, false);
      };
    };
    if setStation {
      this.GetVehicle().SetRadioReceiverStation(Cast<Uint32>(station));
      this.m_radioState = true;
      this.m_vehicleBlackboard.SetBool(GetAllBlackboardDefs().Vehicle.VehRadioState, true);
      this.m_vehicleBlackboard.SetName(GetAllBlackboardDefs().Vehicle.VehRadioStationName, this.GetVehicle().GetRadioReceiverStationName());
      uiSystem.QueueEvent(uiRadioEvent);
      return false;
    };
  }

  protected cb func OnVehicleRadioTierEvent(evt: ref<VehicleRadioTierEvent>) -> Bool {
    this.GetVehicle().SetRadioTier(evt.radioTier, evt.overrideTier);
  }

  private final func SendParkEvent(park: Bool) -> Void {
    let parkEvent: ref<VehicleParkedEvent>;
    if this.GetVehicle() == (this.GetVehicle() as BikeObject) {
      parkEvent = new VehicleParkedEvent();
      parkEvent.park = park;
      this.GetVehicle().QueueEvent(parkEvent);
    };
  }

  protected cb func OnVehicleLightQuestToggleEvent(evt: ref<VehicleLightQuestToggleEvent>) -> Bool {
    let toggle: Bool = evt.toggle;
    let lightType: vehicleELightType = evt.lightType;
    let vehController: ref<vehicleController> = this.GetVehicleController();
    vehController.ToggleLights(toggle, lightType);
  }

  protected cb func OnVehicleLightQuestChangeColorEvent(evt: ref<VehicleLightQuestChangeColorEvent>) -> Bool {
    let vehController: ref<vehicleController> = this.GetVehicleController();
    vehController.SetLightColor(evt.lightType, evt.color, 0.00, evt.forceOverrideEmissiveColor);
  }

  protected cb func OnVehicleCycleHeadLightsEvent(evt: ref<VehicleCycleLightsEvent>) -> Bool {
    this.GetVehicleControllerPS().CycleHeadLightMode();
  }

  protected cb func OnVehicleQuestSirenEvent(evt: ref<VehicleQuestSirenEvent>) -> Bool {
    if this.m_useAuxiliary {
      this.ToggleSiren(this.GetPS().GetSirenLightsState(), this.GetPS().GetSirenSoundsState());
    };
  }

  protected cb func OnVehicleChaseTargetEvent(evt: ref<VehicleChaseTargetEvent>) -> Bool {
    if evt.inProgress {
      this.ToggleSiren(true, true);
    };
  }

  private final func CanShowMappin() -> Bool {
    let bb: ref<IBlackboard>;
    if this.GetVehicle().IsPrevention() && GameInstance.GetPreventionSpawnSystem(this.GetVehicle().GetGame()).IsPreventionVehicleEnabled() {
      return true;
    };
    bb = GameInstance.GetBlackboardSystem(this.GetVehicle().GetGame()).Get(GetAllBlackboardDefs().VehicleSummonData);
    if IsDefined(bb) {
      if Equals(vehicleGarageState.SummonAvailable, IntEnum<vehicleGarageState>(bb.GetUint(GetAllBlackboardDefs().VehicleSummonData.GarageState))) {
        return GameInstance.GetVehicleSystem(this.GetVehicle().GetGame()).IsVehiclePlayerUnlocked(this.GetVehicle().GetRecordID());
      };
    };
    return false;
  }

  private final func CreateMappin() -> Void {
    let isBike: Bool;
    let mappinData: MappinData;
    let system: ref<MappinSystem>;
    if this.CanShowMappin() {
      if this.m_mappinID.value == 0u {
        system = GameInstance.GetMappinSystem(this.GetVehicle().GetGame());
        isBike = this.GetVehicle() == (this.GetVehicle() as BikeObject);
        mappinData.mappinType = t"Mappins.QuestDynamicMappinDefinition";
        mappinData.variant = this.GetVehicle().IsPrevention() ? gamedataMappinVariant.Zzz04_PreventionVehicleVariant : isBike ? gamedataMappinVariant.Zzz03_MotorcycleVariant : gamedataMappinVariant.VehicleVariant;
        mappinData.active = true;
        this.m_mappinID = system.RegisterVehicleMappin(mappinData, this.GetVehicle(), n"vehMappin");
      };
    };
  }

  public final func DestroyMappin() -> Void {
    let system: ref<MappinSystem>;
    if this.m_mappinID.value != 0u {
      system = GameInstance.GetMappinSystem(this.GetVehicle().GetGame());
      system.UnregisterMappin(this.m_mappinID);
      this.m_mappinID.value = 0u;
    };
  }

  protected final func RequestHUDRefresh() -> Void {
    let request: ref<RefreshActorRequest> = new RefreshActorRequest();
    request.ownerID = this.GetVehicle().GetEntityID();
    this.GetVehicle().GetHudManager().QueueRequest(request);
  }

  protected final func SetupListeners() -> Void {
    this.SetupGameTimeToBBListener();
    this.SetupVehicleTPPBBListener();
    this.SetupVehicleSpeedBBListener();
    this.SetupVehicleRPMBBListener();
  }

  protected final func UnregisterListeners() -> Void {
    this.UnregisterGameTimeToBBListener();
    this.UnregisterVehicleTPPBBListener();
    this.UnregisterVehicleSpeedBBListener();
    this.UnregisterVehicleRPMBBListener();
  }

  protected final func SetupGameTimeToBBListener() -> Void {
    let delay: GameTime;
    let evt: ref<MinutePassedEvent>;
    if this.m_timeSystemCallbackID == 0u {
      evt = new MinutePassedEvent();
      delay = GameTime.MakeGameTime(0, 0, 1, 0);
      this.m_timeSystemCallbackID = GameInstance.GetTimeSystem(this.GetVehicle().GetGame()).RegisterDelayedListener(this.GetVehicle(), evt, delay, -1);
      this.PassGameTimeToVehBB();
    };
  }

  protected cb func OnMinutePassedEvent(evt: ref<MinutePassedEvent>) -> Bool {
    this.PassGameTimeToVehBB();
  }

  protected final func PassGameTimeToVehBB() -> Void {
    let timeString: String;
    let timeSys: ref<TimeSystem> = GameInstance.GetTimeSystem(this.GetVehicle().GetGame());
    let currTime: GameTime = timeSys.GetGameTime();
    let hours: Int32 = GameTime.Hours(currTime);
    if IsDefined(this.GetVehicle() as ncartMetroObject) {
    } else {
      if hours > 12 {
        hours = hours - 12;
      };
    };
    timeString = StrReplace(SpaceFill(IntToString(hours), 2, ESpaceFillMode.JustifyRight), " ", "0") + ":" + StrReplace(SpaceFill(IntToString(GameTime.Minutes(currTime)), 2, ESpaceFillMode.JustifyRight), " ", "0");
    this.m_vehicleBlackboard.SetString(GetAllBlackboardDefs().Vehicle.GameTime, timeString);
  }

  protected final func UnregisterGameTimeToBBListener() -> Void {
    if this.m_timeSystemCallbackID > 0u {
      GameInstance.GetTimeSystem(this.GetVehicle().GetGame()).UnregisterListener(this.m_timeSystemCallbackID);
      this.m_timeSystemCallbackID = 0u;
    };
  }

  protected final func SetupVehicleTPPBBListener() -> Void {
    let activeVehicleUIBlackboard: wref<IBlackboard>;
    let bbSys: ref<BlackboardSystem>;
    if !IsDefined(this.m_vehicleTPPCallbackID) {
      bbSys = GameInstance.GetBlackboardSystem(this.GetVehicle().GetGame());
      activeVehicleUIBlackboard = bbSys.Get(GetAllBlackboardDefs().UI_ActiveVehicleData);
      this.m_vehicleTPPCallbackID = activeVehicleUIBlackboard.RegisterListenerBool(GetAllBlackboardDefs().UI_ActiveVehicleData.IsTPPCameraOn, this, n"OnVehicleCameraChange");
    };
  }

  protected final func UnregisterVehicleTPPBBListener() -> Void {
    let activeVehicleUIBlackboard: wref<IBlackboard>;
    let bbSys: ref<BlackboardSystem>;
    if IsDefined(this.m_vehicleTPPCallbackID) {
      bbSys = GameInstance.GetBlackboardSystem(this.GetVehicle().GetGame());
      activeVehicleUIBlackboard = bbSys.Get(GetAllBlackboardDefs().UI_ActiveVehicleData);
      activeVehicleUIBlackboard.UnregisterListenerBool(GetAllBlackboardDefs().UI_ActiveVehicleData.IsTPPCameraOn, this.m_vehicleTPPCallbackID);
    };
  }

  protected final func OnVehicleCameraChange(state: Bool) -> Void {
    let animFeature: ref<AnimFeature_VehicleState>;
    if this.GetPS().GetCrystalDomeState() {
      animFeature = new AnimFeature_VehicleState();
      animFeature.tppEnabled = !state;
      AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), n"VehicleState", animFeature);
      this.TogglePanzerShadowMeshes(state);
    };
  }

  protected final func SetupCarAlarmHonkListener() -> Void {
    let vehicleDefBlackboard: wref<IBlackboard>;
    if !IsDefined(this.m_carAlarmCallbackID) {
      vehicleDefBlackboard = this.GetVehicle().GetBlackboard();
      this.m_carAlarmCallbackID = vehicleDefBlackboard.RegisterListenerBool(GetAllBlackboardDefs().Vehicle.UseCarAlarmStim, this, n"OnCarAlarmHonk");
    };
  }

  protected final func UnregisterCarAlarmHonkListener() -> Void {
    let vehicleDefBlackboard: wref<IBlackboard>;
    if IsDefined(this.m_carAlarmCallbackID) {
      vehicleDefBlackboard = this.GetVehicle().GetBlackboard();
      vehicleDefBlackboard.UnregisterListenerBool(GetAllBlackboardDefs().Vehicle.UseCarAlarmStim, this.m_carAlarmCallbackID);
    };
  }

  protected final func SetupVehicleSpeedBBListener() -> Void {
    let vehicleDefBlackboard: wref<IBlackboard>;
    if !IsDefined(this.m_vehicleSpeedCallbackID) {
      vehicleDefBlackboard = this.GetVehicle().GetBlackboard();
      this.m_vehicleSpeedCallbackID = vehicleDefBlackboard.RegisterListenerFloat(GetAllBlackboardDefs().Vehicle.SpeedValue, this, n"OnVehicleSpeedChange");
    };
  }

  protected final func UnregisterVehicleSpeedBBListener() -> Void {
    let vehicleDefBlackboard: wref<IBlackboard>;
    if IsDefined(this.m_vehicleSpeedCallbackID) {
      vehicleDefBlackboard = this.GetVehicle().GetBlackboard();
      vehicleDefBlackboard.UnregisterListenerFloat(GetAllBlackboardDefs().Vehicle.SpeedValue, this.m_vehicleSpeedCallbackID);
    };
  }

  protected final func SetupVehicleRPMBBListener() -> Void {
    let vehicleDefBlackboard: wref<IBlackboard>;
    if !IsDefined(this.m_vehicleRPMCallbackID) {
      vehicleDefBlackboard = this.GetVehicle().GetBlackboard();
      this.m_vehicleRPMCallbackID = vehicleDefBlackboard.RegisterListenerFloat(GetAllBlackboardDefs().Vehicle.RPMValue, this, n"OnVehicleRPMChange");
    };
  }

  protected final func UnregisterVehicleRPMBBListener() -> Void {
    let vehicleDefBlackboard: wref<IBlackboard>;
    if IsDefined(this.m_vehicleRPMCallbackID) {
      vehicleDefBlackboard = this.GetVehicle().GetBlackboard();
      vehicleDefBlackboard.UnregisterListenerFloat(GetAllBlackboardDefs().Vehicle.RPMValue, this.m_vehicleRPMCallbackID);
    };
  }

  protected cb func OnCarAlarmHonk(UseCarAlarmStim: Bool) -> Bool {
    if UseCarAlarmStim {
      this.GetVehicle().GetStimBroadcasterComponent().TriggerSingleBroadcast(this.GetVehicle(), gamedataStimType.CarAlarm);
    } else {
      this.GetVehicle().GetStimBroadcasterComponent().TriggerSingleBroadcast(this.GetVehicle(), gamedataStimType.VehicleHorn);
    };
  }

  protected final func OnVehicleSpeedChange(speed: Float) -> Void {
    let animFeature: ref<AnimFeature_PartData>;
    let doors: array<CName>;
    let vehDataPackage: wref<VehicleDataPackage_Record>;
    if this.m_hasSpoiler {
      if !this.m_spoilerDeployed {
        if speed >= this.m_spoilerUp {
          animFeature = new AnimFeature_PartData();
          animFeature.state = 1;
          animFeature.duration = 0.75;
          AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), n"spoiler", animFeature);
          this.m_spoilerDeployed = true;
        };
      } else {
        if speed <= this.m_spoilerDown {
          animFeature = new AnimFeature_PartData();
          animFeature.state = 3;
          animFeature.duration = 0.75;
          AnimationControllerComponent.ApplyFeatureToReplicate(this.GetVehicle(), n"spoiler", animFeature);
          this.m_spoilerDeployed = false;
        };
      };
    };
    if this.GetPS().GetHasAnyDoorOpen() {
      if this.m_ignoreAutoDoorClose {
        return;
      };
      VehicleComponent.GetVehicleDataPackage(this.GetVehicle().GetGame(), this.GetVehicle(), vehDataPackage);
      if speed < 0.00 {
        speed = AbsF(speed);
      };
      if speed < 0.50 {
        return;
      };
      if speed >= vehDataPackage.SpeedToClose() {
        ArrayPush(doors, n"seat_front_left");
        ArrayPush(doors, n"seat_front_right");
        ArrayPush(doors, n"hood");
        if !vehDataPackage.SlidingRearDoors() {
          ArrayPush(doors, n"seat_back_left");
          ArrayPush(doors, n"seat_back_right");
        };
        if !vehDataPackage.BarnDoorsTailgate() {
          ArrayPush(doors, n"trunk");
        };
        this.CloseSelectedDoors(doors);
        ArrayClear(doors);
      };
    };
  }

  private final func CloseSelectedDoors(const doors: script_ref<[CName]>) -> Void {
    let PSVehicleDoorCloseRequest: ref<VehicleDoorClose>;
    let size: Int32 = ArraySize(Deref(doors));
    let i: Int32 = 0;
    while i < size {
      PSVehicleDoorCloseRequest = new VehicleDoorClose();
      PSVehicleDoorCloseRequest.slotID = Deref(doors)[i];
      GameInstance.GetPersistencySystem(this.GetVehicle().GetGame()).QueuePSEvent(this.GetPS().GetID(), this.GetPS().GetClassName(), PSVehicleDoorCloseRequest);
      i += 1;
    };
    this.GetPS().SetHasAnyDoorOpen(false);
  }

  protected final func OnVehicleRPMChange(rpm: Float) -> Void {
    let value: Float;
    if rpm >= 2500.00 {
      if !this.m_overheatActive {
        if !IsDefined(this.m_overheatEffectBlackboard) {
          this.m_overheatEffectBlackboard = new worldEffectBlackboard();
          this.m_overheatEffectBlackboard.SetValue(n"overheatValue", 1.00);
        };
        GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"overheating", false, this.m_overheatEffectBlackboard);
        this.m_overheatActive = true;
      };
      value = (7100.00 - rpm) / 7100.00;
      this.m_overheatEffectBlackboard.SetValue(n"overheatValue", value);
    } else {
      if this.m_overheatActive {
        GameObjectEffectHelper.BreakEffectLoopEvent(this.GetVehicle(), n"overheating");
        this.m_overheatActive = false;
      };
    };
  }

  protected final func StartEffectEvent(self: ref<GameObject>, effectName: CName, opt shouldPersist: Bool, opt blackboard: ref<worldEffectBlackboard>) -> Void {
    let evt: ref<entSpawnEffectEvent>;
    if !IsNameValid(effectName) {
      return;
    };
    evt = new entSpawnEffectEvent();
    evt.effectName = effectName;
    evt.persistOnDetach = shouldPersist;
    evt.blackboard = blackboard;
    this.GetVehicle().QueueEvent(evt);
  }

  private final func ShouldCycle() -> Bool {
    let settings: ref<UserSettings> = GameInstance.GetSettingsSystem(this.GetVehicle().GetGame());
    let settingsGroup: ref<ConfigGroup> = settings.GetGroup(n"/gameplay/radioport");
    return (settingsGroup.GetVar(n"radioport_enable_cycling") as ConfigVarBool).GetValue();
  }

  protected cb func OnAction(action: ListenerAction, consumer: ListenerActionConsumer) -> Bool {
    let cycleEvent: ref<UIVehicleRadioCycleEvent>;
    let radioEvent: ref<VehicleRadioEvent>;
    let releaseTime: Float;
    let sirenState: Bool;
    let toggleEvent: ref<RadioToggleEvent>;
    let vehicle: ref<VehicleObject>;
    if this.GetPS().GetIsDestroyed() {
      return false;
    };
    vehicle = this.GetVehicle();
    if !IsDefined(vehicle) {
      return false;
    };
    if Equals(ListenerAction.GetName(action), n"VehicleInsideWheel") {
      if !StatusEffectSystem.ObjectHasStatusEffectWithTag(vehicle, n"VehicleBlockRadioInput") && this.IsRadioEnabled(vehicle.GetGame()) {
        if ListenerAction.IsButtonJustPressed(action) {
          this.m_radioPressTime = EngineTime.ToFloat(GameInstance.GetEngineTime(vehicle.GetGame()));
        };
        if ListenerAction.IsButtonJustReleased(action) {
          releaseTime = EngineTime.ToFloat(GameInstance.GetEngineTime(vehicle.GetGame()));
          if releaseTime <= this.m_radioPressTime + 0.20 {
            if this.ShouldCycle() {
              radioEvent = new VehicleRadioEvent();
              radioEvent.toggle = true;
              vehicle.QueueEvent(radioEvent);
              if vehicle.IsRadioReceiverActive() {
                cycleEvent = new UIVehicleRadioCycleEvent();
                GameInstance.GetUISystem(vehicle.GetGame()).QueueEvent(cycleEvent);
              };
              if IsDefined(this.m_mountedPlayer) {
                this.m_mountedPlayer.QueueEvent(radioEvent);
              };
            } else {
              toggleEvent = new RadioToggleEvent();
              vehicle.QueueEvent(toggleEvent);
              if IsDefined(this.m_mountedPlayer) {
                this.m_mountedPlayer.QueueEvent(toggleEvent);
              };
            };
          };
        };
      };
    };
    if (Equals(ListenerAction.GetName(action), n"VehicleHornHold") && Equals(ListenerAction.GetType(action), gameinputActionType.BUTTON_HOLD_COMPLETE) || Equals(ListenerAction.GetName(action), n"VehicleHorn") && ListenerAction.IsButtonJustPressed(action)) && !this.m_hornOn {
      this.ToggleVehicleHorn(true);
      vehicle.GetStimBroadcasterComponent().TriggerSingleBroadcast(vehicle, gamedataStimType.VehicleHorn);
      this.m_hornOn = true;
    };
    if (Equals(ListenerAction.GetName(action), n"VehicleHornHold") || Equals(ListenerAction.GetName(action), n"VehicleHorn")) && this.m_hornOn && ListenerAction.IsButtonJustReleased(action) {
      this.ToggleVehicleHorn(false);
      this.m_hornOn = false;
    };
    if this.m_useAuxiliary && Equals(ListenerAction.GetName(action), n"VehicleSiren") {
      if ListenerAction.IsButtonJustPressed(action) {
        this.m_sirenPressTime = EngineTime.ToFloat(GameInstance.GetEngineTime(vehicle.GetGame()));
      };
      if ListenerAction.IsButtonJustReleased(action) && EngineTime.ToFloat(GameInstance.GetEngineTime(this.GetVehicle().GetGame())) - this.m_sirenPressTime < 0.25 {
        sirenState = this.GetPS().GetSirenState();
        this.ToggleSiren(!sirenState, !sirenState);
      };
    };
  }

  protected final const func IsRadioEnabled(game: GameInstance) -> Bool {
    let blackboardSystem: ref<BlackboardSystem> = GameInstance.GetBlackboardSystem(game);
    let blackboard: ref<IBlackboard> = blackboardSystem.Get(GetAllBlackboardDefs().UIGameData);
    return blackboard.GetBool(GetAllBlackboardDefs().UIGameData.Popup_Radio_Enabled);
  }

  protected cb func OnVehicleQuestHornEvent(evt: ref<VehicleQuestHornEvent>) -> Bool {
    let delayTimer: Float;
    let hornOffDelayEvent: ref<VehicleHornOffDelayEvent>;
    this.ToggleVehicleHorn(true);
    this.m_hornOn = true;
    delayTimer = evt.honkTime;
    if delayTimer == 0.00 {
      delayTimer = 4.00;
    };
    hornOffDelayEvent = new VehicleHornOffDelayEvent();
    GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), hornOffDelayEvent, delayTimer);
  }

  protected cb func OnVehicleDelayedQuestHornEvent(evt: ref<VehicleQuestDelayedHornEvent>) -> Bool {
    let delayTimer: Float = evt.delayTime;
    let honkEvent: ref<VehicleQuestHornEvent> = new VehicleQuestHornEvent();
    honkEvent.honkTime = evt.honkTime;
    GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), honkEvent, delayTimer);
  }

  protected cb func OnVehicleHornProbEvent(evt: ref<VehicleHornProbsEvent>) -> Bool {
    let delayTimer: Float;
    let hornOffDelayEvent: ref<VehicleHornOffDelayEvent>;
    let vehicleObject: wref<VehicleObject>;
    let randomDraw: Float = RandRangeF(0.00, 1.00);
    if randomDraw <= evt.probability {
      vehicleObject = this.GetVehicle();
      this.ToggleVehicleHorn(true);
      this.m_hornOn = true;
      delayTimer = RandRangeF(evt.honkMinTime, evt.honkMaxTime);
      if delayTimer == 0.00 {
        delayTimer = 3.00;
      };
      hornOffDelayEvent = new VehicleHornOffDelayEvent();
      GameInstance.GetDelaySystem(vehicleObject.GetGame()).DelayEvent(vehicleObject, hornOffDelayEvent, delayTimer);
    };
  }

  protected cb func OnVehicleHornOffDelayEvent(evt: ref<VehicleHornOffDelayEvent>) -> Bool {
    this.ToggleVehicleHorn(false);
    this.m_hornOn = false;
  }

  private final func ToggleVehicleHorn(state: Bool, opt isPolice: Bool) -> Void {
    this.GetVehicle().ToggleHorn(state, isPolice);
  }

  public final func ToggleLightsAndSirens(lights: Bool, sirens: Bool) -> Void {
    this.ToggleSiren(lights, sirens);
  }

  protected final func ToggleSiren(lights: Bool, sounds: Bool) -> Void {
    if this.m_useAuxiliary && (!this.GetPS().GetIsDestroyed() || !lights || !sounds) {
      if NotEquals(this.GetPS().GetSirenLightsState(), lights) {
        this.GetVehicleController().ToggleLights(lights, vehicleELightType.Utility);
      };
      if lights {
        this.StartEffectEvent(this.GetVehicle(), n"police_sign_combat", true);
      } else {
        this.StartEffectEvent(this.GetVehicle(), n"police_sign_default", true);
      };
      this.GetPS().SetSirenLightsState(lights);
      this.GetVehicle().ToggleSiren(sounds);
      this.GetPS().SetSirenSoundsState(sounds);
      if lights || sounds {
        this.GetPS().SetSirenState(true);
      } else {
        this.GetPS().SetSirenState(false);
      };
    };
  }

  protected cb func OnVehicleSirenDelayEvent(evt: ref<VehicleSirenDelayEvent>) -> Bool {
    this.ToggleSiren(evt.lights, evt.sounds);
  }

  private final func QueueLethalVehicleImpactToAllNonFriendlyAggressivePassengers(sourceName: CName) -> Bool {
    let i: Int32;
    let passenger: wref<ScriptedPuppet>;
    let passengers: array<wref<GameObject>>;
    let gi: GameInstance = this.GetVehicle().GetGame();
    let vehicleID: EntityID = this.GetVehicle().GetEntityID();
    let success: Bool = false;
    VehicleComponent.GetAllPassengers(gi, vehicleID, false, passengers);
    i = 0;
    while i < ArraySize(passengers) {
      passenger = passengers[i] as ScriptedPuppet;
      if IsDefined(passenger) && !IsFriendlyTowardsPlayer(passenger) && passenger.IsAggressive() {
        this.QueueVehicleImpactLethalHitEvent(passenger, passenger, sourceName);
        success = true;
      };
      i += 1;
    };
    return success;
  }

  protected cb func OnVehicleFlippedOverEvent(evt: ref<VehicleFlippedOverEvent>) -> Bool {
    if evt.isFlippedOver {
      if this.QueueLethalVehicleImpactToAllNonFriendlyAggressivePassengers(n"OnVehicleFlippedOverEvent") {
        this.CheckAboutToExplodeStateOnFlip();
      };
    } else {
      this.RemoveVehicleDOT();
    };
  }

  public final func GetIsVehicleVisualCustomizationEnabled() -> Bool {
    let recordID: TweakDBID = this.GetVehicle().GetRecordID();
    let record: ref<Vehicle_Record> = TweakDBInterface.GetVehicleRecord(recordID);
    let globalEnablingFact: Int32 = GameInstance.GetQuestsSystem(this.GetVehicle().GetGame()).GetFact(n"vvc_visual_customization_unlocked");
    if GameInstance.GetQuestsSystem(this.GetVehicle().GetGame()).GetFact(n"vvc_visual_customization_for_all_vehicles") > 0 {
      return true;
    };
    if Equals(record.HasVisualCustomization(), true) && globalEnablingFact > 0 {
      return true;
    };
    return false;
  }

  public final func GetIsVehicleVisualCustomizationTeaser() -> Bool {
    let recordID: TweakDBID = this.GetVehicle().GetRecordID();
    let record: ref<Vehicle_Record> = TweakDBInterface.GetVehicleRecord(recordID);
    return record.VisualCustomizationTeaser();
  }

  private final func PlayVehicleVisialCustomisationCollisionGlitch() -> Void {
    let template: VehicleVisualCustomizationTemplate = this.GetVehicle().GetVehiclePS().GetVehicleVisualCustomizationTemplate();
    if this.GetVehicle().GetVehicleComponent().GetIsVehicleVisualCustomizationEnabled() && !this.GetPS().GetIsVehicleVisualCustomizationBlockedByDamage() && this.GetVehicle().GetVehiclePS().GetIsVehicleVisualCustomizationActive() && VehicleVisualCustomizationTemplate.IsValid(template) && !VehicleVisualCustomizationTemplate.IsLightsOnly(template) {
      GameObjectEffectHelper.StartEffectEvent(this.GetVehicle(), n"vvc_damage_glitch");
    };
  }

  private final func SignalDamageToVehicleVisualCustomization() -> Void {
    this.DisableCurrentCustomization();
    if this.GetVehicle().GetVehicleComponent().GetIsVehicleVisualCustomizationEnabled() && this.GetVehicle().IsPlayerMounted() && !this.GetVehicle().GetVehicleComponent().GetIsVehicleVisualCustomizationTeaser() && !this.GetPS().GetIsVehicleVisualCustomizationBlockedByDamage() {
      StatusEffectHelper.ApplyStatusEffect(this.m_mountedPlayer, t"BaseStatusEffect.VehicleVisualModCooldown");
      this.VisualCustomizationBlockedNotification(GetLocalizedText("LocKey#96051"), SimpleMessageType.Negative);
    };
    this.GetPS().SetVehicleVisualCustomizationnBlockedByDamage(true);
  }

  public final func EnableCustomizableAppearance(val: Bool) -> Void {
    let currentAppName: CName;
    let customAppName: CName;
    let defaultAppName: CName;
    let record: ref<Vehicle_Record>;
    let recordID: TweakDBID;
    if !this.GetIsVehicleVisualCustomizationEnabled() {
      return;
    };
    recordID = this.GetVehicle().GetRecordID();
    record = TweakDBInterface.GetVehicleRecord(recordID);
    defaultAppName = record.AppearanceName();
    customAppName = record.CustomizableAppearance();
    currentAppName = this.GetVehicle().GetCurrentAppearanceName();
    if val {
      if NotEquals(currentAppName, customAppName) {
        if IsNameValid(customAppName) {
          this.GetVehicle().ScheduleAppearanceChange(customAppName);
        };
      };
    } else {
      if NotEquals(currentAppName, defaultAppName) {
        if IsNameValid(defaultAppName) {
          this.GetVehicle().ScheduleAppearanceChange(defaultAppName);
        };
      };
    };
  }

  protected cb func OnExecuteVehicleVisualCustomizationEvent(evt: ref<ExecuteVehicleVisualCustomizationEvent>) -> Bool {
    if evt.set && !this.CanApplyTemplateOnVehicle(this.GetPS().GetVehicleVisualCustomizationTemplate(), true) {
      return false;
    };
    this.ExecuteVehicleVisualCustomization(evt.set, evt.reset, evt.instant);
  }

  private final func ExecuteVehicleVisualCustomization(set: Bool, reset: Bool, instant: Bool) -> Void {
    let customizationDelay: Float;
    let customizationEvent: ref<NewVehicleVisualCustomizationTemplateEvent>;
    let newTemplate: VehicleVisualCustomizationTemplate;
    if reset {
      this.GetPS().SetVehicleVisualCustomizationActive(false);
      this.DisableCurrentCustomization();
      customizationDelay = 0.50;
    } else {
      this.GetPS().SetVehicleVisualCustomizationActive(set);
      this.DisableCurrentCustomization();
      customizationDelay = 0.50;
      newTemplate = this.GetPS().GetVehicleVisualCustomizationTemplate();
      if set && VehicleVisualCustomizationTemplate.IsValid(newTemplate) && !VehicleVisualCustomizationTemplate.IsLightsOnly(newTemplate) {
        customizationEvent = new NewVehicleVisualCustomizationTemplateEvent();
        customizationEvent.template = newTemplate;
        customizationEvent.isInstant = instant;
        GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), customizationEvent, 0.60);
        customizationDelay += 1.10;
      };
    };
    GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).DelayEvent(this.GetVehicle(), new VehicleCustomizationLightsEvent(), customizationDelay);
    StatusEffectHelper.ApplyStatusEffect(GetPlayer(this.GetVehicle().GetGame()), t"BaseStatusEffect.VehicleVisualModCooldown");
  }

  public final func DisableCurrentCustomization() -> Void {
    let vehicleCustomizationComponent: ref<VehicleCustomizationComponent> = this.GetVehicle().GetCustomizationComponent();
    vehicleCustomizationComponent.HideUniqueCustomizationDecalsWithDisolve();
    vehicleCustomizationComponent.SetDefaultDecalsEnabled(true);
    this.PlayVehicleVisualCustomizationShader(false);
  }

  protected final func PlayVehicleVisualCustomizationShader(play: Bool, opt instant: Bool, opt blackboard: ref<worldEffectBlackboard>, opt affectRims: Bool) -> Void {
    let vehicle: ref<VehicleObject> = this.GetVehicle();
    if play {
      if instant {
        GameObjectEffectHelper.StartEffectEvent(vehicle, n"vvc_color_instant", true, blackboard, false);
        if affectRims {
          GameObjectEffectHelper.StartEffectEvent(vehicle, n"vvc_color_rims_instant", true, blackboard, false);
        };
      } else {
        GameObjectEffectHelper.StartEffectEvent(vehicle, n"vvc_color", true, blackboard, false);
        if affectRims {
          GameObjectEffectHelper.StartEffectEvent(vehicle, n"vvc_color_rims", true, blackboard, false);
        };
      };
    } else {
      GameObjectEffectHelper.BreakEffectLoopEvent(vehicle, n"vvc_color");
      GameObjectEffectHelper.BreakEffectLoopEvent(vehicle, n"vvc_color_instant");
      GameObjectEffectHelper.BreakEffectLoopEvent(vehicle, n"vvc_color_rims");
      GameObjectEffectHelper.BreakEffectLoopEvent(vehicle, n"vvc_color_rims_instant");
    };
  }

  protected cb func OnNewVehicleVisualCustomization(evt: ref<NewVehicleVisualCustomizationTemplateEvent>) -> Bool {
    if !this.CanApplyTemplateOnVehicle(evt.template, true) {
      return false;
    };
    this.ApplyVehicleVisualCustomizationTemplate(evt.template, evt.isInstant);
  }

  protected final func ApplyVehicleVisualCustomizationTemplate(template: VehicleVisualCustomizationTemplate, isInstant: Bool) -> Void {
    let packedColorA: Float;
    let packedColorB: Float;
    let record: ref<Vehicle_Record> = TweakDBInterface.GetVehicleRecord(this.GetVehicle().GetRecordID());
    this.m_customizationComponent.SetDefaultDecalsEnabled(false);
    if Equals(VehicleVisualCustomizationTemplate.GetType(template), VehicleVisualCustomizationType.Generic) {
      packedColorA = this.PackColorForEffect(GenericTemplatePersistentData.GetPrimaryColor(template.genericData));
      packedColorB = this.PackColorForEffect(GenericTemplatePersistentData.GetSecondaryColor(template.genericData));
      this.effectBlackboard = new worldEffectBlackboard();
      this.effectBlackboard.SetValueUnclamped(n"colorA", packedColorA);
      this.effectBlackboard.SetValueUnclamped(n"colorB", packedColorB);
      this.m_customizationComponent.SetupGenericCustomization(record.ExcludedComponentsGeneric(), record.GenericCustomizationMask());
      this.PlayVehicleVisualCustomizationShader(true, isInstant, this.effectBlackboard, record.CustomizeCarRims());
    } else {
      this.m_customizationComponent.SetupUniqueCustomization(template.uniqueData.customMultilayers, template.uniqueData.globalClearCoatOverrides, template.uniqueData.partsClearCoatOverrides);
      this.m_customizationComponent.ShowUniqueCustomizationDecalsWithDisolve(template.uniqueData.customDecals);
      this.PlayVehicleVisualCustomizationShader(true, isInstant, null, false);
    };
  }

  public final func PackColorForEffect(color: Color) -> Float {
    let rNormalized: Float = 1.00 - RoundTo(Cast<Float>(color.Red) / 255.00, 2);
    let gNormalized: Float = 1.00 - RoundTo(Cast<Float>(color.Green) / 255.00, 2);
    let bNormalized: Float = 1.00 - RoundTo(Cast<Float>(color.Blue) / 255.00, 2);
    rNormalized = ClampF(rNormalized, 0.01, 0.99);
    gNormalized = ClampF(gNormalized, 0.01, 0.99);
    bNormalized = ClampF(bNormalized, 0.01, 0.99);
    let packedValue: Float = rNormalized + gNormalized / 100.00 + bNormalized / 10000.00;
    return packedValue;
  }

  public final func ProcessHeatLevelOnVisualCustomization(previousTemplate: VehicleVisualCustomizationTemplate, newTemplate: VehicleVisualCustomizationTemplate) -> Void {
    let isNewProfileDifferentEnough: Bool;
    let preventionSystem: ref<PreventionSystem> = GameInstance.GetScriptableSystemsContainer(this.GetVehicle().GetGame()).Get(n"PreventionSystem") as PreventionSystem;
    if !preventionSystem.IsChasingPlayer() {
      return;
    };
    if preventionSystem.GetHeatStageAsInt() > 4u || Equals(preventionSystem.GetStarState(), EStarState.Active) {
      return;
    };
    if VehicleVisualCustomizationTemplate.Equals(previousTemplate, newTemplate) || !VehicleVisualCustomizationTemplate.IsValid(previousTemplate) && !VehicleVisualCustomizationTemplate.IsValid(newTemplate) {
      return;
    };
    if NotEquals(VehicleVisualCustomizationTemplate.GetType(previousTemplate), VehicleVisualCustomizationTemplate.GetType(newTemplate)) {
      isNewProfileDifferentEnough = true;
    } else {
      if !VehicleVisualCustomizationTemplate.IsValid(previousTemplate) ^ !VehicleVisualCustomizationTemplate.IsValid(newTemplate) {
        isNewProfileDifferentEnough = true;
      } else {
        if Equals(VehicleVisualCustomizationTemplate.GetType(previousTemplate), VehicleVisualCustomizationType.Generic) {
          isNewProfileDifferentEnough = this.IsColorChangeLargeEnoughForPolice(GenericTemplatePersistentData.GetPrimaryColor(previousTemplate.genericData), GenericTemplatePersistentData.GetPrimaryColor(newTemplate.genericData)) || this.IsColorChangeLargeEnoughForPolice(GenericTemplatePersistentData.GetSecondaryColor(previousTemplate.genericData), GenericTemplatePersistentData.GetSecondaryColor(newTemplate.genericData));
        } else {
          if Equals(VehicleVisualCustomizationTemplate.GetType(previousTemplate), VehicleVisualCustomizationType.Unique) {
            isNewProfileDifferentEnough = true;
          };
        };
      };
    };
    if !isNewProfileDifferentEnough {
      return;
    };
    preventionSystem.QueueRequest(new PreventionSearchingStatusRequest());
    this.SendPoliceNotification();
  }

  private final func IsColorChangeLargeEnoughForPolice(colorA: Color, colorB: Color) -> Bool {
    let squaredDifference: Int32 = (Cast<Int32>(colorB.Red) - Cast<Int32>(colorA.Red)) * (Cast<Int32>(colorB.Red) - Cast<Int32>(colorA.Red));
    squaredDifference += (Cast<Int32>(colorB.Green) - Cast<Int32>(colorA.Green)) * (Cast<Int32>(colorB.Green) - Cast<Int32>(colorA.Green));
    squaredDifference += (Cast<Int32>(colorB.Blue) - Cast<Int32>(colorA.Blue)) * (Cast<Int32>(colorB.Blue) - Cast<Int32>(colorA.Blue));
    return squaredDifference >= Cast<Int32>(SqrF(76.50));
  }

  private final func SendPoliceNotification() -> Void {
    let warningMsg: SimpleScreenMessage;
    warningMsg.isShown = true;
    warningMsg.message = LocKeyToString(n"UI-Scanner-Twintone-PoliceSystemReduction");
    warningMsg.type = SimpleMessageType.Police;
    GameInstance.GetBlackboardSystem(this.m_mountedPlayer.GetGame()).Get(GetAllBlackboardDefs().UI_Notifications).SetVariant(GetAllBlackboardDefs().UI_Notifications.WarningMessage, ToVariant(warningMsg), true);
  }

  public final func VisualCustomizationBlockedNotification(message: String, opt messageType: SimpleMessageType) -> Void {
    let warningMsg: SimpleScreenMessage;
    warningMsg.isShown = true;
    warningMsg.duration = 5.00;
    warningMsg.message = message;
    if NotEquals(messageType, SimpleMessageType.Undefined) {
      warningMsg.type = messageType;
    };
    GameInstance.GetBlackboardSystem(this.GetVehicle().GetGame()).Get(GetAllBlackboardDefs().UI_Notifications).SetVariant(GetAllBlackboardDefs().UI_Notifications.WarningMessage, ToVariant(warningMsg), true);
  }

  public final func GetCurrentAppearanceColorTemplate() -> VehicleVisualCustomizationTemplate {
    let appearanceMatch: Bool;
    let colorVariantMatch: Bool;
    let currentAppearance: CName;
    let currentColorVariant: CName;
    let i: Int32;
    let templateRecord: wref<VehicleColorTemplate_Record>;
    let vehicleColorTemplate: VehicleVisualCustomizationTemplate;
    let colorTemplateRecords: wref<VehicleAppearancesToColorTemplate_Record> = this.GetVehicle().GetRecord().AppearancesToColorTemplate();
    if !IsDefined(colorTemplateRecords) {
      return vehicleColorTemplate;
    };
    currentAppearance = this.GetVehicle().GetCurrentAppearanceName();
    currentColorVariant = this.GetVehicle().GetCurrentColorVariantName();
    i = 0;
    while i < colorTemplateRecords.GetColorTemplatesCount() {
      templateRecord = colorTemplateRecords.GetColorTemplatesItem(i);
      if !IsDefined(templateRecord) {
      } else {
        appearanceMatch = templateRecord.MatchingAppearancesContains(currentAppearance) && Equals(currentColorVariant, n"None");
        colorVariantMatch = templateRecord.MatchingColorVariantContains(currentColorVariant) && NotEquals(currentColorVariant, n"None");
        if appearanceMatch || colorVariantMatch {
          return VehicleVisualCustomizationTemplate.FromRecord(templateRecord, this.GetVehicle().GetRecord().TwintoneModelName());
        };
      };
      i += 1;
    };
    return vehicleColorTemplate;
  }

  public final func CanApplyTemplateOnVehicle(template: VehicleVisualCustomizationTemplate, opt ignoreAlreadyApplied: Bool) -> Bool {
    let m_vehicle: ref<VehicleObject> = this.GetVehicle();
    if !VehicleVisualCustomizationTemplate.IsValid(template) || !m_vehicle.GetVehicleComponent().GetIsVehicleVisualCustomizationEnabled() {
      return false;
    };
    if m_vehicle.GetVehiclePS().GetIsVehicleVisualCustomizationBlockedByDamage() || m_vehicle.GetVehicleComponent().GetIsVehicleVisualCustomizationTeaser() {
      return false;
    };
    if !ignoreAlreadyApplied && VehicleVisualCustomizationTemplate.Equals(m_vehicle.GetVehiclePS().GetVehicleVisualCustomizationTemplate(), template) {
      return false;
    };
    if Equals(VehicleVisualCustomizationTemplate.GetType(template), VehicleVisualCustomizationType.Generic) {
      return true;
    };
    return Equals(template.uniqueData.twintoneModelName, m_vehicle.GetRecord().TwintoneModelName());
  }

  private final func CheckAboutToExplodeStateOnFlip() -> Void {
    let speedTreshold: Float = TweakDBInterface.GetFloat(t"vehicles.common.hostile_explode_on_flip_speed_treshold", 5.00);
    let probability: Float = TweakDBInterface.GetFloat(t"vehicles.common.hostile_explode_on_flip_probability", 0.30);
    if AbsF(this.GetVehicle().GetCurrentSpeed()) > speedTreshold && RandF() < probability {
      this.ForceAboutToExplodeState();
    };
  }

  protected cb func OnHasVehicleBeenFlippedOverForSomeTimeEvent(evt: ref<HasVehicleBeenFlippedOverForSomeTimeEvent>) -> Bool {
    let godMode: gameGodModeType;
    let isDestroyed: Bool = this.GetPS().GetIsDestroyed();
    if isDestroyed {
      return false;
    };
    if GetImmortality(this.GetVehicle(), godMode) {
      return false;
    };
    this.ApplyVehicleDOT();
  }

  protected final func ApplyVehicleDOT(opt type: CName) -> Void {
    let statusEffectSystem: ref<StatusEffectSystem> = GameInstance.GetStatusEffectSystem(this.GetVehicle().GetGame());
    switch type {
      case n"high":
        if !statusEffectSystem.HasStatusEffect(this.GetVehicle().GetEntityID(), t"BaseStatusEffect.VehicleHighDamageOverTimeEffect") {
          statusEffectSystem.ApplyStatusEffect(this.GetVehicle().GetEntityID(), t"BaseStatusEffect.VehicleHighDamageOverTimeEffect", this.GetVehicle().GetRecordID(), this.GetVehicle().GetEntityID());
        };
        break;
      default:
        if !statusEffectSystem.HasStatusEffect(this.GetVehicle().GetEntityID(), t"BaseStatusEffect.VehicleBaseDamageOverTimeEffect") {
          statusEffectSystem.ApplyStatusEffect(this.GetVehicle().GetEntityID(), t"BaseStatusEffect.VehicleBaseDamageOverTimeEffect", this.GetVehicle().GetRecordID(), this.GetVehicle().GetEntityID());
        };
    };
  }

  protected final func RemoveVehicleDOT() -> Void {
    let statusEffectSystem: ref<StatusEffectSystem> = GameInstance.GetStatusEffectSystem(this.GetVehicle().GetGame());
    if statusEffectSystem.HasStatusEffect(this.GetVehicle().GetEntityID(), t"BaseStatusEffect.VehicleHighDamageOverTimeEffect") {
      statusEffectSystem.RemoveStatusEffect(this.GetVehicle().GetEntityID(), t"BaseStatusEffect.VehicleHighDamageOverTimeEffect");
    };
    if statusEffectSystem.HasStatusEffect(this.GetVehicle().GetEntityID(), t"BaseStatusEffect.VehicleBaseDamageOverTimeEffect") {
      statusEffectSystem.RemoveStatusEffect(this.GetVehicle().GetEntityID(), t"BaseStatusEffect.VehicleBaseDamageOverTimeEffect");
    };
  }

  protected cb func OnVehicleQuestAVThrusterEvent(evt: ref<VehicleQuestAVThrusterEvent>) -> Bool {
    this.SetupThrusterFX();
  }

  protected cb func OnVehicleQuestWindowDestructionEvent(evt: ref<VehicleQuestWindowDestructionEvent>) -> Bool {
    let windowName: CName;
    let windowDestructionEvent: ref<VehicleGlassDestructionEvent> = new VehicleGlassDestructionEvent();
    if NotEquals(evt.windowName, n"None") {
      windowName = evt.windowName;
    } else {
      windowName = EnumValueToName(n"vehicleQuestWindowDestruction", Cast<Int64>(EnumInt(evt.window)));
    };
    windowDestructionEvent.glassName = windowName;
    this.GetVehicle().QueueEvent(windowDestructionEvent);
  }

  protected cb func OnFactChangedEvent(evt: ref<FactChangedEvent>) -> Bool {
    let forwardEvent: ref<VehicleForwardRaceCheckpointFactEvent>;
    let uiSystem: ref<UISystem>;
    if Equals(evt.GetFactName(), n"sq024_current_race_checkpoint_fact_add") {
      uiSystem = GameInstance.GetUISystem(this.GetVehicle().GetGame());
      forwardEvent = new VehicleForwardRaceCheckpointFactEvent();
      uiSystem.QueueEvent(forwardEvent);
    };
  }

  protected cb func OnVehicleRaceQuestEvent(evt: ref<VehicleRaceQuestEvent>) -> Bool {
    switch evt.mode {
      case vehicleRaceUI.RaceStart:
        this.ToggleRaceClock(true);
        break;
      case vehicleRaceUI.RaceEnd:
        this.ToggleRaceClock(false);
    };
  }

  private final func ToggleRaceClock(toggle: Bool) -> Void {
    let raceClockEvent: ref<VehicleRaceClockUpdateEvent>;
    let delaySystem: ref<DelaySystem> = GameInstance.GetDelaySystem(this.GetVehicle().GetGame());
    if toggle {
      raceClockEvent = new VehicleRaceClockUpdateEvent();
      this.m_raceClockTickID = delaySystem.TickOnEvent(this.GetVehicle(), raceClockEvent, 600.00);
    } else {
      delaySystem.CancelTick(this.m_raceClockTickID);
    };
  }

  protected cb func OnVehicleRaceClockUpdateEvent(evt: ref<VehicleRaceClockUpdateEvent>) -> Bool {
    let uiSystem: ref<UISystem> = GameInstance.GetUISystem(this.GetVehicle().GetGame());
    let forwardEvent: ref<VehicleForwardRaceClockUpdateEvent> = new VehicleForwardRaceClockUpdateEvent();
    uiSystem.QueueEvent(forwardEvent);
  }

  protected final func CleanUpRace() -> Void {
    let uiSystem: ref<UISystem> = GameInstance.GetUISystem(this.GetVehicle().GetGame());
    GameInstance.GetDelaySystem(this.GetVehicle().GetGame()).CancelTick(this.m_raceClockTickID);
    uiSystem.PopGameContext(UIGameContext.VehicleRace);
  }

  private final func CreateObjectActionsCallbackController(instigator: wref<Entity>) -> Void {
    this.m_objectActionsCallbackCtrl = gameObjectActionsCallbackController.Create(this.GetEntity(), instigator, this.GetVehicle().GetGame());
    this.m_objectActionsCallbackCtrl.RegisterSkillCheckCallbacks();
  }

  private final func DestroyObjectActionsCallbackController() -> Void {
    this.m_objectActionsCallbackCtrl.UnregisterSkillCheckCallbacks();
    this.m_objectActionsCallbackCtrl = null;
  }

  protected cb func OnObjectActionRefreshEvent(evt: ref<gameObjectActionRefreshEvent>) -> Bool {
    if IsDefined(this.m_objectActionsCallbackCtrl) {
      this.m_objectActionsCallbackCtrl.UnlockNotifications();
      this.DetermineInteractionState();
    };
  }

  protected cb func OnVehicleQuestToggleEngineEvent(evt: ref<VehicleQuestToggleEngineEvent>) -> Bool {
    if evt.vehicleOnEngineOff {
      if evt.toggle {
        this.ToggleVehicleSystems(true, true, false, evt.lockState);
      } else {
        this.ToggleVehicleSystems(false, false, true, evt.lockState);
      };
    } else {
      this.ToggleVehicleSystems(evt.toggle, true, true, evt.lockState);
    };
  }

  protected cb func OnSetIgnoreAutoDoorCloseEvent(evt: ref<SetIgnoreAutoDoorCloseEvent>) -> Bool {
    this.m_ignoreAutoDoorClose = evt.set;
  }

  public final static func OnThreatInstantDrop(passenger: wref<GameObject>) -> Void {
    let vehicle: wref<VehicleObject>;
    let gi: GameInstance = passenger.GetGame();
    if !VehicleComponent.GetVehicle(gi, passenger, vehicle) || !vehicle.IsAttached() || !VehicleComponent.IsDriver(gi, passenger) {
      return;
    };
    VehicleComponent.SendPoliceJustLostPlayerSearchCommand(vehicle as WheeledObject);
  }

  private final static func SendPoliceJustLostPlayerSearchCommand(vehicle: wref<WheeledObject>) -> Void {
    let gi: GameInstance = vehicle.GetGame();
    let destination: Vector4 = GameInstance.GetPreventionSpawnSystem(gi).GetIntersectionInFrontOfPlayerPos();
    vehicle.SetPoliceStrategy(vehiclePoliceStrategy.DriveTowardsPlayer);
    if Vector4.IsXYZZero(destination) {
      destination = GameInstance.GetPlayerSystem(gi).GetLocalPlayerControlledGameObject().GetWorldPosition();
    };
    vehicle.SetPoliceStrategyDestination(destination);
    if !GameInstance.GetPreventionSpawnSystem(gi).IsPlayerInDogTown() {
      vehicle.GetAIComponent().SetKeepStrategyOnSearch(true);
    };
    PreventionSystem.StartActiveVehicleBehaviour(gi, vehicle);
  }

  protected cb func OnUploadProgramProgress(evt: ref<UploadProgramProgressEvent>) -> Bool {
    let currentlyUploadingAction: ref<ScriptableDeviceAction>;
    let icon: wref<ChoiceCaptionIconPart_Record>;
    let quickSlotCommandUsed: ref<QuickSlotCommandUsed>;
    let quickhackMappinScriptData: ref<GameplayRoleMappinData>;
    if Equals(evt.progressBarContext, EProgressBarContext.QuickHack) {
      switch evt.state {
        case EUploadProgramState.STARTED:
          quickhackMappinScriptData = new GameplayRoleMappinData();
          quickhackMappinScriptData.statPoolType = evt.statPoolType;
          quickhackMappinScriptData.m_mappinVisualState = EMappinVisualState.Default;
          quickhackMappinScriptData.m_progressBarType = evt.progressBarType;
          quickhackMappinScriptData.m_progressBarContext = evt.progressBarContext;
          quickhackMappinScriptData.m_duration = evt.duration;
          quickhackMappinScriptData.m_visibleThroughWalls = true;
          quickhackMappinScriptData.m_action = evt.action;
          icon = evt.action.GetInteractionIcon();
          if IsDefined(icon) {
            quickhackMappinScriptData.m_textureID = icon.TexturePartID().GetID();
          } else {
            if IsDefined(evt.iconRecord) {
              quickhackMappinScriptData.m_textureID = evt.iconRecord.TexturePartID().GetID();
            };
          };
          this.CreateQuickHackMappin(quickhackMappinScriptData);
          break;
        case EUploadProgramState.COMPLETED:
          this.DestroyQuickHackMappin();
          currentlyUploadingAction = this.GetVehicle().GetCurrentlyUploadingAction();
          quickSlotCommandUsed = QuickHackableQueueHelper.PopFromQuickHackQueue(evt, null);
          if IsDefined(quickSlotCommandUsed) && !this.GetPS().GetIsDestroyed() {
            this.OnQuickSlotCommandUsed(quickSlotCommandUsed);
          } else {
            if IsDefined(currentlyUploadingAction) {
              currentlyUploadingAction.m_isInactive = true;
              this.GetVehicle().SetCurrentlyUploadingAction(currentlyUploadingAction);
            };
          };
      };
    };
  }

  private final func CreateQuickHackMappin(quickhackMappinScriptData: ref<GameplayRoleMappinData>) -> Void {
    let mappinData: MappinData;
    let vehicle: wref<VehicleObject> = this.GetVehicle();
    mappinData.active = true;
    mappinData.mappinType = t"Mappins.InteractionMappinDefinition";
    mappinData.variant = gamedataMappinVariant.QuickHackVariant;
    mappinData.scriptData = quickhackMappinScriptData;
    mappinData.visibleThroughWalls = true;
    this.m_quickhackMappinID = GameInstance.GetMappinSystem(vehicle.GetGame()).RegisterMappinWithObject(mappinData, vehicle, vehicle.GetQuickHackIndicatorSlotName());
  }

  private final func DestroyQuickHackMappin() -> Void {
    if this.m_quickhackMappinID.value != 0u {
      GameInstance.GetMappinSystem(this.GetVehicle().GetGame()).UnregisterMappin(this.m_quickhackMappinID);
      this.m_quickhackMappinID.value = 0u;
    };
  }
}

public class VehicleHealthStatPoolListener extends CustomValueStatPoolsListener {

  public let m_owner: wref<VehicleObject>;

  public func OnStatPoolValueChanged(oldValue: Float, newValue: Float, percToPoints: Float) -> Void {
    if IsDefined(this.m_owner) && IsDefined(this.m_owner.GetVehicleComponent()) {
      this.m_owner.GetVehicleComponent().ReactToHPChange(newValue);
    };
  }
}

public class VehicleRadioTierEvent extends Event {

  @default(VehicleRadioTierEvent, 1)
  public edit let radioTier: Uint32;

  public edit let overrideTier: Bool;

  public final func GetFriendlyDescription() -> String {
    return "Toggle or set Radio Tier";
  }
}

public class ShouldNPCReEquipWeaponOnDismount extends AIbehaviorconditionScript {

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let bike: ref<BikeObject>;
    let vehicle: wref<GameObject>;
    let vehicleID: EntityID;
    let vehicleSlotID: MountingSlotId;
    let owner: ref<gamePuppet> = ScriptExecutionContext.GetOwner(context);
    let gameInstance: GameInstance = owner.GetGame();
    if AIBehaviorScriptBase.GetAIComponent(context).GetAssignedVehicle(gameInstance, vehicle) {
      if AIBehaviorScriptBase.GetAIComponent(context).GetAssignedVehicleData(vehicleID, vehicleSlotID) {
        if !VehicleComponent.GetDriverMounted(ScriptExecutionContext.GetOwner(context).GetGame(), vehicleID).IsPlayer() {
          bike = vehicle as BikeObject;
          if IsDefined(bike) {
            if !VehicleComponent.IsDestroyed(ScriptExecutionContext.GetOwner(context).GetGame(), vehicleID) {
              return AIbehaviorConditionOutcomes.False;
            };
          };
        };
      };
    };
    return AIbehaviorConditionOutcomes.True;
  }
}
