
public class PlayerDevelopmentData extends IScriptable {

  public let m_owner: wref<GameObject>;

  private persistent let m_ownerID: EntityID;

  private persistent let m_queuedCombatExp: [SExperiencePoints];

  private persistent let m_proficiencies: [SProficiency];

  private persistent let m_attributes: [SAttribute];

  private persistent let m_perkAreas: [SPerkArea];

  private persistent let m_traits: [STrait];

  private persistent let m_devPoints: [SDevelopmentPoints];

  private persistent let m_skillPrereqs: [ref<SkillCheckPrereqState>];

  private persistent let m_statPrereqs: [ref<StatCheckPrereqState>];

  private persistent let m_knownRecipes: [ItemRecipe];

  private persistent let m_attributesData: [SAttributeData];

  private persistent let m_highestCompletedMinigameLevel: Int32;

  @default(PlayerDevelopmentData, 1)
  private const let m_startingLevel: Int32;

  @default(PlayerDevelopmentData, 0)
  private const let m_startingExperience: Int32;

  private persistent let m_lifePath: gamedataLifePath;

  @default(PlayerDevelopmentData, true)
  private let m_displayActivityLog: Bool;

  @default(PlayerDevelopmentData, true)
  private let m_progressionBuildSetCompleted: Bool;

  public final func OnAttach() -> Void;

  public final func OnDetach() -> Void;

  public final func OnNewGame() -> Void {
    this.SetProficiencies();
    this.InitializeAttributesData();
    this.InitializePerkAreas();
    this.InitializeTraits();
    this.SetDevelopmentPoints();
    this.UpdateUIBB();
  }

  public final func EspionageAttributeRetrofix() -> Void {
    let attribute: SAttribute;
    if ArraySize(this.m_attributes) == 6 {
      attribute.value = 0;
      attribute.attributeName = gamedataStatType.Espionage;
      attribute.id = TDBID.Create("BaseStats." + ToString(attribute.attributeName));
      ArrayInsert(this.m_attributes, 1, attribute);
    };
  }

  public final func OnRestored(gameInstance: GameInstance) -> Void {
    let i: Int32;
    let j: Int32;
    let k: Int32;
    let proficiencyIndex: Int32;
    let shouldTraitUnlock: Bool;
    let statMod: ref<gameConstantStatModifierData>;
    let statSys: ref<StatsSystem>;
    let traitIndex: Int32;
    let traitType: gamedataTraitType;
    let type: gamedataProficiencyType;
    if !EntityID.IsDefined(this.m_ownerID) {
      this.m_owner = GetPlayer(gameInstance);
      this.m_ownerID = this.m_owner.GetEntityID();
    } else {
      this.m_owner = GameInstance.FindEntityByID(gameInstance, this.m_ownerID) as GameObject;
    };
    statSys = GameInstance.GetStatsSystem(gameInstance);
    i = 0;
    while i < ArraySize(this.m_proficiencies) {
      type = this.m_proficiencies[i].type;
      proficiencyIndex = this.GetProficiencyIndexByType(type);
      this.RestoreProficiencyPassiveBonuses(proficiencyIndex, gameInstance);
      this.UpdateUIBB();
      if this.IsProficiencyStatAdded(type) {
      } else {
        this.SetProficiencyStat(this.m_proficiencies[i].type, this.m_proficiencies[i].currentLevel);
        this.m_proficiencies[i].maxLevel = this.GetProficiencyMaxLevel(this.m_proficiencies[i].type);
        this.m_proficiencies[i].isAtMaxLevel = this.m_proficiencies[i].maxLevel == this.m_proficiencies[i].currentLevel;
        this.m_proficiencies[i].expToLevel = this.GetRemainingExpForLevelUp(this.m_proficiencies[i].type);
        traitType = RPGManager.GetProficiencyRecord(type).Trait().Type();
        traitIndex = this.GetTraitIndex(traitType);
        if traitIndex >= 0 {
          shouldTraitUnlock = this.IsTraitReqMet(this.m_traits[traitIndex].type);
          if this.m_traits[traitIndex].unlocked && !shouldTraitUnlock {
            this.AddDevelopmentPoints(this.m_traits[traitIndex].currLevel, gamedataDevelopmentPointType.Primary);
            this.m_traits[traitIndex].currLevel = 0;
          };
          this.m_traits[traitIndex].unlocked = shouldTraitUnlock;
          if this.m_traits[traitIndex].unlocked {
            this.ActivateTraitBase(this.m_traits[traitIndex].type);
            this.EvaluateTraitInfiniteData(traitIndex);
          };
        };
      };
      i += 1;
    };
    GameInstance.GetLevelAssignmentSystem(gameInstance).MarkPlayerLevelRestored();
    i = 0;
    while i < ArraySize(this.m_attributes) {
      statMod = new gameConstantStatModifierData();
      statMod.statType = this.m_attributes[i].attributeName;
      statMod.value = Cast<Float>(this.m_attributes[i].value);
      statMod.modifierType = gameStatModifierType.Additive;
      statSys.ForceModifier(Cast<StatsObjectID>(this.m_owner.GetEntityID()), statMod);
      i += 1;
    };
    i = 0;
    while i < ArraySize(this.m_perkAreas) {
      j = 0;
      while j < ArraySize(this.m_perkAreas[i].boughtPerks) {
        this.ActivatePerkLevelData(i, this.GetPerkIndex(this.m_perkAreas[i].boughtPerks[j].type));
        j += 1;
      };
      i += 1;
    };
    if ArraySize(this.m_attributesData) == 0 {
      this.InitializeAttributesData();
    };
    i = 0;
    while i < ArraySize(this.m_attributesData) {
      j = 0;
      while j < ArraySize(this.m_attributesData[i].unlockedPerks) {
        k = 1;
        while k <= this.m_attributesData[i].unlockedPerks[j].currLevel {
          this.ActivateNewPerk(this.m_attributesData[i].unlockedPerks[j].type, k - 1);
          k += 1;
        };
        j += 1;
      };
      i += 1;
    };
  }

  public final func SetOwner(owner: ref<GameObject>) -> Void {
    this.m_owner = owner;
    this.m_ownerID = owner.GetEntityID();
  }

  public final func GetOwner() -> wref<GameObject> {
    return this.m_owner;
  }

  public final func GetOwnerID() -> EntityID {
    return this.m_ownerID;
  }

  public final const func GetLifePath() -> gamedataLifePath {
    return this.m_lifePath;
  }

  public final const func GetProficiencyLevel(type: gamedataProficiencyType) -> Int32 {
    let profIndex: Int32 = this.GetProficiencyIndexByType(type);
    if profIndex >= 0 {
      return this.m_proficiencies[profIndex].currentLevel;
    };
    return -1;
  }

  public final const func GetProficiencyAbsoluteMaxLevel(type: gamedataProficiencyType) -> Int32 {
    return RPGManager.GetProficiencyRecord(type).MaxLevel();
  }

  public final const func GetCurrentLevelProficiencyExp(type: gamedataProficiencyType) -> Int32 {
    let profIndex: Int32 = this.GetProficiencyIndexByType(type);
    if profIndex >= 0 {
      return this.m_proficiencies[profIndex].currentExp;
    };
    return -1;
  }

  public final const func GetTotalProfExperience(type: gamedataProficiencyType) -> Int32 {
    let colName: CName;
    let curvName: CName;
    let i: Int32;
    let maxLvl: Int32;
    let totalExp: Int32;
    let statDataSys: ref<StatsDataSystem> = GameInstance.GetStatsDataSystem(this.m_owner.GetGame());
    let pIndex: Int32 = this.GetProficiencyIndexByType(type);
    if pIndex < 0 {
      return -1;
    };
    this.GetProficiencyExpCurveNames(type, curvName, colName);
    if !IsNameValid(curvName) || !IsNameValid(colName) {
      return -1;
    };
    maxLvl = this.GetProficiencyMaxLevel(type);
    i = 0;
    while i <= maxLvl {
      totalExp += Cast<Int32>(statDataSys.GetValueFromCurve(curvName, Cast<Float>(i), colName));
      i += 1;
    };
    return totalExp;
  }

  public final const func GetRemainingExpForLevelUp(type: gamedataProficiencyType) -> Int32 {
    let exp: Int32;
    let pIndex: Int32 = this.GetProficiencyIndexByType(type);
    if pIndex >= 0 {
      if !this.IsProficiencyMaxLvl(type) {
        exp = this.GetExperienceForNextLevel(type);
        return exp - this.m_proficiencies[pIndex].currentExp;
      };
      return -1;
    };
    return -1;
  }

  public final const func GetDominatingCombatProficiency() -> gamedataProficiencyType {
    let dominatingProf: gamedataProficiencyType;
    let highestLevel: Int32;
    let i: Int32;
    let profsToCheck: array<gamedataProficiencyType>;
    ArrayPush(profsToCheck, gamedataProficiencyType.StrengthSkill);
    ArrayPush(profsToCheck, gamedataProficiencyType.ReflexesSkill);
    ArrayPush(profsToCheck, gamedataProficiencyType.CoolSkill);
    ArrayPush(profsToCheck, gamedataProficiencyType.IntelligenceSkill);
    ArrayPush(profsToCheck, gamedataProficiencyType.TechnicalAbilitySkill);
    i = 0;
    while i < ArraySize(this.m_proficiencies) {
      if ArrayContains(profsToCheck, this.m_proficiencies[i].type) {
        if this.m_proficiencies[i].currentLevel > highestLevel {
          highestLevel = this.m_proficiencies[i].currentLevel;
          dominatingProf = this.m_proficiencies[i].type;
        };
      };
      i += 1;
    };
    return dominatingProf;
  }

  public final const func GetHighestCompletedMinigameLevel() -> Int32 {
    return this.m_highestCompletedMinigameLevel;
  }

  public final const func GetProficiencyRecordByIndex(index: Int32) -> ref<Proficiency_Record> {
    let type: gamedataProficiencyType = this.m_proficiencies[index].type;
    return TweakDBInterface.GetProficiencyRecord(TDBID.Create("Proficiencies." + EnumValueToString("gamedataProficiencyType", Cast<Int64>(EnumInt(type)))));
  }

  public final func ReinitializeProficiencies() -> Void {
    this.SetProficiencies();
    this.UpdateProficiencyMaxLevels();
    this.SetDevelopmentPoints();
  }

  private final func SetProficiencies() -> Void {
    let i: Int32;
    if ArraySize(this.m_proficiencies) == 20 {
      return;
    };
    i = 0;
    while i < 20 {
      if this.GetProficiencyIndexByType(IntEnum<gamedataProficiencyType>(i)) < 0 {
        this.AddProficiency(IntEnum<gamedataProficiencyType>(i));
      };
      i += 1;
    };
  }

  public final const func GetProficiencyIndexByType(type: gamedataProficiencyType) -> Int32 {
    let i: Int32 = 0;
    while i < ArraySize(this.m_proficiencies) {
      if Equals(this.m_proficiencies[i].type, type) {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  public final const func ResetAllProficienciesLevel() -> Void {
    let index: Int32 = 0;
    let limit: Int32 = 20;
    while index < limit {
      if NotEquals(IntEnum<gamedataProficiencyType>(index), gamedataProficiencyType.StreetCred) && NotEquals(IntEnum<gamedataProficiencyType>(index), gamedataProficiencyType.Level) {
        this.ResetProficiencyLevel(IntEnum<gamedataProficiencyType>(index));
      };
      index += 1;
    };
  }

  private final const func ResetProficiencyLevel(type: gamedataProficiencyType) -> Void {
    let pIndex: Int32 = this.GetProficiencyIndexByType(type);
    if pIndex < 0 {
      return;
    };
    this.m_proficiencies[pIndex].currentLevel = this.m_startingLevel;
    this.m_proficiencies[pIndex].currentExp = this.m_startingExperience;
    this.SetProficiencyStat(type, this.m_startingLevel);
  }

  private final const func GetProficiencyMaxLevel(type: gamedataProficiencyType) -> Int32 {
    let absoluteMaxLevel: Int32;
    let attributeInt: Int32;
    let attributeMaxLevel: Int32;
    let attributeRec: ref<Stat_Record>;
    let attributeType: gamedataStatType;
    let attributeValue: Int32;
    let colName: CName;
    let curveSetName: CName;
    let proficiencyRec: ref<Proficiency_Record>;
    let pIndex: Int32 = this.GetProficiencyIndexByType(type);
    if pIndex < 0 {
      return -1;
    };
    this.GetProficiencyExpCurveNames(type, curveSetName, colName);
    if Equals(curveSetName, n"None") {
      return -1;
    };
    proficiencyRec = RPGManager.GetProficiencyRecord(type);
    absoluteMaxLevel = proficiencyRec.MaxLevel();
    attributeRec = proficiencyRec.TiedAttribute();
    attributeInt = -1;
    if IsDefined(attributeRec) {
      attributeInt = Cast<Int32>(EnumValueFromString("gamedataStatType", attributeRec.EnumName()));
    };
    if attributeInt >= 0 {
      attributeType = IntEnum<gamedataStatType>(attributeInt);
      attributeValue = this.m_attributes[this.GetAttributeIndex(attributeType)].value;
      attributeMaxLevel = attributeValue;
      return Max(Min(absoluteMaxLevel, attributeMaxLevel), 1);
    };
    return absoluteMaxLevel;
  }

  private final const func GetProficiencyExpCurveNames(type: gamedataProficiencyType, out curvName: CName, out colName: CName) -> Void {
    let proficiencyRecord: wref<Proficiency_Record>;
    let pIndex: Int32 = this.GetProficiencyIndexByType(type);
    if pIndex < 0 {
      curvName = n"None";
      return;
    };
    proficiencyRecord = RPGManager.GetProficiencyRecord(type);
    colName = proficiencyRecord.CurveName();
    curvName = proficiencyRecord.CurveSetName();
  }

  public final const func ModifyProficiencyLevel(type: gamedataProficiencyType, opt isDebug: Bool, opt levelIncrease: Int32) -> Void {
    let i: Int32 = this.GetProficiencyIndexByType(type);
    if i >= 0 {
      this.ModifyProficiencyLevel(i, isDebug, levelIncrease);
      this.EvaluateTrait(type);
    };
  }

  private final const func ModifyProficiencyLevel(proficiencyIndex: Int32, isDebug: Bool, opt levelIncrease: Int32) -> Void {
    let Blackboard: ref<IBlackboard>;
    let effectTags: array<CName>;
    let effects: array<ref<StatusEffect>>;
    let i: Int32;
    let level: LevelUpData;
    let statusEffectSys: ref<StatusEffectSystem>;
    if levelIncrease == 0 {
      levelIncrease = 1;
    };
    this.m_proficiencies[proficiencyIndex].currentLevel += levelIncrease;
    this.m_proficiencies[proficiencyIndex].currentExp = 0;
    this.m_proficiencies[proficiencyIndex].expToLevel = this.GetRemainingExpForLevelUp(this.m_proficiencies[proficiencyIndex].type);
    if !isDebug {
      this.ModifyDevPoints(this.m_proficiencies[proficiencyIndex].type, this.m_proficiencies[proficiencyIndex].currentLevel);
    };
    level.lvl = this.m_proficiencies[proficiencyIndex].currentLevel;
    level.type = this.m_proficiencies[proficiencyIndex].type;
    level.perkPoints = this.GetDevPoints(gamedataDevelopmentPointType.Primary);
    level.attributePoints = this.GetDevPoints(gamedataDevelopmentPointType.Attribute);
    level.espionagePoints = this.GetDevPoints(gamedataDevelopmentPointType.Espionage);
    this.SetProficiencyStat(this.m_proficiencies[proficiencyIndex].type, this.m_proficiencies[proficiencyIndex].currentLevel);
    this.ProcessProficiencyPassiveBonus(proficiencyIndex);
    Blackboard = GameInstance.GetBlackboardSystem(this.m_owner.GetGame()).Get(GetAllBlackboardDefs().UI_LevelUp);
    if IsDefined(Blackboard) && this.m_owner == GameInstance.GetPlayerSystem(this.m_owner.GetGame()).GetLocalPlayerMainGameObject() {
      Blackboard.SetVariant(GetAllBlackboardDefs().UI_LevelUp.level, ToVariant(level));
      Blackboard.SignalVariant(GetAllBlackboardDefs().UI_LevelUp.level);
    };
    this.SetAchievementProgress(proficiencyIndex);
    if this.m_proficiencies[proficiencyIndex].currentLevel == RPGManager.GetProficiencyRecord(this.m_proficiencies[proficiencyIndex].type).MaxLevel() {
      if Equals(this.m_proficiencies[proficiencyIndex].type, gamedataProficiencyType.StreetCred) {
        this.SendMaxStreetCredLevelReachedTrackingRequest();
      } else {
        if NotEquals(this.m_proficiencies[proficiencyIndex].type, gamedataProficiencyType.Level) && NotEquals(this.m_proficiencies[proficiencyIndex].type, gamedataProficiencyType.Espionage) {
          this.CheckSpecialistAchievement(proficiencyIndex);
        };
      };
    };
    if Equals(this.m_proficiencies[proficiencyIndex].type, gamedataProficiencyType.Level) {
      this.ProcessTutorialFacts();
      if Equals(GameInstance.GetStatsDataSystem(this.m_owner.GetGame()).GetDifficulty(), gameDifficulty.Story) {
        GameInstance.GetStatPoolsSystem(this.m_owner.GetGame()).RequestSettingStatPoolValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), gamedataStatPoolType.Health, 100.00, this.m_owner);
        statusEffectSys = GameInstance.GetStatusEffectSystem(this.m_owner.GetGame());
        statusEffectSys.GetAppliedEffects(this.m_owner.GetEntityID(), effects);
        i = 0;
        while i < ArraySize(effects) {
          effectTags = effects[i].GetRecord().GameplayTags();
          if effects[i].GetRemainingDuration() > 0.00 && ArrayContains(effectTags, n"Debuff") {
            statusEffectSys.RemoveStatusEffect(this.m_owner.GetEntityID(), effects[i].GetRecord().GetID(), effects[i].GetStackCount());
          };
          i += 1;
        };
      };
    };
  }

  private final const func ProcessTutorialFacts() -> Void {
    let questSys: ref<QuestsSystem> = GameInstance.GetQuestsSystem(this.m_owner.GetGame());
    if questSys.GetFact(n"levelup_tutorial") == 0 && questSys.GetFact(n"disable_tutorials") == 0 {
      questSys.SetFact(n"levelup_tutorial", 1);
    };
  }

  private final const func SendMaxStreetCredLevelReachedTrackingRequest() -> Void {
    let achievement: gamedataAchievement = gamedataAchievement.YouKnowWhoIAm;
    let dataTrackingSystem: ref<DataTrackingSystem> = GameInstance.GetScriptableSystemsContainer(this.m_owner.GetGame()).Get(n"DataTrackingSystem") as DataTrackingSystem;
    let achievementRequest: ref<AddAchievementRequest> = new AddAchievementRequest();
    achievementRequest.achievement = achievement;
    dataTrackingSystem.QueueRequest(achievementRequest);
  }

  public final const func CheckSpecialistAchievement(index: Int32) -> Void {
    let achievement: gamedataAchievement = gamedataAchievement.Specialist;
    let dataTrackingSystem: ref<DataTrackingSystem> = GameInstance.GetScriptableSystemsContainer(this.m_owner.GetGame()).Get(n"DataTrackingSystem") as DataTrackingSystem;
    let achievementRequest: ref<AddAchievementRequest> = new AddAchievementRequest();
    achievementRequest.achievement = achievement;
    dataTrackingSystem.QueueRequest(achievementRequest);
  }

  public final const func CheckRelicMasterAchievement() -> Void {
    let achievementRequest: ref<SetAchievementProgressRequest> = new SetAchievementProgressRequest();
    let dataTrackingSystem: ref<DataTrackingSystem> = GameInstance.GetScriptableSystemsContainer(this.m_owner.GetGame()).Get(n"DataTrackingSystem") as DataTrackingSystem;
    achievementRequest.achievement = gamedataAchievement.RelicMaster;
    let i: Int32 = 0;
    while i < ArraySize(this.m_attributesData[2].unlockedPerks) {
      if this.m_attributesData[2].unlockedPerks[i].currLevel > 0 {
        achievementRequest.currentValue += 1;
      };
      i += 1;
    };
    dataTrackingSystem.QueueRequest(achievementRequest);
  }

  private final const func SetAchievementProgress(index: Int32) -> Void {
    let achievement: gamedataAchievement;
    let setAchievementRequest: ref<SetAchievementProgressRequest> = new SetAchievementProgressRequest();
    let dataTrackingSystem: ref<DataTrackingSystem> = GameInstance.GetScriptableSystemsContainer(this.m_owner.GetGame()).Get(n"DataTrackingSystem") as DataTrackingSystem;
    if Equals(this.m_proficiencies[index].type, gamedataProficiencyType.StreetCred) {
      achievement = gamedataAchievement.YouKnowWhoIAm;
    } else {
      if NotEquals(this.m_proficiencies[index].type, gamedataProficiencyType.StreetCred) && NotEquals(this.m_proficiencies[index].type, gamedataProficiencyType.Level) && NotEquals(this.m_proficiencies[index].type, gamedataProficiencyType.Espionage) {
        achievement = gamedataAchievement.Specialist;
      } else {
        return;
      };
    };
    setAchievementRequest.achievement = achievement;
    setAchievementRequest.currentValue = this.m_proficiencies[index].currentLevel;
    dataTrackingSystem.QueueRequest(setAchievementRequest);
  }

  private final const func IsProficiencyStatAdded(type: gamedataProficiencyType) -> Bool {
    let gi: GameInstance = this.m_owner.GetGame();
    let statString: String = EnumValueToString("gamedataProficiencyType", Cast<Int64>(EnumInt(type)));
    let statType: gamedataStatType = IntEnum<gamedataStatType>(Cast<Int32>(EnumValueFromString("gamedataStatType", statString)));
    return GameInstance.GetStatsSystem(gi).GetStatBoolValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), statType);
  }

  private final const func SetProficiencyStat(type: gamedataProficiencyType, level: Int32) -> Void {
    let statString: String = EnumValueToString("gamedataProficiencyType", Cast<Int64>(EnumInt(type)));
    let statType: gamedataStatType = IntEnum<gamedataStatType>(Cast<Int32>(EnumValueFromString("gamedataStatType", statString)));
    let newMod: ref<gameStatModifierData> = RPGManager.CreateStatModifier(statType, gameStatModifierType.Additive, Cast<Float>(level));
    GameInstance.GetStatsSystem(this.m_owner.GetGame()).ForceModifier(Cast<StatsObjectID>(this.m_owner.GetEntityID()), newMod);
  }

  private final const func ProcessProficiencyPassiveBonus(profIndex: Int32) -> Void {
    let bonusRecord: ref<PassiveProficiencyBonus_Record>;
    let effectorRecord: ref<Effector_Record>;
    if this.GetProficiencyRecordByIndex(profIndex).GetPassiveBonusesCount() > 0 {
      bonusRecord = this.GetProficiencyRecordByIndex(profIndex).GetPassiveBonusesItem(this.m_proficiencies[profIndex].currentLevel - 1);
      effectorRecord = bonusRecord.EffectorToTrigger();
      if IsDefined(effectorRecord) {
        GameInstance.GetEffectorSystem(this.m_owner.GetGame()).ApplyEffector(this.m_owner.GetEntityID(), this.m_owner, effectorRecord.GetID());
      };
    };
  }

  private final const func RestoreProficiencyPassiveBonuses(profIndex: Int32, gameInstance: GameInstance) -> Void {
    let bonusRecord: ref<PassiveProficiencyBonus_Record>;
    let effectorRecord: ref<Effector_Record>;
    let proficiencyRecord: ref<Proficiency_Record> = this.GetProficiencyRecordByIndex(profIndex);
    let effectorSystem: ref<EffectorSystem> = GameInstance.GetEffectorSystem(gameInstance);
    let maxLevel: Int32 = proficiencyRecord.GetPassiveBonusesCount();
    let i: Int32 = 0;
    while i < this.m_proficiencies[profIndex].currentLevel {
      if i >= maxLevel {
        return;
      };
      bonusRecord = proficiencyRecord.GetPassiveBonusesItem(i);
      effectorRecord = bonusRecord.EffectorToTrigger();
      if IsDefined(effectorRecord) && !effectorRecord.IsA(n"gamedataAddDevelopmentPointEffector_Record") {
        effectorSystem.ApplyEffector(this.m_ownerID, this.m_owner, effectorRecord.GetID());
      };
      i += 1;
    };
  }

  private final const func RefreshProficiencyStats() -> Void {
    let pIndex: Int32;
    let profType: gamedataProficiencyType;
    let i: Int32 = 0;
    while i < 20 {
      profType = IntEnum<gamedataProficiencyType>(i);
      pIndex = this.GetProficiencyIndexByType(profType);
      if NotEquals(profType, gamedataProficiencyType.Level) && pIndex >= 0 {
        this.SetProficiencyStat(profType, this.m_proficiencies[pIndex].currentLevel);
      };
      i += 1;
    };
  }

  private final const func UpdateProficiencyMaxLevels() -> Void {
    let i: Int32 = 0;
    while i < ArraySize(this.m_proficiencies) {
      this.m_proficiencies[i].maxLevel = this.GetProficiencyMaxLevel(this.m_proficiencies[i].type);
      i += 1;
    };
  }

  private final func AddProficiency(type: gamedataProficiencyType) -> Void {
    let newProf: SProficiency;
    newProf.type = type;
    newProf.currentLevel = this.m_startingLevel;
    newProf.currentExp = this.m_startingExperience;
    newProf.maxLevel = this.GetProficiencyMaxLevel(type);
    newProf.expToLevel = this.GetRemainingExpForLevelUp(type);
    ArrayPush(this.m_proficiencies, newProf);
  }

  public final const func AddExperience(amount: Int32, type: gamedataProficiencyType, telemetryGainReason: telemetryLevelGainReason, opt isDebug: Bool) -> Void {
    let awardedAmount: Int32;
    let proficiencyProgress: ref<ProficiencyProgressEvent>;
    let reqExp: Int32;
    let telemetryEvt: TelemetryLevelGained;
    let pIndex: Int32 = this.GetProficiencyIndexByType(type);
    if pIndex >= 0 && !this.IsProficiencyMaxLvl(type) {
      while amount > 0 && !this.IsProficiencyMaxLvl(type) {
        reqExp = this.GetRemainingExpForLevelUp(type);
        if amount - reqExp >= 0 {
          awardedAmount += reqExp;
          amount -= reqExp;
          this.m_proficiencies[pIndex].currentExp += reqExp;
          this.m_proficiencies[pIndex].expToLevel = this.GetRemainingExpForLevelUp(type);
          if this.CanGainNextProficiencyLevel(pIndex) {
            this.ModifyProficiencyLevel(type, isDebug);
            this.UpdateUIBB();
            if this.m_owner.IsPlayerControlled() && NotEquals(telemetryGainReason, telemetryLevelGainReason.Ignore) {
              telemetryEvt.playerPuppet = this.m_owner;
              telemetryEvt.proficiencyType = type;
              telemetryEvt.proficiencyValue = this.m_proficiencies[pIndex].currentLevel;
              telemetryEvt.isDebugEvt = Equals(telemetryGainReason, telemetryLevelGainReason.IsDebug);
              telemetryEvt.perkPointsAwarded = this.GetDevPointsForLevel(this.m_proficiencies[pIndex].currentLevel, type, gamedataDevelopmentPointType.Primary);
              telemetryEvt.attributePointsAwarded = this.GetDevPointsForLevel(this.m_proficiencies[pIndex].currentLevel, type, gamedataDevelopmentPointType.Attribute);
              GameInstance.GetTelemetrySystem(this.m_owner.GetGame()).LogLevelGained(telemetryEvt);
            };
          } else {
            return;
          };
        } else {
          this.m_proficiencies[pIndex].currentExp += amount;
          this.m_proficiencies[pIndex].expToLevel = this.GetRemainingExpForLevelUp(type);
          awardedAmount += amount;
          amount -= amount;
        };
      };
      if awardedAmount > 0 {
        if this.m_displayActivityLog {
          if Equals(type, gamedataProficiencyType.StreetCred) && GameInstance.GetQuestsSystem(this.m_owner.GetGame()).GetFact(n"street_cred_tutorial") == 0 && GameInstance.GetQuestsSystem(this.m_owner.GetGame()).GetFact(n"disable_tutorials") == 0 && Equals(telemetryGainReason, telemetryLevelGainReason.Gameplay) && GameInstance.GetQuestsSystem(this.m_owner.GetGame()).GetFact(n"q001_show_sts_tut") > 0 {
            GameInstance.GetQuestsSystem(this.m_owner.GetGame()).SetFact(n"street_cred_tutorial", 1);
          };
        };
        proficiencyProgress = new ProficiencyProgressEvent();
        proficiencyProgress.type = type;
        proficiencyProgress.expValue = this.GetCurrentLevelProficiencyExp(type);
        proficiencyProgress.delta = awardedAmount;
        proficiencyProgress.remainingXP = this.GetRemainingExpForLevelUp(type);
        proficiencyProgress.currentLevel = this.GetProficiencyLevel(type);
        proficiencyProgress.isLevelMaxed = this.GetProficiencyLevel(type) + 1 == this.GetProficiencyAbsoluteMaxLevel(type);
        GameInstance.GetUISystem(this.m_owner.GetGame()).QueueEvent(proficiencyProgress);
        if Equals(type, gamedataProficiencyType.Level) {
          this.UpdatePlayerXP();
        };
      };
    };
  }

  private final const func UpdatePlayerXP() -> Void {
    let gi: GameInstance = this.m_owner.GetGame();
    let m_ownerStatsBB: ref<IBlackboard> = GameInstance.GetBlackboardSystem(gi).Get(GetAllBlackboardDefs().UI_PlayerStats);
    m_ownerStatsBB.SetInt(GetAllBlackboardDefs().UI_PlayerStats.CurrentXP, this.GetCurrentLevelProficiencyExp(gamedataProficiencyType.Level), true);
  }

  public final func QueueCombatExperience(amount: Float, type: gamedataProficiencyType, entity: EntityID) -> Void {
    let expPackage: SExperiencePoints;
    expPackage.amount = amount;
    expPackage.forType = type;
    expPackage.entity = entity;
    ArrayPush(this.m_queuedCombatExp, expPackage);
  }

  public final func ProcessQueuedCombatExperience(entity: EntityID) -> Void {
    let expAmount: Float;
    let expAwarded: Bool;
    let j: Int32;
    let removeIndex: Int32;
    let toRemove: array<Int32>;
    let i: Int32 = 0;
    while i < 20 {
      expAwarded = false;
      expAmount = 0.00;
      j = 0;
      while j < ArraySize(this.m_queuedCombatExp) {
        if this.m_queuedCombatExp[j].entity == entity && EnumInt(this.m_queuedCombatExp[j].forType) == i {
          expAmount += this.m_queuedCombatExp[j].amount;
          expAwarded = true;
          ArrayPush(toRemove, j);
        };
        j += 1;
      };
      j = 0;
      while j < ArraySize(toRemove) {
        removeIndex = ArrayPop(toRemove);
        ArrayErase(this.m_queuedCombatExp, removeIndex);
        j += 1;
      };
      if expAwarded {
        this.AddExperience(Cast<Int32>(expAmount), IntEnum<gamedataProficiencyType>(i), telemetryLevelGainReason.Gameplay);
      };
      i += 1;
    };
  }

  private final const func CanGainNextProficiencyLevel(pIndex: Int32) -> Bool {
    if pIndex >= 0 {
      if this.GetProficiencyMaxLevel(this.m_proficiencies[pIndex].type) > this.m_proficiencies[pIndex].currentLevel {
        return true;
      };
      return false;
    };
    return false;
  }

  private final const func GetExperienceForNextLevel(type: gamedataProficiencyType) -> Int32 {
    let colName: CName;
    let curveSetName: CName;
    let pIndex: Int32;
    let val: Int32;
    let statDataSys: ref<StatsDataSystem> = GameInstance.GetStatsDataSystem(this.m_owner.GetGame());
    if this.IsProficiencyMaxLvl(type) {
      return -1;
    };
    pIndex = this.GetProficiencyIndexByType(type);
    if pIndex < 0 {
      return -1;
    };
    this.GetProficiencyExpCurveNames(type, curveSetName, colName);
    if Equals(curveSetName, n"None") || Equals(colName, n"None") {
      return -1;
    };
    val = Cast<Int32>(statDataSys.GetValueFromCurve(curveSetName, Cast<Float>(this.m_proficiencies[pIndex].currentLevel + 1), colName));
    return val;
  }

  public final const func GetExperiencePercentage() -> Int32 {
    let pIndex: Int32 = this.GetProficiencyIndexByType(gamedataProficiencyType.Level);
    let maxExp: Int32 = this.m_proficiencies[pIndex].currentExp + this.m_proficiencies[pIndex].expToLevel;
    let expPerc: Int32 = (this.m_proficiencies[pIndex].currentExp * 100) / maxExp;
    return expPerc;
  }

  private final const func AddLevels(type: gamedataProficiencyType, opt amount: Int32) -> Void {
    let i: Int32;
    let pIndex: Int32 = this.GetProficiencyIndexByType(type);
    if amount == 0 {
      amount = 1;
    };
    if pIndex >= 0 {
      i = 0;
      while i <= amount {
        this.AddExperience(this.GetRemainingExpForLevelUp(type), type, telemetryLevelGainReason.IsDebug);
        i += 1;
      };
    };
  }

  public final const func SetLevel(type: gamedataProficiencyType, lvl: Int32, telemetryGainReason: telemetryLevelGainReason, opt isDebug: Bool) -> Void {
    let i: Int32;
    let tempGainReason: telemetryLevelGainReason;
    let toAdd: Int32;
    let pIndex: Int32 = this.GetProficiencyIndexByType(type);
    if pIndex >= 0 && this.m_proficiencies[pIndex].currentLevel != lvl {
      if lvl < this.m_proficiencies[pIndex].currentLevel {
        this.ResetProficiencyLevel(type);
      };
      toAdd = lvl - this.m_proficiencies[pIndex].currentLevel;
      i = 0;
      while i < toAdd {
        tempGainReason = i == toAdd - 1 ? telemetryGainReason : telemetryLevelGainReason.Ignore;
        this.AddExperience(this.GetRemainingExpForLevelUp(type), type, tempGainReason, isDebug);
        i += 1;
      };
    };
  }

  public final func BumpNetrunnerMinigameLevel(value: Int32) -> Void {
    if value > this.m_highestCompletedMinigameLevel {
      this.m_highestCompletedMinigameLevel = value;
    };
  }

  public final const func IsProficiencyMaxLvl(type: gamedataProficiencyType) -> Bool {
    let pIndex: Int32 = this.GetProficiencyIndexByType(type);
    if pIndex != -1 && this.m_proficiencies[pIndex].currentLevel == this.GetProficiencyMaxLevel(type) {
      return true;
    };
    return false;
  }

  public final const func GetDevPoints(type: gamedataDevelopmentPointType) -> Int32 {
    return this.m_devPoints[this.GetDevPointsIndex(type)].unspent;
  }

  private final const func GetDevPointsIndex(type: gamedataDevelopmentPointType) -> Int32 {
    let i: Int32 = 0;
    while i < ArraySize(this.m_devPoints) {
      if Equals(this.m_devPoints[i].type, type) {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  private final func SetDevelopmentPoints() -> Void {
    let devPts: SDevelopmentPoints;
    let i: Int32 = 0;
    while i < 4 {
      if this.GetDevPointsIndex(IntEnum<gamedataDevelopmentPointType>(i)) < 0 {
        devPts.type = IntEnum<gamedataDevelopmentPointType>(i);
        devPts.spent = 0;
        devPts.unspent = 0;
        ArrayPush(this.m_devPoints, devPts);
      };
      i += 1;
    };
  }

  public final const func ModifyDevPoints(type: gamedataProficiencyType, level: Int32) -> Void {
    let i: Int32;
    let val: Int32 = 0;
    if Equals(type, gamedataProficiencyType.Espionage) {
      val = this.GetDevPointsForLevel(level, gamedataProficiencyType.Espionage, gamedataDevelopmentPointType.Espionage);
      this.AddDevelopmentPoints(val, gamedataDevelopmentPointType.Espionage);
    } else {
      i = 0;
      while i <= 4 {
        val = this.GetDevPointsForLevel(level, type, IntEnum<gamedataDevelopmentPointType>(i));
        if val > 0 {
          this.AddDevelopmentPoints(val, IntEnum<gamedataDevelopmentPointType>(i));
        };
        i += 1;
      };
    };
  }

  private final const func GetDevPointsForLevel(level: Int32, profType: gamedataProficiencyType, devPtsType: gamedataDevelopmentPointType) -> Int32 {
    let awardFloat: Float;
    let col: CName;
    let curve: CName;
    let statDataSys: ref<StatsDataSystem> = GameInstance.GetStatsDataSystem(this.m_owner.GetGame());
    let dIndex: Int32 = this.GetDevPointsIndex(devPtsType);
    if dIndex < 0 {
      return -1;
    };
    this.GetProficiencyExpCurveNames(profType, curve, col);
    if Equals(curve, n"None") || Equals(col, n"None") {
      return -1;
    };
    if Equals(profType, gamedataProficiencyType.Level) {
      if Equals(devPtsType, gamedataDevelopmentPointType.Espionage) {
        return -1;
      };
      if Equals(devPtsType, gamedataDevelopmentPointType.Attribute) && level % 2 == 0 {
        return 1;
      };
      awardFloat = statDataSys.GetValueFromCurve(n"player_levelToXP", Cast<Float>(level), n"perk_point_award_from_level");
      return Cast<Int32>(awardFloat);
    };
    if Equals(profType, gamedataProficiencyType.Espionage) && Equals(devPtsType, gamedataDevelopmentPointType.Espionage) {
      return 1;
    };
    return -1;
  }

  public final const func AddDevelopmentPoints(amount: Int32, type: gamedataDevelopmentPointType) -> Void {
    let dIndex: Int32 = this.GetDevPointsIndex(type);
    if dIndex < 0 {
      return;
    };
    this.m_devPoints[dIndex].unspent += amount;
  }

  private final const func SpendDevelopmentPoint(type: gamedataDevelopmentPointType) -> Void {
    let dIndex: Int32 = this.GetDevPointsIndex(type);
    if dIndex < 0 {
      return;
    };
    this.m_devPoints[dIndex].unspent -= 1;
    this.m_devPoints[dIndex].spent += 1;
  }

  private final const func SpendDevelopmentPoint(type: gamedataDevelopmentPointType, amount: Int32) -> Void {
    let dIndex: Int32 = this.GetDevPointsIndex(type);
    if dIndex < 0 {
      return;
    };
    this.m_devPoints[dIndex].unspent -= amount;
    this.m_devPoints[dIndex].spent += amount;
  }

  private final const func ResetDevelopmentPoints(type: gamedataDevelopmentPointType) -> Void {
    let dIndex: Int32 = this.GetDevPointsIndex(type);
    if dIndex < 0 {
      return;
    };
    this.m_devPoints[dIndex].unspent += this.m_devPoints[dIndex].spent;
    this.m_devPoints[dIndex].spent = 0;
  }

  public final const func ResetAllDevPoints() -> Void {
    let i: Int32 = 1;
    while i < ArraySize(this.m_devPoints) {
      this.ResetDevelopmentPoints(IntEnum<gamedataDevelopmentPointType>(i));
      i += 1;
    };
  }

  public final const func SetDevelopmentsPoint(type: gamedataDevelopmentPointType, value: Int32) -> Void {
    let dIndex: Int32 = this.GetDevPointsIndex(type);
    if dIndex < 0 {
      return;
    };
    this.m_devPoints[dIndex].unspent = value;
    this.m_devPoints[dIndex].spent = 0;
  }

  public final const func ClearAllDevPoints() -> Void {
    let i: Int32 = 0;
    while i < ArraySize(this.m_devPoints) {
      this.m_devPoints[i].spent = 0;
      this.m_devPoints[i].unspent = 0;
      i += 1;
    };
  }

  private final func InitializeAttributesData() -> Void {
    let attributeData: SAttributeData;
    let i: Int32 = 0;
    while i < 6 {
      attributeData.type = IntEnum<gamedataAttributeDataType>(i);
      ArrayClear(attributeData.unlockedPerks);
      ArrayPush(this.m_attributesData, attributeData);
      this.UnlockFreeNewPerks(attributeData.type);
      i += 1;
    };
  }

  public final func GetAttributePoints(attributeDataType: gamedataAttributeDataType) -> Int32 {
    let statType: gamedataStatType = PlayerDevelopmentData.AttributeDataTypeToStatType(attributeDataType);
    return this.m_attributes[this.GetAttributeIndex(statType)].value;
  }

  public final func HasEnoughtAttributePoints(perkType: gamedataNewPerkType) -> Bool {
    let newPerkRec: ref<NewPerk_Record> = RPGManager.GetNewPerkRecord(perkType);
    let tierRec: wref<NewPerkTier_Record> = newPerkRec.Tier();
    let requiredPoints: Int32 = tierRec.RequiredAttributePoints();
    let currentPoints: Int32 = this.GetAttributePoints(newPerkRec.Attribute().Type());
    return currentPoints >= requiredPoints;
  }

  public final func UnlockNewPerk(perkType: gamedataNewPerkType) -> Bool {
    let attributeDataType: gamedataAttributeDataType;
    let i: Int32;
    let j: Int32;
    let perk: SNewPerk;
    let perkRec: ref<NewPerk_Record>;
    if this.FindNewPerk(perkType, i, j) {
      return false;
    };
    perk.type = perkType;
    perk.currLevel = 0;
    perkRec = RPGManager.GetNewPerkRecord(perkType);
    attributeDataType = perkRec.Attribute().Type();
    ArrayPush(this.m_attributesData[EnumInt(attributeDataType)].unlockedPerks, perk);
    return true;
  }

  private final const func FindNewPerk(perkType: gamedataNewPerkType, out i: Int32, out j: Int32) -> Bool {
    let attributeData: SAttributeData;
    i = 0;
    while i < ArraySize(this.m_attributesData) {
      attributeData = this.m_attributesData[i];
      j = 0;
      while j < ArraySize(attributeData.unlockedPerks) {
        if Equals(attributeData.unlockedPerks[j].type, perkType) {
          return true;
        };
        j += 1;
      };
      i += 1;
    };
    return false;
  }

  public final func LockNewPerk(perkType: gamedataNewPerkType) -> Bool {
    let i: Int32;
    let j: Int32;
    if this.FindNewPerk(perkType, i, j) {
      ArrayErase(this.m_attributesData[i].unlockedPerks, j);
      return true;
    };
    return false;
  }

  public final const func IsNewPerkUnlocked(perkType: gamedataNewPerkType) -> Bool {
    let i: Int32;
    let j: Int32;
    return this.FindNewPerk(perkType, i, j);
  }

  private final const func GetNewPerkGLPackageTDBID(perkType: gamedataNewPerkType, perkLevel: Int32) -> TweakDBID {
    let gLPRec: wref<GameplayLogicPackage_Record>;
    let levelData: wref<NewPerkLevelData_Record>;
    let perkRec: ref<NewPerk_Record> = RPGManager.GetNewPerkRecord(perkType);
    if perkRec.GetLevelsCount() <= perkLevel {
      return TDBID.None();
    };
    levelData = perkRec.GetLevelsItem(perkLevel);
    gLPRec = levelData.DataPackage();
    return gLPRec.GetID();
  }

  private final const func ActivateNewPerk(perkType: gamedataNewPerkType, perkLevel: Int32) -> Void {
    let GLPS: ref<GameplayLogicPackageSystem> = GameInstance.GetGameplayLogicPackageSystem(this.m_owner.GetGame());
    let packageID: TweakDBID = this.GetNewPerkGLPackageTDBID(perkType, perkLevel);
    GLPS.ApplyPackage(this.m_owner, this.m_owner, packageID);
  }

  private final const func DeactivateNewPerk(perkType: gamedataNewPerkType, perkLevel: Int32) -> Void {
    let GLPS: ref<GameplayLogicPackageSystem> = GameInstance.GetGameplayLogicPackageSystem(this.m_owner.GetGame());
    let packageID: TweakDBID = this.GetNewPerkGLPackageTDBID(perkType, perkLevel);
    GLPS.RemovePackage(this.m_owner, packageID);
  }

  public final const func GetPerkAttribute(perkType: gamedataNewPerkType) -> wref<AttributeData_Record> {
    let perkRec: ref<NewPerk_Record> = RPGManager.GetNewPerkRecord(perkType);
    return perkRec.Attribute();
  }

  public final func UnlockFreeNewPerks(attributeDataType: gamedataAttributeDataType) -> Void {
    let checkPerk: wref<NewPerk_Record>;
    let currLevel: Int32;
    let i: Int32;
    let j: Int32;
    let jLimit: Int32;
    let maxLevel: Int32;
    let perks: array<wref<NewPerk_Record>>;
    let previousPerksUnlocked: Bool;
    let requiredPerks: array<wref<NewPerk_Record>>;
    let attribute: wref<AttributeData_Record> = RPGManager.GetAttributeDataRecord(attributeDataType);
    attribute.Perks(perks);
    i = 0;
    while i < ArraySize(perks) {
      checkPerk = perks[i];
      ArrayClear(requiredPerks);
      checkPerk.RequiresPerks(requiredPerks);
      previousPerksUnlocked = true;
      j = 0;
      jLimit = ArraySize(requiredPerks);
      while j < jLimit {
        currLevel = this.IsNewPerkBought(requiredPerks[j].Type());
        maxLevel = this.GetNewPerkMaxLevel(requiredPerks[j].Type());
        previousPerksUnlocked = currLevel == maxLevel && this.IsNewPerkUnlocked(requiredPerks[j].Type());
        if !previousPerksUnlocked {
          break;
        };
        j += 1;
      };
      if (previousPerksUnlocked || checkPerk.GetRequiresPerksCount() == 0) && this.HasEnoughtAttributePoints(checkPerk.Type()) {
        this.UnlockNewPerk(checkPerk.Type());
      };
      i += 1;
    };
  }

  public final func GetUnlockedPerkList(perkType: gamedataNewPerkType, out unlockedPerks: [gamedataNewPerkType]) -> Void {
    let attributeData: SAttributeData;
    let checkPerk: wref<NewPerk_Record>;
    let found: Bool;
    let i: Int32;
    let j: Int32;
    let k: Int32;
    let perks: array<wref<NewPerk_Record>>;
    let requiredPerks: array<wref<NewPerk_Record>>;
    let attribute: wref<AttributeData_Record> = this.GetPerkAttribute(perkType);
    attribute.Perks(perks);
    i = 0;
    while i < ArraySize(perks) {
      checkPerk = perks[i];
      ArrayClear(requiredPerks);
      checkPerk.RequiresPerks(requiredPerks);
      found = false;
      j = 0;
      while j < ArraySize(requiredPerks) && !found {
        if Equals(requiredPerks[j].Type(), perkType) {
          found = true;
        };
        j += 1;
      };
      if found {
        if ArraySize(requiredPerks) == 1 {
          ArrayPush(unlockedPerks, checkPerk.Type());
        } else {
          attributeData = this.m_attributesData[EnumInt(attribute.Type())];
          j = 0;
          while j < ArraySize(requiredPerks) && found {
            found = false;
            k = 0;
            while k < ArraySize(attributeData.unlockedPerks) && !found {
              if Equals(attributeData.unlockedPerks[k].type, requiredPerks[j].Type()) {
                if attributeData.unlockedPerks[k].currLevel > 0 && attributeData.unlockedPerks[k].currLevel == this.GetNewPerkMaxLevel(attributeData.unlockedPerks[k].type) {
                  found = true;
                };
              };
              k += 1;
            };
            j += 1;
          };
          if found {
            ArrayPush(unlockedPerks, checkPerk.Type());
          };
        };
      };
      i += 1;
    };
  }

  private final const func CanNewPerkBeBought(perkType: gamedataNewPerkType, isEspionagePerk: Bool, isEspionageMilestonePerk: Bool) -> Bool {
    let i: Int32;
    let j: Int32;
    let k: Int32;
    let kLimit: Int32;
    let menuNotification: ref<UIMenuNotificationEvent>;
    let newPerkRecord: wref<NewPerk_Record>;
    let primDevIndex: Int32;
    let requiredPerks: array<wref<NewPerk_Record>>;
    if !this.FindNewPerk(perkType, i, j) {
      return false;
    };
    if this.GetNewPerkMaxLevel(perkType) <= this.m_attributesData[i].unlockedPerks[j].currLevel {
      menuNotification = new UIMenuNotificationEvent();
      menuNotification.m_notificationType = UIMenuNotificationType.MaxLevelPerks;
      GameInstance.GetUISystem(this.m_owner.GetGame()).QueueEvent(menuNotification);
      return false;
    };
    primDevIndex = this.GetDevPointsIndex(isEspionagePerk ? gamedataDevelopmentPointType.Espionage : gamedataDevelopmentPointType.Primary);
    if primDevIndex < 0 {
      return false;
    };
    if this.m_devPoints[primDevIndex].unspent <= 0 || isEspionageMilestonePerk && this.m_devPoints[primDevIndex].unspent < 3 {
      menuNotification = new UIMenuNotificationEvent();
      menuNotification.m_notificationType = UIMenuNotificationType.NoPerksPoints;
      GameInstance.GetUISystem(this.m_owner.GetGame()).QueueEvent(menuNotification);
      return false;
    };
    newPerkRecord = RPGManager.GetNewPerkRecord(perkType);
    newPerkRecord.RequiresPerks(requiredPerks);
    k = 0;
    kLimit = ArraySize(requiredPerks);
    while k < kLimit {
      if this.FindNewPerk(requiredPerks[k].Type(), i, j) {
        if this.m_attributesData[i].unlockedPerks[j].currLevel < this.GetNewPerkMaxLevel(this.m_attributesData[i].unlockedPerks[j].type) {
          return false;
        };
      };
      k += 1;
    };
    return true;
  }

  public final func CheckIfAllnewPerkParentSold(perkType: gamedataNewPerkType) -> Bool {
    let i: Int32;
    let j: Int32;
    if this.FindNewPerk(perkType, i, j) {
      return this.CheckIfAllNewPerkParentSold(i, this.m_attributesData[i].unlockedPerks[j]);
    };
    return false;
  }

  private final func CheckIfAllNewPerkParentSold(attributeIdx: Int32, perkRecord: script_ref<SNewPerk>) -> Bool {
    let j: Int32;
    let jLimit: Int32;
    let newPerkRecord: wref<NewPerk_Record>;
    let requiredPerks: array<wref<NewPerk_Record>>;
    let attributeData: SAttributeData = this.m_attributesData[attributeIdx];
    let i: Int32 = 0;
    while i < ArraySize(attributeData.unlockedPerks) {
      ArrayClear(requiredPerks);
      newPerkRecord = RPGManager.GetNewPerkRecord(attributeData.unlockedPerks[i].type);
      newPerkRecord.RequiresPerks(requiredPerks);
      j = 0;
      jLimit = ArraySize(requiredPerks);
      while j < jLimit {
        if Equals(requiredPerks[j].Type(), Deref(perkRecord).type) && attributeData.unlockedPerks[i].currLevel > 0 {
          return false;
        };
        j += 1;
      };
      i += 1;
    };
    return true;
  }

  private final func LockAllNewPerkParents(attributeIdx: Int32, perkRecord: script_ref<SNewPerk>) -> Void {
    let j: Int32;
    let jLimit: Int32;
    let newPerkLockedEvent: ref<NewPerkLockedEvent>;
    let newPerkRecord: wref<NewPerk_Record>;
    let requiredPerks: array<wref<NewPerk_Record>>;
    let attributeData: SAttributeData = this.m_attributesData[attributeIdx];
    let i: Int32 = 0;
    while i < ArraySize(attributeData.unlockedPerks) {
      ArrayClear(requiredPerks);
      newPerkRecord = RPGManager.GetNewPerkRecord(attributeData.unlockedPerks[i].type);
      newPerkRecord.RequiresPerks(requiredPerks);
      j = 0;
      jLimit = ArraySize(requiredPerks);
      while j < jLimit {
        if Equals(requiredPerks[j].Type(), Deref(perkRecord).type) {
          this.LockNewPerk(attributeData.unlockedPerks[i].type);
          newPerkLockedEvent = new NewPerkLockedEvent();
          newPerkLockedEvent.perkType = attributeData.unlockedPerks[i].type;
          GameInstance.GetUISystem(this.m_owner.GetGame()).QueueEvent(newPerkLockedEvent);
        };
        j += 1;
      };
      i += 1;
    };
  }

  public final func BuyNewPerk(perkType: gamedataNewPerkType, opt forceBuy: Bool) -> Bool {
    let canBeBought: Bool;
    let currlevel: Int32;
    let devPoints: Int32;
    let i: Int32;
    let isEspionageMilestonePerk: Bool;
    let isEspionagePerk: Bool;
    let j: Int32;
    if this.FindNewPerk(perkType, i, j) {
      isEspionagePerk = Equals(this.m_attributesData[i].type, gamedataAttributeDataType.EspionageAttributeData);
      isEspionageMilestonePerk = this.IsEspionageMilestonePerk(this.m_attributesData[i].unlockedPerks[j].type);
      canBeBought = forceBuy || this.CanNewPerkBeBought(perkType, isEspionagePerk, isEspionageMilestonePerk);
      if canBeBought {
        currlevel = this.m_attributesData[i].unlockedPerks[j].currLevel;
        this.m_attributesData[i].unlockedPerks[j].currLevel += 1;
        this.ActivateNewPerk(perkType, currlevel);
        this.HandleAddingPerkLevel(i, j);
        if !forceBuy {
          if isEspionagePerk {
            devPoints = isEspionageMilestonePerk ? 3 : 1;
            this.SpendDevelopmentPoint(gamedataDevelopmentPointType.Espionage, devPoints);
            this.CheckRelicMasterAchievement();
          } else {
            this.SpendDevelopmentPoint(gamedataDevelopmentPointType.Primary);
          };
        };
        return true;
      };
    };
    return false;
  }

  public final func SellNewPerk(perkType: gamedataNewPerkType, out perkLevelSold: Int32) -> Bool {
    let i: Int32;
    let j: Int32;
    let previousPerksSold: Bool;
    perkLevelSold = 0;
    if this.FindNewPerk(perkType, i, j) {
      previousPerksSold = this.CheckIfAllNewPerkParentSold(i, this.m_attributesData[i].unlockedPerks[j]);
      if previousPerksSold && this.m_attributesData[i].unlockedPerks[j].currLevel > 0 {
        this.LockAllNewPerkParents(i, this.m_attributesData[i].unlockedPerks[j]);
        this.FindNewPerk(perkType, i, j);
        perkLevelSold = this.HandleRemovingPerkLevel(i, j);
        return true;
      };
    };
    return false;
  }

  public final func ForceSellNewPerk(perkType: gamedataNewPerkType, out perkLevelSold: Int32) -> Bool {
    let i: Int32;
    let j: Int32;
    perkLevelSold = 0;
    if this.FindNewPerk(perkType, i, j) {
      if this.m_attributesData[i].unlockedPerks[j].currLevel > 0 {
        perkLevelSold = this.HandleRemovingPerkLevel(i, j);
        return true;
      };
    };
    return false;
  }

  private final func HandleAddingPerkLevel(i: Int32, j: Int32) -> Void {
    if Equals(this.m_attributesData[i].unlockedPerks[j].type, gamedataNewPerkType.Tech_Central_Milestone_3) && this.m_attributesData[i].unlockedPerks[j].currLevel == 3 {
      PowerUpCyberwareEffector.PowerUpCyberwareInSlot(this.m_owner, gamedataEquipmentArea.MusculoskeletalSystemCW, 0);
      PowerUpCyberwareEffector.PowerUpCyberwareInSlot(this.m_owner, gamedataEquipmentArea.MusculoskeletalSystemCW, 1);
      PowerUpCyberwareEffector.PowerUpCyberwareInSlot(this.m_owner, gamedataEquipmentArea.MusculoskeletalSystemCW, 2);
    };
  }

  private final func HandleRemovingTech_Central_Milestone_3(playerData: ref<EquipmentSystemPlayerData>) -> Void {
    let equipRequest: ref<EquipRequest>;
    let i: Int32;
    let itemID: ItemID;
    let itemList: array<wref<gameItemData>>;
    let j: Int32;
    let partData: InnerItemData;
    let rngSeed: Uint32;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let slotCount: Int32 = playerData.GetNumberOfSlots(gamedataEquipmentArea.MusculoskeletalSystemCW);
    let statsShardSlotTDBID: TweakDBID = t"AttachmentSlots.StatsShardSlot";
    if PlayerDevelopmentSystem.GetData(this.m_owner).IsNewPerkBought(gamedataNewPerkType.Tech_Central_Milestone_3) < 2 {
      return;
    };
    transactionSystem.GetItemList(this.m_owner, itemList);
    i = 0;
    while i < slotCount {
      itemID = playerData.GetItemInEquipSlot(gamedataEquipmentArea.MusculoskeletalSystemCW, i);
      if !ItemID.IsValid(itemID) {
      } else {
        EquipmentSystem.RequestUnequipItem(this.m_owner, gamedataEquipmentArea.MusculoskeletalSystemCW, i);
        j = 0;
        while j < ArraySize(itemList) {
          if itemList[j].GetID() != itemID && Equals(TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemList[j].GetID())).Quality().Type(), TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID)).Quality().Type()) && Equals(TDB.GetCName(ItemID.GetTDBID(itemList[j].GetID()) + t".cyberwareType"), TDB.GetCName(ItemID.GetTDBID(itemID) + t".cyberwareType")) {
            transactionSystem.GetItemData(this.m_owner, itemList[j].GetID()).GetItemPart(partData, statsShardSlotTDBID);
            rngSeed = ItemID.GetRngSeed(InnerItemData.GetItemID(partData));
            transactionSystem.GetItemData(this.m_owner, itemID).GetItemPart(partData, statsShardSlotTDBID);
            if rngSeed == ItemID.GetRngSeed(InnerItemData.GetItemID(partData)) {
              equipRequest = new EquipRequest();
              equipRequest.itemID = itemList[j].GetID();
              equipRequest.owner = this.m_owner;
              equipRequest.addToInventory = false;
              EquipmentSystem.GetInstance(equipRequest.owner).QueueRequest(equipRequest);
              break;
            };
          };
          j += 1;
        };
      };
      i += 1;
    };
    EquipmentSystem.RequestUnequipItem(this.m_owner, gamedataEquipmentArea.MusculoskeletalSystemCW, 2);
  }

  private final func HandleRemovingPerkLevel(i: Int32, j: Int32) -> Int32 {
    let inventoryIndex: Int32;
    let inventoryItems: array<InventoryItemData>;
    let inventoryManager: ref<InventoryDataManagerV2>;
    let itemID: ItemID;
    let maxSlots: Int32;
    let removePartRequest: ref<RemoveItemPart>;
    let slotIdx: Int32;
    let perkType: gamedataNewPerkType = this.m_attributesData[i].unlockedPerks[j].type;
    let equipmentSystem: ref<EquipmentSystem> = EquipmentSystem.GetInstance(this.m_owner);
    let playerData: ref<EquipmentSystemPlayerData> = equipmentSystem.GetPlayerData(this.m_owner);
    let perkLevelSold: Int32 = this.m_attributesData[i].unlockedPerks[j].currLevel;
    this.m_attributesData[i].unlockedPerks[j].currLevel -= 1;
    this.DeactivateNewPerk(perkType, this.m_attributesData[i].unlockedPerks[j].currLevel);
    if Equals(perkType, gamedataNewPerkType.Tech_Central_Milestone_3) {
      this.HandleRemovingTech_Central_Milestone_3(playerData);
    };
    if Equals(perkType, gamedataNewPerkType.Tech_Central_Perk_3_2) {
      EquipmentSystem.RequestUnequipItem(this.m_owner, gamedataEquipmentArea.HandsCW, 1);
    };
    if Equals(perkType, gamedataNewPerkType.Tech_Central_Perk_3_3) {
      maxSlots = playerData.GetNumberOfSlots(gamedataEquipmentArea.IntegumentarySystemCW);
      slotIdx = 0;
      while slotIdx < maxSlots {
        itemID = playerData.GetItemInEquipSlot(gamedataEquipmentArea.IntegumentarySystemCW, slotIdx);
        if ItemID.IsValid(itemID) && RPGManager.IsItemAdaptiveStemCells(ItemID.GetTDBID(itemID)) {
          EquipmentSystem.RequestUnequipItem(this.m_owner, gamedataEquipmentArea.IntegumentarySystemCW, slotIdx);
        };
        slotIdx += 1;
      };
    };
    if Equals(perkType, gamedataNewPerkType.Espionage_Central_Milestone_1) {
      itemID = playerData.GetItemInEquipSlot(gamedataEquipmentArea.ArmsCW, slotIdx);
      if ItemID.IsValid(itemID) {
        inventoryManager = new InventoryDataManagerV2();
        inventoryManager.Initialize(this.m_owner as PlayerPuppet);
        inventoryItems = inventoryManager.GetPlayerInventoryData(gamedataEquipmentArea.ArmsCW);
        inventoryIndex = 0;
        while inventoryIndex < ArraySize(inventoryItems) {
          if Equals(InventoryItemData.GetItemType(inventoryItems[inventoryIndex]), gamedataItemType.Cyb_NanoWires) && inventoryItems[inventoryIndex].ID == itemID {
            removePartRequest = new RemoveItemPart();
            removePartRequest.obj = this.m_owner;
            removePartRequest.baseItem = itemID;
            removePartRequest.slotToEmpty = t"AttachmentSlots.NanoWiresQuickhackSlot";
            GameInstance.GetScriptableSystemsContainer(this.m_owner.GetGame()).Get(n"ItemModificationSystem").QueueRequest(removePartRequest);
          };
          inventoryIndex += 1;
        };
      };
    };
    this.ReturnDevelopmentPointForSoldPerk(i, j);
    return perkLevelSold;
  }

  private final func ReturnDevelopmentPointForSoldPerk(i: Int32, j: Int32) -> Void {
    let devPoints: Int32;
    let isEspionagePerk: Bool = Equals(this.m_attributesData[i].type, gamedataAttributeDataType.EspionageAttributeData);
    let isEspionageMilestonePerk: Bool = this.IsEspionageMilestonePerk(this.m_attributesData[i].unlockedPerks[j].type);
    if isEspionagePerk {
      devPoints = isEspionageMilestonePerk ? 3 : 1;
      this.AddDevelopmentPoints(devPoints, gamedataDevelopmentPointType.Espionage);
    } else {
      this.AddDevelopmentPoints(1, gamedataDevelopmentPointType.Primary);
    };
  }

  public final func ResetNewPerks() -> Void {
    let newPerkLockedEvent: ref<NewPerkLockedEvent>;
    let newPerkSoldEvent: ref<NewPerkSoldEvent>;
    let perkLevelSold: Int32;
    let perkType: gamedataNewPerkType;
    let developmentData: ref<PlayerDevelopmentData> = PlayerDevelopmentSystem.GetData(this.m_owner);
    let perkIndex: Int32 = 0;
    while perkIndex < 189 {
      perkType = IntEnum<gamedataNewPerkType>(perkIndex);
      while developmentData.ForceSellNewPerk(perkType, perkLevelSold) {
        GameInstance.GetTelemetrySystem(this.m_owner.GetGame()).LogNewPerkRemoved(perkType);
        newPerkSoldEvent = new NewPerkSoldEvent();
        newPerkSoldEvent.perkType = perkType;
        newPerkSoldEvent.perkLevelSold = perkLevelSold;
        GameInstance.GetUISystem(this.m_owner.GetGame()).QueueEvent(newPerkSoldEvent);
        this.m_owner.QueueEvent(newPerkSoldEvent);
      };
      this.LockNewPerk(perkType);
      newPerkLockedEvent = new NewPerkLockedEvent();
      newPerkLockedEvent.perkType = perkType;
      GameInstance.GetUISystem(this.m_owner.GetGame()).QueueEvent(newPerkLockedEvent);
      perkIndex += 1;
    };
  }

  public final const func IsNewPerkBought(perkType: gamedataNewPerkType) -> Int32 {
    let i: Int32;
    let j: Int32;
    if this.FindNewPerk(perkType, i, j) {
      return this.m_attributesData[i].unlockedPerks[j].currLevel;
    };
    return 0;
  }

  public final const func IsNewPerkBoughtAnyLevel(perkType: gamedataNewPerkType) -> Bool {
    return this.IsNewPerkBought(perkType) > 0;
  }

  public final const func GetNewPerkMaxLevel(perkType: gamedataNewPerkType) -> Int32 {
    let i: Int32;
    let j: Int32;
    if this.FindNewPerk(perkType, i, j) {
      return RPGManager.GetNewPerkRecord(perkType).GetLevelsCount();
    };
    return 0;
  }

  public final const func IsNewPerkEspionage(perkType: gamedataNewPerkType) -> Bool {
    let i: Int32;
    let j: Int32;
    if this.FindNewPerk(perkType, i, j) {
      return Equals(this.m_attributesData[i].type, gamedataAttributeDataType.EspionageAttributeData);
    };
    return false;
  }

  public final const func IsEspionageMilestonePerk(type: gamedataNewPerkType) -> Bool {
    return Equals(type, gamedataNewPerkType.Espionage_Central_Milestone_1) || Equals(type, gamedataNewPerkType.Espionage_Left_Milestone_Perk) || Equals(type, gamedataNewPerkType.Espionage_Right_Milestone_1);
  }

  private final const func GetEspionagePerksCount() -> Int32 {
    let espionageAttributeData: wref<AttributeData_Record> = TweakDBInterface.GetAttributeDataRecord(t"NewPerks.EspionageAttributeData");
    return espionageAttributeData.GetPerksCount();
  }

  private final func GetAttributeData(type: gamedataAttributeDataType) -> SAttributeData {
    return this.m_attributesData[EnumInt(type)];
  }

  public final static func StatTypeToAttributeDataType(type: gamedataStatType) -> gamedataAttributeDataType {
    switch type {
      case gamedataStatType.Strength:
        return gamedataAttributeDataType.BodyAttributeData;
      case gamedataStatType.Intelligence:
        return gamedataAttributeDataType.IntelligenceAttributeData;
      case gamedataStatType.Cool:
        return gamedataAttributeDataType.CoolAttributeData;
      case gamedataStatType.TechnicalAbility:
        return gamedataAttributeDataType.TechnicalAbilityAttributeData;
      case gamedataStatType.Reflexes:
        return gamedataAttributeDataType.ReflexesAttributeData;
      case gamedataStatType.Espionage:
        return gamedataAttributeDataType.EspionageAttributeData;
      default:
        return gamedataAttributeDataType.Invalid;
    };
  }

  public final static func AttributeDataTypeToStatType(type: gamedataAttributeDataType) -> gamedataStatType {
    switch type {
      case gamedataAttributeDataType.BodyAttributeData:
        return gamedataStatType.Strength;
      case gamedataAttributeDataType.IntelligenceAttributeData:
        return gamedataStatType.Intelligence;
      case gamedataAttributeDataType.CoolAttributeData:
        return gamedataStatType.Cool;
      case gamedataAttributeDataType.TechnicalAbilityAttributeData:
        return gamedataStatType.TechnicalAbility;
      case gamedataAttributeDataType.ReflexesAttributeData:
        return gamedataStatType.Reflexes;
      case gamedataAttributeDataType.EspionageAttributeData:
        return gamedataStatType.Espionage;
      default:
        return gamedataStatType.Invalid;
    };
  }

  private final func GetAttributeData(statType: gamedataStatType, out attrData: SAttributeData) -> Bool {
    let dataType: gamedataAttributeDataType = PlayerDevelopmentData.StatTypeToAttributeDataType(statType);
    if Equals(dataType, gamedataAttributeDataType.Invalid) {
      return false;
    };
    attrData = this.m_attributesData[EnumInt(dataType)];
    return true;
  }

  private final func InitializePerkAreas() -> Void {
    let i: Int32 = 0;
    while i <= 116 {
      this.InitializePerkArea(IntEnum<gamedataPerkArea>(i));
      i += 1;
    };
  }

  private final func InitializePerkArea(areaType: gamedataPerkArea) -> Void {
    let newPerkArea: SPerkArea;
    if this.IsPerkAreaValid(areaType) {
      newPerkArea.type = areaType;
      newPerkArea.unlocked = this.ShouldPerkAreaBeAvailable(areaType);
      ArrayClear(newPerkArea.boughtPerks);
      ArrayPush(this.m_perkAreas, newPerkArea);
    };
  }

  private final const func InitializePerk(perkType: gamedataPerkType) -> SPerk {
    let newPerk: SPerk;
    newPerk.type = perkType;
    newPerk.currLevel = 0;
    return newPerk;
  }

  private final const func IncreasePerkLevel(areaIndex: Int32, perkIndex: Int32) -> Void {
    this.m_perkAreas[areaIndex].boughtPerks[perkIndex].currLevel += 1;
  }

  public final const func RefreshPerkAreas() -> Void {
    let i: Int32 = 0;
    while i < ArraySize(this.m_proficiencies) {
      this.EvaluatePerkAreas(this.m_proficiencies[i].type);
      i += 1;
    };
  }

  private final const func EvaluatePerkAreas(prof: gamedataProficiencyType) -> Void {
    let aIndex: Int32;
    let i: Int32;
    let perkAreas: array<wref<PerkArea_Record>>;
    let pIndex: Int32 = this.GetProficiencyIndexByType(prof);
    if pIndex < 0 {
      return;
    };
    RPGManager.GetProficiencyRecord(prof).PerkAreas(perkAreas);
    i = 0;
    while i < ArraySize(perkAreas) {
      aIndex = this.GetPerkAreaIndex(perkAreas[i].Type());
      if aIndex < 0 {
      } else {
        this.m_perkAreas[aIndex].unlocked = this.IsPerkAreaReqMet(perkAreas[i]);
      };
      i += 1;
    };
  }

  public final const func GetPerkAreaIndex(areaType: gamedataPerkArea) -> Int32 {
    let i: Int32 = 0;
    while i < ArraySize(this.m_perkAreas) {
      if Equals(this.m_perkAreas[i].type, areaType) {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  private final const func GetPerkIndex(areaIndex: Int32, perkType: gamedataPerkType) -> Int32 {
    let i: Int32 = 0;
    while i < ArraySize(this.m_perkAreas[areaIndex].boughtPerks) {
      if Equals(this.m_perkAreas[areaIndex].boughtPerks[i].type, perkType) {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  public final const func GetPerkIndex(areaType: gamedataPerkArea, perkType: gamedataPerkType) -> Int32 {
    let i: Int32;
    let pAreaIndex: Int32 = this.GetPerkAreaIndex(areaType);
    if pAreaIndex < 0 {
      return -1;
    };
    i = 0;
    while i < ArraySize(this.m_perkAreas[pAreaIndex].boughtPerks) {
      if Equals(this.m_perkAreas[pAreaIndex].boughtPerks[i].type, perkType) {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  private final const func GetPerkIndex(perkType: gamedataPerkType) -> Int32 {
    let pIndex: Int32 = this.GetPerkIndex(this.GetPerkAreaFromPerk(perkType), perkType);
    return pIndex;
  }

  public final const func UnlockPerkArea(areaType: gamedataPerkArea) -> Void {
    let pAreaIndex: Int32 = this.GetPerkAreaIndex(areaType);
    if pAreaIndex < 0 {
      return;
    };
    this.m_perkAreas[pAreaIndex].unlocked = true;
  }

  public final const func LockPerkArea(areaType: gamedataPerkArea) -> Void {
    let pAreaIndex: Int32 = this.GetPerkAreaIndex(areaType);
    if pAreaIndex < 0 {
      return;
    };
    this.m_perkAreas[pAreaIndex].unlocked = false;
  }

  public final const func BuyPerk(perkType: gamedataPerkType) -> Bool {
    let canBeBought: Bool;
    let newPerk: SPerk;
    let pIndex: Int32;
    let profIndex: Int32;
    let pAreaIndex: Int32 = this.GetPerkAreaIndex(this.GetPerkAreaFromPerk(perkType));
    if pAreaIndex < 0 {
      return false;
    };
    pIndex = this.GetPerkIndex(perkType);
    canBeBought = this.CanPerkBeBought(perkType);
    if pIndex < 0 && canBeBought {
      newPerk = this.InitializePerk(perkType);
      ArrayPush(this.m_perkAreas[pAreaIndex].boughtPerks, newPerk);
      pIndex = this.GetPerkIndex(perkType);
    };
    if !this.IsPerkMaxLevel(perkType) && canBeBought {
      profIndex = this.GetProficiencyIndexFromPerkArea(this.m_perkAreas[pAreaIndex].type);
      this.DeactivatePerkLevelData(pAreaIndex, pIndex);
      this.IncreasePerkLevel(pAreaIndex, pIndex);
      this.ActivatePerkLevelData(pAreaIndex, pIndex);
      this.SpendDevelopmentPoint(gamedataDevelopmentPointType.Primary);
      this.m_proficiencies[profIndex].spentPerkPoints += 1;
      this.EvaluatePerkAreas(this.m_proficiencies[profIndex].type);
      return true;
    };
    return false;
  }

  public final const func RemovePerk(perkType: gamedataPerkType) -> Bool {
    let currentPerkLevel: Int32;
    let dIndex: Int32;
    let pIndex: Int32;
    let profIndex: Int32;
    let tempPDevPts: Int32;
    let areaType: gamedataPerkArea = this.GetPerkAreaFromPerk(perkType);
    let pAreaIndex: Int32 = this.GetPerkAreaIndex(areaType);
    if pAreaIndex < 0 {
      return false;
    };
    pIndex = this.GetPerkIndex(perkType);
    if pIndex >= 0 {
      this.DeactivatePerkLevelData(pAreaIndex, pIndex);
      currentPerkLevel = this.m_perkAreas[pAreaIndex].boughtPerks[pIndex].currLevel;
      tempPDevPts = Cast<Int32>(GameInstance.GetStatsDataSystem(this.m_owner.GetGame()).GetValueFromCurve(this.GetPerkAreaRecord(areaType).Curve().CurveSetName(), Cast<Float>(currentPerkLevel), n"Primary"));
      if tempPDevPts > 0 {
        profIndex = this.GetProficiencyIndexFromPerkArea(this.m_perkAreas[pAreaIndex].type);
        dIndex = this.GetDevPointsIndex(gamedataDevelopmentPointType.Primary);
        this.m_devPoints[dIndex].unspent += tempPDevPts;
        this.m_devPoints[dIndex].spent -= tempPDevPts;
        this.m_proficiencies[profIndex].spentPerkPoints -= tempPDevPts;
        this.RemovePerkRecipes(perkType);
      };
      ArrayErase(this.m_perkAreas[pAreaIndex].boughtPerks, pIndex);
      return true;
    };
    return false;
  }

  public final const func RemoveAllPerks(free: Bool) -> Void {
    let i: Int32;
    let perkResetEvent: ref<PerkResetEvent>;
    let perkType: gamedataPerkType;
    let perksRemoved: array<gamedataPerkType>;
    let respecCost: Int32 = 0;
    if !free {
      respecCost = this.GetTotalRespecCost();
      GameInstance.GetTransactionSystem(this.m_owner.GetGame()).RemoveItem(this.m_owner, MarketSystem.Money(), respecCost);
    };
    i = 0;
    while i < 228 {
      perkType = IntEnum<gamedataPerkType>(i);
      if this.HasPerk(perkType) {
        this.RemovePerk(perkType);
        ArrayPush(perksRemoved, perkType);
      };
      i += 1;
    };
    i = 0;
    while i < 12 {
      this.RemoveTrait(IntEnum<gamedataTraitType>(i));
      i += 1;
    };
    GameInstance.GetTelemetrySystem(this.m_owner.GetGame()).LogPerksRemoved(respecCost, perksRemoved);
    perkResetEvent = new PerkResetEvent();
    GameInstance.GetUISystem(this.m_owner.GetGame()).QueueEvent(perkResetEvent);
  }

  public final const func RemoveDeprecatedPerkPoints() -> Void {
    let bonusEffectorRecord: ref<Effector_Record>;
    let bonusRecord: wref<PassiveProficiencyBonus_Record>;
    let currLevel: Int32;
    let profIndex: Int32;
    let profRecord: ref<Proficiency_Record>;
    let i: Int32 = 0;
    let deprecatedPoints: Int32 = 0;
    while i < ArraySize(this.m_proficiencies) {
      profIndex = this.GetProficiencyIndexByType(this.m_proficiencies[i].type);
      profRecord = this.GetProficiencyRecordByIndex(profIndex);
      if !profRecord.IsA(n"gamedataNewSkillsProficiency_Record") {
        currLevel = 0;
        while currLevel < this.m_proficiencies[i].currentLevel && currLevel < profRecord.GetPassiveBonusesCount() {
          bonusRecord = profRecord.GetPassiveBonusesItem(currLevel);
          bonusEffectorRecord = bonusRecord.EffectorToTrigger();
          if bonusEffectorRecord.IsA(n"gamedataAddDevelopmentPointEffector_Record") {
            deprecatedPoints += 1;
          };
          currLevel += 1;
        };
      };
      i += 1;
    };
    this.AddDevelopmentPoints(-deprecatedPoints, gamedataDevelopmentPointType.Primary);
  }

  public final const func GetTotalRespecCost() -> Int32 {
    let basePrice: Int32 = Cast<Int32>(TweakDBInterface.GetConstantStatModifierRecord(t"Price.RespecBase").Value());
    let singlePerkPrice: Int32 = Cast<Int32>(TweakDBInterface.GetConstantStatModifierRecord(t"Price.RespecSinglePerk").Value());
    let cost: Int32 = basePrice + singlePerkPrice * (this.GetSpentPerkPoints() + this.GetSpentTraitPoints());
    return cost;
  }

  public final func CheckPlayerRespecCost() -> Bool {
    let resetCost: Int32 = this.GetTotalRespecCost();
    let userMoney: Int32 = GameInstance.GetTransactionSystem(this.m_owner.GetGame()).GetItemQuantity(this.m_owner, MarketSystem.Money());
    return userMoney >= resetCost;
  }

  private final const func RemovePerkRecipes(perkType: gamedataPerkType) -> Void {
    let addItemsEffector: ref<AddItemsEffector_Record>;
    let craftingSystem: ref<CraftingSystem>;
    let hideRecipeRequest: ref<HideRecipeRequest>;
    let i: Int32;
    let itemsToAdd: array<wref<InventoryItem_Record>>;
    let j: Int32;
    let k: Int32;
    let perkLevelEffectors: array<wref<Effector_Record>>;
    let perkLevels: array<wref<PerkLevelData_Record>>;
    this.GetPerkRecord(perkType).Levels(perkLevels);
    i = 0;
    while i < ArraySize(perkLevels) {
      perkLevels[i].DataPackage().Effectors(perkLevelEffectors);
      j = 0;
      while j < ArraySize(perkLevelEffectors) {
        addItemsEffector = perkLevelEffectors[j] as AddItemsEffector_Record;
        if IsDefined(addItemsEffector) {
          addItemsEffector.ItemsToAdd(itemsToAdd);
          k = 0;
          while k < ArraySize(itemsToAdd) {
            craftingSystem = GameInstance.GetScriptableSystemsContainer(this.m_owner.GetGame()).Get(n"CraftingSystem") as CraftingSystem;
            hideRecipeRequest = new HideRecipeRequest();
            hideRecipeRequest.recipe = (itemsToAdd[k].Item() as ItemRecipe_Record).CraftingResult().Item().GetID();
            craftingSystem.QueueRequest(hideRecipeRequest);
            k += 1;
          };
        };
        j += 1;
      };
      i += 1;
    };
  }

  public final const func GetPerkRecord(perkType: gamedataPerkType) -> ref<Perk_Record> {
    return TweakDBInterface.GetPerkRecord(TDBID.Create("Perks." + EnumValueToString("gamedataPerkType", Cast<Int64>(EnumInt(perkType)))));
  }

  public final const func GetPerkAreaRecord(areaType: gamedataPerkArea) -> ref<PerkArea_Record> {
    return TweakDBInterface.GetPerkAreaRecord(TDBID.Create("Perks." + EnumValueToString("gamedataPerkArea", Cast<Int64>(EnumInt(areaType)))));
  }

  public final const func GetPerkPackageTDBID(areaIndex: Int32, perkIndex: Int32) -> TweakDBID {
    let packageID: TweakDBID = this.GetPerkPackageTDBID(this.m_perkAreas[areaIndex].boughtPerks[perkIndex].type);
    return packageID;
  }

  public final const func GetPerkPackageTDBID(perkType: gamedataPerkType) -> TweakDBID {
    let levelsData: array<wref<PerkLevelData_Record>>;
    let pAreaIndex: Int32;
    let pIndex: Int32;
    let packageID: TweakDBID;
    this.GetPerkRecord(perkType).Levels(levelsData);
    pAreaIndex = this.GetPerkAreaIndex(this.GetPerkAreaFromPerk(perkType));
    if pAreaIndex < 0 {
      return TDBID.None();
    };
    pIndex = this.GetPerkIndex(pAreaIndex, perkType);
    if pIndex < 0 {
      return TDBID.None();
    };
    packageID = levelsData[this.m_perkAreas[pAreaIndex].boughtPerks[pIndex].currLevel - 1].DataPackage().GetID();
    return packageID;
  }

  public final const func HasPerk(perkType: gamedataPerkType) -> Bool {
    let pIndex: Int32 = this.GetPerkIndex(this.GetPerkAreaFromPerk(perkType), perkType);
    return pIndex >= 0;
  }

  public final const func GetInvestedPerkPoints(profType: gamedataProficiencyType) -> Int32 {
    let pIndex: Int32 = this.GetProficiencyIndexByType(profType);
    return this.m_proficiencies[pIndex].spentPerkPoints;
  }

  public final const func ShouldPerkAreaBeAvailable(areaType: gamedataPerkArea) -> Bool {
    let areaRecord: ref<PerkArea_Record> = this.GetPerkAreaRecord(areaType);
    if TDBID.IsValid(areaRecord.GetID()) {
      if this.IsPerkAreaReqMet(areaRecord) {
        return true;
      };
    };
    return false;
  }

  public final const func IsPerkAreaUnlocked(area: gamedataPerkArea) -> Bool {
    let aIndex: Int32 = this.GetPerkAreaIndex(area);
    if aIndex < 0 {
      return false;
    };
    return this.m_perkAreas[aIndex].unlocked;
  }

  public final const func IsPerkAreaUnlocked(aIndex: Int32) -> Bool {
    if aIndex < 0 {
      return false;
    };
    return this.m_perkAreas[aIndex].unlocked;
  }

  private final const func IsPerkAreaValid(areaType: gamedataPerkArea) -> Bool {
    let i: Int32 = 0;
    while i < 116 {
      if Equals(areaType, IntEnum<gamedataPerkArea>(i)) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final const func IsPerkMaxLevel(perkType: gamedataPerkType) -> Bool {
    return this.GetPerkLevel(perkType) >= this.GetPerkMaxLevel(perkType);
  }

  public final const func GetPerkMaxLevel(perkType: gamedataPerkType) -> Int32 {
    return this.GetPerkAreaRecord(this.GetPerkAreaFromPerk(perkType)).MaxLevel();
  }

  public final const func GetPerkLevel(perkType: gamedataPerkType) -> Int32 {
    let pIndex: Int32;
    let pAreaIndex: Int32 = this.GetPerkAreaIndex(this.GetPerkAreaFromPerk(perkType));
    if pAreaIndex < 0 {
      return -1;
    };
    pIndex = this.GetPerkIndex(pAreaIndex, perkType);
    if pIndex < 0 {
      return -1;
    };
    return this.m_perkAreas[pAreaIndex].boughtPerks[pIndex].currLevel;
  }

  private final const func CanPerkBeBought(perkType: gamedataPerkType) -> Bool {
    let pIndex: Int32;
    let primDevIndex: Int32;
    let pAreaIndex: Int32 = this.GetPerkAreaIndex(this.GetPerkAreaFromPerk(perkType));
    if pAreaIndex < 0 {
      return false;
    };
    pIndex = this.GetPerkIndex(pAreaIndex, perkType);
    if pIndex >= 0 && this.GetPerkMaxLevel(perkType) <= this.m_perkAreas[pAreaIndex].boughtPerks[pIndex].currLevel {
      return false;
    };
    primDevIndex = this.GetDevPointsIndex(gamedataDevelopmentPointType.Primary);
    if primDevIndex < 0 {
      return false;
    };
    if this.m_devPoints[primDevIndex].unspent <= 0 {
      return false;
    };
    return true;
  }

  public final const func CanTraitBeBought() -> Bool {
    let primDevIndex: Int32 = this.GetDevPointsIndex(gamedataDevelopmentPointType.Primary);
    if primDevIndex < 0 {
      return false;
    };
    if this.m_devPoints[primDevIndex].unspent <= 0 {
      return false;
    };
    return true;
  }

  public final const func IsPerkAreaReqMet(areaRecord: ref<PerkArea_Record>) -> Bool {
    return this.IsPerkAreaBaseReqMet(areaRecord);
  }

  public final const func IsPerkAreaMasteryReqMet(areaRecord: ref<PerkArea_Record>) -> Bool {
    let prereqID: TweakDBID = areaRecord.MasteryLevel().GetID();
    let prereq: ref<StatPrereq> = IPrereq.CreatePrereq(prereqID) as StatPrereq;
    return prereq.IsFulfilled(this.m_owner.GetGame(), this.m_owner);
  }

  private final const func IsPerkAreaBaseReqMet(areaRecord: ref<PerkArea_Record>) -> Bool {
    let prereqID: TweakDBID = areaRecord.Requirement().GetID();
    let prereq: ref<IPrereq> = IPrereq.CreatePrereq(prereqID);
    return prereq.IsFulfilled(this.m_owner.GetGame(), this.m_owner);
  }

  public final const func GetRemainingRequiredPerkPoints(areaRecord: ref<PerkArea_Record>, out amount: Int32) -> Bool {
    let invested: Int32;
    let required: Int32;
    let prereq: ref<InvestedPerksPrereq> = IPrereq.CreatePrereq(areaRecord.Requirement().GetID()) as InvestedPerksPrereq;
    if IsDefined(prereq) {
      required = prereq.GetRequiredAmount();
      invested = this.GetInvestedPerkPoints(prereq.GetProficiencyType());
      amount = required - invested;
      if amount > 0 {
        return true;
      };
      return false;
    };
    amount = -1;
    return false;
  }

  public final const func GetRemainingRequiredPerkPoints(traitRecord: ref<Trait_Record>, out amount: Int32) -> Bool {
    let invested: Int32;
    let required: Int32;
    let prereq: ref<InvestedPerksPrereq> = IPrereq.CreatePrereq(traitRecord.Requirement().GetID()) as InvestedPerksPrereq;
    if IsDefined(prereq) {
      required = prereq.GetRequiredAmount();
      invested = this.GetInvestedPerkPoints(prereq.GetProficiencyType());
      amount = required - invested;
      if amount > 0 {
        return true;
      };
      return false;
    };
    amount = -1;
    return false;
  }

  public final const func GetPerkAreaFromPerk(perkType: gamedataPerkType) -> gamedataPerkArea {
    let areaString: String = EnumValueToString("gamedataPerkType", Cast<Int64>(EnumInt(perkType)));
    areaString = StrBeforeFirst(areaString, "_Perk");
    return IntEnum<gamedataPerkArea>(Cast<Int32>(EnumValueFromString("gamedataPerkArea", areaString)));
  }

  public final const func GetProficiencyFromPerkArea(perkArea: gamedataPerkArea) -> gamedataProficiencyType {
    let proficiencyString: String = EnumValueToString("gamedataPerkArea", Cast<Int64>(EnumInt(perkArea)));
    proficiencyString = StrBeforeFirst(proficiencyString, "_Area");
    return IntEnum<gamedataProficiencyType>(Cast<Int32>(EnumValueFromString("gamedataProficiencyType", proficiencyString)));
  }

  public final const func GetProficiencyIndexFromPerkArea(perkArea: gamedataPerkArea) -> Int32 {
    let pIndex: Int32 = this.GetProficiencyIndexByType(this.GetProficiencyFromPerkArea(perkArea));
    return pIndex;
  }

  private final const func ActivatePerkLevelData(areaIndex: Int32, perkIndex: Int32) -> Void {
    let GLPS: ref<GameplayLogicPackageSystem> = GameInstance.GetGameplayLogicPackageSystem(this.m_owner.GetGame());
    let packageID: TweakDBID = this.GetPerkPackageTDBID(areaIndex, perkIndex);
    GLPS.ApplyPackage(this.m_owner, this.m_owner, packageID);
  }

  private final const func DeactivatePerkLevelData(areaIndex: Int32, perkIndex: Int32) -> Void {
    let GLPS: ref<GameplayLogicPackageSystem> = GameInstance.GetGameplayLogicPackageSystem(this.m_owner.GetGame());
    let packageID: TweakDBID = this.GetPerkPackageTDBID(areaIndex, perkIndex);
    GLPS.RemovePackage(this.m_owner, packageID);
  }

  public final const func GetSpentPerkPoints() -> Int32 {
    let sum: Int32;
    let count: Int32 = ArraySize(this.m_proficiencies);
    let i: Int32 = 0;
    while i < count {
      sum += this.m_proficiencies[i].spentPerkPoints;
      i += 1;
    };
    return sum;
  }

  public final const func GetPerks() -> [SPerk] {
    let allPerks: array<SPerk>;
    let j: Int32;
    let i: Int32 = 0;
    while i < ArraySize(this.m_perkAreas) {
      j = 0;
      while j < ArraySize(this.m_perkAreas[i].boughtPerks) {
        ArrayPush(allPerks, this.m_perkAreas[i].boughtPerks[j]);
        j += 1;
      };
      i += 1;
    };
    return allPerks;
  }

  private final func InitializeTraits() -> Void {
    let i: Int32 = 0;
    while i < 12 {
      this.AddTrait(IntEnum<gamedataTraitType>(i));
      i += 1;
    };
  }

  private final func AddTrait(traitType: gamedataTraitType) -> Void {
    let newTrait: STrait;
    newTrait.type = traitType;
    newTrait.unlocked = false;
    newTrait.currLevel = 0;
    ArrayPush(this.m_traits, newTrait);
  }

  public final const func IncreaseTraitLevel(traitType: gamedataTraitType) -> Bool {
    let primDevIndex: Int32;
    let traitIndex: Int32 = this.GetTraitIndex(traitType);
    if traitIndex < 0 || !this.IsTraitUnlocked(traitIndex) {
      return false;
    };
    if this.CanTraitBeBought() {
      this.m_traits[traitIndex].currLevel += 1;
      primDevIndex = this.GetDevPointsIndex(gamedataDevelopmentPointType.Primary);
      this.m_devPoints[primDevIndex].unspent -= 1;
      this.m_devPoints[primDevIndex].spent += 1;
      this.EvaluateTraitInfiniteData(traitIndex);
      return true;
    };
    return false;
  }

  private final const func RemoveTrait(traitType: gamedataTraitType) -> Bool {
    let primDevIndex: Int32;
    let traitLevel: Int32;
    let traitIndex: Int32 = this.GetTraitIndex(traitType);
    if traitIndex < 0 || !this.IsTraitUnlocked(traitIndex) {
      return false;
    };
    traitLevel = this.m_traits[traitIndex].currLevel;
    if this.m_traits[traitIndex].currLevel > 0 {
      this.ClearTraitInfiniteData(traitIndex);
      this.m_traits[traitIndex].currLevel = 0;
      primDevIndex = this.GetDevPointsIndex(gamedataDevelopmentPointType.Primary);
      this.m_devPoints[primDevIndex].unspent += traitLevel;
      this.m_devPoints[primDevIndex].spent -= traitLevel;
      return true;
    };
    return false;
  }

  public final const func GetSpentTraitPoints() -> Int32 {
    let spentPoints: Int32;
    let i: Int32 = 0;
    while i < 12 {
      if this.IsTraitUnlocked(i) {
        spentPoints += this.GetTraitLevel(i);
      };
      i += 1;
    };
    return spentPoints;
  }

  private final const func EvaluateTraitInfiniteData(traitIndex: Int32) -> Void {
    let GLPS: ref<GameplayLogicPackageSystem> = GameInstance.GetGameplayLogicPackageSystem(this.m_owner.GetGame());
    let traitPackage: TweakDBID = RPGManager.GetTraitRecord(this.m_traits[traitIndex].type).InfiniteTraitData().DataPackage().GetID();
    let traitLevel: Uint32 = Cast<Uint32>(this.m_traits[traitIndex].currLevel);
    GLPS.RemovePackages(this.m_owner, traitPackage, traitLevel);
    GLPS.ApplyPackages(this.m_owner, this.m_owner, traitPackage, traitLevel);
  }

  private final const func ClearTraitInfiniteData(traitIndex: Int32) -> Void {
    let GLPS: ref<GameplayLogicPackageSystem> = GameInstance.GetGameplayLogicPackageSystem(this.m_owner.GetGame());
    let traitPackage: TweakDBID = RPGManager.GetTraitRecord(this.m_traits[traitIndex].type).InfiniteTraitData().DataPackage().GetID();
    let traitLevel: Uint32 = Cast<Uint32>(this.m_traits[traitIndex].currLevel);
    GLPS.RemovePackages(this.m_owner, traitPackage, traitLevel);
  }

  private final const func EvaluateTrait(profType: gamedataProficiencyType) -> Void {
    let traitType: gamedataTraitType = RPGManager.GetProficiencyRecord(profType).Trait().Type();
    let traitIndex: Int32 = this.GetTraitIndex(traitType);
    if traitIndex < 0 || this.IsTraitUnlocked(traitIndex) {
      return;
    };
    if this.IsTraitReqMet(this.m_traits[traitIndex].type) {
      this.m_traits[traitIndex].unlocked = true;
      this.ActivateTraitBase(this.m_traits[traitIndex].type);
    };
  }

  private final const func ActivateTraitBase(traitType: gamedataTraitType) -> Void {
    let traitPackage: TweakDBID = RPGManager.GetTraitRecord(traitType).BaseTraitData().DataPackage().GetID();
    GameInstance.GetGameplayLogicPackageSystem(this.m_owner.GetGame()).ApplyPackage(this.m_owner, this.m_owner, traitPackage);
  }

  public final const func IsTraitUnlocked(traitType: gamedataTraitType) -> Bool {
    return this.IsTraitUnlocked(this.GetTraitIndex(traitType));
  }

  private final const func IsTraitUnlocked(traitIndex: Int32) -> Bool {
    if traitIndex < 0 {
      return false;
    };
    return this.m_traits[traitIndex].unlocked;
  }

  private final const func IsTraitReqMet(traitType: gamedataTraitType) -> Bool {
    let traitRecord: ref<Trait_Record> = this.GetTraitRecord(traitType);
    let prereqID: TweakDBID = traitRecord.Requirement().GetID();
    let prereq: ref<StatPrereq> = IPrereq.CreatePrereq(prereqID) as StatPrereq;
    return prereq.IsFulfilled(this.m_owner.GetGame(), this.m_owner);
  }

  public final const func GetTraitLevel(traitType: gamedataTraitType) -> Int32 {
    return this.GetTraitLevel(this.GetTraitIndex(traitType));
  }

  private final const func GetTraitLevel(traitIndex: Int32) -> Int32 {
    if traitIndex < 0 {
      return 0;
    };
    return this.m_traits[traitIndex].currLevel;
  }

  private final const func GetTraitIndex(traitType: gamedataTraitType) -> Int32 {
    let i: Int32 = 0;
    while Cast<Bool>(ArraySize(this.m_traits)) {
      if Equals(this.m_traits[i].type, traitType) {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  private final const func GetTraitRecord(traitType: gamedataTraitType) -> ref<Trait_Record> {
    return TweakDBInterface.GetTraitRecord(TDBID.Create("Traits." + EnumValueToString("gamedataTraitType", Cast<Int64>(EnumInt(traitType)))));
  }

  public final const func BuyAttribute(type: gamedataStatType) -> Bool {
    let cost: Int32;
    let dIndex: Int32 = this.GetDevPointsIndex(gamedataDevelopmentPointType.Attribute);
    if dIndex < 0 {
      return false;
    };
    if !this.CanAttributeBeBought(type) {
      return false;
    };
    cost = this.GetAttributeNextLevelCost(type);
    this.ModifyAttribute(type, 1.00);
    this.m_devPoints[dIndex].unspent -= cost;
    this.m_devPoints[dIndex].spent += cost;
    return true;
  }

  public final const func ResetAttribute(type: gamedataStatType) -> Void {
    let amount: Float;
    let dIndex: Int32;
    let statSys: ref<StatsSystem>;
    if !this.IsStatValid(type) {
      return;
    };
    if !PlayerDevelopmentData.IsAttribute(type) {
      return;
    };
    statSys = GameInstance.GetStatsSystem(this.m_owner.GetGame());
    amount = statSys.GetStatValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), type);
    this.SetAttribute(type, 3.00);
    dIndex = this.GetDevPointsIndex(gamedataDevelopmentPointType.Attribute);
    this.m_devPoints[dIndex].unspent += Cast<Int32>(amount - 3.00);
    this.m_devPoints[dIndex].spent -= Cast<Int32>(amount - 3.00);
  }

  public final const func ResetAttributes() -> Void {
    this.ResetAttribute(gamedataStatType.Reflexes);
    this.ResetAttribute(gamedataStatType.TechnicalAbility);
    this.ResetAttribute(gamedataStatType.Cool);
    this.ResetAttribute(gamedataStatType.Intelligence);
    this.ResetAttribute(gamedataStatType.Strength);
  }

  public final const func SellAllAttributes() -> Void {
    let attributes: array<SAttribute> = this.GetAttributes();
    let index: Int32 = 0;
    let limit: Int32 = ArraySize(attributes);
    while index < limit {
      this.SellAttribute(attributes[index].attributeName);
      index += 1;
    };
  }

  public final const func SellAttribute(type: gamedataStatType) -> Bool {
    let attVal: Float;
    let diff: Float;
    let defaultAttVal: Float = 3.00;
    let dIndex: Int32 = this.GetDevPointsIndex(gamedataDevelopmentPointType.Attribute);
    if dIndex < 0 {
      return false;
    };
    attVal = this.GetAttributeValue(type);
    this.SetAttribute(type, defaultAttVal);
    diff = attVal - defaultAttVal;
    this.m_devPoints[dIndex].unspent += Cast<Int32>(diff);
    this.m_devPoints[dIndex].spent -= Cast<Int32>(diff);
    return true;
  }

  public final const func SetAttribute(type: gamedataStatType, amount: Float) -> Void {
    let aIndex: Int32;
    let newMod: ref<gameStatModifierData>;
    let statSys: ref<StatsSystem>;
    if !this.IsStatValid(type) {
      return;
    };
    if !PlayerDevelopmentData.IsAttribute(type) {
      return;
    };
    statSys = GameInstance.GetStatsSystem(this.m_owner.GetGame());
    newMod = RPGManager.CreateStatModifier(type, gameStatModifierType.Additive, amount);
    statSys.ForceModifier(Cast<StatsObjectID>(this.m_owner.GetEntityID()), newMod);
    aIndex = this.GetAttributeIndex(type);
    this.m_attributes[aIndex].value = Cast<Int32>(statSys.GetStatValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), type));
    this.UpdateProficiencyMaxLevels();
    this.RefreshPerkAreas();
  }

  public final const func GetAttributes() -> [SAttribute] {
    return this.m_attributes;
  }

  public final const func GetAttributeValue(type: gamedataStatType) -> Float {
    let ss: ref<StatsSystem> = GameInstance.GetStatsSystem(this.m_owner.GetGame());
    if PlayerDevelopmentData.IsAttribute(type) {
      return ss.GetStatValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), type);
    };
    return -1.00;
  }

  private final func SetAttributes() -> Void {
    let attVal: Float;
    let attribute: SAttribute;
    let ss: ref<StatsSystem> = GameInstance.GetStatsSystem(this.m_owner.GetGame());
    let i: Int32 = 0;
    while i < 1709 {
      if PlayerDevelopmentData.IsAttribute(IntEnum<gamedataStatType>(i)) {
        if IsDefined(this.m_owner) {
          attVal = ss.GetStatValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), IntEnum<gamedataStatType>(i));
        } else {
          attVal = 2.00;
        };
        attribute.value = Cast<Int32>(attVal);
        attribute.attributeName = IntEnum<gamedataStatType>(i);
        attribute.id = TDBID.Create("BaseStats." + ToString(attribute.attributeName));
        ArrayPush(this.m_attributes, attribute);
      };
      i += 1;
    };
  }

  private final const func ModifyAttribute(type: gamedataStatType, amount: Float) -> Void {
    let newValue: Float;
    if !this.IsStatValid(type) {
      return;
    };
    newValue = GameInstance.GetStatsSystem(this.m_owner.GetGame()).GetStatValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), type) + amount;
    this.SetAttribute(type, newValue);
  }

  private final const func GetAttributeIndex(type: gamedataStatType) -> Int32 {
    let i: Int32 = 0;
    while i < ArraySize(this.m_attributes) {
      if Equals(this.m_attributes[i].attributeName, type) {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  private final const func GetAttributeDevCap(type: gamedataStatType) -> Int32 {
    let value: Int32 = Cast<Int32>(this.GetAttributeRecord(type).Max());
    return value;
  }

  public final const func CanAttributeBeBought(type: gamedataStatType) -> Bool {
    let currVal: Int32;
    let enoughPoints: Bool;
    let maxLvlNotReached: Bool;
    let objectID: StatsObjectID = Cast<StatsObjectID>(this.m_owner.GetEntityID());
    let dIndex: Int32 = this.GetDevPointsIndex(gamedataDevelopmentPointType.Attribute);
    if dIndex < 0 {
      return false;
    };
    currVal = Cast<Int32>(GameInstance.GetStatsSystem(this.m_owner.GetGame()).GetStatValue(objectID, type));
    enoughPoints = this.m_devPoints[dIndex].unspent >= this.GetAttributeNextLevelCost(type);
    maxLvlNotReached = this.GetAttributeDevCap(type) > currVal;
    if enoughPoints && maxLvlNotReached {
      return true;
    };
    if !enoughPoints {
    };
    return false;
  }

  private final const func IsStatValid(type: gamedataStatType) -> Bool {
    if EnumInt(type) >= 1709 {
      return false;
    };
    return true;
  }

  public final static func IsAttribute(type: gamedataStatType) -> Bool {
    switch type {
      case gamedataStatType.Espionage:
      case gamedataStatType.Gunslinger:
      case gamedataStatType.Reflexes:
      case gamedataStatType.TechnicalAbility:
      case gamedataStatType.Cool:
      case gamedataStatType.Intelligence:
      case gamedataStatType.Strength:
        return true;
      default:
        return false;
    };
  }

  public final static func IsProfficiencyObsolete(profficeinct: gamedataProficiencyType) -> Bool {
    switch profficeinct {
      case gamedataProficiencyType.TechnicalAbilitySkill:
      case gamedataProficiencyType.StrengthSkill:
      case gamedataProficiencyType.ReflexesSkill:
      case gamedataProficiencyType.IntelligenceSkill:
      case gamedataProficiencyType.CoolSkill:
        return false;
      default:
        return true;
    };
  }

  private final const func GetAttributeNextLevelCost(type: gamedataStatType) -> Int32 {
    let statSystem: ref<StatsSystem> = GameInstance.GetStatsSystem(this.m_owner.GetGame());
    let statDataSystem: ref<StatsDataSystem> = GameInstance.GetStatsDataSystem(this.m_owner.GetGame());
    let objectID: StatsObjectID = Cast<StatsObjectID>(this.m_owner.GetEntityID());
    let level: Float = statSystem.GetStatValue(objectID, type);
    let statName: CName = EnumValueToName(n"gamedataStatType", EnumInt(type));
    let cost: Int32 = Cast<Int32>(statDataSystem.GetValueFromCurve(n"player_attributeLevelToCostIncrease", level + 1.00, statName));
    return cost;
  }

  public final const func GetAttributeRecord(type: gamedataStatType) -> ref<Stat_Record> {
    if PlayerDevelopmentData.IsAttribute(type) {
      return TweakDBInterface.GetStatRecord(TDBID.Create("BaseStats." + EnumValueToString("gamedataStatType", Cast<Int64>(EnumInt(type)))));
    };
    return null;
  }

  public final func RegisterSkillCheckPrereq(skillPrereq: ref<SkillCheckPrereqState>) -> Void {
    if !ArrayContains(this.m_skillPrereqs, skillPrereq) {
      ArrayPush(this.m_skillPrereqs, skillPrereq);
    };
  }

  public final func RegisterStatCheckPrereq(statPrereq: ref<StatCheckPrereqState>) -> Void {
    if !ArrayContains(this.m_statPrereqs, statPrereq) {
      ArrayPush(this.m_statPrereqs, statPrereq);
    };
  }

  public final func UnregisterSkillCheckPrereq(skillPrereq: ref<SkillCheckPrereqState>) -> Void {
    ArrayRemove(this.m_skillPrereqs, skillPrereq);
  }

  public final func UnregisterStatCheckPrereq(statPrereq: ref<StatCheckPrereqState>) -> Void {
    ArrayRemove(this.m_statPrereqs, statPrereq);
  }

  private final const func UpdateSkillPrereqs(changedSkill: gamedataProficiencyType, newLevel: Int32) -> Void {
    let i: Int32 = 0;
    while i < ArraySize(this.m_skillPrereqs) {
      if Equals(this.m_skillPrereqs[i].GetSkillToCheck(), changedSkill) {
      };
      i += 1;
    };
  }

  private final const func UpdateStatPrereqs(changedStat: gamedataStatType, newValue: Float) -> Void {
    let i: Int32 = 0;
    while i < ArraySize(this.m_statPrereqs) {
      if Equals(this.m_statPrereqs[i].GetStatToCheck(), changedStat) {
      };
      i += 1;
    };
  }

  public final func SetProgressionBuild(build: gamedataBuildType, opt isDebugBuild: Bool) -> Void {
    let buildString: String = EnumValueToString("gamedataBuildType", Cast<Int64>(EnumInt(build)));
    this.ProcessProgressionBuild(TweakDBInterface.GetProgressionBuildRecord(TDBID.Create("ProgressionBuilds." + buildString)), isDebugBuild);
  }

  public final func SetProgressionBuild(buildID: TweakDBID, opt isDebugBuild: Bool) -> Void {
    this.ProcessProgressionBuild(TweakDBInterface.GetProgressionBuildRecord(buildID), isDebugBuild);
  }

  public final func SetLifePath(lifePath: TweakDBID) -> Void {
    this.ProcessLifePath(TweakDBInterface.GetLifePathRecord(lifePath));
  }

  public final func UpdateAttributes(const attributes: script_ref<[CharacterCustomizationAttribute]>) -> Void {
    let i: Int32 = 0;
    while i < ArraySize(Deref(attributes)) {
      this.SetAttribute(Deref(attributes)[i].type, Cast<Float>(Deref(attributes)[i].value));
      i += 1;
    };
  }

  public final const func GetIsProgressionBuildSetCompleted() -> Bool {
    return this.m_progressionBuildSetCompleted;
  }

  public final func SetIsProgressionBuildSetCompleted(value: Bool) -> Void {
    this.m_progressionBuildSetCompleted = value;
  }

  private final func ProcessProgressionBuild(buildRecord: ref<ProgressionBuild_Record>, opt isDebugBuild: Bool) -> Void {
    let buildAttributeSet: wref<BuildAttributeSet_Record>;
    let buildAttributes: array<wref<BuildAttribute_Record>>;
    let buildCraftableItems: wref<Craftable_Record>;
    let buildCyberwareItems: array<wref<BuildCyberware_Record>>;
    let buildCyberwareSet: wref<BuildCyberwareSet_Record>;
    let buildEquipmentItems: array<wref<BuildEquipment_Record>>;
    let buildEquipmentSet: wref<BuildEquipmentSet_Record>;
    let buildItems: array<wref<InventoryItem_Record>>;
    let buildNewPerks: array<wref<BuildNewPerk_Record>>;
    let buildPerkSet: wref<BuildPerkSet_Record>;
    let buildPerks: array<wref<BuildPerk_Record>>;
    let buildProficiencies: array<wref<BuildProficiency_Record>>;
    let buildProficiencySet: wref<BuildProficiencySet_Record>;
    let buildProgramItems: array<wref<BuildProgram_Record>>;
    let clearAllDevPointsRequest: ref<ClearAllDevPointsRequest>;
    let i: Int32;
    let inventoryItemSet: wref<InventoryItemSet_Record>;
    this.m_displayActivityLog = false;
    let randomizeClothing: Bool = buildRecord.RandomizeClothing();
    this.FlushDevelopment();
    if isDebugBuild {
      this.ClearAllDevPoints();
    };
    buildAttributeSet = buildRecord.AttributeSet();
    buildProficiencySet = buildRecord.ProficiencySet();
    buildPerkSet = buildRecord.PerkSet();
    inventoryItemSet = buildRecord.InventorySet();
    buildEquipmentSet = buildRecord.EquipmentSet();
    buildCyberwareSet = buildRecord.CyberwareSet();
    if IsDefined(buildAttributeSet) {
      buildAttributeSet.Attributes(buildAttributes);
    };
    if IsDefined(buildProficiencySet) {
      buildProficiencySet.Proficiencies(buildProficiencies);
    };
    if IsDefined(buildPerkSet) {
      buildPerkSet.Perks(buildPerks);
    };
    i = 0;
    while i < buildRecord.GetPerkSetsCount() {
      buildRecord.GetPerkSetsItem(i).Perks(buildPerks);
      i += 1;
    };
    if IsDefined(inventoryItemSet) {
      inventoryItemSet.Items(buildItems);
    };
    if IsDefined(buildEquipmentSet) {
      buildEquipmentSet.Equipment(buildEquipmentItems);
    };
    if IsDefined(buildCyberwareSet) {
      buildCyberwareSet.Cyberware(buildCyberwareItems);
      buildCyberwareSet.Programs(buildProgramItems);
    };
    this.ProcessBuildItems(buildItems);
    this.ProcessBuildAttributes(buildAttributes);
    this.ProcessBuildProficiencies(buildProficiencies, isDebugBuild);
    this.ProcessBuildPerks(buildPerks);
    this.ProcessBuildEquipment(buildEquipmentItems, randomizeClothing);
    this.ProcessBuildCyberware(buildCyberwareItems);
    this.ProcessBuildPrograms(buildProgramItems);
    buildRecord.StartingAttributes(buildAttributes);
    buildRecord.StartingProficiencies(buildProficiencies);
    buildRecord.StartingPerks(buildPerks);
    buildRecord.StartingNewPerks(buildNewPerks);
    buildRecord.StartingItems(buildItems);
    buildRecord.StartingEquipment(buildEquipmentItems);
    buildRecord.StartingCyberware(buildCyberwareItems);
    buildRecord.StartingPrograms(buildProgramItems);
    this.ProcessBuildItems(buildItems);
    this.ProcessBuildAttributes(buildAttributes);
    this.ProcessBuildProficiencies(buildProficiencies, isDebugBuild);
    this.ProcessBuildPerks(buildPerks);
    this.ProcessBuildNewPerks(buildNewPerks, isDebugBuild);
    this.ProcessBuildEquipment(buildEquipmentItems, randomizeClothing);
    this.ProcessBuildCyberware(buildCyberwareItems);
    this.ProcessBuildPrograms(buildProgramItems);
    if isDebugBuild {
      clearAllDevPointsRequest = new ClearAllDevPointsRequest();
      clearAllDevPointsRequest.Set(this.m_owner);
      PlayerDevelopmentSystem.GetInstance(this.m_owner).QueueRequest(clearAllDevPointsRequest);
      this.ScaleNPCsToPlayerLevel();
    };
    buildCraftableItems = buildRecord.CraftBook();
    this.ProcessCraftbook(buildCraftableItems);
    if randomizeClothing {
      this.RandomizeClothing();
    };
    this.ScaleItems();
    this.m_displayActivityLog = true;
  }

  private final func ScaleItems() -> Void {
    let i: Int32;
    let inventory: array<wref<gameItemData>>;
    let itemData: wref<gameItemData>;
    let statMod: ref<gameStatModifierData> = RPGManager.CreateStatModifier(gamedataStatType.PowerLevel, gameStatModifierType.Additive, Cast<Float>(this.GetProficiencyLevel(gamedataProficiencyType.Level)));
    GameInstance.GetTransactionSystem(this.m_owner.GetGame()).GetItemList(this.m_owner, inventory);
    i = 0;
    while i < ArraySize(inventory) {
      itemData = inventory[i];
      if !RPGManager.GetItemRecord(itemData.GetID()).IsSingleInstance() && ItemID.GetTDBID(itemData.GetID()) != t"Items.Preset_V_Unity_Cutscene" && ItemID.GetTDBID(itemData.GetID()) != t"Items.w_melee_004__fists_a" {
        GameInstance.GetStatsSystem(this.m_owner.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.PowerLevel);
        GameInstance.GetStatsSystem(this.m_owner.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), statMod);
      };
      i += 1;
    };
  }

  public final func ScaleNPCsToPlayerLevel() -> Void {
    let gameObject: ref<GameObject>;
    let isPreventionMT: Bool;
    let npc: ref<NPCPuppet>;
    let statsSystem: ref<StatsSystem> = GameInstance.GetStatsSystem(this.m_owner.GetGame());
    let playerLevel: Float = statsSystem.GetStatValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), gamedataStatType.Level);
    let statMod: ref<gameConstantStatModifierData> = RPGManager.CreateStatModifier(gamedataStatType.PowerLevel, gameStatModifierType.Additive, playerLevel) as gameConstantStatModifierData;
    let entityList: array<ref<Entity>> = GameInstance.GetEntityList(this.m_owner.GetGame());
    let i: Int32 = 0;
    while i < ArraySize(entityList) {
      gameObject = entityList[i] as GameObject;
      if !IsDefined(gameObject) {
      } else {
        npc = gameObject as NPCPuppet;
        isPreventionMT = NPCManager.HasTag(npc.GetRecordID(), n"MaxTac_Prevention");
        if IsDefined(npc) && TDBID.IsValid(npc.GetRecord().ContentAssignmentHandle().GetID()) || gameObject.IsDevice() {
          if isPreventionMT && playerLevel < 25.00 {
            statMod = RPGManager.CreateStatModifier(gamedataStatType.PowerLevel, gameStatModifierType.Additive, 25.00) as gameConstantStatModifierData;
          };
          statsSystem.RemoveAllModifiers(Cast<StatsObjectID>(gameObject.GetEntityID()), gamedataStatType.PowerLevel, true);
          statsSystem.AddSavedModifier(Cast<StatsObjectID>(gameObject.GetEntityID()), statMod);
        };
      };
      i += 1;
    };
  }

  private final func FlushDevelopment() -> Void {
    let attributeType: gamedataStatType;
    let clearRequest: ref<ClearEquipmentRequest>;
    let es: ref<EquipmentSystem>;
    let newPerkLevel: Int32;
    let newPerkType: gamedataNewPerkType;
    let perkType: gamedataPerkType;
    let playerItems: array<wref<gameItemData>>;
    let proficiencyType: gamedataProficiencyType;
    let gi: GameInstance = this.m_owner.GetGame();
    let i: Int32 = 0;
    while i < 1709 {
      attributeType = IntEnum<gamedataStatType>(i);
      if PlayerDevelopmentData.IsAttribute(attributeType) {
        this.SetAttribute(attributeType, 0.00);
      };
      i += 1;
    };
    GameInstance.GetStatsSystem(this.m_owner.GetGame()).RemoveAllModifiers(Cast<StatsObjectID>(this.m_owner.GetEntityID()), gamedataStatType.Level);
    i = 0;
    while i < 20 {
      proficiencyType = IntEnum<gamedataProficiencyType>(i);
      this.SetLevel(proficiencyType, 0, telemetryLevelGainReason.Ignore);
      i += 1;
    };
    i = 0;
    while i < 228 {
      perkType = IntEnum<gamedataPerkType>(i);
      if this.HasPerk(perkType) {
        this.RemovePerk(perkType);
      };
      i += 1;
    };
    i = 0;
    while i < 189 {
      newPerkType = IntEnum<gamedataNewPerkType>(i);
      if this.IsNewPerkUnlocked(newPerkType) {
        while this.ForceSellNewPerk(newPerkType, newPerkLevel) {
        };
        this.LockNewPerk(newPerkType);
      };
      i += 1;
    };
    es = GameInstance.GetScriptableSystemsContainer(gi).Get(n"EquipmentSystem") as EquipmentSystem;
    clearRequest = new ClearEquipmentRequest();
    clearRequest.owner = this.m_owner;
    es.QueueRequest(clearRequest);
    GameInstance.GetTransactionSystem(gi).GetItemList(this.m_owner, playerItems);
    i = 0;
    while i < ArraySize(playerItems) {
      if !playerItems[i].HasTag(n"Quest") {
        GameInstance.GetTransactionSystem(gi).RemoveItem(this.m_owner, playerItems[i].GetID(), playerItems[i].GetQuantity());
      };
      i += 1;
    };
  }

  private final const func ProcessBuildEquipment(const equipment: script_ref<[wref<BuildEquipment_Record>]>, randomizeClothing: Bool) -> Void {
    let drawItemRequest: ref<DrawItemRequest>;
    let equipRequest: ref<GameplayEquipRequest>;
    let itemID: ItemID;
    let gi: GameInstance = this.m_owner.GetGame();
    let isWeaponEquipped: Bool = false;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(gi);
    let es: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(gi).Get(n"EquipmentSystem") as EquipmentSystem;
    let i: Int32 = 0;
    while i < ArraySize(Deref(equipment)) {
      itemID = ItemID.FromTDBID(Deref(equipment)[i].Equipment().GetID());
      if randomizeClothing && EquipmentSystem.IsClothing(itemID) {
      } else {
        transactionSystem.GiveItem(this.m_owner, itemID, 1);
        equipRequest = new GameplayEquipRequest();
        equipRequest.owner = this.m_owner;
        equipRequest.itemID = itemID;
        equipRequest.blockUpdateWeaponActiveSlots = true;
        es.QueueRequest(equipRequest);
        if IsMultiplayer() && !isWeaponEquipped {
          if Equals(TweakDBInterface.GetItemRecord(Deref(equipment)[i].Equipment().GetID()).ItemCategory().Type(), gamedataItemCategory.Weapon) {
            isWeaponEquipped = true;
            drawItemRequest = new DrawItemRequest();
            drawItemRequest.owner = this.m_owner;
            drawItemRequest.itemID = itemID;
            es.QueueRequest(drawItemRequest);
          };
        };
      };
      i += 1;
    };
  }

  private final const func ProcessBuildCyberware(const cyberware: script_ref<[wref<BuildCyberware_Record>]>) -> Void {
    let installModuleRequest: ref<EquipRequest>;
    let gi: GameInstance = this.m_owner.GetGame();
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let es: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(gi).Get(n"EquipmentSystem") as EquipmentSystem;
    let i: Int32 = 0;
    while i < ArraySize(Deref(cyberware)) {
      installModuleRequest = new EquipRequest();
      installModuleRequest.owner = this.m_owner;
      installModuleRequest.itemID = ItemID.FromTDBID(Deref(cyberware)[i].Cyberware().GetID());
      if !ts.HasItem(this.m_owner, ItemID.CreateQuery(Deref(cyberware)[i].Cyberware().GetID())) {
        installModuleRequest.addToInventory = true;
      };
      es.QueueRequest(installModuleRequest);
      i += 1;
    };
  }

  private final const func ProcessBuildPrograms(const programs: script_ref<[wref<BuildProgram_Record>]>) -> Void {
    let equipProgramsRequest: ref<GameplayEquipProgramsRequest>;
    let es: ref<EquipmentSystem>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let i: Int32 = 0;
    while i < ArraySize(Deref(programs)) {
      transactionSystem.GiveItemByTDBID(this.m_owner, Deref(programs)[i].Program().GetID(), 1);
      i += 1;
    };
    if ArraySize(Deref(programs)) > 0 {
      equipProgramsRequest = new GameplayEquipProgramsRequest();
      equipProgramsRequest.owner = this.m_owner;
      i = 0;
      while i < ArraySize(Deref(programs)) {
        ArrayPush(equipProgramsRequest.programIDs, ItemID.CreateQuery(Deref(programs)[i].Program().GetID()));
        i += 1;
      };
      es = GameInstance.GetScriptableSystemsContainer(this.m_owner.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
      es.QueueRequest(equipProgramsRequest);
    };
  }

  private final const func ProcessBuildAttributes(const attributes: script_ref<[wref<BuildAttribute_Record>]>) -> Void {
    let i: Int32 = 0;
    while i < ArraySize(Deref(attributes)) {
      this.SetAttribute(Deref(attributes)[i].Attribute().StatType(), Cast<Float>(Deref(attributes)[i].Level()));
      i += 1;
    };
  }

  private final const func ProcessBuildProficiencies(const proficiencies: script_ref<[wref<BuildProficiency_Record>]>, isDebugBuild: Bool) -> Void {
    let level: Int32;
    let type: gamedataProficiencyType;
    let i: Int32 = 0;
    while i < ArraySize(Deref(proficiencies)) {
      type = Deref(proficiencies)[i].Proficiency().Type();
      level = Deref(proficiencies)[i].Level();
      this.SetLevel(type, level, telemetryLevelGainReason.Ignore, isDebugBuild);
      i += 1;
    };
  }

  private final func ProcessBuildPerks(const perks: script_ref<[wref<BuildPerk_Record>]>) -> Void {
    let j: Int32;
    let i: Int32 = 0;
    while i < ArraySize(Deref(perks)) {
      j = 0;
      while j < Deref(perks)[i].Level() {
        this.BuyPerk(Deref(perks)[i].Perk().Type());
        j += 1;
      };
      i += 1;
    };
  }

  private final func ProcessBuildNewPerks(const perks: script_ref<[wref<BuildNewPerk_Record>]>, opt forceBuy: Bool) -> Void {
    let j: Int32;
    let isEspionageUnlocked: Bool = GetFact(this.m_owner.GetGame(), n"ep1_tree_unlocked") > 0;
    let i: Int32 = 0;
    while i < ArraySize(Deref(perks)) {
      j = 0;
      while j < Deref(perks)[i].Level() {
        this.UnlockNewPerk(Deref(perks)[i].Perk().Type());
        this.BuyNewPerk(Deref(perks)[i].Perk().Type(), forceBuy);
        if forceBuy && !isEspionageUnlocked && this.IsNewPerkEspionage(Deref(perks)[i].Perk().Type()) {
          AddFact(this.m_owner.GetGame(), n"ep1_tree_unlocked");
          isEspionageUnlocked = true;
        };
        j += 1;
      };
      i += 1;
    };
  }

  private final const func ProcessBuildItems(const items: script_ref<[wref<InventoryItem_Record>]>) -> Void {
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let i: Int32 = 0;
    while i < ArraySize(Deref(items)) {
      transactionSystem.GiveItemByTDBID(this.m_owner, Deref(items)[i].Item().GetID(), Deref(items)[i].Quantity());
      i += 1;
    };
  }

  private final const func ProcessCraftbook(recipes: wref<Craftable_Record>) -> Void {
    let craftingSystem: ref<CraftingSystem> = GameInstance.GetScriptableSystemsContainer(this.m_owner.GetGame()).Get(n"CraftingSystem") as CraftingSystem;
    craftingSystem.GetPlayerCraftBook().InitializeCraftBook(this.m_owner, recipes);
  }

  private final func ProcessLifePath(lifePath: wref<LifePath_Record>) -> Void {
    this.m_lifePath = lifePath.Type();
  }

  private final func RandomizeClothing() -> Void {
    let equipRequest: ref<EquipRequest>;
    let itemID: ItemID;
    let items: array<wref<BuildEquipment_Record>>;
    let random: Int32;
    let setRecord: wref<BuildEquipmentSet_Record>;
    let tdbid: TweakDBID;
    let slots: array<gamedataEquipmentArea> = EquipmentSystem.GetClothingEquipmentAreas();
    let es: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(this.m_owner.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    let i: Int32 = 0;
    while i < ArraySize(slots) {
      ArrayClear(items);
      tdbid = TDBID.Create("BuildSets." + EnumValueToString("gamedataEquipmentArea", Cast<Int64>(EnumInt(slots[i]))));
      setRecord = TweakDBInterface.GetBuildEquipmentSetRecord(tdbid);
      setRecord.Equipment(items);
      if ArraySize(items) > 0 {
        random = RandRange(0, ArraySize(items));
        itemID = ItemID.FromTDBID(items[random].Equipment().GetID());
        equipRequest = new EquipRequest();
        equipRequest.owner = this.m_owner;
        equipRequest.itemID = itemID;
        equipRequest.addToInventory = true;
        es.QueueRequest(equipRequest);
      };
      i += 1;
    };
  }

  public final func RefreshDevelopmentSystemOnNewGameStarted() -> Void {
    let charCreationAttributes: array<CharacterCustomizationAttribute>;
    let unset: Uint32;
    let playerPuppet: ref<PlayerPuppet> = this.m_owner as PlayerPuppet;
    this.RefreshDevelopmentSystem();
    unset = GameInstance.GetCharacterCustomizationSystem(playerPuppet.GetGame()).GetState().GetAttributePointsAvailable();
    this.AddDevelopmentPoints(Cast<Int32>(unset), gamedataDevelopmentPointType.Attribute);
    charCreationAttributes = GameInstance.GetCharacterCustomizationSystem(playerPuppet.GetGame()).GetState().GetAttributes();
    this.UpdateAttributes(charCreationAttributes);
    GameInstance.GetCharacterCustomizationSystem(playerPuppet.GetGame()).ClearState();
  }

  public final func RefreshDevelopmentSystem() -> Void {
    this.RefreshProficiencyStats();
    this.SetAttributes();
    this.UpdateProficiencyMaxLevels();
    if Equals(this.GetLifePath(), gamedataLifePath.StreetKid) {
      this.SetProgressionBuild(gamedataBuildType.StreetKidStarting);
    } else {
      if Equals(this.GetLifePath(), gamedataLifePath.Nomad) {
        this.SetProgressionBuild(gamedataBuildType.NomadStarting);
      } else {
        if Equals(this.GetLifePath(), gamedataLifePath.Corporate) {
          this.SetProgressionBuild(gamedataBuildType.CorporateStarting);
        } else {
          this.SetProgressionBuild(gamedataBuildType.StartingBuild);
        };
      };
    };
  }

  public final const func UpdatePerkAreaBB(areaIndex: Int32) -> Void {
    let m_ownerStatsBB: ref<IBlackboard>;
    let gi: GameInstance = this.m_owner.GetGame();
    if this.m_owner == GameInstance.GetPlayerSystem(gi).GetLocalPlayerMainGameObject() {
      m_ownerStatsBB = GameInstance.GetBlackboardSystem(gi).Get(GetAllBlackboardDefs().UI_PlayerStats);
      m_ownerStatsBB.SetVariant(GetAllBlackboardDefs().UI_PlayerStats.ModifiedPerkArea, ToVariant(this.m_perkAreas[areaIndex]), true);
    };
  }

  public final const func UpdateUIBB() -> Void {
    let gi: GameInstance = this.m_owner.GetGame();
    let m_ownerStatsBB: ref<IBlackboard> = GameInstance.GetBlackboardSystem(gi).Get(GetAllBlackboardDefs().UI_PlayerStats);
    if IsDefined(m_ownerStatsBB) && this.m_owner == GameInstance.GetPlayerSystem(gi).GetLocalPlayerMainGameObject() {
      m_ownerStatsBB.SetInt(GetAllBlackboardDefs().UI_PlayerStats.Level, this.GetProficiencyLevel(gamedataProficiencyType.Level), true);
      m_ownerStatsBB.SetInt(GetAllBlackboardDefs().UI_PlayerStats.CurrentXP, this.GetCurrentLevelProficiencyExp(gamedataProficiencyType.Level), true);
      m_ownerStatsBB.SetInt(GetAllBlackboardDefs().UI_PlayerStats.RequiredXP, this.GetExperienceForNextLevel(gamedataProficiencyType.Level), true);
      m_ownerStatsBB.SetInt(GetAllBlackboardDefs().UI_PlayerStats.StreetCredLevel, this.GetProficiencyLevel(gamedataProficiencyType.StreetCred), true);
      m_ownerStatsBB.SetInt(GetAllBlackboardDefs().UI_PlayerStats.StreetCredPoints, this.GetCurrentLevelProficiencyExp(gamedataProficiencyType.StreetCred), true);
      m_ownerStatsBB.SetVariant(GetAllBlackboardDefs().UI_PlayerStats.DevelopmentPoints, ToVariant(this.m_devPoints), true);
      m_ownerStatsBB.SetVariant(GetAllBlackboardDefs().UI_PlayerStats.Proficiency, ToVariant(this.m_proficiencies), true);
      m_ownerStatsBB.SetVariant(GetAllBlackboardDefs().UI_PlayerStats.Perks, ToVariant(this.m_perkAreas), true);
      m_ownerStatsBB.SetVariant(GetAllBlackboardDefs().UI_PlayerStats.Attributes, ToVariant(this.m_attributes), true);
      m_ownerStatsBB.SetInt(GetAllBlackboardDefs().UI_PlayerStats.PhysicalResistance, Cast<Int32>(GameInstance.GetStatsSystem(gi).GetStatValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), gamedataStatType.PhysicalResistance)), true);
      m_ownerStatsBB.SetInt(GetAllBlackboardDefs().UI_PlayerStats.ThermalResistance, Cast<Int32>(GameInstance.GetStatsSystem(gi).GetStatValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), gamedataStatType.ThermalDamage)), true);
      m_ownerStatsBB.SetInt(GetAllBlackboardDefs().UI_PlayerStats.EnergyResistance, Cast<Int32>(GameInstance.GetStatsSystem(gi).GetStatValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), gamedataStatType.ElectricResistance)), true);
      m_ownerStatsBB.SetInt(GetAllBlackboardDefs().UI_PlayerStats.ChemicalResistance, Cast<Int32>(GameInstance.GetStatsSystem(gi).GetStatValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), gamedataStatType.ChemicalResistance)), true);
      m_ownerStatsBB.SetInt(GetAllBlackboardDefs().UI_PlayerStats.weightMax, Cast<Int32>(GameInstance.GetStatsSystem(gi).GetStatValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), gamedataStatType.CarryCapacity)), true);
    };
  }
}

public class PlayerDevelopmentSystem extends ScriptableSystem {

  private persistent let m_playerData: [ref<PlayerDevelopmentData>];

  private let m_playerDevelopmentUpdated: Bool;

  private let m_progressionBuildUpdated: Bool;

  public final static func GetInstance(owner: ref<GameObject>) -> ref<PlayerDevelopmentSystem> {
    let PDS: ref<PlayerDevelopmentSystem> = GameInstance.GetScriptableSystemsContainer(owner.GetGame()).Get(n"PlayerDevelopmentSystem") as PlayerDevelopmentSystem;
    return PDS;
  }

  private final func OnPlayerAttach(request: ref<PlayerAttachRequest>) -> Void {
    let data: ref<PlayerDevelopmentData>;
    let updatePDS: ref<UpdatePlayerDevelopment>;
    if !IsDefined(this.GetDevelopmentData(request.owner)) {
      data = new PlayerDevelopmentData();
      data.SetOwner(request.owner);
      data.SetLifePath(GameInstance.GetCharacterCustomizationSystem(request.owner.GetGame()).GetState().GetLifePath());
      updatePDS = new UpdatePlayerDevelopment();
      updatePDS.Set(request.owner);
      updatePDS.ForceRefresh = request.owner.IsVRReplacer();
      GameInstance.GetScriptableSystemsContainer(request.owner.GetGame()).Get(n"PlayerDevelopmentSystem").QueueRequest(updatePDS);
      ArrayPush(this.m_playerData, data);
      data.OnNewGame();
    } else {
      data = this.GetDevelopmentData(request.owner);
    };
    data.OnAttach();
  }

  private final func OnPlayerDetach(request: ref<PlayerDetachRequest>) -> Void {
    let entID: EntityID;
    let i: Int32;
    if EntityID.IsDefined(request.owner.GetEntityID()) {
      entID = request.owner.GetEntityID();
    } else {
      entID = request.ownerID;
    };
    i = 0;
    while i < ArraySize(this.m_playerData) {
      if this.m_playerData[i].GetOwnerID() == entID {
        this.m_playerData[i].OnDetach();
        if this.m_playerData[i].GetOwnerID() != GameInstance.GetPlayerSystem(request.owner.GetGame()).GetLocalPlayerMainGameObject().GetEntityID() {
          ArrayRemove(this.m_playerData, this.m_playerData[i]);
        };
        return;
      };
      i += 1;
    };
  }

  private final const func GetDevelopmentData(owner: ref<GameObject>) -> ref<PlayerDevelopmentData> {
    let i: Int32 = 0;
    while i < ArraySize(this.m_playerData) {
      if this.m_playerData[i].GetOwnerID() == owner.GetEntityID() {
        return this.m_playerData[i];
      };
      i += 1;
    };
    return null;
  }

  public final static func GetData(owner: ref<GameObject>) -> ref<PlayerDevelopmentData> {
    let playerDevSystem: ref<PlayerDevelopmentSystem> = GameInstance.GetScriptableSystemsContainer(owner.GetGame()).Get(n"PlayerDevelopmentSystem") as PlayerDevelopmentSystem;
    return playerDevSystem.GetDevelopmentData(owner);
  }

  private func OnRestored(saveVersion: Int32, gameVersion: Int32) -> Void {
    let gameInstance: GameInstance;
    let i: Int32;
    let factVal: Int32 = GetFact(this.GetGameInstance(), n"EspionageAttributeRetrofix");
    if factVal <= 0 && true {
      i = 0;
      while i < ArraySize(this.m_playerData) {
        this.m_playerData[i].EspionageAttributeRetrofix();
        i += 1;
      };
      SetFactValue(this.GetGameInstance(), n"EspionageAttributeRetrofix", 1);
    };
    gameInstance = this.GetGameInstance();
    i = 0;
    while i < ArraySize(this.m_playerData) {
      this.m_playerData[i].OnRestored(gameInstance);
      i += 1;
    };
    factVal = GetFact(this.GetGameInstance(), n"FreePerkRespec");
    if factVal <= 0 && true && saveVersion <= 212 {
      this.GrantFreeRespec();
      SetFactValue(this.GetGameInstance(), n"FreePerkRespec", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"Perks2");
    if factVal <= 0 {
      factVal = GetFact(this.GetGameInstance(), n"ep1_standalone");
      if factVal <= 0 {
        this.ResetProgressionForNewPerks();
      };
      SetFactValue(this.GetGameInstance(), n"Perks2", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"CalculateXPOldSaves");
    if factVal <= 0 && true && saveVersion <= 212 {
      SetFactValue(this.GetGameInstance(), n"CalculateXPOldSaves", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"RelicDevelopmentData");
    if factVal <= 0 {
      this.ReinitializeProficiencies();
      SetFactValue(this.GetGameInstance(), n"RelicDevelopmentData", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"Skills2");
    if factVal <= 0 && true && saveVersion <= 224 {
      this.UpgradeExperienceToSkills2();
      SetFactValue(this.GetGameInstance(), n"Skills2", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"RetrofixCraftingComponents");
    if factVal <= 0 && true && saveVersion <= 224 {
      this.RetrofixCraftingComponents();
      SetFactValue(this.GetGameInstance(), n"RetrofixCraftingComponents", 1);
    };
  }

  private final func GrantFreeRespec() -> Void {
    let removeAllPerks: ref<RemoveAllPerks> = new RemoveAllPerks();
    let player: ref<GameObject> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject();
    removeAllPerks.Set(player, true, true);
    this.QueueRequest(removeAllPerks);
  }

  private final func ResetProgressionForNewPerks() -> Void {
    let resetProgressionForNewPerks: ref<ResetProgressionForNewPerks> = new ResetProgressionForNewPerks();
    let player: ref<GameObject> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject();
    resetProgressionForNewPerks.Set(player);
    this.QueueRequest(resetProgressionForNewPerks);
  }

  private final func ReinitializeProficiencies() -> Void {
    let request: ref<ReinitializeProficiencies> = new ReinitializeProficiencies();
    request.owner = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject();
    this.QueueRequest(request);
  }

  private final func UpgradeExperienceToSkills2() -> Void {
    let assault: Int32;
    let athletics: Int32;
    let body: Int32;
    let brawling: Int32;
    let coldBlood: Int32;
    let combatHacking: Int32;
    let cool: Int32;
    let crafting: Int32;
    let demolition: Int32;
    let engineering: Int32;
    let gunslinger: Int32;
    let hacking: Int32;
    let int: Int32;
    let kenjutsu: Int32;
    let reflex: Int32;
    let request: ref<SetProficiencyLevel>;
    let stealth: Int32;
    let tech: Int32;
    let player: wref<GameObject> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject();
    if !IsDefined(player) {
      return;
    };
    brawling = this.GetProficiencyLevel(player, gamedataProficiencyType.Brawling);
    athletics = this.GetProficiencyLevel(player, gamedataProficiencyType.Athletics);
    demolition = this.GetProficiencyLevel(player, gamedataProficiencyType.Demolition);
    body = Max(this.GetProficiencyLevel(player, gamedataProficiencyType.StrengthSkill), 0);
    body += brawling + athletics + demolition;
    coldBlood = this.GetProficiencyLevel(player, gamedataProficiencyType.ColdBlood);
    gunslinger = this.GetProficiencyLevel(player, gamedataProficiencyType.Gunslinger);
    stealth = this.GetProficiencyLevel(player, gamedataProficiencyType.Stealth);
    cool = Max(this.GetProficiencyLevel(player, gamedataProficiencyType.CoolSkill), 0);
    cool += coldBlood + gunslinger + stealth;
    combatHacking = this.GetProficiencyLevel(player, gamedataProficiencyType.CombatHacking);
    hacking = this.GetProficiencyLevel(player, gamedataProficiencyType.Hacking);
    int = Max(this.GetProficiencyLevel(player, gamedataProficiencyType.IntelligenceSkill), 0);
    int += combatHacking + hacking;
    crafting = this.GetProficiencyLevel(player, gamedataProficiencyType.Crafting);
    engineering = this.GetProficiencyLevel(player, gamedataProficiencyType.Engineering);
    tech = Max(this.GetProficiencyLevel(player, gamedataProficiencyType.TechnicalAbilitySkill), 0);
    tech += crafting + engineering;
    assault = this.GetProficiencyLevel(player, gamedataProficiencyType.Assault);
    kenjutsu = this.GetProficiencyLevel(player, gamedataProficiencyType.Kenjutsu);
    reflex = Max(this.GetProficiencyLevel(player, gamedataProficiencyType.ReflexesSkill), 0);
    reflex += assault + kenjutsu;
    request = new SetProficiencyLevel();
    request.Set(player, body, gamedataProficiencyType.StrengthSkill, telemetryLevelGainReason.Gameplay);
    this.QueueRequest(request);
    request = new SetProficiencyLevel();
    request.Set(player, cool, gamedataProficiencyType.CoolSkill, telemetryLevelGainReason.Gameplay);
    this.QueueRequest(request);
    request = new SetProficiencyLevel();
    request.Set(player, int, gamedataProficiencyType.IntelligenceSkill, telemetryLevelGainReason.Gameplay);
    this.QueueRequest(request);
    request = new SetProficiencyLevel();
    request.Set(player, tech, gamedataProficiencyType.TechnicalAbilitySkill, telemetryLevelGainReason.Gameplay);
    this.QueueRequest(request);
    request = new SetProficiencyLevel();
    request.Set(player, reflex, gamedataProficiencyType.ReflexesSkill, telemetryLevelGainReason.Gameplay);
    this.QueueRequest(request);
  }

  public final const func GetIsProgressionBuildSetCompleted(owner: ref<GameObject>) -> Bool {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetIsProgressionBuildSetCompleted();
  }

  public final const func SetIsProgressionBuildSetCompleted(owner: ref<GameObject>, value: Bool) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    developmentData.SetIsProgressionBuildSetCompleted(value);
  }

  private final func RetrofixCraftingComponents() -> Void {
    let itemIDs: array<TweakDBID>;
    let player: ref<PlayerPuppet> = GetPlayer(this.GetGameInstance());
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGameInstance());
    if !IsDefined(player) || !IsDefined(transactionSystem) {
      return;
    };
    if this.RetrofixCraftingComponent(player, transactionSystem, t"LootPrereqs.PlayerLevel_Tier_4_to_5_Exclusion_Prereq", itemIDs) {
      return;
    };
    ArrayPush(itemIDs, t"Items.LegendaryMaterial1");
    ArrayPush(itemIDs, t"Items.QuickHackLegendaryMaterial1");
    if this.RetrofixCraftingComponent(player, transactionSystem, t"LootPrereqs.PlayerLevel_Tier_3_to_4_Exclusion_Prereq", itemIDs) {
      return;
    };
    ArrayPush(itemIDs, t"Items.EpicMaterial1");
    ArrayPush(itemIDs, t"Items.QuickHackEpicMaterial1");
    if this.RetrofixCraftingComponent(player, transactionSystem, t"LootPrereqs.PlayerLevel_Tier_2_to_3_Exclusion_Prereq", itemIDs) {
      return;
    };
    ArrayPush(itemIDs, t"Items.RareMaterial1");
    ArrayPush(itemIDs, t"Items.QuickHackRareMaterial1");
    if this.RetrofixCraftingComponent(player, transactionSystem, t"LootPrereqs.PlayerLevel_Tier_1_to_2_Exclusion_Prereq", itemIDs) {
      return;
    };
    ArrayPush(itemIDs, t"Items.UncommonMaterial1");
    ArrayPush(itemIDs, t"Items.QuickHackUncommonMaterial1");
    this.RetrofixCraftingComponent(player, transactionSystem, t"LootPrereqs.PlayerLevel_Tier_1_Exclusion_Prereq", itemIDs);
  }

  private final func RetrofixCraftingComponent(player: ref<PlayerPuppet>, transactionSystem: ref<TransactionSystem>, requirementID: TweakDBID, itemIDs: [TweakDBID]) -> Bool {
    let i: Int32;
    let itemData: ref<gameItemData>;
    let quantity: Int32;
    let prereq: ref<IPrereq> = IPrereq.CreatePrereq(requirementID);
    if prereq.IsFulfilled(player.GetGame(), player) {
      i = 0;
      while i < ArraySize(itemIDs) {
        itemData = transactionSystem.GetItemDataByTDBID(player, itemIDs[i]);
        if !IsDefined(itemData) {
        } else {
          quantity = itemData.GetQuantity();
          transactionSystem.RemoveItemByTDBID(player, itemIDs[i], quantity);
        };
        i += 1;
      };
      return true;
    };
    return false;
  }

  public final const func IsProficiencyMaxLvl(owner: ref<GameObject>, type: gamedataProficiencyType) -> Bool {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.IsProficiencyMaxLvl(type);
  }

  public final const func GetProficiencyLevel(owner: ref<GameObject>, type: gamedataProficiencyType) -> Int32 {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetProficiencyLevel(type);
  }

  public final const func GetProficiencyAbsoluteMaxLevel(owner: ref<GameObject>, type: gamedataProficiencyType) -> Int32 {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetProficiencyAbsoluteMaxLevel(type);
  }

  public final const func GetCurrentLevelProficiencyExp(owner: ref<GameObject>, type: gamedataProficiencyType) -> Int32 {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetCurrentLevelProficiencyExp(type);
  }

  public final const func GetTotalProfExperience(owner: ref<GameObject>, type: gamedataProficiencyType) -> Int32 {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetTotalProfExperience(type);
  }

  public final const func GetRemainingExpForLevelUp(owner: ref<GameObject>, type: gamedataProficiencyType) -> Int32 {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetRemainingExpForLevelUp(type);
  }

  public final const func GetDominatingCombatProficiency(owner: ref<GameObject>) -> gamedataProficiencyType {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetDominatingCombatProficiency();
  }

  public final const func GetDevPoints(owner: ref<GameObject>, type: gamedataDevelopmentPointType) -> Int32 {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetDevPoints(type);
  }

  private final func OnClearAllDevPoints(evt: ref<ClearAllDevPointsRequest>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(evt.owner);
    developmentData.ClearAllDevPoints();
  }

  public final const func GetPerkLevel(owner: ref<GameObject>, type: gamedataPerkType) -> Int32 {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetPerkLevel(type);
  }

  public final const func GetPerkMaxLevel(owner: ref<GameObject>, type: gamedataPerkType) -> Int32 {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetPerkMaxLevel(type);
  }

  public final const func HasPerk(owner: ref<GameObject>, type: gamedataPerkType) -> Bool {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.HasPerk(type);
  }

  public final const func GetPerks(owner: ref<GameObject>) -> [SPerk] {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetPerks();
  }

  public final const func IsPerkImplemented(owner: ref<GameObject>, perk: gamedataPerkType) -> Bool {
    let levelsData: array<wref<PerkLevelData_Record>>;
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    developmentData.GetPerkRecord(perk).Levels(levelsData);
    return ArraySize(levelsData) > 0;
  }

  public final const func GetPerkLevel(owner: ref<GameObject>, type: gamedataNewPerkType) -> Int32 {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.IsNewPerkBought(type);
  }

  public final const func GetPerkMaxLevel(owner: ref<GameObject>, type: gamedataNewPerkType) -> Int32 {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetNewPerkMaxLevel(type);
  }

  public final const func BuyAttribute(owner: ref<GameObject>, obj: ref<GameObject>, type: gamedataStatType) -> Bool {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.BuyAttribute(type);
  }

  public final const func SetAttribute(owner: ref<GameObject>, obj: ref<GameObject>, type: gamedataStatType, amount: Float) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.SetAttribute(type, amount);
  }

  public final const func GetAttributes(owner: ref<GameObject>) -> [SAttribute] {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetAttributes();
  }

  private final func UnlockNewPerk(owner: ref<GameObject>, perkType: gamedataNewPerkType) -> Bool {
    let newPerkUnlockedEvent: ref<NewPerkUnlockedEvent>;
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    let success: Bool = developmentData.UnlockNewPerk(perkType);
    if success {
      newPerkUnlockedEvent = new NewPerkUnlockedEvent();
      newPerkUnlockedEvent.perkType = perkType;
      GameInstance.GetUISystem(this.GetGameInstance()).QueueEvent(newPerkUnlockedEvent);
    };
    return success;
  }

  private final func LockNewPerk(owner: ref<GameObject>, perkType: gamedataNewPerkType) -> Bool {
    let newPerkLockedEvent: ref<NewPerkLockedEvent>;
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    let success: Bool = developmentData.LockNewPerk(perkType);
    if success {
      newPerkLockedEvent = new NewPerkLockedEvent();
      newPerkLockedEvent.perkType = perkType;
      GameInstance.GetUISystem(this.GetGameInstance()).QueueEvent(newPerkLockedEvent);
    };
    return success;
  }

  public final const func IsNewPerkUnlocked(owner: ref<GameObject>, perkType: gamedataNewPerkType) -> Bool {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.IsNewPerkUnlocked(perkType);
  }

  private final func BuyNewPerk(owner: ref<GameObject>, perkType: gamedataNewPerkType) -> Bool {
    let currLevel: Int32;
    let i: Int32;
    let maxLevel: Int32;
    let newPerkBoughtEvent: ref<NewPerkBoughtEvent>;
    let unlockedPerkList: array<gamedataNewPerkType>;
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    let success: Bool = developmentData.BuyNewPerk(perkType);
    if success {
      newPerkBoughtEvent = new NewPerkBoughtEvent();
      newPerkBoughtEvent.perkType = perkType;
      GameInstance.GetUISystem(this.GetGameInstance()).QueueEvent(newPerkBoughtEvent);
      currLevel = developmentData.IsNewPerkBought(perkType);
      maxLevel = developmentData.GetNewPerkMaxLevel(perkType);
      if currLevel == maxLevel {
        developmentData.GetUnlockedPerkList(perkType, unlockedPerkList);
        i = 0;
        while i < ArraySize(unlockedPerkList) {
          if developmentData.HasEnoughtAttributePoints(unlockedPerkList[i]) {
            this.UnlockNewPerk(owner, unlockedPerkList[i]);
          };
          i += 1;
        };
      };
      if developmentData.IsNewPerkEspionage(perkType) {
        GameInstance.GetTelemetrySystem(this.GetGameInstance()).LogNewPerkUpgraded(perkType, currLevel, gamedataDevelopmentPointType.Espionage);
      } else {
        GameInstance.GetTelemetrySystem(this.GetGameInstance()).LogNewPerkUpgraded(perkType, currLevel, gamedataDevelopmentPointType.Primary);
      };
    };
    return success;
  }

  private final func SellNewPerk(owner: ref<GameObject>, perkType: gamedataNewPerkType) -> Bool {
    let newPerkSoldEvent: ref<NewPerkSoldEvent>;
    let perkLevelSold: Int32;
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    let success: Bool = developmentData.SellNewPerk(perkType, perkLevelSold);
    if success {
      GameInstance.GetTelemetrySystem(this.GetGameInstance()).LogNewPerkRemoved(perkType);
      newPerkSoldEvent = new NewPerkSoldEvent();
      newPerkSoldEvent.perkType = perkType;
      newPerkSoldEvent.perkLevelSold = perkLevelSold;
      GameInstance.GetUISystem(this.GetGameInstance()).QueueEvent(newPerkSoldEvent);
      owner.QueueEvent(newPerkSoldEvent);
    };
    return success;
  }

  public final static func CanSellNewPerk(player: ref<PlayerPuppet>, perkType: gamedataNewPerkType) -> CanSellNewPerkResult {
    let itemID: ItemID;
    let items: array<ItemID>;
    let maxSlots: Int32;
    let result: CanSellNewPerkResult;
    let slotIdx: Int32;
    let perkLevel: Int32 = PlayerDevelopmentSystem.GetData(player).IsNewPerkBought(perkType);
    let statsSystem: ref<StatsSystem> = GameInstance.GetStatsSystem(player.GetGame());
    let statValue: Float = 0.00;
    let humanityAvailable: Float = 0.00;
    let playerData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(player);
    result.success = true;
    result.title = GetLocalizedText(RPGManager.GetNewPerkRecord(perkType).Loc_name_key());
    humanityAvailable = statsSystem.GetStatValue(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.HumanityAvailable);
    if Equals(perkType, gamedataNewPerkType.Tech_Central_Perk_2_2) {
      statValue = statsSystem.GetStatValue(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.Tech_Central_Perk_2_2_Humanity);
    } else {
      if Equals(perkType, gamedataNewPerkType.Tech_Master_Perk_3) {
        statValue = statsSystem.GetStatValue(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.Tech_Master_Perk_3_Humanity);
      } else {
        if Equals(perkType, gamedataNewPerkType.Tech_Central_Milestone_2) {
          statValue = statsSystem.GetStatValue(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.Tech_Central_Milestone_2_Discount);
        };
      };
    };
    if humanityAvailable < statValue {
      result.perkType = perkType;
      result.success = false;
      result.message = "UI-Notifications-RespecCyberwareCapacityBlocked";
      return result;
    };
    if Equals(perkType, gamedataNewPerkType.Tech_Central_Milestone_3) && perkLevel == 3 {
      items = EquipmentSystem.GetItemsInArea(player, gamedataEquipmentArea.MusculoskeletalSystemCW);
      if ArraySize(items) == 3 && ItemID.IsValid(items[2]) {
        result.success = false;
        result.message = "UI-Notifications-RespecCyberwareSlotBlocked";
        return result;
      };
    };
    if Equals(perkType, gamedataNewPerkType.Tech_Central_Perk_3_2) {
      items = EquipmentSystem.GetItemsInArea(player, gamedataEquipmentArea.HandsCW);
      if ArraySize(items) == 2 && ItemID.IsValid(items[1]) {
        result.success = false;
        result.message = "UI-Notifications-RespecCyberwareSlotBlocked";
        return result;
      };
    };
    if Equals(perkType, gamedataNewPerkType.Tech_Central_Perk_3_3) {
      maxSlots = playerData.GetNumberOfSlots(gamedataEquipmentArea.IntegumentarySystemCW);
      slotIdx = 0;
      while slotIdx < maxSlots {
        itemID = playerData.GetItemInEquipSlot(gamedataEquipmentArea.IntegumentarySystemCW, slotIdx);
        if ItemID.IsValid(itemID) && RPGManager.IsItemAdaptiveStemCells(ItemID.GetTDBID(itemID)) {
          result.success = false;
          result.message = "UI-Notifications-RespecCyberwareMasterBlocked";
          return result;
        };
        slotIdx += 1;
      };
    };
    return result;
  }

  public final static func CanSellNewPerks(player: ref<PlayerPuppet>) -> CanSellNewPerkResult {
    let result: CanSellNewPerkResult = PlayerDevelopmentSystem.CanSellNewPerk(player, gamedataNewPerkType.Tech_Central_Perk_2_2);
    if !result.success {
      return result;
    };
    result = PlayerDevelopmentSystem.CanSellNewPerk(player, gamedataNewPerkType.Tech_Master_Perk_3);
    if !result.success {
      return result;
    };
    result = PlayerDevelopmentSystem.CanSellNewPerk(player, gamedataNewPerkType.Tech_Central_Milestone_2);
    if !result.success {
      return result;
    };
    result = PlayerDevelopmentSystem.CanSellNewPerk(player, gamedataNewPerkType.Tech_Central_Milestone_3);
    if !result.success {
      return result;
    };
    result = PlayerDevelopmentSystem.CanSellNewPerk(player, gamedataNewPerkType.Tech_Central_Perk_3_2);
    if !result.success {
      return result;
    };
    result = PlayerDevelopmentSystem.CanSellNewPerk(player, gamedataNewPerkType.Tech_Central_Perk_3_3);
    if !result.success {
      return result;
    };
    return result;
  }

  public final const func IsNewPerkBought(owner: ref<GameObject>, perkType: gamedataNewPerkType) -> Int32 {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.IsNewPerkBought(perkType);
  }

  private final func OnUnlockNewPerk(request: ref<UnlockNewPerk>) -> Void {
    this.UnlockNewPerk(request.owner, request.m_perkType);
  }

  private final func OnLockNewPerk(request: ref<LockNewPerk>) -> Void {
    this.LockNewPerk(request.owner, request.m_perkType);
  }

  private final func OnBuyNewPerk(request: ref<BuyNewPerk>) -> Void {
    this.BuyNewPerk(request.owner, request.m_perkType);
  }

  private final func OnSellNewPerk(request: ref<SellNewPerk>) -> Void {
    this.SellNewPerk(request.owner, request.m_perkType);
  }

  public final const func GetHighestCompletedMinigameLevel(owner: ref<GameObject>) -> Int32 {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return developmentData.GetHighestCompletedMinigameLevel();
  }

  public final const func GetLifePath(owner: ref<GameObject>) -> gamedataLifePath {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(owner);
    return IsDefined(developmentData) ? developmentData.GetLifePath() : gamedataLifePath.Invalid;
  }

  private final func OnExperienceQueued(request: ref<QueueCombatExperience>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.QueueCombatExperience(request.m_amount, request.m_experienceType, request.m_entity);
  }

  private final func OnProcessQueuedExperience(request: ref<ProcessQueuedCombatExperience>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.ProcessQueuedCombatExperience(request.m_entity);
  }

  private final func OnExperienceAdded(request: ref<AddExperience>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.AddExperience(request.m_amount, request.m_experienceType, request.m_debug ? telemetryLevelGainReason.IsDebug : telemetryLevelGainReason.Gameplay);
    if request.m_debug {
      developmentData.ScaleNPCsToPlayerLevel();
    };
  }

  private final func OnSetProficiencyLevel(request: ref<SetProficiencyLevel>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.SetLevel(request.m_proficiencyType, request.m_newLevel, request.m_telemetryLevelGainReason);
    if Equals(request.m_telemetryLevelGainReason, telemetryLevelGainReason.IsDebug) {
      developmentData.ScaleNPCsToPlayerLevel();
    };
  }

  private final func OnLevelUpProficiency(request: ref<LevelUpProficiency>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.SetLevel(request.proficiencyType, developmentData.GetProficiencyLevel(request.proficiencyType) + 1, telemetryLevelGainReason.Ignore);
  }

  private final func OnPerkBought(request: ref<BuyPerk>) -> Void {
    let perkBoughtEvent: ref<PerkBoughtEvent>;
    let playerStatsBB: ref<IBlackboard>;
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    let buyResult: Bool = developmentData.BuyPerk(request.m_perkType);
    if buyResult {
      perkBoughtEvent = new PerkBoughtEvent();
      perkBoughtEvent.perkType = request.m_perkType;
      playerStatsBB = GameInstance.GetBlackboardSystem(this.GetGameInstance()).Get(GetAllBlackboardDefs().UI_PlayerStats);
      playerStatsBB.SetInt(GetAllBlackboardDefs().UI_PlayerStats.weightMax, Cast<Int32>(GameInstance.GetStatsSystem(this.GetGameInstance()).GetStatValue(Cast<StatsObjectID>(request.owner.GetEntityID()), gamedataStatType.CarryCapacity)), true);
      GameInstance.GetUISystem(this.GetGameInstance()).QueueEvent(perkBoughtEvent);
      GameInstance.GetTelemetrySystem(this.GetGameInstance()).LogPerkUpgraded(request.m_perkType, developmentData.GetPerkLevel(request.m_perkType));
    };
  }

  private final func OnTraitLevelIncreased(request: ref<IncreaseTraitLevel>) -> Void {
    let traitBoughtEvent: ref<TraitBoughtEvent>;
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    let buyResult: Bool = developmentData.IncreaseTraitLevel(request.m_trait);
    if buyResult {
      traitBoughtEvent = new TraitBoughtEvent();
      traitBoughtEvent.traitType = request.m_trait;
      GameInstance.GetUISystem(this.GetGameInstance()).QueueEvent(traitBoughtEvent);
    };
  }

  private final func OnPerkRemoved(request: ref<RemovePerk>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.RemovePerk(request.m_perkType);
  }

  private final func OnAllPerksRemoved(request: ref<RemoveAllPerks>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.RemoveAllPerks(request.m_removeCost);
    if request.m_unequipPerkItems {
      EquipmentSystem.UnequipPrereqItems(request.owner);
    };
  }

  private final func OnUnlockPerkArea(request: ref<UnlockPerkArea>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.UnlockPerkArea(request.m_perkArea);
  }

  private final func OnLockPerkArea(request: ref<LockPerkArea>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.LockPerkArea(request.m_perkArea);
  }

  private final func OnAttributeSet(request: ref<SetAttribute>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.SetAttribute(request.m_attributeType, request.m_statLevel);
  }

  private final func OnAttributeBuy(request: ref<BuyAttribute>) -> Void {
    let attributeBoughtEvent: ref<AttributeBoughtEvent>;
    let blackboard: ref<IBlackboard>;
    let newStatValue: Int32;
    let result: Bool;
    let uiAttributeBoughtData: AttributeBoughtData;
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    if !PlayerDevelopmentData.IsAttribute(request.m_attributeType) {
      return;
    };
    if request.m_grantAttributePoint {
      developmentData.AddDevelopmentPoints(1, gamedataDevelopmentPointType.Attribute);
    };
    result = developmentData.BuyAttribute(request.m_attributeType);
    if result {
      newStatValue = RoundF(GameInstance.GetStatsSystem(this.GetGameInstance()).GetStatValue(Cast<StatsObjectID>(request.owner.GetEntityID()), request.m_attributeType));
      attributeBoughtEvent = new AttributeBoughtEvent();
      attributeBoughtEvent.attributeType = request.m_attributeType;
      GameInstance.GetUISystem(this.GetGameInstance()).QueueEvent(attributeBoughtEvent);
      blackboard = GameInstance.GetBlackboardSystem(request.owner.GetGame()).Get(GetAllBlackboardDefs().UI_AttributeBought);
      if IsDefined(blackboard) {
        uiAttributeBoughtData.attribute = request.m_attributeType;
        uiAttributeBoughtData.value = newStatValue;
        blackboard.SetVariant(GetAllBlackboardDefs().UI_AttributeBought.attribute, ToVariant(uiAttributeBoughtData));
        blackboard.SignalVariant(GetAllBlackboardDefs().UI_AttributeBought.attribute);
      };
      GameInstance.GetTelemetrySystem(this.GetGameInstance()).LogAttributeUpgraded(request.m_attributeType, newStatValue);
    };
  }

  private final func OnDevelopmentPointsAdded(request: ref<AddDevelopmentPoints>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.AddDevelopmentPoints(request.amountOfPoints, request.developmentPointType);
    if Equals(request.developmentPointType, gamedataDevelopmentPointType.Espionage) {
      developmentData.ModifyProficiencyLevel(gamedataProficiencyType.Espionage, true, request.amountOfPoints);
    };
  }

  private final func OnSkillCheckPrereqModified(request: ref<ModifySkillCheckPrereq>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    if request.m_register {
      developmentData.RegisterSkillCheckPrereq(request.m_skillCheckState);
    } else {
      developmentData.UnregisterSkillCheckPrereq(request.m_skillCheckState);
    };
  }

  private final func OnStatCheckPrereqModified(request: ref<ModifyStatCheckPrereq>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    if request.m_register {
      developmentData.RegisterStatCheckPrereq(request.m_statCheckState);
    } else {
      developmentData.UnregisterStatCheckPrereq(request.m_statCheckState);
    };
  }

  private final func OnUpdatePlayerDevelopment(request: ref<UpdatePlayerDevelopment>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    if request.ForceRefresh || !this.WasRestored() && !this.m_playerDevelopmentUpdated {
      developmentData.RefreshDevelopmentSystemOnNewGameStarted();
      this.m_playerDevelopmentUpdated = true;
    };
  }

  private final func OnSetProgressionBuild(request: ref<SetProgressionBuild>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.SetProgressionBuild(request.m_buildType, request.m_isDebug);
    this.SetIsProgressionBuildSetCompleted(request.owner, true);
  }

  private final func OnSetProgressionBuild(request: ref<questSetProgressionBuildRequest>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.SetProgressionBuild(request.buildID, true);
  }

  private final func OnSetProgressionBuild(request: ref<gameSetProgressionBuildRequest>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    if !this.WasRestored() && !this.m_progressionBuildUpdated {
      developmentData.RefreshDevelopmentSystem();
      this.m_progressionBuildUpdated = true;
    };
    developmentData.SetProgressionBuild(request.buildID);
  }

  private final func OnRefreshPerkAreas(request: ref<RefreshPerkAreas>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.RefreshPerkAreas();
  }

  private final func OnSetLifePath(request: ref<questSetLifePathRequest>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.SetLifePath(request.lifePathID);
  }

  private final func OnBumpNetrunnerMinigameLevel(request: ref<BumpNetrunnerMinigameLevel>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.BumpNetrunnerMinigameLevel(request.completedMinigameLevel);
  }

  private final func OnResetProgressionForNewPerks(request: ref<ResetProgressionForNewPerks>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.RemoveAllPerks(true);
    developmentData.RemoveDeprecatedPerkPoints();
    developmentData.ScaleNPCsToPlayerLevel();
    EquipmentSystem.UnequipPrereqItems(request.owner);
  }

  private final func OnReinitializeProficiencies(request: ref<ReinitializeProficiencies>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.ReinitializeProficiencies();
  }

  private final const func GetProficiencyRecord(type: gamedataProficiencyType) -> ref<Proficiency_Record> {
    return TweakDBInterface.GetProficiencyRecord(TDBID.Create("Proficiencies." + EnumValueToString("gamedataProficiencyType", Cast<Int64>(EnumInt(type)))));
  }

  private final func OnRequestStatsBB(request: ref<RequestStatsBB>) -> Void {
    let developmentData: ref<PlayerDevelopmentData> = this.GetDevelopmentData(request.owner);
    developmentData.UpdateUIBB();
  }
}
