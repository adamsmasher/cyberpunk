
public static func Cast(hotkey: EHotkey) -> Int32 {
  return EnumInt(hotkey);
}

public class AssignHotkeyIfEmptySlot extends PlayerScriptableSystemRequest {

  private let itemID: ItemID;

  public final static func Construct(itemID: ItemID, owner: wref<GameObject>) -> ref<AssignHotkeyIfEmptySlot> {
    let self: ref<AssignHotkeyIfEmptySlot> = new AssignHotkeyIfEmptySlot();
    self.owner = owner;
    self.itemID = itemID;
    return self;
  }

  public final const func ItemID() -> ItemID {
    return this.itemID;
  }

  public final const func Owner() -> ref<GameObject> {
    return this.owner;
  }

  public final const func IsValid() -> Bool {
    if IsDefined(this.owner) && ItemID.IsValid(this.itemID) {
      return true;
    };
    return false;
  }
}

public class HotkeyAssignmentRequest extends PlayerScriptableSystemRequest {

  protected let itemID: ItemID;

  private let hotkey: EHotkey;

  protected let requestType: EHotkeyRequestType;

  public final const func ItemID() -> ItemID {
    return this.itemID;
  }

  public final const func GetHotkey() -> EHotkey {
    return this.hotkey;
  }

  public final const func Owner() -> ref<GameObject> {
    return this.owner;
  }

  public final const func GetRequestType() -> EHotkeyRequestType {
    return this.requestType;
  }

  public final static func Construct(itemID: ItemID, hotkey: EHotkey, owner: wref<GameObject>, requestType: EHotkeyRequestType) -> ref<HotkeyAssignmentRequest> {
    let self: ref<HotkeyAssignmentRequest> = new HotkeyAssignmentRequest();
    self.owner = owner;
    self.itemID = itemID;
    self.hotkey = hotkey;
    self.requestType = requestType;
    return self;
  }

  public final const func IsValid() -> Bool {
    if IsDefined(this.owner) && NotEquals(this.hotkey, EHotkey.INVALID) {
      return true;
    };
    return false;
  }
}

public class Hotkey extends IScriptable {

  private persistent let hotkey: EHotkey;

  private persistent let itemID: ItemID;

  private persistent let scope: [gamedataItemType];

  public final static func Construct(hotk: EHotkey, opt id: ItemID) -> ref<Hotkey> {
    let h: ref<Hotkey> = new Hotkey();
    h.hotkey = hotk;
    if ItemID.IsValid(id) {
      h.itemID = id;
    };
    h.SetScope(Hotkey.GetScope(hotk));
    return h;
  }

  public final func StoreItem(id: ItemID) -> Void {
    this.itemID = id;
  }

  public final const func IsEmpty() -> Bool {
    return this.itemID == ItemID.None();
  }

  public final const func GetItemID() -> ItemID {
    return this.itemID;
  }

  public final const func GetHotkey() -> EHotkey {
    return this.hotkey;
  }

  public final const func GetScope() -> [gamedataItemType] {
    if ArraySize(this.scope) > 0 {
      return this.scope;
    };
    return Hotkey.GetScope(this.GetHotkey());
  }

  public final const func IsCompatible(type: gamedataItemType) -> Bool {
    let range: array<gamedataItemType> = this.GetScope();
    let i: Int32 = 0;
    while i < ArraySize(range) {
      if Equals(range[i], type) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final const func IsCompatible(equipmentArea: gamedataEquipmentArea) -> Bool {
    return Equals(this.hotkey, EHotkey.LBRB) && Equals(equipmentArea, gamedataEquipmentArea.SystemReplacementCW);
  }

  public final func SetScope(const itemTypes: script_ref<[gamedataItemType]>) -> Void {
    this.scope = Deref(itemTypes);
  }

  public final static func IsCompatible(hotkey: EHotkey, type: gamedataItemType) -> Bool {
    let scope: array<gamedataItemType> = Hotkey.GetScope(hotkey);
    return ArrayContains(scope, type);
  }

  public final static func IsCompatible(hotkey: EHotkey, equipmentArea: gamedataEquipmentArea) -> Bool {
    return Equals(hotkey, EHotkey.LBRB) && Equals(equipmentArea, gamedataEquipmentArea.SystemReplacementCW);
  }

  public final static func GetScope(hotkey: EHotkey) -> [gamedataItemType] {
    let scope: array<gamedataItemType>;
    if Equals(hotkey, EHotkey.DPAD_UP) {
      ArrayPush(scope, gamedataItemType.Con_Inhaler);
      ArrayPush(scope, gamedataItemType.Con_Injector);
      ArrayPush(scope, gamedataItemType.Cyb_HealingAbility);
    } else {
      if Equals(hotkey, EHotkey.RB) {
        ArrayPush(scope, gamedataItemType.Cyb_Ability);
        ArrayPush(scope, gamedataItemType.Cyb_Launcher);
        ArrayPush(scope, gamedataItemType.Gad_Grenade);
      };
    };
    return scope;
  }

  public final static func ItemTypeMustBeEquipped(itemType: gamedataItemType) -> Bool {
    return Equals(itemType, gamedataItemType.Cyb_Ability) || Equals(itemType, gamedataItemType.Cyb_HealingAbility) || Equals(itemType, gamedataItemType.Cyb_Launcher);
  }
}

public struct HotkeyManager {

  public final static func InitializeHotkeys(hotkeys: script_ref<[ref<Hotkey>]>) -> Void {
    let freshHotkey: ref<Hotkey>;
    let hotkeyIndex: Int32;
    let hotkeysCount: Int32;
    ArrayClear(Deref(hotkeys));
    hotkeysCount = Cast<Int32>(EnumGetMax(n"EHotkey"));
    hotkeysCount += 1;
    hotkeyIndex = 0;
    while hotkeyIndex < hotkeysCount {
      freshHotkey = Hotkey.Construct(IntEnum<EHotkey>(hotkeyIndex));
      ArrayPush(Deref(hotkeys), freshHotkey);
      hotkeyIndex += 1;
    };
  }

  public final static func AddMissingHotkeys(hotkeys: script_ref<[ref<Hotkey>]>) -> Void {
    let freshHotkey: ref<Hotkey>;
    let hotkeysCount: Int32 = Cast<Int32>(EnumGetMax(n"EHotkey")) + 1;
    let hotkeyIndex: Int32 = ArraySize(Deref(hotkeys));
    while hotkeyIndex < hotkeysCount {
      freshHotkey = Hotkey.Construct(IntEnum<EHotkey>(hotkeyIndex));
      ArrayPush(Deref(hotkeys), freshHotkey);
      hotkeyIndex += 1;
    };
  }

  public final static func IsItemInHotkey(hotkeys: script_ref<[ref<Hotkey>]>, itemID: ItemID) -> Bool {
    let i: Int32 = 0;
    while i < ArraySize(Deref(hotkeys)) {
      if Deref(hotkeys)[i].GetItemID() == itemID {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final static func GetHotkeyTypeForItemID(owner: wref<GameObject>, hotkeys: script_ref<[ref<Hotkey>]>, itemID: ItemID) -> EHotkey {
    let itemData: wref<gameItemData> = RPGManager.GetItemData(owner.GetGame(), owner, itemID);
    let i: Int32 = 0;
    while i < ArraySize(Deref(hotkeys)) {
      if Deref(hotkeys)[i].IsCompatible(itemData.GetItemType()) || Deref(hotkeys)[i].IsCompatible(EquipmentSystem.GetEquipAreaType(itemData.GetID())) {
        return Deref(hotkeys)[i].GetHotkey();
      };
      i += 1;
    };
    return EHotkey.INVALID;
  }

  public final static func GetHotkeyTypeFromItemID(hotkeys: script_ref<[ref<Hotkey>]>, itemID: ItemID) -> EHotkey {
    let i: Int32 = 0;
    while i < ArraySize(Deref(hotkeys)) {
      if Deref(hotkeys)[i].GetItemID() == itemID {
        return Deref(hotkeys)[i].GetHotkey();
      };
      i += 1;
    };
    return EHotkey.INVALID;
  }

  public final static func GetItemIDFromHotkey(hotkeys: script_ref<[ref<Hotkey>]>, hotkey: EHotkey) -> ItemID {
    return Deref(hotkeys)[EnumInt(hotkey)].GetItemID();
  }
}

public class EquipmentSystemPlayerData extends IScriptable {

  public let m_owner: wref<ScriptedPuppet>;

  private persistent let m_ownerID: EntityID;

  private persistent let m_equipment: SLoadout;

  private persistent let m_lastUsedStruct: SLastUsedWeapon;

  private persistent let m_slotActiveItemsInHands: SSlotActiveItems;

  private persistent let m_clothingSlotsInfo: [SSlotInfo];

  private persistent let m_clothingVisualsInfo: [SSlotVisualInfo];

  private let m_visualUnequipTransition: Bool;

  private persistent let m_wardrobeDisabled: Bool;

  @default(EquipmentSystemPlayerData, gameWardrobeClothingSetIndex.INVALID)
  private persistent let m_lastActiveWardrobeSet: gameWardrobeClothingSetIndex;

  private let m_visualTagProcessingInfo: [SVisualTagProcessing];

  private let m_eventsSent: Int32;

  private persistent let m_hotkeys: [ref<Hotkey>];

  private let m_inventoryManager: ref<InventoryDataManagerV2>;

  private let m_wardrobeSystem: ref<WardrobeSystem>;

  private let m_equipPending: Bool;

  private let m_equipAreaIndexCache: [Int32];

  public final func OnAttach() -> Void {
    let scope: array<gamedataItemType>;
    if IsDefined(this.m_owner as PlayerPuppet) {
      if ArraySize(this.m_hotkeys) == 0 {
        HotkeyManager.InitializeHotkeys(this.m_hotkeys);
      } else {
        if ArraySize(this.m_hotkeys) < Cast<Int32>(EnumGetMax(n"EHotkey")) + 1 {
          HotkeyManager.AddMissingHotkeys(this.m_hotkeys);
        };
      };
      this.TryFillCyberwareHotkey();
      scope = this.m_hotkeys[0].GetScope();
      if !ArrayContains(scope, gamedataItemType.Cyb_HealingAbility) {
        ArrayPush(scope, gamedataItemType.Cyb_HealingAbility);
        this.m_hotkeys[0].SetScope(scope);
      };
      this.m_inventoryManager = new InventoryDataManagerV2();
      this.m_inventoryManager.Initialize(this.m_owner as PlayerPuppet);
      this.UnequipPrereqItems();
      this.m_wardrobeSystem = GameInstance.GetWardrobeSystem(this.m_owner.GetGame());
    };
  }

  public final func OnDetach() -> Void;

  public final func OnInitialize() -> Void {
    this.InitializeEquipment();
    this.InitializeClothingSlotsInfo();
    this.InitializeClothingOverrideInfo();
    this.InitializeEquipmentAreaIndexCache();
  }

  public final func OnRestored() -> Void {
    let areaSize: Int32;
    let currEquipSlot: SEquipSlot;
    let currentEquipmentArea: gamedataEquipmentArea;
    let currentItem: ItemID;
    let i: Int32;
    let itemData: ref<gameItemData>;
    let itemRecord: ref<Item_Record>;
    let j: Int32;
    let placementSlot: TweakDBID;
    let playerControlledObject: ref<GameObject>;
    let slotsSize: Int32;
    let transactionSystem: ref<TransactionSystem>;
    this.InitializeEquipmentAreaIndexCache();
    playerControlledObject = GameInstance.GetPlayerSystem(this.m_owner.GetGame()).GetLocalPlayerControlledGameObject();
    transactionSystem = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    if transactionSystem == null {
      return;
    };
    this.m_wardrobeSystem = GameInstance.GetWardrobeSystem(this.m_owner.GetGame());
    this.UnequipFootwearAudio();
    this.UnequipOutfitFootwearAudio();
    this.UnequipAllFoleyAudio();
    i = 0;
    areaSize = ArraySize(this.m_equipment.equipAreas);
    while i < areaSize {
      this.RestoreEquipSlotsData(this.m_equipment.equipAreas[i]);
      currentEquipmentArea = this.m_equipment.equipAreas[i].areaType;
      if Equals(currentEquipmentArea, gamedataEquipmentArea.QuickWheel) {
      } else {
        j = 0;
        slotsSize = ArraySize(this.m_equipment.equipAreas[i].equipSlots);
        while j < slotsSize {
          currEquipSlot = this.m_equipment.equipAreas[i].equipSlots[j];
          currentItem = currEquipSlot.itemID;
          if ItemID.IsValid(currentItem) {
            itemRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(currentItem));
            placementSlot = this.GetPlacementSlot(i, j);
            if itemRecord == null {
            } else {
              if NotEquals(itemRecord.EquipArea().Type(), gamedataEquipmentArea.Consumable) {
                this.ApplyEquipGLPs(currentItem);
                this.ApplySlotGLPs(currEquipSlot.slotID);
              };
              if Equals(itemRecord.EquipArea().Type(), gamedataEquipmentArea.SystemReplacementCW) {
                transactionSystem.AddItemToSlot(this.m_owner, placementSlot, currentItem);
              };
              if Equals(itemRecord.EquipArea().Type(), gamedataEquipmentArea.PersonalLink) {
                transactionSystem.AddItemToSlot(this.m_owner, placementSlot, currentItem);
              };
              if Equals(itemRecord.ItemCategory().Type(), gamedataItemCategory.Clothing) {
                this.AddItemToSlot(transactionSystem, placementSlot, currentItem, currentEquipmentArea);
              } else {
                if itemRecord.UsesVariants() && NotEquals(itemRecord.EquipArea().Type(), gamedataEquipmentArea.ArmsCW) || Equals(itemRecord.EquipArea().Type(), gamedataEquipmentArea.RightArm) {
                  itemData = RPGManager.GetItemData(this.m_owner.GetGame(), this.m_owner, currentItem);
                  if IsDefined(itemData) {
                    itemData.AddStatsOnEquip(this.m_owner);
                  };
                };
              };
              if Equals(currentEquipmentArea, gamedataEquipmentArea.ArmsCW) && placementSlot == t"AttachmentSlots.WeaponLeft" && !ItemID.IsValid(this.GetItemInEquipSlot(gamedataEquipmentArea.RightArm, 0)) {
                this.EquipItem(ItemID.CreateQuery(TweakDBInterface.GetWeaponItemRecord(ItemID.GetTDBID(currentItem)).HolsteredItem().GetID()));
              };
              if this.m_owner == playerControlledObject {
                transactionSystem.OnItemAddedToEquipmentSlot(this.m_owner, currentItem);
                if Equals(currentEquipmentArea, gamedataEquipmentArea.Weapon) || Equals(currentEquipmentArea, gamedataEquipmentArea.WeaponHeavy) || Equals(currentEquipmentArea, gamedataEquipmentArea.ArmsCW) && placementSlot != t"AttachmentSlots.WeaponLeft" {
                  this.SendPSMWeaponManipulationRequest(EquipmentManipulationRequestType.Equip, EquipmentManipulationRequestSlot.Right, gameEquipAnimationType.Default);
                };
              };
              this.SendPaperdollUpdate(this.m_equipment.equipAreas[i], true, placementSlot, j, false, true);
            };
          } else {
            if this.IsSlotOverriden(currentEquipmentArea) {
              this.AddVisualItemToSlot(transactionSystem, currentEquipmentArea);
              this.SendPaperdollUpdate(this.m_equipment.equipAreas[i], true, this.GetPlacementSlotByAreaType(currentEquipmentArea), j, false, true);
            };
          };
          j += 1;
        };
      };
      i += 1;
    };
    this.HotkeysOnRestore();
    this.UpdateWeaponWheel();
  }

  public final func HotkeysOnRestore() -> Void {
    let hotkey: EHotkey;
    let item: ItemID;
    let scope: array<gamedataItemType>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let i: Int32 = 0;
    while i < ArraySize(this.m_hotkeys) {
      hotkey = IntEnum<EHotkey>(i);
      item = this.m_hotkeys[i].GetItemID();
      scope = Hotkey.GetScope(hotkey);
      this.m_hotkeys[i].SetScope(scope);
      if ArraySize(scope) > 0 && !ArrayContains(scope, RPGManager.GetItemType(item)) {
        this.ClearItemFromHotkey(hotkey);
      };
      if ItemID.IsValid(item) {
        transactionSystem.OnItemAddedToEquipmentSlot(this.m_owner, item);
        if Equals(RPGManager.GetItemCategory(item), gamedataItemCategory.Consumable) {
          this.ApplyEquipGLPs(item);
        };
      };
      i += 1;
    };
  }

  private final func TryFillCyberwareHotkey() -> Void {
    let item: ItemID;
    let i: Int32 = 0;
    while i < ArraySize(this.m_hotkeys) {
      if NotEquals(this.m_hotkeys[i].GetHotkey(), EHotkey.LBRB) {
      } else {
        if !ItemID.IsValid(this.m_hotkeys[i].GetItemID()) {
          item = this.GetActiveItem(gamedataEquipmentArea.SystemReplacementCW);
          if ItemID.IsValid(item) {
            this.AssignItemToHotkey(item, EHotkey.LBRB);
          };
        };
      };
      i += 1;
    };
  }

  public final func SetOwner(owner: ref<ScriptedPuppet>) -> Void {
    this.m_owner = owner;
    this.m_ownerID = owner.GetEntityID();
  }

  public final func GetOwner() -> wref<ScriptedPuppet> {
    return this.m_owner;
  }

  public final func GetOwnerID() -> EntityID {
    return this.m_ownerID;
  }

  public final func GetEquipment() -> SLoadout {
    return this.m_equipment;
  }

  public final func GetLastUsedStruct() -> SLastUsedWeapon {
    return this.m_lastUsedStruct;
  }

  public final func ClearLastUsedStruct() -> Void {
    let emptyLastUsedStruct: SLastUsedWeapon;
    this.m_lastUsedStruct = emptyLastUsedStruct;
  }

  public final func GetSlotActiveItemStruct() -> SSlotActiveItems {
    return this.m_slotActiveItemsInHands;
  }

  private final func GetPlayerEquipmentAreas(out list: [wref<EquipmentArea_Record>]) -> Void {
    return TweakDBInterface.GetCharacterRecord(this.m_owner.GetRecordID()).EquipmentAreas(list);
  }

  private final func GetEquipAreaRecordByType(areaType: gamedataEquipmentArea) -> ref<EquipmentArea_Record> {
    let i: Int32;
    let list: array<wref<EquipmentArea_Record>>;
    let retVal: ref<EquipmentArea_Record>;
    let size: Int32;
    this.GetPlayerEquipmentAreas(list);
    i = 0;
    size = ArraySize(list);
    while i < size {
      retVal = list[i];
      if Equals(retVal.Type(), areaType) {
        return retVal;
      };
      i += 1;
    };
    return null;
  }

  private final func InitializeEquipment() -> Void {
    let equipArea: SEquipArea;
    let equipAreaRecord: ref<EquipmentArea_Record>;
    let equipAreas: array<wref<EquipmentArea_Record>>;
    let i: Int32;
    let size: Int32;
    this.GetPlayerEquipmentAreas(equipAreas);
    i = 0;
    size = ArraySize(equipAreas);
    while i < size {
      equipArea = new SEquipArea();
      equipAreaRecord = equipAreas[i];
      equipArea.areaType = equipAreaRecord.Type();
      equipArea.activeIndex = 0;
      this.InitializeEquipmentArea(equipAreaRecord, equipArea);
      ArrayPush(this.m_equipment.equipAreas, equipArea);
      i += 1;
    };
  }

  private final func InitializeEquipmentArea(equipAreaRecord: ref<EquipmentArea_Record>, out equipArea: SEquipArea) -> Void {
    let equipSlotRecords: array<wref<EquipSlot_Record>>;
    equipAreaRecord.EquipSlots(equipSlotRecords);
    this.InitializeEquipSlotsFromRecords(equipSlotRecords, equipArea.equipSlots);
  }

  private final func InitializeEquipSlotsFromRecords(slotRecords: [wref<EquipSlot_Record>], out equipSlots: [SEquipSlot]) -> Void {
    let equipSlot: SEquipSlot;
    let i: Int32 = 0;
    let size: Int32 = ArraySize(slotRecords);
    while i < size {
      equipSlot = new SEquipSlot();
      this.InitializeEquipSlotFromRecord(slotRecords[i], equipSlot);
      if ArraySize(equipSlots) > i {
        if equipSlot.unlockPrereq.IsFulfilled(this.m_owner.GetGame(), this.m_owner) {
          equipSlot.itemID = equipSlots[i].itemID;
        };
        equipSlots[i] = equipSlot;
      } else {
        ArrayPush(equipSlots, equipSlot);
      };
      i += 1;
    };
  }

  private final func InitializeEquipSlotFromRecord(slotRecord: ref<EquipSlot_Record>, out equipSlot: SEquipSlot) -> Void {
    let equipPrereqID: TweakDBID = slotRecord.UnlockPrereqRecord().GetID();
    if TDBID.IsValid(equipPrereqID) {
      equipSlot.unlockPrereq = IPrereq.CreatePrereq(equipPrereqID);
    };
    equipSlot.visibleWhenLocked = slotRecord.VisibleWhenLocked();
    equipSlot.slotID = slotRecord.GetID();
  }

  private final func RestoreEquipSlotsData(out equipArea: SEquipArea) -> Void {
    let areaRecord: wref<EquipmentArea_Record> = this.GetEquipAreaRecordByType(equipArea.areaType);
    this.InitializeEquipmentArea(areaRecord, equipArea);
  }

  private final func InitializeEquipmentAreaIndexCache() -> Void {
    let equipArea: SEquipArea;
    let i: Int32;
    let limit: Int32;
    ArrayResize(this.m_equipAreaIndexCache, 44);
    i = 0;
    limit = ArraySize(this.m_equipAreaIndexCache);
    while i < limit {
      this.m_equipAreaIndexCache[i] = -1;
      i += 1;
    };
    i = 0;
    limit = ArraySize(this.m_equipment.equipAreas);
    while i < limit {
      equipArea = this.m_equipment.equipAreas[i];
      if NotEquals(equipArea.areaType, gamedataEquipmentArea.Invalid) {
        this.m_equipAreaIndexCache[EnumInt(equipArea.areaType)] = i;
      };
      i += 1;
    };
  }

  private final func InitializeClothingSlotsInfo() -> Void {
    ArrayClear(this.m_clothingSlotsInfo);
    ArrayPush(this.m_clothingSlotsInfo, this.CreateSlotInfo(gamedataEquipmentArea.OuterChest, "AttachmentSlots.Torso", n"hide_T2"));
    ArrayPush(this.m_clothingSlotsInfo, this.CreateSlotInfo(gamedataEquipmentArea.InnerChest, "AttachmentSlots.Chest", n"hide_T1"));
    ArrayPush(this.m_clothingSlotsInfo, this.CreateSlotInfo(gamedataEquipmentArea.Legs, "AttachmentSlots.Legs", n"hide_L1"));
    ArrayPush(this.m_clothingSlotsInfo, this.CreateSlotInfo(gamedataEquipmentArea.Feet, "AttachmentSlots.Feet", n"hide_S1"));
    ArrayPush(this.m_clothingSlotsInfo, this.CreateSlotInfo(gamedataEquipmentArea.Head, "AttachmentSlots.Head", n"hide_H1"));
    ArrayPush(this.m_clothingSlotsInfo, this.CreateSlotInfo(gamedataEquipmentArea.Face, "AttachmentSlots.Eyes", n"hide_F1"));
    ArrayPush(this.m_clothingSlotsInfo, this.CreateSlotInfo(gamedataEquipmentArea.UnderwearBottom, "AttachmentSlots.UnderwearBottom", n"hide_Genitals"));
  }

  public final func InitializeClothingOverrideInfo() -> Void {
    ArrayClear(this.m_clothingVisualsInfo);
    ArrayPush(this.m_clothingVisualsInfo, this.CreateClothingVisualSlotInfo(gamedataEquipmentArea.Outfit));
    ArrayPush(this.m_clothingVisualsInfo, this.CreateClothingVisualSlotInfo(gamedataEquipmentArea.OuterChest));
    ArrayPush(this.m_clothingVisualsInfo, this.CreateClothingVisualSlotInfo(gamedataEquipmentArea.InnerChest));
    ArrayPush(this.m_clothingVisualsInfo, this.CreateClothingVisualSlotInfo(gamedataEquipmentArea.Legs));
    ArrayPush(this.m_clothingVisualsInfo, this.CreateClothingVisualSlotInfo(gamedataEquipmentArea.Feet));
    ArrayPush(this.m_clothingVisualsInfo, this.CreateClothingVisualSlotInfo(gamedataEquipmentArea.Head));
    ArrayPush(this.m_clothingVisualsInfo, this.CreateClothingVisualSlotInfo(gamedataEquipmentArea.Face));
    ArrayPush(this.m_clothingVisualsInfo, this.CreateClothingVisualSlotInfo(gamedataEquipmentArea.UnderwearTop));
    ArrayPush(this.m_clothingVisualsInfo, this.CreateClothingVisualSlotInfo(gamedataEquipmentArea.UnderwearBottom));
  }

  public final func IsClothingVisualsInfoEmpty() -> Bool {
    return ArraySize(this.m_clothingVisualsInfo) == 0;
  }

  private final func CreateSlotInfo(area: gamedataEquipmentArea, const slot: script_ref<String>, visualTag: CName) -> SSlotInfo {
    let slotInfo: SSlotInfo;
    slotInfo.areaType = area;
    slotInfo.equipSlot = TDBID.Create(Deref(slot));
    slotInfo.visualTag = visualTag;
    return slotInfo;
  }

  private final func CreateClothingVisualSlotInfo(area: gamedataEquipmentArea) -> SSlotVisualInfo {
    let slotInfo: SSlotVisualInfo;
    slotInfo.areaType = area;
    return slotInfo;
  }

  public final func IsEquipPending() -> Bool {
    return this.m_equipPending;
  }

  public final func EquipItem(itemID: ItemID, opt blockActiveSlotsUpdate: Bool, opt forceEquipWeapon: Bool) -> Void {
    let equipArea: SEquipArea;
    let equipAreaIndex: Int32;
    let equipAreaType: gamedataEquipmentArea;
    let equipAtIndex: Int32;
    let equipSlot: SEquipSlot;
    let i: Int32;
    if ItemID.IsValid(itemID) && !this.IsEquipped(itemID) {
      equipAreaType = EquipmentSystem.GetEquipAreaType(itemID);
      equipAreaIndex = this.GetEquipAreaIndex(equipAreaType);
      equipArea = this.m_equipment.equipAreas[equipAreaIndex];
      i = 0;
      while i < ArraySize(equipArea.equipSlots) {
        equipSlot = equipArea.equipSlots[i];
        if !this.IsSlotLocked(equipSlot, false) && !ItemID.IsValid(equipSlot.itemID) {
          this.EquipItem(itemID, i, blockActiveSlotsUpdate, forceEquipWeapon);
          return;
        };
        i += 1;
      };
      this.EquipItem(itemID, equipArea.activeIndex, blockActiveSlotsUpdate, forceEquipWeapon);
    } else {
      if ItemID.IsValid(itemID) && this.IsEquipped(itemID) {
        equipAtIndex = this.GetSlotIndex(itemID);
        if equipAtIndex >= 0 {
          this.EquipItem(itemID, equipAtIndex, blockActiveSlotsUpdate, forceEquipWeapon);
        };
      };
    };
  }

  private final func EquipItem(itemID: ItemID, slotIndex: Int32, opt blockActiveSlotsUpdate: Bool, opt forceEquipWeapon: Bool) -> Void {
    let currentItem: ItemID;
    let currentItemData: ref<gameItemData>;
    let cyberwareType: CName;
    let equipArea: SEquipArea;
    let equipAreaIndex: Int32;
    let equipSlot: SEquipSlot;
    let equipmentArea: gamedataEquipmentArea;
    let i: Int32;
    let placementSlot: TweakDBID;
    let transactionSystem: ref<TransactionSystem>;
    let weaponRecord: ref<WeaponItem_Record>;
    let itemData: ref<gameItemData> = RPGManager.GetItemData(this.m_owner.GetGame(), this.m_owner, itemID);
    if !this.IsEquippable(itemData) {
      return;
    };
    equipmentArea = EquipmentSystem.GetEquipAreaType(itemID);
    equipAreaIndex = this.GetEquipAreaIndex(equipmentArea);
    equipArea = this.m_equipment.equipAreas[equipAreaIndex];
    equipSlot = equipArea.equipSlots[slotIndex];
    currentItem = equipSlot.itemID;
    currentItemData = RPGManager.GetItemData(this.m_owner.GetGame(), this.m_owner, currentItem);
    if IsDefined(currentItemData) && currentItemData.HasTag(n"UnequipBlocked") {
      return;
    };
    if Equals(equipmentArea, gamedataEquipmentArea.Outfit) && this.IsVisualSetActive() {
      this.UnequipWardrobeSet();
    };
    transactionSystem = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    this.m_equipPending = true;
    if this.IsItemOfCategory(itemID, gamedataItemCategory.Weapon) && equipArea.activeIndex == slotIndex && this.CheckWeaponAgainstGameplayRestrictions(itemID, true) && !blockActiveSlotsUpdate {
      this.SetSlotActiveItem(EquipmentManipulationRequestSlot.Right, itemID);
      this.SetLastUsedItem(itemID);
      this.SendPSMWeaponManipulationRequest(EquipmentManipulationRequestType.Equip, EquipmentManipulationRequestSlot.Right, gameEquipAnimationType.Default);
    } else {
      if this.IsItemOfCategory(itemID, gamedataItemCategory.Weapon) && forceEquipWeapon && this.CheckWeaponAgainstGameplayRestrictions(itemID, true) {
        this.m_equipment.equipAreas[equipAreaIndex].equipSlots[slotIndex].itemID = itemID;
        this.SetSlotActiveItem(EquipmentManipulationRequestSlot.Right, itemID);
        this.UpdateEquipAreaActiveIndex(itemID);
        this.SetLastUsedItem(itemID);
      } else {
        this.UnequipItem(equipAreaIndex, slotIndex);
        cyberwareType = TweakDBInterface.GetCName(ItemID.GetTDBID(itemID) + t".cyberwareType", n"None");
        i = 0;
        while i < ArraySize(this.m_equipment.equipAreas[equipAreaIndex].equipSlots) {
          if Equals(cyberwareType, TweakDBInterface.GetCName(ItemID.GetTDBID(this.m_equipment.equipAreas[equipAreaIndex].equipSlots[i].itemID) + t".cyberwareType", n"type")) {
            this.UnequipItem(equipAreaIndex, i);
          };
          i += 1;
        };
      };
    };
    this.m_equipPending = false;
    this.m_equipment.equipAreas[equipAreaIndex].equipSlots[slotIndex].itemID = itemID;
    placementSlot = this.GetPlacementSlot(equipAreaIndex, slotIndex);
    if Equals(itemData.GetItemType(), gamedataItemType.Cyb_StrongArms) {
      this.HandleStrongArmsEquip(itemID);
    };
    if placementSlot == t"AttachmentSlots.WeaponRight" || placementSlot == t"AttachmentSlots.WeaponLeft" {
      weaponRecord = TweakDBInterface.GetWeaponItemRecord(ItemID.GetTDBID(itemID));
      if IsDefined(weaponRecord) && IsDefined(weaponRecord.HolsteredItem()) {
        EquipmentSystemPlayerData.UpdateArmSlot(this.m_owner as PlayerPuppet, itemID, true);
      };
    };
    if placementSlot != t"AttachmentSlots.WeaponRight" && placementSlot != t"AttachmentSlots.WeaponLeft" && placementSlot != t"AttachmentSlots.Consumable" {
      if !transactionSystem.HasItemInSlot(this.m_owner, placementSlot, itemID) {
        transactionSystem.RemoveItemFromSlot(this.m_owner, placementSlot);
        this.AddItemToSlot(transactionSystem, placementSlot, itemID, equipmentArea);
      };
    };
    this.ApplyEquipGLPs(itemID);
    this.ApplySlotGLPs(equipSlot.slotID);
    if itemData.UsesVariants() && NotEquals(equipArea.areaType, gamedataEquipmentArea.ArmsCW) || Equals(equipArea.areaType, gamedataEquipmentArea.RightArm) {
      itemData.AddStatsOnEquip(this.m_owner);
    };
    this.UpdateWeaponWheel();
    this.UpdateQuickWheel();
    this.UpdateUIBBAreaChanged(equipmentArea, slotIndex);
    this.SendPaperdollUpdate(this.m_equipment.equipAreas[equipAreaIndex], true, placementSlot, slotIndex, true);
    i = 0;
    while i < ArraySize(this.m_hotkeys) {
      if this.m_hotkeys[i].IsCompatible(itemData.GetItemType()) || this.m_hotkeys[i].IsCompatible(EquipmentSystem.GetEquipAreaType(itemData.GetID())) {
        this.AssignItemToHotkey(itemData.GetID(), this.m_hotkeys[i].GetHotkey());
      };
      i += 1;
    };
    EquipmentSystem.GetInstance(this.m_owner).Debug_FillESSlotData(slotIndex, this.m_equipment.equipAreas[equipAreaIndex].areaType, itemID, this.m_owner);
    if ItemID.IsValid(currentItem) && currentItem != itemID {
      transactionSystem.OnItemRemovedFromEquipmentSlot(this.m_owner, currentItem);
    };
    transactionSystem.OnItemAddedToEquipmentSlot(this.m_owner, itemID);
    if this.IsItemOfCategory(itemID, gamedataItemCategory.Cyberware) || Equals(equipArea.areaType, gamedataEquipmentArea.ArmsCW) {
      this.CheckCyberjunkieAchievement();
    };
    if EquipmentSystem.IsItemCyberdeck(itemID) {
      PlayerPuppet.ChacheQuickHackListCleanup(this.m_owner);
    };
  }

  private final func AddItemToSlot(transactionSystem: ref<TransactionSystem>, slot: TweakDBID, itemID: ItemID, area: gamedataEquipmentArea) -> Bool {
    let itemData: wref<gameItemData>;
    let previewItem: ItemID;
    let visualItem: ItemID;
    if RPGManager.IsItemClothing(itemID) {
      itemData = RPGManager.GetItemData(this.m_owner.GetGame(), this.m_owner, itemID);
      if itemData.HasTag(n"TransmogBlocked") {
        if this.IsVisualSetActive() {
          this.UnequipVisuals(area);
        };
        this.m_clothingVisualsInfo[this.GetVisualSlotIndex(area)].visualItem = itemID;
      } else {
        if this.IsSlotHidden(area) {
          return transactionSystem.AddItemToSlot(this.m_owner, slot, itemID, n"empty_appearance_default");
        };
        if this.IsSlotOverriden(area) {
          visualItem = this.GetSlotOverridenVisualItem(area);
          if !ItemID.IsValid(this.GetActiveItem(area)) && ItemID.IsValid(itemID) {
            previewItem = transactionSystem.CreatePreviewItemID(visualItem);
            transactionSystem.RemoveItem(this.m_owner, previewItem, 1);
          };
          this.SendEquipAudioEvents(visualItem);
          return transactionSystem.AddItemToSlot(this.m_owner, slot, itemID, visualItem);
        };
      };
    };
    this.SendEquipAudioEvents(itemID);
    return transactionSystem.AddItemToSlot(this.m_owner, slot, itemID);
  }

  private final func AddVisualItemToSlot(transactionSystem: ref<TransactionSystem>, area: gamedataEquipmentArea) -> Bool {
    let previewItem: ItemID = transactionSystem.CreatePreviewItemID(this.GetSlotOverridenVisualItem(area));
    if transactionSystem.AddItemToSlot(this.m_owner, this.GetPlacementSlotByAreaType(area), previewItem) {
      this.SendEquipAudioEvents(previewItem);
      return true;
    };
    return false;
  }

  private final func SendPaperdollUpdate(const area: script_ref<SEquipArea>, equipped: Bool, slot: TweakDBID, opt slotindex: Int32, opt ignoreSlot: Bool, opt force: Bool) -> Void {
    let paperdollEquipData: SPaperdollEquipData;
    paperdollEquipData.equipArea = Deref(area);
    paperdollEquipData.equipped = equipped;
    paperdollEquipData.placementSlot = slot;
    paperdollEquipData.slotIndex = slotindex;
    if ignoreSlot || TDBID.IsValid(paperdollEquipData.placementSlot) {
      this.UpdateEquipmentUIBB(paperdollEquipData, force);
    };
  }

  private final func SendEquipAudioEvents(itemID: ItemID) -> Void {
    let audioSystem: ref<AudioSystem>;
    let audioEventFoley: ref<AudioEvent> = new AudioEvent();
    audioEventFoley.eventName = n"equipItem";
    audioEventFoley.nameData = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID)).AppearanceName();
    this.m_owner.QueueEvent(audioEventFoley);
    if Equals(RPGManager.GetItemType(itemID), gamedataItemType.Clo_Feet) {
      audioSystem = GameInstance.GetAudioSystem(this.m_owner.GetGame());
      audioSystem.EquipNewFootwearByPlayer(itemID);
    };
    if Equals(RPGManager.GetItemType(itemID), gamedataItemType.Clo_Outfit) {
      audioSystem = GameInstance.GetAudioSystem(this.m_owner.GetGame());
      audioSystem.EquipNewOutfitByPlayer(itemID);
    };
  }

  private final func UnequipFootwearAudio() -> Void {
    let unequipFootwearEvent: ref<AudioEvent> = new AudioEvent();
    unequipFootwearEvent.eventName = n"equipFootwear";
    unequipFootwearEvent.nameData = n"None";
    this.m_owner.QueueEvent(unequipFootwearEvent);
  }

  private final func UnequipOutfitFootwearAudio() -> Void {
    let unequipFootwearEvent: ref<AudioEvent> = new AudioEvent();
    unequipFootwearEvent.eventName = n"equipOutfitFootwear";
    unequipFootwearEvent.nameData = n"None";
    this.m_owner.QueueEvent(unequipFootwearEvent);
  }

  private final func UnequipAllFoleyAudio() -> Void {
    let unequipAllFoleyEvent: ref<AudioEvent> = new AudioEvent();
    unequipAllFoleyEvent.eventName = n"unequipAll";
    unequipAllFoleyEvent.nameData = n"None";
    this.m_owner.QueueEvent(unequipAllFoleyEvent);
  }

  private final func SendUnequipAudioEvents(itemID: ItemID) -> Void {
    let audioSystem: ref<AudioSystem>;
    let audioEventFoley: ref<AudioEvent> = new AudioEvent();
    audioEventFoley.eventName = n"unequipItem";
    audioEventFoley.nameData = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID)).AppearanceName();
    this.m_owner.QueueEvent(audioEventFoley);
    if Equals(RPGManager.GetItemType(itemID), gamedataItemType.Clo_Feet) {
      this.UnequipFootwearAudio();
    };
    if Equals(RPGManager.GetItemType(itemID), gamedataItemType.Clo_Outfit) {
      audioSystem = GameInstance.GetAudioSystem(this.m_owner.GetGame());
      audioSystem.UnequipOutfitByPlayer(itemID);
    };
  }

  private final func EquipVisuals(item: ItemID) -> Void {
    let area: gamedataEquipmentArea = EquipmentSystem.GetEquipAreaType(item);
    let visualSlotIndex: Int32 = this.GetVisualSlotIndex(area);
    let equipAreaIndex: Int32 = this.GetEquipAreaIndex(area);
    let activeItem: ItemID = this.GetActiveItem(area);
    let oldVisualItem: ItemID = oldVisualItem = this.m_clothingVisualsInfo[visualSlotIndex].visualItem;
    if visualSlotIndex < 0 || this.IsTransmogBlockedOnSlot(area) {
      return;
    };
    if ItemID.IsValid(oldVisualItem) {
      this.OnUnequipUpdateVisuals(oldVisualItem, area);
      this.SendUnequipAudioEvents(oldVisualItem);
      if !ItemID.IsValid(activeItem) {
        this.ClearPreviewItem(area);
      };
    } else {
      this.SendUnequipAudioEvents(activeItem);
      this.OnUnequipUpdateVisuals(activeItem, area);
    };
    this.m_clothingVisualsInfo[visualSlotIndex].visualItem = item;
    this.m_clothingVisualsInfo[visualSlotIndex].isHidden = false;
    this.ChangeAppearanceToItem(item);
    this.OnEquipProcessVisualTags(item);
    this.SendPaperdollUpdate(this.m_equipment.equipAreas[equipAreaIndex], true, this.GetPlacementSlotByAreaType(area), 0, false, true);
  }

  private final func UnequipVisuals(area: gamedataEquipmentArea) -> Void {
    let visualSlotIndex: Int32 = this.GetVisualSlotIndex(area);
    let equipAreaIndex: Int32 = this.GetEquipAreaIndex(area);
    let visualItem: ItemID = this.GetVisualItemInSlot(area);
    if visualSlotIndex < 0 || this.IsTransmogBlockedOnSlot(area) {
      return;
    };
    this.SendUnequipAudioEvents(visualItem);
    this.OnUnequipProcessVisualTags(visualItem);
    if !ItemID.IsValid(this.GetActiveItem(area)) {
      this.ClearPreviewItem(area);
    } else {
      this.ResetItemAppearanceEvent(area);
    };
    this.m_clothingVisualsInfo[visualSlotIndex].isHidden = false;
    this.m_clothingVisualsInfo[visualSlotIndex].visualItem = ItemID.None();
    this.SendPaperdollUpdate(this.m_equipment.equipAreas[equipAreaIndex], false, this.GetPlacementSlotByAreaType(area), 0, false, true);
  }

  private final func ClearVisuals(area: gamedataEquipmentArea) -> Void {
    let equippedItemData: wref<gameItemData>;
    let visualSlotIndex: Int32 = this.GetVisualSlotIndex(area);
    let equipAreaIndex: Int32 = this.GetEquipAreaIndex(area);
    let activeItem: ItemID = this.GetActiveItem(area);
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    if visualSlotIndex < 0 || this.IsTransmogBlockedOnSlot(area) {
      return;
    };
    if ItemID.IsValid(activeItem) {
      equippedItemData = transactionSystem.GetItemData(this.m_owner, activeItem);
      if IsDefined(equippedItemData) && equippedItemData.HasTag(n"TransmogBlocked") {
        return;
      };
    };
    if !ItemID.IsValid(activeItem) {
      this.ClearPreviewItem(area);
    } else {
      this.OnUnequipUpdateVisuals(activeItem, area);
      this.ClearItemAppearance(area);
    };
    this.m_clothingVisualsInfo[visualSlotIndex].visualItem = ItemID.None();
    this.m_clothingVisualsInfo[visualSlotIndex].isHidden = true;
    this.SendPaperdollUpdate(this.m_equipment.equipAreas[equipAreaIndex], false, this.GetPlacementSlotByAreaType(area), 0, false, true);
  }

  private final func ClearPreviewItem(area: gamedataEquipmentArea) -> Void {
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let previewItem: ItemID = transactionSystem.CreatePreviewItemID(this.GetVisualItemInSlot(area));
    transactionSystem.ClearAttachmentAppearance(this.m_owner, this.GetPlacementSlotByAreaType(area));
    transactionSystem.RemoveItem(this.m_owner, previewItem, 1);
  }

  private final func IsTransmogBlockedOnSlot(area: gamedataEquipmentArea) -> Bool {
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let activeItem: ItemID = this.GetActiveItem(area);
    let equippedItemData: wref<gameItemData> = transactionSystem.GetItemData(this.m_owner, activeItem);
    return IsDefined(equippedItemData) && equippedItemData.HasTag(n"TransmogBlocked");
  }

  public final func QuestHideSlot(area: gamedataEquipmentArea) -> Void {
    this.ClearVisuals(area);
  }

  public final func QuestRestoreSlot(area: gamedataEquipmentArea) -> Void {
    if this.IsVisualSetActive() {
      this.EquipVisuals(this.GetVisualItemInSlot(area));
    } else {
      if ItemID.IsValid(this.GetActiveItem(area)) {
        this.ResetItemAppearanceEvent(area);
      };
    };
  }

  private final func ChangeAppearanceToItem(item: ItemID) -> Void {
    let oldItemID: ItemID = this.GetItemInEquipSlot(EquipmentSystem.GetEquipAreaType(item), 0);
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    if ItemID.IsValid(oldItemID) {
      transactionSystem.ChangeItemAppearanceByItemID(this.m_owner, oldItemID, item);
    } else {
      transactionSystem.GivePreviewItemByItemID(this.m_owner, item);
      transactionSystem.AddItemToSlot(this.m_owner, EquipmentSystem.GetPlacementSlot(item), transactionSystem.CreatePreviewItemID(item), true);
    };
    this.SendEquipAudioEvents(item);
  }

  private final func ProcessGadgetsTutorials(item: ItemID) -> Void {
    let questSystem: ref<QuestsSystem> = GameInstance.GetQuestsSystem(this.m_owner.GetGame());
    if Equals(RPGManager.GetItemCategory(item), gamedataItemCategory.Gadget) && questSystem.GetFact(n"grenade_use_tutorial") == 0 && questSystem.GetFact(n"disable_tutorials") == 0 {
      questSystem.SetFact(n"grenade_use_tutorial", 1);
    };
    if (Equals(RPGManager.GetItemType(item), gamedataItemType.Con_Inhaler) || Equals(RPGManager.GetItemType(item), gamedataItemType.Con_Injector)) && questSystem.GetFact(n"consumable_use_tutorial") == 0 && questSystem.GetFact(n"disable_tutorials") == 0 {
      questSystem.SetFact(n"consumable_use_tutorial", 1);
    };
  }

  public final func OnEquipProcessVisualTags(itemID: ItemID) -> Void {
    let i: Int32;
    let isUnderwearHidden: Bool;
    let visualTagsTweakDB: array<CName>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let areaType: gamedataEquipmentArea = EquipmentSystem.GetEquipAreaType(itemID);
    if this.IsVisualTagActive(this.GetVisualTagByAreaType(areaType)) {
      this.ClearItemAppearanceEvent(areaType);
    } else {
      visualTagsTweakDB = transactionSystem.GetVisualTagsByItemID(itemID, this.m_owner);
      i = 0;
      while i < ArraySize(visualTagsTweakDB) {
        if this.GetSlotsInfoIndex(visualTagsTweakDB[i]) > -1 {
          this.ClearItemAppearanceEvent(this.m_clothingSlotsInfo[this.GetSlotsInfoIndex(visualTagsTweakDB[i])].areaType);
        };
        i += 1;
      };
      if Equals(areaType, gamedataEquipmentArea.OuterChest) && this.IsPartialVisualTagActive(itemID, transactionSystem) {
        this.UpdateInnerChest();
      };
      if (!this.IsUnderwearHidden() || Equals(areaType, gamedataEquipmentArea.UnderwearBottom)) && (ItemID.IsValid(this.GetVisualItemInSlot(gamedataEquipmentArea.Legs)) || this.IsVisualTagActive(n"hide_L1")) {
        this.ClearItemAppearanceEvent(gamedataEquipmentArea.UnderwearBottom);
      };
      isUnderwearHidden = this.EvaluateUnderwearTopHiddenState();
      if (!isUnderwearHidden || Equals(areaType, gamedataEquipmentArea.UnderwearTop)) && this.IsBuildCensored() && (ItemID.IsValid(this.GetVisualItemInSlot(gamedataEquipmentArea.InnerChest)) || this.IsVisualTagActive(n"hide_T1")) {
        this.ClearItemAppearanceEvent(gamedataEquipmentArea.UnderwearTop);
      };
    };
  }

  private final func GetSlotsInfoIndex(tag: CName) -> Int32 {
    let i: Int32 = 0;
    while i < ArraySize(this.m_clothingSlotsInfo) {
      if Equals(tag, this.m_clothingSlotsInfo[i].visualTag) {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  private final func ClearItemAppearanceEvent(areaType: gamedataEquipmentArea) -> Void {
    let evt: ref<ClearItemAppearanceEvent>;
    let resetItemID: ItemID;
    this.HideItem(areaType, true);
    resetItemID = this.GetActiveItem(areaType);
    if !ItemID.IsValid(resetItemID) {
      resetItemID = this.GetVisualItemInSlot(areaType);
    };
    if ItemID.IsValid(resetItemID) {
      evt = new ClearItemAppearanceEvent();
      evt.itemID = resetItemID;
      this.m_eventsSent += 1;
      this.UpdateVisualTagProcessingInfo(areaType, false);
      this.m_owner.QueueEvent(evt);
    };
  }

  public final func OnClearItemAppearance(resetItemID: ItemID) -> Void {
    this.OnUnequipProcessVisualTags(resetItemID, true);
    this.m_eventsSent -= 1;
    this.FinalizeVisualTagProcessing();
  }

  protected final func ClearItemAppearance(area: gamedataEquipmentArea) -> Void {
    let emptySlotTransmog: Bool;
    let equipAreaIndex: Int32;
    let slotIndex: Int32;
    let transactionSystem: ref<TransactionSystem>;
    let currentID: ItemID = this.GetActiveItem(area);
    if !ItemID.IsValid(currentID) && ItemID.IsValid(this.GetVisualItemInSlot(area)) {
      emptySlotTransmog = true;
      currentID = this.GetVisualItemInSlot(area);
    };
    equipAreaIndex = this.GetEquipAreaIndex(area);
    slotIndex = this.GetSlotIndex(currentID);
    if emptySlotTransmog {
      this.ClearPreviewItem(area);
    } else {
      transactionSystem = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
      transactionSystem.ChangeItemAppearanceByName(this.m_owner, currentID, n"empty_appearance_default");
    };
    this.SendPaperdollUpdate(this.m_equipment.equipAreas[equipAreaIndex], false, this.GetPlacementSlot(equipAreaIndex, slotIndex), slotIndex, false, true);
    this.SendUnequipAudioEvents(currentID);
  }

  public final func OnUnequipProcessVisualTags(currentItem: ItemID, opt forceIfHidden: Bool) -> Void {
    let i: Int32;
    let transactionSystem: ref<TransactionSystem>;
    let visualTagsTweakDB: array<CName>;
    let area: gamedataEquipmentArea = EquipmentSystem.GetEquipAreaType(currentItem);
    if !ItemID.IsValid(currentItem) || this.IsSlotHidden(area) && !forceIfHidden {
      return;
    };
    transactionSystem = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    visualTagsTweakDB = transactionSystem.GetVisualTagsByItemID(currentItem, this.m_owner);
    i = 0;
    while i < ArraySize(visualTagsTweakDB) {
      if this.GetSlotsInfoIndex(visualTagsTweakDB[i]) > -1 {
        this.ResetItemAppearanceEvent(this.m_clothingSlotsInfo[this.GetSlotsInfoIndex(visualTagsTweakDB[i])].areaType);
      };
      i += 1;
    };
    this.OnUnequipUpdateVisuals(currentItem, area);
  }

  private final func OnUnequipUpdateVisuals(currentItem: ItemID, area: gamedataEquipmentArea) -> Void {
    let isUnderwearHidden: Bool;
    if Equals(area, gamedataEquipmentArea.OuterChest) && this.IsPartialVisualTagActive(currentItem, GameInstance.GetTransactionSystem(this.m_owner.GetGame())) {
      this.UpdateInnerChest();
    };
    if this.IsUnderwearHidden() && this.ShouldUnderwearBeVisible(currentItem) {
      this.ResetItemAppearanceEvent(gamedataEquipmentArea.UnderwearBottom);
    };
    if NotEquals(area, gamedataEquipmentArea.UnderwearTop) {
      isUnderwearHidden = this.EvaluateUnderwearTopHiddenState();
      if isUnderwearHidden && this.IsBuildCensored() && this.ShouldUnderwearTopBeVisible(currentItem) {
        this.ResetItemAppearanceEvent(gamedataEquipmentArea.UnderwearTop);
      };
    };
  }

  private final func ResetItemAppearanceEvent(area: gamedataEquipmentArea) -> Void {
    let evt: ref<ResetItemAppearanceEvent>;
    let resetItemID: ItemID;
    this.HideItem(area, false);
    resetItemID = this.GetActiveItem(area);
    if ItemID.IsValid(resetItemID) {
      evt = new ResetItemAppearanceEvent();
      evt.itemID = resetItemID;
      this.m_eventsSent += 1;
      this.UpdateVisualTagProcessingInfo(area, true);
      this.m_owner.QueueEvent(evt);
    };
  }

  public final func OnResetItemAppearance(resetItemID: ItemID) -> Void {
    this.OnEquipProcessVisualTags(resetItemID);
    this.m_eventsSent -= 1;
    this.FinalizeVisualTagProcessing();
  }

  private final func ResetItemAppearance(area: gamedataEquipmentArea, opt force: Bool) -> Void {
    let hasVisualOverride: Bool;
    let transactionSystem: ref<TransactionSystem>;
    let resetItemID: ItemID = this.GetActiveItem(area);
    let equipAreaIndex: Int32 = this.GetEquipAreaIndex(area);
    let slotIndex: Int32 = this.GetSlotIndex(resetItemID);
    let visualSlotIndex: Int32 = this.GetVisualSlotIndex(area);
    if ItemID.IsValid(this.m_clothingVisualsInfo[visualSlotIndex].visualItem) && resetItemID != this.m_clothingVisualsInfo[visualSlotIndex].visualItem {
      hasVisualOverride = true;
    };
    transactionSystem = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    if visualSlotIndex >= 0 && hasVisualOverride {
      transactionSystem.ChangeItemAppearanceByItemID(this.m_owner, resetItemID, this.m_clothingVisualsInfo[visualSlotIndex].visualItem);
    } else {
      transactionSystem.ResetItemAppearance(this.m_owner, resetItemID);
    };
    this.SendPaperdollUpdate(this.m_equipment.equipAreas[equipAreaIndex], true, this.GetPlacementSlot(equipAreaIndex, slotIndex), slotIndex, false, force);
    this.SendEquipAudioEvents(resetItemID);
  }

  private final func UpdateInnerChest() -> Void {
    let itemID: ItemID = this.GetActiveItem(gamedataEquipmentArea.InnerChest);
    if ItemID.IsValid(itemID) && !this.IsSlotHidden(gamedataEquipmentArea.InnerChest) {
      this.ResetItemAppearance(gamedataEquipmentArea.InnerChest, true);
    };
  }

  private final func UpdateVisualTagProcessingInfo(area: gamedataEquipmentArea, show: Bool) -> Void {
    let info: SVisualTagProcessing;
    let updated: Bool;
    info.areaType = area;
    info.showItem = show;
    let i: Int32 = 0;
    while i < ArraySize(this.m_visualTagProcessingInfo) {
      if Equals(this.m_visualTagProcessingInfo[i].areaType, area) {
        this.m_visualTagProcessingInfo[i].showItem = show;
        updated = true;
      };
      i += 1;
    };
    if !updated {
      ArrayPush(this.m_visualTagProcessingInfo, info);
    };
  }

  private final func FinalizeVisualTagProcessing() -> Void {
    let i: Int32;
    if this.m_eventsSent == 0 {
      i = 0;
      while i < ArraySize(this.m_visualTagProcessingInfo) {
        if this.m_visualTagProcessingInfo[i].showItem {
          this.ResetItemAppearance(this.m_visualTagProcessingInfo[i].areaType);
        } else {
          this.ClearItemAppearance(this.m_visualTagProcessingInfo[i].areaType);
        };
        i += 1;
      };
      ArrayClear(this.m_visualTagProcessingInfo);
    };
  }

  public final const func IsSlotHidden(area: gamedataEquipmentArea) -> Bool {
    return this.m_clothingVisualsInfo[this.GetVisualSlotIndex(area)].isHidden;
  }

  public final const func ShouldSlotBeHidden(area: gamedataEquipmentArea) -> Bool {
    if Equals(area, gamedataEquipmentArea.UnderwearBottom) || Equals(area, gamedataEquipmentArea.UnderwearTop) {
      return this.IsSlotHidden(area);
    };
    return this.IsVisualTagActive(this.GetVisualTagByAreaType(area));
  }

  public final const func GetSlotOverridenVisualItem(area: gamedataEquipmentArea) -> ItemID {
    let visualSlotIndex: Int32 = this.GetVisualSlotIndex(area);
    if visualSlotIndex < 0 {
      return ItemID.None();
    };
    if ItemID.IsValid(this.m_clothingVisualsInfo[visualSlotIndex].visualItem) {
      return this.m_clothingVisualsInfo[visualSlotIndex].visualItem;
    };
    return ItemID.None();
  }

  public final func IsUnderwearHidden() -> Bool {
    let item: ItemID = this.GetActiveItem(gamedataEquipmentArea.UnderwearBottom);
    if ItemID.IsValid(item) {
      return this.IsSlotHidden(gamedataEquipmentArea.UnderwearBottom);
    };
    this.UnderwearEquipFailsafe();
    return true;
  }

  public final func EvaluateUnderwearTopHiddenState() -> Bool {
    let ts: ref<TransactionSystem>;
    let item: ItemID = this.GetActiveItem(gamedataEquipmentArea.UnderwearTop);
    let underwearHidden: Bool = true;
    if this.IsBuildCensored() {
      if ItemID.IsValid(item) {
        return this.IsSlotHidden(gamedataEquipmentArea.UnderwearTop);
      };
      if this.IsVisualSetActive() && !this.IsVisualSetUnequipInTransition() {
        underwearHidden = !this.ShouldUnderwearTopBeVisibleInSet();
      } else {
        if !ItemID.IsValid(this.GetVisualItemInSlot(gamedataEquipmentArea.InnerChest)) && !this.IsVisualTagActive(n"hide_T1") {
          underwearHidden = false;
        } else {
          underwearHidden = true;
        };
      };
      this.UnderwearTopEquipFailsafe();
      this.m_clothingVisualsInfo[this.GetVisualSlotIndex(gamedataEquipmentArea.UnderwearTop)].isHidden = underwearHidden;
      return underwearHidden;
    };
    if ItemID.IsValid(item) {
      this.UnequipItem(item);
      ts = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
      ts.RemoveItem(this.m_owner, item, 1);
    };
    return true;
  }

  private final func HideItem(area: gamedataEquipmentArea, hide: Bool) -> Void {
    let index: Int32 = this.GetVisualSlotIndex(area);
    if index >= 0 {
      this.m_clothingVisualsInfo[this.GetVisualSlotIndex(area)].isHidden = hide;
    };
  }

  private final func GetPlacementSlotByAreaType(area: gamedataEquipmentArea) -> TweakDBID {
    let i: Int32 = 0;
    while i < ArraySize(this.m_clothingSlotsInfo) {
      if Equals(this.m_clothingSlotsInfo[i].areaType, area) {
        return this.m_clothingSlotsInfo[i].equipSlot;
      };
      i += 1;
    };
    return TDBID.None();
  }

  private final const func GetVisualTagByAreaType(area: gamedataEquipmentArea) -> CName {
    let i: Int32 = 0;
    while i < ArraySize(this.m_clothingSlotsInfo) {
      if Equals(this.m_clothingSlotsInfo[i].areaType, area) {
        return this.m_clothingSlotsInfo[i].visualTag;
      };
      i += 1;
    };
    return n"None";
  }

  private final func GetAreaTypeByVisualTag(tag: CName) -> gamedataEquipmentArea {
    let i: Int32 = 0;
    while i < ArraySize(this.m_clothingSlotsInfo) {
      if Equals(this.m_clothingSlotsInfo[i].visualTag, tag) {
        return this.m_clothingSlotsInfo[i].areaType;
      };
      i += 1;
    };
    return gamedataEquipmentArea.Invalid;
  }

  private final func IsVisualTagValid(tag: CName) -> Bool {
    let i: Int32 = 0;
    while i < ArraySize(this.m_clothingSlotsInfo) {
      if Equals(this.m_clothingSlotsInfo[i].visualTag, tag) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final const func IsVisualTagActive(tag: CName) -> Bool {
    let activeItem: ItemID;
    let i: Int32;
    let transactionSystem: ref<TransactionSystem>;
    if Equals(tag, n"None") {
      return false;
    };
    transactionSystem = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    activeItem = this.GetActiveItem(gamedataEquipmentArea.Outfit);
    if ItemID.IsValid(activeItem) && transactionSystem.MatchVisualTagByItemID(activeItem, this.m_owner, tag) {
      return true;
    };
    i = 0;
    while i < ArraySize(this.m_clothingSlotsInfo) {
      activeItem = this.GetVisualItemInSlot(this.m_clothingSlotsInfo[i].areaType);
      if ItemID.IsValid(activeItem) && transactionSystem.MatchVisualTagByItemID(activeItem, this.m_owner, tag) && !this.IsSlotHidden(this.m_clothingSlotsInfo[i].areaType) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final const func IsPartialVisualTagActive(itemID: ItemID, transactionSystem: ref<TransactionSystem>) -> Bool {
    if transactionSystem.MatchVisualTagByItemID(itemID, this.m_owner, n"hide_T1part") && !this.IsSlotHidden(gamedataEquipmentArea.OuterChest) {
      return true;
    };
    return false;
  }

  private final const func ShouldUnderwearBeVisible(unequippedItem: ItemID) -> Bool {
    return this.IsVisualSetActive() && !this.IsVisualSetUnequipInTransition() ? this.ShouldUnderwearBeVisibleInSet() : this.EvaluateUnderwearVisibility(unequippedItem);
  }

  private final const func ShouldUnderwearBeVisibleInSet() -> Bool {
    let activeItem: ItemID;
    let tagCounter: Int32;
    let set: ref<ClothingSet> = this.GetActiveWardrobeSet();
    let i: Int32 = 0;
    while i < ArraySize(set.clothingList) {
      activeItem = set.clothingList[i].visualItem;
      if ItemID.IsValid(activeItem) && (Equals(set.clothingList[i].areaType, gamedataEquipmentArea.Legs) || this.HasUnderwearVisualTags(activeItem)) {
        tagCounter += 1;
      };
      i += 1;
    };
    return tagCounter == 0;
  }

  private final const func HasUnderwearVisualTags(item: ItemID) -> Bool {
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    return transactionSystem.MatchVisualTagByItemID(item, this.m_owner, n"hide_L1") || transactionSystem.MatchVisualTagByItemID(item, this.m_owner, n"hide_Genitals");
  }

  private final const func EvaluateUnderwearVisibility(unequippedItem: ItemID) -> Bool {
    let activeItem: ItemID;
    let i: Int32;
    let tagCounter: Int32;
    if this.HasUnderwearVisualTags(unequippedItem) || Equals(EquipmentSystem.GetEquipAreaType(unequippedItem), gamedataEquipmentArea.Legs) {
      i = 0;
      while i < ArraySize(this.m_clothingSlotsInfo) {
        activeItem = this.GetActiveItem(this.m_clothingSlotsInfo[i].areaType);
        if ItemID.IsValid(activeItem) {
          if activeItem == unequippedItem {
          } else {
            if this.HasUnderwearVisualTags(activeItem) || Equals(this.m_clothingSlotsInfo[i].areaType, gamedataEquipmentArea.Legs) {
              tagCounter += 1;
            };
          };
        };
        i += 1;
      };
      activeItem = this.GetActiveItem(gamedataEquipmentArea.Outfit);
      if ItemID.IsValid(activeItem) && this.HasUnderwearVisualTags(activeItem) && activeItem != unequippedItem {
        tagCounter += 1;
      };
      return tagCounter == 0;
    };
    return false;
  }

  private final const func ShouldUnderwearTopBeVisible(unequippedItem: ItemID) -> Bool {
    return this.IsVisualSetActive() && !this.IsVisualSetUnequipInTransition() ? this.ShouldUnderwearTopBeVisibleInSet() : this.EvaluateUnderwearTopVisibility(unequippedItem);
  }

  private final const func ShouldUnderwearTopBeVisibleInSet() -> Bool {
    let activeItem: ItemID;
    let tagCounter: Int32;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let set: ref<ClothingSet> = this.GetActiveWardrobeSet();
    let i: Int32 = 0;
    while i < ArraySize(set.clothingList) {
      activeItem = set.clothingList[i].visualItem;
      if ItemID.IsValid(activeItem) && (Equals(set.clothingList[i].areaType, gamedataEquipmentArea.InnerChest) || transactionSystem.MatchVisualTagByItemID(activeItem, this.m_owner, n"hide_T1")) {
        tagCounter += 1;
      };
      i += 1;
    };
    return tagCounter == 0;
  }

  private final const func EvaluateUnderwearTopVisibility(unequippedItem: ItemID) -> Bool {
    let activeItem: ItemID;
    let i: Int32;
    let tagCounter: Int32;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    if transactionSystem.MatchVisualTagByItemID(unequippedItem, this.m_owner, n"hide_T1") || Equals(EquipmentSystem.GetEquipAreaType(unequippedItem), gamedataEquipmentArea.InnerChest) {
      i = 0;
      while i < ArraySize(this.m_clothingSlotsInfo) {
        activeItem = this.GetActiveItem(this.m_clothingSlotsInfo[i].areaType);
        if ItemID.IsValid(activeItem) {
          if activeItem == unequippedItem {
          } else {
            if transactionSystem.MatchVisualTagByItemID(activeItem, this.m_owner, n"hide_T1") || Equals(this.m_clothingSlotsInfo[i].areaType, gamedataEquipmentArea.InnerChest) {
              tagCounter += 1;
            };
          };
        };
        i += 1;
      };
      activeItem = this.GetActiveItem(gamedataEquipmentArea.Outfit);
      if ItemID.IsValid(activeItem) && transactionSystem.MatchVisualTagByItemID(activeItem, this.m_owner, n"hide_T1") && activeItem != unequippedItem {
        tagCounter += 1;
      };
      return tagCounter == 0;
    };
    return false;
  }

  private final func UnderwearEquipFailsafe() -> Void {
    let evt: ref<UnderwearEquipFailsafeEvent>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let underwear: ItemID = ItemID.CreateQuery(t"Items.Underwear_Basic_01_Bottom");
    if !transactionSystem.HasItem(this.m_owner, underwear) {
      transactionSystem.GiveItem(this.m_owner, underwear, 1);
    };
    if !transactionSystem.HasItemInSlot(this.m_owner, t"AttachmentSlots.UnderwearBottom", underwear) {
      evt = new UnderwearEquipFailsafeEvent();
      evt.bottom = true;
      GameInstance.GetDelaySystem(this.m_owner.GetGame()).DelayEventNextFrame(this.m_owner, evt);
    };
  }

  private final func UnderwearTopEquipFailsafe() -> Void {
    let evt: ref<UnderwearEquipFailsafeEvent>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let underwear: ItemID = ItemID.CreateQuery(t"Items.Underwear_Basic_01_Top");
    if !transactionSystem.HasItem(this.m_owner, underwear) {
      transactionSystem.GiveItem(this.m_owner, underwear, 1);
    };
    if !transactionSystem.HasItemInSlot(this.m_owner, t"AttachmentSlots.UnderwearTop", underwear) {
      evt = new UnderwearEquipFailsafeEvent();
      evt.bottom = false;
      GameInstance.GetDelaySystem(this.m_owner.GetGame()).DelayEventNextFrame(this.m_owner, evt);
    };
  }

  public final func OnUnderwearEquipFailsafe(bottom: Bool) -> Void {
    let underwear: ItemID;
    if bottom {
      underwear = ItemID.CreateQuery(t"Items.Underwear_Basic_01_Bottom");
    } else {
      underwear = ItemID.CreateQuery(t"Items.Underwear_Basic_01_Top");
    };
    this.EquipItem(underwear, false, false);
  }

  public final const func GetActiveWardrobeSet() -> ref<ClothingSet> {
    let set: ref<ClothingSet>;
    if this.IsVisualSetActive() {
      set = this.m_wardrobeSystem.GetActiveClothingSet();
    } else {
      set = new ClothingSet();
    };
    return set;
  }

  public final func OnQuestDisableWardrobeSetRequest(request: ref<QuestDisableWardrobeSetRequest>) -> Void {
    this.m_wardrobeDisabled = request.blockReequipping;
    if this.IsVisualSetActive() {
      this.m_lastActiveWardrobeSet = this.m_wardrobeSystem.GetActiveClothingSetIndex();
      this.UnequipWardrobeSet();
    };
  }

  public final func OnQuestRestoreWardrobeSetRequest(request: ref<QuestRestoreWardrobeSetRequest>) -> Void {
    this.m_wardrobeDisabled = false;
    if NotEquals(this.m_lastActiveWardrobeSet, gameWardrobeClothingSetIndex.INVALID) {
      this.EquipWardrobeSet(this.m_lastActiveWardrobeSet);
      this.m_lastActiveWardrobeSet = gameWardrobeClothingSetIndex.INVALID;
    };
  }

  public final func OnQuestEnableWardrobeSetRequest(request: ref<QuestEnableWardrobeSetRequest>) -> Void {
    this.m_wardrobeDisabled = false;
  }

  public final func EquipWardrobeSet(setID: gameWardrobeClothingSetIndex) -> Void {
    let i: Int32;
    let outfitData: wref<gameItemData>;
    let visualItem: ItemID;
    let clothingSet: ref<ClothingSet> = this.FindWardrobeClothingSetByID(setID);
    let outfit: ItemID = this.GetActiveItem(gamedataEquipmentArea.Outfit);
    if this.m_wardrobeDisabled || Equals(setID, gameWardrobeClothingSetIndex.INVALID) || ArraySize(clothingSet.clothingList) == 0 {
      return;
    };
    if ItemID.IsValid(outfit) {
      outfitData = GameInstance.GetTransactionSystem(this.m_owner.GetGame()).GetItemData(this.m_owner, outfit);
    };
    if IsDefined(outfitData) && outfitData.HasTag(n"UnequipBlocked") {
      return;
    };
    if ItemID.IsValid(outfit) {
      this.UnequipItem(outfit);
    };
    this.m_wardrobeSystem.SetActiveClothingSetIndex(setID);
    i = 0;
    while i <= ArraySize(clothingSet.clothingList) {
      visualItem = clothingSet.clothingList[i].visualItem;
      if ItemID.IsValid(visualItem) {
        this.EquipVisuals(visualItem);
      } else {
        this.ClearVisuals(clothingSet.clothingList[i].areaType);
      };
      i += 1;
    };
  }

  public final const func FindWardrobeClothingSetByID(setID: gameWardrobeClothingSetIndex) -> ref<ClothingSet> {
    let clothingSets: array<ref<ClothingSet>> = this.m_wardrobeSystem.GetClothingSets();
    let i: Int32 = 0;
    while i <= ArraySize(clothingSets) {
      if Equals(clothingSets[i].setID, setID) {
        return clothingSets[i];
      };
      i += 1;
    };
    return new ClothingSet();
  }

  public final func UnequipWardrobeSet() -> Void {
    let i: Int32;
    let currentSet: ref<ClothingSet> = this.GetActiveWardrobeSet();
    if NotEquals(this.m_wardrobeSystem.GetActiveClothingSetIndex(), gameWardrobeClothingSetIndex.INVALID) {
      this.m_visualUnequipTransition = true;
      i = 0;
      while i <= ArraySize(currentSet.clothingList) {
        this.UnequipVisuals(currentSet.clothingList[i].areaType);
        i += 1;
      };
      this.m_visualUnequipTransition = false;
      this.m_wardrobeSystem.SetActiveClothingSetIndex(gameWardrobeClothingSetIndex.INVALID);
    };
    this.UpdateUIBBAreaChanged(gamedataEquipmentArea.Outfit, 0);
  }

  public final func DeleteWardrobeSet(setID: gameWardrobeClothingSetIndex) -> Void {
    if Equals(this.m_wardrobeSystem.GetActiveClothingSetIndex(), setID) {
      this.UnequipWardrobeSet();
    };
    this.m_wardrobeSystem.DeleteClothingSet(setID);
  }

  public final const func IsVisualSetActive() -> Bool {
    let activeClothingSetIndex: gameWardrobeClothingSetIndex = this.m_wardrobeSystem.GetActiveClothingSetIndex();
    return NotEquals(activeClothingSetIndex, gameWardrobeClothingSetIndex.INVALID);
  }

  public final const func IsVisualSetUnequipInTransition() -> Bool {
    return this.m_visualUnequipTransition;
  }

  public final const func IsSlotOverriden(area: gamedataEquipmentArea) -> Bool {
    let visualSlotIndex: Int32 = this.GetVisualSetIndex(area);
    return visualSlotIndex >= 0;
  }

  private final const func GetVisualSetIndex(area: gamedataEquipmentArea) -> Int32 {
    let i: Int32 = -1;
    if this.IsVisualSetActive() {
      i = this.GetVisualSlotIndex(area);
    };
    return i >= 0 && ItemID.IsValid(this.m_clothingVisualsInfo[i].visualItem) ? i : -1;
  }

  private final const func GetVisualSlotIndex(area: gamedataEquipmentArea, opt excludeUnderwear: Bool) -> Int32 {
    let i: Int32;
    if Equals(area, gamedataEquipmentArea.Outfit) || excludeUnderwear && this.IsUnderwear(area) {
      return -1;
    };
    i = 0;
    while i <= ArraySize(this.m_clothingVisualsInfo) {
      if Equals(this.m_clothingVisualsInfo[i].areaType, area) {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  public final const func GetVisualItemInSlot(area: gamedataEquipmentArea) -> ItemID {
    let item: ItemID;
    let visualSlotIndex: Int32 = this.GetVisualSlotIndex(area, true);
    if this.IsVisualSetActive() && visualSlotIndex != -1 {
      item = this.m_clothingVisualsInfo[visualSlotIndex].visualItem;
    } else {
      item = this.GetActiveItem(area);
    };
    return item;
  }

  private final const func IsUnderwear(area: gamedataEquipmentArea) -> Bool {
    return Equals(area, gamedataEquipmentArea.UnderwearBottom) || Equals(area, gamedataEquipmentArea.UnderwearTop);
  }

  public final const func IsWardrobeEnabled() -> Bool {
    return !this.m_wardrobeDisabled;
  }

  private final func GetHighestPriorityMovementAudio() -> CName {
    let j: Int32;
    let maxPriority: Float;
    let priority: Float;
    let soundName: CName;
    let i: Int32 = 0;
    while i < ArraySize(this.m_equipment.equipAreas) {
      j = 0;
      while j < ArraySize(this.m_equipment.equipAreas[i].equipSlots) {
        priority = RPGManager.GetItemRecord(this.m_equipment.equipAreas[i].equipSlots[j].itemID).MovementSound().Priority();
        if priority > maxPriority {
          maxPriority = priority;
          soundName = RPGManager.GetItemRecord(this.m_equipment.equipAreas[i].equipSlots[j].itemID).MovementSound().AudioMovementName();
        };
        j += 1;
      };
      i += 1;
    };
    return soundName;
  }

  private final const func IsItemAWeapon(item: ItemID) -> Bool {
    let record: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item));
    return Equals(record.ItemCategory().Type(), gamedataItemCategory.Weapon);
  }

  private final const func IsItemOfCategory(item: ItemID, category: gamedataItemCategory) -> Bool {
    let record: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item));
    if IsDefined(record) && IsDefined(record.ItemCategory()) {
      return Equals(record.ItemCategory().Type(), category);
    };
    return false;
  }

  private final const func IsItemConstructed(item: ItemID) -> Bool {
    let record: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item));
    let blueprint: ref<ItemBlueprint_Record> = record.Blueprint();
    return TDBID.IsValid(blueprint.GetID());
  }

  public final const func IsEquippable(itemData: wref<gameItemData>) -> Bool {
    let itemLevel: Float;
    let ownerLevel: Float;
    let statsSys: ref<StatsSystem>;
    if itemData == null {
      return false;
    };
    if RPGManager.IsItemBroken(itemData) {
      return false;
    };
    if !this.CheckEquipPrereqs(itemData.GetID(), itemData.GetVariant()) {
      return false;
    };
    statsSys = GameInstance.GetStatsSystem(this.m_owner.GetGame());
    ownerLevel = statsSys.GetStatValue(Cast<StatsObjectID>(this.m_owner.GetEntityID()), gamedataStatType.Level);
    itemLevel = Cast<Float>(FloorF(itemData.GetStatValueByType(gamedataStatType.Level)));
    return ownerLevel >= itemLevel;
  }

  public final const func IsItemInHotkey(itemID: ItemID) -> Bool {
    return HotkeyManager.IsItemInHotkey(this.m_hotkeys, itemID);
  }

  public final const func GetHotkeyTypeForItemID(itemID: ItemID) -> EHotkey {
    return HotkeyManager.GetHotkeyTypeForItemID(this.m_owner, this.m_hotkeys, itemID);
  }

  public final const func GetHotkeyTypeFromItemID(itemID: ItemID) -> EHotkey {
    return HotkeyManager.GetHotkeyTypeFromItemID(this.m_hotkeys, itemID);
  }

  public final const func GetItemIDFromHotkey(hotkey: EHotkey) -> ItemID {
    return HotkeyManager.GetItemIDFromHotkey(this.m_hotkeys, hotkey);
  }

  private final const func CheckEquipPrereqs(itemID: ItemID, randomVariant: Int32) -> Bool {
    let i: Int32;
    let prereqs: array<wref<IPrereq_Record>>;
    let result: Bool;
    let itemRecord: wref<Item_Record> = RPGManager.GetItemRecord(itemID);
    itemRecord.EquipPrereqs(prereqs);
    if itemRecord.UsesVariants() {
      itemRecord.GetVariantsItem(randomVariant).VariantPrereqs(prereqs);
    };
    i = 0;
    while i < ArraySize(prereqs) {
      result = RPGManager.CheckPrereq(prereqs[i], this.m_owner, Cast<StatsObjectID>(itemID));
      if !result {
        return false;
      };
      i += 1;
    };
    return true;
  }

  private final func AssignNextValidItemToHotkey(currentItem: ItemID) -> Bool {
    let i: Int32;
    let newHotkeyItem: ItemID;
    let sameTypeItems: array<ItemID>;
    let hotkey: EHotkey = this.GetHotkeyTypeFromItemID(currentItem);
    let currentItemType: gamedataItemType = RPGManager.GetItemType(currentItem);
    if Equals(currentItemType, gamedataItemType.Cyb_Launcher) || Equals(currentItemType, gamedataItemType.Cyb_Ability) {
      this.m_inventoryManager.GetPlayerItemsIDsByType(gamedataItemType.Gad_Grenade, sameTypeItems);
    } else {
      if Equals(currentItemType, gamedataItemType.Cyb_HealingAbility) {
        this.m_inventoryManager.GetPlayerItemsIDsByType(gamedataItemType.Con_Inhaler, sameTypeItems);
        this.m_inventoryManager.GetPlayerItemsIDsByType(gamedataItemType.Con_Injector, sameTypeItems);
      } else {
        this.m_inventoryManager.GetPlayerItemsIDsByType(currentItemType, sameTypeItems);
      };
    };
    if ArraySize(sameTypeItems) > 0 {
      i = 0;
      while i < ArraySize(sameTypeItems) {
        if sameTypeItems[i] == currentItem {
          if ArraySize(sameTypeItems) == 1 {
            this.ClearItemFromHotkey(hotkey);
            return false;
          };
          newHotkeyItem = this.GetNextItemInList(sameTypeItems, i);
          if ItemID.IsValid(newHotkeyItem) {
            this.AssignItemToHotkey(newHotkeyItem, hotkey);
            return true;
          };
        };
        i += 1;
      };
      this.AssignItemToHotkey(sameTypeItems[0], hotkey);
    } else {
      this.ClearItemFromHotkey(hotkey);
    };
    return false;
  }

  public final func OnHotkeyRefreshRequest(request: ref<HotkeyRefreshRequest>) -> Void {
    let i: Int32;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    if !IsDefined(transactionSystem) {
      return;
    };
    i = 0;
    while i < ArraySize(this.m_hotkeys) {
      if Equals(this.m_hotkeys[i].GetHotkey(), EHotkey.RB) || Equals(this.m_hotkeys[i].GetHotkey(), EHotkey.DPAD_UP) {
        if transactionSystem.HasItem(this.m_owner, this.m_hotkeys[i].GetItemID()) {
          this.SyncHotkeyData(this.m_hotkeys[i].GetHotkey());
        } else {
          this.AssignNextValidItemToHotkey(this.m_hotkeys[i].GetItemID());
        };
      };
      i += 1;
    };
  }

  public final func OnHotkeyAssignmentRequest(request: ref<HotkeyAssignmentRequest>) -> Void {
    this.AssignItemToHotkey(request.ItemID(), request.GetHotkey());
    if Equals(request.GetRequestType(), EHotkeyRequestType.Assign) {
      this.ProcessGadgetsTutorials(request.ItemID());
    };
  }

  public final func OnAssignHotkeyIfEmptySlot(request: ref<AssignHotkeyIfEmptySlot>) -> Void {
    let hotkey: EHotkey;
    if this.ShouldPickedUpItemBeAddedToHotkey(request.ItemID(), hotkey) {
      this.AssignItemToHotkey(request.ItemID(), hotkey);
    };
  }

  public final func AssignItemToHotkey(newID: ItemID, hotkey: EHotkey) -> Void {
    let transactionSystem: ref<TransactionSystem>;
    let oldID: ItemID = this.m_hotkeys[EnumInt(hotkey)].GetItemID();
    if NotEquals(hotkey, EHotkey.INVALID) {
      if newID != oldID {
        transactionSystem = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
        transactionSystem.OnItemRemovedFromEquipmentSlot(this.m_owner, oldID);
        transactionSystem.OnItemAddedToEquipmentSlot(this.m_owner, newID);
        if Equals(RPGManager.GetItemCategory(oldID), gamedataItemCategory.Consumable) {
          this.RemoveEquipGLPs(oldID);
        };
        this.m_hotkeys[EnumInt(hotkey)].StoreItem(newID);
        if Equals(RPGManager.GetItemCategory(newID), gamedataItemCategory.Consumable) {
          this.ApplyEquipGLPs(newID);
        };
        this.SyncHotkeyData(hotkey);
      };
    };
  }

  public final func ClearItemFromHotkey(hotkey: EHotkey) -> Void {
    let oldID: ItemID = this.m_hotkeys[EnumInt(hotkey)].GetItemID();
    if NotEquals(hotkey, EHotkey.INVALID) {
      if Equals(RPGManager.GetItemCategory(oldID), gamedataItemCategory.Consumable) {
        this.RemoveEquipGLPs(oldID);
      };
      this.m_hotkeys[EnumInt(hotkey)].StoreItem(ItemID.None());
      this.SyncHotkeyData(hotkey);
    };
  }

  private final func SyncHotkeyData(hotkey: EHotkey) -> Void {
    let blackboard: ref<IBlackboard> = GameInstance.GetBlackboardSystem(this.m_owner.GetGame()).Get(GetAllBlackboardDefs().UI_Hotkeys);
    if !IsDefined(blackboard) {
      return;
    };
    blackboard.SetVariant(GetAllBlackboardDefs().UI_Hotkeys.ModifiedHotkey, ToVariant(hotkey), true);
  }

  private final const func ShouldPickedUpItemBeAddedToHotkey(itemID: ItemID, out hotkey: EHotkey) -> Bool {
    let type: gamedataItemType = RPGManager.GetItemType(itemID);
    let i: Int32 = 0;
    while i < ArraySize(this.m_hotkeys) {
      if this.m_hotkeys[i].IsEmpty() && (this.m_hotkeys[i].IsCompatible(type) || this.m_hotkeys[i].IsCompatible(EquipmentSystem.GetEquipAreaType(itemID))) {
        hotkey = this.m_hotkeys[i].GetHotkey();
        return true;
      };
      i += 1;
    };
    hotkey = EHotkey.INVALID;
    return false;
  }

  private final func GetNextItemInList(const arr: script_ref<[ItemID]>, fromIndex: Int32) -> ItemID {
    if fromIndex > ArraySize(Deref(arr)) - 1 || fromIndex < 0 {
      return ItemID.None();
    };
    if fromIndex == ArraySize(Deref(arr)) - 1 {
      return Deref(arr)[0];
    };
    return Deref(arr)[fromIndex + 1];
  }

  private final func UnequipItem(itemID: ItemID) -> Void {
    let equipAreaIndex: Int32;
    if this.IsEquipped(itemID) {
      equipAreaIndex = this.GetEquipAreaIndex(EquipmentSystem.GetEquipAreaType(itemID));
      this.UnequipItem(equipAreaIndex, this.GetSlotIndex(itemID));
    };
  }

  private final func UnequipItem(equipAreaIndex: Int32, slotIndex: Int32, opt forceRemove: Bool) -> Void {
    let itemType: gamedataItemType;
    let placementSlot: TweakDBID;
    let unequipNotifyEvent: ref<AudioNotifyItemUnequippedEvent>;
    let visualItemID: ItemID;
    let currentItem: ItemID = this.m_equipment.equipAreas[equipAreaIndex].equipSlots[slotIndex].itemID;
    let equipArea: SEquipArea = this.GetEquipAreaFromItemID(currentItem);
    let currentItemRecord: ref<Item_Record> = RPGManager.GetItemRecord(currentItem);
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let itemData: wref<gameItemData> = RPGManager.GetItemData(this.m_owner.GetGame(), this.m_owner, currentItem);
    if !forceRemove && IsDefined(itemData) && itemData.HasTag(n"UnequipBlocked") {
      return;
    };
    if Equals(equipArea.areaType, gamedataEquipmentArea.SystemReplacementCW) {
      this.AssignItemToHotkey(ItemID.None(), EHotkey.LBRB);
    };
    if Equals(RPGManager.GetItemType(currentItem), gamedataItemType.Cyb_Ability) {
      this.AssignNextValidItemToHotkey(this.GetItemIDFromHotkey(EHotkey.RB));
    } else {
      if Equals(RPGManager.GetItemType(currentItem), gamedataItemType.Cyb_HealingAbility) {
        this.AssignNextValidItemToHotkey(this.GetItemIDFromHotkey(EHotkey.DPAD_UP));
      };
    };
    placementSlot = this.GetPlacementSlot(equipAreaIndex, slotIndex);
    if this.IsItemOfCategory(currentItem, gamedataItemCategory.Weapon) && equipArea.activeIndex == slotIndex {
      if currentItem != this.GetActiveHeavyWeapon() && currentItem == this.GetSlotActiveItem(this.GetRequestSlotFromItemID(currentItem)) {
        this.CreateUnequipWeaponManipulationRequest();
      };
      this.m_equipment.equipAreas[equipAreaIndex].equipSlots[slotIndex].itemID = ItemID.None();
      if PlayerDevelopmentSystem.GetData(this.m_owner).IsNewPerkBought(gamedataNewPerkType.Espionage_Central_Milestone_1) > 0 && Equals(RPGManager.GetItemType(currentItem), gamedataItemType.Cyb_NanoWires) {
        this.UnequipCyberwareParts(itemData);
      };
    } else {
      if (this.IsItemOfCategory(currentItem, gamedataItemCategory.Gadget) || Equals(RPGManager.GetItemType(currentItem), gamedataItemType.Cyb_Launcher)) && equipArea.activeIndex == slotIndex {
        if this.IsItemInHotkey(currentItem) {
          this.CreateUnequipGadgetWeaponManipulationRequest();
          this.AssignNextValidItemToHotkey(this.GetItemIDFromHotkey(EHotkey.RB));
        };
      } else {
        if this.IsItemOfCategory(currentItem, gamedataItemCategory.Consumable) && equipArea.activeIndex == slotIndex {
          itemType = RPGManager.GetItemType(currentItem);
          if Equals(itemType, gamedataItemType.Con_Inhaler) || Equals(itemType, gamedataItemType.Con_Injector) {
            this.CreateUnequipConsumableWeaponManipulationRequest();
            this.AssignNextValidItemToHotkey(this.GetItemIDFromHotkey(EHotkey.DPAD_UP));
          };
        } else {
          if ItemID.IsValid(currentItem) {
            if this.IsItemOfCategory(currentItem, gamedataItemCategory.Clothing) {
              this.OnUnequipProcessVisualTags(currentItem);
            };
            if transactionSystem.HasItemInSlot(this.m_owner, placementSlot, currentItem) || transactionSystem.IsSlotSpawningItem(this.m_owner, placementSlot, currentItem) {
              unequipNotifyEvent = new AudioNotifyItemUnequippedEvent();
              unequipNotifyEvent.itemName = currentItemRecord.EntityName();
              this.m_owner.QueueEvent(unequipNotifyEvent);
              transactionSystem.RemoveItemFromSlot(this.m_owner, placementSlot);
              if this.IsItemOfCategory(currentItem, gamedataItemCategory.Clothing) && this.IsSlotOverriden(equipArea.areaType) {
                visualItemID = this.GetVisualItemInSlot(equipArea.areaType);
                transactionSystem.GivePreviewItemByItemID(this.m_owner, visualItemID);
                transactionSystem.AddItemToSlot(this.m_owner, placementSlot, transactionSystem.CreatePreviewItemID(visualItemID), true);
              };
            };
            this.RemoveSlotGLPs(this.m_equipment.equipAreas[equipAreaIndex].equipSlots[slotIndex].slotID);
            this.m_equipment.equipAreas[equipAreaIndex].equipSlots[slotIndex].itemID = ItemID.None();
            this.RemoveEquipGLPs(currentItem);
            this.SendUnequipAudioEvents(currentItem);
            if this.IsItemOfCategory(currentItem, gamedataItemCategory.Cyberware) && this.IsItemConstructed(currentItem) {
              this.UnequipCyberwareParts(itemData);
            };
          };
        };
      };
    };
    if itemData.UsesVariants() || Equals(equipArea.areaType, gamedataEquipmentArea.RightArm) {
      itemData.SubtractStatsOnUnequip(this.m_owner);
    };
    if ItemID.IsValid(currentItem) && Equals(equipArea.areaType, gamedataEquipmentArea.ArmsCW) {
      this.HandleArmsCWUnequip(this.m_owner as PlayerPuppet);
    };
    this.UpdateWeaponWheel();
    this.UpdateQuickWheel();
    this.SendPaperdollUpdate(this.m_equipment.equipAreas[equipAreaIndex], false, placementSlot, slotIndex, true);
    this.UpdateUIBBAreaChanged(this.m_equipment.equipAreas[equipAreaIndex].areaType, slotIndex);
    transactionSystem.OnItemRemovedFromEquipmentSlot(this.m_owner, currentItem);
  }

  private final func ClearEquipment() -> Void {
    let equipArea: SEquipArea;
    let j: Int32;
    let i: Int32 = 0;
    while i < ArraySize(this.m_equipment.equipAreas) {
      equipArea = this.m_equipment.equipAreas[i];
      if NotEquals(equipArea.areaType, gamedataEquipmentArea.BaseFists) && NotEquals(equipArea.areaType, gamedataEquipmentArea.VDefaultHandgun) {
        j = 0;
        while j < ArraySize(equipArea.equipSlots) {
          this.UnequipItem(i, j);
          j += 1;
        };
      };
      i += 1;
    };
  }

  private final func HandleStrongArmsEquip(armsCWID: ItemID) -> Void {
    this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.BaseFists)].equipSlots[0].itemID = armsCWID;
  }

  private final func UnequipCyberwareParts(cyberwareData: wref<gameItemData>) -> Void {
    let i: Int32;
    let partData: InnerItemData;
    let staticData: wref<Item_Record>;
    let usedSlots: array<TweakDBID>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    transactionSystem.GetUsedSlotsOnItem(this.m_owner, cyberwareData.GetID(), usedSlots);
    i = 0;
    while i < ArraySize(usedSlots) {
      if usedSlots[i] == t"AttachmentSlots.StatsShardSlot" {
      } else {
        cyberwareData.GetItemPart(partData, usedSlots[i]);
        staticData = InnerItemData.GetStaticData(partData);
        if IsDefined(staticData) && staticData.TagsContains(n"DummyPart") {
        } else {
          transactionSystem.RemovePart(this.m_owner, cyberwareData.GetID(), usedSlots[i]);
        };
      };
      i += 1;
    };
  }

  private final func HandleArmsCWUnequip(owner: ref<PlayerPuppet>) -> Void {
    let baseFistsID: ItemID = this.EquipBaseFists();
    EquipmentSystemPlayerData.UpdateArmSlot(owner, baseFistsID, true);
  }

  private final func EquipBaseFists() -> ItemID {
    let baseFistsID: ItemID = this.GetBaseFistsItemID();
    let fistsData: ref<gameItemData> = GameInstance.GetTransactionSystem(this.m_owner.GetGame()).GetItemData(this.m_owner, baseFistsID);
    if IsDefined(fistsData) {
      baseFistsID = fistsData.GetID();
    } else {
      GameInstance.GetTransactionSystem(this.m_owner.GetGame()).GiveItem(this.m_owner, baseFistsID, 1);
    };
    this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.BaseFists)].equipSlots[0].itemID = baseFistsID;
    return baseFistsID;
  }

  private final func ApplyEquipGLPs(itemID: ItemID) -> Void {
    let itemParts: array<InnerItemData>;
    let j: Int32;
    let gameplayLogicPackageSystem: ref<GameplayLogicPackageSystem> = GameInstance.GetGameplayLogicPackageSystem(this.m_owner.GetGame());
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID));
    let packages: array<wref<GameplayLogicPackage_Record>> = this.GetItemGLPs(ItemID.GetTDBID(itemID));
    let itemData: ref<gameItemData> = GameInstance.GetTransactionSystem(this.m_owner.GetGame()).GetItemData(this.m_owner, itemID);
    let i: Int32 = 0;
    while i < ArraySize(packages) {
      gameplayLogicPackageSystem.ApplyPackage(this.m_owner, this.m_owner, packages[i].GetID());
      i += 1;
    };
    ArrayClear(packages);
    itemData.GetItemParts(itemParts);
    i = 0;
    while i < ArraySize(itemParts) {
      itemRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(InnerItemData.GetItemID(itemParts[i])));
      itemRecord.OnEquip(packages);
      j = 0;
      while j < ArraySize(packages) {
        gameplayLogicPackageSystem.ApplyPackage(this.m_owner, this.m_owner, packages[j].GetID());
        j += 1;
      };
      ArrayClear(packages);
      i += 1;
    };
  }

  private final func RemoveEquipGLPs(itemID: ItemID) -> Void {
    let itemParts: array<InnerItemData>;
    let j: Int32;
    let gameplayLogicPackageSystem: ref<GameplayLogicPackageSystem> = GameInstance.GetGameplayLogicPackageSystem(this.m_owner.GetGame());
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID));
    let packages: array<wref<GameplayLogicPackage_Record>> = this.GetItemGLPs(ItemID.GetTDBID(itemID));
    let itemData: ref<gameItemData> = GameInstance.GetTransactionSystem(this.m_owner.GetGame()).GetItemData(this.m_owner, itemID);
    let i: Int32 = 0;
    while i < ArraySize(packages) {
      gameplayLogicPackageSystem.RemovePackage(this.m_owner, packages[i].GetID());
      i += 1;
    };
    ArrayClear(packages);
    if IsDefined(itemData) {
      itemData.GetItemParts(itemParts);
    };
    i = 0;
    while i < ArraySize(itemParts) {
      ArrayClear(packages);
      itemRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(InnerItemData.GetItemID(itemParts[i])));
      if IsDefined(itemRecord) {
        itemRecord.OnEquip(packages);
      };
      j = 0;
      while j < ArraySize(packages) {
        gameplayLogicPackageSystem.RemovePackage(this.m_owner, packages[j].GetID());
        j += 1;
      };
      i += 1;
    };
  }

  private final func ApplySlotGLPs(equipSlotID: TweakDBID) -> Void {
    let gameplayLogicPackageSystem: ref<GameplayLogicPackageSystem> = GameInstance.GetGameplayLogicPackageSystem(this.m_owner.GetGame());
    let packages: array<wref<GameplayLogicPackage_Record>> = this.GetSlotGLPs(equipSlotID);
    let i: Int32 = 0;
    while i < ArraySize(packages) {
      gameplayLogicPackageSystem.ApplyPackage(this.m_owner, this.m_owner, packages[i].GetID());
      i += 1;
    };
  }

  private final func RemoveSlotGLPs(equipSlotID: TweakDBID) -> Void {
    let gameplayLogicPackageSystem: ref<GameplayLogicPackageSystem> = GameInstance.GetGameplayLogicPackageSystem(this.m_owner.GetGame());
    let packages: array<wref<GameplayLogicPackage_Record>> = this.GetSlotGLPs(equipSlotID);
    let i: Int32 = 0;
    while i < ArraySize(packages) {
      gameplayLogicPackageSystem.RemovePackage(this.m_owner, packages[i].GetID());
      i += 1;
    };
  }

  private final func GetSlotGLPs(equipSlotID: TweakDBID) -> [wref<GameplayLogicPackage_Record>] {
    let packages: array<wref<GameplayLogicPackage_Record>>;
    let slotRecord: ref<EquipSlot_Record> = TweakDBInterface.GetEquipSlotRecord(equipSlotID);
    slotRecord.OnInsertion(packages);
    return packages;
  }

  private final func GetItemGLPs(itemID: TweakDBID) -> [wref<GameplayLogicPackage_Record>] {
    let packages: array<wref<GameplayLogicPackage_Record>>;
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(itemID);
    itemRecord.OnEquip(packages);
    return packages;
  }

  public final func GetLastUsedItemID(lastUsedWeaponType: ELastUsed) -> ItemID {
    let lastUsedStruct: SLastUsedWeapon = this.GetLastUsedStruct();
    switch lastUsedWeaponType {
      case ELastUsed.Melee:
        return lastUsedStruct.lastUsedMelee;
      case ELastUsed.Ranged:
        return lastUsedStruct.lastUsedRanged;
      case ELastUsed.Weapon:
        return lastUsedStruct.lastUsedWeapon;
      case ELastUsed.Heavy:
        return lastUsedStruct.lastUsedHeavy;
      default:
        return ItemID.None();
    };
  }

  private final func SetLastUsedItem(item: ItemID) -> Void {
    let lastUsedStruct: SLastUsedWeapon = this.GetLastUsedStruct();
    let tags: array<CName> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item)).Tags();
    if ArrayContains(tags, n"RangedWeapon") {
      if ArrayContains(tags, n"HeavyWeapon") {
        lastUsedStruct.lastUsedHeavy = item;
      } else {
        lastUsedStruct.lastUsedRanged = item;
        lastUsedStruct.lastUsedWeapon = item;
        lastUsedStruct.lastUsedHeavy = ItemID.None();
      };
    } else {
      if ArrayContains(tags, n"MeleeWeapon") {
        lastUsedStruct.lastUsedMelee = item;
        lastUsedStruct.lastUsedWeapon = item;
        lastUsedStruct.lastUsedHeavy = ItemID.None();
      } else {
        return;
      };
    };
    this.m_lastUsedStruct = lastUsedStruct;
  }

  private final func SetSlotActiveItem(slot: EquipmentManipulationRequestSlot, item: ItemID) -> Void {
    let sink: SDOSink = GameInstance.GetScriptsDebugOverlaySystem(this.m_owner.GetGame()).CreateSink();
    let slotItems: SSlotActiveItems = this.GetSlotActiveItemStruct();
    switch slot {
      case EquipmentManipulationRequestSlot.Left:
        this.m_slotActiveItemsInHands.leftHandItem = item;
        break;
      case EquipmentManipulationRequestSlot.Right:
        this.m_slotActiveItemsInHands.rightHandItem = item;
        break;
      case EquipmentManipulationRequestSlot.Both:
        this.m_slotActiveItemsInHands.leftHandItem = item;
        this.m_slotActiveItemsInHands.rightHandItem = item;
    };
    SDOSink.SetRoot(sink, EquipmentSystem.ComposeSDORootPath(this.GetOwner(), "Slot active items"));
    SDOSink.PushString(sink, "Left hand", ToString(slotItems.leftHandItem));
    SDOSink.PushString(sink, "Right hand", ToString(slotItems.rightHandItem));
  }

  public final func GetSlotActiveItem(slot: EquipmentManipulationRequestSlot) -> ItemID {
    let slotsStruct: SSlotActiveItems = this.GetSlotActiveItemStruct();
    switch slot {
      case EquipmentManipulationRequestSlot.Left:
        return slotsStruct.leftHandItem;
      case EquipmentManipulationRequestSlot.Right:
        return slotsStruct.rightHandItem;
      default:
        return ItemID.None();
    };
  }

  public final func RemoveItemFromSlotActiveItem(item: ItemID) -> Void {
    let slotsStruct: SSlotActiveItems = this.GetSlotActiveItemStruct();
    if slotsStruct.rightHandItem == item {
      this.m_slotActiveItemsInHands.rightHandItem = ItemID.None();
    };
    if slotsStruct.leftHandItem == item {
      this.m_slotActiveItemsInHands.leftHandItem = ItemID.None();
    };
  }

  private final func DrawItem(itemToDraw: ItemID, drawAnimationType: gameEquipAnimationType) -> Void {
    let equipAreaIndex: Int32 = this.GetEquipAreaIndex(EquipmentSystem.GetEquipAreaType(itemToDraw));
    let slotIndex: Int32 = this.GetSlotIndex(itemToDraw);
    let request: ref<EquipmentSystemWeaponManipulationRequest> = new EquipmentSystemWeaponManipulationRequest();
    let equipArea: SEquipArea = this.m_equipment.equipAreas[equipAreaIndex];
    request.requestType = EquipmentManipulationAction.Undefined;
    request.equipAnimType = drawAnimationType;
    if slotIndex == -1 {
      this.EquipItem(itemToDraw, false, false);
      slotIndex = this.GetSlotIndex(itemToDraw);
    };
    if slotIndex >= 0 && slotIndex < ArraySize(equipArea.equipSlots) {
      this.m_equipment.equipAreas[equipAreaIndex].activeIndex = slotIndex;
      this.UpdateActiveWheelItem(this.GetItemInEquipSlot(equipAreaIndex, slotIndex));
      switch equipArea.areaType {
        case gamedataEquipmentArea.VDefaultHandgun:
        case gamedataEquipmentArea.ArmsCW:
        case gamedataEquipmentArea.BaseFists:
        case gamedataEquipmentArea.Weapon:
          request.requestType = EquipmentManipulationAction.RequestSlotActiveWeapon;
          if this.CheckWeaponAgainstGameplayRestrictions(itemToDraw) {
            this.SetSlotActiveItem(EquipmentManipulationRequestSlot.Right, itemToDraw);
          };
          this.UpdateEquipAreaActiveIndex(itemToDraw);
          this.SetLastUsedItem(itemToDraw);
          break;
        case gamedataEquipmentArea.WeaponHeavy:
          if this.CheckWeaponAgainstGameplayRestrictions(itemToDraw) {
            this.SetSlotActiveItem(EquipmentManipulationRequestSlot.Right, itemToDraw);
          };
          this.UpdateEquipAreaActiveIndex(itemToDraw);
          this.SetLastUsedItem(itemToDraw);
          request.requestType = EquipmentManipulationAction.RequestHeavyWeapon;
      };
      if NotEquals(request.requestType, EquipmentManipulationAction.Undefined) {
        this.OnEquipmentSystemWeaponManipulationRequest(request);
      };
    };
  }

  public final static func UpdateArmSlot(owner: ref<PlayerPuppet>, itemToDraw: ItemID, equipHolsteredItem: Bool) -> Void {
    let equipmentSystemData: ref<EquipmentSystemPlayerData>;
    let equippedArmsCW: ItemID;
    let holsteredArms: ItemID;
    let itemTags: array<CName>;
    let record: ref<WeaponItem_Record>;
    let slotID: TweakDBID;
    let transactionSystem: ref<TransactionSystem>;
    if !IsDefined(owner) {
      return;
    };
    record = TweakDBInterface.GetWeaponItemRecord(ItemID.GetTDBID(itemToDraw));
    if !IsDefined(record) {
      return;
    };
    if !IsDefined(record.HolsteredItem()) {
      return;
    };
    equipmentSystemData = EquipmentSystem.GetData(owner);
    if !IsDefined(equipmentSystemData) {
      return;
    };
    holsteredArms = ItemID.CreateQuery(record.HolsteredItem().GetID());
    if !ItemID.IsValid(holsteredArms) {
      return;
    };
    transactionSystem = GameInstance.GetTransactionSystem(owner.GetGame());
    itemTags = record.Tags();
    equippedArmsCW = equipmentSystemData.GetActiveItem(gamedataEquipmentArea.ArmsCW);
    slotID = t"AttachmentSlots.RightArm";
    if ItemID.IsValid(equippedArmsCW) {
      if itemToDraw != equippedArmsCW {
        return;
      };
    } else {
      if itemToDraw != equipmentSystemData.GetItemInEquipSlot(equipmentSystemData.GetEquipAreaIndex(gamedataEquipmentArea.BaseFists), 0) {
        return;
      };
    };
    if equipHolsteredItem {
      if !transactionSystem.HasItem(owner, holsteredArms) {
        transactionSystem.GiveItem(owner, holsteredArms, 1);
      };
      EquipmentSystemPlayerData.ForceQualityAndDuplicateStatsShard(owner, itemToDraw, holsteredArms);
      if !transactionSystem.HasItemInSlot(owner, slotID, holsteredArms) {
        equipmentSystemData.EquipItem(holsteredArms, false, false);
      };
    } else {
      if !ArrayContains(itemTags, n"base_fists") {
        equipmentSystemData.UnequipItem(holsteredArms);
      };
    };
  }

  public final static func ForceQualityAndDuplicateStatsShard(owner: ref<PlayerPuppet>, originalItemID: ItemID, destinationItemID: ItemID) -> Void {
    let destinationStatsShard: InnerItemData;
    let duplicateStatsShardItemID: ItemID;
    let emptySlots: array<TweakDBID>;
    let forcedQualityMod: ref<gameStatModifierData>;
    let installPartRequest: ref<InstallItemPart>;
    let itemDataOriginal: ref<gameItemData>;
    let itemModificationSystem: ref<ItemModificationSystem>;
    let originalItemQuality: Float;
    let originalStatsShard: InnerItemData;
    let statsSystem: ref<StatsSystem>;
    let transactionSystem: ref<TransactionSystem>;
    let statsShardSlotTDBID: TweakDBID = t"AttachmentSlots.StatsShardSlot";
    let itemDataDestination: ref<gameItemData> = RPGManager.GetItemData(owner.GetGame(), owner, destinationItemID);
    if !IsDefined(itemDataDestination) {
      return;
    };
    statsSystem = GameInstance.GetStatsSystem(owner.GetGame());
    originalItemQuality = statsSystem.GetStatValue(Cast<StatsObjectID>(originalItemID), gamedataStatType.Quality);
    forcedQualityMod = RPGManager.CreateStatModifier(gamedataStatType.Quality, gameStatModifierType.Additive, originalItemQuality);
    statsSystem.ForceModifier(Cast<StatsObjectID>(itemDataDestination.GetID()), forcedQualityMod);
    itemDataOriginal = RPGManager.GetItemData(owner.GetGame(), owner, originalItemID);
    if itemDataOriginal.HasPartInSlot(statsShardSlotTDBID) {
      itemDataOriginal.GetItemPart(originalStatsShard, statsShardSlotTDBID);
      if itemDataDestination.HasPartInSlot(statsShardSlotTDBID) {
        itemDataDestination.GetItemPart(destinationStatsShard, statsShardSlotTDBID);
        if RPGManager.IsItemEffectivelyIdentical(InnerItemData.GetItemID(originalStatsShard), InnerItemData.GetItemID(destinationStatsShard)) {
          return;
        };
      } else {
        itemDataDestination.GetEmptySlotsOnItem(emptySlots);
        if !ArrayContains(emptySlots, statsShardSlotTDBID) {
          return;
        };
      };
      duplicateStatsShardItemID = ItemID.DuplicateRandomSeedWithOffset(InnerItemData.GetItemID(originalStatsShard), InnerItemData.GetStaticData(originalStatsShard).GetRecordID(), 0);
      transactionSystem = GameInstance.GetTransactionSystem(owner.GetGame());
      transactionSystem.GiveItem(owner, duplicateStatsShardItemID, 1);
      installPartRequest = new InstallItemPart();
      installPartRequest.Set(owner, destinationItemID, duplicateStatsShardItemID, statsShardSlotTDBID);
      itemModificationSystem = GameInstance.GetScriptableSystemsContainer(owner.GetGame()).Get(n"ItemModificationSystem") as ItemModificationSystem;
      itemModificationSystem.QueueRequest(installPartRequest);
    };
  }

  private final func SaveEquipmentSet(const setName: script_ref<String>, setType: EEquipmentSetType) -> Void {
    let areasToSave: array<gamedataEquipmentArea>;
    let equipmentSet: SEquipmentSet;
    let i: Int32;
    let itemInfo: SItemInfo;
    let j: Int32;
    equipmentSet.setName = StringToName(setName);
    switch setType {
      case EEquipmentSetType.Offensive:
        ArrayPush(areasToSave, gamedataEquipmentArea.Weapon);
        ArrayPush(areasToSave, gamedataEquipmentArea.QuickSlot);
        ArrayPush(areasToSave, gamedataEquipmentArea.Consumable);
        ArrayPush(areasToSave, gamedataEquipmentArea.Gadget);
        break;
      case EEquipmentSetType.Defensive:
        break;
      case EEquipmentSetType.Cyberware:
        ArrayPush(areasToSave, gamedataEquipmentArea.ArmsCW);
      default:
    };
    i = 0;
    while i < ArraySize(this.m_equipment.equipAreas) {
      if ArrayContains(areasToSave, this.m_equipment.equipAreas[i].areaType) {
        j = 0;
        while j < ArraySize(this.m_equipment.equipAreas[i].equipSlots) {
          itemInfo.itemID = this.m_equipment.equipAreas[i].equipSlots[j].itemID;
          itemInfo.slotIndex = j;
          ArrayPush(equipmentSet.setItems, itemInfo);
          j += 1;
        };
      };
      i += 1;
    };
    if ArraySize(equipmentSet.setItems) > 0 {
      ArrayPush(this.m_equipment.equipmentSets, equipmentSet);
    };
  }

  private final func LoadEquipmentSet(const setName: script_ref<String>) -> Void {
    let equipmentSet: SEquipmentSet;
    let itemToEquip: ItemID;
    let j: Int32;
    let slotIndex: Int32;
    let i: Int32 = 0;
    while i < ArraySize(this.m_equipment.equipmentSets) {
      equipmentSet = this.m_equipment.equipmentSets[i];
      if Equals(equipmentSet.setName, StringToName(setName)) {
        j = 0;
        while j < ArraySize(equipmentSet.setItems) {
          itemToEquip = equipmentSet.setItems[j].itemID;
          slotIndex = equipmentSet.setItems[j].slotIndex;
          if GameInstance.GetTransactionSystem(this.m_owner.GetGame()).HasItem(this.m_owner, itemToEquip) {
            this.EquipItem(itemToEquip, slotIndex, false);
          };
          j += 1;
        };
        return;
      };
      i += 1;
    };
    this.ClearLastUsedStruct();
  }

  private final func DeleteEquipmentSet(const setName: script_ref<String>) -> Void {
    let equipmentSet: SEquipmentSet;
    let i: Int32 = 0;
    while i < ArraySize(this.m_equipment.equipmentSets) {
      equipmentSet = this.m_equipment.equipmentSets[i];
      if Equals(equipmentSet.setName, StringToName(setName)) {
        ArrayErase(this.m_equipment.equipmentSets, i);
      };
      i += 1;
    };
  }

  private final const func GetEquipAreaIndex(equipAreaID: TweakDBID) -> Int32 {
    let areaType: gamedataEquipmentArea;
    if TDBID.IsValid(equipAreaID) {
      areaType = TweakDBInterface.GetEquipmentAreaRecord(equipAreaID).Type();
      if NotEquals(areaType, gamedataEquipmentArea.Invalid) {
        return this.m_equipAreaIndexCache[EnumInt(areaType)];
      };
    };
    return -1;
  }

  private final const func GetEquipAreaIndex(areaType: gamedataEquipmentArea) -> Int32 {
    if NotEquals(areaType, gamedataEquipmentArea.Invalid) {
      return this.m_equipAreaIndexCache[EnumInt(areaType)];
    };
    return -1;
  }

  private final const func GetEquipArea(areaType: gamedataEquipmentArea) -> SEquipArea {
    let emptyArea: SEquipArea;
    let i: Int32 = 0;
    while i < ArraySize(this.m_equipment.equipAreas) {
      if Equals(this.m_equipment.equipAreas[i].areaType, areaType) {
        return this.m_equipment.equipAreas[i];
      };
      i += 1;
    };
    return emptyArea;
  }

  public final const func GetActiveItemID(equipAreaIndex: Int32) -> ItemID {
    let activeIndex: Int32 = this.m_equipment.equipAreas[equipAreaIndex].activeIndex;
    let activeItem: ItemID = this.GetItemInEquipSlot(equipAreaIndex, activeIndex);
    if activeItem == ItemID.None() {
      activeIndex = this.GetNextActiveItemIndex(equipAreaIndex);
      this.m_equipment.equipAreas[equipAreaIndex].activeIndex = activeIndex;
      activeItem = this.GetItemInEquipSlot(equipAreaIndex, activeIndex);
    };
    return activeItem;
  }

  public final const func GetEquipAreaFromItemID(item: ItemID) -> SEquipArea {
    let voidEmptyArea: SEquipArea;
    let equipAreaIndex: Int32 = this.GetEquipAreaIndex(EquipmentSystem.GetEquipAreaType(item));
    if equipAreaIndex != -1 {
      return this.m_equipment.equipAreas[equipAreaIndex];
    };
    return voidEmptyArea;
  }

  private final const func GetItemInEquipSlot(equipAreaIndex: Int32, slotIndex: Int32) -> ItemID {
    return this.m_equipment.equipAreas[equipAreaIndex].equipSlots[slotIndex].itemID;
  }

  private final const func GetNextActiveItemIndex(equipAreaIndex: Int32) -> Int32 {
    let requiredTags: array<CName>;
    return this.GetNextActiveItemIndex(equipAreaIndex, requiredTags);
  }

  private final const func GetNextActiveItemIndex(equipAreaIndex: Int32, const requiredTags: script_ref<[CName]>) -> Int32 {
    let checkIndex: Int32;
    let equipArea: SEquipArea = this.m_equipment.equipAreas[equipAreaIndex];
    let numSlots: Int32 = ArraySize(equipArea.equipSlots);
    let nextIndex: Int32 = (this.m_equipment.equipAreas[equipAreaIndex].activeIndex + 1) % numSlots;
    let i: Int32 = 0;
    while i < numSlots {
      checkIndex = (nextIndex + i) % numSlots;
      if ItemID.IsValid(equipArea.equipSlots[checkIndex].itemID) && this.CheckTagsInItem(equipArea.equipSlots[checkIndex].itemID, requiredTags) {
        return checkIndex;
      };
      i += 1;
    };
    return 0;
  }

  private final const func CheckTagsInItem(itemID: ItemID, const requiredTags: script_ref<[CName]>) -> Bool {
    let itemTags: array<CName>;
    let tagNo: Int32;
    if ArraySize(Deref(requiredTags)) > 0 {
      itemTags = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID)).Tags();
      tagNo = 0;
      while tagNo < ArraySize(Deref(requiredTags)) {
        if !ArrayContains(itemTags, Deref(requiredTags)[tagNo]) {
          return false;
        };
        tagNo += 1;
      };
    };
    return true;
  }

  private final const func GetPlacementSlot(equipAreaIndex: Int32, slotIndex: Int32) -> TweakDBID {
    return EquipmentSystem.GetPlacementSlot(this.GetItemInEquipSlot(equipAreaIndex, slotIndex));
  }

  private final const func HasItemInInventory(item: ItemID) -> Bool {
    return GameInstance.GetTransactionSystem(this.m_owner.GetGame()).HasItem(this.m_owner, item);
  }

  private final const func HasItemEquipped(equipAreaIndex: Int32, opt slotIndex: Int32) -> Bool {
    return ItemID.IsValid(this.m_equipment.equipAreas[equipAreaIndex].equipSlots[slotIndex].itemID);
  }

  public final const func GetSlotIndex(itemID: ItemID) -> Int32 {
    let equipAreaType: gamedataEquipmentArea;
    let equipSlots: array<SEquipSlot>;
    let i: Int32;
    let j: Int32;
    if ItemID.IsValid(itemID) {
      equipAreaType = EquipmentSystem.GetEquipAreaType(itemID);
      i = this.GetEquipAreaIndex(equipAreaType);
      if i >= 0 {
        equipSlots = this.m_equipment.equipAreas[i].equipSlots;
        j = 0;
        while j < ArraySize(equipSlots) {
          if equipSlots[j].itemID == itemID {
            return j;
          };
          j += 1;
        };
      };
    };
    return -1;
  }

  public final const func GetSlotIndex(itemID: ItemID, equipAreaType: gamedataEquipmentArea) -> Int32 {
    let equipSlots: array<SEquipSlot>;
    let i: Int32;
    let j: Int32;
    if ItemID.IsValid(itemID) {
      i = this.GetEquipAreaIndex(equipAreaType);
      if i >= 0 {
        equipSlots = this.m_equipment.equipAreas[i].equipSlots;
        j = 0;
        while j < ArraySize(equipSlots) {
          if equipSlots[j].itemID == itemID {
            return j;
          };
          j += 1;
        };
      };
    };
    return -1;
  }

  public final const func IsSlotLocked(equipAreaType: gamedataEquipmentArea, index: Int32, out visibleWhenLocked: Bool) -> Bool {
    let equipSlots: array<SEquipSlot>;
    let retVal: Bool = false;
    let i: Int32 = this.GetEquipAreaIndex(equipAreaType);
    if i >= 0 {
      equipSlots = this.m_equipment.equipAreas[i].equipSlots;
      if index < ArraySize(equipSlots) {
        retVal = this.IsSlotLocked(equipSlots[index], visibleWhenLocked);
      };
    };
    return retVal;
  }

  private final const func IsSlotLocked(slot: SEquipSlot, out visibleWhenLocked: Bool) -> Bool {
    let retVal: Bool = !slot.unlockPrereq.IsFulfilled(this.m_owner.GetGame(), this.m_owner);
    visibleWhenLocked = slot.visibleWhenLocked;
    return retVal;
  }

  private final const func GetOwnerGender() -> CName {
    return this.m_owner.GetResolvedGenderName();
  }

  private final const func GetItemAppearanceForGender(itemID: ItemID) -> CName {
    let appearanceName: CName;
    let gender: CName = this.GetOwnerGender();
    if Equals(gender, n"Female") {
      appearanceName = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID)).AppearanceName();
    } else {
      appearanceName = TweakDBInterface.GetCName(ItemID.GetTDBID(itemID) + t".appearanceNameMale", n"None");
    };
    return appearanceName;
  }

  public final const func GetItemInEquipSlot(areaType: gamedataEquipmentArea, slotIndex: Int32) -> ItemID {
    return this.m_equipment.equipAreas[this.GetEquipAreaIndex(areaType)].equipSlots[slotIndex].itemID;
  }

  public final const func GetNumberOfSlots(areaType: gamedataEquipmentArea, opt includeLocked: Bool) -> Int32 {
    let i: Int32;
    let size: Int32;
    let retVal: Int32 = 0;
    let slots: array<SEquipSlot> = this.m_equipment.equipAreas[this.GetEquipAreaIndex(areaType)].equipSlots;
    if includeLocked {
      retVal = ArraySize(slots);
    } else {
      i = 0;
      size = ArraySize(slots);
      while i < size {
        if !this.IsSlotLocked(slots[i], false) {
          retVal += 1;
        };
        i += 1;
      };
    };
    return retVal;
  }

  public final const func GetNumberOfItemsInEquipmentArea(areaType: gamedataEquipmentArea) -> Int32 {
    let items: Int32;
    let equipArea: SEquipArea = this.m_equipment.equipAreas[this.GetEquipAreaIndex(areaType)];
    let i: Int32 = 0;
    while i < ArraySize(equipArea.equipSlots) {
      if ItemID.IsValid(equipArea.equipSlots[i].itemID) {
        items += 1;
      };
      i += 1;
    };
    return items;
  }

  public final const func GetNumberEquippedWeapons() -> Int32 {
    let numWeaponsEquipped: Int32 = 0;
    let equipAreaIndex: Int32 = this.GetEquipAreaIndex(gamedataEquipmentArea.WeaponWheel);
    let equipArea: SEquipArea = this.m_equipment.equipAreas[equipAreaIndex];
    let i: Int32 = 0;
    while i < ArraySize(equipArea.equipSlots) {
      if this.HasItemEquipped(equipAreaIndex, i) {
        numWeaponsEquipped += 1;
      };
      i += 1;
    };
    return numWeaponsEquipped;
  }

  public final const func GetEquippedQuestItems() -> [ItemID] {
    let itemData: wref<gameItemData>;
    let itemID: ItemID;
    let j: Int32;
    let questItems: array<ItemID>;
    let i: Int32 = 0;
    while i < ArraySize(this.m_equipment.equipAreas) {
      j = 0;
      while j < ArraySize(this.m_equipment.equipAreas[i].equipSlots) {
        itemID = this.m_equipment.equipAreas[i].equipSlots[j].itemID;
        if ItemID.IsValid(itemID) {
          itemData = GameInstance.GetTransactionSystem(this.m_owner.GetGame()).GetItemData(this.m_owner, itemID);
          if itemData.HasTag(n"Quest") {
            ArrayPush(questItems, itemID);
          };
        };
        j += 1;
      };
      i += 1;
    };
    return questItems;
  }

  public final const func GetActiveItem(equipArea: gamedataEquipmentArea) -> ItemID {
    return this.GetActiveItemID(this.GetEquipAreaIndex(equipArea));
  }

  public final const func GetActiveWeaponObject(equipArea: gamedataEquipmentArea) -> ref<ItemObject> {
    let itemID: ItemID = this.GetActiveItem(equipArea);
    return GameInstance.GetTransactionSystem(this.m_owner.GetGame()).GetItemInSlotByItemID(this.m_owner, itemID);
  }

  public final const func GetNextActiveItem(equipArea: gamedataEquipmentArea) -> ItemID {
    return this.GetItemInEquipSlot(equipArea, this.GetNextActiveItemIndex(this.GetEquipAreaIndex(equipArea)));
  }

  public final const func GetActiveConsumable() -> ItemID {
    let consumable: ItemID;
    let blackboard: ref<IBlackboard> = GameInstance.GetBlackboardSystem(this.m_owner.GetGame()).Get(GetAllBlackboardDefs().UI_QuickSlotsData);
    let containerConsumable: ItemID = FromVariant<ItemID>(blackboard.GetVariant(GetAllBlackboardDefs().UI_QuickSlotsData.containerConsumable));
    if ItemID.IsValid(containerConsumable) {
      return containerConsumable;
    };
    consumable = this.GetItemIDFromHotkey(EHotkey.DPAD_UP);
    if ItemID.IsValid(consumable) {
      return consumable;
    };
    return ItemID.None();
  }

  public final const func GetNextWeaponWheelItem() -> ItemID {
    let requiredTags: array<CName>;
    let weaponWheelEquipArea: gamedataEquipmentArea = gamedataEquipmentArea.WeaponWheel;
    if IsMultiplayer() || GameInstance.GetPlayerSystem(this.m_owner.GetGame()).IsCPOControlSchemeForced() {
      ArrayPush(requiredTags, n"RangedWeapon");
    };
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"OneHandedFirearms") {
      ArrayPush(requiredTags, n"OneHandedRangedWeapon");
    } else {
      if StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"DriverCombatFirearms") {
        ArrayPush(requiredTags, n"DriverCombatRangedWeapon");
      } else {
        if StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"DriverCombatBikeWeapons") {
          ArrayPush(requiredTags, n"DriverCombatBikeWeapon");
        };
      };
    };
    return this.GetItemInEquipSlot(weaponWheelEquipArea, this.GetNextActiveItemIndex(this.GetEquipAreaIndex(weaponWheelEquipArea), requiredTags));
  }

  public final const func GetActiveHeavyWeapon() -> ItemID {
    return this.GetActiveItem(gamedataEquipmentArea.WeaponHeavy);
  }

  public final const func GetActiveGadget() -> ItemID {
    let gadget: ItemID = this.GetItemIDFromHotkey(EHotkey.RB);
    if ItemID.IsValid(gadget) {
      return gadget;
    };
    return ItemID.None();
  }

  public final const func GetActiveCyberware() -> ItemID {
    let equipArea: SEquipArea;
    let i: Int32;
    let moduleID: ItemID;
    let moduleRecord: ref<Item_Record>;
    if Equals(TweakDBInterface.GetItemRecord(ItemID.GetTDBID(this.GetItemIDFromHotkey(EHotkey.RB))).ItemType().Type(), gamedataItemType.Cyb_Launcher) {
      return this.GetItemIDFromHotkey(EHotkey.RB);
    };
    equipArea = this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.ArmsCW)];
    i = 0;
    while i < this.GetNumberOfSlots(gamedataEquipmentArea.ArmsCW) {
      moduleID = equipArea.equipSlots[i].itemID;
      if ItemID.IsValid(moduleID) {
        moduleRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(moduleID));
        if Equals(moduleRecord.ItemType().Type(), gamedataItemType.Cyb_Launcher) {
          return moduleID;
        };
      } else {
        moduleID = this.GetActiveGadget();
        if ItemID.IsValid(moduleID) {
          moduleRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(moduleID));
          if Equals(moduleRecord.ItemType().Type(), gamedataItemType.Cyb_Ability) {
            return moduleID;
          };
        };
      };
      i += 1;
    };
    return ItemID.None();
  }

  public final const func GetAllAbilityCyberwareSlots() -> [SEquipSlot] {
    let cyberwareList: array<SEquipSlot>;
    this.GetItemsByEquipAreaAndItemType(gamedataEquipmentArea.ArmsCW, gamedataItemType.Cyb_Launcher, cyberwareList);
    this.GetItemsByEquipAreaAndItemType(gamedataEquipmentArea.AbilityCW, gamedataItemType.Cyb_Launcher, cyberwareList);
    return cyberwareList;
  }

  public final const func GetAllCyberwareEquipmentAreas() -> [gamedataEquipmentArea] {
    let equipmentAreas: array<gamedataEquipmentArea>;
    ArrayPush(equipmentAreas, gamedataEquipmentArea.SystemReplacementCW);
    ArrayPush(equipmentAreas, gamedataEquipmentArea.FrontalCortexCW);
    ArrayPush(equipmentAreas, gamedataEquipmentArea.EyesCW);
    ArrayPush(equipmentAreas, gamedataEquipmentArea.MusculoskeletalSystemCW);
    ArrayPush(equipmentAreas, gamedataEquipmentArea.NervousSystemCW);
    ArrayPush(equipmentAreas, gamedataEquipmentArea.CardiovascularSystemCW);
    ArrayPush(equipmentAreas, gamedataEquipmentArea.IntegumentarySystemCW);
    ArrayPush(equipmentAreas, gamedataEquipmentArea.ArmsCW);
    ArrayPush(equipmentAreas, gamedataEquipmentArea.LegsCW);
    ArrayPush(equipmentAreas, gamedataEquipmentArea.HandsCW);
    return equipmentAreas;
  }

  public final const func GetActiveMeleeWare() -> ItemID {
    let moduleID: ItemID;
    let moduleRecord: ref<Item_Record>;
    let equipArea: SEquipArea = this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.ArmsCW)];
    let i: Int32 = 0;
    while i < this.GetNumberOfSlots(gamedataEquipmentArea.ArmsCW) {
      moduleID = equipArea.equipSlots[i].itemID;
      if ItemID.IsValid(moduleID) {
        moduleRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(moduleID));
        if Equals(moduleRecord.ItemCategory().Type(), gamedataItemCategory.Weapon) {
          return moduleID;
        };
      };
      i += 1;
    };
    return ItemID.None();
  }

  public final const func IsEquipped(item: ItemID) -> Bool {
    return this.GetSlotIndex(item) >= 0;
  }

  public final const func IsEquipped(item: ItemID, equipmentArea: gamedataEquipmentArea) -> Bool {
    return this.GetSlotIndex(item, equipmentArea) >= 0;
  }

  public final const func IsSideUpgradeEquipped(itemID: ItemID) -> Bool {
    let i: Int32;
    let itemRecord: ref<Item_Record>;
    let returnArray: array<SEquipSlot>;
    let sideUpgradeRecord: ref<Item_Record>;
    let retVal: Bool = false;
    if RPGManager.CyberwareHasSideUpgrade(itemID, sideUpgradeRecord) {
      itemRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID));
      this.GetItemsByEquipAreaAndItemType(EquipmentSystem.GetEquipAreaType(itemID), itemRecord.ItemType().Type(), returnArray);
      i = 0;
      while i < ArraySize(returnArray) {
        if ItemID.GetTDBID(returnArray[i].itemID) == sideUpgradeRecord.GetID() {
          retVal = true;
        };
        i += 1;
      };
    };
    return retVal;
  }

  public final const func GetOriginalItemIDFromSideUpgrade(sideUpgradeID: ItemID) -> ItemID {
    let itemID: ItemID;
    let itemList: array<wref<gameItemData>>;
    let j: Int32;
    let partData: InnerItemData;
    let shardTDBID: TweakDBID;
    let sideUpgradeRecord: ref<Item_Record>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let statsShardSlotTDBID: TweakDBID = t"AttachmentSlots.StatsShardSlot";
    if !ItemID.IsValid(sideUpgradeID) {
      return ItemID.None();
    };
    sideUpgradeRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(sideUpgradeID));
    transactionSystem.GetItemListByTags(this.m_owner, sideUpgradeRecord.Tags(), itemList);
    j = 0;
    while j < ArraySize(itemList) {
      itemID = itemList[j].GetID();
      if itemID != sideUpgradeID && Equals(TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID)).Quality().Type(), sideUpgradeRecord.Quality().Type()) && Equals(TweakDBInterface.GetCName(ItemID.GetTDBID(itemID) + t".cyberwareType", n"None"), TweakDBInterface.GetCName(ItemID.GetTDBID(sideUpgradeID) + t".cyberwareType", n"None")) {
        transactionSystem.GetItemData(this.m_owner, itemID).GetItemPart(partData, statsShardSlotTDBID);
        shardTDBID = InnerItemData.GetStaticData(partData).GetRecordID();
        transactionSystem.GetItemData(this.m_owner, sideUpgradeID).GetItemPart(partData, statsShardSlotTDBID);
        if shardTDBID == InnerItemData.GetStaticData(partData).GetRecordID() {
          return itemID;
        };
      };
      j += 1;
    };
    return ItemID.None();
  }

  public final func GetLastUsedWeaponItemID() -> ItemID {
    let item: ItemID = ItemID.None();
    let lastUsedWeaponID: ItemID = ItemID.None();
    item = this.FindItemInEquipArea(this.GetLastUsedItemID(ELastUsed.Heavy), gamedataEquipmentArea.WeaponHeavy);
    if !ItemID.IsValid(item) {
      lastUsedWeaponID = this.GetLastUsedItemID(ELastUsed.Weapon);
      item = this.FindItemInEquipArea(lastUsedWeaponID, gamedataEquipmentArea.WeaponWheel);
      if !ItemID.IsValid(item) {
        item = this.FindItemInEquipArea(lastUsedWeaponID, gamedataEquipmentArea.Weapon);
      };
      if !ItemID.IsValid(item) {
        item = this.FindItemInEquipArea(lastUsedWeaponID, gamedataEquipmentArea.ArmsCW);
      };
      if !ItemID.IsValid(item) {
        item = this.FindItemInEquipArea(lastUsedWeaponID, gamedataEquipmentArea.BaseFists);
      };
    };
    if this.CheckWeaponAgainstGameplayRestrictions(item, true) {
      return item;
    };
    return ItemID.None();
  }

  public final func GetActiveWeaponToUnequip() -> ItemID {
    let equipArea: SEquipArea = this.GetEquipAreaFromItemID(this.GetSlotActiveItem(EquipmentManipulationRequestSlot.Right));
    if Equals(equipArea.areaType, gamedataEquipmentArea.BaseFists) {
      return this.GetFistsItemID();
    };
    return this.GetActiveItem(equipArea.areaType);
  }

  public final func GetActiveWeapon() -> ItemID {
    return this.GetActiveItem(gamedataEquipmentArea.WeaponWheel);
  }

  public final func GetSlotActiveWeapon() -> ItemID {
    return this.GetSlotActiveItem(EquipmentManipulationRequestSlot.Right);
  }

  public final func GetFirstMeleeWeaponItemID() -> ItemID {
    let item: ItemID = ItemID.None();
    item = this.GetActiveMeleeWare();
    if ItemID.IsValid(item) {
      return item;
    };
    item = this.FindItemInEquipAreaByTag(n"MeleeWeapon", gamedataEquipmentArea.Weapon);
    if ItemID.IsValid(item) {
      return item;
    };
    item = this.GetFistsItemID();
    if ItemID.IsValid(item) {
      return item;
    };
    return item;
  }

  public final func GetLastUsedMeleeWeaponItemID() -> ItemID {
    let item: ItemID = ItemID.None();
    let lastUsedWeaponID: ItemID = ItemID.None();
    lastUsedWeaponID = this.GetLastUsedItemID(ELastUsed.Melee);
    item = this.FindItemInEquipArea(lastUsedWeaponID, gamedataEquipmentArea.WeaponWheel);
    if ItemID.IsValid(item) {
      return item;
    };
    item = this.FindItemInEquipArea(lastUsedWeaponID, gamedataEquipmentArea.ArmsCW);
    if ItemID.IsValid(item) {
      return item;
    };
    item = this.FindItemInEquipArea(lastUsedWeaponID, gamedataEquipmentArea.BaseFists);
    if ItemID.IsValid(item) {
      return item;
    };
    return item;
  }

  public final func GetLastUsedOrFirstAvailableWeapon() -> ItemID {
    let item: ItemID = ItemID.None();
    item = this.FindItemInEquipArea(this.GetLastUsedItemID(ELastUsed.Weapon), gamedataEquipmentArea.WeaponWheel);
    if ItemID.IsValid(item) {
      return item;
    };
    item = this.FindItemInEquipArea(this.GetLastUsedItemID(ELastUsed.Weapon), gamedataEquipmentArea.ArmsCW);
    if ItemID.IsValid(item) {
      return item;
    };
    item = this.FindItemInEquipArea(this.GetLastUsedItemID(ELastUsed.Heavy), gamedataEquipmentArea.WeaponHeavy);
    if ItemID.IsValid(item) {
      return item;
    };
    item = this.FindItemInEquipArea(this.GetLastUsedItemID(ELastUsed.Weapon), gamedataEquipmentArea.BaseFists);
    if ItemID.IsValid(item) {
      return item;
    };
    item = EquipmentSystem.GetFirstAvailableWeapon(this.m_owner);
    if ItemID.IsValid(item) {
      return item;
    };
    return item;
  }

  public final func GetLastUsedOrFirstAvailableRangedWeapon() -> ItemID {
    let item: ItemID = ItemID.None();
    item = this.FindItemInEquipArea(this.GetLastUsedItemID(ELastUsed.Ranged), gamedataEquipmentArea.WeaponWheel);
    if ItemID.IsValid(item) {
      return item;
    };
    item = this.FindItemInEquipAreaByTag(n"RangedWeapon", gamedataEquipmentArea.Weapon);
    if ItemID.IsValid(item) {
      return item;
    };
    return item;
  }

  public final func GetLastUsedOrFirstAvailableMeleeWeapon() -> ItemID {
    let item: ItemID = ItemID.None();
    item = this.GetLastUsedMeleeWeaponItemID();
    if ItemID.IsValid(item) {
      return item;
    };
    item = this.GetFirstMeleeWeaponItemID();
    if ItemID.IsValid(item) {
      return item;
    };
    return item;
  }

  public final func GetLastUsedOrFirstAvailableOneHandedRangedWeapon() -> ItemID {
    let itemTags: array<CName>;
    let item: ItemID = ItemID.None();
    item = this.FindItemInEquipArea(this.GetLastUsedItemID(ELastUsed.Ranged), gamedataEquipmentArea.WeaponWheel);
    if ItemID.IsValid(item) {
      itemTags = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item)).Tags();
      if ArrayContains(itemTags, n"OneHandedRangedWeapon") {
        return item;
      };
    };
    item = this.FindItemInEquipAreaByTag(n"OneHandedRangedWeapon", gamedataEquipmentArea.Weapon);
    return item;
  }

  public final func GetLastUsedOrFirstAvailableDriverCombatWeapon(driverCombatWeaponTag: CName) -> ItemID {
    let itemTags: array<CName>;
    let item: ItemID = ItemID.None();
    item = this.FindItemInEquipArea(this.GetLastUsedItemID(ELastUsed.Weapon), gamedataEquipmentArea.WeaponWheel);
    if ItemID.IsValid(item) {
      itemTags = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item)).Tags();
      if ArrayContains(itemTags, driverCombatWeaponTag) && this.CheckWeaponAgainstGameplayRestrictions(item) {
        return item;
      };
    };
    item = this.FindItemInEquipAreaByTag(driverCombatWeaponTag, gamedataEquipmentArea.Weapon);
    if ItemID.IsValid(item) && this.CheckWeaponAgainstGameplayRestrictions(item) {
      return item;
    };
    return ItemID.None();
  }

  public final func GetWeaponSlotItem(weaponSlot: Int32) -> ItemID {
    let activeItem: ItemID;
    let requestedWeapon: ItemID;
    let equipArea: SEquipArea = this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.WeaponWheel)];
    let item: ref<ItemObject> = GameInstance.GetTransactionSystem(this.m_owner.GetGame()).GetItemInSlot(this.m_owner, t"AttachmentSlots.WeaponRight");
    if IsDefined(item) {
      activeItem = item.GetItemID();
    };
    if weaponSlot == 4 {
      requestedWeapon = this.GetMeleewareOrFistsItemID();
    } else {
      requestedWeapon = equipArea.equipSlots[weaponSlot - 1].itemID;
    };
    if ItemID.IsValid(requestedWeapon) && requestedWeapon != activeItem && this.CheckWeaponAgainstGameplayRestrictions(requestedWeapon) {
      return requestedWeapon;
    };
    return ItemID.None();
  }

  public final func GetNextThrowableWeapon() -> ItemID {
    let itemTags: array<CName>;
    let nextItem: ItemID;
    let equipArea: SEquipArea = this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.WeaponWheel)];
    let eqAreaSize: Int32 = ArraySize(equipArea.equipSlots);
    let activeItem: ItemID = this.GetLastUsedWeaponItemID();
    let currentItemSlot: Int32 = equipArea.activeIndex;
    let nextItemSlot: Int32 = currentItemSlot;
    let i: Int32 = 0;
    while i < eqAreaSize {
      nextItemSlot = (nextItemSlot + 1) % eqAreaSize;
      nextItem = equipArea.equipSlots[nextItemSlot].itemID;
      if ItemID.IsValid(nextItem) && nextItem != activeItem && this.CheckWeaponAgainstGameplayRestrictions(nextItem, true) {
        itemTags = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(nextItem)).Tags();
        if ArrayContains(itemTags, n"Throwable") {
          return nextItem;
        };
      };
      i += 1;
    };
    return this.CycleWeapon(true, false);
  }

  public final func CycleWeapon(cycleNext: Bool, onlyCheck: Bool) -> ItemID {
    let activeItem: ItemID;
    let barebonesWeapon: ItemID;
    let currentItemSlot: Int32;
    let direction: Int32;
    let i: Int32;
    let nextItem: ItemID;
    let nextItemSlot: Int32;
    let x: Int32;
    let equipArea: SEquipArea = this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.WeaponWheel)];
    let eqAreaSize: Int32 = ArraySize(equipArea.equipSlots);
    if !(this.m_owner as PlayerPuppet).ShouldAllowCycleToFistCyberware() {
      eqAreaSize = eqAreaSize - 1;
    };
    activeItem = this.GetLastUsedWeaponItemID();
    currentItemSlot = equipArea.activeIndex;
    nextItemSlot = currentItemSlot;
    barebonesWeapon = this.GetMeleewareOrFistsItemID();
    direction = cycleNext ? 1 : -1;
    if Equals(EquipmentSystem.GetEquipAreaType(activeItem), gamedataEquipmentArea.WeaponHeavy) {
      nextItem = this.GetActiveWeapon();
      if ItemID.IsValid(nextItem) {
        return nextItem;
      };
    };
    i = 0;
    while i < eqAreaSize {
      x = nextItemSlot + direction;
      nextItemSlot = (x % eqAreaSize + eqAreaSize) % eqAreaSize;
      nextItem = equipArea.equipSlots[nextItemSlot].itemID;
      if ItemID.IsValid(nextItem) && nextItem != activeItem && this.CheckWeaponAgainstGameplayRestrictions(nextItem, true) {
        return nextItem;
      };
      if !ItemID.IsValid(nextItem) && activeItem != barebonesWeapon && nextItemSlot + direction != eqAreaSize && this.CheckWeaponAgainstGameplayRestrictions(barebonesWeapon, true) {
        if !WeaponObject.IsCyberwareWeapon(barebonesWeapon) && nextItemSlot == eqAreaSize - 1 {
        } else {
          if WeaponObject.IsCyberwareWeapon(barebonesWeapon) && nextItemSlot != eqAreaSize - 1 {
          } else {
            if !onlyCheck {
              this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.WeaponWheel)].activeIndex = nextItemSlot;
            };
            return barebonesWeapon;
          };
        };
      };
      i += 1;
    };
    return ItemID.None();
  }

  public final func CheckWeaponAgainstGameplayRestrictions(weaponItem: ItemID, opt suppressDriverWarnings: Bool) -> Bool {
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(weaponItem));
    let itemType: gamedataItemType = itemRecord.ItemType().Type();
    let equipmentArea: gamedataEquipmentArea = itemRecord.EquipArea().Type();
    let itemTags: array<CName> = itemRecord.Tags();
    let notificationEvent: ref<UIInGameNotificationEvent> = new UIInGameNotificationEvent();
    notificationEvent.m_notificationType = UIInGameNotificationType.ActionRestriction;
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"NoWeapons") {
      GameInstance.GetUISystem((this.m_owner as PlayerPuppet).GetGame()).QueueEvent(notificationEvent);
      return false;
    };
    if Equals(equipmentArea, gamedataEquipmentArea.ArmsCW) && StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"NoArmsCW") && NotEquals(itemType, gamedataItemType.Cyb_StrongArms) {
      return false;
    };
    if ArrayContains(itemTags, n"RangedWeapon") {
      if StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"Melee") || StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"Fists") {
        GameInstance.GetUISystem((this.m_owner as PlayerPuppet).GetGame()).QueueEvent(notificationEvent);
        return false;
      };
      if StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"OneHandedFirearms") && !ArrayContains(itemTags, n"OneHandedRangedWeapon") {
        GameInstance.GetUISystem((this.m_owner as PlayerPuppet).GetGame()).QueueEvent(notificationEvent);
        return false;
      };
      if StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"DriverCombatFirearms") && !ArrayContains(itemTags, n"DriverCombatRangedWeapon") {
        if !suppressDriverWarnings {
          GameInstance.GetUISystem((this.m_owner as PlayerPuppet).GetGame()).QueueEvent(notificationEvent);
        };
        return false;
      };
      if StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"DriverCombatBikeWeapons") && !ArrayContains(itemTags, n"DriverCombatBikeWeapon") {
        if !suppressDriverWarnings {
          GameInstance.GetUISystem((this.m_owner as PlayerPuppet).GetGame()).QueueEvent(notificationEvent);
        };
        return false;
      };
      return true;
    };
    if ArrayContains(itemTags, n"MeleeWeapon") {
      if StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"Firearms") || StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"OneHandedFirearms") {
        GameInstance.GetUISystem((this.m_owner as PlayerPuppet).GetGame()).QueueEvent(notificationEvent);
        return false;
      };
      if StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"DriverCombatFirearms") && !ArrayContains(itemTags, n"DriverCombatRangedWeapon") {
        if !suppressDriverWarnings {
          GameInstance.GetUISystem((this.m_owner as PlayerPuppet).GetGame()).QueueEvent(notificationEvent);
        };
        return false;
      };
      if StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"DriverCombatBikeWeapons") && !ArrayContains(itemTags, n"DriverCombatBikeWeapon") {
        if !suppressDriverWarnings {
          GameInstance.GetUISystem((this.m_owner as PlayerPuppet).GetGame()).QueueEvent(notificationEvent);
        };
        return false;
      };
      if !WeaponObject.IsFists(weaponItem) && StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"Fists") && !StatusEffectSystem.ObjectHasStatusEffect(this.m_owner, t"GameplayRestriction.FistFight") {
        GameInstance.GetUISystem((this.m_owner as PlayerPuppet).GetGame()).QueueEvent(notificationEvent);
        return false;
      };
      if !WeaponObject.IsFists(weaponItem) && !WeaponObject.IsOfType(weaponItem, gamedataItemType.Cyb_StrongArms) && StatusEffectSystem.ObjectHasStatusEffect(this.m_owner, t"GameplayRestriction.FistFight") {
        GameInstance.GetUISystem((this.m_owner as PlayerPuppet).GetGame()).QueueEvent(notificationEvent);
        return false;
      };
      return true;
    };
    if !suppressDriverWarnings || !StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"DriverCombatFirearms") && !StatusEffectSystem.ObjectHasStatusEffectWithTag(this.m_owner, n"DriverCombatBikeWeapons") {
      GameInstance.GetUISystem((this.m_owner as PlayerPuppet).GetGame()).QueueEvent(notificationEvent);
    };
    return false;
  }

  private final func RequestEquipmentStateMachine(reqType: EquipmentManipulationRequestType, reqSlot: EquipmentManipulationRequestSlot, equipAnim: gameEquipAnimationType, referenceName: CName, requestName: CName) -> Void {
    let instanceData: StateMachineInstanceData;
    let weaRequest: ref<EquipmentManipulationRequest> = new EquipmentManipulationRequest();
    let psmRequest: ref<PSMPostponedParameterScriptable> = new PSMPostponedParameterScriptable();
    let psmAdd: ref<PSMAddOnDemandStateMachine> = new PSMAddOnDemandStateMachine();
    let equipmentInitData: ref<EquipmentInitData> = new EquipmentInitData();
    if (this.m_owner as PlayerPuppet).GetPlayerStateMachineBlackboard().GetInt(GetAllBlackboardDefs().PlayerStateMachine.UpperBody) != 5 {
      weaRequest.requestType = reqType;
      weaRequest.requestSlot = reqSlot;
      weaRequest.equipAnim = equipAnim;
      psmRequest.value = weaRequest;
      psmRequest.id = requestName;
      psmRequest.aspect = gamestateMachineParameterAspect.Permanent;
      this.m_owner.QueueEvent(psmRequest);
    };
    psmAdd.stateMachineName = n"Equipment";
    instanceData.referenceName = referenceName;
    psmAdd.instanceData = instanceData;
    equipmentInitData.eqManipulationVarName = requestName;
    psmAdd.instanceData.initData = equipmentInitData;
    psmAdd.owner = this.m_owner;
    this.m_owner.QueueEvent(psmAdd);
  }

  public final func SendPSMWeaponManipulationRequest(reqType: EquipmentManipulationRequestType, reqSlot: EquipmentManipulationRequestSlot, equipAnim: gameEquipAnimationType) -> Void {
    if Equals(reqSlot, EquipmentManipulationRequestSlot.Right) || Equals(reqSlot, EquipmentManipulationRequestSlot.Both) {
      this.RequestEquipmentStateMachine(reqType, reqSlot, equipAnim, n"RightHand", n"EqManipulationRight");
    };
    if Equals(reqSlot, EquipmentManipulationRequestSlot.Left) || Equals(reqSlot, EquipmentManipulationRequestSlot.Both) {
      this.RequestEquipmentStateMachine(reqType, reqSlot, equipAnim, n"LeftHand", n"EqManipulationLeft");
    };
  }

  public final func FindItemInEquipArea(item: ItemID, area: gamedataEquipmentArea) -> ItemID {
    let items: array<ItemID> = EquipmentSystem.GetItemsInArea(this.m_owner, area);
    let i: Int32 = 0;
    while i < ArraySize(items) {
      if items[i] == item {
        return items[i];
      };
      i += 1;
    };
    return ItemID.None();
  }

  public final func FindItemInEquipAreaByTag(tag: CName, area: gamedataEquipmentArea) -> ItemID {
    let itemTags: array<CName>;
    let items: array<ItemID> = EquipmentSystem.GetItemsInArea(this.m_owner, area);
    let i: Int32 = 0;
    while i < ArraySize(items) {
      itemTags = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(items[i])).Tags();
      if ArrayContains(itemTags, tag) {
        return items[i];
      };
      i += 1;
    };
    return ItemID.None();
  }

  public final func RemoveItemFromEquipSlot(item: ItemID) -> Void {
    let equipAreaIndex: Int32 = this.GetEquipAreaIndex(EquipmentSystem.GetEquipAreaType(item));
    let slotIndex: Int32 = this.GetSlotIndex(item);
    this.m_equipment.equipAreas[equipAreaIndex].equipSlots[slotIndex].itemID = ItemID.None();
  }

  private final const func UpdateWeaponWheel() -> Void {
    let meleeWare: SEquipSlot;
    let weaponWheelItems: array<SEquipSlot> = this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.Weapon)].equipSlots;
    meleeWare.itemID = this.GetActiveMeleeWare();
    ArrayPush(weaponWheelItems, meleeWare);
    this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.WeaponWheel)].equipSlots = weaponWheelItems;
  }

  public final func ClearAllWeaponSlots() -> Void {
    let index: Int32 = this.GetEquipAreaIndex(gamedataEquipmentArea.Weapon);
    let i: Int32 = 0;
    while i < 3 {
      this.m_equipment.equipAreas[index].equipSlots[i].itemID = ItemID.None();
      i += 1;
    };
    this.UpdateWeaponWheel();
  }

  private final const func UpdateQuickWheel() -> Void {
    let returnArray: array<SEquipSlot>;
    this.GetItemsByEquipAreaAndItemType(gamedataEquipmentArea.QuickSlot, gamedataItemType.Invalid, returnArray);
    this.GetItemsByEquipAreaAndItemType(gamedataEquipmentArea.IntegumentarySystemCW, gamedataItemType.Invalid, returnArray);
    this.GetItemsByEquipAreaAndItemType(gamedataEquipmentArea.ArmsCW, gamedataItemType.Cyb_Launcher, returnArray);
    this.GetItemsByEquipAreaAndItemType(gamedataEquipmentArea.SystemReplacementCW, gamedataItemType.Cyb_Ability, returnArray);
    this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.QuickWheel)].equipSlots = returnArray;
  }

  private final const func GetItemsByEquipAreaAndItemType(equipmentArea: gamedataEquipmentArea, itemType: gamedataItemType, outItems: script_ref<[SEquipSlot]>) -> Void {
    let record: ref<Item_Record>;
    let equipSlots: array<SEquipSlot> = this.m_equipment.equipAreas[this.GetEquipAreaIndex(equipmentArea)].equipSlots;
    let i: Int32 = 0;
    while i < ArraySize(equipSlots) {
      if ItemID.IsValid(equipSlots[i].itemID) {
        if Equals(itemType, gamedataItemType.Invalid) {
          ArrayPush(Deref(outItems), equipSlots[i]);
        } else {
          record = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(equipSlots[i].itemID));
          if Equals(record.ItemType().Type(), itemType) {
            ArrayPush(Deref(outItems), equipSlots[i]);
          };
        };
      };
      i += 1;
    };
  }

  private final const func GetAllAbilityCyberwareSlotsByEquipmentArea(equipmentArea: gamedataEquipmentArea, outCyberwareList: script_ref<[SEquipSlot]>) -> Void {
    let cyberwareID: ItemID;
    let cyberwareRecord: ref<Item_Record>;
    let equipArea: SEquipArea = this.m_equipment.equipAreas[this.GetEquipAreaIndex(equipmentArea)];
    let i: Int32 = 0;
    while i < this.GetNumberOfSlots(equipmentArea) {
      cyberwareID = equipArea.equipSlots[i].itemID;
      if ItemID.IsValid(cyberwareID) {
        cyberwareRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(cyberwareID));
        if Equals(cyberwareRecord.ItemType().Type(), gamedataItemType.Cyb_Launcher) {
          ArrayPush(Deref(outCyberwareList), equipArea.equipSlots[i]);
        };
      };
      i += 1;
    };
  }

  private final func GetFistsItemID() -> ItemID {
    let items: array<ItemID> = EquipmentSystem.GetItemsInArea(this.m_owner, gamedataEquipmentArea.BaseFists);
    if ArraySize(items) > 0 {
      if ItemID.IsValid(items[0]) {
        return items[0];
      };
      return this.EquipBaseFists();
    };
    return ItemID.None();
  }

  private final func GetBaseFistsItemID() -> ItemID {
    return ItemID.CreateQuery(t"Items.w_melee_004__fists_a");
  }

  private final func GetMeleewareOrFistsItemID() -> ItemID {
    let item: ItemID = ItemID.None();
    item = this.GetActiveMeleeWare();
    if ItemID.IsValid(item) {
      return item;
    };
    return this.GetFistsItemID();
  }

  private final const func UpdateActiveWheelItem(itemID: ItemID) -> Void {
    let equipAreaIndex: Int32 = this.GetEquipAreaIndex(gamedataEquipmentArea.WeaponWheel);
    let i: Int32 = 0;
    while i < this.GetNumberOfSlots(gamedataEquipmentArea.WeaponWheel) {
      if this.m_equipment.equipAreas[equipAreaIndex].equipSlots[i].itemID == itemID {
        this.m_equipment.equipAreas[equipAreaIndex].activeIndex = i;
        return;
      };
      i += 1;
    };
    equipAreaIndex = this.GetEquipAreaIndex(gamedataEquipmentArea.QuickWheel);
    i = 0;
    while i < this.GetNumberOfSlots(gamedataEquipmentArea.QuickWheel) {
      if this.m_equipment.equipAreas[equipAreaIndex].equipSlots[i].itemID == itemID {
        this.m_equipment.equipAreas[equipAreaIndex].activeIndex = i;
        return;
      };
      i += 1;
    };
  }

  private final const func UpdateEquipAreaActiveIndex(newCurrentItem: ItemID) -> Void {
    let areaType: gamedataEquipmentArea = EquipmentSystem.GetEquipAreaType(newCurrentItem);
    let areaIndex: Int32 = this.GetEquipAreaIndex(areaType);
    let i: Int32 = 0;
    while i < this.GetNumberOfSlots(areaType) {
      if this.m_equipment.equipAreas[areaIndex].equipSlots[i].itemID == newCurrentItem {
        this.m_equipment.equipAreas[areaIndex].activeIndex = i;
        break;
      };
      i += 1;
    };
    this.UpdateActiveWheelItem(newCurrentItem);
  }

  private final const func UpdateEquipmentUIBB(paperDollEqData: SPaperdollEquipData, opt forceFire: Bool) -> Void {
    let paperdollAreas: array<gamedataEquipmentArea>;
    let equipmentBB: ref<IBlackboard> = GameInstance.GetBlackboardSystem(this.m_owner.GetGame()).Get(GetAllBlackboardDefs().UI_Equipment);
    if IsDefined(equipmentBB) {
      equipmentBB.SetVariant(GetAllBlackboardDefs().UI_Equipment.itemEquipped, ToVariant(paperDollEqData.equipArea.equipSlots[paperDollEqData.slotIndex].itemID), true);
      paperdollAreas = this.GetPaperDollSlots();
      ArrayPush(paperdollAreas, gamedataEquipmentArea.Weapon);
      if ArrayContains(paperdollAreas, paperDollEqData.equipArea.areaType) {
        equipmentBB.SetVariant(GetAllBlackboardDefs().UI_Equipment.lastModifiedArea, ToVariant(paperDollEqData), forceFire);
        equipmentBB.FireCallbacks();
      };
    };
  }

  private final const func UpdateUIBBAreaChanged(equipmentArea: gamedataEquipmentArea, slotIndex: Int32) -> Void {
    let equipmentBB: ref<IBlackboard> = GameInstance.GetBlackboardSystem(this.m_owner.GetGame()).Get(GetAllBlackboardDefs().UI_Equipment);
    if IsDefined(equipmentBB) {
      equipmentBB.SetInt(GetAllBlackboardDefs().UI_Equipment.areaChangedSlotIndex, slotIndex);
      equipmentBB.SetInt(GetAllBlackboardDefs().UI_Equipment.areaChanged, EnumInt(equipmentArea), true);
    };
  }

  public final const func GetPaperDollEquipAreas() -> [SEquipArea] {
    let areas: array<SEquipArea>;
    let slots: array<gamedataEquipmentArea> = this.GetPaperDollSlots();
    let i: Int32 = 0;
    while i < ArraySize(slots) {
      ArrayPush(areas, this.GetEquipArea(slots[i]));
      i += 1;
    };
    return areas;
  }

  public final const func GetPaperDollItems() -> [ItemID] {
    let item: ItemID;
    let items: array<ItemID>;
    let slots: array<gamedataEquipmentArea> = this.GetPaperDollSlots();
    let i: Int32 = 0;
    while i < ArraySize(slots) {
      item = this.GetActiveItem(slots[i]);
      if ItemID.IsValid(item) {
        ArrayPush(items, item);
      };
      i += 1;
    };
    return items;
  }

  public final const func GetPaperDollSlots() -> [gamedataEquipmentArea] {
    let slots: array<gamedataEquipmentArea>;
    ArrayPush(slots, gamedataEquipmentArea.Outfit);
    ArrayPush(slots, gamedataEquipmentArea.OuterChest);
    ArrayPush(slots, gamedataEquipmentArea.InnerChest);
    ArrayPush(slots, gamedataEquipmentArea.Head);
    ArrayPush(slots, gamedataEquipmentArea.Face);
    ArrayPush(slots, gamedataEquipmentArea.Legs);
    ArrayPush(slots, gamedataEquipmentArea.Feet);
    ArrayPush(slots, gamedataEquipmentArea.HandsCW);
    ArrayPush(slots, gamedataEquipmentArea.RightArm);
    if this.IsBuildCensored() {
      ArrayPush(slots, gamedataEquipmentArea.UnderwearTop);
    };
    if !this.ShouldShowGenitals() || this.IsBuildCensored() {
      ArrayPush(slots, gamedataEquipmentArea.UnderwearBottom);
    };
    return slots;
  }

  public final const func ShouldShowGenitals() -> Bool {
    let charCustomization: ref<gameuiICharacterCustomizationState> = GameInstance.GetCharacterCustomizationSystem(this.m_owner.GetGame()).GetState();
    if charCustomization != null {
      return charCustomization.HasOption(n"genitals", n"genitals_01", false) || charCustomization.HasOption(n"genitals", n"genitals_02", false) || charCustomization.HasOption(n"genitals", n"genitals_03", false);
    };
    return false;
  }

  public final const func IsBuildCensored() -> Bool {
    let charCustomization: ref<gameuiICharacterCustomizationSystem> = GameInstance.GetCharacterCustomizationSystem(this.m_owner.GetGame());
    if charCustomization != null {
      return !charCustomization.IsNudityAllowed();
    };
    return false;
  }

  public final func OnEquipRequest(request: ref<EquipRequest>) -> Void {
    this.ProcessEquipRequest(request.owner, request.slotIndex, request.addToInventory, request.itemID, request.equipToCurrentActiveSlot, false, true);
  }

  public final func OnGameplayEquipRequest(request: ref<GameplayEquipRequest>) -> Void {
    let equipProgramsRequest: ref<GameplayEquipProgramsRequest>;
    this.ProcessEquipRequest(request.owner, request.slotIndex, request.addToInventory, request.itemID, request.equipToCurrentActiveSlot, request.blockUpdateWeaponActiveSlots, request.forceEquipWeapon, request.extraQuality);
    if ArraySize(request.partsToAdd) > 0 {
      equipProgramsRequest = new GameplayEquipProgramsRequest();
      equipProgramsRequest.owner = request.owner;
      equipProgramsRequest.programIDs = request.partsToAdd;
      this.OnGameplayEquipProgramsRequest(equipProgramsRequest);
    };
  }

  public final func OnGameplayEquipProgramsRequest(request: ref<GameplayEquipProgramsRequest>) -> Void {
    let availableSlots: array<TweakDBID>;
    let i: Int32;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(request.owner.GetGame());
    let cyberdeckID: ItemID = EquipmentSystem.GetData(request.owner).GetActiveItem(gamedataEquipmentArea.SystemReplacementCW);
    if !EquipmentSystem.IsItemCyberdeck(cyberdeckID) {
      return;
    };
    transactionSystem.GetAvailableSlotsOnItem(request.owner, cyberdeckID, availableSlots);
    i = 0;
    while i < ArraySize(availableSlots) && i < ArraySize(request.programIDs) {
      transactionSystem.AddPart(request.owner, cyberdeckID, request.programIDs[i], availableSlots[i]);
      this.ApplyEquipGLPs(request.programIDs[i]);
      i += 1;
    };
  }

  private final func ProcessEquipRequest(owner: wref<GameObject>, slotIndex: Int32, addToInventory: Bool, itemID: ItemID, equipToCurrentActiveSlot: Bool, opt blockUpdateWeaponActiveSlots: Bool, opt forceEquipWeapon: Bool, opt extraQuality: Float) -> Void {
    let modifier: ref<gameConstantStatModifierData>;
    let statsSystem: ref<StatsSystem>;
    if IsMultiplayer() && equipToCurrentActiveSlot {
      slotIndex = this.m_equipment.equipAreas[this.GetEquipAreaIndex(EquipmentSystem.GetEquipAreaType(itemID))].activeIndex;
    };
    if addToInventory {
      if !GameInstance.GetTransactionSystem(owner.GetGame()).HasItem(owner, itemID) {
        itemID = ItemID.FromTDBID(ItemID.GetTDBID(itemID));
        GameInstance.GetTransactionSystem(owner.GetGame()).GiveItem(owner, itemID, 1, RPGManager.GetItemRecord(itemID).Tags());
      } else {
        itemID = GameInstance.GetTransactionSystem(owner.GetGame()).GetItemData(owner, itemID).GetID();
      };
    };
    if extraQuality > 0.00 {
      statsSystem = GameInstance.GetStatsSystem(owner.GetGame());
      modifier = new gameConstantStatModifierData();
      modifier.modifierType = gameStatModifierType.Additive;
      modifier.statType = gamedataStatType.Quality;
      modifier.value = extraQuality;
      statsSystem.AddSavedModifier(Cast<StatsObjectID>(itemID), modifier);
    };
    if slotIndex == -1 {
      this.EquipItem(itemID, blockUpdateWeaponActiveSlots, forceEquipWeapon);
    } else {
      this.EquipItem(itemID, slotIndex, blockUpdateWeaponActiveSlots, forceEquipWeapon);
    };
  }

  public final func OnEquipVisualsRequest(request: ref<EquipVisualsRequest>) -> Void {
    this.EquipVisuals(request.itemID);
  }

  public final func OnUnequipVisualsRequest(request: ref<UnequipVisualsRequest>) -> Void {
    this.UnequipVisuals(request.area);
  }

  public final func OnAssignToCyberwareWheelRequest(request: ref<AssignToCyberwareWheelRequest>) -> Void {
    let cyberwareBB: ref<IBlackboard>;
    if ArraySize(this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.CyberwareWheel)].equipSlots) <= request.slotIndex {
      return;
    };
    this.m_equipment.equipAreas[this.GetEquipAreaIndex(gamedataEquipmentArea.CyberwareWheel)].equipSlots[request.slotIndex].itemID = request.itemID;
    this.UpdateQuickWheel();
    cyberwareBB = GameInstance.GetBlackboardSystem(this.m_owner.GetGame()).Get(GetAllBlackboardDefs().UI_QuickSlotsData);
    cyberwareBB.SetBool(GetAllBlackboardDefs().UI_QuickSlotsData.CyberwareAssignmentComplete, true, true);
  }

  public final func OnUnequipRequest(request: ref<UnequipRequest>) -> Void {
    this.UnequipItem(this.GetEquipAreaIndex(request.areaType), request.slotIndex, request.force);
  }

  public final func OnUnequipItemsRequest(request: ref<UnequipItemsRequest>) -> Void {
    let hotkey: EHotkey;
    let itemID: ItemID;
    let i: Int32 = 0;
    while i < ArraySize(request.items) {
      itemID = request.items[i];
      this.UnequipItem(itemID);
      hotkey = this.GetHotkeyTypeFromItemID(itemID);
      if NotEquals(hotkey, EHotkey.INVALID) {
        if this.AssignNextValidItemToHotkey(itemID) {
          return;
        };
        this.AssignItemToHotkey(ItemID.None(), hotkey);
      };
      i += 1;
    };
  }

  public final func OnUnequipByTDBIDRequest(request: ref<UnequipByTDBIDRequest>) -> Void {
    let eqRequest: ref<EquipmentSystemWeaponManipulationRequest>;
    let itemQuery: ItemID = ItemID.CreateQuery(request.itemTDBID);
    if Equals(RPGManager.GetItemCategory(itemQuery), gamedataItemCategory.Weapon) {
      eqRequest = new EquipmentSystemWeaponManipulationRequest();
      eqRequest.requestType = EquipmentManipulationAction.UnequipWeapon;
      this.OnEquipmentSystemWeaponManipulationRequest(eqRequest);
    } else {
      this.UnequipItem(itemQuery);
    };
  }

  public final func OnInstallCyberwareRequest(request: ref<InstallCyberwareRequest>) -> Void {
    if request.slotIndex == -1 {
      this.EquipItem(request.itemID, false);
    } else {
      this.EquipItem(request.itemID, request.slotIndex, false);
    };
  }

  public final func OnUninstallCyberwareRequest(request: ref<UninstallCyberwareRequest>) -> Void {
    this.UnequipItem(this.GetEquipAreaIndex(request.areaType), request.slotIndex);
  }

  public final func OnReplaceEquipmentRequest(request: ref<ReplaceEquipmentRequest>) -> Void {
    let availableSlots: array<TweakDBID>;
    let equipArea: gamedataEquipmentArea;
    let equipAreaIndex: Int32;
    let i: Int32;
    let itemID: ItemID;
    let itemModParams: ItemModParams;
    let itemModParamsArray: array<ItemModParams>;
    let j: Int32;
    let oldItemID: ItemID;
    let parts: array<SPartSlots>;
    let sideUpgradeItem: ref<Item_Record>;
    let transactionSystem: ref<TransactionSystem>;
    let uiEvent: ref<UIEquipmentReplacedEvent>;
    let owner: ref<GameObject> = request.owner;
    if !IsDefined(owner) {
      return;
    };
    if !ItemID.IsValid(request.itemID) {
      return;
    };
    transactionSystem = GameInstance.GetTransactionSystem(owner.GetGame());
    if transactionSystem.HasItem(owner, request.itemID) {
      itemID = request.itemID;
    } else {
      if request.addToInventory {
        if request.rerollIdOnAddToInventory {
          itemID = ItemID.FromTDBID(ItemID.GetTDBID(request.itemID));
        } else {
          itemID = request.itemID;
        };
        itemModParams.itemID = itemID;
        itemModParams.quantity = 1;
        if ItemID.IsValid(request.customPartToGenerateID) {
          ArrayPush(itemModParams.customPartsToInstall, request.customPartToGenerateID);
        };
        ArrayPush(itemModParamsArray, itemModParams);
        transactionSystem.GiveItems(owner, itemModParamsArray);
      } else {
        return;
      };
    };
    equipArea = EquipmentSystem.GetEquipAreaType(itemID);
    equipAreaIndex = this.GetEquipAreaIndex(equipArea);
    oldItemID = this.GetItemInEquipSlot(equipAreaIndex, request.slotIndex);
    if request.transferInstalledParts {
      parts = ItemModificationSystem.GetAllSlots(this.m_owner, oldItemID);
    };
    this.UnequipItem(equipAreaIndex, request.slotIndex);
    if request.slotIndex == -1 {
      this.EquipItem(itemID, false);
    } else {
      this.EquipItem(itemID, request.slotIndex, false);
    };
    if request.transferInstalledParts && ItemID.IsValid(this.GetItemInEquipSlot(equipAreaIndex, request.slotIndex)) {
      transactionSystem.GetAvailableSlotsOnItem(this.m_owner, itemID, availableSlots);
      i = 0;
      j = 0;
      while i < ArraySize(parts) && j < ArraySize(availableSlots) {
        if NotEquals(parts[i].status, ESlotState.Empty) && ArrayContains(availableSlots, parts[i].slotID) {
          transactionSystem.AddPart(this.m_owner, itemID, parts[i].installedPart, parts[i].slotID);
          j += 1;
        };
        i += 1;
      };
    };
    if request.removeOldItem {
      if ItemID.IsValid(oldItemID) && Equals(equipArea, gamedataEquipmentArea.MusculoskeletalSystemCW) && !RPGManager.CyberwareHasSideUpgrade(oldItemID, sideUpgradeItem) {
        oldItemID = this.GetOriginalItemIDFromSideUpgrade(oldItemID);
      };
      transactionSystem.RemoveItem(owner, oldItemID, 1);
    };
    uiEvent = new UIEquipmentReplacedEvent();
    uiEvent.itemID = request.itemID;
    GameInstance.GetUISystem((this.m_owner as PlayerPuppet).GetGame()).QueueEvent(uiEvent);
  }

  public final func OnDrawItemRequest(request: ref<DrawItemRequest>) -> Void {
    this.DrawItem(request.itemID, request.equipAnimationType);
  }

  public final func OnPartInstallRequest(request: ref<PartInstallRequest>) -> Void {
    if this.IsEquipped(request.itemID) {
      this.ApplyEquipGLPs(request.partID);
    };
  }

  public final func OnPartUninstallRequest(request: ref<PartUninstallRequest>) -> Void {
    if this.IsEquipped(request.itemID) {
      this.RemoveEquipGLPs(request.partID);
    };
  }

  public final func OnClearEquipmentRequest(request: ref<ClearEquipmentRequest>) -> Void {
    this.ClearEquipment();
  }

  public final func OnSaveEquipmentSetRequest(request: ref<SaveEquipmentSetRequest>) -> Void {
    this.SaveEquipmentSet(request.setName, request.setType);
  }

  public final func OnLoadEquipmentSetRequest(request: ref<LoadEquipmentSetRequest>) -> Void {
    this.LoadEquipmentSet(request.setName);
  }

  public final func OnDeleteEquipmentSetRequest(request: ref<DeleteEquipmentSetRequest>) -> Void {
    this.DeleteEquipmentSet(request.setName);
  }

  public final func OnEquipmentUIBBRequest(request: ref<EquipmentUIBBRequest>) -> Void {
    let equipData: SPaperdollEquipData;
    this.UpdateEquipmentUIBB(equipData);
  }

  public final func OnCheckRemovedItemWithSlotActiveItem(request: ref<CheckRemovedItemWithSlotActiveItem>) -> Void {
    let activeItems: SSlotActiveItems = this.GetSlotActiveItemStruct();
    if activeItems.rightHandItem == request.itemID || activeItems.leftHandItem == request.itemID {
      this.RemoveItemFromSlotActiveItem(request.itemID);
    };
  }

  public final func OnSynchronizeAttachmentSlotRequest(request: ref<SynchronizeAttachmentSlotRequest>) -> Void {
    let activeItemID: ItemID = GameInstance.GetTransactionSystem(this.m_owner.GetGame()).GetActiveItemInSlot(request.owner, request.slotID);
    if !ItemID.IsValid(activeItemID) {
      return;
    };
    if this.IsEquipped(activeItemID) {
      this.m_equipment.equipAreas[this.GetEquipAreaIndex(EquipmentSystem.GetEquipAreaType(activeItemID))].activeIndex = this.GetSlotIndex(activeItemID);
    };
  }

  public final func OnEquipmentSystemWeaponManipulationRequest(request: ref<EquipmentSystemWeaponManipulationRequest>) -> Void {
    let actions: array<wref<ObjectAction_Record>>;
    let isActivatedCyberware: Bool;
    let requestSlot: EquipmentManipulationRequestSlot;
    let isUnequip: Bool = this.IsEquipmentManipulationAnUnequipRequest(request.requestType);
    let item: ItemID = this.GetItemIDfromEquipmentManipulationAction(request.requestType);
    if !isUnequip {
      if this.GetItemIDFromHotkey(EHotkey.RB) == item {
        if PlayerGameplayRestrictions.IsHotkeyRestricted(this.m_owner.GetGame(), EHotkey.RB) {
          return;
        };
      } else {
        if this.GetItemIDFromHotkey(EHotkey.DPAD_UP) == item {
          if PlayerGameplayRestrictions.IsHotkeyRestricted(this.m_owner.GetGame(), EHotkey.DPAD_UP) {
            return;
          };
        };
      };
    };
    if Equals(request.requestType, EquipmentManipulationAction.UnequipAll) {
      requestSlot = EquipmentManipulationRequestSlot.Both;
    };
    if !ItemID.IsValid(item) && NotEquals(requestSlot, EquipmentManipulationRequestSlot.Both) && NotEquals(request.requestType, EquipmentManipulationAction.UnequipConsumable) && NotEquals(request.requestType, EquipmentManipulationAction.UnequipGadget) {
      return;
    };
    isActivatedCyberware = this.CheckCyberwareItemForActivatedAction(item);
    if isActivatedCyberware && NotEquals(request.requestType, EquipmentManipulationAction.UnequipConsumable) {
      TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item)).ObjectActions(actions);
      ItemActionsHelper.UseItem(this.m_owner, item);
      return;
    };
    if !isUnequip {
      if !this.HasItemInInventory(item) {
        return;
      };
      requestSlot = this.GetRequestSlotFromItemID(item);
      this.SetSlotActiveItem(requestSlot, item);
      this.UpdateEquipAreaActiveIndex(item);
      this.SetLastUsedItem(item);
      this.SendPSMWeaponManipulationRequest(EquipmentManipulationRequestType.Equip, requestSlot, request.equipAnimType);
    } else {
      if NotEquals(requestSlot, EquipmentManipulationRequestSlot.Both) {
        if ItemID.IsValid(item) {
          requestSlot = this.GetRequestSlotFromItemID(item);
        } else {
          if Equals(request.requestType, EquipmentManipulationAction.UnequipConsumable) || Equals(request.requestType, EquipmentManipulationAction.UnequipGadget) {
            requestSlot = EquipmentManipulationRequestSlot.Left;
          };
        };
      };
      this.SetSlotActiveItem(requestSlot, ItemID.None());
      this.SendPSMWeaponManipulationRequest(EquipmentManipulationRequestType.Unequip, requestSlot, request.equipAnimType);
      if request.removeItemFromEquipSlot {
        this.RemoveItemFromEquipSlot(item);
      };
    };
  }

  public final func UnequipPrereqItems() -> Void {
    let itemData: wref<gameItemData>;
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    let items: array<ItemID> = this.GetEquippedClothesAndWeapons();
    let i: Int32 = 0;
    while i < ArraySize(items) {
      itemData = ts.GetItemData(this.m_owner, items[i]);
      if RPGManager.IsItemCrafted(itemData) && !this.IsEquippable(itemData) {
        this.UnequipItem(items[i]);
      };
      i += 1;
    };
  }

  private final func GetEquippedClothesAndWeapons() -> [ItemID] {
    let equipArea: SEquipArea;
    let itemList: array<ItemID>;
    let j: Int32;
    let i: Int32 = 0;
    while i < ArraySize(this.m_equipment.equipAreas) {
      equipArea = this.m_equipment.equipAreas[i];
      if Equals(equipArea.areaType, gamedataEquipmentArea.Face) || Equals(equipArea.areaType, gamedataEquipmentArea.Head) || Equals(equipArea.areaType, gamedataEquipmentArea.InnerChest) || Equals(equipArea.areaType, gamedataEquipmentArea.OuterChest) || Equals(equipArea.areaType, gamedataEquipmentArea.Legs) || Equals(equipArea.areaType, gamedataEquipmentArea.Feet) || Equals(equipArea.areaType, gamedataEquipmentArea.Weapon) {
        j = 0;
        while j < ArraySize(equipArea.equipSlots) {
          if ItemID.IsValid(equipArea.equipSlots[j].itemID) {
            ArrayPush(itemList, equipArea.equipSlots[j].itemID);
          };
          j += 1;
        };
      };
      i += 1;
    };
    return itemList;
  }

  public final func OnClearAllWeaponSlotsRequest(request: ref<ClearAllWeaponSlotsRequest>) -> Void {
    this.ClearAllWeaponSlots();
  }

  public final func CreateUnequipWeaponManipulationRequest() -> Void {
    let request: ref<EquipmentSystemWeaponManipulationRequest> = new EquipmentSystemWeaponManipulationRequest();
    request.requestType = EquipmentManipulationAction.UnequipWeapon;
    this.OnEquipmentSystemWeaponManipulationRequest(request);
  }

  private final func CreateUnequipGadgetWeaponManipulationRequest() -> Void {
    let request: ref<EquipmentSystemWeaponManipulationRequest> = new EquipmentSystemWeaponManipulationRequest();
    request.requestType = EquipmentManipulationAction.UnequipGadget;
    request.removeItemFromEquipSlot = true;
    this.OnEquipmentSystemWeaponManipulationRequest(request);
  }

  private final func CreateUnequipConsumableWeaponManipulationRequest() -> Void {
    let request: ref<EquipmentSystemWeaponManipulationRequest> = new EquipmentSystemWeaponManipulationRequest();
    request.requestType = EquipmentManipulationAction.UnequipConsumable;
    request.removeItemFromEquipSlot = true;
    this.OnEquipmentSystemWeaponManipulationRequest(request);
  }

  public final func IsEquipmentManipulationAnUnequipRequest(eqManipulationAction: EquipmentManipulationAction) -> Bool {
    return Equals(eqManipulationAction, EquipmentManipulationAction.UnequipWeapon) || Equals(eqManipulationAction, EquipmentManipulationAction.UnequipConsumable) || Equals(eqManipulationAction, EquipmentManipulationAction.UnequipGadget) || Equals(eqManipulationAction, EquipmentManipulationAction.UnequipLeftHandCyberware) || Equals(eqManipulationAction, EquipmentManipulationAction.UnequipAll);
  }

  public final func GetRequestSlotFromEquipmentManipulationAction(eqManipulationAction: EquipmentManipulationAction) -> EquipmentManipulationRequestSlot {
    if Equals(eqManipulationAction, EquipmentManipulationAction.Undefined) {
      return EquipmentManipulationRequestSlot.Undefined;
    };
    if Equals(eqManipulationAction, EquipmentManipulationAction.UnequipConsumable) || Equals(eqManipulationAction, EquipmentManipulationAction.UnequipGadget) || Equals(eqManipulationAction, EquipmentManipulationAction.UnequipLeftHandCyberware) || Equals(eqManipulationAction, EquipmentManipulationAction.RequestGadget) || Equals(eqManipulationAction, EquipmentManipulationAction.RequestLeftHandCyberware) || Equals(eqManipulationAction, EquipmentManipulationAction.RequestConsumable) {
      return EquipmentManipulationRequestSlot.Left;
    };
    if Equals(eqManipulationAction, EquipmentManipulationAction.UnequipAll) {
      return EquipmentManipulationRequestSlot.Both;
    };
    return EquipmentManipulationRequestSlot.Right;
  }

  public final func GetRequestSlotFromItemID(item: ItemID) -> EquipmentManipulationRequestSlot {
    let record: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item));
    switch record.ItemCategory().Type() {
      case gamedataItemCategory.Weapon:
        if Equals(record.ItemType().Type(), gamedataItemType.Cyb_Launcher) {
          return EquipmentManipulationRequestSlot.Left;
        };
        return EquipmentManipulationRequestSlot.Right;
      case gamedataItemCategory.Consumable:
        return EquipmentManipulationRequestSlot.Left;
      case gamedataItemCategory.Gadget:
        return EquipmentManipulationRequestSlot.Left;
      case gamedataItemCategory.Cyberware:
        return EquipmentManipulationRequestSlot.Left;
      default:
        return EquipmentManipulationRequestSlot.Undefined;
    };
  }

  public final func GetItemIDfromEquipmentManipulationAction(eqManipulationAction: EquipmentManipulationAction) -> ItemID {
    switch eqManipulationAction {
      case EquipmentManipulationAction.RequestActiveMeleeware:
        return this.GetActiveMeleeWare();
      case EquipmentManipulationAction.RequestSlotActiveWeapon:
        return this.GetSlotActiveWeapon();
      case EquipmentManipulationAction.RequestActiveWeapon:
        return this.GetActiveWeapon();
      case EquipmentManipulationAction.RequestLastUsedWeapon:
        return this.GetLastUsedWeaponItemID();
      case EquipmentManipulationAction.RequestFirstMeleeWeapon:
        return this.GetFirstMeleeWeaponItemID();
      case EquipmentManipulationAction.RequestLastUsedMeleeWeapon:
        return this.GetLastUsedMeleeWeaponItemID();
      case EquipmentManipulationAction.RequestLastUsedOrFirstAvailableWeapon:
        return this.GetLastUsedOrFirstAvailableWeapon();
      case EquipmentManipulationAction.RequestLastUsedOrFirstAvailableRangedWeapon:
        return this.GetLastUsedOrFirstAvailableRangedWeapon();
      case EquipmentManipulationAction.RequestLastUsedOrFirstAvailableMeleeWeapon:
        return this.GetLastUsedOrFirstAvailableMeleeWeapon();
      case EquipmentManipulationAction.RequestLastUsedOrFirstAvailableOneHandedRangedWeapon:
        return this.GetLastUsedOrFirstAvailableOneHandedRangedWeapon();
      case EquipmentManipulationAction.RequestLastUsedOrFirstAvailableDriverCombatRangedWeapon:
        return this.GetLastUsedOrFirstAvailableDriverCombatWeapon(n"DriverCombatRangedWeapon");
      case EquipmentManipulationAction.RequestLastUsedOrFirstAvailableDriverCombatBikeWeapon:
        return this.GetLastUsedOrFirstAvailableDriverCombatWeapon(n"DriverCombatBikeWeapon");
      case EquipmentManipulationAction.RequestHeavyWeapon:
        return this.GetActiveHeavyWeapon();
      case EquipmentManipulationAction.RequestConsumable:
        return this.GetActiveConsumable();
      case EquipmentManipulationAction.RequestGadget:
        return this.GetActiveGadget();
      case EquipmentManipulationAction.RequestLeftHandCyberware:
        return this.GetActiveCyberware();
      case EquipmentManipulationAction.RequestFists:
        return this.GetFistsItemID();
      case EquipmentManipulationAction.UnequipGadget:
        return this.GetActiveGadget();
      case EquipmentManipulationAction.UnequipLeftHandCyberware:
        return this.GetActiveCyberware();
      case EquipmentManipulationAction.UnequipConsumable:
        return this.GetActiveConsumable();
      case EquipmentManipulationAction.UnequipWeapon:
        return this.GetActiveWeaponToUnequip();
      case EquipmentManipulationAction.CycleWeaponWheelItem:
        return this.GetNextWeaponWheelItem();
      case EquipmentManipulationAction.CycleNextWeaponWheelItem:
        return this.CycleWeapon(true, false);
      case EquipmentManipulationAction.CyclePreviousWeaponWheelItem:
        return this.CycleWeapon(false, false);
      case EquipmentManipulationAction.ReequipWeapon:
        return this.GetLastUsedWeaponItemID();
      case EquipmentManipulationAction.RequestWeaponSlot1:
        return this.GetWeaponSlotItem(1);
      case EquipmentManipulationAction.RequestWeaponSlot2:
        return this.GetWeaponSlotItem(2);
      case EquipmentManipulationAction.RequestWeaponSlot3:
        return this.GetWeaponSlotItem(3);
      case EquipmentManipulationAction.RequestWeaponSlot4:
        return this.GetWeaponSlotItem(4);
      case EquipmentManipulationAction.RequestNextThrowableWeapon:
        return this.GetNextThrowableWeapon();
      default:
        return ItemID.None();
    };
  }

  public final func CheckCyberwareItemForActivatedAction(item: ItemID) -> Bool {
    let actions: array<wref<ObjectAction_Record>>;
    let record: wref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item));
    if IsDefined(record) && (NotEquals(record.ItemCategory().Type(), gamedataItemCategory.Cyberware) || Equals(record.ItemType().Type(), gamedataItemType.Cyb_Launcher)) {
      return false;
    };
    if IsDefined(record) {
      record.ObjectActions(actions);
    };
    return ArraySize(actions) > 0;
  }

  public final func OnSetActiveItemInEquipmentArea(request: ref<SetActiveItemInEquipmentArea>) -> Void {
    let slotIndex: Int32 = this.GetSlotIndex(request.itemID);
    let equipAreaIndex: Int32 = this.GetEquipAreaIndex(EquipmentSystem.GetEquipAreaTypeForDpad(request.itemID));
    let equipArea: SEquipArea = this.m_equipment.equipAreas[equipAreaIndex];
    if slotIndex >= 0 && slotIndex < ArraySize(equipArea.equipSlots) {
      this.m_equipment.equipAreas[equipAreaIndex].activeIndex = slotIndex;
      this.UpdateActiveWheelItem(this.GetItemInEquipSlot(equipAreaIndex, slotIndex));
    };
  }

  public final func CheckCyberjunkieAchievement() -> Void {
    let achievementRequest: ref<AddAchievementRequest>;
    let progress: Int32;
    let progressionRequest: ref<SetAchievementProgressRequest>;
    let dataTrackingSystem: ref<DataTrackingSystem> = GameInstance.GetScriptableSystemsContainer(this.m_owner.GetGame()).Get(n"DataTrackingSystem") as DataTrackingSystem;
    let achievement: gamedataAchievement = gamedataAchievement.Cyberjunkie;
    let equipmentAreas: array<gamedataEquipmentArea> = this.GetAllCyberwareEquipmentAreas();
    let i: Int32 = 0;
    while i < ArraySize(equipmentAreas) {
      if this.GetNumberOfItemsInEquipmentArea(equipmentAreas[i]) > 0 {
        progress += 1;
      };
      i += 1;
    };
    progressionRequest = new SetAchievementProgressRequest();
    progressionRequest.achievement = achievement;
    progressionRequest.currentValue = progress;
    dataTrackingSystem.QueueRequest(progressionRequest);
    i = 0;
    while i < ArraySize(equipmentAreas) {
      if this.GetNumberOfItemsInEquipmentArea(equipmentAreas[i]) == 0 {
        return;
      };
      i += 1;
    };
    achievementRequest = new AddAchievementRequest();
    achievementRequest.achievement = achievement;
    dataTrackingSystem.QueueRequest(achievementRequest);
  }

  public final func GetInventoryManager() -> wref<InventoryDataManagerV2> {
    return this.m_inventoryManager;
  }
}

public class EquipmentSystem extends IEquipmentSystem {

  private persistent let m_ownerData: [ref<EquipmentSystemPlayerData>];

  public final static func GetInstance(owner: ref<GameObject>) -> ref<EquipmentSystem> {
    let equipmentSystem: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(owner.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    return equipmentSystem;
  }

  private final const func GetHairSuffix(itemId: ItemID, owner: wref<GameObject>, suffixRecord: ref<ItemsFactoryAppearanceSuffixBase_Record>) -> String {
    let customizationState: ref<gameuiICharacterCustomizationState>;
    let characterCustomizationSystem: ref<gameuiICharacterCustomizationSystem> = GameInstance.GetCharacterCustomizationSystem(owner.GetGame());
    if (owner as PlayerPuppet) == null && !characterCustomizationSystem.HasCharacterCustomizationComponent(owner) {
      return "Bald";
    };
    customizationState = characterCustomizationSystem.GetState();
    if customizationState != null {
      if customizationState.HasTag(n"Short") {
        return "Short";
      };
      if customizationState.HasTag(n"Long") {
        return "Long";
      };
      if customizationState.HasTag(n"Dreads") {
        return "Dreads";
      };
      if customizationState.HasTag(n"Buzz") {
        return "Buzz";
      };
      return "Bald";
    };
    return "Error";
  }

  private final func OnPlayerAttach(request: ref<PlayerAttachRequest>) -> Void {
    let data: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    if !IsDefined(data) {
      data = new EquipmentSystemPlayerData();
      data.SetOwner(request.owner as ScriptedPuppet);
      ArrayPush(this.m_ownerData, data);
      data.OnInitialize();
    } else {
      if data.IsClothingVisualsInfoEmpty() {
        data.InitializeClothingOverrideInfo();
      };
      data.EvaluateUnderwearTopHiddenState();
    };
    data.OnAttach();
    if !IsFinal() {
      this.Debug_SetupEquipmentSystemOverlay(request.owner);
    };
  }

  private final func OnPlayerDetach(request: ref<PlayerDetachRequest>) -> Void {
    let i: Int32 = 0;
    while i < ArraySize(this.m_ownerData) {
      if this.m_ownerData[i].GetOwner() == request.owner {
        this.m_ownerData[i].OnDetach();
        ArrayErase(this.m_ownerData, i);
        return;
      };
      i += 1;
    };
  }

  public final const func GetPlayerData(const owner: ref<GameObject>) -> ref<EquipmentSystemPlayerData> {
    let i: Int32 = 0;
    while i < ArraySize(this.m_ownerData) {
      if this.m_ownerData[i].GetOwner() == owner {
        return this.m_ownerData[i];
      };
      i += 1;
    };
    return null;
  }

  private func OnRestored(saveVersion: Int32, gameVersion: Int32) -> Void {
    let factVal: Int32;
    let owner: ref<ScriptedPuppet>;
    let ownerID: EntityID;
    let i: Int32 = 0;
    while i < ArraySize(this.m_ownerData) {
      ownerID = this.m_ownerData[i].GetOwnerID();
      owner = GameInstance.FindEntityByID(this.GetGameInstance(), ownerID) as ScriptedPuppet;
      if IsDefined(owner) {
        this.m_ownerData[i].SetOwner(owner);
        this.m_ownerData[i].OnRestored();
      };
      i += 1;
    };
    factVal = GetFact(this.GetGameInstance(), n"WardrobeInitialized");
    if factVal <= 0 && true {
      this.InitializeWardrobeDatabase();
      SetFactValue(this.GetGameInstance(), n"WardrobeInitialized", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"PowerLevelRescaled2");
    if factVal <= 0 && true {
      this.RefreshItemPlayerScaling();
      SetFactValue(this.GetGameInstance(), n"PowerLevelRescaled2", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"ScalingBlocked");
    if factVal <= 0 && true && saveVersion <= 212 {
      this.BlockAndCompensateScaling();
      SetFactValue(this.GetGameInstance(), n"ScalingBlocked", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"FixerShirtAdded");
    if factVal <= 0 && true {
      if GetFact(this.GetGameInstance(), n"loc_ma_hey_rey_114_finished") >= 1 && GetFact(this.GetGameInstance(), n"loc_ma_hey_rey_110_finished") <= 0 {
        this.GiveFixerShirt();
      };
      SetFactValue(this.GetGameInstance(), n"FixerShirtAdded", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"IconicReworkCompleted");
    if factVal <= 0 && true && saveVersion <= 224 {
      this.IconicsReworkCompensate();
      SetFactValue(this.GetGameInstance(), n"IconicReworkCompleted", 1);
    };
    if gameVersion <= 1650 {
      SetFactValue(this.GetGameInstance(), n"ripperdoc_screen_glitched", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"RetrofixQuickhacks");
    if factVal <= 0 && true && saveVersion <= 250 {
      this.RetrofixQuickhacks();
      SetFactValue(this.GetGameInstance(), n"RetrofixQuickhacks", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"RetrofixCyberwares");
    if factVal <= 0 && true && saveVersion <= 250 {
      this.RetrofixCyberwares();
      SetFactValue(this.GetGameInstance(), n"RetrofixCyberwares", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"ConsumablesUpgraded");
    if factVal <= 0 && true && saveVersion <= 224 {
      this.ConsumablesChargesRework();
      SetFactValue(this.GetGameInstance(), n"ConsumablesUpgraded", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"SaburoTantoAdded");
    if factVal <= 0 && true && saveVersion <= 224 {
      if GetFact(this.GetGameInstance(), n"q005_done") <= 0 {
        this.CheckSaburoDogTagPresence();
      };
      SetFactValue(this.GetGameInstance(), n"SaburoTantoAdded", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"AutoscalingFleshFistsEquipped");
    if factVal <= 0 && true && saveVersion <= 224 {
      this.GiveAndEquipAutoscalingFleshFists();
      SetFactValue(this.GetGameInstance(), n"AutoscalingFleshFistsEquipped", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"IconicsFactsForBlackMarketerAdded");
    if factVal <= 0 && true && saveVersion <= 224 {
      this.ProcessIconicsFactsForBlackMarketer();
      SetFactValue(this.GetGameInstance(), n"IconicsFactsForBlackMarketerAdded", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"LeftHandWeaponsCompensated");
    if factVal <= 0 && true && saveVersion <= 224 {
      this.ReplaceLeftHandVariantWeaponsWithRegular();
      SetFactValue(this.GetGameInstance(), n"LeftHandWeaponsCompensated", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"NonIconicWeaponsRescaled");
    if factVal <= 0 && true && saveVersion <= 254 {
      this.ProcessNonIconicWeaponsRescale();
      SetFactValue(this.GetGameInstance(), n"NonIconicWeaponsRescaled", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"CWQualityRestorationHack");
    if factVal <= 0 && true && saveVersion <= 257 {
      this.RestoreCybWeaponQualities();
      SetFactValue(this.GetGameInstance(), n"CWQualityRestorationHack", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"ReginaRewardCompensated");
    if factVal <= 0 && true && saveVersion <= 257 {
      if GetFact(this.GetGameInstance(), n"wat_sts_counter") >= 23 {
        this.CheckReginaRewardsPresence();
      };
      SetFactValue(this.GetGameInstance(), n"ReginaRewardCompensated", 1);
    };
    if saveVersion < 258 {
      this.RetrofixHolsteredArms();
    };
    factVal = GetFact(this.GetGameInstance(), n"NPCMeleewareRemoved");
    if factVal <= 0 && true && saveVersion <= 257 {
      this.RemoveNPCMeleeware();
      SetFactValue(this.GetGameInstance(), n"NPCMeleewareRemoved", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"Q105KeycardFactsAdded");
    if factVal <= 0 && true && saveVersion <= 257 {
      this.ProcessQ105AccessCardFacts();
      SetFactValue(this.GetGameInstance(), n"Q105KeycardFactsAdded", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"RasetsuRescaledandLocked");
    if factVal <= 0 && true && saveVersion <= 257 {
      this.RasetsuItemPlayerScaling();
      SetFactValue(this.GetGameInstance(), n"RasetsuRescaledandLocked", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"IconicsUpgradeCountWithEffectiveTierUnified");
    if factVal <= 0 && true && saveVersion <= 257 {
      this.IconicsUpgradeCountWithEffectiveTierMatch();
      SetFactValue(this.GetGameInstance(), n"IconicsUpgradeCountWithEffectiveTierUnified", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"KurtMetelFactRetrofixed");
    if factVal <= 0 && true && saveVersion <= 257 {
      SetFactValue(this.GetGameInstance(), n"metel_kurt_owned", 0);
      this.ProcessIconicsFactsForBlackMarketer();
      SetFactValue(this.GetGameInstance(), n"KurtMetelFactRetrofixed", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"MaskCWDupeExploitSecured");
    if factVal <= 0 && true && saveVersion <= 261 {
      this.ProcessMaskCWRestoration();
      SetFactValue(this.GetGameInstance(), n"MaskCWDupeExploitSecured", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"Allow_mq043_reward_retrofix");
    if factVal <= 0 && true && saveVersion <= 261 {
      SetFactValue(this.GetGameInstance(), n"Allow_mq043_reward_retrofix", 1);
    };
    factVal = GetFact(this.GetGameInstance(), n"RetrofixOverallocatedCyberware");
    if factVal <= 0 && true && saveVersion <= 261 {
      this.RetrofixOverallocatedCyberware();
      SetFactValue(this.GetGameInstance(), n"RetrofixOverallocatedCyberware", 1);
    };
  }

  private final func InitializeWardrobeDatabase() -> Void {
    let i: Int32;
    let itemList: array<wref<gameItemData>>;
    let transactionSystem: ref<TransactionSystem>;
    let wardrobeSystem: ref<WardrobeSystem>;
    let player: wref<PlayerPuppet> = GetPlayer(this.GetGameInstance());
    if IsDefined(player) {
      transactionSystem = GameInstance.GetTransactionSystem(this.GetGameInstance());
      transactionSystem.GetItemList(player, itemList);
      wardrobeSystem = GameInstance.GetWardrobeSystem(this.GetGameInstance());
      i = 0;
      while i < ArraySize(itemList) {
        if RPGManager.IsItemClothing(itemList[i].GetID()) {
          wardrobeSystem.StoreUniqueItemIDAndMarkNew(this.GetGameInstance(), itemList[i].GetID());
        };
        i += 1;
      };
    };
  }

  private final func RefreshItemPlayerScaling() -> Void {
    let evt: ref<RefreshItemPlayerScalingEvent>;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    if IsDefined(player) {
      evt = new RefreshItemPlayerScalingEvent();
      player.QueueEvent(evt);
    };
  }

  private final func ProcessNonIconicWeaponsRescale() -> Void {
    let evt: ref<RescaleNonIconicWeaponsEvent>;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    if IsDefined(player) {
      evt = new RescaleNonIconicWeaponsEvent();
      player.QueueEvent(evt);
    };
  }

  private final func RestoreCybWeaponQualities() -> Void {
    let evt: ref<RestoreCybWeaponQualitiesEvent>;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    if IsDefined(player) {
      evt = new RestoreCybWeaponQualitiesEvent();
      player.QueueEvent(evt);
    };
  }

  private final func ConsumablesChargesRework() -> Void {
    let evt: ref<ConsumablesChargesReworkEvent>;
    let player: wref<PlayerPuppet> = GetPlayer(this.GetGameInstance());
    if IsDefined(player) {
      evt = new ConsumablesChargesReworkEvent();
      player.QueueEvent(evt);
    };
  }

  private final func BlockAndCompensateScaling() -> Void {
    let evt: ref<BlockAndCompensateScalingEvent>;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    if IsDefined(player) {
      evt = new BlockAndCompensateScalingEvent();
      player.QueueEvent(evt);
    };
  }

  private final func IconicsReworkCompensate() -> Void {
    let evt: ref<IconicsReworkCompensateEvent>;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    if IsDefined(player) {
      evt = new IconicsReworkCompensateEvent();
      player.QueueEvent(evt);
    };
  }

  private final func GiveFixerShirt() -> Void {
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGameInstance());
    let item: ItemID = ItemID.FromTDBID(t"Items.Fixer_01_Set_TShirt");
    ts.GiveItem(player, item, 1);
  }

  private final func GiveSaburoTanto() -> Void {
    let player: wref<PlayerPuppet> = GetPlayer(this.GetGameInstance());
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGameInstance());
    let item: ItemID = ItemID.FromTDBID(t"Items.Preset_Tanto_Saburo_Retrofix");
    ts.GiveItem(player, item, 1);
  }

  private final func CheckSaburoDogTagPresence() -> Void {
    let TS: ref<TransactionSystem>;
    let i: Int32;
    let itemList: array<wref<gameItemData>>;
    let player: wref<PlayerPuppet> = GetPlayer(this.GetGameInstance());
    if IsDefined(player) {
      TS = GameInstance.GetTransactionSystem(this.GetGameInstance());
      TS.GetItemList(player, itemList);
    };
    i = 0;
    while i < ArraySize(itemList) {
      if itemList[i].HasTag(n"q005_saburo_dogtag") {
        this.GiveSaburoTanto();
      };
      i += 1;
    };
  }

  private final func ProcessIconicsFactsForBlackMarketer() -> Void {
    let TS: ref<TransactionSystem>;
    let i: Int32;
    let itemList: array<wref<gameItemData>>;
    let player: wref<PlayerPuppet> = GetPlayer(this.GetGameInstance());
    if IsDefined(player) {
      TS = GameInstance.GetTransactionSystem(this.GetGameInstance());
      TS.GetItemList(player, itemList);
    };
    i = 0;
    while i < ArraySize(itemList) {
      if itemList[i].HasTag(n"IconicWeapon") {
        RPGManager.ProcessOnLootedPackages(player, itemList[i].GetID());
      };
      i += 1;
    };
  }

  private final func GiveAndEquipAutoscalingFleshFists() -> Void {
    let equipRequest: ref<GameplayEquipRequest>;
    let es: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGameInstance()).Get(n"EquipmentSystem") as EquipmentSystem;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGameInstance());
    let item: ItemID = ItemID.FromTDBID(t"Items.w_melee_004__fists_a");
    ts.GiveItem(player, item, 1);
    equipRequest = new GameplayEquipRequest();
    equipRequest.owner = player;
    equipRequest.itemID = item;
    equipRequest.blockUpdateWeaponActiveSlots = true;
    es.QueueRequest(equipRequest);
  }

  private final func ReplaceLeftHandVariantWeaponsWithRegular() -> Void {
    let TS: ref<TransactionSystem>;
    let i: Int32;
    let itemList: array<wref<gameItemData>>;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    if IsDefined(player) {
      TS = GameInstance.GetTransactionSystem(this.GetGameInstance());
      TS.GetItemList(player, itemList);
    };
    i = 0;
    while i < ArraySize(itemList) {
      if itemList[i].HasTag(n"Left_Hand") {
        RPGManager.ProcessOnLootedPackages(player, itemList[i].GetID());
      };
      i += 1;
    };
  }

  private final func RetrofixQuickhacks() -> Void {
    let evt: ref<RetrofixQuickhacksEvent> = new RetrofixQuickhacksEvent();
    let player: ref<PlayerPuppet> = GetPlayer(this.GetGameInstance());
    if IsDefined(player) {
      player.QueueEvent(evt);
    };
  }

  private final func RetrofixHolsteredArms() -> Void {
    let activeItemID: ItemID;
    let armsItemID: ItemID;
    let player: ref<PlayerPuppet>;
    let slotID: TweakDBID = t"AttachmentSlots.WeaponRight";
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGameInstance());
    let i: Int32 = 0;
    while i < ArraySize(this.m_ownerData) {
      player = this.m_ownerData[i].GetOwner() as PlayerPuppet;
      if !IsDefined(player) {
      } else {
        activeItemID = ts.GetActiveItemInSlot(player, slotID);
        armsItemID = this.m_ownerData[i].GetActiveItem(gamedataEquipmentArea.ArmsCW);
        if ItemID.IsValid(armsItemID) {
          if activeItemID != armsItemID {
            EquipmentSystemPlayerData.UpdateArmSlot(player, armsItemID, true);
          };
        } else {
          armsItemID = this.m_ownerData[i].GetActiveItem(gamedataEquipmentArea.BaseFists);
          if ItemID.IsValid(armsItemID) && activeItemID != armsItemID {
            EquipmentSystemPlayerData.UpdateArmSlot(player, armsItemID, true);
          };
        };
      };
      i += 1;
    };
  }

  private final func RetrofixCyberwares() -> Void {
    let evt: ref<RetrofixCyberwaresEvent> = new RetrofixCyberwaresEvent();
    let player: ref<PlayerPuppet> = GetPlayer(this.GetGameInstance());
    if IsDefined(player) {
      player.QueueEvent(evt);
    };
  }

  private final func RetrofixOverallocatedCyberware() -> Void {
    let evt: ref<RetrofixOverallocatedCyberwareEvent> = new RetrofixOverallocatedCyberwareEvent();
    let player: ref<PlayerPuppet> = GetPlayer(this.GetGameInstance());
    if IsDefined(player) {
      player.QueueEvent(evt);
    };
  }

  private final func CheckReginaRewardsPresence() -> Void {
    let TS: ref<TransactionSystem>;
    let i: Int32;
    let itemList: array<wref<gameItemData>>;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    if IsDefined(player) {
      TS = GameInstance.GetTransactionSystem(this.GetGameInstance());
      TS.GetItemList(player, itemList);
    };
    i = 0;
    while i < ArraySize(itemList) {
      if itemList[i].HasTag(n"AdvancedSubdermalCoProcessor_Regina") {
        RPGManager.ProcessOnLootedPackages(player, itemList[i].GetID());
      };
      i += 1;
    };
    this.RemoveDeprecatedReginaCWReward();
  }

  private final func RemoveDeprecatedReginaCWReward() -> Void {
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGameInstance());
    ts.RemoveItemByTDBID(player, t"Items.NeoFiberLegendary", 1);
  }

  private final func RemoveNPCMeleeware() -> Void {
    let TS: ref<TransactionSystem>;
    let i: Int32;
    let itemData: ItemID;
    let itemList: array<wref<gameItemData>>;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    if IsDefined(player) {
      TS = GameInstance.GetTransactionSystem(this.GetGameInstance());
      TS.GetItemList(player, itemList);
    };
    i = 0;
    while i < ArraySize(itemList) {
      if itemList[i].HasTag(n"NPCMeleeware") {
        itemData = itemList[i].GetID();
        TS.RemoveItemByTDBID(player, ItemID.GetTDBID(itemData), 1);
      };
      i += 1;
    };
  }

  private final func ProcessMaskCWRestoration() -> Void {
    let TS: ref<TransactionSystem>;
    let i: Int32;
    let itemData: ItemID;
    let itemList: array<wref<gameItemData>>;
    let maskPlusCounter: Float;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    if IsDefined(player) {
      TS = GameInstance.GetTransactionSystem(this.GetGameInstance());
      TS.GetItemList(player, itemList);
    };
    if GetFact(this.GetGameInstance(), n"q304_04d_farida_done") >= 1 {
      maskPlusCounter = 0.00;
      i = 0;
      while i < ArraySize(itemList) {
        if itemList[i].HasTag(n"MaskCWPlus") {
          maskPlusCounter += 1.00;
          itemData = itemList[i].GetID();
          TS.RemoveItemByTDBID(player, ItemID.GetTDBID(itemData), 1);
        };
        i += 1;
      };
      if maskPlusCounter >= 1.00 {
        itemData = ItemID.FromTDBID(t"Items.MaskCWPlus");
        TS.GiveItem(player, itemData, 1);
      };
    };
  }

  private final func ProcessQ105AccessCardFacts() -> Void {
    let TS: ref<TransactionSystem>;
    let i: Int32;
    let itemList: array<wref<gameItemData>>;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    if IsDefined(player) {
      TS = GameInstance.GetTransactionSystem(this.GetGameInstance());
      TS.GetItemList(player, itemList);
    };
    i = 0;
    while i < ArraySize(itemList) {
      if itemList[i].HasTag(n"Clouds_VIP") {
        RPGManager.ProcessOnLootedPackages(player, itemList[i].GetID());
      };
      i += 1;
    };
  }

  private final func IconicsUpgradeCountWithEffectiveTierMatch() -> Void {
    let evt: ref<UnifyIconicsUpgradeCountWithEffectiveTierEvent>;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    if IsDefined(player) {
      evt = new UnifyIconicsUpgradeCountWithEffectiveTierEvent();
      player.QueueEvent(evt);
    };
  }

  private final func RasetsuItemPlayerScaling() -> Void {
    let evt: ref<RasetsuToPlayerScalingEvent>;
    let player: wref<PlayerPuppet> = GetMainPlayer(this.GetGameInstance());
    if IsDefined(player) {
      evt = new RasetsuToPlayerScalingEvent();
      player.QueueEvent(evt);
    };
  }

  public final const func PrintEquipment() -> Void {
    let equipmentId: Int32 = 0;
    while equipmentId < ArraySize(this.m_ownerData) {
      equipmentId += 1;
    };
  }

  public final const func GetItemInEquipSlot(owner: ref<GameObject>, equipArea: gamedataEquipmentArea, slotIndex: Int32) -> ItemID {
    return this.GetPlayerData(owner).GetItemInEquipSlot(equipArea, slotIndex);
  }

  public final const func IsEquipped(owner: ref<GameObject>, item: ItemID) -> Bool {
    return this.GetPlayerData(owner).IsEquipped(item);
  }

  public final const func IsEquippable(owner: ref<GameObject>, itemData: wref<gameItemData>) -> Bool {
    return this.GetPlayerData(owner).IsEquippable(itemData);
  }

  public final const func IsEquipped(owner: ref<GameObject>, item: ItemID, equipmentArea: gamedataEquipmentArea) -> Bool {
    return this.GetPlayerData(owner).IsEquipped(item, equipmentArea);
  }

  public final const func GetActiveItem(owner: ref<GameObject>, area: gamedataEquipmentArea) -> ItemID {
    return this.GetPlayerData(owner).GetActiveItem(area);
  }

  public final const func GetActiveVisualItem(owner: ref<GameObject>, area: gamedataEquipmentArea) -> ItemID {
    return this.GetPlayerData(owner).GetVisualItemInSlot(area);
  }

  public final const func GetPaperDollSlots(owner: ref<GameObject>) -> [gamedataEquipmentArea] {
    return this.GetPlayerData(owner).GetPaperDollSlots();
  }

  public final const func GetActiveWeaponObject(owner: ref<GameObject>, area: gamedataEquipmentArea) -> ref<ItemObject> {
    return this.GetPlayerData(owner).GetActiveWeaponObject(area);
  }

  public final const func GetAllInstalledCyberwareAbilities(owner: ref<GameObject>) -> [SEquipSlot] {
    return this.GetPlayerData(owner).GetAllAbilityCyberwareSlots();
  }

  public final static func GetLastUsedItemByType(owner: ref<GameObject>, type: ELastUsed) -> ItemID {
    return EquipmentSystem.GetData(owner).GetLastUsedItemID(type);
  }

  public final const func GetItemSlotIndex(owner: ref<GameObject>, item: ItemID) -> Int32 {
    if !IsDefined(EquipmentSystem.GetData(owner)) {
      return -1;
    };
    return EquipmentSystem.GetData(owner).GetSlotIndex(item);
  }

  public final static func IsCyberdeckEquipped(owner: ref<GameObject>) -> Bool {
    let systemReplacementID: ItemID = EquipmentSystem.GetData(owner).GetActiveItem(gamedataEquipmentArea.SystemReplacementCW);
    return EquipmentSystem.IsItemCyberdeck(systemReplacementID);
  }

  public final static func IsItemCyberdeck(itemID: ItemID) -> Bool {
    let itemRecord: wref<Item_Record> = RPGManager.GetItemRecord(itemID);
    let itemTags: array<CName> = itemRecord.Tags();
    return ArrayContains(itemTags, n"Cyberdeck");
  }

  public final static func GetPlacementSlot(item: ItemID) -> TweakDBID {
    let placementSlots: array<wref<AttachmentSlot_Record>>;
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item));
    if IsDefined(itemRecord) {
      itemRecord.PlacementSlots(placementSlots);
    };
    if ArraySize(placementSlots) > 0 {
      return placementSlots[0].GetID();
    };
    return TDBID.None();
  }

  public final static func GetEquipAreaType(item: ItemID) -> gamedataEquipmentArea {
    let equipAreaRecord: ref<EquipmentArea_Record>;
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item));
    if IsDefined(itemRecord) {
      equipAreaRecord = itemRecord.EquipArea();
      if IsDefined(equipAreaRecord) && TDBID.IsValid(equipAreaRecord.GetID()) {
        return equipAreaRecord.Type();
      };
    };
    return gamedataEquipmentArea.Invalid;
  }

  public final static func GetClothingItemAppearanceName(itemID: ItemID) -> CName {
    let appearance: CName;
    let itemRecord: wref<Item_Record> = RPGManager.GetItemRecord(itemID);
    if IsDefined(itemRecord) {
      appearance = itemRecord.AppearanceName();
    };
    return appearance;
  }

  public final static func GetEquipAreaTypeForDpad(item: ItemID) -> gamedataEquipmentArea {
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item));
    let equipAreaRecord: ref<EquipmentArea_Record> = itemRecord.EquipArea();
    if TDBID.IsValid(equipAreaRecord.GetID()) {
      if Equals(itemRecord.ItemType().Type(), gamedataItemType.Cyb_Launcher) {
        return gamedataEquipmentArea.ArmsCW;
      };
      return equipAreaRecord.Type();
    };
    return gamedataEquipmentArea.Invalid;
  }

  public final const func IsItemInHotkey(const owner: wref<GameObject>, itemID: ItemID) -> Bool {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(owner);
    if IsDefined(playerData) {
      return playerData.IsItemInHotkey(itemID);
    };
    return false;
  }

  public final const func GetHotkeyTypeForItemID(owner: wref<GameObject>, itemID: ItemID) -> EHotkey {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(owner);
    if IsDefined(playerData) {
      return playerData.GetHotkeyTypeForItemID(itemID);
    };
    return EHotkey.INVALID;
  }

  public final const func GetHotkeyTypeFromItemID(owner: wref<GameObject>, itemID: ItemID) -> EHotkey {
    return this.GetPlayerData(owner).GetHotkeyTypeFromItemID(itemID);
  }

  public final const func GetItemIDFromHotkey(owner: wref<GameObject>, hotkey: EHotkey) -> ItemID {
    return this.GetPlayerData(owner).GetItemIDFromHotkey(hotkey);
  }

  public final static func GetData(owner: ref<GameObject>) -> ref<EquipmentSystemPlayerData> {
    return EquipmentSystem.GetInstance(owner).GetPlayerData(owner);
  }

  public final static func GetSlotActiveItem(owner: ref<GameObject>, requestSlot: EquipmentManipulationRequestSlot) -> ItemID {
    let playerData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(owner);
    if IsDefined(playerData) {
      return playerData.GetSlotActiveItem(requestSlot);
    };
    return ItemID.None();
  }

  public final static func GetItemsInArea(owner: ref<GameObject>, area: gamedataEquipmentArea) -> [ItemID] {
    let i: Int32;
    let returnArray: array<ItemID>;
    let equipment: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(owner);
    if IsDefined(equipment) {
      i = 0;
      while i < equipment.GetNumberOfSlots(area) {
        ArrayPush(returnArray, equipment.GetItemInEquipSlot(area, i));
        i += 1;
      };
    };
    return returnArray;
  }

  public final static func GetSlotOverridenItem(owner: ref<GameObject>, area: gamedataEquipmentArea) -> ItemID {
    let equipment: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(owner);
    return equipment.GetSlotOverridenVisualItem(area);
  }

  public final static func HasItemInArea(owner: ref<GameObject>, area: gamedataEquipmentArea) -> Bool {
    let itemsInArea: array<ItemID> = EquipmentSystem.GetItemsInArea(owner, area);
    let i: Int32 = 0;
    while i < ArraySize(itemsInArea) {
      if ItemID.IsValid(itemsInArea[i]) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final static func GetFirstAvailableWeapon(owner: ref<GameObject>) -> ItemID {
    let item: ItemID;
    let playerData: ref<EquipmentSystemPlayerData>;
    let items: array<ItemID> = EquipmentSystem.GetItemsInArea(owner, gamedataEquipmentArea.WeaponWheel);
    let i: Int32 = 0;
    while i < ArraySize(items) {
      if ItemID.IsValid(items[i]) {
        return items[i];
      };
      i += 1;
    };
    playerData = EquipmentSystem.GetData(owner);
    if IsDefined(playerData) {
      item = playerData.GetActiveMeleeWare();
    };
    if ItemID.IsValid(item) {
      return item;
    };
    items = EquipmentSystem.GetItemsInArea(owner, gamedataEquipmentArea.BaseFists);
    if ArraySize(items) > 0 {
      return items[0];
    };
    return ItemID.None();
  }

  public final static func HasTag(item: ref<ItemObject>, tag: CName) -> Bool {
    let tags: array<CName> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item.GetItemID())).Tags();
    return ArrayContains(tags, tag);
  }

  public final static func IsClothing(item: ItemID) -> Bool {
    return EquipmentSystem.IsItemOfCategory(item, gamedataItemCategory.Clothing);
  }

  public final static func IsItemOfCategory(item: ItemID, category: gamedataItemCategory) -> Bool {
    let record: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item));
    if IsDefined(record) && IsDefined(record.ItemCategory()) {
      return Equals(record.ItemCategory().Type(), category);
    };
    return false;
  }

  public final static func GetClothingEquipmentAreas() -> [gamedataEquipmentArea] {
    let slots: array<gamedataEquipmentArea>;
    ArrayPush(slots, gamedataEquipmentArea.OuterChest);
    ArrayPush(slots, gamedataEquipmentArea.InnerChest);
    ArrayPush(slots, gamedataEquipmentArea.Head);
    ArrayPush(slots, gamedataEquipmentArea.Legs);
    ArrayPush(slots, gamedataEquipmentArea.Feet);
    ArrayPush(slots, gamedataEquipmentArea.Face);
    return slots;
  }

  public final const func GetEquipAreaFromItemID(owner: ref<GameObject>, item: ItemID) -> SEquipArea {
    let voidEquipArea: SEquipArea;
    let playerData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(owner);
    if IsDefined(playerData) {
      return playerData.GetEquipAreaFromItemID(item);
    };
    return voidEquipArea;
  }

  public final static func RequestUnequipItem(owner: ref<GameObject>, equipmentArea: gamedataEquipmentArea, slotIndex: Int32) -> Void {
    let unequipRequest: ref<UnequipRequest>;
    let equipmentSystem: ref<EquipmentSystem> = EquipmentSystem.GetInstance(owner);
    let itemInSlot: ItemID = equipmentSystem.GetItemInEquipSlot(owner, equipmentArea, slotIndex);
    if ItemID.IsValid(itemInSlot) {
      unequipRequest = new UnequipRequest();
      unequipRequest.owner = owner;
      unequipRequest.areaType = equipmentArea;
      unequipRequest.slotIndex = slotIndex;
      equipmentSystem.QueueRequest(unequipRequest);
    };
  }

  private final func OnEquipRequest(request: ref<EquipRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnEquipRequest(request);
  }

  private final func OnGameplayEquipRequest(request: ref<GameplayEquipRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnGameplayEquipRequest(request);
  }

  private final func OnGameplayEquipProgramsRequest(request: ref<GameplayEquipProgramsRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnGameplayEquipProgramsRequest(request);
  }

  private final func OnClearAllWeaponSlotsRequest(request: ref<ClearAllWeaponSlotsRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnClearAllWeaponSlotsRequest(request);
  }

  private final func OnEquipVisualsRequest(request: ref<EquipVisualsRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnEquipVisualsRequest(request);
  }

  private final func OnUnequipVisualsRequest(request: ref<UnequipVisualsRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnUnequipVisualsRequest(request);
  }

  private final func OnUnequipRequest(request: ref<UnequipRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnUnequipRequest(request);
  }

  private final func OnUnequipItemsRequest(request: ref<UnequipItemsRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnUnequipItemsRequest(request);
  }

  private final func OnReplaceEquipmentRequest(request: ref<ReplaceEquipmentRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnReplaceEquipmentRequest(request);
  }

  private final func OnHotkeyRefreshRequest(request: ref<HotkeyRefreshRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnHotkeyRefreshRequest(request);
  }

  private final func OnHotkeyAssignmentRequest(request: ref<HotkeyAssignmentRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.Owner());
    playerData.OnHotkeyAssignmentRequest(request);
  }

  private final func OnAssignHotkeyIfEmptySlot(request: ref<AssignHotkeyIfEmptySlot>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.Owner());
    playerData.OnAssignHotkeyIfEmptySlot(request);
  }

  private final func OnInstallCyberwareRequest(request: ref<InstallCyberwareRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnInstallCyberwareRequest(request);
  }

  private final func OnUninstallCyberwareRequest(request: ref<UninstallCyberwareRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnUninstallCyberwareRequest(request);
  }

  private final func OnDrawItemRequest(request: ref<DrawItemRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnDrawItemRequest(request);
  }

  private final func OnPartInstallRequest(request: ref<PartInstallRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnPartInstallRequest(request);
  }

  private final func OnPartUninstallRequest(request: ref<PartUninstallRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnPartUninstallRequest(request);
  }

  private final func OnClearEquipmentRequest(request: ref<ClearEquipmentRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnClearEquipmentRequest(request);
  }

  private final func OnSaveEquipmentSetRequest(request: ref<SaveEquipmentSetRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnSaveEquipmentSetRequest(request);
  }

  private final func OnLoadEquipmentSetRequest(request: ref<LoadEquipmentSetRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnLoadEquipmentSetRequest(request);
  }

  private final func OnDeleteEquipmentSetRequest(request: ref<DeleteEquipmentSetRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnDeleteEquipmentSetRequest(request);
  }

  private final func OnAssignToCyberwareWheelRequest(request: ref<AssignToCyberwareWheelRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnAssignToCyberwareWheelRequest(request);
  }

  private final func OnEquipmentUIBBRequest(request: ref<EquipmentUIBBRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnEquipmentUIBBRequest(request);
  }

  private final func OnCheckRemovedItemWithSlotActiveItem(request: ref<CheckRemovedItemWithSlotActiveItem>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnCheckRemovedItemWithSlotActiveItem(request);
  }

  private final func OnSynchronizeAttachmentSlotRequest(request: ref<SynchronizeAttachmentSlotRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnSynchronizeAttachmentSlotRequest(request);
  }

  private final func OnEquipWardrobeSetRequest(request: ref<EquipWardrobeSetRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.EquipWardrobeSet(request.setID);
  }

  private final func OnUnequipWardrobeSetRequest(request: ref<UnequipWardrobeSetRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.UnequipWardrobeSet();
  }

  private final func OnDeleteWardrobeSetRequest(request: ref<DeleteWardrobeSetRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.DeleteWardrobeSet(request.setID);
  }

  private final func OnQuestRestoreWardrobeSetRequest(request: ref<QuestRestoreWardrobeSetRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnQuestRestoreWardrobeSetRequest(request);
  }

  private final func OnQuestDisableWardrobeSetRequest(request: ref<QuestDisableWardrobeSetRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnQuestDisableWardrobeSetRequest(request);
  }

  private final func OnQuestEnableWardrobeSetRequest(request: ref<QuestEnableWardrobeSetRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnQuestEnableWardrobeSetRequest(request);
  }

  private final func OnQuestHideSlotRequest(request: ref<QuestHideSlotRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.QuestHideSlot(request.slot);
  }

  private final func OnQuestRestoreSlotRequest(request: ref<QuestRestoreSlotRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.QuestRestoreSlot(request.slot);
  }

  public final static func GetActiveWardrobeSetID(owner: ref<GameObject>) -> gameWardrobeClothingSetIndex {
    return GameInstance.GetWardrobeSystem(owner.GetGame()).GetActiveClothingSetIndex();
  }

  public final static func GetActiveWardrobeSet(owner: ref<GameObject>) -> ref<ClothingSet> {
    let playerData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(owner);
    return playerData.GetActiveWardrobeSet();
  }

  public final static func UnequipPrereqItems(owner: ref<GameObject>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(owner);
    playerData.UnequipPrereqItems();
  }

  public final const func EquipCyberwareByTDBID(player: ref<PlayerPuppet>, tdbid: TweakDBID) -> Void {
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(player.GetGame());
    let equipRequest: ref<EquipRequest> = new EquipRequest();
    let itemData: ref<gameItemData> = ts.GetItemDataByTDBID(player, tdbid);
    if ItemID.IsValid(itemData.GetID()) {
      equipRequest.owner = player;
      equipRequest.itemID = itemData.GetID();
      this.QueueRequest(equipRequest);
    };
  }

  private final func EquipTutorialCyberware(player: ref<PlayerPuppet>) -> Void {
    let requiredQuality: gamedataQuality = RPGManager.ConvertPlayerLevelToCyberwareQuality(GameInstance.GetStatsSystem(player.GetGame()).GetStatValue(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.Level), false);
    this.EquipCyberwareByTDBID(player, RipperDocGameController.GetAppropriateEyesTutorialCyberware(requiredQuality));
    this.EquipCyberwareByTDBID(player, RipperDocGameController.GetAppropriateHandsTutorialCyberware(requiredQuality, false));
  }

  private final func OnDrawItemByContextRequest(request: ref<DrawItemByContextRequest>) -> Void {
    let eqRequest: ref<EquipmentSystemWeaponManipulationRequest> = new EquipmentSystemWeaponManipulationRequest();
    let player: ref<PlayerPuppet> = request.owner as PlayerPuppet;
    let equipData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(request.owner);
    switch request.itemEquipContext {
      case gameItemEquipContexts.LastWeaponEquipped:
        eqRequest.requestType = EquipmentManipulationAction.RequestLastUsedWeapon;
        break;
      case gameItemEquipContexts.LastUsedMeleeWeapon:
        eqRequest.requestType = EquipmentManipulationAction.RequestLastUsedOrFirstAvailableMeleeWeapon;
        break;
      case gameItemEquipContexts.LastUsedRangedWeapon:
        eqRequest.requestType = EquipmentManipulationAction.RequestLastUsedOrFirstAvailableRangedWeapon;
        break;
      case gameItemEquipContexts.Gadget:
        eqRequest.requestType = EquipmentManipulationAction.RequestGadget;
        break;
      case gameItemEquipContexts.MeleeCyberware:
        eqRequest.requestType = EquipmentManipulationAction.RequestActiveMeleeware;
        break;
      case gameItemEquipContexts.LauncherCyberware:
        break;
      case gameItemEquipContexts.Fists:
        eqRequest.requestType = EquipmentManipulationAction.RequestFists;
        break;
      case gameItemEquipContexts.TutorialCyberware:
        if IsDefined(player) {
          this.EquipTutorialCyberware(player);
        };
        return;
    };
    eqRequest.equipAnimType = request.equipAnimationType;
    equipData.OnEquipmentSystemWeaponManipulationRequest(eqRequest);
  }

  private final func OnUnequipByTDBIDRequest(request: ref<UnequipByTDBIDRequest>) -> Void {
    let equipData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(request.owner);
    equipData.OnUnequipByTDBIDRequest(request);
  }

  private final func OnUnequipByContextRequest(request: ref<UnequipByContextRequest>) -> Void {
    let clearRequest: ref<ClearEquipmentRequest>;
    let unequipItemsRequest: ref<UnequipItemsRequest>;
    let unequipVisualsRequest: ref<UnequipWardrobeSetRequest>;
    let eqRequest: ref<EquipmentSystemWeaponManipulationRequest> = new EquipmentSystemWeaponManipulationRequest();
    let equipData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(request.owner);
    let unequipRequest: ref<UnequipRequest> = new UnequipRequest();
    unequipRequest.slotIndex = 0;
    switch request.itemUnequipContext {
      case gameItemUnequipContexts.AllItems:
        clearRequest = new ClearEquipmentRequest();
        equipData.OnClearEquipmentRequest(clearRequest);
        break;
      case gameItemUnequipContexts.HeadClothing:
        unequipRequest.areaType = gamedataEquipmentArea.Head;
        equipData.OnUnequipRequest(unequipRequest);
        break;
      case gameItemUnequipContexts.FaceClothing:
        unequipRequest.areaType = gamedataEquipmentArea.Face;
        equipData.OnUnequipRequest(unequipRequest);
        break;
      case gameItemUnequipContexts.OuterChestClothing:
        unequipRequest.areaType = gamedataEquipmentArea.OuterChest;
        equipData.OnUnequipRequest(unequipRequest);
        break;
      case gameItemUnequipContexts.InnerChestClothing:
        unequipRequest.areaType = gamedataEquipmentArea.InnerChest;
        equipData.OnUnequipRequest(unequipRequest);
        break;
      case gameItemUnequipContexts.LegClothing:
        unequipRequest.areaType = gamedataEquipmentArea.Legs;
        equipData.OnUnequipRequest(unequipRequest);
        break;
      case gameItemUnequipContexts.FootClothing:
        unequipRequest.areaType = gamedataEquipmentArea.Feet;
        equipData.OnUnequipRequest(unequipRequest);
        break;
      case gameItemUnequipContexts.AllClothing:
        unequipRequest.areaType = gamedataEquipmentArea.Head;
        equipData.OnUnequipRequest(unequipRequest);
        unequipRequest.areaType = gamedataEquipmentArea.Face;
        equipData.OnUnequipRequest(unequipRequest);
        unequipRequest.areaType = gamedataEquipmentArea.OuterChest;
        equipData.OnUnequipRequest(unequipRequest);
        unequipRequest.areaType = gamedataEquipmentArea.InnerChest;
        equipData.OnUnequipRequest(unequipRequest);
        unequipRequest.areaType = gamedataEquipmentArea.Legs;
        equipData.OnUnequipRequest(unequipRequest);
        unequipRequest.areaType = gamedataEquipmentArea.Feet;
        equipData.OnUnequipRequest(unequipRequest);
        unequipRequest.areaType = gamedataEquipmentArea.Outfit;
        equipData.OnUnequipRequest(unequipRequest);
        unequipVisualsRequest = new UnequipWardrobeSetRequest();
        unequipVisualsRequest.owner = request.owner;
        this.OnUnequipWardrobeSetRequest(unequipVisualsRequest);
        break;
      case gameItemUnequipContexts.RightHandWeapon:
        eqRequest.requestType = EquipmentManipulationAction.UnequipWeapon;
        equipData.OnEquipmentSystemWeaponManipulationRequest(eqRequest);
        break;
      case gameItemUnequipContexts.LeftHandWeapon:
        eqRequest.requestType = EquipmentManipulationAction.UnequipConsumable;
        equipData.OnEquipmentSystemWeaponManipulationRequest(eqRequest);
        break;
      case gameItemUnequipContexts.AllWeapons:
        eqRequest.requestType = EquipmentManipulationAction.UnequipAll;
        equipData.OnEquipmentSystemWeaponManipulationRequest(eqRequest);
        break;
      case gameItemUnequipContexts.AllQuestItems:
        unequipItemsRequest = new UnequipItemsRequest();
        unequipItemsRequest.items = equipData.GetEquippedQuestItems();
        equipData.OnUnequipItemsRequest(unequipItemsRequest);
    };
  }

  private final func Debug_SetupEquipmentSystemOverlay(dataOwner: wref<GameObject>) -> Void {
    let areas: array<SEquipArea>;
    let i: Int32;
    let loadout: SLoadout;
    let data: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(dataOwner);
    let sink: SDOSink = GameInstance.GetScriptsDebugOverlaySystem(this.GetGameInstance()).CreateSink();
    SDOSink.SetRoot(sink, "Equipment");
    loadout = data.GetEquipment();
    areas = loadout.equipAreas;
    i = 0;
    while i < ArraySize(areas) {
      this.Debug_SetupESAreaButton(areas[i], dataOwner);
      i += 1;
    };
  }

  public final static func ComposeSDORootPath(ownerGameObject: wref<GameObject>, opt suffix: String) -> String {
    let path: String = "Equipment/[Player: " + ToString(ownerGameObject.GetControllingPeerID()) + "]";
    if StrLen(suffix) > 0 {
      path = path + "/" + suffix;
    };
    return path;
  }

  public final const func Debug_SetupESAreaButton(const equipArea: script_ref<SEquipArea>, ownerGameObject: wref<GameObject>) -> Void {
    let area: String;
    let i: Int32;
    let sink: SDOSink = GameInstance.GetScriptsDebugOverlaySystem(this.GetGameInstance()).CreateSink();
    SDOSink.SetRoot(sink, EquipmentSystem.ComposeSDORootPath(ownerGameObject));
    area = EnumValueToString("gamedataEquipmentArea", Cast<Int64>(EnumInt(Deref(equipArea).areaType)));
    SDOSink.PushString(sink, area, "");
    i = 0;
    while i < ArraySize(Deref(equipArea).equipSlots) {
      this.Debug_SetupESSlotButton(i, area, ownerGameObject);
      i += 1;
    };
  }

  public final const func Debug_SetupESSlotButton(slotIndex: Int32, const areaStr: script_ref<String>, ownerGameObject: wref<GameObject>) -> Void {
    let sink: SDOSink = GameInstance.GetScriptsDebugOverlaySystem(this.GetGameInstance()).CreateSink();
    SDOSink.SetRoot(sink, EquipmentSystem.ComposeSDORootPath(ownerGameObject, Deref(areaStr)));
    SDOSink.PushString(sink, "Slot " + slotIndex, "EMPTY");
    this.Debug_SetESSlotData(slotIndex, areaStr, ownerGameObject);
  }

  public final const func Debug_SetESSlotData(slotIndex: Int32, const areaStr: script_ref<String>, ownerGameObject: wref<GameObject>) -> Void {
    let sink: SDOSink = GameInstance.GetScriptsDebugOverlaySystem(this.GetGameInstance()).CreateSink();
    SDOSink.SetRoot(sink, EquipmentSystem.ComposeSDORootPath(ownerGameObject, areaStr + "/Slot " + slotIndex));
  }

  public final const func Debug_FillESSlotData(slotIndex: Int32, area: gamedataEquipmentArea, itemID: ItemID, ownerGameObject: wref<GameObject>) -> Void {
    this.Debug_FillESSlotData(slotIndex, EnumValueToString("gamedataEquipmentArea", Cast<Int64>(EnumInt(area))), itemID, ownerGameObject);
  }

  public final const func Debug_FillESSlotData(slotIndex: Int32, const areaStr: script_ref<String>, itemID: ItemID, ownerGameObject: wref<GameObject>) -> Void {
    let sink: SDOSink = GameInstance.GetScriptsDebugOverlaySystem(this.GetGameInstance()).CreateSink();
    SDOSink.SetRoot(sink, EquipmentSystem.ComposeSDORootPath(ownerGameObject, Deref(areaStr)));
    SDOSink.PushString(sink, "Slot " + slotIndex, TDBID.ToStringDEBUG(ItemID.GetTDBID(itemID)));
    SDOSink.SetRoot(sink, EquipmentSystem.ComposeSDORootPath(ownerGameObject, areaStr + "/Slot " + slotIndex));
    SDOSink.PushString(sink, "Item: ", TDBID.ToStringDEBUG(ItemID.GetTDBID(itemID)));
  }

  private final func OnEquipmentSystemWeaponManipulationRequest(request: ref<EquipmentSystemWeaponManipulationRequest>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnEquipmentSystemWeaponManipulationRequest(request);
  }

  private final func OnSetActiveItemInEquipmentArea(request: ref<SetActiveItemInEquipmentArea>) -> Void {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(request.owner);
    playerData.OnSetActiveItemInEquipmentArea(request);
  }

  public final const func GetInventoryManager(owner: wref<GameObject>) -> wref<InventoryDataManagerV2> {
    let playerData: ref<EquipmentSystemPlayerData> = this.GetPlayerData(owner);
    return playerData.GetInventoryManager();
  }
}
