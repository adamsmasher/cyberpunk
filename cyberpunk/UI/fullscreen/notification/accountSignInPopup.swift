
public class SignInPopupController extends inkGameController {

  private edit let m_qrCodeContainerRef: inkWidgetRef;

  private edit let m_hyperlinkButtonRef: inkWidgetRef;

  private edit let m_closeButtonRef: inkWidgetRef;

  private edit let m_introAnimationName: CName;

  private let m_data: ref<SignInPopupData>;

  private let m_requestHandler: wref<inkISystemRequestsHandler>;

  private let m_introAnimProxy: ref<inkAnimProxy>;

  protected cb func OnInitialize() -> Bool {
    this.m_data = this.GetRootWidget().GetUserData(n"SignInPopupData") as SignInPopupData;
    this.m_requestHandler = this.GetSystemRequestsHandler();
    this.RegisterToGlobalInputCallback(n"OnPostOnRelease", this, n"OnRelease");
    inkWidgetRef.RegisterToCallback(this.m_hyperlinkButtonRef, n"OnPress", this, n"OnPressHyperlink");
    inkWidgetRef.RegisterToCallback(this.m_closeButtonRef, n"OnPress", this, n"OnPressClose");
    this.m_introAnimProxy = this.PlayLibraryAnimation(this.m_introAnimationName);
    this.PlaySound(n"GameMenu", n"OnOpen");
  }

  protected cb func OnUninitialize() -> Bool {
    this.UnregisterFromGlobalInputCallback(n"OnPostOnRelease", this, n"OnRelease");
    inkWidgetRef.UnregisterFromCallback(this.m_hyperlinkButtonRef, n"OnPress", this, n"OnPressHyperlink");
    inkWidgetRef.UnregisterFromCallback(this.m_closeButtonRef, n"OnPress", this, n"OnPressClose");
  }

  private final func Close() -> Void {
    let playbackOptions: inkAnimOptions;
    this.PlaySound(n"Button", n"OnPress");
    playbackOptions.playReversed = true;
    this.m_introAnimProxy = this.PlayLibraryAnimation(this.m_introAnimationName, playbackOptions);
    this.m_introAnimProxy.RegisterToCallback(inkanimEventType.OnFinish, this, n"OnOutroAnimationFinished");
  }

  protected cb func OnRelease(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"close_popup") && !this.m_introAnimProxy.IsPlaying() {
      this.Close();
    };
  }

  protected cb func OnPressHyperlink(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"click") && !this.m_introAnimProxy.IsPlaying() {
      this.m_requestHandler.OpenPrivacyPolicyUrl();
    };
  }

  protected cb func OnPressClose(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"click") && !this.m_introAnimProxy.IsPlaying() {
      this.Close();
    };
  }

  protected cb func OnOutroAnimationFinished(proxy: ref<inkAnimProxy>) -> Bool {
    this.m_introAnimProxy.UnregisterFromCallback(inkanimEventType.OnFinish, this, n"OnOutroAnimationFinished");
    this.m_data.token.TriggerCallback(this.m_data);
  }
}
