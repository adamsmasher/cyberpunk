
public class NotifyPoliceEffector extends Effector {

  protected func ActionOn(owner: ref<GameObject>) -> Void {
    this.ProcessAction(owner);
  }

  private final func ProcessAction(owner: ref<GameObject>) -> Void {
    PreventionSystem.NotifyPolice(owner);
  }
}
