
public class QuestIncreaseNumber extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestIncreaseNumber";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}

public class QuestDecreaseNumber extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestDecreaseNumber";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}

public class QuestIdle extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestIdle";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}
