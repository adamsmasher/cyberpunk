
public class GenericButtonController extends inkLogicController {

  private edit let m_label: inkTextRef;

  private edit let m_frameHovered: inkWidgetRef;

  private let m_itemHovered: Bool;

  private let m_enabled: Bool;

  protected cb func OnInitialize() -> Bool {
    this.RegisterToCallback(n"OnRelease", this, n"OnButtonRelease");
    this.RegisterToCallback(n"OnHoverOver", this, n"OnButtonHoverOver");
    this.RegisterToCallback(n"OnHoverOut", this, n"OnButtonHoverOut");
    inkWidgetRef.SetOpacity(this.m_frameHovered, 0.00);
  }

  protected cb func OnUninitialize() -> Bool {
    this.UnregisterFromCallback(n"OnRelease", this, n"OnButtonRelease");
    this.UnregisterFromCallback(n"OnHoverOver", this, n"OnButtonHoverOver");
    this.UnregisterFromCallback(n"OnHoverOut", this, n"OnButtonHoverOut");
  }

  public final func Init(const label: script_ref<String>) -> Void {
    inkTextRef.SetText(this.m_label, Deref(label));
    GenericButtonController.ApplyEnabledLayout(this.m_label, this.GetRootWidget());
  }

  public final static func ApplyDisabledLayout(label: inkTextRef, root: ref<inkWidget>) -> Void {
    inkTextRef.SetText(label, "LocKey#20482");
    inkWidgetRef.Get(label).BindProperty(n"tintColor", n"MainColors.MildRed");
    inkWidgetRef.SetOpacity(label, 0.80);
  }

  public final static func ApplyEnabledLayout(label: inkTextRef, root: ref<inkWidget>) -> Void {
    inkWidgetRef.Get(label).BindProperty(n"tintColor", n"MainColors.Blue");
    inkWidgetRef.SetOpacity(label, 1.00);
  }

  public final func UpdateButton(const label: script_ref<String>) -> Void {
    inkTextRef.SetText(this.m_label, Deref(label));
  }

  protected cb func OnButtonRelease(e: ref<inkPointerEvent>) -> Bool {
    if e.IsAction(n"click") {
    };
  }

  protected cb func OnButtonHoverOver(evt: ref<inkPointerEvent>) -> Bool {
    if !this.m_enabled {
      inkWidgetRef.SetOpacity(this.m_frameHovered, 1.00);
      this.m_itemHovered = true;
    };
  }

  protected cb func OnButtonHoverOut(evt: ref<inkPointerEvent>) -> Bool {
    if !this.m_enabled {
      inkWidgetRef.SetOpacity(this.m_frameHovered, 0.00);
      this.m_itemHovered = false;
    };
  }
}
