
public abstract final native class UIItemsHelper extends IScriptable {

  public final static native func QualityStringToStateName(quality: String) -> CName;

  public final static native func QualityToLocalizationKey(quality: gamedataQuality) -> String;

  public final static native func QualityEnumToName(quality: gamedataQuality) -> CName;

  public final static native func QualityNameToEnum(quality: CName) -> gamedataQuality;

  public final static native func QualityEnumToInt(quality: gamedataQuality) -> Int32;

  public final static native func QualityEnumToString(quality: gamedataQuality) -> String;

  public final static native func QualityIntToName(quality: Int32) -> CName;

  public final static native func QualityStringToInt(quality: String) -> Int32;

  public final static native func QualityNameToInt(quality: CName) -> Int32;

  public final static native func QualityToInt(quality: gamedataQuality) -> Int32;

  public final static native func IntToQuality(quality: Int32) -> gamedataQuality;

  public final static func QualityToDefaultString(quality: gamedataQuality, opt type: RarityItemType) -> String {
    switch type {
      case RarityItemType.Item:
        return UIItemsHelper.QualityToTierString(quality);
      case RarityItemType.Cyberdeck:
        return UIItemsHelper.QualityToLocalizationKey(quality);
      case RarityItemType.Program:
        return UIItemsHelper.QualityToTierString(quality);
    };
    return UIItemsHelper.QualityToTierString(quality);
  }

  public final static func QualityToTierString(quality: gamedataQuality) -> String {
    switch quality {
      case gamedataQuality.Common:
        return "Gameplay-RPG-Stats-Tiers-Tier1";
      case gamedataQuality.Uncommon:
        return "Gameplay-RPG-Stats-Tiers-Tier2";
      case gamedataQuality.Rare:
        return "Gameplay-RPG-Stats-Tiers-Tier3";
      case gamedataQuality.Epic:
        return "Gameplay-RPG-Stats-Tiers-Tier4";
      case gamedataQuality.Legendary:
        return "Gameplay-RPG-Stats-Tiers-Tier5";
      case gamedataQuality.Iconic:
        return UIItemsHelper.QualityToLocalizationKey(quality);
    };
    return "Gameplay-RPG-Stats-Tiers-Tier1";
  }

  public final static func QualityToTierPlusString(quality: gamedataQuality) -> String {
    switch quality {
      case gamedataQuality.Common:
        return "Gameplay-RPG-Stats-Tiers-Tier1";
      case gamedataQuality.CommonPlus:
        return "Gameplay-RPG-Stats-Tiers-Tier1plus";
      case gamedataQuality.Uncommon:
        return "Gameplay-RPG-Stats-Tiers-Tier2";
      case gamedataQuality.UncommonPlus:
        return "Gameplay-RPG-Stats-Tiers-Tier2plus";
      case gamedataQuality.Rare:
        return "Gameplay-RPG-Stats-Tiers-Tier3";
      case gamedataQuality.RarePlus:
        return "Gameplay-RPG-Stats-Tiers-Tier3plus";
      case gamedataQuality.Epic:
        return "Gameplay-RPG-Stats-Tiers-Tier4";
      case gamedataQuality.EpicPlus:
        return "Gameplay-RPG-Stats-Tiers-Tier4plus";
      case gamedataQuality.Legendary:
        return "Gameplay-RPG-Stats-Tiers-Tier5";
      case gamedataQuality.LegendaryPlus:
        return "Gameplay-RPG-Stats-Tiers-Tier5plus";
      case gamedataQuality.LegendaryPlusPlus:
        return "Gameplay-RPG-Stats-Tiers-Tier5plusplus";
      case gamedataQuality.Iconic:
        return UIItemsHelper.QualityToLocalizationKey(quality);
    };
    return "Gameplay-RPG-Stats-Tiers-Tier1";
  }

  public final static func GetStatTypeByDamageType(type: gamedataDamageType) -> gamedataStatType {
    switch type {
      case gamedataDamageType.Chemical:
        return gamedataStatType.ChemicalDamage;
      case gamedataDamageType.Electric:
        return gamedataStatType.ElectricDamage;
      case gamedataDamageType.Physical:
        return gamedataStatType.PhysicalDamage;
      case gamedataDamageType.Thermal:
        return gamedataStatType.ThermalDamage;
      default:
        return gamedataStatType.Invalid;
    };
  }

  public final static func GetStateNameForDamageType(damageType: gamedataDamageType) -> CName {
    switch damageType {
      case gamedataDamageType.Chemical:
        return n"Chemical";
      case gamedataDamageType.Electric:
        return n"EMP";
      case gamedataDamageType.Physical:
        return n"Physical";
      case gamedataDamageType.Thermal:
        return n"Thermal";
      default:
        return n"Default";
    };
  }

  public final static func GetIconNameForDamageType(damageType: gamedataDamageType) -> String {
    switch damageType {
      case gamedataDamageType.Chemical:
        return "icon_chemical";
      case gamedataDamageType.Electric:
        return "icon_emp";
      case gamedataDamageType.Physical:
        return "icon_physical";
      case gamedataDamageType.Thermal:
        return "icon_thermal";
      default:
        return "None";
    };
  }

  public final static func GetTweakDBIDForDamageType(damageType: gamedataDamageType) -> TweakDBID {
    switch damageType {
      case gamedataDamageType.Chemical:
        return t"UIIcon.DamageType_Chemical";
      case gamedataDamageType.Electric:
        return t"UIIcon.DamageType_EMP";
      case gamedataDamageType.Physical:
        return t"UIIcon.DamageType_Physical";
      case gamedataDamageType.Thermal:
        return t"UIIcon.DamageType_Thermal";
      default:
        return t"UIIcon.ItemIcon";
    };
  }

  public final static func GetStateNameForType(damageType: gamedataDamageType) -> CName {
    switch damageType {
      case gamedataDamageType.Chemical:
        return n"Chemical";
      case gamedataDamageType.Electric:
        return n"EMP";
      case gamedataDamageType.Physical:
        return n"Physical";
      case gamedataDamageType.Thermal:
        return n"Thermal";
      default:
        return n"Default";
    };
  }

  public final static func GetStateNameForStat(statType: gamedataStatType) -> CName {
    switch statType {
      case gamedataStatType.ChemicalDamage:
      case gamedataStatType.ChemicalResistance:
        return n"Chemical";
      case gamedataStatType.ElectricResistance:
      case gamedataStatType.ElectricDamage:
        return n"EMP";
      case gamedataStatType.PhysicalResistance:
      case gamedataStatType.PhysicalDamage:
        return n"Physical";
      case gamedataStatType.ThermalDamage:
      case gamedataStatType.ThermalResistance:
        return n"Thermal";
      case gamedataStatType.Health:
        return n"Health";
      default:
        return n"Default";
    };
  }

  public final static func GetIconNameForStat(statType: gamedataStatType) -> CName {
    switch statType {
      case gamedataStatType.ChemicalDamage:
      case gamedataStatType.ChemicalResistance:
        return n"icon_chemical";
      case gamedataStatType.ElectricResistance:
      case gamedataStatType.ElectricDamage:
        return n"icon_emp";
      case gamedataStatType.PhysicalResistance:
      case gamedataStatType.PhysicalDamage:
        return n"icon_physical";
      case gamedataStatType.ThermalDamage:
      case gamedataStatType.ThermalResistance:
        return n"icon_thermal";
      case gamedataStatType.Health:
        return n"icon_health";
      default:
        return n"None";
    };
  }

  public final static func GetBGIconNameForStat(statType: gamedataStatType) -> CName {
    switch statType {
      case gamedataStatType.ChemicalDamage:
      case gamedataStatType.ChemicalResistance:
        return n"scan_bg_3";
      case gamedataStatType.ElectricResistance:
      case gamedataStatType.ElectricDamage:
        return n"scan_bg_2";
      case gamedataStatType.PhysicalResistance:
      case gamedataStatType.PhysicalDamage:
        return n"scan_bg_1";
      case gamedataStatType.ThermalDamage:
      case gamedataStatType.ThermalResistance:
        return n"scan_bg_2";
      default:
        return n"None";
    };
  }

  public final static func GetMellewareSecondaryTypeText(type: gamedataItemType) -> String {
    switch type {
      case gamedataItemType.Cyb_Launcher:
        return "LocKey#3722";
      case gamedataItemType.Cyb_MantisBlades:
        return "LocKey#77957";
      case gamedataItemType.Cyb_NanoWires:
        return "LocKey#3720";
      case gamedataItemType.Cyb_StrongArms:
        return "LocKey#776";
    };
    return "";
  }

  public final static func GetMellewareEvolutionTexturePartByType(type: gamedataItemType) -> CName {
    switch type {
      case gamedataItemType.Cyb_Launcher:
        return n"ico_projectile_launcher";
      case gamedataItemType.Cyb_MantisBlades:
        return n"ico_blades";
      case gamedataItemType.Cyb_NanoWires:
        return n"ico_monowire";
      case gamedataItemType.Cyb_StrongArms:
        return n"ico_blunt";
    };
    return n"None";
  }

  public final static func GetWeaponEvolutionTexturePart(evolution: gamedataWeaponEvolution) -> CName {
    switch evolution {
      case gamedataWeaponEvolution.Power:
        return n"ico_power";
      case gamedataWeaponEvolution.Smart:
        return n"ico_smart";
      case gamedataWeaponEvolution.Tech:
        return n"ico_tech-1";
      case gamedataWeaponEvolution.Blunt:
        return n"ico_blunt";
      case gamedataWeaponEvolution.Blade:
        return n"ico_blades";
      case gamedataWeaponEvolution.Throwable:
        return n"ico_throwables";
    };
    return n"None";
  }

  public final static func WeaponEvolutionText(evolution: gamedataWeaponEvolution) -> String {
    switch evolution {
      case gamedataWeaponEvolution.Power:
        return "LocKey#54117";
      case gamedataWeaponEvolution.Smart:
        return "LocKey#54120";
      case gamedataWeaponEvolution.Tech:
        return "LocKey#54122";
      case gamedataWeaponEvolution.Blunt:
        return "LocKey#77969";
      case gamedataWeaponEvolution.Blade:
        return "LocKey#77960";
      case gamedataWeaponEvolution.Throwable:
        return "LocKey#91803";
    };
    return "";
  }

  public final static func GetWeaponTypeIcon(itemType: gamedataItemType) -> CName {
    switch itemType {
      case gamedataItemType.Wea_AssaultRifle:
        return n"UIIcon.WeaponTypeIcon_AssaultRifle";
      case gamedataItemType.Wea_Axe:
        return n"UIIcon.WeaponTypeIcon_Axe";
      case gamedataItemType.Wea_Chainsword:
        return n"UIIcon.WeaponTypeIcon_Chainsword";
      case gamedataItemType.Wea_Fists:
        return n"UIIcon.WeaponTypeIcon_Fists";
      case gamedataItemType.Wea_Hammer:
        return n"UIIcon.WeaponTypeIcon_Hammer";
      case gamedataItemType.Wea_Handgun:
        return n"UIIcon.WeaponTypeIcon_Handgun";
      case gamedataItemType.Wea_HeavyMachineGun:
        return n"UIIcon.WeaponTypeIcon_HeavyMachineGun";
      case gamedataItemType.Wea_Katana:
        return n"UIIcon.WeaponTypeIcon_Katana";
      case gamedataItemType.Wea_Sword:
        return n"UIIcon.WeaponTypeIcon_Sword";
      case gamedataItemType.Wea_Knife:
        return n"UIIcon.WeaponTypeIcon_Knife";
      case gamedataItemType.Wea_LightMachineGun:
        return n"UIIcon.WeaponTypeIcon_LightMachineGun";
      case gamedataItemType.Wea_LongBlade:
        return n"UIIcon.WeaponTypeIcon_LongBlade";
      case gamedataItemType.Wea_Machete:
        return n"UIIcon.WeaponTypeIcon_Machete";
      case gamedataItemType.Wea_Melee:
        return n"UIIcon.WeaponTypeIcon_Melee";
      case gamedataItemType.Wea_OneHandedClub:
        return n"UIIcon.WeaponTypeIcon_OneHandedClub";
      case gamedataItemType.Wea_PrecisionRifle:
        return n"UIIcon.WeaponTypeIcon_PrecisionRifle";
      case gamedataItemType.Wea_Revolver:
        return n"UIIcon.WeaponTypeIcon_Revolver";
      case gamedataItemType.Wea_Rifle:
        return n"UIIcon.WeaponTypeIcon_Rifle";
      case gamedataItemType.Wea_ShortBlade:
        return n"UIIcon.WeaponTypeIcon_ShortBlade";
      case gamedataItemType.Wea_Shotgun:
        return n"UIIcon.WeaponTypeIcon_Shotgun";
      case gamedataItemType.Wea_ShotgunDual:
        return n"UIIcon.WeaponTypeIcon_ShotgunDual";
      case gamedataItemType.Wea_SniperRifle:
        return n"UIIcon.WeaponTypeIcon_SniperRifle";
      case gamedataItemType.Wea_SubmachineGun:
        return n"UIIcon.WeaponTypeIcon_SubmachineGun";
      case gamedataItemType.Wea_TwoHandedClub:
        return n"UIIcon.WeaponTypeIcon_TwoHandedClub";
      case gamedataItemType.Wea_GrenadeLauncher:
        return n"UIIcon.WeaponTypeIcon_GrenadeLauncher";
    };
    return n"UIIcon.WeaponTypeIcon_Default";
  }

  public final static func GetAmmoIconByType(itemType: gamedataItemType) -> CName {
    switch itemType {
      case gamedataItemType.Wea_SubmachineGun:
      case gamedataItemType.Wea_Revolver:
      case gamedataItemType.Wea_Handgun:
        return n"ammo_handgun";
      case gamedataItemType.Wea_Rifle:
      case gamedataItemType.Wea_AssaultRifle:
      case gamedataItemType.Wea_LightMachineGun:
      case gamedataItemType.Wea_HeavyMachineGun:
      case gamedataItemType.Wea_PrecisionRifle:
        return n"ammo_rifle";
      case gamedataItemType.Wea_SniperRifle:
        return n"ammo_sniper";
      case gamedataItemType.Wea_ShotgunDual:
      case gamedataItemType.Wea_Shotgun:
        return n"ammo_shotgun";
      default:
        return n"None";
    };
  }

  public final static func GetWeaponTooltipIcon(itemType: gamedataItemType) -> CName {
    switch itemType {
      case gamedataItemType.Wea_AssaultRifle:
        return n"rifle_preci";
      case gamedataItemType.Wea_Handgun:
        return n"pistol";
      case gamedataItemType.Wea_HeavyMachineGun:
        return n"heavy_machine";
      case gamedataItemType.Wea_LightMachineGun:
        return n"light_machine";
      case gamedataItemType.Wea_PrecisionRifle:
        return n"rifle_preci";
      case gamedataItemType.Wea_Revolver:
        return n"revolver_tech";
      case gamedataItemType.Wea_Rifle:
        return n"rifle_preci";
      case gamedataItemType.Wea_Shotgun:
        return n"shotgun_power";
      case gamedataItemType.Wea_ShotgunDual:
        return n"shotgun_power";
      case gamedataItemType.Wea_SniperRifle:
        return n"sniper_smart";
      case gamedataItemType.Wea_SubmachineGun:
        return n"subma_power";
    };
    return n"None";
  }

  public final static func GetBasicPerkRelevance(perkGroup: gamedataPerkWeaponGroupType) -> CName {
    switch perkGroup {
      case gamedataPerkWeaponGroupType.LMGsPerkWeaponGroup:
      case gamedataPerkWeaponGroupType.ShotgunsPerkWeaponGroup:
      case gamedataPerkWeaponGroupType.BodyGunsPerkWeaponGroup:
        return n"ico_body";
      case gamedataPerkWeaponGroupType.AssaultRiflesPerkWeaponGroup:
      case gamedataPerkWeaponGroupType.SMGsPerkWeaponGroup:
      case gamedataPerkWeaponGroupType.ReflexesGunsPerkWeaponGroup:
        return n"ico_ref";
      case gamedataPerkWeaponGroupType.HandgunsPerkWeaponGroup:
      case gamedataPerkWeaponGroupType.CoolGunsPerkWeaponGroup:
        return n"ico_cool";
    };
    return n"None";
  }

  public final static func GetBasicPerkRelevanceGroup(itemType: gamedataItemType) -> gamedataPerkWeaponGroupType {
    switch itemType {
      case gamedataItemType.Wea_LightMachineGun:
      case gamedataItemType.Wea_ShotgunDual:
      case gamedataItemType.Wea_Shotgun:
        return gamedataPerkWeaponGroupType.BodyGunsPerkWeaponGroup;
      case gamedataItemType.Wea_AssaultRifle:
      case gamedataItemType.Wea_SubmachineGun:
        return gamedataPerkWeaponGroupType.ReflexesGunsPerkWeaponGroup;
      case gamedataItemType.Wea_Handgun:
      case gamedataItemType.Wea_SniperRifle:
      case gamedataItemType.Wea_Revolver:
      case gamedataItemType.Wea_PrecisionRifle:
        return gamedataPerkWeaponGroupType.CoolGunsPerkWeaponGroup;
    };
    return gamedataPerkWeaponGroupType.Invalid;
  }

  public final static func GetMasterPerkRelevanceGroup(itemType: gamedataItemType) -> gamedataPerkWeaponGroupType {
    switch itemType {
      case gamedataItemType.Wea_AssaultRifle:
        return gamedataPerkWeaponGroupType.AssaultRiflesPerkWeaponGroup;
      case gamedataItemType.Wea_PrecisionRifle:
        return gamedataPerkWeaponGroupType.PrecisionGunsPerkWeaponGroup;
      case gamedataItemType.Wea_Handgun:
        return gamedataPerkWeaponGroupType.HandgunsPerkWeaponGroup;
      case gamedataItemType.Wea_LightMachineGun:
        return gamedataPerkWeaponGroupType.LMGsPerkWeaponGroup;
      case gamedataItemType.Wea_ShotgunDual:
      case gamedataItemType.Wea_Shotgun:
        return gamedataPerkWeaponGroupType.ShotgunsPerkWeaponGroup;
      case gamedataItemType.Wea_SubmachineGun:
        return gamedataPerkWeaponGroupType.SMGsPerkWeaponGroup;
    };
    return gamedataPerkWeaponGroupType.Invalid;
  }

  public final static func PerkWeaponGroupToText(perkGroup: gamedataPerkWeaponGroupType) -> String {
    switch perkGroup {
      case gamedataPerkWeaponGroupType.AssaultRiflesPerkWeaponGroup:
        return "LocKey#91788";
      case gamedataPerkWeaponGroupType.BladesPerkWeaponGroup:
        return "LocKey#91789";
      case gamedataPerkWeaponGroupType.BluntsPerkWeaponGroup:
        return "LocKey#91790";
      case gamedataPerkWeaponGroupType.BodyGunsPerkWeaponGroup:
        return "LocKey#91791";
      case gamedataPerkWeaponGroupType.CoolGunsPerkWeaponGroup:
        return "LocKey#91792";
      case gamedataPerkWeaponGroupType.HandgunsPerkWeaponGroup:
        return "LocKey#91793";
      case gamedataPerkWeaponGroupType.LMGsPerkWeaponGroup:
        return "LocKey#91794";
      case gamedataPerkWeaponGroupType.PrecisionGunsPerkWeaponGroup:
        return "LocKey#91795";
      case gamedataPerkWeaponGroupType.ReflexesGunsPerkWeaponGroup:
        return "LocKey#91796";
      case gamedataPerkWeaponGroupType.SMGsPerkWeaponGroup:
        return "LocKey#91797";
      case gamedataPerkWeaponGroupType.ShotgunsPerkWeaponGroup:
        return "LocKey#91798";
      case gamedataPerkWeaponGroupType.SmartGunsPerkWeaponGroup:
        return "LocKey#91799";
      case gamedataPerkWeaponGroupType.TechGunsPerkWeaponGroup:
        return "LocKey#91800";
      case gamedataPerkWeaponGroupType.ThrowablePerkWeaponGroup:
        return "LocKey#91801";
    };
    return "";
  }

  public final static func GetSlotShadowIcon(slotID: TweakDBID, itemType: gamedataItemType, equipmentArea: gamedataEquipmentArea) -> CName {
    switch slotID {
      case t"AttachmentSlots.Scope":
        return n"UIIcon.ItemShadow_Scope";
      case t"AttachmentSlots.PowerModule":
        return n"UIIcon.ItemShadow_Silencer";
      case t"AttachmentSlots.Magazine":
        return n"UIIcon.ItemShadow_Magazine";
      case t"AttachmentSlots.Throwable_WeaponMod2_Collectible":
      case t"AttachmentSlots.Throwable_WeaponMod1_Collectible":
      case t"AttachmentSlots.Throwable_WeaponMod2":
      case t"AttachmentSlots.Throwable_WeaponMod1":
      case t"AttachmentSlots.Blunt_WeaponMod2_Collectible":
      case t"AttachmentSlots.Blunt_WeaponMod1_Collectible":
      case t"AttachmentSlots.Blunt_WeaponMod2":
      case t"AttachmentSlots.Blunt_WeaponMod1":
      case t"AttachmentSlots.Blade_WeaponMod2_Collectible":
      case t"AttachmentSlots.Blade_WeaponMod1_Collectible":
      case t"AttachmentSlots.Blade_WeaponMod2":
      case t"AttachmentSlots.Blade_WeaponMod1":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod2_Collectible":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod1_Collectible":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod2":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod1":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod2_Collectible":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod1_Collectible":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod2":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod1":
      case t"AttachmentSlots.Power_Precision_Sniper_Rifle_WeaponMod2":
      case t"AttachmentSlots.Power_Precision_Sniper_Rifle_WeaponMod1":
      case t"AttachmentSlots.Smart_Shotgun_WeaponMod2":
      case t"AttachmentSlots.Smart_Shotgun_WeaponMod1":
      case t"AttachmentSlots.Tech_Shotgun_WeaponMod2":
      case t"AttachmentSlots.Tech_Shotgun_WeaponMod1":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod2_Collectible":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod1_Collectible":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod2":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod1":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod2_Collectible":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod1_Collectible":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod2":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod1":
      case t"AttachmentSlots.Tech_Handgun_WeaponMod2":
      case t"AttachmentSlots.Tech_Handgun_WeaponMod1":
      case t"AttachmentSlots.Power_Handgun_WeaponMod2_Collectible":
      case t"AttachmentSlots.Power_Handgun_WeaponMod1_Collectible":
      case t"AttachmentSlots.Power_Handgun_WeaponMod2":
      case t"AttachmentSlots.Power_Handgun_WeaponMod1":
      case t"AttachmentSlots.Smart_AR_SMG_LMG_WeaponMod2":
      case t"AttachmentSlots.Smart_AR_SMG_LMG_WeaponMod1":
      case t"AttachmentSlots.Tech_AR_SMG_LMG_WeaponMod2":
      case t"AttachmentSlots.Tech_AR_SMG_LMG_WeaponMod1":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod2_Collectible":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod1_Collectible":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod2":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod1":
        return n"UIIcon.ItemShadow_Mod";
      case t"AttachmentSlots.NanoWiresQuickhackSlot":
      case t"AttachmentSlots.CyberdeckProgram8":
      case t"AttachmentSlots.CyberdeckProgram7":
      case t"AttachmentSlots.CyberdeckProgram6":
      case t"AttachmentSlots.CyberdeckProgram5":
      case t"AttachmentSlots.CyberdeckProgram4":
      case t"AttachmentSlots.CyberdeckProgram3":
      case t"AttachmentSlots.CyberdeckProgram2":
      case t"AttachmentSlots.CyberdeckProgram1":
        return n"UIIcon.ItemShadow_Program";
      case t"AttachmentSlots.ArmsCyberwareGeneralSlot":
      case t"AttachmentSlots.ProjectileLauncherWiring":
      case t"AttachmentSlots.ProjectileLauncherRound":
      case t"AttachmentSlots.NanoWiresBattery":
      case t"AttachmentSlots.NanoWiresCable":
      case t"AttachmentSlots.MantisBladesRotor":
      case t"AttachmentSlots.MantisBladesEdge":
      case t"AttachmentSlots.StrongArmsBattery":
      case t"AttachmentSlots.StrongArmsKnuckles":
      case t"AttachmentSlots.KiroshiOpticsSlot3":
      case t"AttachmentSlots.KiroshiOpticsSlot2":
      case t"AttachmentSlots.KiroshiOpticsSlot1":
      case t"AttachmentSlots.BerserkSlot3":
      case t"AttachmentSlots.BerserkSlot2":
      case t"AttachmentSlots.BerserkSlot1":
      case t"AttachmentSlots.SandevistanSlot3":
      case t"AttachmentSlots.SandevistanSlot2":
      case t"AttachmentSlots.SandevistanSlot1":
        return n"UIIcon.ItemShadow_Fragment";
      case t"AttachmentSlots.FootFabricEnhancer3":
      case t"AttachmentSlots.FootFabricEnhancer2":
      case t"AttachmentSlots.FootFabricEnhancer1":
      case t"AttachmentSlots.LegsFabricEnhancer3":
      case t"AttachmentSlots.LegsFabricEnhancer2":
      case t"AttachmentSlots.LegsFabricEnhancer1":
      case t"AttachmentSlots.OuterChestFabricEnhancer3":
      case t"AttachmentSlots.OuterChestFabricEnhancer2":
      case t"AttachmentSlots.OuterChestFabricEnhancer1":
      case t"AttachmentSlots.InnerChestFabricEnhancer3":
      case t"AttachmentSlots.InnerChestFabricEnhancer2":
      case t"AttachmentSlots.InnerChestFabricEnhancer1":
      case t"AttachmentSlots.FaceFabricEnhancer3":
      case t"AttachmentSlots.FaceFabricEnhancer2":
      case t"AttachmentSlots.FaceFabricEnhancer1":
      case t"AttachmentSlots.HeadFabricEnhancer3":
      case t"AttachmentSlots.HeadFabricEnhancer2":
      case t"AttachmentSlots.HeadFabricEnhancer1":
        return n"UIIcon.ItemShadow_Material";
    };
    return UIItemsHelper.GetSlotShadowIcon(itemType, equipmentArea);
  }

  public final static func GetSlotShadowIcon(itemType: gamedataItemType, equipmentArea: gamedataEquipmentArea) -> CName {
    switch itemType {
      case gamedataItemType.Prt_FabricEnhancer:
        return n"UIIcon.ItemShadow_Material";
      case gamedataItemType.Prt_Fragment:
        return n"UIIcon.ItemShadow_Fragment";
      case gamedataItemType.Prt_Magazine:
        return n"UIIcon.ItemShadow_Magazine";
      case gamedataItemType.Prt_ThrowableMod:
      case gamedataItemType.Prt_BluntMod:
      case gamedataItemType.Prt_BladeMod:
      case gamedataItemType.Prt_MeleeMod:
      case gamedataItemType.Prt_ShotgunMod:
      case gamedataItemType.Prt_Precision_Sniper_RifleMod:
      case gamedataItemType.Prt_HandgunMod:
      case gamedataItemType.Prt_AR_SMG_LMGMod:
      case gamedataItemType.Prt_SmartMod:
      case gamedataItemType.Prt_TechMod:
      case gamedataItemType.Prt_PowerMod:
      case gamedataItemType.Prt_RangedMod:
      case gamedataItemType.Prt_Mod:
        return n"UIIcon.ItemShadow_Mod";
      case gamedataItemType.Prt_RifleMuzzle:
      case gamedataItemType.Prt_HandgunMuzzle:
      case gamedataItemType.Prt_Muzzle:
        return n"UIIcon.ItemShadow_Silencer";
      case gamedataItemType.Prt_Program:
        return n"UIIcon.ItemShadow_Program";
      case gamedataItemType.Prt_Receiver:
        return n"UIIcon.ItemShadow_Receiver";
      case gamedataItemType.Prt_PowerSniperScope:
      case gamedataItemType.Prt_TechSniperScope:
      case gamedataItemType.Prt_LongScope:
      case gamedataItemType.Prt_ShortScope:
      case gamedataItemType.Prt_Scope:
        return n"UIIcon.ItemShadow_Scope";
      case gamedataItemType.Prt_ScopeRail:
        return n"UIIcon.ItemShadow_ScopeRail";
      case gamedataItemType.Prt_Stock:
        return n"UIIcon.ItemShadow_Stock";
      case gamedataItemType.Prt_TargetingSystem:
        return n"UIIcon.ItemShadow_TargetingSystem";
    };
    return UIItemsHelper.GetSlotShadowIcon(equipmentArea);
  }

  public final static func GetSlotShadowIcon(equipmentArea: gamedataEquipmentArea) -> CName {
    switch equipmentArea {
      case gamedataEquipmentArea.Consumable:
        return n"UIIcon.ItemShadow_Consumable";
      case gamedataEquipmentArea.AbilityCW:
        return n"UIIcon.ItemShadow_Cyberware";
      case gamedataEquipmentArea.Face:
        return n"UIIcon.ItemShadow_Face";
      case gamedataEquipmentArea.Feet:
        return n"UIIcon.ItemShadow_Feet";
      case gamedataEquipmentArea.Gadget:
      case gamedataEquipmentArea.QuickSlot:
        return n"UIIcon.ItemShadow_Grenade";
      case gamedataEquipmentArea.Head:
        return n"UIIcon.ItemShadow_Head";
      case gamedataEquipmentArea.InnerChest:
        return n"UIIcon.ItemShadow_InnerChest";
      case gamedataEquipmentArea.Legs:
        return n"UIIcon.ItemShadow_Legs";
      case gamedataEquipmentArea.OuterChest:
        return n"UIIcon.ItemShadow_OuterChest";
      case gamedataEquipmentArea.Outfit:
        return n"UIIcon.ItemShadow_Outfit";
      case gamedataEquipmentArea.Weapon:
        return n"UIIcon.ItemShadow_Weapon";
    };
    return n"UIIcon.ItemShadow_Default";
  }

  public final static func GetLootingShadowIcon(itemTDBID: TweakDBID, slotID: TweakDBID, itemType: gamedataItemType, equipmentArea: gamedataEquipmentArea) -> CName {
    switch itemTDBID {
      case t"Items.money":
        return n"UIIcon.LootingShadow_Cash";
      case t"Ammo.HandgunAmmo":
        return n"UIIcon.LootingShadow_HandgunAmmo";
      case t"Ammo.ShotgunAmmo":
        return n"UIIcon.LootingShadow_ShotgunAmmo";
      case t"Ammo.RifleAmmo":
        return n"UIIcon.LootingShadow_RifleAmmo";
      case t"Ammo.SniperRifleAmmo":
        return n"UIIcon.LootingShadow_SniperRifleAmmo";
    };
    return UIItemsHelper.GetLootingShadowIcon(slotID, itemType, equipmentArea);
  }

  public final static func GetLootingShadowIcon(slotID: TweakDBID, itemType: gamedataItemType, equipmentArea: gamedataEquipmentArea) -> CName {
    switch slotID {
      case t"AttachmentSlots.Scope":
        return n"UIIcon.LootingShadow_Scope";
      case t"AttachmentSlots.PowerModule":
        return n"UIIcon.LootingShadow_Silencer";
      case t"AttachmentSlots.Magazine":
        return n"UIIcon.LootingShadow_Magazine";
      case t"AttachmentSlots.Throwable_WeaponMod2_Collectible":
      case t"AttachmentSlots.Throwable_WeaponMod1_Collectible":
      case t"AttachmentSlots.Throwable_WeaponMod2":
      case t"AttachmentSlots.Throwable_WeaponMod1":
      case t"AttachmentSlots.Blunt_WeaponMod2_Collectible":
      case t"AttachmentSlots.Blunt_WeaponMod1_Collectible":
      case t"AttachmentSlots.Blunt_WeaponMod2":
      case t"AttachmentSlots.Blunt_WeaponMod1":
      case t"AttachmentSlots.Blade_WeaponMod2_Collectible":
      case t"AttachmentSlots.Blade_WeaponMod1_Collectible":
      case t"AttachmentSlots.Blade_WeaponMod2":
      case t"AttachmentSlots.Blade_WeaponMod1":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod2_Collectible":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod1_Collectible":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod2":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod1":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod2_Collectible":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod1_Collectible":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod2":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod1":
      case t"AttachmentSlots.Power_Precision_Sniper_Rifle_WeaponMod2":
      case t"AttachmentSlots.Power_Precision_Sniper_Rifle_WeaponMod1":
      case t"AttachmentSlots.Smart_Shotgun_WeaponMod2":
      case t"AttachmentSlots.Smart_Shotgun_WeaponMod1":
      case t"AttachmentSlots.Tech_Shotgun_WeaponMod2":
      case t"AttachmentSlots.Tech_Shotgun_WeaponMod1":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod2_Collectible":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod1_Collectible":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod2":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod1":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod2_Collectible":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod1_Collectible":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod2":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod1":
      case t"AttachmentSlots.Tech_Handgun_WeaponMod2":
      case t"AttachmentSlots.Tech_Handgun_WeaponMod1":
      case t"AttachmentSlots.Power_Handgun_WeaponMod2_Collectible":
      case t"AttachmentSlots.Power_Handgun_WeaponMod1_Collectible":
      case t"AttachmentSlots.Power_Handgun_WeaponMod2":
      case t"AttachmentSlots.Power_Handgun_WeaponMod1":
      case t"AttachmentSlots.Smart_AR_SMG_LMG_WeaponMod2":
      case t"AttachmentSlots.Smart_AR_SMG_LMG_WeaponMod1":
      case t"AttachmentSlots.Tech_AR_SMG_LMG_WeaponMod2":
      case t"AttachmentSlots.Tech_AR_SMG_LMG_WeaponMod1":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod2_Collectible":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod1_Collectible":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod2":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod1":
        return n"UIIcon.LootingShadow_Mod";
      case t"AttachmentSlots.NanoWiresQuickhackSlot":
      case t"AttachmentSlots.CyberdeckProgram8":
      case t"AttachmentSlots.CyberdeckProgram7":
      case t"AttachmentSlots.CyberdeckProgram6":
      case t"AttachmentSlots.CyberdeckProgram5":
      case t"AttachmentSlots.CyberdeckProgram4":
      case t"AttachmentSlots.CyberdeckProgram3":
      case t"AttachmentSlots.CyberdeckProgram2":
      case t"AttachmentSlots.CyberdeckProgram1":
        return n"UIIcon.LootingShadow_Program";
      case t"AttachmentSlots.ArmsCyberwareGeneralSlot":
      case t"AttachmentSlots.ProjectileLauncherWiring":
      case t"AttachmentSlots.ProjectileLauncherRound":
      case t"AttachmentSlots.NanoWiresBattery":
      case t"AttachmentSlots.NanoWiresCable":
      case t"AttachmentSlots.MantisBladesRotor":
      case t"AttachmentSlots.MantisBladesEdge":
      case t"AttachmentSlots.StrongArmsBattery":
      case t"AttachmentSlots.StrongArmsKnuckles":
      case t"AttachmentSlots.KiroshiOpticsSlot3":
      case t"AttachmentSlots.KiroshiOpticsSlot2":
      case t"AttachmentSlots.KiroshiOpticsSlot1":
      case t"AttachmentSlots.BerserkSlot3":
      case t"AttachmentSlots.BerserkSlot2":
      case t"AttachmentSlots.BerserkSlot1":
      case t"AttachmentSlots.SandevistanSlot3":
      case t"AttachmentSlots.SandevistanSlot2":
      case t"AttachmentSlots.SandevistanSlot1":
        return n"UIIcon.LootingShadow_Fragment";
      case t"AttachmentSlots.FootFabricEnhancer3":
      case t"AttachmentSlots.FootFabricEnhancer2":
      case t"AttachmentSlots.FootFabricEnhancer1":
      case t"AttachmentSlots.LegsFabricEnhancer3":
      case t"AttachmentSlots.LegsFabricEnhancer2":
      case t"AttachmentSlots.LegsFabricEnhancer1":
      case t"AttachmentSlots.OuterChestFabricEnhancer3":
      case t"AttachmentSlots.OuterChestFabricEnhancer2":
      case t"AttachmentSlots.OuterChestFabricEnhancer1":
      case t"AttachmentSlots.InnerChestFabricEnhancer3":
      case t"AttachmentSlots.InnerChestFabricEnhancer2":
      case t"AttachmentSlots.InnerChestFabricEnhancer1":
      case t"AttachmentSlots.FaceFabricEnhancer3":
      case t"AttachmentSlots.FaceFabricEnhancer2":
      case t"AttachmentSlots.FaceFabricEnhancer1":
      case t"AttachmentSlots.HeadFabricEnhancer3":
      case t"AttachmentSlots.HeadFabricEnhancer2":
      case t"AttachmentSlots.HeadFabricEnhancer1":
        return n"UIIcon.LootingShadow_Material";
    };
    return UIItemsHelper.GetLootingShadowIcon(itemType, equipmentArea);
  }

  public final static func GetLootingShadowIcon(itemType: gamedataItemType, equipmentArea: gamedataEquipmentArea) -> CName {
    switch itemType {
      case gamedataItemType.Prt_BootsFabricEnhancer:
      case gamedataItemType.Prt_PantsFabricEnhancer:
      case gamedataItemType.Prt_OuterTorsoFabricEnhancer:
      case gamedataItemType.Prt_FaceFabricEnhancer:
      case gamedataItemType.Prt_HeadFabricEnhancer:
      case gamedataItemType.Prt_TorsoFabricEnhancer:
      case gamedataItemType.Prt_FabricEnhancer:
        return n"UIIcon.LootingShadow_Material";
      case gamedataItemType.Prt_Fragment:
        return n"UIIcon.LootingShadow_Fragment";
      case gamedataItemType.Prt_Magazine:
        return n"UIIcon.LootingShadow_Magazine";
      case gamedataItemType.Prt_ThrowableMod:
      case gamedataItemType.Prt_BluntMod:
      case gamedataItemType.Prt_BladeMod:
      case gamedataItemType.Prt_MeleeMod:
      case gamedataItemType.Prt_ShotgunMod:
      case gamedataItemType.Prt_Precision_Sniper_RifleMod:
      case gamedataItemType.Prt_HandgunMod:
      case gamedataItemType.Prt_AR_SMG_LMGMod:
      case gamedataItemType.Prt_SmartMod:
      case gamedataItemType.Prt_TechMod:
      case gamedataItemType.Prt_PowerMod:
      case gamedataItemType.Prt_RangedMod:
      case gamedataItemType.Prt_Mod:
        return n"UIIcon.LootingShadow_Mod";
      case gamedataItemType.Prt_RifleMuzzle:
      case gamedataItemType.Prt_HandgunMuzzle:
      case gamedataItemType.Prt_Muzzle:
        return n"UIIcon.LootingShadow_Silencer";
      case gamedataItemType.Prt_Program:
        return n"UIIcon.LootingShadow_Program";
      case gamedataItemType.Prt_Receiver:
        return n"UIIcon.Lootinghadow_Receiver";
      case gamedataItemType.Prt_PowerSniperScope:
      case gamedataItemType.Prt_TechSniperScope:
      case gamedataItemType.Prt_LongScope:
      case gamedataItemType.Prt_ShortScope:
      case gamedataItemType.Prt_Scope:
        return n"UIIcon.LootingShadow_Scope";
      case gamedataItemType.Prt_ScopeRail:
        return n"UIIcon.LootingShadow_ScopeRail";
      case gamedataItemType.Prt_Stock:
        return n"UIIcon.LootingShadow_Stock";
      case gamedataItemType.Prt_TargetingSystem:
        return n"UIIcon.LootingShadow_TargetingSystem";
      case gamedataItemType.Con_Inhaler:
        return n"UIIcon.LootingShadow_Inhaler";
      case gamedataItemType.Con_Injector:
        return n"UIIcon.LootingShadow_Injector";
      case gamedataItemType.Gen_Readable:
        return n"UIIcon.LootingShadow_Shard";
      case gamedataItemType.Gen_Junk:
        return n"UIIcon.LootingShadow_Junk";
      case gamedataItemType.Gen_Jewellery:
        return n"UIIcon.LootingShadow_Junk";
      case gamedataItemType.Wea_AssaultRifle:
        return n"UIIcon.WeaponTypeIcon_AssaultRifle";
      case gamedataItemType.Wea_Axe:
        return n"UIIcon.WeaponTypeIcon_Axe";
      case gamedataItemType.Wea_Chainsword:
        return n"UIIcon.WeaponTypeIcon_Chainsword";
      case gamedataItemType.Wea_Fists:
        return n"UIIcon.WeaponTypeIcon_Fists";
      case gamedataItemType.Wea_Hammer:
        return n"UIIcon.WeaponTypeIcon_Hammer";
      case gamedataItemType.Wea_Handgun:
        return n"UIIcon.WeaponTypeIcon_Handgun";
      case gamedataItemType.Wea_HeavyMachineGun:
        return n"UIIcon.WeaponTypeIcon_HeavyMachineGun";
      case gamedataItemType.Wea_Katana:
        return n"UIIcon.WeaponTypeIcon_Katana";
      case gamedataItemType.Wea_Sword:
        return n"UIIcon.WeaponTypeIcon_Sword";
      case gamedataItemType.Wea_Knife:
        return n"UIIcon.WeaponTypeIcon_Knife";
      case gamedataItemType.Wea_LightMachineGun:
        return n"UIIcon.WeaponTypeIcon_LightMachineGun";
      case gamedataItemType.Wea_LongBlade:
        return n"UIIcon.WeaponTypeIcon_LongBlade";
      case gamedataItemType.Wea_Machete:
        return n"UIIcon.WeaponTypeIcon_Machete";
      case gamedataItemType.Wea_Melee:
        return n"UIIcon.WeaponTypeIcon_Melee";
      case gamedataItemType.Wea_OneHandedClub:
        return n"UIIcon.WeaponTypeIcon_OneHandedClub";
      case gamedataItemType.Wea_PrecisionRifle:
        return n"UIIcon.WeaponTypeIcon_PrecisionRifle";
      case gamedataItemType.Wea_Revolver:
        return n"UIIcon.WeaponTypeIcon_Revolver";
      case gamedataItemType.Wea_Rifle:
        return n"UIIcon.WeaponTypeIcon_Rifle";
      case gamedataItemType.Wea_ShortBlade:
        return n"UIIcon.WeaponTypeIcon_ShortBlade";
      case gamedataItemType.Wea_Shotgun:
        return n"UIIcon.WeaponTypeIcon_Shotgun";
      case gamedataItemType.Wea_ShotgunDual:
        return n"UIIcon.WeaponTypeIcon_ShotgunDual";
      case gamedataItemType.Wea_SniperRifle:
        return n"UIIcon.WeaponTypeIcon_SniperRifle";
      case gamedataItemType.Wea_SubmachineGun:
        return n"UIIcon.WeaponTypeIcon_SubmachineGun";
      case gamedataItemType.Wea_TwoHandedClub:
        return n"UIIcon.WeaponTypeIcon_TwoHandedClub";
      case gamedataItemType.Gen_CraftingMaterial:
        return n"UIIcon.LootingShadow_Material";
      case gamedataItemType.Con_Skillbook:
        return n"UIIcon.LootingShadow_Shard";
    };
    return UIItemsHelper.GetLootingShadowIcon(equipmentArea);
  }

  public final static func GetLootingShadowIcon(equipmentArea: gamedataEquipmentArea) -> CName {
    switch equipmentArea {
      case gamedataEquipmentArea.Consumable:
        return n"UIIcon.LootingShadow_Consumable";
      case gamedataEquipmentArea.SystemReplacementCW:
      case gamedataEquipmentArea.NervousSystemCW:
      case gamedataEquipmentArea.MusculoskeletalSystemCW:
      case gamedataEquipmentArea.LegsCW:
      case gamedataEquipmentArea.IntegumentarySystemCW:
      case gamedataEquipmentArea.ImmuneSystemCW:
      case gamedataEquipmentArea.HandsCW:
      case gamedataEquipmentArea.FrontalCortexCW:
      case gamedataEquipmentArea.EyesCW:
      case gamedataEquipmentArea.CardiovascularSystemCW:
      case gamedataEquipmentArea.AbilityCW:
        return n"UIIcon.LootingShadow_Cyberware";
      case gamedataEquipmentArea.Face:
        return n"UIIcon.LootingShadow_Face";
      case gamedataEquipmentArea.Feet:
        return n"UIIcon.LootingShadow_Feet";
      case gamedataEquipmentArea.Gadget:
      case gamedataEquipmentArea.QuickSlot:
        return n"UIIcon.LootingShadow_Grenade";
      case gamedataEquipmentArea.Head:
        return n"UIIcon.LootingShadow_Head";
      case gamedataEquipmentArea.InnerChest:
        return n"UIIcon.LootingShadow_InnerChest";
      case gamedataEquipmentArea.Legs:
        return n"UIIcon.LootingShadow_Legs";
      case gamedataEquipmentArea.OuterChest:
        return n"UIIcon.LootingShadow_OuterChest";
      case gamedataEquipmentArea.Outfit:
        return n"UIIcon.LootingShadow_Outfit";
      case gamedataEquipmentArea.Weapon:
        return n"UIIcon.LootingShadow_Weapon";
    };
    return n"UIIcon.LootingShadow_Default";
  }

  public final static func GetSlotName(slotID: TweakDBID, itemType: gamedataItemType, equipmentArea: gamedataEquipmentArea) -> String {
    switch slotID {
      case t"AttachmentSlots.Scope":
        return "Gameplay-Items-Item Type-Prt_Scope";
      case t"AttachmentSlots.PowerModule":
        return "Gameplay-Items-Item Type-Prt_Muzzle";
      case t"AttachmentSlots.Magazine":
        return "Gameplay-Items-Item Type-Prt_Magazine";
      case t"AttachmentSlots.Throwable_WeaponMod2_Collectible":
      case t"AttachmentSlots.Throwable_WeaponMod1_Collectible":
      case t"AttachmentSlots.Throwable_WeaponMod2":
      case t"AttachmentSlots.Throwable_WeaponMod1":
      case t"AttachmentSlots.Blunt_WeaponMod2_Collectible":
      case t"AttachmentSlots.Blunt_WeaponMod1_Collectible":
      case t"AttachmentSlots.Blunt_WeaponMod2":
      case t"AttachmentSlots.Blunt_WeaponMod1":
      case t"AttachmentSlots.Blade_WeaponMod2_Collectible":
      case t"AttachmentSlots.Blade_WeaponMod1_Collectible":
      case t"AttachmentSlots.Blade_WeaponMod2":
      case t"AttachmentSlots.Blade_WeaponMod1":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod2_Collectible":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod1_Collectible":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod2":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod1":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod2_Collectible":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod1_Collectible":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod2":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod1":
      case t"AttachmentSlots.Power_Precision_Sniper_Rifle_WeaponMod2":
      case t"AttachmentSlots.Power_Precision_Sniper_Rifle_WeaponMod1":
      case t"AttachmentSlots.Smart_Shotgun_WeaponMod2":
      case t"AttachmentSlots.Smart_Shotgun_WeaponMod1":
      case t"AttachmentSlots.Tech_Shotgun_WeaponMod2":
      case t"AttachmentSlots.Tech_Shotgun_WeaponMod1":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod2_Collectible":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod1_Collectible":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod2":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod1":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod2_Collectible":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod1_Collectible":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod2":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod1":
      case t"AttachmentSlots.Tech_Handgun_WeaponMod2":
      case t"AttachmentSlots.Tech_Handgun_WeaponMod1":
      case t"AttachmentSlots.Power_Handgun_WeaponMod2_Collectible":
      case t"AttachmentSlots.Power_Handgun_WeaponMod1_Collectible":
      case t"AttachmentSlots.Power_Handgun_WeaponMod2":
      case t"AttachmentSlots.Power_Handgun_WeaponMod1":
      case t"AttachmentSlots.Smart_AR_SMG_LMG_WeaponMod2":
      case t"AttachmentSlots.Smart_AR_SMG_LMG_WeaponMod1":
      case t"AttachmentSlots.Tech_AR_SMG_LMG_WeaponMod2":
      case t"AttachmentSlots.Tech_AR_SMG_LMG_WeaponMod1":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod2_Collectible":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod1_Collectible":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod2":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod1":
        return "Gameplay-Items-Item Type-Prt_Mod";
      case t"AttachmentSlots.NanoWiresQuickhackSlot":
      case t"AttachmentSlots.CyberdeckProgram8":
      case t"AttachmentSlots.CyberdeckProgram7":
      case t"AttachmentSlots.CyberdeckProgram6":
      case t"AttachmentSlots.CyberdeckProgram5":
      case t"AttachmentSlots.CyberdeckProgram4":
      case t"AttachmentSlots.CyberdeckProgram3":
      case t"AttachmentSlots.CyberdeckProgram2":
      case t"AttachmentSlots.CyberdeckProgram1":
        return "Gameplay-Items-Item Type-Prt_Program";
      case t"AttachmentSlots.ArmsCyberwareGeneralSlot":
      case t"AttachmentSlots.ProjectileLauncherWiring":
      case t"AttachmentSlots.ProjectileLauncherRound":
      case t"AttachmentSlots.NanoWiresBattery":
      case t"AttachmentSlots.NanoWiresCable":
      case t"AttachmentSlots.MantisBladesRotor":
      case t"AttachmentSlots.MantisBladesEdge":
      case t"AttachmentSlots.StrongArmsBattery":
      case t"AttachmentSlots.StrongArmsKnuckles":
      case t"AttachmentSlots.KiroshiOpticsSlot3":
      case t"AttachmentSlots.KiroshiOpticsSlot2":
      case t"AttachmentSlots.KiroshiOpticsSlot1":
      case t"AttachmentSlots.BerserkSlot3":
      case t"AttachmentSlots.BerserkSlot2":
      case t"AttachmentSlots.BerserkSlot1":
      case t"AttachmentSlots.SandevistanSlot3":
      case t"AttachmentSlots.SandevistanSlot2":
      case t"AttachmentSlots.SandevistanSlot1":
        return "Gameplay-Items-Item Type-Prt_Fragment";
      case t"AttachmentSlots.FootFabricEnhancer3":
      case t"AttachmentSlots.FootFabricEnhancer2":
      case t"AttachmentSlots.FootFabricEnhancer1":
      case t"AttachmentSlots.LegsFabricEnhancer3":
      case t"AttachmentSlots.LegsFabricEnhancer2":
      case t"AttachmentSlots.LegsFabricEnhancer1":
      case t"AttachmentSlots.OuterChestFabricEnhancer3":
      case t"AttachmentSlots.OuterChestFabricEnhancer2":
      case t"AttachmentSlots.OuterChestFabricEnhancer1":
      case t"AttachmentSlots.InnerChestFabricEnhancer3":
      case t"AttachmentSlots.InnerChestFabricEnhancer2":
      case t"AttachmentSlots.InnerChestFabricEnhancer1":
      case t"AttachmentSlots.FaceFabricEnhancer3":
      case t"AttachmentSlots.FaceFabricEnhancer2":
      case t"AttachmentSlots.FaceFabricEnhancer1":
      case t"AttachmentSlots.HeadFabricEnhancer3":
      case t"AttachmentSlots.HeadFabricEnhancer2":
      case t"AttachmentSlots.HeadFabricEnhancer1":
        return "Gameplay-Items-Item Type-Prt_FabricEnhancer";
    };
    return UIItemsHelper.GetSlotName(itemType, equipmentArea);
  }

  public final static func GetSlotName(itemType: gamedataItemType, equipmentArea: gamedataEquipmentArea) -> String {
    switch itemType {
      case gamedataItemType.Prt_Capacitor:
        return "Gameplay-Items-Item Type-Prt_Capacitor";
      case gamedataItemType.Prt_FabricEnhancer:
        return "Gameplay-Items-Item Type-Prt_FabricEnhancer";
      case gamedataItemType.Prt_Fragment:
        return "Gameplay-Items-Item Type-Prt_Fragment";
      case gamedataItemType.Prt_Magazine:
        return "Gameplay-Items-Item Type-Prt_Magazine";
      case gamedataItemType.Prt_Mod:
        return "Gameplay-Items-Item Type-Prt_Mod";
      case gamedataItemType.Prt_Muzzle:
        return "Gameplay-Items-Item Type-Prt_Muzzle";
      case gamedataItemType.Prt_Program:
        return "Gameplay-Items-Item Type-Prt_Program";
      case gamedataItemType.Prt_Receiver:
        return "Gameplay-Items-Item Type-Prt_Receiver";
      case gamedataItemType.Prt_Scope:
        return "Gameplay-Items-Item Type-Prt_Scope";
      case gamedataItemType.Prt_ScopeRail:
        return "Gameplay-Items-Item Type-Prt_ScopeRail";
      case gamedataItemType.Prt_Stock:
        return "Gameplay-Items-Item Type-Prt_Stock";
      case gamedataItemType.Prt_TargetingSystem:
        return "Gameplay-Items-Item Type-Prt_TargetingSystem";
    };
    return UIItemsHelper.GetSlotName(equipmentArea);
  }

  public final static func GetSlotName(equipmentArea: gamedataEquipmentArea) -> String {
    switch equipmentArea {
      case gamedataEquipmentArea.Consumable:
        return "UI-Inventory-Tooltips-ConsumablesDescription";
      case gamedataEquipmentArea.AbilityCW:
        return "UI-Inventory-Tooltips-CyberwareDescription";
      case gamedataEquipmentArea.Face:
        return "UI-Inventory-Tooltips-FaceDescription";
      case gamedataEquipmentArea.Feet:
        return "UI-Inventory-Tooltips-FeetDescription";
      case gamedataEquipmentArea.Gadget:
      case gamedataEquipmentArea.QuickSlot:
        return "UI-Inventory-Tooltips-GadgetsDescription";
      case gamedataEquipmentArea.Head:
        return "UI-Inventory-Tooltips-HeadDescription";
      case gamedataEquipmentArea.InnerChest:
        return "UI-Inventory-Tooltips-InnerChestDescription";
      case gamedataEquipmentArea.Legs:
        return "UI-Inventory-Tooltips-LegsDescription";
      case gamedataEquipmentArea.OuterChest:
        return "UI-Inventory-Tooltips-OuterChestDescription";
      case gamedataEquipmentArea.Outfit:
        return "UI-Inventory-Tooltips-OutfitDescription";
      case gamedataEquipmentArea.Weapon:
        return "UI-Inventory-Tooltips-WeaponDescription";
    };
    return "";
  }

  public final static func GetItemTypeKey(itemData: wref<gameItemData>, equipmentArea: gamedataEquipmentArea, itemID: TweakDBID, itemType: gamedataItemType, weaponEvolutionType: gamedataWeaponEvolution) -> String {
    return UIItemsHelper.GetItemTypeKey(equipmentArea, itemID, itemType, weaponEvolutionType);
  }

  public final static func GetItemTypeKey(equipmentArea: gamedataEquipmentArea, itemID: TweakDBID, itemType: gamedataItemType, weaponEvolutionType: gamedataWeaponEvolution) -> String {
    switch equipmentArea {
      case gamedataEquipmentArea.SystemReplacementCW:
      case gamedataEquipmentArea.NervousSystemCW:
      case gamedataEquipmentArea.MusculoskeletalSystemCW:
      case gamedataEquipmentArea.LegsCW:
      case gamedataEquipmentArea.IntegumentarySystemCW:
      case gamedataEquipmentArea.ImmuneSystemCW:
      case gamedataEquipmentArea.HandsCW:
      case gamedataEquipmentArea.FrontalCortexCW:
      case gamedataEquipmentArea.EyesCW:
      case gamedataEquipmentArea.CardiovascularSystemCW:
      case gamedataEquipmentArea.ArmsCW:
      case gamedataEquipmentArea.AbilityCW:
        return "UI-Inventory-Tooltips-CyberwareDescription";
    };
    return UIItemsHelper.GetItemTypeKey(itemID, itemType, weaponEvolutionType);
  }

  public final static func GetItemTypeKey(itemID: TweakDBID, itemType: gamedataItemType, weaponEvolutionType: gamedataWeaponEvolution) -> String {
    switch itemID {
      case t"Items.money":
        return "UI-ItemLabel-Money";
    };
    return UIItemsHelper.GetItemTypeKey(itemType, weaponEvolutionType);
  }

  public final static func GetItemTypeKey(itemType: gamedataItemType, weaponEvolutionType: gamedataWeaponEvolution) -> String {
    let keySuffix: String;
    switch weaponEvolutionType {
      case gamedataWeaponEvolution.Power:
        keySuffix = "_Power";
        break;
      case gamedataWeaponEvolution.Smart:
        keySuffix = "_Smart";
        break;
      case gamedataWeaponEvolution.Tech:
        keySuffix = "_Tech";
    };
    if IsStringValid(keySuffix) {
      return UIItemsHelper.GetEvolutionWeaponType(itemType) + keySuffix;
    };
    return UIItemsHelper.GetItemTypeKey(itemType);
  }

  public final static func GetEvolutionWeaponType(itemType: gamedataItemType) -> String {
    switch itemType {
      case gamedataItemType.Wea_AssaultRifle:
        return "UI-WeaponItemType-Wea_AssaultRifle";
      case gamedataItemType.Wea_Axe:
        return "UI-WeaponItemType-Wea_Axe";
      case gamedataItemType.Wea_Chainsword:
        return "UI-WeaponItemType-Wea_Chainsword";
      case gamedataItemType.Wea_Fists:
        return "UI-WeaponItemType-Wea_Fists";
      case gamedataItemType.Wea_Hammer:
        return "UI-WeaponItemType-Wea_Hammer";
      case gamedataItemType.Wea_Handgun:
        return "UI-WeaponItemType-Wea_Handgun";
      case gamedataItemType.Wea_HeavyMachineGun:
        return "UI-WeaponItemType-Wea_HeavyMachineGun";
      case gamedataItemType.Wea_Katana:
        return "UI-WeaponItemType-Wea_Katana";
      case gamedataItemType.Wea_Sword:
        return "UI-WeaponItemType-Wea_Sword";
      case gamedataItemType.Wea_Knife:
        return "UI-WeaponItemType-Wea_Knife";
      case gamedataItemType.Wea_LightMachineGun:
        return "UI-WeaponItemType-Wea_LightMachineGun";
      case gamedataItemType.Wea_LongBlade:
        return "UI-WeaponItemType-Wea_LongBlade";
      case gamedataItemType.Wea_Machete:
        return "UI-WeaponItemType-Wea_Machete";
      case gamedataItemType.Wea_Melee:
        return "UI-WeaponItemType-Wea_Melee";
      case gamedataItemType.Wea_OneHandedClub:
        return "UI-WeaponItemType-Wea_OneHandedClub";
      case gamedataItemType.Wea_PrecisionRifle:
        return "UI-WeaponItemType-Wea_PrecisionRifle";
      case gamedataItemType.Wea_Revolver:
        return "UI-WeaponItemType-Wea_Revolver";
      case gamedataItemType.Wea_Rifle:
        return "UI-WeaponItemType-Wea_Rifle";
      case gamedataItemType.Wea_ShortBlade:
        return "UI-WeaponItemType-Wea_ShortBlade";
      case gamedataItemType.Wea_Shotgun:
        return "UI-WeaponItemType-Wea_Shotgun";
      case gamedataItemType.Wea_ShotgunDual:
        return "UI-WeaponItemType-Wea_ShotgunDual";
      case gamedataItemType.Wea_SniperRifle:
        return "UI-WeaponItemType-Wea_SniperRifle";
      case gamedataItemType.Wea_SubmachineGun:
        return "UI-WeaponItemType-Wea_SubmachineGun";
      case gamedataItemType.Wea_TwoHandedClub:
        return "UI-WeaponItemType-Wea_TwoHandedClub";
    };
    return "MISSING KEY";
  }

  public final static func GetItemTypeKey(itemType: gamedataItemType) -> String {
    switch itemType {
      case gamedataItemType.Clo_Face:
        return "Gameplay-Items-Item Type-Clo_Face";
      case gamedataItemType.Clo_Feet:
        return "Gameplay-Items-Item Type-Clo_Feet";
      case gamedataItemType.Clo_Head:
        return "Gameplay-Items-Item Type-Clo_Head";
      case gamedataItemType.Clo_InnerChest:
        return "Gameplay-Items-Item Type-Clo_InnerChest";
      case gamedataItemType.Clo_Legs:
        return "Gameplay-Items-Item Type-Clo_Legs";
      case gamedataItemType.Clo_OuterChest:
        return "Gameplay-Items-Item Type-Clo_OuterChest";
      case gamedataItemType.Clo_Outfit:
        return "Gameplay-Items-Item Type-Clo_Outfit";
      case gamedataItemType.Con_Ammo:
        return "Gameplay-RPG-Items-Types-Con_Ammo";
      case gamedataItemType.Con_Edible:
        return "Gameplay-Items-Item Type-Con_Edible";
      case gamedataItemType.Con_Inhaler:
        return "Gameplay-Items-Item Type-Con_Inhaler";
      case gamedataItemType.Con_Injector:
        return "Gameplay-Items-Item Type-Con_Injector";
      case gamedataItemType.Con_LongLasting:
        return "Gameplay-Items-Item Type-Con_LongLasting";
      case gamedataItemType.Con_Skillbook:
        return "Gameplay-Items-Item Type-Con_Skillbook";
      case gamedataItemType.Cyb_Ability:
        return "Gameplay-RPG-Items-Types-Cyb_Ability";
      case gamedataItemType.Cyb_Launcher:
        return "Gameplay-Items-Item Type-Cyb_Launcher";
      case gamedataItemType.Cyb_MantisBlades:
        return "Gameplay-Items-Item Type-Cyb_MantisBlades";
      case gamedataItemType.Cyb_NanoWires:
        return "Gameplay-Items-Item Type-Cyb_NanoWires";
      case gamedataItemType.Cyb_StrongArms:
        return "Gameplay-Items-Item Type-Cyb_StrongArms";
      case gamedataItemType.Fla_Launcher:
        return "MISSING KEY";
      case gamedataItemType.Fla_Rifle:
        return "MISSING KEY";
      case gamedataItemType.Fla_Shock:
        return "MISSING KEY";
      case gamedataItemType.Fla_Support:
        return "MISSING KEY";
      case gamedataItemType.Gad_Grenade:
        return "Gameplay-RPG-Items-Types-Gad_Grenade";
      case gamedataItemType.Gen_CraftingMaterial:
        return "Gameplay-Items-Item Type-Gen_CraftingMaterial";
      case gamedataItemType.Gen_Junk:
        return "Gameplay-Items-Item Type-Gen_Junk";
      case gamedataItemType.Gen_Jewellery:
        return "Gameplay-Items-Item Type-Gen_Jewellery";
      case gamedataItemType.Gen_Keycard:
        return "Gameplay-Items-Item Type-Gen_Keycard";
      case gamedataItemType.Gen_Misc:
        return "Gameplay-Items-Item Type-Gen_Misc";
      case gamedataItemType.Gen_Tarot:
        return "UI-MappinTypes-Tarot";
      case gamedataItemType.Gen_Readable:
        return "Gameplay-Items-Item Type-Gen_Readable";
      case gamedataItemType.GrenadeDelivery:
        return "Gameplay-Items-Item Type-Prt_DeliveryMethod";
      case gamedataItemType.Grenade_Core:
        return "Gameplay-Items-Item Type-Prt_GrenadeCore";
      case gamedataItemType.Prt_Capacitor:
        return "Gameplay-Items-Item Type-Prt_Capacitor";
      case gamedataItemType.Prt_FabricEnhancer:
        return "Gameplay-Items-Item Type-Prt_FabricEnhancer";
      case gamedataItemType.Prt_HeadFabricEnhancer:
        return "Gameplay-Items-Item Type-Prt_HeadFabricEnhancer";
      case gamedataItemType.Prt_FaceFabricEnhancer:
        return "Gameplay-Items-Item Type-Prt_FaceFabricEnhancer";
      case gamedataItemType.Prt_TorsoFabricEnhancer:
        return "Gameplay-Items-Item Type-Prt_TorsoFabricEnhancer";
      case gamedataItemType.Prt_OuterTorsoFabricEnhancer:
        return "Gameplay-Items-Item Type-Prt_OuterTorsoFabricEnhancer";
      case gamedataItemType.Prt_PantsFabricEnhancer:
        return "Gameplay-Items-Item Type-Prt_PantsFabricEnhancer";
      case gamedataItemType.Prt_BootsFabricEnhancer:
        return "Gameplay-Items-Item Type-Prt_BootsFabricEnhancer";
      case gamedataItemType.Prt_Fragment:
        return "Gameplay-Items-Item Type-Prt_Fragment";
      case gamedataItemType.Prt_Magazine:
        return "Gameplay-Items-Item Type-Prt_Magazine";
      case gamedataItemType.Prt_Mod:
        return "Gameplay-Items-Item Type-Prt_Mod";
      case gamedataItemType.Prt_RangedMod:
        return "Gameplay-Items-Item Type-Prt_RangedMod";
      case gamedataItemType.Prt_PowerMod:
        return "Gameplay-Items-Item Type-Prt_PowerMod";
      case gamedataItemType.Prt_TechMod:
        return "Gameplay-Items-Item Type-Prt_TechMod";
      case gamedataItemType.Prt_SmartMod:
        return "Gameplay-Items-Item Type-Prt_SmartMod";
      case gamedataItemType.Prt_AR_SMG_LMGMod:
        return "Gameplay-Items-Item Type-Prt_AR_SMG_LMGMod";
      case gamedataItemType.Prt_HandgunMod:
        return "Gameplay-Items-Item Type-Prt_HandgunMod";
      case gamedataItemType.Prt_Precision_Sniper_RifleMod:
        return "Gameplay-Items-Item Type-Prt_Precision_Sniper_RifleMod";
      case gamedataItemType.Prt_ShotgunMod:
        return "Gameplay-Items-Item Type-Prt_ShotgunMod";
      case gamedataItemType.Prt_MeleeMod:
        return "Gameplay-Items-Item Type-Prt_MeleeMod";
      case gamedataItemType.Prt_BladeMod:
        return "Gameplay-Items-Item Type-Prt_BladeMod";
      case gamedataItemType.Prt_BluntMod:
        return "Gameplay-Items-Item Type-Prt_BluntMod";
      case gamedataItemType.Prt_ThrowableMod:
        return "Gameplay-Items-Item Type-Prt_ThrowableMod";
      case gamedataItemType.Prt_Muzzle:
        return "UI-ResourceExports-Silencer";
      case gamedataItemType.Prt_HandgunMuzzle:
        return "Gameplay-Items-Item Type-Prt_HandgunMuzzle";
      case gamedataItemType.Prt_RifleMuzzle:
        return "Gameplay-Items-Item Type-Prt_RifleMuzzle";
      case gamedataItemType.Prt_Program:
        return "Gameplay-Items-Item Type-Prt_Program";
      case gamedataItemType.Prt_Receiver:
        return "Gameplay-Items-Item Type-Prt_Receiver";
      case gamedataItemType.Prt_Scope:
        return "Gameplay-Items-Item Type-Prt_Scope";
      case gamedataItemType.Prt_ShortScope:
        return "Gameplay-Items-Item Type-Prt_ShortScope_2";
      case gamedataItemType.Prt_LongScope:
        return "Gameplay-Items-Item Type-Prt_LongScope_2";
      case gamedataItemType.Prt_TechSniperScope:
        return "Gameplay-Items-Item Type-Prt_TechSniperScope";
      case gamedataItemType.Prt_PowerSniperScope:
        return "Gameplay-Items-Item Type-Prt_PowerSniperScope";
      case gamedataItemType.Prt_ScopeRail:
        return "Gameplay-Items-Item Type-Prt_ScopeRail";
      case gamedataItemType.Prt_Stock:
        return "Gameplay-Items-Item Type-Prt_Stock";
      case gamedataItemType.Prt_TargetingSystem:
        return "Gameplay-Items-Item Type-Prt_TargetingSystem";
      case gamedataItemType.Wea_AssaultRifle:
        return "Gameplay-RPG-Items-Types-Wea_AssaultRifle";
      case gamedataItemType.Wea_Axe:
        return "Gameplay-Items-Item Type-Wea_Axe";
      case gamedataItemType.Wea_Chainsword:
        return "Gameplay-Items-Item Type-Wea_Chainsword";
      case gamedataItemType.Wea_Fists:
        return "Gameplay-RPG-Items-Types-Wea_Fists";
      case gamedataItemType.Wea_Hammer:
        return "Gameplay-RPG-Items-Types-Wea_Hammer";
      case gamedataItemType.Wea_Handgun:
        return "Gameplay-RPG-Items-Types-Wea_Handgun";
      case gamedataItemType.Wea_HeavyMachineGun:
        return "Gameplay-Items-Item Type-Wea_HeavyMachineGun";
      case gamedataItemType.Wea_Katana:
        return "Gameplay-RPG-Items-Types-Wea_Katana";
      case gamedataItemType.Wea_Sword:
        return "Gameplay-Items-Item Type-Wea_Sword";
      case gamedataItemType.Wea_Knife:
        return "Gameplay-RPG-Items-Types-Wea_Knife";
      case gamedataItemType.Wea_LightMachineGun:
        return "Gameplay-RPG-Items-Types-Wea_LightMachineGun";
      case gamedataItemType.Wea_LongBlade:
        return "Gameplay-RPG-Items-Types-Wea_Knife";
      case gamedataItemType.Wea_Machete:
        return "Gameplay-Items-Item Type-Wea_Machete";
      case gamedataItemType.Wea_Melee:
        return "Gameplay-RPG-Items-Types-Wea_Melee";
      case gamedataItemType.Wea_OneHandedClub:
        return "Gameplay-RPG-Items-Types-Wea_OneHandedClub";
      case gamedataItemType.Wea_PrecisionRifle:
        return "Gameplay-RPG-Items-Types-Wea_PrecisionRifle";
      case gamedataItemType.Wea_Revolver:
        return "Gameplay-RPG-Items-Types-Wea_Revolver";
      case gamedataItemType.Wea_Rifle:
        return "Gameplay-RPG-Items-Types-Wea_Rifle";
      case gamedataItemType.Wea_ShortBlade:
        return "Gameplay-RPG-Items-Types-Wea_ShortBlade";
      case gamedataItemType.Wea_Shotgun:
        return "Gameplay-RPG-Items-Types-Wea_Shotgun";
      case gamedataItemType.Wea_ShotgunDual:
        return "Gameplay-RPG-Items-Types-Wea_ShotgunDual";
      case gamedataItemType.Wea_SniperRifle:
        return "Gameplay-RPG-Items-Types-Wea_SniperRifle";
      case gamedataItemType.Wea_SubmachineGun:
        return "Gameplay-RPG-Items-Types-Wea_SubmachineGun";
      case gamedataItemType.Wea_TwoHandedClub:
        return "Gameplay-RPG-Items-Types-Wea_TwoHandedClub";
    };
    return "";
  }

  public final static func GetEmptySlotName(slotId: TweakDBID) -> String {
    switch slotId {
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod2_Collectible":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod1_Collectible":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod2":
      case t"AttachmentSlots.Smart_Precision_Sniper_Rifle_WeaponMod1":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod2_Collectible":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod1_Collectible":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod2":
      case t"AttachmentSlots.Tech_Precision_Sniper_Rifle_WeaponMod1":
      case t"AttachmentSlots.Power_Precision_Sniper_Rifle_WeaponMod2":
      case t"AttachmentSlots.Power_Precision_Sniper_Rifle_WeaponMod1":
      case t"AttachmentSlots.Smart_Shotgun_WeaponMod2":
      case t"AttachmentSlots.Smart_Shotgun_WeaponMod1":
      case t"AttachmentSlots.Tech_Shotgun_WeaponMod2":
      case t"AttachmentSlots.Tech_Shotgun_WeaponMod1":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod2_Collectible":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod1_Collectible":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod2":
      case t"AttachmentSlots.Power_Shotgun_WeaponMod1":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod2_Collectible":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod1_Collectible":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod2":
      case t"AttachmentSlots.Smart_Handgun_WeaponMod1":
      case t"AttachmentSlots.Tech_Handgun_WeaponMod2":
      case t"AttachmentSlots.Tech_Handgun_WeaponMod1":
      case t"AttachmentSlots.Power_Handgun_WeaponMod2_Collectible":
      case t"AttachmentSlots.Power_Handgun_WeaponMod1_Collectible":
      case t"AttachmentSlots.Power_Handgun_WeaponMod2":
      case t"AttachmentSlots.Power_Handgun_WeaponMod1":
      case t"AttachmentSlots.Smart_AR_SMG_LMG_WeaponMod2":
      case t"AttachmentSlots.Smart_AR_SMG_LMG_WeaponMod1":
      case t"AttachmentSlots.Tech_AR_SMG_LMG_WeaponMod2":
      case t"AttachmentSlots.Tech_AR_SMG_LMG_WeaponMod1":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod2_Collectible":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod1_Collectible":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod2":
      case t"AttachmentSlots.Power_AR_SMG_LMG_WeaponMod1":
        return "UI-Labels-EmptyModSlot";
      case t"AttachmentSlots.Throwable_WeaponMod2_Collectible":
      case t"AttachmentSlots.Throwable_WeaponMod1_Collectible":
      case t"AttachmentSlots.Throwable_WeaponMod2":
      case t"AttachmentSlots.Throwable_WeaponMod1":
      case t"AttachmentSlots.Blunt_WeaponMod2_Collectible":
      case t"AttachmentSlots.Blunt_WeaponMod1_Collectible":
      case t"AttachmentSlots.Blunt_WeaponMod2":
      case t"AttachmentSlots.Blunt_WeaponMod1":
      case t"AttachmentSlots.Blade_WeaponMod2_Collectible":
      case t"AttachmentSlots.Blade_WeaponMod1_Collectible":
      case t"AttachmentSlots.Blade_WeaponMod2":
      case t"AttachmentSlots.Blade_WeaponMod1":
        return "UI-Labels-EmptyMeleeModSlot";
      case t"AttachmentSlots.Scope":
        return "UI-Labels-EmptyScopeSlot";
      case t"AttachmentSlots.PowerModule":
        return "UI-Labels-EmptyMuzzleSlot";
      case t"AttachmentSlots.FootFabricEnhancer3":
      case t"AttachmentSlots.FootFabricEnhancer2":
      case t"AttachmentSlots.FootFabricEnhancer1":
      case t"AttachmentSlots.LegsFabricEnhancer3":
      case t"AttachmentSlots.LegsFabricEnhancer2":
      case t"AttachmentSlots.LegsFabricEnhancer1":
      case t"AttachmentSlots.OuterChestFabricEnhancer4":
      case t"AttachmentSlots.OuterChestFabricEnhancer3":
      case t"AttachmentSlots.OuterChestFabricEnhancer2":
      case t"AttachmentSlots.OuterChestFabricEnhancer1":
      case t"AttachmentSlots.InnerChestFabricEnhancer4":
      case t"AttachmentSlots.InnerChestFabricEnhancer3":
      case t"AttachmentSlots.InnerChestFabricEnhancer2":
      case t"AttachmentSlots.InnerChestFabricEnhancer1":
      case t"AttachmentSlots.FaceFabricEnhancer3":
      case t"AttachmentSlots.FaceFabricEnhancer2":
      case t"AttachmentSlots.FaceFabricEnhancer1":
      case t"AttachmentSlots.HeadFabricEnhancer3":
      case t"AttachmentSlots.HeadFabricEnhancer2":
      case t"AttachmentSlots.HeadFabricEnhancer1":
        return "UI-Labels-EmptyClothingModSlot";
      case t"AttachmentSlots.NanoWiresQuickhackSlot":
      case t"AttachmentSlots.CyberdeckProgram8":
      case t"AttachmentSlots.CyberdeckProgram7":
      case t"AttachmentSlots.CyberdeckProgram6":
      case t"AttachmentSlots.CyberdeckProgram5":
      case t"AttachmentSlots.CyberdeckProgram4":
      case t"AttachmentSlots.CyberdeckProgram3":
      case t"AttachmentSlots.CyberdeckProgram2":
      case t"AttachmentSlots.CyberdeckProgram1":
        return "UI-Labels-EmptyProgramSlot";
      case t"AttachmentSlots.KiroshiOpticsSlot3":
      case t"AttachmentSlots.KiroshiOpticsSlot2":
      case t"AttachmentSlots.KiroshiOpticsSlot1":
      case t"AttachmentSlots.KERSSlot3":
      case t"AttachmentSlots.KERSSlot2":
      case t"AttachmentSlots.KERSSlot1":
      case t"AttachmentSlots.BerserkSlot3":
      case t"AttachmentSlots.BerserkSlot2":
      case t"AttachmentSlots.BerserkSlot1":
      case t"AttachmentSlots.SandevistanSlot3":
      case t"AttachmentSlots.SandevistanSlot2":
      case t"AttachmentSlots.SandevistanSlot1":
        return "UI-Labels-EmptyCyberwareModSlot";
    };
    return "UI-Labels-EmptySlot";
  }

  public final static func GetTooltipItemName(data: ref<InventoryTooltipData>) -> String {
    let id: TweakDBID = ItemID.GetTDBID(InventoryItemData.GetID(data.inventoryItemData));
    let itemData: wref<gameItemData> = InventoryItemData.GetGameItemData(data.inventoryItemData);
    return UIItemsHelper.GetTooltipItemName(id, itemData, data.itemName);
  }

  public final static func GetTooltipItemName(itemID: TweakDBID, itemData: wref<gameItemData>, const fallbackName: script_ref<String>) -> String {
    if itemData.HasTag(n"Shard") {
      return Deref(fallbackName);
    };
    return UIItemsHelper.GetItemName(itemID, itemData);
  }

  public final static func GetItemName(const itemData: script_ref<InventoryItemData>) -> String {
    return UIItemsHelper.GetItemName(ItemID.GetTDBID(InventoryItemData.GetID(itemData)), InventoryItemData.GetGameItemData(itemData));
  }

  public final static func GetItemName(itemID: TweakDBID, itemData: wref<gameItemData>) -> String {
    return UIItemsHelper.GetItemName(TweakDBInterface.GetItemRecord(itemID), itemData);
  }

  public final static func GetItemName(itemRecord: ref<Item_Record>, itemData: wref<gameItemData>) -> String {
    let craftingResult: wref<CraftingResult_Record>;
    let itemName: String;
    let recipeRecord: wref<ItemRecipe_Record> = itemRecord as ItemRecipe_Record;
    if IsDefined(recipeRecord) {
      craftingResult = recipeRecord.CraftingResult();
      if IsDefined(craftingResult) {
        itemName = GetLocalizedItemNameByCName(craftingResult.Item().DisplayName());
      } else {
        itemName = GetLocalizedItemNameByCName(itemRecord.DisplayName());
      };
    } else {
      if IsDefined(itemRecord) {
        itemName = GetLocalizedItemNameByCName(itemRecord.DisplayName());
      };
    };
    return itemName;
  }

  public final static func GetShardName(itemRecord: wref<Item_Record>, gameInstance: GameInstance) -> String {
    let shardData: wref<JournalOnscreen> = UIItemsHelper.GetShardData(itemRecord, gameInstance);
    if IsDefined(shardData) {
      return shardData.GetTitle();
    };
    return "";
  }

  public final static func GetShardData(itemRecord: wref<Item_Record>, gameInstance: GameInstance) -> wref<JournalOnscreen> {
    let isShard: Bool;
    let journalPath: String;
    if !IsDefined(itemRecord) {
      return null;
    };
    isShard = itemRecord.TagsContains(n"Shard");
    if isShard {
      journalPath = TweakDBInterface.GetString(itemRecord.ItemSecondaryAction().GetID() + t".journalEntry", "");
      return GameInstance.GetJournalManager(gameInstance).GetEntryByString(journalPath, "gameJournalOnscreen") as JournalOnscreen;
    };
    return null;
  }

  public final static func IsShardRead(itemTDBID: TweakDBID, gameInstance: GameInstance) -> Bool {
    let journalEntry: wref<JournalEntry>;
    let journalManager: ref<JournalManager>;
    let journalPath: String;
    let isRead: Bool = false;
    let action: wref<ItemAction_Record> = TweakDBInterface.GetItemRecord(itemTDBID).ItemSecondaryAction();
    if IsDefined(action) && GameInstance.IsValid(gameInstance) {
      journalPath = TweakDBInterface.GetString(action.GetID() + t".journalEntry", "");
      journalManager = GameInstance.GetJournalManager(gameInstance);
      if IsDefined(journalManager) {
        journalEntry = journalManager.GetEntryByString(journalPath, "gameJournalOnscreen") as JournalOnscreen;
        isRead = journalManager.IsEntryVisited(journalEntry);
      };
    };
    return isRead;
  }

  public final static func ItemTypeToRarity(itemType: gamedataItemType, opt itemData: wref<gameItemData>) -> RarityItemType {
    switch itemType {
      case gamedataItemType.Prt_Program:
        return RarityItemType.Program;
      case gamedataItemType.Cyberware:
        if IsDefined(itemData) {
          if itemData.HasTag(n"Cyberdeck") {
            return RarityItemType.Cyberdeck;
          };
        };
    };
    return RarityItemType.Item;
  }

  public final static func GetQualityF(const item: script_ref<InventoryItemData>) -> Float {
    let itemData: ref<gameItemData> = InventoryItemData.GetGameItemData(item);
    return UIItemsHelper.GetQualityF(UIItemsHelper.QualityNameToInt(InventoryItemData.GetQuality(item)), RPGManager.IsItemIconic(itemData), RPGManager.GetItemPlus(itemData));
  }

  public final static func GetQualityF(itemData: ref<gameItemData>) -> Float {
    return UIItemsHelper.GetQualityF(UIItemsHelper.QualityToInt(RPGManager.GetItemQuality(itemData)), RPGManager.IsItemIconic(itemData), RPGManager.GetItemPlus(itemData));
  }

  public final static func GetQualityF(qualityInt: Int32, isIconic: Bool, plusValue: Float) -> Float {
    let result: Float = Cast<Float>(qualityInt);
    if isIconic {
      result += 0.05;
    };
    result += plusValue * 0.10;
    return result;
  }

  public final static func ShouldDisplayTier(itemType: gamedataItemType) -> Bool {
    return !(Equals(itemType, gamedataItemType.Clo_Face) || Equals(itemType, gamedataItemType.Clo_Feet) || Equals(itemType, gamedataItemType.Clo_Head) || Equals(itemType, gamedataItemType.Clo_InnerChest) || Equals(itemType, gamedataItemType.Clo_Legs) || Equals(itemType, gamedataItemType.Clo_OuterChest) || Equals(itemType, gamedataItemType.Clo_Outfit) || Equals(itemType, gamedataItemType.Con_Ammo) || Equals(itemType, gamedataItemType.Con_Edible) || Equals(itemType, gamedataItemType.Gen_Keycard) || Equals(itemType, gamedataItemType.Gen_Misc) || Equals(itemType, gamedataItemType.Gen_Tarot) || Equals(itemType, gamedataItemType.Gen_Jewellery) || Equals(itemType, gamedataItemType.Gen_Junk) || Equals(itemType, gamedataItemType.Gen_Readable));
  }

  public final static func IsAttributeAllocationStat(stat: gamedataStatType) -> Bool {
    switch stat {
      case gamedataStatType.HumanityAvailable:
      case gamedataStatType.CoolAvailable:
      case gamedataStatType.TechnicalAbilityAvailable:
      case gamedataStatType.IntelligenceAvailable:
      case gamedataStatType.ReflexesAvailable:
      case gamedataStatType.StrengthAvailable:
      case gamedataStatType.HumanityAllocated:
      case gamedataStatType.CoolAllocated:
      case gamedataStatType.TechnicalAbilityAllocated:
      case gamedataStatType.IntelligenceAllocated:
      case gamedataStatType.ReflexesAllocated:
      case gamedataStatType.StrengthAllocated:
        return true;
      default:
        return false;
    };
    return false;
  }
}
