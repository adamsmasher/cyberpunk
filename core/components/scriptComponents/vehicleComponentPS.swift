
public class VehicleComponentPS extends ScriptableDeviceComponentPS {

  protected persistent let m_defaultStateSet: Bool;

  protected persistent let m_stateModifiedByQuest: Bool;

  protected persistent let m_playerVehicle: Bool;

  protected let m_npcOccupiedSlots: [CName];

  protected persistent let m_isDestroyed: Bool;

  protected persistent let m_isStolen: Bool;

  protected persistent let m_crystalDomeQuestModified: Bool;

  protected persistent let m_crystalDomeQuestState: Bool;

  protected persistent let m_crystalDomeState: Bool;

  protected persistent let m_visualDestructionSet: Bool;

  protected persistent let m_visualDestructionNeeded: Bool;

  protected persistent let m_exploded: Bool;

  protected persistent let m_submerged: Bool;

  protected persistent let m_sirenOn: Bool;

  protected persistent let m_sirenSoundOn: Bool;

  protected persistent let m_sirenLightsOn: Bool;

  @default(VehicleComponentPS, false)
  protected persistent let m_isDefaultLightToggleSet: Bool;

  protected persistent let m_anyDoorOpen: Bool;

  protected persistent let m_previousInteractionState: [TemporaryDoorState];

  protected persistent let m_thrusterState: Bool;

  protected persistent let m_uiQuestModified: Bool;

  protected persistent let m_uiState: Bool;

  protected let m_vehicleSkillChecks: ref<EngDemoContainer>;

  @default(VehicleComponentPS, false)
  private let m_controlStimShouldBeActive: Bool;

  @default(VehicleComponentPS, false)
  private let m_controlStimRunning: Bool;

  protected persistent let m_vehicleApperanceCustomizationActive: Bool;

  protected persistent let m_vehicleApperanceCustomizationInDistanceTermination: Bool;

  protected persistent let m_vehicleAppearanceCustomizationBlockedByDamage: Bool;

  protected persistent let m_vehicleVisualCustomizationTemplate: SavedVehicleVisualCustomizationTemplate;

  protected persistent let m_vehicleApperanceDefinition: vehicleVisualModdingDefinition;

  public let m_ready: Bool;

  public let m_isPlayerPerformingBodyDisposal: Bool;

  public let m_submergedTimestamp: Float;

  private let m_vehicleControllerPS: ref<vehicleControllerPS>;

  private final func GetVehicleControllerPS() -> ref<vehicleControllerPS> {
    let persistentId: PersistentID;
    if this.m_vehicleControllerPS == null {
      persistentId = CreatePersistentID(this.GetMyEntityID(), n"VehicleController");
      this.m_vehicleControllerPS = this.GetPersistencySystem().GetConstAccessToPSObject(persistentId, n"gamevehicleControllerPS") as vehicleControllerPS;
    };
    return this.m_vehicleControllerPS;
  }

  private final const func GetVehicleControllerPSConst() -> ref<vehicleControllerPS> {
    let persistentId: PersistentID;
    if this.m_vehicleControllerPS == null {
      persistentId = CreatePersistentID(this.GetMyEntityID(), n"VehicleController");
      return this.GetPersistencySystem().GetConstAccessToPSObject(persistentId, n"gamevehicleControllerPS") as vehicleControllerPS;
    };
    return this.m_vehicleControllerPS;
  }

  protected func Initialize() -> Void {
    super.Initialize();
    this.InitializeTempDoorStateStruct();
    this.InitializeDoorInteractionState();
    this.UpdateCustomizationDataToNewSystem();
  }

  protected func GameAttached() -> Void {
    this.RefreshSkillchecks();
  }

  protected final const func GetOwnerEntity() -> wref<VehicleObject> {
    return GameInstance.FindEntityByID(this.GetGameInstance(), PersistentID.ExtractEntityID(this.GetID())) as VehicleObject;
  }

  public final func GetHasDefaultStateBeenSet() -> Bool {
    return this.m_defaultStateSet;
  }

  public final func SetHasDefaultStateBeenSet(set: Bool) -> Void {
    this.m_defaultStateSet = set;
  }

  public final func GetHasStateBeenModifiedByQuest() -> Bool {
    return this.m_stateModifiedByQuest;
  }

  public final func GetNpcOccupiedSlots() -> [CName] {
    return this.m_npcOccupiedSlots;
  }

  public final func GetIsDestroyed() -> Bool {
    return this.m_isDestroyed;
  }

  public final func SetIsDestroyed(value: Bool) -> Void {
    if value {
      this.GetVehicleControllerPS().SetState(vehicleEState.Destroyed);
    };
    this.m_isDestroyed = value;
  }

  public final func GetIsStolen() -> Bool {
    return this.m_isStolen;
  }

  public final func SetIsStolen(value: Bool) -> Void {
    this.m_isStolen = value;
  }

  public final func SetHasStateBeenModifiedByQuest(set: Bool) -> Void {
    this.m_stateModifiedByQuest = set;
  }

  public final func GetIsPlayerVehicle() -> Bool {
    return this.m_playerVehicle;
  }

  public final func SetIsPlayerVehicle(set: Bool) -> Void {
    this.m_playerVehicle = set;
  }

  public final func GetIsCrystalDomeQuestModified() -> Bool {
    return this.m_crystalDomeQuestModified;
  }

  public final func SetIsCrystalDomeQuestModified(value: Bool) -> Void {
    this.m_crystalDomeQuestModified = value;
  }

  public final func GetCrystalDomeQuestState() -> Bool {
    return this.m_crystalDomeQuestState;
  }

  public final func SetCrystalDomeQuestState(value: Bool) -> Void {
    this.m_crystalDomeQuestState = value;
  }

  public final func GetCrystalDomeState() -> Bool {
    return this.m_crystalDomeState;
  }

  public final func SetCrystalDomeState(value: Bool) -> Void {
    this.m_crystalDomeState = value;
  }

  public final func GetIsUiQuestModified() -> Bool {
    return this.m_uiQuestModified;
  }

  public final func SetIsUiQuestModified(value: Bool) -> Void {
    this.m_uiQuestModified = value;
  }

  public final func GetUiQuestState() -> Bool {
    return this.m_uiState;
  }

  public final func SetUiQuestState(value: Bool) -> Void {
    this.m_uiState = value;
  }

  public final func GetSirenState() -> Bool {
    return this.m_sirenOn;
  }

  public final func SetSirenState(value: Bool) -> Void {
    this.m_sirenOn = value;
  }

  public final func GetSirenLightsState() -> Bool {
    return this.m_sirenLightsOn;
  }

  public final func SetSirenLightsState(value: Bool) -> Void {
    this.m_sirenLightsOn = value;
  }

  public final func GetIsDefaultLightToggleSet() -> Bool {
    return this.m_isDefaultLightToggleSet;
  }

  public final func SetIsDefaultLightToggleSet(value: Bool) -> Void {
    this.m_isDefaultLightToggleSet = value;
  }

  public final func GetSirenSoundsState() -> Bool {
    return this.m_sirenSoundOn;
  }

  public final func SetSirenSoundsState(value: Bool) -> Void {
    this.m_sirenSoundOn = value;
  }

  public final func GetHasVisualDestructionBeenSet() -> Bool {
    return this.m_visualDestructionSet;
  }

  public final func SetHasVisualDestructionBeenSet(set: Bool) -> Void {
    this.m_visualDestructionSet = set;
  }

  public final func GetHasExploded() -> Bool {
    return this.m_exploded;
  }

  public final func SetHasExploded(set: Bool) -> Void {
    if set {
      this.GetOwnerEntity().SetHasExploded();
    };
    this.m_exploded = set;
  }

  public final func GetIsSubmerged() -> Bool {
    return this.m_submerged;
  }

  public final func SetIsSubmerged(set: Bool) -> Void {
    this.m_submerged = set;
    this.m_submergedTimestamp = this.m_submerged ? EngineTime.ToFloat(GameInstance.GetTimeSystem(this.GetGameInstance()).GetSimTime()) : 100000000.00;
  }

  public final func GetSubmergedTimestamp() -> Float {
    return this.m_submergedTimestamp;
  }

  public final func GetHasAnyDoorOpen() -> Bool {
    return this.m_anyDoorOpen;
  }

  public final func SetHasAnyDoorOpen(set: Bool) -> Void {
    this.m_anyDoorOpen = set;
  }

  public final func GetThrusterState() -> Bool {
    return this.m_thrusterState;
  }

  public final func SetThrusterState(set: Bool) -> Void {
    this.m_thrusterState = set;
  }

  public final func GetIsVehicleVisualCustomizationActive() -> Bool {
    return this.m_vehicleApperanceCustomizationActive;
  }

  public final func SetVehicleVisualCustomizationActive(set: Bool) -> Void {
    this.m_vehicleApperanceCustomizationActive = set;
  }

  public final func GetIsVehicleApperanceCustomizationInDistanceTermination() -> Bool {
    return this.m_vehicleApperanceCustomizationInDistanceTermination;
  }

  public final func SetVehicleApperanceCustomizationInDistanceTermination(set: Bool) -> Void {
    this.m_vehicleApperanceCustomizationInDistanceTermination = set;
  }

  public final func GetVehicleVisualCustomizationTemplate() -> VehicleVisualCustomizationTemplate {
    return SavedVehicleVisualCustomizationTemplate.ToVehicleVisualCustomizationTemplate(this.m_vehicleVisualCustomizationTemplate);
  }

  public final func SetVehicleVisualCustomizationTemplate(template: VehicleVisualCustomizationTemplate) -> Void {
    this.m_vehicleVisualCustomizationTemplate = SavedVehicleVisualCustomizationTemplate.FromVehicleVisualCustomizationTemplate(template);
  }

  public final func SetVehicleVisualCustomizationnBlockedByDamage(value: Bool) -> Void {
    this.m_vehicleAppearanceCustomizationBlockedByDamage = value;
  }

  public final func GetIsVehicleVisualCustomizationBlockedByDamage() -> Bool {
    return this.m_vehicleAppearanceCustomizationBlockedByDamage;
  }

  private final func UpdateCustomizationDataToNewSystem() -> Void {
    let emptyData: vehicleVisualModdingDefinition;
    if !this.m_vehicleApperanceDefinition.primaryColorDefined && !this.m_vehicleApperanceDefinition.secondaryColorDefined && !this.m_vehicleApperanceDefinition.lightsColorDefined {
      return;
    };
    this.m_vehicleVisualCustomizationTemplate = SavedVehicleVisualCustomizationTemplate.FromVehicleVisualCustomizationTemplate(vehicleVisualModdingDefinition.UpdateToNewFormat(this.m_vehicleApperanceDefinition));
    this.m_vehicleApperanceDefinition = emptyData;
  }

  public final func OnToggleVehicle(evt: ref<ToggleVehicle>) -> EntityNotificationType {
    let controllerPS: ref<vehicleControllerPS> = this.GetVehicleControllerPS();
    controllerPS.SetState(evt.GetValue() ? vehicleEState.On : vehicleEState.Default);
    this.UseNotifier(evt);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final func OnForceCarAlarm(evt: ref<ForceCarAlarm>) -> EntityNotificationType {
    let controllerPS: ref<vehicleControllerPS> = this.GetVehicleControllerPS();
    controllerPS.SetAlarm(true);
    this.UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func OnForceDisableCarAlarm(evt: ref<ForceDisableCarAlarm>) -> EntityNotificationType {
    let controllerPS: ref<vehicleControllerPS> = this.GetVehicleControllerPS();
    controllerPS.SetAlarm(false);
    QuickhackModule.RequestRefreshQuickhackMenu(this.GetGameInstance(), evt.GetRequesterID());
    this.UseNotifier(evt);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final func DisableAlarm() -> Void {
    let action: ref<ForceDisableCarAlarm> = this.ActionForceDisableCarAlarm();
    this.ExecutePSAction(action);
  }

  private final func InitializeTempDoorStateStruct() -> Void {
    let size: Int32 = Cast<Int32>(EnumGetMax(n"EVehicleDoor")) + 1;
    ArrayResize(this.m_previousInteractionState, size);
  }

  private final func InitializeDoorInteractionState() -> Void {
    this.SetDoorInteractionState(EVehicleDoor.seat_back_left, VehicleDoorInteractionState.Disabled, "InitializeDoorInteractionState");
    this.SetDoorInteractionState(EVehicleDoor.seat_back_right, VehicleDoorInteractionState.Disabled, "InitializeDoorInteractionState");
  }

  protected final func RefreshSkillchecks() -> Void {
    let demoCheck: ref<DemolitionSkillCheck> = new DemolitionSkillCheck();
    let engCheck: ref<EngineeringSkillCheck> = new EngineeringSkillCheck();
    this.m_vehicleSkillChecks = new EngDemoContainer();
    let difficultyTDBID: TweakDBID = this.GetOwnerEntity().GetRecordID();
    TDBID.Append(difficultyTDBID, t".hijackDifficulty");
    demoCheck.SetDifficulty(IntEnum<EGameplayChallengeLevel>(Cast<Int32>(EnumValueFromName(n"EGameplayChallengeLevel", StringToName(TweakDBInterface.GetString(difficultyTDBID, "NONE"))))));
    demoCheck.SetAlternativeName(t"Interactions.HijackVehicle");
    this.m_vehicleSkillChecks.m_demolitionCheck = demoCheck;
    difficultyTDBID = this.GetOwnerEntity().GetRecordID();
    TDBID.Append(difficultyTDBID, t".crackLockDifficulty");
    engCheck.SetDifficulty(IntEnum<EGameplayChallengeLevel>(Cast<Int32>(EnumValueFromName(n"EGameplayChallengeLevel", StringToName(TweakDBInterface.GetString(difficultyTDBID, "NONE"))))));
    engCheck.SetAlternativeName(t"Interactions.VehicleCrackLock");
    this.m_vehicleSkillChecks.m_engineeringCheck = engCheck;
    this.InitializeSkillChecks(this.m_vehicleSkillChecks, true);
  }

  public final func ToggleReserveSeatDuringUnmounting(const active: Bool, const slotID: CName) -> Void {
    let seatReservationEvent: ref<VehicleSeatReservationEvent> = new VehicleSeatReservationEvent();
    seatReservationEvent.slotID = slotID;
    seatReservationEvent.reserve = active;
    GameInstance.GetPersistencySystem(this.GetOwnerEntity().GetGame()).QueuePSEvent(this.GetID(), this.GetOwnerEntity().GetPSClassName(), seatReservationEvent);
  }

  protected final func ChangeToActionContext(const vehicleContext: script_ref<VehicleActionsContext>) -> GetActionsContext {
    let getActionsContext: GetActionsContext;
    getActionsContext.requestorID = Deref(vehicleContext).requestorID;
    getActionsContext.requestType = Deref(vehicleContext).requestType;
    getActionsContext.interactionLayerTag = Deref(vehicleContext).interactionLayerTag;
    getActionsContext.processInitiatorObject = Deref(vehicleContext).processInitiatorObject;
    return getActionsContext;
  }

  public final func SetDoorState(door: EVehicleDoor, state: VehicleDoorState, immediate: Bool) -> Void {
    this.GetVehicleControllerPS().SetDoorState(door, state, immediate);
  }

  public final func GetDoorState(door: EVehicleDoor) -> VehicleDoorState {
    return this.GetVehicleControllerPS().GetDoorState(door);
  }

  public final func SetWindowState(door: EVehicleDoor, state: EVehicleWindowState) -> Void {
    this.GetVehicleControllerPS().SetWindowState(door, state);
  }

  public final func GetWindowState(door: EVehicleDoor) -> EVehicleWindowState {
    return this.GetVehicleControllerPS().GetWindowState(door);
  }

  public final func SetDoorInteractionState(door: EVehicleDoor, state: VehicleDoorInteractionState, const source: script_ref<String>) -> Void {
    this.GetVehicleControllerPS().SetDoorInteractionState(door, state);
  }

  public final func GetDoorInteractionState(door: EVehicleDoor) -> VehicleDoorInteractionState {
    return this.GetVehicleControllerPS().GetDoorInteractionState(door);
  }

  public final func SetTempDoorInteractionState(door: EVehicleDoor, state: VehicleDoorInteractionState) -> Void {
    this.m_previousInteractionState[EnumInt(door)].interactionState = state;
  }

  public final const func GetTempDoorInteractionState(door: EVehicleDoor) -> VehicleDoorInteractionState {
    return this.m_previousInteractionState[EnumInt(door)].interactionState;
  }

  public final func GetVehicleDoorEnum(out door: EVehicleDoor, doorName: CName) -> Bool {
    let res: Int32 = Cast<Int32>(EnumValueFromName(n"EVehicleDoor", doorName));
    if res < 0 {
      return false;
    };
    door = IntEnum<EVehicleDoor>(res);
    return true;
  }

  public final func OnVehicleDoorInteraction(evt: ref<VehicleDoorInteraction>) -> EntityNotificationType {
    if evt.isInteractionSource {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func OnVehicleForceOccupantOut(evt: ref<VehicleForceOccupantOut>) -> EntityNotificationType {
    return EntityNotificationType.SendThisEventToEntity;
  }

  public func OnActionDemolition(evt: ref<ActionDemolition>) -> EntityNotificationType {
    this.ProcessVehicleHijackTutorialUsed();
    if !evt.WasPassed() {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    super.OnActionDemolition(evt);
    if evt.IsCompleted() {
      this.m_skillCheckContainer.GetDemolitionSlot().SetIsPassed(false);
      this.InitializeSkillChecks(this.m_vehicleSkillChecks, true);
      return EntityNotificationType.SendThisEventToEntity;
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public func OnActionEngineering(evt: ref<ActionEngineering>) -> EntityNotificationType {
    this.ProcessVehicleCrackLockTutorialUsed();
    if !evt.WasPassed() {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    super.OnActionEngineering(evt);
    if evt.IsCompleted() {
      this.ResetVehicleInteractionState();
      return EntityNotificationType.SendThisEventToEntity;
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected final func ProcessVehicleCrackLockTutorial() -> Void {
    if GameInstance.GetQuestsSystem(this.GetGameInstance()).GetFact(n"tutorial_vehicle_crack_lock") == 0 {
      GameInstance.GetQuestsSystem(this.GetGameInstance()).SetFact(n"tutorial_vehicle_crack_lock", 1);
    };
  }

  protected final func ProcessVehicleCrackLockTutorialUsed() -> Void {
    if GameInstance.GetQuestsSystem(this.GetGameInstance()).GetFact(n"tutorial_vehicle_crack_lock") == 1 {
      GameInstance.GetQuestsSystem(this.GetGameInstance()).SetFact(n"tutorial_vehicle_crack_lock", 2);
    };
  }

  protected final func ProcessVehicleHijackTutorial() -> Void {
    if GameInstance.GetQuestsSystem(this.GetGameInstance()).GetFact(n"tutorial_vehicle_hijack") == 0 {
      GameInstance.GetQuestsSystem(this.GetGameInstance()).SetFact(n"tutorial_vehicle_hijack", 1);
    };
  }

  protected final func ProcessVehicleHijackTutorialUsed() -> Void {
    if GameInstance.GetQuestsSystem(this.GetGameInstance()).GetFact(n"tutorial_vehicle_hijack") == 1 {
      GameInstance.GetQuestsSystem(this.GetGameInstance()).SetFact(n"tutorial_vehicle_hijack", 2);
    };
  }

  public final func OnVehicleDoorOpen(evt: ref<VehicleDoorOpen>) -> EntityNotificationType {
    let curDoorState: VehicleDoorState;
    let doorID: EVehicleDoor = IntEnum<EVehicleDoor>(Cast<Int32>(EnumValueFromName(n"EVehicleDoor", evt.slotID)));
    if EnumInt(doorID) < 6 && EnumInt(doorID) != -1 {
      curDoorState = this.GetDoorState(doorID);
      if NotEquals(curDoorState, VehicleDoorState.Detached) {
        this.SetDoorState(doorID, VehicleDoorState.Open, evt.forceScene);
      };
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func OnVehicleDoorClose(evt: ref<VehicleDoorClose>) -> EntityNotificationType {
    let curDoorState: VehicleDoorState;
    let doorID: EVehicleDoor = IntEnum<EVehicleDoor>(Cast<Int32>(EnumValueFromName(n"EVehicleDoor", evt.slotID)));
    if EnumInt(doorID) < 6 && EnumInt(doorID) != -1 {
      curDoorState = this.GetDoorState(doorID);
      if NotEquals(curDoorState, VehicleDoorState.Detached) {
        this.SetDoorState(doorID, VehicleDoorState.Closed, evt.forceScene);
      };
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func OnVehicleDoorInteractionStateChange(evt: ref<VehicleDoorInteractionStateChange>) -> EntityNotificationType {
    let newState: VehicleDoorInteractionState = evt.newState;
    if !this.IsStateValidForVehicle(newState) {
      newState = VehicleDoorInteractionState.Available;
    };
    this.SetDoorInteractionState(evt.door, newState, evt.source);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func GetQuestLockedActions(actions: script_ref<[ref<DeviceAction>]>, const context: script_ref<VehicleActionsContext>) -> Void {
    ArrayPush(Deref(actions), this.ActionVehicleDoorQuestLocked());
  }

  public final func IsStateValidForVehicle(state: VehicleDoorInteractionState) -> Bool {
    if this.GetOwnerEntity() == (this.GetOwnerEntity() as TankObject) {
      if Equals(state, VehicleDoorInteractionState.Locked) || Equals(state, VehicleDoorInteractionState.Reserved) || Equals(state, VehicleDoorInteractionState.QuestLocked) {
        return false;
      };
    };
    return true;
  }

  public final func OnVehicleWindowOpen(evt: ref<VehicleWindowOpen>) -> EntityNotificationType {
    let doorID: EVehicleDoor = IntEnum<EVehicleDoor>(Cast<Int32>(EnumValueFromName(n"EVehicleDoor", evt.slotID)));
    if EnumInt(doorID) < 6 && EnumInt(doorID) != -1 {
      this.SetWindowState(doorID, EVehicleWindowState.Open);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func OnVehicleWindowClose(evt: ref<VehicleWindowClose>) -> EntityNotificationType {
    let doorID: EVehicleDoor = IntEnum<EVehicleDoor>(Cast<Int32>(EnumValueFromName(n"EVehicleDoor", evt.slotID)));
    if EnumInt(doorID) < 6 && EnumInt(doorID) != -1 {
      this.SetWindowState(doorID, EVehicleWindowState.Closed);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func OnOpenTrunk(evt: ref<VehicleOpenTrunk>) -> EntityNotificationType {
    this.SetDoorState(EVehicleDoor.trunk, VehicleDoorState.Open, false);
    if IsDefined(evt.GetExecutor()) && !this.GetOwnerEntity().IsCrowdVehicle() {
      this.GetOwnerEntity().GetDeviceLink().TriggerSecuritySystemNotification(this.GetOwnerEntity().GetWorldPosition(), evt.GetExecutor(), ESecurityNotificationType.ALARM);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func OnCloseTrunk(evt: ref<VehicleCloseTrunk>) -> EntityNotificationType {
    this.SetDoorState(EVehicleDoor.trunk, VehicleDoorState.Closed, false);
    if IsDefined(evt.GetExecutor()) && !this.GetOwnerEntity().IsCrowdVehicle() {
      this.GetOwnerEntity().GetDeviceLink().TriggerSecuritySystemNotification(this.GetOwnerEntity().GetWorldPosition(), evt.GetExecutor(), ESecurityNotificationType.DEESCALATE);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func OnVehicleDumpBody(evt: ref<VehicleDumpBody>) -> EntityNotificationType {
    this.ProcessBodyDisposalEvent();
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func OnVehicleTakeBody(evt: ref<VehicleTakeBody>) -> EntityNotificationType {
    this.ProcessBodyDisposalEvent();
    return EntityNotificationType.SendThisEventToEntity;
  }

  private final func ProcessBodyDisposalEvent() -> Void {
    let evt: ref<VehicleBodyDisposalPerformedEvent> = new VehicleBodyDisposalPerformedEvent();
    GameInstance.GetDelaySystem(this.GetGameInstance()).DelayPSEvent(this.GetID(), n"VehicleComponentPS", evt, 3.00, true);
    this.m_isPlayerPerformingBodyDisposal = true;
  }

  protected final func OnVehicleBodyDisposalPerformedEvent(evt: ref<VehicleBodyDisposalPerformedEvent>) -> EntityNotificationType {
    this.m_isPlayerPerformingBodyDisposal = false;
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func OnVehiclePlayerTrunk(evt: ref<VehiclePlayerTrunk>) -> EntityNotificationType {
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func GetTrunkActions(actions: script_ref<[ref<DeviceAction>]>, const context: script_ref<VehicleActionsContext>) -> Void {
    let vehDataPackage: wref<VehicleDataPackage_Record>;
    if this.GetOwnerEntity().MatchVisualTag(n"NoTrunk") {
      return;
    };
    if this.GetOwnerEntity().IsCrowdVehicle() {
      return;
    };
    VehicleComponent.GetVehicleDataPackage(this.GetGameInstance(), this.GetOwnerEntity(), vehDataPackage);
    if this.GetIsPlayerVehicle() {
      ArrayPush(Deref(actions), this.ActionPlayerTrunk());
    };
    if this.GetOwnerEntity() != (this.GetOwnerEntity() as CarObject) {
      return;
    };
    if Equals(this.GetDoorInteractionState(EVehicleDoor.trunk), VehicleDoorInteractionState.Disabled) {
      return;
    };
    if this.m_isPlayerPerformingBodyDisposal {
      if Equals(this.GetDoorState(EVehicleDoor.trunk), VehicleDoorState.Open) {
        ArrayPush(Deref(actions), this.ActionCloseTrunk());
      };
      return;
    };
    if Equals(this.GetDoorState(EVehicleDoor.trunk), VehicleDoorState.Detached) {
      if this.IsPlayerCarryingBody(false) && !VehicleComponent.IsSlotOccupied(this.GetGameInstance(), this.GetOwnerEntity().GetEntityID(), n"trunk_body") && vehDataPackage.CanStoreBody() {
        ArrayPush(Deref(actions), this.ActionVehicleDumpBody());
      };
      if VehicleComponent.IsSlotOccupied(this.GetGameInstance(), this.GetOwnerEntity().GetEntityID(), n"trunk_body") {
        ArrayPush(Deref(actions), this.ActionVehicleTakeBody());
      };
      return;
    };
    if Equals(this.GetDoorState(EVehicleDoor.trunk), VehicleDoorState.Closed) {
      if this.IsPlayerCarryingBody(false) && !VehicleComponent.IsSlotOccupied(this.GetGameInstance(), this.GetOwnerEntity().GetEntityID(), n"trunk_body") && vehDataPackage.CanStoreBody() {
        ArrayPush(Deref(actions), this.ActionVehicleDumpBody());
      } else {
        ArrayPush(Deref(actions), this.ActionOpenTrunk());
      };
    };
    if Equals(this.GetDoorState(EVehicleDoor.trunk), VehicleDoorState.Open) {
      if this.IsPlayerCarryingBody(false) && !VehicleComponent.IsSlotOccupied(this.GetGameInstance(), this.GetOwnerEntity().GetEntityID(), n"trunk_body") && vehDataPackage.CanStoreBody() {
        ArrayPush(Deref(actions), this.ActionVehicleDumpBody());
        return;
      };
      if !this.IsPlayerCarryingBody(true) && VehicleComponent.IsSlotOccupied(this.GetGameInstance(), this.GetOwnerEntity().GetEntityID(), n"trunk_body") && !this.IsPlayerSwimming() {
        ArrayPush(Deref(actions), this.ActionVehicleTakeBody());
      };
      ArrayPush(Deref(actions), this.ActionCloseTrunk());
    };
    return;
  }

  public final func IsPlayerCarryingBody(includePickupPhase: Bool) -> Bool {
    let playerPuppet: ref<PlayerPuppet> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerControlledGameObject() as PlayerPuppet;
    let playerStateMachineBlackboard: ref<IBlackboard> = GameInstance.GetBlackboardSystem(this.GetGameInstance()).GetLocalInstanced(playerPuppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    if includePickupPhase {
      return playerStateMachineBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.Carrying);
    };
    return playerStateMachineBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.BodyCarrying) == 2;
  }

  public final func IsPlayerSwimming() -> Bool {
    let playerPuppet: ref<PlayerPuppet> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject() as PlayerPuppet;
    return PlayerPuppet.IsSwimming(playerPuppet);
  }

  public final func GetPlayerTrunkActions(actions: script_ref<[ref<DeviceAction>]>, const context: script_ref<VehicleActionsContext>) -> Void {
    ArrayPush(Deref(actions), this.ActionPlayerTrunk());
    return;
  }

  public final func OnOpenHood(evt: ref<VehicleOpenHood>) -> EntityNotificationType {
    this.SetDoorState(EVehicleDoor.hood, VehicleDoorState.Open, false);
    if IsDefined(evt.GetExecutor()) && !this.GetOwnerEntity().IsCrowdVehicle() {
      this.GetOwnerEntity().GetDeviceLink().TriggerSecuritySystemNotification(this.GetOwnerEntity().GetWorldPosition(), evt.GetExecutor(), ESecurityNotificationType.ALARM);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func OnCloseHood(evt: ref<VehicleCloseHood>) -> EntityNotificationType {
    this.SetDoorState(EVehicleDoor.hood, VehicleDoorState.Closed, false);
    if IsDefined(evt.GetExecutor()) && !this.GetOwnerEntity().IsCrowdVehicle() {
      this.GetOwnerEntity().GetDeviceLink().TriggerSecuritySystemNotification(this.GetOwnerEntity().GetWorldPosition(), evt.GetExecutor(), ESecurityNotificationType.DEESCALATE);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final func GetHoodActions(actions: script_ref<[ref<DeviceAction>]>, const context: script_ref<VehicleActionsContext>) -> Void {
    if this.GetOwnerEntity().MatchVisualTag(n"NoHood") {
      return;
    };
    if this.GetOwnerEntity().IsCrowdVehicle() {
      return;
    };
    if this.GetOwnerEntity() != (this.GetOwnerEntity() as CarObject) {
      return;
    };
    if Equals(this.GetDoorInteractionState(EVehicleDoor.hood), VehicleDoorInteractionState.Disabled) {
      return;
    };
    if Equals(this.GetDoorState(EVehicleDoor.hood), VehicleDoorState.Detached) {
      return;
    };
    if Equals(this.GetDoorState(EVehicleDoor.hood), VehicleDoorState.Closed) {
      ArrayPush(Deref(actions), this.ActionOpenHood());
    };
    if Equals(this.GetDoorState(EVehicleDoor.hood), VehicleDoorState.Open) {
      ArrayPush(Deref(actions), this.ActionCloseHood());
    };
    return;
  }

  public final func DetermineActionsToPush(interaction: ref<InteractionComponent>, context: VehicleActionsContext, objectActionsCallbackController: wref<gameObjectActionsCallbackController>, isAutoRefresh: Bool) -> Void {
    let actionRecords: array<wref<ObjectAction_Record>>;
    let actionToExtractChoices: ref<ScriptableDeviceAction>;
    let actions: array<ref<DeviceAction>>;
    let choiceTDBname: String;
    let choices: array<InteractionChoice>;
    let door: EVehicleDoor;
    let doorLayer: CName;
    let i: Int32;
    let playerPuppet: ref<PlayerPuppet>;
    let playerStateMachineBlackboard: ref<IBlackboard>;
    let uploadingQuickHackIDs: array<TweakDBID>;
    let vehicleID: EntityID;
    if this.GetIsDestroyed() {
      this.PushActionsToInteractionComponent(interaction, choices, context);
      return;
    };
    playerPuppet = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject() as PlayerPuppet;
    playerStateMachineBlackboard = GameInstance.GetBlackboardSystem(playerPuppet.GetGame()).GetLocalInstanced(playerPuppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    vehicleID = playerStateMachineBlackboard.GetEntityID(GetAllBlackboardDefs().PlayerStateMachine.EntityIDVehicleRemoteControlled);
    if EntityID.IsDefined(vehicleID) {
      return;
    };
    if playerPuppet.GetTakeOverControlSystem().IsDeviceControlled() {
      return;
    };
    uploadingQuickHackIDs = FromVariant<array<TweakDBID>>(playerStateMachineBlackboard.GetVariant(GetAllBlackboardDefs().PlayerStateMachine.UploadingQuickHackIDs));
    if ArrayContains(uploadingQuickHackIDs, t"DeviceAction.TakeControlVehicleClassHack") || ArrayContains(uploadingQuickHackIDs, t"DeviceAction.TakeControlClassHack") || ArrayContains(uploadingQuickHackIDs, t"DeviceAction.TakeControlCameraClassHack") || ArrayContains(uploadingQuickHackIDs, t"DeviceAction.VehicleExplodeClassHack") {
      return;
    };
    if this.IsDoorLayer(context.interactionLayerTag) {
      if Equals(this.GetOwnerEntity().GetVehicleType(), gamedataVehicleType.Bike) && this.GetOwnerEntity().IsFlippedOver() {
        return;
      };
      doorLayer = context.interactionLayerTag;
      this.GetVehicleDoorEnum(door, doorLayer);
      if Equals(this.GetDoorInteractionState(door), VehicleDoorInteractionState.Disabled) {
        return;
      };
      if Equals(this.GetDoorInteractionState(door), VehicleDoorInteractionState.Reserved) {
        this.PushActionsToInteractionComponent(interaction, choices, context);
        return;
      };
      if Equals(this.GetDoorInteractionState(door), VehicleDoorInteractionState.QuestLocked) {
        this.GetQuestLockedActions(actions, context);
      };
    };
    if Equals(context.interactionLayerTag, n"trunk") {
      this.GetTrunkActions(actions, context);
    };
    if Equals(context.interactionLayerTag, n"hood") {
      this.GetHoodActions(actions, context);
    };
    if Equals(context.interactionLayerTag, n"Mount") {
      return;
    };
    context.requestType = gamedeviceRequestType.Direct;
    this.GetOwnerEntity().GetRecord().ObjectActions(actionRecords);
    this.GetValidChoices(actionRecords, this.ChangeToActionContext(context), objectActionsCallbackController, choices, isAutoRefresh);
    this.FinalizeGetActions(actions);
    i = 0;
    while i < ArraySize(actions) {
      actionToExtractChoices = actions[i] as ScriptableDeviceAction;
      (actions[i] as ScriptableDeviceAction).SetExecutor(context.processInitiatorObject);
      ArrayPush(choices, actionToExtractChoices.GetInteractionChoice());
      i += 1;
    };
    if !isAutoRefresh {
      i = 0;
      while i < ArraySize(choices) {
        choiceTDBname = choices[i].choiceMetaData.tweakDBName;
        switch choiceTDBname {
          case "ActionDemolition":
            this.ProcessVehicleHijackTutorial();
            break;
          case "ActionEngineering":
            this.ProcessVehicleCrackLockTutorial();
        };
        i += 1;
      };
    };
    this.PushActionsToInteractionComponent(interaction, choices, context);
  }

  protected final func IsDoorLayer(layer: CName) -> Bool {
    return Equals(layer, n"seat_front_right") || Equals(layer, n"seat_front_left") || Equals(layer, n"seat_back_left") || Equals(layer, n"seat_back_right");
  }

  public final func GetValidChoices(const objectActionRecords: script_ref<[wref<ObjectAction_Record>]>, const context: script_ref<GetActionsContext>, objectActionsCallbackController: wref<gameObjectActionsCallbackController>, choices: script_ref<[InteractionChoice]>, isAutoRefresh: Bool) -> Void {
    let actionName: CName;
    let actionRecord: wref<ObjectAction_Record>;
    let actionType: gamedataObjectActionType;
    let choice: InteractionChoice;
    let compareAction: ref<ScriptableDeviceAction>;
    let i: Int32;
    let instigator: wref<GameObject>;
    let isRemote: Bool;
    let j: Int32;
    let newAction: ref<ScriptableDeviceAction>;
    let objectActionInteractionLayer: CName;
    let playerInteractionLayer: CName;
    let vehDataPackage: wref<VehicleDataPackage_Record>;
    let maxChoices: Int32 = 4;
    VehicleComponent.GetVehicleDataPackage(this.GetGameInstance(), this.GetOwnerEntity(), vehDataPackage);
    playerInteractionLayer = Deref(context).interactionLayerTag;
    instigator = Deref(context).processInitiatorObject;
    i = 0;
    while i < ArraySize(Deref(objectActionRecords)) {
      actionType = Deref(objectActionRecords)[i].ObjectActionType().Type();
      objectActionInteractionLayer = Deref(objectActionRecords)[i].InteractionLayer();
      if NotEquals(objectActionInteractionLayer, playerInteractionLayer) {
      } else {
        switch actionType {
          case gamedataObjectActionType.Payment:
          case gamedataObjectActionType.Item:
          case gamedataObjectActionType.Direct:
            isRemote = false;
            break;
          case gamedataObjectActionType.MinigameUpload:
          case gamedataObjectActionType.VehicleQuickHack:
          case gamedataObjectActionType.DeviceQuickHack:
          case gamedataObjectActionType.Remote:
            isRemote = true;
            break;
          default:
            isRemote = false;
        };
        if !isRemote && Equals(Deref(context).requestType, gamedeviceRequestType.Direct) || isRemote && Equals(Deref(context).requestType, gamedeviceRequestType.Remote) {
          actionName = Deref(objectActionRecords)[i].ActionName();
          switch actionName {
            case n"VehicleHijack":
              newAction = this.ActionDemolition(context);
              newAction.SetIllegal(true);
              break;
            case n"VehicleMount":
              newAction = this.ActionVehicleDoorInteraction(ToString(Deref(objectActionRecords)[i].InteractionLayer()), true);
              break;
            case n"VehicleCrackLock":
              newAction = this.ActionEngineering(context);
              newAction.SetIllegal(true);
          };
          newAction.SetObjectActionID(Deref(objectActionRecords)[i].GetID());
          newAction.SetExecutor(instigator);
          if IsDefined(objectActionsCallbackController) {
            if !objectActionsCallbackController.HasObjectAction(Deref(objectActionRecords)[i]) {
              objectActionsCallbackController.AddObjectAction(Deref(objectActionRecords)[i]);
            };
          };
          if newAction.IsPossible(this.GetOwnerEntity(), objectActionsCallbackController) {
            if newAction.IsVisible(context, objectActionsCallbackController) {
              actionRecord = Deref(objectActionRecords)[i];
              choice = newAction.GetInteractionChoice();
              newAction.prop.name = playerInteractionLayer;
              ArrayPush(choice.data, ToVariant(newAction));
              j = 0;
              while j < maxChoices {
                compareAction = FromVariant<ref<ScriptableDeviceAction>>(Deref(choices)[j].data[0]);
                if IsDefined(compareAction) {
                  if actionRecord.Priority() >= compareAction.GetObjectActionRecord().Priority() {
                    ArrayInsert(Deref(choices), j, choice);
                    break;
                  };
                } else {
                  ArrayPush(Deref(choices), choice);
                  break;
                };
                j += 1;
              };
            } else {
              newAction.SetInactiveWithReason(false, "LocKey#7009");
            };
          };
        };
      };
      i += 1;
    };
    if ArraySize(Deref(choices)) > maxChoices {
      ArrayResize(Deref(choices), maxChoices);
    };
  }

  public final func PushActionsToInteractionComponent(interaction: ref<InteractionComponent>, const choices: script_ref<[InteractionChoice]>, const context: script_ref<VehicleActionsContext>) -> Void {
    if !IsDefined(interaction) {
      return;
    };
    interaction.SetChoices(Deref(choices), Deref(context).interactionLayerTag);
  }

  protected final func OnVehicleFinishedMounting(evt: ref<VehicleFinishedMountingEvent>) -> EntityNotificationType {
    let i: Int32;
    if !evt.isMounting {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    if evt.character.IsPlayer() {
      return EntityNotificationType.SendThisEventToEntity;
    };
    i = 0;
    while i < ArraySize(this.m_npcOccupiedSlots) {
      if Equals(this.m_npcOccupiedSlots[i], evt.slotID) {
        if evt.isMounting {
          return EntityNotificationType.SendThisEventToEntity;
        };
      };
      i = i + 1;
    };
    if evt.isMounting {
      ArrayPush(this.m_npcOccupiedSlots, evt.slotID);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehicleStartedUnmountingEvent(evt: ref<VehicleStartedUnmountingEvent>) -> EntityNotificationType {
    let i: Int32;
    if evt.character.IsPlayer() || evt.isMounting {
      return EntityNotificationType.DoNotNotifyEntity;
    };
    i = 0;
    while i < ArraySize(this.m_npcOccupiedSlots) {
      if Equals(this.m_npcOccupiedSlots[i], evt.slotID) {
        ArrayErase(this.m_npcOccupiedSlots, i);
        this.ToggleReserveSeatDuringUnmounting(true, evt.slotID);
        return EntityNotificationType.SendThisEventToEntity;
      };
      i = i + 1;
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  public final const func IsSlotOccupiedByNPC(slotID: CName) -> Bool {
    let i: Int32 = 0;
    while i < ArraySize(this.m_npcOccupiedSlots) {
      if Equals(this.m_npcOccupiedSlots[i], slotID) {
        return true;
      };
      i = i + 1;
    };
    return false;
  }

  public func OnSetExposeQuickHacks(evt: ref<SetExposeQuickHacks>) -> EntityNotificationType {
    super.OnSetExposeQuickHacks(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public func GetActions(out outActions: [ref<DeviceAction>], context: GetActionsContext) -> Bool {
    this.SetActionIllegality(outActions, this.m_illegalActions.regularActions);
    return true;
  }

  protected const func CanCreateAnyQuickHackActions() -> Bool {
    return !this.m_isDestroyed;
  }

  protected func GetQuickHackActions(out outActions: [ref<DeviceAction>], const context: script_ref<GetActionsContext>) -> Void {
    let areVehicleQuickhacksAvailable: Bool;
    let controllerPS: ref<vehicleControllerPS>;
    let currentAction: ref<ScriptableDeviceAction>;
    let playerMountedVehicle: wref<VehicleObject>;
    let playerPuppet: ref<PlayerPuppet>;
    let vehicleState: vehicleEState;
    let isValidRange: Bool = false;
    let isVehicleRemoteControlled: Bool = this.GetOwnerEntity().IsVehicleRemoteControlled();
    let maxDistance: Float = TweakDBInterface.GetFloat(t"player.vehicleQuickHacks.maxRange", 0.00);
    if this.GetOwnerEntity().GetDistanceToPlayerSquared() < maxDistance * maxDistance {
      isValidRange = true;
    };
    playerPuppet = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject() as PlayerPuppet;
    if PlayerDevelopmentSystem.GetData(playerPuppet).IsNewPerkBoughtAnyLevel(gamedataNewPerkType.Intelligence_Right_Milestone_1) {
      controllerPS = this.GetVehicleControllerPS();
      vehicleState = controllerPS.GetState();
      areVehicleQuickhacksAvailable = this.GetOwnerEntity().IsHackable();
      if !this.GetIsDestroyed() {
        if !VehicleComponent.GetVehicle(playerPuppet.GetGame(), playerPuppet.GetEntityID(), playerMountedVehicle) {
          currentAction = this.ActionToggleTakeOverControl();
          currentAction.SetObjectActionID(t"DeviceAction.TakeControlVehicleClassHack");
          currentAction.SetExecutor(GetPlayer(this.GetGameInstance()));
          currentAction.SetDurationValue(currentAction.GetDurationTime());
          ArrayPush(outActions, currentAction);
          if !isValidRange || isVehicleRemoteControlled || Equals(this.GetOwnerEntity().GetVehicleType(), gamedataVehicleType.Bike) && playerPuppet.CheckIsStandingOnVehicle(this.GetOwnerEntity().GetEntityID()) {
            currentAction.SetInactiveWithReason(false, "LocKey#7003");
          };
        };
        currentAction = this.ActionVehicleOverrideForceBrakes();
        currentAction.SetObjectActionID(t"DeviceAction.VehicleForceBrakesClassHack");
        currentAction.SetExecutor(GetPlayer(this.GetGameInstance()));
        currentAction.SetDurationValue(TweakDBInterface.GetFloat(t"player.vehicleQuickHacks.forceBrakesDuration", 0.00));
        if !isValidRange || isVehicleRemoteControlled {
          currentAction.SetInactiveWithReason(false, "LocKey#7003");
        } else {
          if this.GetOwnerEntity().IsVehicleForceBrakesQuickhackActive() {
            currentAction.SetInactiveWithReason(false, "LocKey#7004");
          };
        };
        ArrayPush(outActions, currentAction);
        currentAction = this.ActionVehicleOverrideExplode();
        currentAction.SetObjectActionID(t"DeviceAction.VehicleExplodeClassHack");
        currentAction.SetExecutor(GetPlayer(this.GetGameInstance()));
        currentAction.SetDurationValue(currentAction.GetDurationTime());
        ArrayPush(outActions, currentAction);
        if !isValidRange {
          currentAction.SetInactiveWithReason(false, "LocKey#7003");
        };
        currentAction = this.ActionVehicleOverrideAccelerate();
        currentAction.SetObjectActionID(t"DeviceAction.VehicleAccelerateClassHack");
        currentAction.SetExecutor(GetPlayer(this.GetGameInstance()));
        currentAction.SetDurationValue(TweakDBInterface.GetFloat(t"player.vehicleQuickHacks.accelerateDuration", 0.00));
        if !isValidRange || isVehicleRemoteControlled {
          currentAction.SetInactiveWithReason(false, "LocKey#7003");
        } else {
          if this.GetOwnerEntity().IsVehicleAccelerateQuickhackActive() {
            currentAction.SetInactiveWithReason(false, "LocKey#7004");
          };
        };
        ArrayPush(outActions, currentAction);
      };
      if !areVehicleQuickhacksAvailable || Equals(vehicleState, vehicleEState.Destroyed) || Equals(vehicleState, vehicleEState.Disabled) || this.GetOwnerEntity().IsVehicleInsideInnerAreaOfAreaSpeedLimiter() {
        ScriptableDeviceComponentPS.SetActionsInactiveAll(outActions, "LocKey#27694");
      };
    };
    this.FinalizeGetQuickHackActions(outActions, context);
  }

  protected func OnToggleTakeOverControl(evt: ref<ToggleTakeOverControl>) -> EntityNotificationType {
    let triggerVehicleRemoteControlEvent: ref<TriggerVehicleRemoteControlEvent> = new TriggerVehicleRemoteControlEvent();
    triggerVehicleRemoteControlEvent.enable = true;
    triggerVehicleRemoteControlEvent.shouldUnseatPassengers = true;
    triggerVehicleRemoteControlEvent.shouldModifyInteractionState = true;
    this.SendStimsOnVehicleQuickhack(false, true);
    TakeOverControlSystem.ReleaseControlOfRemoteControlledVehicle(this.GetGameInstance(), GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerControlledGameObject().GetEntityID());
    TakeOverControlSystem.ReleaseControl(this.GetGameInstance(), triggerVehicleRemoteControlEvent, this.GetOwnerEntity().GetEntityID());
    return super.OnToggleTakeOverControl(evt);
  }

  protected func OnVehicleOverrideForceBrakes(evt: ref<VehicleOverrideForceBrakes>) -> EntityNotificationType {
    let controllerPS: ref<vehicleControllerPS>;
    this.GetOwnerEntity().ActivateNetrunnerQuickhack(VehicleNetrunnerQuickhackType.ForceBrakes);
    controllerPS = this.GetVehicleControllerPS();
    controllerPS.SetAlarm(true);
    this.SendStimsOnVehicleQuickhack(false, false);
    return super.OnVehicleOverrideForceBrakes(evt);
  }

  protected func OnVehicleOverrideExplode(evt: ref<VehicleOverrideExplode>) -> EntityNotificationType {
    this.GetOwnerEntity().ActivateNetrunnerQuickhack(VehicleNetrunnerQuickhackType.Explode);
    this.SendStimsOnVehicleQuickhack(true, false);
    return super.OnVehicleOverrideExplode(evt);
  }

  protected func OnVehicleOverrideAccelerate(evt: ref<VehicleOverrideAccelerate>) -> EntityNotificationType {
    this.GetOwnerEntity().EnableHighPriorityPanicDriving();
    this.GetOwnerEntity().ActivateNetrunnerQuickhack(VehicleNetrunnerQuickhackType.Accelerate);
    this.SendStimsOnVehicleQuickhack(true, false);
    return super.OnVehicleOverrideAccelerate(evt);
  }

  public final func OnPreventionVehicleHackedEvent(evt: ref<PreventionVehicleHackedEvent>) -> EntityNotificationType {
    let vehicleId: EntityID = this.GetOwnerEntity().GetEntityID();
    let driverPuppet: ref<ScriptedPuppet> = VehicleComponent.GetDriverMounted(this.GetGameInstance(), vehicleId) as ScriptedPuppet;
    if IsDefined(driverPuppet) {
      NPCPuppet.RevealPlayerPositionIfNeeded(driverPuppet, evt.instigatorID, true);
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final func OnCheckVehicleVelocityForStimsEvent(evt: ref<CheckVehicleVelocityForStimsEvent>) -> EntityNotificationType {
    if this.m_controlStimShouldBeActive {
      this.CheckVehicleVelocityForStims();
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  private final func CheckVehicleVelocityForStims() -> Void {
    let checkVelocityEvent: ref<CheckVehicleVelocityForStimsEvent>;
    let velocity: Float = Vector4.Length(this.GetOwnerEntity().GetLinearVelocity());
    let shouldRunStims: Bool = velocity > 0.10;
    if NotEquals(shouldRunStims, this.m_controlStimRunning) {
      if shouldRunStims {
        this.SendVehicleStimsOnVehicleQuickhack(-1.00);
      } else {
        this.EndVehicleStimsOnVehicleQuickhack();
      };
      this.m_controlStimRunning = shouldRunStims;
    };
    checkVelocityEvent = new CheckVehicleVelocityForStimsEvent();
    this.QueuePSEventWithDelay(this, checkVelocityEvent, 2.00);
  }

  protected final func SendStimsOnVehicleQuickhack(shouldTriggerIllegalActionStim: Bool, areHacksIndefiniteDuration: Bool) -> Void {
    let preventionVehicleHackedEvent: ref<PreventionVehicleHackedEvent>;
    let reevaluateDetectionRange: Float;
    let vehicleHackedEvent: ref<VehicleHackedEvent>;
    let vehicleQuickhackStimDuration: Float;
    let vehicleQuickhackStimVisualRange: Float;
    let player: ref<PlayerPuppet> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject() as PlayerPuppet;
    let playerBroadcaster: ref<StimBroadcasterComponent> = player.GetStimBroadcasterComponent();
    if shouldTriggerIllegalActionStim && IsDefined(playerBroadcaster) {
      vehicleQuickhackStimVisualRange = TweakDBInterface.GetFloat(t"player.vehicleQuickHacks.vehicleQuickhackStimVisualRange", 15.00);
      vehicleQuickhackStimDuration = TweakDBInterface.GetFloat(t"player.vehicleQuickHacks.vehicleQuickhackIllegalStimDuration", 2.00);
      playerBroadcaster.AddActiveStimuli(player, gamedataStimType.IllegalAction, vehicleQuickhackStimDuration, vehicleQuickhackStimVisualRange);
    };
    if areHacksIndefiniteDuration {
      StatusEffectHelper.ApplyStatusEffect(player, t"PreventionStatusEffect.PerformingIllegalAction");
      if IsDefined(playerBroadcaster) {
        reevaluateDetectionRange = TweakDBInterface.GetFloat(t"player.vehicleQuickHacks.reevaluateDetectionRange", 40.00);
        playerBroadcaster.TriggerSingleBroadcast(player, gamedataStimType.ReevaluateDetectionOverwrite, reevaluateDetectionRange);
      };
      this.m_controlStimShouldBeActive = true;
      this.m_controlStimRunning = false;
      this.CheckVehicleVelocityForStims();
    } else {
      vehicleQuickhackStimDuration = TweakDBInterface.GetFloat(t"player.vehicleQuickHacks.vehicleQuickhackAlarmStimDuration", 6.00);
      this.SendVehicleStimsOnVehicleQuickhack(vehicleQuickhackStimDuration);
    };
    if this.GetOwnerEntity().IsPrevention() {
      preventionVehicleHackedEvent = new PreventionVehicleHackedEvent();
      preventionVehicleHackedEvent.instigatorID = player.GetEntityID();
      this.QueuePSEventWithDelay(this, preventionVehicleHackedEvent, 2.00);
      vehicleHackedEvent = new VehicleHackedEvent();
      VehicleComponent.QueueEventToAllPassengers(this.GetGameInstance(), this.GetOwnerEntity().GetEntityID(), vehicleHackedEvent);
      PreventionSystem.CrimeWitnessRequestToPreventionSystem(this.GetGameInstance(), player.GetWorldPosition());
    };
    if this.IsConnectedToSecuritySystem() {
      this.GetOwnerEntity().GetDeviceLink().TriggerSecuritySystemNotification(this.GetOwnerEntity().GetWorldPosition(), player, ESecurityNotificationType.DEFAULT);
    };
  }

  protected final func SendVehicleStimsOnVehicleQuickhack(vehicleQuickhackStimDuration: Float) -> Void {
    let vehicleQuickhackStimAudioRange: Float;
    let vehicleQuickhackStimVisualRange: Float;
    let vehicleBroadcaster: ref<StimBroadcasterComponent> = this.GetOwnerEntity().GetStimBroadcasterComponent();
    if IsDefined(vehicleBroadcaster) {
      vehicleQuickhackStimAudioRange = TweakDBInterface.GetFloat(t"player.vehicleQuickHacks.vehicleQuickhackStimAudioRange", 13.00);
      vehicleBroadcaster.AddActiveStimuli(this.GetOwnerEntity(), gamedataStimType.CarAlarm, vehicleQuickhackStimDuration, vehicleQuickhackStimAudioRange);
      vehicleQuickhackStimVisualRange = TweakDBInterface.GetFloat(t"player.vehicleQuickHacks.vehicleQuickhackStimVisualRange", 15.00);
      vehicleBroadcaster.AddActiveStimuli(this.GetOwnerEntity(), gamedataStimType.CarAlarm, vehicleQuickhackStimDuration, vehicleQuickhackStimVisualRange, true);
    };
  }

  public final func EndStimsOnVehicleQuickhack() -> Void {
    let playerBroadcaster: ref<StimBroadcasterComponent>;
    let reevaluateDetectionRange: Float;
    let player: ref<PlayerPuppet> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject() as PlayerPuppet;
    StatusEffectHelper.RemoveStatusEffect(player, t"PreventionStatusEffect.PerformingIllegalAction");
    playerBroadcaster = player.GetStimBroadcasterComponent();
    if IsDefined(playerBroadcaster) {
      playerBroadcaster.RemoveActiveStimuliByName(player, gamedataStimType.IllegalAction);
      reevaluateDetectionRange = TweakDBInterface.GetFloat(t"player.vehicleQuickHacks.reevaluateDetectionRange", 40.00);
      playerBroadcaster.TriggerSingleBroadcast(player, gamedataStimType.ReevaluateDetectionOverwrite, reevaluateDetectionRange);
    };
    this.m_controlStimShouldBeActive = false;
    this.m_controlStimRunning = false;
    this.EndVehicleStimsOnVehicleQuickhack();
  }

  public final func EndVehicleStimsOnVehicleQuickhack() -> Void {
    let vehicleBroadcaster: ref<StimBroadcasterComponent> = this.GetOwnerEntity().GetStimBroadcasterComponent();
    if IsDefined(vehicleBroadcaster) {
      vehicleBroadcaster.RemoveActiveStimuliByName(this.GetOwnerEntity(), gamedataStimType.CarAlarm);
      vehicleBroadcaster.RemoveActiveStimuliByName(this.GetOwnerEntity(), gamedataStimType.CarAlarm);
    };
  }

  private final func ActionForceCarAlarm() -> ref<ForceCarAlarm> {
    let action: ref<ForceCarAlarm> = new ForceCarAlarm();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties(true);
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  private final func ActionForceDisableCarAlarm() -> ref<ForceDisableCarAlarm> {
    let action: ref<ForceDisableCarAlarm> = new ForceDisableCarAlarm();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties(false);
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  private final func ActionToggleVehicle(toggleOn: Bool) -> ref<ToggleVehicle> {
    let action: ref<ToggleVehicle> = new ToggleVehicle();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties(toggleOn);
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  private final func ActionOpenTrunk() -> ref<VehicleOpenTrunk> {
    let action: ref<VehicleOpenTrunk> = new VehicleOpenTrunk();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  private final func ActionCloseTrunk() -> ref<VehicleCloseTrunk> {
    let action: ref<VehicleCloseTrunk> = new VehicleCloseTrunk();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  private final func ActionOpenHood() -> ref<VehicleOpenHood> {
    let action: ref<VehicleOpenHood> = new VehicleOpenHood();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  private final func ActionCloseHood() -> ref<VehicleCloseHood> {
    let action: ref<VehicleCloseHood> = new VehicleCloseHood();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  private final func ActionVehicleDumpBody() -> ref<VehicleDumpBody> {
    let action: ref<VehicleDumpBody> = new VehicleDumpBody();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  private final func ActionVehicleTakeBody() -> ref<VehicleTakeBody> {
    let action: ref<VehicleTakeBody> = new VehicleTakeBody();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  private final func ActionPlayerTrunk() -> ref<VehiclePlayerTrunk> {
    let action: ref<VehiclePlayerTrunk> = new VehiclePlayerTrunk();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  private final func ActionVehicleDoorInteraction(const slotName: script_ref<String>, opt fromInteraction: Bool, opt locked: Bool) -> ref<VehicleDoorInteraction> {
    let action: ref<VehicleDoorInteraction> = new VehicleDoorInteraction();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties(slotName, fromInteraction, locked);
    action.AddDeviceName(this.GetDeviceName());
    if !locked {
      action.CreateInteraction(Deref(slotName));
    } else {
      action.CreateInteraction();
    };
    return action;
  }

  private final func ActionVehicleDoorInteractionStateChange(doorToChange: EVehicleDoor, desiredState: VehicleDoorInteractionState, const source: script_ref<String>) -> ref<VehicleDoorInteractionStateChange> {
    let action: ref<VehicleDoorInteractionStateChange> = new VehicleDoorInteractionStateChange();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties(doorToChange, desiredState, source);
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  private final func ActionVehicleDoorOpen(const slotName: script_ref<String>) -> ref<VehicleDoorOpen> {
    let action: ref<VehicleDoorOpen> = new VehicleDoorOpen();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties(slotName);
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction(Deref(slotName));
    return action;
  }

  private final func ActionVehicleDoorClose(const slotName: script_ref<String>) -> ref<VehicleDoorClose> {
    let action: ref<VehicleDoorClose> = new VehicleDoorClose();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties(slotName);
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction(Deref(slotName));
    return action;
  }

  private final func ActionVehicleForceOccupantOut(const slotName: script_ref<String>) -> ref<VehicleForceOccupantOut> {
    let action: ref<VehicleForceOccupantOut> = new VehicleForceOccupantOut();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties(slotName);
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    return action;
  }

  private final func ActionVehicleDoorQuestLocked() -> ref<VehicleQuestDoorLocked> {
    let action: ref<VehicleQuestDoorLocked> = new VehicleQuestDoorLocked();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.GetDeviceName());
    action.CreateInteraction();
    action.SetInactive();
    return action;
  }

  protected final func OnVehicleQuestChangeDoorStateEvent(evt: ref<VehicleQuestChangeDoorStateEvent>) -> EntityNotificationType {
    let InteractionStateChangeEvent: ref<VehicleDoorInteractionStateChange>;
    let doorCloseEvent: ref<VehicleDoorClose>;
    let doorOpenEvent: ref<VehicleDoorOpen>;
    let desiredState: EQuestVehicleDoorState = evt.newState;
    switch desiredState {
      case EQuestVehicleDoorState.ForceOpen:
        doorOpenEvent = new VehicleDoorOpen();
        doorOpenEvent.slotID = EnumValueToName(n"EVehicleDoor", Cast<Int64>(EnumInt(evt.door)));
        doorOpenEvent.forceScene = evt.forceScene;
        this.QueuePSEvent(this, doorOpenEvent);
        break;
      case EQuestVehicleDoorState.ForceClose:
        doorCloseEvent = new VehicleDoorClose();
        doorCloseEvent.slotID = EnumValueToName(n"EVehicleDoor", Cast<Int64>(EnumInt(evt.door)));
        doorCloseEvent.forceScene = evt.forceScene;
        this.QueuePSEvent(this, doorCloseEvent);
        break;
      case EQuestVehicleDoorState.OpenAll:
        this.OpenAllVehDoors(evt.forceScene);
        break;
      case EQuestVehicleDoorState.CloseAll:
        this.CloseAllVehDoors(evt.forceScene);
        break;
      case EQuestVehicleDoorState.OpenAllRegular:
        this.OpenAllRegularVehDoors(evt.forceScene);
        break;
      case EQuestVehicleDoorState.ForceLock:
        InteractionStateChangeEvent = new VehicleDoorInteractionStateChange();
        InteractionStateChangeEvent.door = evt.door;
        InteractionStateChangeEvent.newState = VehicleDoorInteractionState.Locked;
        InteractionStateChangeEvent.source = "QuestForceLock";
        this.QueuePSEvent(this, InteractionStateChangeEvent);
        break;
      case EQuestVehicleDoorState.ForceUnlock:
        InteractionStateChangeEvent = new VehicleDoorInteractionStateChange();
        InteractionStateChangeEvent.door = evt.door;
        InteractionStateChangeEvent.newState = VehicleDoorInteractionState.Available;
        InteractionStateChangeEvent.source = "QuestForceUnlock";
        this.QueuePSEvent(this, InteractionStateChangeEvent);
        break;
      case EQuestVehicleDoorState.LockAll:
        this.LockAllVehDoors();
        break;
      case EQuestVehicleDoorState.EnableInteraction:
        InteractionStateChangeEvent = new VehicleDoorInteractionStateChange();
        InteractionStateChangeEvent.door = evt.door;
        InteractionStateChangeEvent.newState = VehicleDoorInteractionState.Available;
        InteractionStateChangeEvent.source = "QuestEnableInteraction";
        this.QueuePSEvent(this, InteractionStateChangeEvent);
        break;
      case EQuestVehicleDoorState.DisableInteraction:
        InteractionStateChangeEvent = new VehicleDoorInteractionStateChange();
        InteractionStateChangeEvent.door = evt.door;
        InteractionStateChangeEvent.newState = VehicleDoorInteractionState.Disabled;
        InteractionStateChangeEvent.source = "QuestDisableInteraction";
        this.QueuePSEvent(this, InteractionStateChangeEvent);
        break;
      case EQuestVehicleDoorState.DisableAllInteractions:
        this.DisableAllVehInteractions();
        break;
      case EQuestVehicleDoorState.ResetInteractions:
        this.ResetVehicleInteractionState();
        break;
      case EQuestVehicleDoorState.ResetVehicle:
        this.ResetVehicle();
        break;
      case EQuestVehicleDoorState.QuestLock:
        InteractionStateChangeEvent = new VehicleDoorInteractionStateChange();
        InteractionStateChangeEvent.door = evt.door;
        InteractionStateChangeEvent.newState = VehicleDoorInteractionState.QuestLocked;
        InteractionStateChangeEvent.source = "QuestLock";
        this.QueuePSEvent(this, InteractionStateChangeEvent);
        break;
      case EQuestVehicleDoorState.QuestLockAll:
        this.QuestLockAllVehDoors();
        break;
      default:
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected final func OnVehicleQuestChangeWindowStateEvent(evt: ref<vehicleChangeWindowStateEvent>) -> EntityNotificationType {
    let windowCloseEvent: ref<VehicleWindowClose>;
    let windowOpenEvent: ref<VehicleWindowOpen>;
    let desiredState: EQuestVehicleWindowState = evt.state;
    switch desiredState {
      case EQuestVehicleWindowState.OpenAll:
        this.OpenAllVehWindows();
        break;
      case EQuestVehicleWindowState.CloseAll:
        this.CloseAllVehWindows();
        break;
      case EQuestVehicleWindowState.ForceOpen:
        windowOpenEvent = new VehicleWindowOpen();
        windowOpenEvent.slotID = EnumValueToName(n"EVehicleDoor", Cast<Int64>(EnumInt(evt.door)));
        this.QueuePSEvent(this, windowOpenEvent);
        break;
      case EQuestVehicleWindowState.ForceClose:
        windowCloseEvent = new VehicleWindowClose();
        windowCloseEvent.slotID = EnumValueToName(n"EVehicleDoor", Cast<Int64>(EnumInt(evt.door)));
        this.QueuePSEvent(this, windowCloseEvent);
        break;
      default:
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  public final func OnToggleDoorWrapperEvent(evt: ref<vehicleToggleDoorWrapperEvent>) -> EntityNotificationType {
    let newEvt: ref<VehicleQuestChangeDoorStateEvent> = new VehicleQuestChangeDoorStateEvent();
    newEvt.door = evt.door;
    newEvt.newState = evt.action;
    newEvt.forceScene = evt.forceScene;
    this.OnVehicleQuestChangeDoorStateEvent(newEvt);
    this.SetHasStateBeenModifiedByQuest(true);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  private final func OpenAllVehDoors(forceScene: Bool) -> Void {
    let doorOpenEvent: ref<VehicleDoorOpen>;
    let size: Int32 = 6;
    let i: Int32 = 0;
    while i < size {
      doorOpenEvent = new VehicleDoorOpen();
      doorOpenEvent.slotID = EnumValueToName(n"EVehicleDoor", EnumInt(IntEnum<EVehicleDoor>(i)));
      doorOpenEvent.forceScene = forceScene;
      this.QueuePSEvent(this, doorOpenEvent);
      i += 1;
    };
  }

  public final func OpenAllRegularVehDoors(forceScene: Bool) -> Void {
    let doorOpenEvent: ref<VehicleDoorOpen>;
    let seatSet: array<wref<VehicleSeat_Record>> = this.GetSeats();
    let i: Int32 = 0;
    while i < ArraySize(seatSet) {
      doorOpenEvent = new VehicleDoorOpen();
      doorOpenEvent.slotID = seatSet[i].SeatName();
      doorOpenEvent.forceScene = forceScene;
      this.QueuePSEvent(this, doorOpenEvent);
      i += 1;
    };
  }

  private final func CloseAllVehDoors(forceScene: Bool) -> Void {
    let doorCloseEvent: ref<VehicleDoorClose>;
    let size: Int32 = 6;
    let i: Int32 = 0;
    while i < size {
      if Equals(this.GetDoorState(IntEnum<EVehicleDoor>(i)), VehicleDoorState.Open) {
        doorCloseEvent = new VehicleDoorClose();
        doorCloseEvent.slotID = EnumValueToName(n"EVehicleDoor", EnumInt(IntEnum<EVehicleDoor>(i)));
        doorCloseEvent.forceScene = forceScene;
        this.QueuePSEvent(this, doorCloseEvent);
      };
      i += 1;
    };
  }

  private final func LockAllVehDoors() -> Void {
    let InteractionStateChangeEvent: ref<VehicleDoorInteractionStateChange>;
    let size: Int32 = 6;
    let i: Int32 = 0;
    while i < size {
      InteractionStateChangeEvent = new VehicleDoorInteractionStateChange();
      InteractionStateChangeEvent.source = "LockAllVehDoors";
      InteractionStateChangeEvent.door = IntEnum<EVehicleDoor>(i);
      InteractionStateChangeEvent.newState = VehicleDoorInteractionState.Locked;
      this.QueuePSEvent(this, InteractionStateChangeEvent);
      i += 1;
    };
  }

  private final func UnlockAllVehDoors() -> Void {
    let InteractionStateChangeEvent: ref<VehicleDoorInteractionStateChange>;
    let size: Int32 = 6;
    let i: Int32 = 0;
    while i < size {
      InteractionStateChangeEvent = new VehicleDoorInteractionStateChange();
      InteractionStateChangeEvent.source = "UnlockAllVehDoors";
      InteractionStateChangeEvent.door = IntEnum<EVehicleDoor>(i);
      InteractionStateChangeEvent.newState = VehicleDoorInteractionState.Available;
      this.QueuePSEvent(this, InteractionStateChangeEvent);
      i += 1;
    };
  }

  private final func OpenAllVehWindows() -> Void {
    let windowOpenEvent: ref<VehicleWindowOpen>;
    let size: Int32 = 6;
    let i: Int32 = 0;
    while i < size {
      windowOpenEvent = new VehicleWindowOpen();
      windowOpenEvent.slotID = EnumValueToName(n"EVehicleDoor", EnumInt(IntEnum<EVehicleDoor>(i)));
      this.QueuePSEvent(this, windowOpenEvent);
      i += 1;
    };
  }

  private final func CloseAllVehWindows() -> Void {
    let windowCloseEvent: ref<VehicleWindowClose>;
    let size: Int32 = 6;
    let i: Int32 = 0;
    while i < size {
      windowCloseEvent = new VehicleWindowClose();
      windowCloseEvent.slotID = EnumValueToName(n"EVehicleDoor", EnumInt(IntEnum<EVehicleDoor>(i)));
      this.QueuePSEvent(this, windowCloseEvent);
      i += 1;
    };
  }

  private final func DisableAllVehInteractions() -> Void {
    let InteractionStateChangeEvent: ref<VehicleDoorInteractionStateChange>;
    let size: Int32 = 6;
    let i: Int32 = 0;
    while i < size {
      InteractionStateChangeEvent = new VehicleDoorInteractionStateChange();
      InteractionStateChangeEvent.source = "DisableAllVehInteractions";
      InteractionStateChangeEvent.door = IntEnum<EVehicleDoor>(i);
      InteractionStateChangeEvent.newState = VehicleDoorInteractionState.Disabled;
      this.QueuePSEvent(this, InteractionStateChangeEvent);
      i += 1;
    };
  }

  private final func ResetVehicleInteractionState() -> Void {
    let InteractionStateChangeEvent: ref<VehicleDoorInteractionStateChange>;
    let size: Int32 = 6;
    let i: Int32 = 0;
    while i < size {
      InteractionStateChangeEvent = new VehicleDoorInteractionStateChange();
      InteractionStateChangeEvent.source = "ResetVehicleInteractionState";
      InteractionStateChangeEvent.door = IntEnum<EVehicleDoor>(i);
      if Equals(IntEnum<EVehicleDoor>(i), EVehicleDoor.seat_front_left) || Equals(IntEnum<EVehicleDoor>(i), EVehicleDoor.seat_front_right) {
        InteractionStateChangeEvent.newState = VehicleDoorInteractionState.Available;
        this.QueuePSEvent(this, InteractionStateChangeEvent);
      } else {
        if Equals(IntEnum<EVehicleDoor>(i), EVehicleDoor.seat_back_left) || Equals(IntEnum<EVehicleDoor>(i), EVehicleDoor.seat_back_right) {
          InteractionStateChangeEvent.newState = VehicleDoorInteractionState.Disabled;
          this.QueuePSEvent(this, InteractionStateChangeEvent);
        };
      };
      i += 1;
    };
    this.SetHasStateBeenModifiedByQuest(false);
  }

  private final func ResetVehicle() -> Void {
    let InteractionStateChangeEvent: ref<VehicleDoorInteractionStateChange>;
    let size: Int32 = 6;
    let i: Int32 = 0;
    while i < size {
      InteractionStateChangeEvent = new VehicleDoorInteractionStateChange();
      InteractionStateChangeEvent.door = IntEnum<EVehicleDoor>(i);
      InteractionStateChangeEvent.source = "ResetVehicle";
      if Equals(IntEnum<EVehicleDoor>(i), EVehicleDoor.seat_front_left) || Equals(IntEnum<EVehicleDoor>(i), EVehicleDoor.seat_front_right) {
        InteractionStateChangeEvent.newState = VehicleDoorInteractionState.Available;
        this.QueuePSEvent(this, InteractionStateChangeEvent);
      } else {
        if Equals(IntEnum<EVehicleDoor>(i), EVehicleDoor.seat_back_left) || Equals(IntEnum<EVehicleDoor>(i), EVehicleDoor.seat_back_right) {
          InteractionStateChangeEvent.newState = VehicleDoorInteractionState.Disabled;
          this.QueuePSEvent(this, InteractionStateChangeEvent);
        };
      };
      i += 1;
    };
    this.SetHasStateBeenModifiedByQuest(false);
  }

  private final func QuestLockAllVehDoors() -> Void {
    let InteractionStateChangeEvent: ref<VehicleDoorInteractionStateChange>;
    let size: Int32 = 6;
    let i: Int32 = 0;
    while i < size {
      InteractionStateChangeEvent = new VehicleDoorInteractionStateChange();
      InteractionStateChangeEvent.source = "QuestLockAllVehDoors";
      InteractionStateChangeEvent.door = IntEnum<EVehicleDoor>(i);
      InteractionStateChangeEvent.newState = VehicleDoorInteractionState.QuestLocked;
      this.QueuePSEvent(this, InteractionStateChangeEvent);
      i += 1;
    };
  }

  private final func OnVehicleSeatReservationEvent(evt: ref<VehicleSeatReservationEvent>) -> EntityNotificationType {
    let currentState: VehicleDoorInteractionState;
    let door: EVehicleDoor;
    let previousState: VehicleDoorInteractionState;
    this.GetVehicleDoorEnum(door, evt.slotID);
    if evt.reserve {
      currentState = this.GetDoorInteractionState(door);
      if Equals(currentState, VehicleDoorInteractionState.Disabled) {
        return EntityNotificationType.DoNotNotifyEntity;
      };
      this.SetTempDoorInteractionState(door, this.GetDoorInteractionState(door));
      this.SetDoorInteractionState(door, VehicleDoorInteractionState.Reserved, "ReservationStart");
    };
    if !evt.reserve {
      previousState = this.GetTempDoorInteractionState(door);
      currentState = this.GetDoorInteractionState(door);
      if Equals(currentState, VehicleDoorInteractionState.Reserved) {
        this.SetDoorInteractionState(door, previousState, "ReservationEnd");
      };
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func GetSeats() -> [wref<VehicleSeat_Record>] {
    let seatSet: array<wref<VehicleSeat_Record>>;
    VehicleComponent.GetSeats(this.GetGameInstance(), this.GetOwnerEntity(), seatSet);
    return seatSet;
  }

  protected final func OnVehicleQuestCrystalDomeEvent(evt: ref<VehicleQuestCrystalDomeEvent>) -> EntityNotificationType {
    let toggle: Bool = evt.toggle;
    this.SetCrystalDomeQuestState(toggle);
    if evt.removeQuestControl {
      this.SetIsCrystalDomeQuestModified(false);
    } else {
      this.SetIsCrystalDomeQuestModified(true);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehicleQuestSirenEvent(evt: ref<VehicleQuestSirenEvent>) -> EntityNotificationType {
    this.SetSirenLightsState(evt.lights);
    this.SetSirenSoundsState(evt.sounds);
    if evt.lights || evt.sounds {
      this.SetSirenState(true);
    };
    if !evt.lights && !evt.sounds {
      this.SetSirenState(false);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehicleLightQuestToggleEvent(evt: ref<VehicleLightQuestToggleEvent>) -> EntityNotificationType {
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehicleLightQuestChangeColorEvent(evt: ref<VehicleLightQuestChangeColorEvent>) -> EntityNotificationType {
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehicleQuestHornEvent(evt: ref<VehicleQuestHornEvent>) -> EntityNotificationType {
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehicleDelayedQuestHornEvent(evt: ref<VehicleQuestDelayedHornEvent>) -> EntityNotificationType {
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehicleQuestVisualDestructionEvent(evt: ref<VehicleQuestVisualDestructionEvent>) -> EntityNotificationType {
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehicleDetachPartEvent(evt: ref<VehicleDetachPartEvent>) -> EntityNotificationType {
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehicleQuestAVThrusterEvent(evt: ref<VehicleQuestAVThrusterEvent>) -> EntityNotificationType {
    this.SetThrusterState(evt.enable);
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehicleQuestUIEvent(evt: ref<VehicleQuestEnableUIEvent>) -> EntityNotificationType {
    let uiSystem: ref<UISystem> = GameInstance.GetUISystem(this.GetGameInstance());
    let forwardEvent: ref<ForwardVehicleQuestEnableUIEvent> = new ForwardVehicleQuestEnableUIEvent();
    forwardEvent.mode = evt.mode;
    uiSystem.QueueEvent(forwardEvent);
    if Equals(evt.mode, vehicleQuestUIEnable.Gameplay) {
      this.SetIsUiQuestModified(false);
      return EntityNotificationType.DoNotNotifyEntity;
    };
    this.SetIsUiQuestModified(true);
    if Equals(evt.mode, vehicleQuestUIEnable.ForceEnable) {
      this.SetUiQuestState(true);
    };
    if Equals(evt.mode, vehicleQuestUIEnable.ForceDisable) {
      this.SetUiQuestState(false);
    };
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected final func OnVehicleQuestUIEffectEvent(evt: ref<VehicleQuestUIEffectEvent>) -> EntityNotificationType {
    let uiSystem: ref<UISystem> = GameInstance.GetUISystem(this.GetGameInstance());
    let forwardEvent: ref<ForwardVehicleQuestUIEffectEvent> = new ForwardVehicleQuestUIEffectEvent();
    forwardEvent.glitch = evt.glitch;
    forwardEvent.panamVehicleStartup = evt.panamVehicleStartup;
    forwardEvent.panamScreenType1 = evt.panamScreenType1;
    forwardEvent.panamScreenType2 = evt.panamScreenType2;
    forwardEvent.panamScreenType3 = evt.panamScreenType3;
    forwardEvent.panamScreenType4 = evt.panamScreenType4;
    uiSystem.QueueEvent(forwardEvent);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected final func OnVehicleRadioEvent(evt: ref<VehicleRadioEvent>) -> EntityNotificationType {
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehicleQuestWindowDestructionEvent(evt: ref<VehicleQuestWindowDestructionEvent>) -> EntityNotificationType {
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehicleRaceQuestEvent(evt: ref<VehicleRaceQuestEvent>) -> EntityNotificationType {
    let uiSystem: ref<UISystem> = GameInstance.GetUISystem(this.GetGameInstance());
    let forwardEvent: ref<ForwardVehicleRaceUIEvent> = new ForwardVehicleRaceUIEvent();
    forwardEvent.mode = evt.mode;
    forwardEvent.maxPosition = evt.maxPosition;
    forwardEvent.maxCheckpoints = evt.maxCheckpoints;
    uiSystem.QueueEvent(forwardEvent);
    switch evt.mode {
      case vehicleRaceUI.PreRaceSetup:
        uiSystem.PushGameContext(UIGameContext.VehicleRace);
        break;
      case vehicleRaceUI.Disable:
        uiSystem.PopGameContext(UIGameContext.VehicleRace);
    };
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func OnVehiclePanzerBootupUIQuestEvent(evt: ref<VehiclePanzerBootupUIQuestEvent>) -> EntityNotificationType {
    let uiSystem: ref<UISystem> = GameInstance.GetUISystem(this.GetGameInstance());
    let forwardEvent: ref<VehiclePanzerBootupUIQuestEvent> = new VehiclePanzerBootupUIQuestEvent();
    forwardEvent.mode = evt.mode;
    uiSystem.QueueEvent(forwardEvent);
    return EntityNotificationType.DoNotNotifyEntity;
  }

  protected final func OnVehicleQuestToggleEngineEvent(evt: ref<VehicleQuestToggleEngineEvent>) -> EntityNotificationType {
    return EntityNotificationType.SendThisEventToEntity;
  }
}

public class VehicleQuestChangeDoorStateEvent extends Event {

  public let door: EVehicleDoor;

  public let newState: EQuestVehicleDoorState;

  public let forceScene: Bool;

  public final func GetFriendlyDescription() -> String {
    return "Change Veh Door states";
  }
}

public class VehicleQuestToggleEngineEvent extends Event {

  public edit let toggle: Bool;

  @default(VehicleQuestToggleEngineEvent, VehicleQuestEngineLockState.DontToggleIfLocked)
  public edit let lockState: VehicleQuestEngineLockState;

  @runtimeProperty("tooltip", "Set this to True with 'toggle' True to turn on the vehicle, but leave the engine off. Set this to True with 'toggle' false to turn off the engine, but leave vehicle on.")
  @default(VehicleQuestToggleEngineEvent, false)
  public edit let vehicleOnEngineOff: Bool;

  public final func GetFriendlyDescription() -> String {
    return "Enable/Disable Vehicle Engine";
  }
}

public class VehicleQuestCrystalDomeEvent extends Event {

  public edit let toggle: Bool;

  public edit let removeQuestControl: Bool;

  public final func GetFriendlyDescription() -> String {
    return "Toggle Crystal Dome on and off";
  }
}

public class VehicleQuestSirenEvent extends Event {

  public edit let lights: Bool;

  public edit let sounds: Bool;

  public final func GetFriendlyDescription() -> String {
    return "Toggle Sirens on and off";
  }
}

public class VehicleLightQuestToggleEvent extends Event {

  public edit let toggle: Bool;

  @default(VehicleLightQuestToggleEvent, vehicleELightType.Default)
  public let lightType: vehicleELightType;

  public final func GetFriendlyDescription() -> String {
    return "Toggle lightType ON/OFF";
  }
}

public class VehicleLightQuestChangeColorEvent extends Event {

  public edit let color: Color;

  @default(VehicleLightQuestChangeColorEvent, vehicleELightType.Default)
  public let lightType: vehicleELightType;

  @default(VehicleLightQuestChangeColorEvent, false)
  public let forceOverrideEmissiveColor: Bool;

  public final func GetFriendlyDescription() -> String {
    return "Change light color";
  }
}

public class VehicleQuestHornEvent extends Event {

  public edit let honkTime: Float;

  public final func GetFriendlyDescription() -> String {
    return "Honk the horn";
  }
}

public class VehicleQuestDelayedHornEvent extends Event {

  public edit let honkTime: Float;

  public edit let delayTime: Float;

  public final func GetFriendlyDescription() -> String {
    return "Delay Honk the horn";
  }
}

public class VehicleQuestVisualDestructionEvent extends Event {

  public edit let accumulate: Bool;

  public edit let frontLeft: Float;

  public edit let frontRight: Float;

  public edit let front: Float;

  public edit let right: Float;

  public edit let left: Float;

  public edit let backLeft: Float;

  public edit let backRight: Float;

  public edit let back: Float;

  public edit let roof: Float;

  public final func GetFriendlyDescription() -> String {
    return "Set Visual Deformation";
  }
}

public class VehicleQuestAVThrusterEvent extends Event {

  public edit let enable: Bool;

  public final func GetFriendlyDescription() -> String {
    return "Enable/Disable AV thruster FX";
  }
}

public class VehicleRadioEvent extends Event {

  public edit let toggle: Bool;

  public edit let setStation: Bool;

  @runtimeProperty("tooltip", "0: Aggro Ind   1: Elec Ind   2: HipHop   3: Aggro Techno   4: Downtempo   5: Att Rock   6: Pop   7: Latino   8: Metal   9: Minimal Techno   10: Jazz	11: Growl FM	12: Dark Star	13: Impulse")
  public edit let station: Int32;

  public final func GetFriendlyDescription() -> String {
    return "Toggle radio or set station";
  }
}

public class VehicleQuestEnableUIEvent extends Event {

  @default(VehicleQuestEnableUIEvent, vehicleQuestUIEnable.Gameplay)
  public edit let mode: vehicleQuestUIEnable;

  public final func GetFriendlyDescription() -> String {
    return "Enable/Disable Vehicle UI";
  }
}

public class VehicleQuestUIEffectEvent extends Event {

  public edit let glitch: Bool;

  public edit let panamVehicleStartup: Bool;

  public edit let panamScreenType1: Bool;

  public edit let panamScreenType2: Bool;

  public edit let panamScreenType3: Bool;

  public edit let panamScreenType4: Bool;

  public final func GetFriendlyDescription() -> String {
    return "Trigger vehicle UI effects";
  }
}

public class VehicleRaceQuestEvent extends Event {

  @default(VehicleRaceQuestEvent, vehicleRaceUI.PreRaceSetup)
  public edit let mode: vehicleRaceUI;

  public edit let maxPosition: Int32;

  public edit let maxCheckpoints: Int32;

  public final func GetFriendlyDescription() -> String {
    return "Manage Race UI";
  }
}

public struct GenericTemplatePersistentData {

  @default(GenericTemplatePersistentData, false)
  public persistent let primaryColorDefined: Bool;

  public persistent let primaryColorR: Uint8;

  public persistent let primaryColorG: Uint8;

  public persistent let primaryColorB: Uint8;

  @default(GenericTemplatePersistentData, false)
  public persistent let secondaryColorDefined: Bool;

  public persistent let secondaryColorR: Uint8;

  public persistent let secondaryColorG: Uint8;

  public persistent let secondaryColorB: Uint8;

  @default(GenericTemplatePersistentData, false)
  public persistent let lightsColorDefined: Bool;

  public persistent let lightsColorHue: Float;

  public final static func Equals(dataA: GenericTemplatePersistentData, dataB: GenericTemplatePersistentData) -> Bool {
    let c_tolerance: Int32 = 2;
    return Equals(dataA.primaryColorDefined, dataB.primaryColorDefined) && Equals(dataA.secondaryColorDefined, dataB.secondaryColorDefined) && Equals(dataA.lightsColorDefined, dataB.lightsColorDefined) && Color.Equals(GenericTemplatePersistentData.GetPrimaryColor(dataA), GenericTemplatePersistentData.GetPrimaryColor(dataB), c_tolerance) && Color.Equals(GenericTemplatePersistentData.GetSecondaryColor(dataA), GenericTemplatePersistentData.GetSecondaryColor(dataB), c_tolerance) && AbsF(dataA.lightsColorHue - dataB.lightsColorHue) <= Cast<Float>(c_tolerance);
  }

  public final static func GetPrimaryColor(data: GenericTemplatePersistentData) -> Color {
    return new Color(data.primaryColorR, data.primaryColorG, data.primaryColorB, 255u);
  }

  public final static func SetPrimaryColor(out data: GenericTemplatePersistentData, color: Color) -> Void {
    data.primaryColorR = color.Red;
    data.primaryColorG = color.Green;
    data.primaryColorB = color.Blue;
  }

  public final static func GetSecondaryColor(data: GenericTemplatePersistentData) -> Color {
    return new Color(data.secondaryColorR, data.secondaryColorG, data.secondaryColorB, 255u);
  }

  public final static func SetSecondaryColor(out data: GenericTemplatePersistentData, color: Color) -> Void {
    data.secondaryColorR = color.Red;
    data.secondaryColorG = color.Green;
    data.secondaryColorB = color.Blue;
  }
}

public native struct VehicleCustomMultilayer {

  public native let affectedComponents: [CName];

  public native let excludedComponents: [CName];

  public native let customMlSetup: ResRef;

  public native let customMlMask: ResRef;

  public native let coatLayerMin: Float;

  public native let coatLayerMax: Float;

  public final static func FromRecord(record: ref<VehicleCustomMultilayer_Record>) -> VehicleCustomMultilayer {
    let ret: VehicleCustomMultilayer;
    ret.affectedComponents = record.AffectedComponents();
    ret.excludedComponents = record.ExcludedComponents();
    ret.customMlSetup = record.CustomMlSetup();
    ret.customMlMask = record.CustomMlMask();
    ret.coatLayerMin = record.CoatLayerMin();
    ret.coatLayerMax = record.CoatLayerMax();
    return ret;
  }
}

public struct UniqueTemplateData {

  public let recordId: TweakDBID;

  public let twintoneModelName: CName;

  public let customIcon: ref<UIIcon_Record>;

  public let customMultilayers: [VehicleCustomMultilayer];

  public let customDecals: [VehicleDecalAttachmentData];

  public let globalClearCoatOverrides: VehicleClearCoatOverrides;

  public let partsClearCoatOverrides: [VehiclePartsClearCoatOverrides];

  public final static func Equals(dataA: UniqueTemplateData, dataB: UniqueTemplateData) -> Bool {
    return dataA.recordId == dataB.recordId;
  }
}

public struct VehicleVisualCustomizationTemplate {

  @default(VehicleVisualCustomizationTemplate, false)
  public let hasUniqueTemplate: Bool;

  public let genericData: GenericTemplatePersistentData;

  public let uniqueData: UniqueTemplateData;

  public final static func IsValid(template: VehicleVisualCustomizationTemplate) -> Bool {
    if template.hasUniqueTemplate {
      return ArraySize(template.uniqueData.customMultilayers) > 0;
    };
    return template.genericData.primaryColorDefined || template.genericData.lightsColorDefined;
  }

  public final static func IsLightsOnly(template: VehicleVisualCustomizationTemplate) -> Bool {
    return !(template.genericData.primaryColorDefined || template.hasUniqueTemplate);
  }

  public final static func GetType(template: VehicleVisualCustomizationTemplate) -> VehicleVisualCustomizationType {
    return template.hasUniqueTemplate ? VehicleVisualCustomizationType.Unique : VehicleVisualCustomizationType.Generic;
  }

  public final static func Equals(templateA: VehicleVisualCustomizationTemplate, templateB: VehicleVisualCustomizationTemplate) -> Bool {
    if templateA.hasUniqueTemplate && templateB.hasUniqueTemplate {
      return UniqueTemplateData.Equals(templateA.uniqueData, templateB.uniqueData);
    };
    if !templateA.hasUniqueTemplate && !templateB.hasUniqueTemplate {
      return GenericTemplatePersistentData.Equals(templateA.genericData, templateB.genericData);
    };
    return false;
  }

  public final static func FromRecord(templateRecord: ref<VehicleColorTemplate_Record>, twintoneModelName: CName) -> VehicleVisualCustomizationTemplate {
    let i: Int32;
    let vehicleColorTemplate: VehicleVisualCustomizationTemplate;
    if !IsDefined(templateRecord) {
      return vehicleColorTemplate;
    };
    if templateRecord.HasCustomPattern() {
      vehicleColorTemplate.hasUniqueTemplate = true;
      vehicleColorTemplate.uniqueData.twintoneModelName = twintoneModelName;
      vehicleColorTemplate.uniqueData.customIcon = templateRecord.CustomIconHandle();
      vehicleColorTemplate.uniqueData.recordId = templateRecord.GetRecordID();
      i = 0;
      while i < templateRecord.GetCustomMultilayersCount() {
        ArrayPush(vehicleColorTemplate.uniqueData.customMultilayers, VehicleCustomMultilayer.FromRecord(templateRecord.GetCustomMultilayersItemHandle(i)));
        i += 1;
      };
      i = 0;
      while i < templateRecord.GetCustomDecalsCount() {
        ArrayPush(vehicleColorTemplate.uniqueData.customDecals, VehicleDecalAttachmentData.FromRecord(templateRecord.GetCustomDecalsItemHandle(i)));
        i += 1;
      };
      vehicleColorTemplate.uniqueData.globalClearCoatOverrides = VehicleClearCoatOverrides.FromRecord(templateRecord.GlobalClearCoatOverridesHandle());
      i = 0;
      while i < templateRecord.GetPartsClearCoatOverridesCount() {
        ArrayPush(vehicleColorTemplate.uniqueData.partsClearCoatOverrides, VehiclePartsClearCoatOverrides.FromRecord(templateRecord.GetPartsClearCoatOverridesItemHandle(i)));
        i += 1;
      };
    } else {
      if templateRecord.GetMainColorCount() == 3 {
        vehicleColorTemplate.genericData.primaryColorDefined = true;
        GenericTemplatePersistentData.SetPrimaryColor(vehicleColorTemplate.genericData, Color.ToLinear(new Color(Cast<Uint8>(templateRecord.GetMainColorItem(0)), Cast<Uint8>(templateRecord.GetMainColorItem(1)), Cast<Uint8>(templateRecord.GetMainColorItem(2)), 255u)));
      };
      if templateRecord.GetSecondaryColorCount() == 3 {
        vehicleColorTemplate.genericData.secondaryColorDefined = true;
        GenericTemplatePersistentData.SetSecondaryColor(vehicleColorTemplate.genericData, Color.ToLinear(new Color(Cast<Uint8>(templateRecord.GetSecondaryColorItem(0)), Cast<Uint8>(templateRecord.GetSecondaryColorItem(1)), Cast<Uint8>(templateRecord.GetSecondaryColorItem(2)), 255u)));
      };
      if templateRecord.LightsHue() != -1.00 {
        vehicleColorTemplate.genericData.lightsColorDefined = true;
        vehicleColorTemplate.genericData.lightsColorHue = templateRecord.LightsHue();
      };
    };
    return vehicleColorTemplate;
  }
}

public struct SavedVehicleVisualCustomizationTemplate {

  @default(SavedVehicleVisualCustomizationTemplate, false)
  public persistent let hasUniqueTemplate: Bool;

  public persistent let genericData: GenericTemplatePersistentData;

  public persistent let uniqueDataID: TweakDBID;

  public persistent let twintoneModelName: CName;

  public final static func FromVehicleVisualCustomizationTemplate(template: VehicleVisualCustomizationTemplate) -> SavedVehicleVisualCustomizationTemplate {
    let savedData: SavedVehicleVisualCustomizationTemplate;
    savedData.hasUniqueTemplate = template.hasUniqueTemplate;
    savedData.twintoneModelName = template.uniqueData.twintoneModelName;
    if template.hasUniqueTemplate {
      savedData.uniqueDataID = template.uniqueData.recordId;
    } else {
      savedData.genericData = template.genericData;
    };
    return savedData;
  }

  public final static func ToVehicleVisualCustomizationTemplate(savedData: SavedVehicleVisualCustomizationTemplate) -> VehicleVisualCustomizationTemplate {
    let template: VehicleVisualCustomizationTemplate;
    if savedData.hasUniqueTemplate {
      template = VehicleVisualCustomizationTemplate.FromRecord(TweakDBInterface.GetVehicleColorTemplateRecord(savedData.uniqueDataID), savedData.twintoneModelName);
    } else {
      template.hasUniqueTemplate = false;
      template.genericData = savedData.genericData;
    };
    return template;
  }
}

public struct vehicleVisualModdingDefinition {

  public persistent let primaryColorDefined: Bool;

  public persistent let primaryColorH: Float;

  public persistent let primaryColorS: Float;

  public persistent let primaryColorB: Float;

  public persistent let secondaryColorDefined: Bool;

  public persistent let secondaryColorH: Float;

  public persistent let secondaryColorS: Float;

  public persistent let secondaryColorB: Float;

  public persistent let lightsColorDefined: Bool;

  public persistent let lightsColorH: Float;

  public persistent let decoPreset: Int32;

  public persistent let defaultAppearance: CName;

  public final static func UpdateToNewFormat(oldData: vehicleVisualModdingDefinition) -> VehicleVisualCustomizationTemplate {
    let updatedData: VehicleVisualCustomizationTemplate;
    if oldData.primaryColorDefined {
      updatedData.genericData.primaryColorDefined = true;
      GenericTemplatePersistentData.SetPrimaryColor(updatedData.genericData, Color.HSBToColor(Rad2Deg(oldData.primaryColorH), false, oldData.primaryColorS, oldData.primaryColorB * 0.80));
    };
    if oldData.secondaryColorDefined {
      updatedData.genericData.secondaryColorDefined = true;
      GenericTemplatePersistentData.SetSecondaryColor(updatedData.genericData, Color.HSBToColor(Rad2Deg(oldData.secondaryColorH), false, oldData.secondaryColorS, oldData.secondaryColorB * 0.80));
    };
    if oldData.lightsColorDefined {
      updatedData.genericData.lightsColorDefined = true;
      updatedData.genericData.lightsColorHue = Rad2Deg(oldData.lightsColorH);
    };
    return updatedData;
  }
}

public class VehicleQuestWindowDestructionEvent extends Event {

  public edit let windowName: CName;

  @default(VehicleQuestWindowDestructionEvent, vehicleQuestWindowDestruction.window_f)
  public edit let window: vehicleQuestWindowDestruction;

  public final func GetFriendlyDescription() -> String {
    return "Destroy vehicle windows";
  }
}

public class VehiclePanzerBootupUIQuestEvent extends Event {

  @default(VehiclePanzerBootupUIQuestEvent, panzerBootupUI.Loop)
  public edit let mode: panzerBootupUI;

  public final func GetFriendlyDescription() -> String {
    return "Manage Panzer Bootup UI";
  }
}
