
public class PhoneStatusEffectListener extends ScriptStatusEffectListener {

  private let m_phoneSystem: wref<PhoneSystem>;

  public final func Init(system: ref<PhoneSystem>) -> Void {
    this.m_phoneSystem = system;
  }

  public func OnStatusEffectApplied(statusEffect: wref<StatusEffect_Record>) -> Void {
    this.m_phoneSystem.RefreshPhoneEnabled();
  }

  public func OnStatusEffectRemoved(statusEffect: wref<StatusEffect_Record>) -> Void {
    this.m_phoneSystem.RefreshPhoneEnabled();
  }
}

public class PhoneStatsListener extends ScriptStatsListener {

  private let m_phoneSystem: wref<PhoneSystem>;

  public final func Init(system: ref<PhoneSystem>) -> Void {
    this.m_phoneSystem = system;
  }

  public func OnStatChanged(ownerID: StatsObjectID, statType: gamedataStatType, diff: Float, total: Float) -> Void {
    this.m_phoneSystem.RefreshPhoneEnabled();
  }
}

public class PhoneSystem extends ScriptableSystem {

  private let m_BlackboardSystem: ref<BlackboardSystem>;

  private let m_Blackboard: wref<IBlackboard>;

  private let m_PsmBlackboard: wref<IBlackboard>;

  private let m_LastCallInformation: PhoneCallInformation;

  private let m_StatusEffectsListener: ref<PhoneStatusEffectListener>;

  private let m_StatsListener: ref<PhoneStatsListener>;

  @default(PhoneSystem, false)
  private let m_ContactsOpen: Bool;

  private let m_PhoneVisibilityBBId: ref<CallbackHandle>;

  private let m_ContactsOpenBBId: ref<CallbackHandle>;

  private let m_HighLevelBBId: ref<CallbackHandle>;

  private let m_CombatBBId: ref<CallbackHandle>;

  private let m_SwimmingBBId: ref<CallbackHandle>;

  private let m_IsContrDeviceBBId: ref<CallbackHandle>;

  private let m_IsUIZoomDeviceBBId: ref<CallbackHandle>;

  private let m_PlayerAttachedCallbackID: Uint32;

  private let m_PlayerDetachedCallbackID: Uint32;

  private final func IsShowingMessage() -> Bool {
    return this.m_Blackboard.GetBool(GetAllBlackboardDefs().UI_ComDevice.isDisplayingMessage);
  }

  private final func IsPhoneOpened() -> Bool {
    return this.m_Blackboard.GetBool(GetAllBlackboardDefs().UI_ComDevice.ContactsActive);
  }

  private final func OnSetPhoneStatus(request: ref<questSetPhoneStatusRequest>) -> Void {
    this.m_Blackboard.SetName(GetAllBlackboardDefs().UI_ComDevice.comDeviceSetStatusText, request.status, true);
  }

  private final func OnTriggerCall(request: ref<questTriggerCallRequest>) -> Void {
    let contactName: CName;
    let shouldPlayIncomingCallSound: Bool = Equals(request.callPhase, questPhoneCallPhase.IncomingCall);
    if Equals(request.callPhase, questPhoneCallPhase.IncomingCall) || Equals(request.callPhase, questPhoneCallPhase.StartCall) {
      this.ToggleContacts(false);
    };
    if IsNameValid(request.caller) && NotEquals(request.caller, n"player") && NotEquals(request.caller, n"Player") {
      if shouldPlayIncomingCallSound && Equals(request.visuals, questPhoneCallVisuals.Default) {
        GameInstance.GetAudioSystem(GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject().GetGame()).Play(n"ui_phone_incoming_call");
      };
      contactName = request.caller;
    } else {
      if IsNameValid(request.addressee) && NotEquals(request.addressee, n"player") && NotEquals(request.addressee, n"Player") {
        if shouldPlayIncomingCallSound {
          GameInstance.GetAudioSystem(GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject().GetGame()).Play(n"ui_phone_initiation_call");
        };
        contactName = request.addressee;
      };
    };
    if IsNameValid(contactName) {
      this.TriggerCall(request.callMode, Equals(request.callMode, questPhoneCallMode.Audio), contactName, Equals(request.caller, n"Player") || Equals(request.caller, n"player"), request.callPhase, request.isPlayerTriggered, request.isRejectable, request.showAvatar, request.visuals);
    };
  }

  private final func TriggerCall(callMode: questPhoneCallMode, isAudio: Bool, contactName: CName, isPlayerCalling: Bool, callPhase: questPhoneCallPhase, isPlayerTriggered: Bool, opt isRejectable: Bool, showAvatar: Bool, callVisuals: questPhoneCallVisuals) -> Void {
    let state: questPhoneTalkingState;
    this.m_LastCallInformation = new PhoneCallInformation(callMode, isAudio, contactName, isPlayerCalling, isPlayerTriggered, callPhase, isRejectable, showAvatar, callVisuals);
    this.m_Blackboard.SetVariant(GetAllBlackboardDefs().UI_ComDevice.PhoneCallInformation, ToVariant(this.m_LastCallInformation), true);
    if Equals(callPhase, questPhoneCallPhase.EndCall) {
      state = questPhoneTalkingState.Ended;
      if isPlayerTriggered {
        GameInstance.GetPhoneManager(this.GetGameInstance()).ApplyPhoneCallRestriction(false);
      };
    } else {
      state = questPhoneTalkingState.Initializing;
      if isPlayerTriggered {
        GameInstance.GetPhoneManager(this.GetGameInstance()).ApplyPhoneCallRestriction(true);
      };
    };
    this.SetPhoneFact(isPlayerCalling, contactName, state);
  }

  private final func OnPickupPhone(request: ref<PickupPhoneRequest>) -> Void {
    if Equals(this.m_LastCallInformation.callPhase, questPhoneCallPhase.IncomingCall) && Equals(request.CallInformation.contactName, this.m_LastCallInformation.contactName) {
      GameInstance.GetAudioSystem(GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject().GetGame()).Play(n"ui_phone_initiation_call_stop");
      GameInstance.GetAudioSystem(GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject().GetGame()).Play(n"ui_phone_incoming_call_stop");
      if request.shouldBeRejected {
        this.SetPhoneFact(request.CallInformation.isPlayerCalling, request.CallInformation.contactName, questPhoneTalkingState.Rejected);
      } else {
        this.SetPhoneFact(request.CallInformation.isPlayerCalling, request.CallInformation.contactName, questPhoneTalkingState.Talking);
      };
    };
  }

  private final func OnPhoneTimeoutRequest(request: ref<PhoneTimeoutRequest>) -> Void {
    if Equals(this.m_LastCallInformation.callPhase, questPhoneCallPhase.IncomingCall) {
      this.TriggerCall(questPhoneCallMode.Undefined, this.m_LastCallInformation.isAudioCall, this.m_LastCallInformation.contactName, this.m_LastCallInformation.isPlayerCalling, questPhoneCallPhase.EndCall, this.m_LastCallInformation.isPlayerTriggered, this.m_LastCallInformation.isRejectable, this.m_LastCallInformation.showAvatar, this.m_LastCallInformation.visuals);
    };
  }

  private final func OnUsePhone(request: ref<UsePhoneRequest>) -> Void {
    let hash: Int32;
    let localPlayer: wref<GameObject>;
    let notificationEvent: ref<UIInGameNotificationEvent>;
    if this.IsPhoneOpened() {
      return;
    };
    localPlayer = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject();
    if Equals(this.m_LastCallInformation.callPhase, questPhoneCallPhase.IncomingCall) {
      if this.m_LastCallInformation.isPlayerCalling {
        GameInstance.GetAudioSystem(localPlayer.GetGame()).Play(n"ui_phone_incoming_call_stop");
        this.TriggerCall(questPhoneCallMode.Undefined, this.m_LastCallInformation.isAudioCall, this.m_LastCallInformation.contactName, this.m_LastCallInformation.isPlayerCalling, questPhoneCallPhase.EndCall, this.m_LastCallInformation.isPlayerTriggered, this.m_LastCallInformation.isRejectable, this.m_LastCallInformation.showAvatar, this.m_LastCallInformation.visuals);
      } else {
        if Equals(this.m_LastCallInformation.visuals, questPhoneCallVisuals.Default) {
          GameInstance.GetAudioSystem(localPlayer.GetGame()).Play(n"ui_phone_incoming_call_positive");
        };
        GameInstance.GetAudioSystem(localPlayer.GetGame()).Play(n"ui_phone_incoming_call_stop");
        this.SetPhoneFact(this.m_LastCallInformation.isPlayerCalling, this.m_LastCallInformation.contactName, questPhoneTalkingState.Talking);
      };
    } else {
      if !this.IsPhoneEnabled() {
        GameInstance.GetUISystem(localPlayer.GetGame()).QueueEvent(new UIInGameNotificationRemoveEvent());
        notificationEvent = new UIInGameNotificationEvent();
        notificationEvent.m_notificationType = UIInGameNotificationType.CombatRestriction;
        GameInstance.GetUISystem(localPlayer.GetGame()).QueueEvent(notificationEvent);
        return;
      };
      if !this.m_ContactsOpen {
        if request.MessageToOpen != null {
          hash = GameInstance.GetJournalManager(localPlayer.GetGame()).GetEntryHash(request.MessageToOpen);
          this.m_Blackboard.SetInt(GetAllBlackboardDefs().UI_ComDevice.MessageToOpenHash, hash, true);
        };
        this.ToggleContacts(true);
      };
    };
  }

  private final func ToggleContacts(open: Bool) -> Void {
    this.m_Blackboard.SetBool(GetAllBlackboardDefs().UI_ComDevice.ContactsActive, open, true);
  }

  private final func OnContactsStateChanged(newState: Bool) -> Void {
    this.m_ContactsOpen = newState;
  }

  private final func OnPhoneVisibilityChanged(newValue: Variant) -> Void {
    this.RefreshPhoneEnabled();
  }

  private final func OnTalkingTriggerRequest(request: ref<TalkingTriggerRequest>) -> Void {
    GameInstance.GetAudioSystem(GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject().GetGame()).Play(n"ui_phone_initiation_call_stop");
    GameInstance.GetAudioSystem(GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject().GetGame()).Play(n"ui_phone_incoming_call_stop");
    if Equals(request.visuals, questPhoneCallVisuals.Default) {
      GameInstance.GetAudioSystem(GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject().GetGame()).Play(n"ui_phone_incoming_call_positive");
    };
    this.SetPhoneFact(request.isPlayerCalling, request.contact, request.state);
  }

  private final func OnMinimizeCallRequest(request: ref<questMinimizeCallRequest>) -> Void {
    this.m_LastCallInformation.isAudioCall = request.minimized;
    this.m_LastCallInformation.callMode = request.minimized ? questPhoneCallMode.Audio : questPhoneCallMode.Video;
    this.m_Blackboard.SetVariant(GetAllBlackboardDefs().UI_ComDevice.PhoneCallInformation, ToVariant(this.m_LastCallInformation), false);
    this.m_Blackboard.SetBool(GetAllBlackboardDefs().UI_ComDevice.PhoneStyle_Minimized, request.minimized, true);
  }

  private final func SetPhoneFact(isPlayerCalling: Bool, contactName: CName, state: questPhoneTalkingState) -> Void {
    let factName: String;
    if isPlayerCalling {
      factName = this.GetPhoneCallFactName(n"player", contactName);
    } else {
      factName = this.GetPhoneCallFactName(contactName, n"player");
    };
    GameInstance.GetQuestsSystem(this.GetGameInstance()).SetFactStr(factName, EnumInt(state));
  }

  private func OnAttach() -> Void {
    this.m_BlackboardSystem = GameInstance.GetBlackboardSystem(this.GetGameInstance());
    this.m_Blackboard = this.m_BlackboardSystem.Get(GetAllBlackboardDefs().UI_ComDevice);
    this.m_ContactsOpenBBId = this.m_Blackboard.RegisterListenerBool(GetAllBlackboardDefs().UI_ComDevice.ContactsActive, this, n"OnContactsStateChanged");
    this.m_PhoneVisibilityBBId = this.m_Blackboard.RegisterListenerVariant(GetAllBlackboardDefs().UI_ComDevice.PhoneVisibility, this, n"OnPhoneVisibilityChanged");
    this.m_PlayerAttachedCallbackID = GameInstance.GetPlayerSystem(this.GetGameInstance()).RegisterPlayerPuppetAttachedCallback(this, n"PlayerAttachedCallback");
  }

  private func OnDetach() -> Void {
    if IsDefined(this.m_Blackboard) {
      this.m_Blackboard.UnregisterListenerBool(GetAllBlackboardDefs().UI_ComDevice.ContactsActive, this.m_ContactsOpenBBId);
      this.m_Blackboard.UnregisterListenerVariant(GetAllBlackboardDefs().UI_ComDevice.PhoneVisibility, this.m_PhoneVisibilityBBId);
    };
    GameInstance.GetPlayerSystem(this.GetGameInstance()).UnregisterPlayerPuppetAttachedCallback(this.m_PlayerAttachedCallbackID);
    this.PlayerDetached();
  }

  private final func PlayerAttachedCallback(playerPuppet: ref<GameObject>) -> Void {
    this.m_PsmBlackboard = this.m_BlackboardSystem.GetLocalInstanced(playerPuppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    if IsDefined(this.m_PsmBlackboard) {
      this.m_CombatBBId = this.m_PsmBlackboard.RegisterListenerInt(GetAllBlackboardDefs().PlayerStateMachine.Combat, this, n"OnPhoneEnabledChangedInt");
      this.m_HighLevelBBId = this.m_PsmBlackboard.RegisterListenerInt(GetAllBlackboardDefs().PlayerStateMachine.HighLevel, this, n"OnPhoneEnabledChangedInt");
      this.m_SwimmingBBId = this.m_PsmBlackboard.RegisterListenerInt(GetAllBlackboardDefs().PlayerStateMachine.Swimming, this, n"OnPhoneEnabledChangedInt");
      this.m_IsContrDeviceBBId = this.m_PsmBlackboard.RegisterListenerBool(GetAllBlackboardDefs().PlayerStateMachine.IsControllingDevice, this, n"OnPhoneEnabledChangedBool");
      this.m_IsUIZoomDeviceBBId = this.m_PsmBlackboard.RegisterListenerBool(GetAllBlackboardDefs().PlayerStateMachine.IsUIZoomDevice, this, n"OnPhoneEnabledChangedBool");
    };
    this.m_StatusEffectsListener = new PhoneStatusEffectListener();
    this.m_StatusEffectsListener.Init(this);
    GameInstance.GetStatusEffectSystem(this.GetGameInstance()).RegisterListener(playerPuppet.GetEntityID(), this.m_StatusEffectsListener);
    this.m_StatsListener = new PhoneStatsListener();
    this.m_StatsListener.Init(this);
    GameInstance.GetStatsSystem(this.GetGameInstance()).RegisterListener(Cast<StatsObjectID>(playerPuppet.GetEntityID()), this.m_StatsListener);
  }

  private final func PlayerDetached() -> Void {
    let localPlayer: wref<GameObject> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject();
    if IsDefined(this.m_PsmBlackboard) {
      this.m_PsmBlackboard.UnregisterListenerInt(GetAllBlackboardDefs().PlayerStateMachine.Combat, this.m_CombatBBId);
      this.m_PsmBlackboard.UnregisterListenerInt(GetAllBlackboardDefs().PlayerStateMachine.HighLevel, this.m_HighLevelBBId);
      this.m_PsmBlackboard.UnregisterListenerInt(GetAllBlackboardDefs().PlayerStateMachine.Swimming, this.m_SwimmingBBId);
      this.m_PsmBlackboard.UnregisterListenerBool(GetAllBlackboardDefs().PlayerStateMachine.IsControllingDevice, this.m_IsContrDeviceBBId);
      this.m_PsmBlackboard.UnregisterListenerBool(GetAllBlackboardDefs().PlayerStateMachine.IsUIZoomDevice, this.m_IsUIZoomDeviceBBId);
    };
    this.m_StatusEffectsListener = null;
    if IsDefined(localPlayer) {
      GameInstance.GetStatsSystem(this.GetGameInstance()).UnregisterListener(Cast<StatsObjectID>(localPlayer.GetEntityID()), this.m_StatsListener);
    };
  }

  protected cb func OnPhoneEnabledChangedInt(value: Int32) -> Bool {
    this.RefreshPhoneEnabled();
  }

  protected cb func OnPhoneEnabledChangedBool(value: Bool) -> Bool {
    this.RefreshPhoneEnabled();
  }

  public final const func RefreshPhoneEnabled() -> Void {
    if IsDefined(this.m_Blackboard) {
      this.m_Blackboard.SetBool(GetAllBlackboardDefs().UI_ComDevice.PhoneEnabled, this.IsPhoneEnabled());
    };
  }

  public final const func IsPhoneEnabled() -> Bool {
    let blocedByCombat: Bool = this.IsBlockedByCombat();
    let blockedByStatus: Bool = this.IsBlockedByStatus();
    let blockedByTier: Bool = this.IsBlockedByTier();
    let blockedByBlackboard: Bool = this.IsBlockedByBlackboard();
    let blockedByHud: Bool = this.IsBlockedByHUD();
    let blockedByVisiblity: Bool = this.IsBlockedByVisiblity();
    let enabledByQuest: Bool = this.IsEnabledByQuestSystem();
    let enabledByVisiblity: Bool = this.IsEnabledByVisiblity();
    if blocedByCombat {
      return false;
    };
    if enabledByQuest || enabledByVisiblity {
      return true;
    };
    return !blockedByTier && !blockedByBlackboard && !blockedByHud && !blockedByVisiblity && !blockedByStatus;
  }

  private final const func IsBlockedByCombat() -> Bool {
    let combat: Int32 = this.m_PsmBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Combat);
    return combat == 1;
  }

  private final const func IsBlockedByStatus() -> Bool {
    let localPlayer: wref<GameObject> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject();
    let noPhone: Bool = StatusEffectSystem.ObjectHasStatusEffectWithTag(localPlayer, n"NoPhone");
    let interrupted: Bool = StatusEffectSystem.ObjectHasStatusEffectWithTag(localPlayer, n"PhoneInterrupted");
    let noCalling: Bool = StatusEffectSystem.ObjectHasStatusEffectWithTag(localPlayer, n"PhoneNoCalling");
    return (noPhone || interrupted) && !noCalling;
  }

  private final const func IsBlockedByTier() -> Bool {
    let tier: Int32 = this.m_PsmBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.HighLevel);
    return tier >= 3 && tier <= 5;
  }

  private final const func IsBlockedByBlackboard() -> Bool {
    let localPlayer: wref<GameObject> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject();
    if this.m_PsmBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Swimming) == 2 && !GameInstance.GetStatsSystem(this.GetGameInstance()).GetStatBoolValue(Cast<StatsObjectID>(localPlayer.GetEntityID()), gamedataStatType.CanUsePhoneUnderWater) {
      return true;
    };
    if this.m_PsmBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsControllingDevice) || this.m_PsmBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsUIZoomDevice) {
      return true;
    };
    return false;
  }

  private final const func IsEnabledByQuestSystem() -> Bool {
    let questsSystem: wref<QuestsSystem> = GameInstance.GetQuestsSystem(this.GetGameInstance());
    let isEnabled: Bool = Cast<Bool>(questsSystem.GetFact(n"q304_phone_in_car_on"));
    return isEnabled;
  }

  private final const func IsBlockedByHUD() -> Bool {
    let localPlayer: wref<GameObject> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject();
    return StatusEffectSystem.ObjectHasStatusEffectWithTag(localPlayer, n"BlockAllHubMenu");
  }

  private final const func IsEnabledByVisiblity() -> Bool {
    let vis: worlduiEntryVisibility = FromVariant<worlduiEntryVisibility>(this.m_Blackboard.GetVariant(GetAllBlackboardDefs().UI_ComDevice.PhoneVisibility));
    return Equals(vis, worlduiEntryVisibility.ForceShow);
  }

  private final const func IsBlockedByVisiblity() -> Bool {
    let vis: worlduiEntryVisibility = FromVariant<worlduiEntryVisibility>(this.m_Blackboard.GetVariant(GetAllBlackboardDefs().UI_ComDevice.PhoneVisibility));
    return Equals(vis, worlduiEntryVisibility.ForceHide);
  }

  public final const func IsCallingEnabled() -> Bool {
    let phoneEnabled: Bool = this.IsPhoneEnabled();
    let callingAvaliable: Bool = this.IsCallingAvaliable();
    let blockedByGameplay: Bool = this.IsCallingBlockedByStatus();
    return phoneEnabled && callingAvaliable && !blockedByGameplay;
  }

  private final const func IsCallingAvaliable() -> Bool {
    return Equals(this.m_LastCallInformation.callPhase, questPhoneCallPhase.Undefined) || Equals(this.m_LastCallInformation.callPhase, questPhoneCallPhase.EndCall);
  }

  private final const func IsCallingBlockedByStatus() -> Bool {
    let localPlayer: wref<GameObject> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject();
    return StatusEffectSystem.ObjectHasStatusEffectWithTag(localPlayer, n"PhoneNoCalling");
  }

  public final const func IsTextingEnabled() -> Bool {
    let phoneEnabled: Bool = this.IsPhoneEnabled();
    let blockedByStatus: Bool = this.IsTextingBlockedByStatus();
    return phoneEnabled && !blockedByStatus;
  }

  private final const func IsTextingBlockedByStatus() -> Bool {
    let localPlayer: wref<GameObject> = GameInstance.GetPlayerSystem(this.GetGameInstance()).GetLocalPlayerMainGameObject();
    return StatusEffectSystem.ObjectHasStatusEffectWithTag(localPlayer, n"PhoneNoTexting");
  }

  public final const func IsPhoneAvailable() -> Bool {
    return Equals(this.m_LastCallInformation.callPhase, questPhoneCallPhase.Undefined) || Equals(this.m_LastCallInformation.callPhase, questPhoneCallPhase.EndCall) || Equals(this.m_LastCallInformation.callPhase, questPhoneCallPhase.IncomingCall);
  }

  public final const func GetPhoneCallFactName(contactName1: CName, contactName2: CName) -> String {
    return "phonecall_" + StrLower(NameToString(contactName1)) + "_with_" + StrLower(NameToString(contactName2));
  }
}
