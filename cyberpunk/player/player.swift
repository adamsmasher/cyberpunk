
public static func GetPlayer(gameInstance: GameInstance) -> ref<PlayerPuppet> {
  if IsDefined(GameInstance.GetPlayerSystem(gameInstance)) {
    return GameInstance.GetPlayerSystem(gameInstance).GetLocalPlayerControlledGameObject() as PlayerPuppet;
  };
  return null;
}

public static func GetMainPlayer(gameInstance: GameInstance) -> ref<PlayerPuppet> {
  if IsDefined(GameInstance.GetPlayerSystem(gameInstance)) {
    return GameInstance.GetPlayerSystem(gameInstance).GetLocalPlayerMainGameObject() as PlayerPuppet;
  };
  return null;
}

public static func GetPlayerObject(gameInstance: GameInstance) -> ref<GameObject> {
  return GameInstance.GetPlayerSystem(gameInstance).GetLocalPlayerControlledGameObject();
}

public static func IsHostileTowardsPlayer(object: wref<GameObject>) -> Bool {
  let player: wref<PlayerPuppet>;
  if !IsDefined(object) {
    return false;
  };
  player = GetPlayer(object.GetGame());
  if IsDefined(player) && Equals(GameObject.GetAttitudeTowards(object, player), EAIAttitude.AIA_Hostile) {
    return true;
  };
  return false;
}

public static func IsFriendlyTowardsPlayer(object: wref<GameObject>) -> Bool {
  let player: wref<PlayerPuppet>;
  if !IsDefined(object) {
    return false;
  };
  player = GetPlayer(object.GetGame());
  if IsDefined(player) && Equals(GameObject.GetAttitudeTowards(object, player), EAIAttitude.AIA_Friendly) {
    return true;
  };
  return false;
}

public class PlayerPuppetPS extends ScriptedPuppetPS {

  private persistent let keybindigs: KeyBindings;

  private persistent let m_availablePrograms: [MinigameProgramData];

  private persistent let m_hasAutoReveal: Bool;

  private persistent let m_combatExitTimestamp: Float;

  private persistent let m_isInDriverCombat: Bool;

  private persistent let m_pocketRadioStation: Int32;

  private persistent let m_permanentHealthBonus: Float;

  private persistent let m_permanentStaminaBonus: Float;

  private persistent let m_permanentMemoryBonus: Float;

  private let m_minigameBB: wref<IBlackboard>;

  public final const func GetCombatExitTimestamp() -> Float {
    return this.m_combatExitTimestamp;
  }

  public final func SetCombatExitTimestamp(timestamp: Float) -> Void {
    this.m_combatExitTimestamp = timestamp;
  }

  public final const func HasAutoReveal() -> Bool {
    return this.m_hasAutoReveal;
  }

  public final func SetAutoReveal(value: Bool) -> Void {
    this.m_hasAutoReveal = value;
  }

  public final const func IsInDriverCombat() -> Bool {
    return this.m_isInDriverCombat;
  }

  public final func SetIsInDriverCombat(value: Bool) -> Void {
    this.m_isInDriverCombat = value;
  }

  public final const func GetPocketRadioStation() -> Int32 {
    return this.m_pocketRadioStation;
  }

  public final func SetPocketRadioStation(value: Int32) -> Void {
    this.m_pocketRadioStation = value;
  }

  public final func GetPermanentHealthBonus() -> Float {
    return this.m_permanentHealthBonus;
  }

  public final func SetPermanentHealthBonus(value: Float) -> Void {
    this.m_permanentHealthBonus = value;
  }

  public final func GetPermanentStaminaBonus() -> Float {
    return this.m_permanentStaminaBonus;
  }

  public final func SetPermanentStaminaBonus(value: Float) -> Void {
    this.m_permanentStaminaBonus = value;
  }

  public final func GetPermanentMemoryBonus() -> Float {
    return this.m_permanentMemoryBonus;
  }

  public final func SetPermanentMemoryBonus(value: Float) -> Void {
    this.m_permanentMemoryBonus = value;
  }

  protected final func OnStoreMinigameProgram(evt: ref<StoreMiniGameProgramEvent>) -> EntityNotificationType {
    if evt.add {
      this.AddMinigameProgram(evt.program);
    } else {
      this.RemoveMinigameProgram(evt.program);
    };
    this.GetMinigameBlackboard().SetVariant(GetAllBlackboardDefs().HackingMinigame.PlayerPrograms, ToVariant(this.m_availablePrograms));
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected final func AddMinigameProgram(program: MinigameProgramData) -> Void {
    let programTemp: MinigameProgramData;
    if !this.HasProgram(program.actionID, this.m_availablePrograms) || program.actionID == t"MinigameAction.NetworkLowerICEMedium" {
      if program.actionID == t"MinigameAction.NetworkLowerICEMedium" || program.actionID == t"MinigameAction.NetworkLowerICEMajor" || program.actionID == t"MinigameAction.NetworkLowerICEMinorFirst" {
        programTemp = this.DecideProgramToAdd(program.actionID);
        if NotEquals(programTemp.programName, n"None") {
          ArrayInsert(this.m_availablePrograms, 0, programTemp);
        };
      } else {
        this.UpgradePrograms(program.actionID);
        ArrayPush(this.m_availablePrograms, program);
      };
    };
  }

  protected final func RemoveMinigameProgram(program: MinigameProgramData) -> Void {
    this.RemoveProgram(program.actionID);
  }

  protected final func RemoveProgram(id: TweakDBID) -> Void {
    let i: Int32 = ArraySize(this.m_availablePrograms) - 1;
    while i >= 0 {
      if this.m_availablePrograms[i].actionID == id {
        ArrayErase(this.m_availablePrograms, i);
      };
      i -= 1;
    };
  }

  protected final func HasProgram(id: TweakDBID) -> Bool {
    let i: Int32 = ArraySize(this.m_availablePrograms) - 1;
    while i >= 0 {
      if this.m_availablePrograms[i].actionID == id {
        return true;
      };
      i -= 1;
    };
    return false;
  }

  protected final func UpgradePrograms(id: TweakDBID) -> Void {
    if id == t"MinigameAction.NetworkCameraFriendly" {
      this.RemoveProgram(t"MinigameAction.NetworkCameraMalfunction");
    } else {
      if id == t"MinigameAction.NetworkCameraShutdown" {
        this.RemoveProgram(t"MinigameAction.NetworkCameraMalfunction");
      } else {
        if id == t"MinigameAction.NetworkTurretFriendly" {
          this.RemoveProgram(t"MinigameAction.NetworkTurretMalfunction");
        } else {
          if id == t"MinigameAction.NetworkTurretShutdown" {
            this.RemoveProgram(t"MinigameAction.NetworkTurretMalfunction");
          };
        };
      };
    };
  }

  protected final func DecideProgramToAdd(id: TweakDBID) -> MinigameProgramData {
    let program: MinigameProgramData;
    if id == t"MinigameAction.NetworkLowerICEMinorFirst" {
      if this.HasProgram(t"MinigameAction.NetworkLowerICEMedium") || this.HasProgram(t"MinigameAction.NetworkLowerICEMajor") {
        program.programName = n"None";
        return program;
      };
      program.actionID = t"MinigameAction.NetworkLowerICEMinorFirst";
      program.programName = n"LocKey#34844";
      return program;
    };
    if id == t"MinigameAction.NetworkLowerICEMedium" {
      if this.HasProgram(t"MinigameAction.NetworkLowerICEMedium") {
        program.actionID = t"MinigameAction.NetworkLowerICEMajor";
        program.programName = n"LocKey#34844";
        this.RemoveProgram(t"MinigameAction.NetworkLowerICEMedium");
        return program;
      };
      if this.HasProgram(t"MinigameAction.NetworkLowerICEMajor") {
        program.programName = n"None";
        return program;
      };
      program.actionID = t"MinigameAction.NetworkLowerICEMedium";
      program.programName = n"LocKey#34844";
      this.RemoveProgram(t"MinigameAction.NetworkLowerICEMinorFirst");
      return program;
    };
    if id == t"MinigameAction.NetworkLowerICEMajor" {
      if !this.HasProgram(t"MinigameAction.NetworkLowerICEMedium") {
        program.actionID = t"MinigameAction.NetworkLowerICEMedium";
        program.programName = n"LocKey#34844";
        this.RemoveProgram(t"MinigameAction.NetworkLowerICEMinorFirst");
        return program;
      };
      program.actionID = t"MinigameAction.NetworkLowerICEMajor";
      program.programName = n"LocKey#34844";
      this.RemoveProgram(t"MinigameAction.NetworkLowerICEMedium");
      return program;
    };
    return program;
  }

  private final func GetMinigameBlackboard() -> ref<IBlackboard> {
    if this.m_minigameBB == null {
      this.m_minigameBB = GameInstance.GetBlackboardSystem(this.GetGameInstance()).Get(GetAllBlackboardDefs().HackingMinigame);
    };
    return this.m_minigameBB;
  }

  public final const func GetMinigamePrograms() -> [MinigameProgramData] {
    return this.m_availablePrograms;
  }

  protected final func HasProgram(id: TweakDBID, const programs: script_ref<[MinigameProgramData]>) -> Bool {
    let i: Int32 = 0;
    while i < ArraySize(Deref(programs)) {
      if Deref(programs)[i].actionID == id {
        return true;
      };
      i += 1;
    };
    return false;
  }
}

public class CPOMissionDataState extends IScriptable {

  public let m_CPOMissionDataDamagesPreset: CName;

  public let m_compatibleDeviceName: CName;

  public let m_ownerDecidesOnTransfer: Bool;

  @default(CPOMissionDataState, false)
  public let m_isChoiceToken: Bool;

  @default(CPOMissionDataState, 0)
  public let m_choiceTokenTimeout: Uint32;

  public let m_delayedGiveChoiceTokenEventId: DelayID;

  private let m_dataDamageTextLayerId: Uint32;

  public final func OnDamage(puppet: ref<PlayerPuppet>, healthDamage: Bool) -> Void {
    let message: String;
    let soundEvent: ref<SoundPlayEvent>;
    let updateEvent: ref<CPOMissionDataUpdateEvent>;
    let delaySystem: ref<DelaySystem> = GameInstance.GetDelaySystem(puppet.GetGame());
    if puppet.HasCPOMissionData() && !puppet.m_CPOMissionDataState.m_isChoiceToken {
      message = "Corrupted data - internal damage!";
      this.m_dataDamageTextLayerId = GameInstance.GetDebugVisualizerSystem(puppet.GetGame()).DrawText(new Vector4(500.00, 200.00, 0.00, 1.50), message, gameDebugViewETextAlignment.Center, new Color(255u, 0u, 0u, 255u), 1.00);
      GameInstance.GetDebugVisualizerSystem(puppet.GetGame()).SetScale(this.m_dataDamageTextLayerId, new Vector4(3.00, 3.00, 0.00, 0.00));
      if healthDamage {
        soundEvent = new SoundPlayEvent();
        soundEvent.soundName = n"test_ad_emitter_2_1";
        puppet.QueueEvent(soundEvent);
      };
      GameObject.SetAudioParameter(puppet, n"g_player_health", 30.00);
      GameObject.PlaySoundEvent(puppet, n"ST_Health_Status_Low_Set_State");
      updateEvent = new CPOMissionDataUpdateEvent();
      delaySystem.DelayEvent(puppet, updateEvent, 1.00);
    };
  }

  public final func UpdateSounds(puppet: ref<PlayerPuppet>) -> Void {
    if !puppet.HasCPOMissionData() {
      GameObject.SetAudioParameter(puppet, n"g_player_health", 1.00);
      GameObject.PlaySoundEvent(puppet, n"ST_Health_Status_Hi_Set_State");
    };
  }
}

public class PlayerPuppet extends ScriptedPuppet {

  private let m_quickSlotsManager: ref<QuickSlotsManager>;

  private let m_inspectionComponent: ref<InspectionComponent>;

  private let m_enviroDamageRcvComponent: ref<EnvironmentDamageReceiverComponent>;

  private let m_mountedVehicle: wref<VehicleObject>;

  private let m_vehicleKnockdownTimestamp: Float;

  public let m_Phone: ref<PlayerPhone>;

  private let m_fppCameraComponent: ref<FPPCameraComponent>;

  private let m_primaryTargetingComponent: ref<TargetingComponent>;

  private let m_breachFinderComponent: ref<BreachFinderComponent>;

  private let m_chaseSpawnComponent: ref<gameChaseSpawnComponent>;

  private let m_isInFinisher: Bool;

  public let DEBUG_Visualizer: ref<DEBUG_VisualizerComponent>;

  private let m_Debug_DamageInputRec: ref<DEBUG_DamageInputReceiver>;

  public let m_highDamageThreshold: Float;

  public let m_medDamageThreshold: Float;

  public let m_lowDamageThreshold: Float;

  public let m_meleeHighDamageThreshold: Float;

  public let m_meleeMedDamageThreshold: Float;

  public let m_meleeLowDamageThreshold: Float;

  public let m_explosionHighDamageThreshold: Float;

  public let m_explosionMedDamageThreshold: Float;

  public let m_explosionLowDamageThreshold: Float;

  public let m_effectTimeStamp: Float;

  public let m_curInventoryWeight: Float;

  public let m_healthVfxBlackboard: ref<worldEffectBlackboard>;

  public let m_laserTargettingVfxBlackboard: ref<worldEffectBlackboard>;

  public let m_itemLogBlackboard: wref<IBlackboard>;

  public let m_interactionDataListener: ref<CallbackHandle>;

  public let m_popupIsModalListener: ref<CallbackHandle>;

  public let m_uiVendorContextListener: ref<CallbackHandle>;

  public let m_uiRadialContextistener: ref<CallbackHandle>;

  public let m_contactsActiveListener: ref<CallbackHandle>;

  public let m_smsMessengerActiveListener: ref<CallbackHandle>;

  public let m_currentVisibleTargetListener: ref<CallbackHandle>;

  public let lastScanTarget: wref<GameObject>;

  public let meleeSelectInputProcessed: Bool;

  @default(PlayerPuppet, false)
  private let m_waitingForDelayEvent: Bool;

  private let m_randomizedTime: Float;

  private let m_isResetting: Bool;

  private let m_delayEventID: DelayID;

  private let m_resetTickID: DelayID;

  private let m_katanaAnimProgression: Float;

  private let m_coverModifierActive: Bool;

  private let m_workspotDamageReductionActive: Bool;

  private let m_workspotVisibilityReductionActive: Bool;

  private let m_currentPlayerWorkspotTags: [CName];

  private let m_incapacitated: Bool;

  private let m_remoteMappinId: NewMappinID;

  public let m_CPOMissionDataState: ref<CPOMissionDataState>;

  private let m_CPOMissionDataBbId: ref<CallbackHandle>;

  private let m_visibilityListener: ref<VisibilityStatListener>;

  private let m_secondHeartListener: ref<SecondHeartStatListener>;

  private let m_armorStatListener: ref<ArmorStatListener>;

  private let m_healthStatListener: ref<HealthStatListener>;

  private let m_oxygenStatListener: ref<OxygenStatListener>;

  private let m_aimAssistListener: ref<AimAssistSettingsListener>;

  private let m_autoRevealListener: ref<AutoRevealStatListener>;

  private let m_allStatsListener: ref<PlayerPuppetAllStatListener>;

  private let m_rightHandAttachmentSlotListener: ref<AttachmentSlotsScriptListener>;

  private let m_HealingItemsChargeStatListener: ref<HealingItemsChargeStatListener>;

  private let m_GrenadesChargeStatListener: ref<GrenadesChargeStatListener>;

  private let m_ProjectileLauncherChargeStatListener: ref<ProjectileLauncherChargeStatListener>;

  private let m_OpticalCamoChargeStatListener: ref<OpticalCamoChargeStatListener>;

  private let m_OverclockChargeListener: ref<OverclockChargeListener>;

  private let m_accessibilityControlsListener: ref<AccessibilityControlsListener>;

  private let isTalkingOnPhone: Bool;

  private let m_DataDamageUpdateID: DelayID;

  private let m_playerAttachedCallbackID: Uint32;

  private let m_playerDetachedCallbackID: Uint32;

  private let m_callbackHandles: [ref<CallbackHandle>];

  private let m_numberOfCombatants: Int32;

  private let m_equipmentMeshOverlayEffectName: CName;

  private let m_equipmentMeshOverlayEffectTag: CName;

  private let m_equipmentMeshOverlaySlots: [TweakDBID];

  private let m_coverVisibilityPerkBlocked: Bool;

  private let m_behindCover: Bool;

  private let m_inCombat: Bool;

  private let m_isBeingRevealed: Bool;

  private let m_hasBeenDetected: Bool;

  private let m_inCrouch: Bool;

  private let m_hasKiroshiOpticsFragment: Bool;

  private let m_doingQuickMelee: Bool;

  private let m_vehicleState: gamePSMVehicle;

  private let m_inMountedWeaponVehicle: Bool;

  private let m_inDriverCombatTPP: Bool;

  private let m_driverCombatWeaponType: gamedataItemType;

  private let m_isAiming: Bool;

  private let m_focusModeActive: Bool;

  private let m_customFastForwardPossible: Bool;

  private let m_equippedRightHandWeapon: wref<WeaponObject>;

  private let m_aimAssistUpdateQueued: Bool;

  private let m_locomotionState: Int32;

  private let m_leftHandCyberwareState: Int32;

  private let m_meleeWeaponState: Int32;

  private let m_weaponZoomLevel: Float;

  private let m_controllingDeviceID: EntityID;

  private let m_gunshotRange: Float;

  private let m_explosionRange: Float;

  private let m_isInBodySlam: Bool;

  private let m_combatGadgetState: Int32;

  @default(PlayerPuppet, GameplayTier.Undefined)
  private let m_sceneTier: GameplayTier;

  private let m_nextBufferModifier: Int32;

  private let m_attackingNetrunnerID: EntityID;

  private let m_NPCDeathInstigator: wref<NPCPuppet>;

  private let m_bestTargettingWeapon: wref<WeaponObject>;

  private let m_bestTargettingDot: Float;

  private let m_targettingEnemies: Int32;

  private let m_isAimingAtFriendly: Bool;

  private let m_isAimingAtChild: Bool;

  private let m_distanceFromTargetSquared: Float;

  private let m_coverRecordID: TweakDBID;

  private let m_damageReductionRecordID: TweakDBID;

  private let m_visReductionRecordID: TweakDBID;

  private let m_lastDmgInflicted: EngineTime;

  @default(PlayerPuppet, false)
  private let m_critHealthRumblePlayed: Bool;

  private let m_critHealthRumbleDurationID: DelayID;

  private let m_lastHealthUpdate: Float;

  private let m_staminaListener: ref<StaminaListener>;

  private let m_memoryListener: ref<MemoryListener>;

  @default(PlayerPuppet, ESecurityAreaType.DISABLED)
  public let m_securityAreaTypeE3HACK: ESecurityAreaType;

  private let m_overlappedSecurityZones: [PersistentID];

  private let m_interestingFacts: InterestingFacts;

  private let m_interestingFactsListenersIds: InterestingFactsListenersIds;

  private let m_interestingFactsListenersFunctions: InterestingFactsListenersFunctions;

  private let m_visionModeController: ref<PlayerVisionModeController>;

  private let m_combatController: ref<PlayerCombatController>;

  private let m_handlingModifiers: ref<PlayerWeaponHandlingModifiers>;

  private let m_cachedGameplayRestrictions: [TweakDBID];

  private let m_delayEndGracePeriodAfterSpawnEventID: DelayID;

  private let m_CWMaskInVehicleInputHeld: Bool;

  private let m_friendlyDevicesHostileToEnemiesLock: RWLock;

  private let m_friendlyDevicesHostileToEnemies: [EntityID];

  private let m_pocketRadio: ref<PocketRadio>;

  private let m_bossThatTargetsPlayer: EntityID;

  @default(PlayerPuppet, 0)
  private let m_choiceTokenTextLayerId: Uint32;

  @default(PlayerPuppet, false)
  private let m_choiceTokenTextDrawn: Bool;

  public const func IsPlayer() -> Bool {
    return true;
  }

  public const func IsReplacer() -> Bool {
    return this.GetRecord().GetID() != t"Character.Player_Puppet_Base";
  }

  public const func IsVRReplacer() -> Bool {
    return this.GetRecord().GetID() == t"Character.q000_vr_replacer";
  }

  public const func IsJohnnyReplacer() -> Bool {
    return this.GetRecord().GetID() == t"Character.johnny_replacer";
  }

  public final const func IsReplicable() -> Bool {
    return true;
  }

  public final const func GetReplicatedStateClass() -> CName {
    return n"gamePlayerPuppetReplicatedState";
  }

  public final const func IsCoverModifierAdded() -> Bool {
    return this.m_coverModifierActive;
  }

  public final const func IsWorkspotDamageReductionAdded() -> Bool {
    return this.m_workspotDamageReductionActive;
  }

  public final const func IsWorkspotVisibilityReductionActive() -> Bool {
    return this.m_workspotVisibilityReductionActive;
  }

  public final const func GetOverlappedSecurityZones() -> [PersistentID] {
    return this.m_overlappedSecurityZones;
  }

  public final const func GetIsInWorkspotFinisher() -> Bool {
    return this.m_isInFinisher;
  }

  protected const func GetPS() -> ref<PlayerPuppetPS> {
    return this.GetBasePS() as PlayerPuppetPS;
  }

  public final const func GetCombatExitTimestamp() -> Float {
    return this.GetPS().GetCombatExitTimestamp();
  }

  public final const func SetPermanentFoodBonus(type: gamedataStatType, value: Float) -> Void {
    if Equals(type, gamedataStatType.HealthBonusBlackmarket) {
      this.GetPS().SetPermanentHealthBonus(value);
    } else {
      if Equals(type, gamedataStatType.StaminaRegenBonusBlackmarket) {
        this.GetPS().SetPermanentStaminaBonus(value);
      } else {
        if Equals(type, gamedataStatType.MemoryRegenBonusBlackmarket) {
          this.GetPS().SetPermanentMemoryBonus(value);
        };
      };
    };
  }

  public final const func GetPermanentFoodBonus(type: gamedataStatType) -> Float {
    let resultValue: Float;
    if Equals(type, gamedataStatType.HealthBonusBlackmarket) {
      resultValue = this.GetPS().GetPermanentHealthBonus();
    } else {
      if Equals(type, gamedataStatType.StaminaRegenBonusBlackmarket) {
        resultValue = this.GetPS().GetPermanentStaminaBonus();
      } else {
        if Equals(type, gamedataStatType.MemoryRegenBonusBlackmarket) {
          resultValue = this.GetPS().GetPermanentMemoryBonus();
        };
      };
    };
    return resultValue;
  }

  protected cb func OnRequestComponents(ri: EntityRequestComponentsInterface) -> Bool {
    super.OnRequestComponents(ri);
    EntityRequestComponentsInterface.RequestComponent(ri, n"phone", n"PlayerPhone", false);
    EntityRequestComponentsInterface.RequestComponent(ri, n"inspect", n"InspectionComponent", false);
    EntityRequestComponentsInterface.RequestComponent(ri, n"DEBUG_Visualizer", n"DEBUG_VisualizerComponent", false);
    EntityRequestComponentsInterface.RequestComponent(ri, n"quickSlots", n"QuickSlotsManager", false);
    EntityRequestComponentsInterface.RequestComponent(ri, n"camera", n"gameFPPCameraComponent", false);
    EntityRequestComponentsInterface.RequestComponent(ri, n"targeting_primary", n"gameTargetingComponent", false);
    EntityRequestComponentsInterface.RequestComponent(ri, n"breachFinder", n"gameBreachFinderComponent", false);
    EntityRequestComponentsInterface.RequestComponent(ri, n"ChaseSpawnComponent", n"gameChaseSpawnComponent", true);
    EntityRequestComponentsInterface.RequestComponent(ri, n"environmentDamageReceiver", n"EnvironmentDamageReceiverComponent", false);
    EntityRequestComponentsInterface.RequestComponent(ri, n"vehicleVisualCustomization", n"vehicleVisualCustomizationComponent", false);
    EntityRequestComponentsInterface.RequestComponent(ri, n"vehicleCameraManager", n"vehicleCameraManagerComponent", true);
    EntityRequestComponentsInterface.RequestComponent(ri, n"vehicleTPPCamera", n"vehicleTPPCameraComponent", true);
  }

  public final func FindVehicleCameraManager() -> ref<VehicleCameraManager> {
    let component: ref<VehicleCameraManagerComponent> = this.FindComponentByName(n"vehicleCameraManager") as VehicleCameraManagerComponent;
    if IsDefined(component) {
      return component.GetManagerHandle();
    };
    return null;
  }

  protected cb func OnTakeControl(ri: EntityResolveComponentsInterface) -> Bool {
    super.OnTakeControl(ri);
    this.m_quickSlotsManager = EntityResolveComponentsInterface.GetComponent(ri, n"quickSlots") as QuickSlotsManager;
    this.m_inspectionComponent = EntityResolveComponentsInterface.GetComponent(ri, n"inspect") as InspectionComponent;
    this.m_enviroDamageRcvComponent = EntityResolveComponentsInterface.GetComponent(ri, n"environmentDamageReceiver") as EnvironmentDamageReceiverComponent;
    this.DEBUG_Visualizer = EntityResolveComponentsInterface.GetComponent(ri, n"DEBUG_Visualizer") as DEBUG_VisualizerComponent;
    this.m_Phone = EntityResolveComponentsInterface.GetComponent(ri, n"phone") as PlayerPhone;
    this.m_fppCameraComponent = EntityResolveComponentsInterface.GetComponent(ri, n"camera") as FPPCameraComponent;
    this.m_primaryTargetingComponent = EntityResolveComponentsInterface.GetComponent(ri, n"targeting_primary") as TargetingComponent;
    this.m_breachFinderComponent = EntityResolveComponentsInterface.GetComponent(ri, n"breachFinder") as BreachFinderComponent;
    this.m_breachFinderComponent.Init(this);
    this.m_chaseSpawnComponent = EntityResolveComponentsInterface.GetComponent(ri, n"ChaseSpawnComponent") as gameChaseSpawnComponent;
    this.m_visionModeController = new PlayerVisionModeController();
    this.m_combatController = new PlayerCombatController();
  }

  protected cb func OnReleaseControl() -> Bool {
    this.m_visionModeController = null;
    this.m_combatController = null;
  }

  private final func GracePeriodAfterSpawn() -> Void {
    let invisibilityDuration: Float = TweakDBInterface.GetFloat(t"player.stealth.durationOfGracePeriodAfterSpawn", -1.00);
    if invisibilityDuration > 0.00 {
      this.SetInvisible(true);
      GameInstance.GetGodModeSystem(this.GetGame()).AddGodMode(this.GetEntityID(), gameGodModeType.Invulnerable, n"GracePeriodAfterSpawn");
      GameInstance.GetDelaySystem(this.GetGame()).CancelDelay(this.m_delayEndGracePeriodAfterSpawnEventID);
      this.m_delayEndGracePeriodAfterSpawnEventID = GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, new EndGracePeriodAfterSpawn(), invisibilityDuration);
    };
  }

  public final func GetVehicleVisualCustomizationComponent() -> ref<vehicleVisualCustomizationComponent> {
    let component: ref<vehicleVisualCustomizationComponent> = this.FindComponentByName(n"vehicleVisualCustomization") as vehicleVisualCustomizationComponent;
    if IsDefined(component) {
      return component;
    };
    return null;
  }

  private final func UnlockAccessPointPrograms() -> Void {
    let program: MinigameProgramData;
    program.actionID = t"MinigameAction.NetworkDataMineLootAll";
    program.programName = StringToName(LocKeyToString(TweakDBInterface.GetObjectActionRecord(t"MinigameAction.NetworkDataMineLootAll").ObjectActionUI().Caption()));
    this.UpdateMinigamePrograms(program, true);
    program.actionID = t"MinigameAction.NetworkDataMineLootAllAdvanced";
    program.programName = StringToName(LocKeyToString(TweakDBInterface.GetObjectActionRecord(t"MinigameAction.NetworkDataMineLootAllAdvanced").ObjectActionUI().Caption()));
    this.UpdateMinigamePrograms(program, true);
    program.actionID = t"MinigameAction.NetworkDataMineLootAllMaster";
    program.programName = StringToName(LocKeyToString(TweakDBInterface.GetObjectActionRecord(t"MinigameAction.NetworkDataMineLootAllMaster").ObjectActionUI().Caption()));
    this.UpdateMinigamePrograms(program, true);
  }

  protected cb func OnMakePlayerVisibleAfterSpawn(evt: ref<EndGracePeriodAfterSpawn>) -> Bool {
    this.SetInvisible(false);
    GameInstance.GetGodModeSystem(this.GetGame()).RemoveGodMode(this.GetEntityID(), gameGodModeType.Invulnerable, n"GracePeriodAfterSpawn");
  }

  public final func IsAimingAtFriendly() -> Bool {
    return this.m_isAimingAtFriendly;
  }

  public final func IsAimingAtChild() -> Bool {
    return this.m_isAimingAtChild;
  }

  public final func DistanceFromTargetSquared() -> Float {
    return this.m_distanceFromTargetSquared;
  }

  public final func ReevaluateLookAtTarget() -> Void {
    let targetingSystem: ref<TargetingSystem> = GameInstance.GetTargetingSystem(this.GetGame());
    let targetObject: ref<GameObject> = targetingSystem.GetLookAtObject(this, true, true);
    this.UpdateLookAtObject(targetObject);
  }

  private final func UpdateLookAtObject(target: ref<GameObject>) -> Void {
    if !IsDefined(target) {
      this.m_isAimingAtFriendly = false;
      this.m_isAimingAtChild = false;
    } else {
      this.m_isAimingAtFriendly = PlayerPuppet.IsTargetFriendlyNPC(this, target);
      this.m_isAimingAtChild = PlayerPuppet.IsTargetChildNPC(this, target);
      this.m_distanceFromTargetSquared = Vector4.DistanceSquared(this.GetWorldPosition(), target.GetWorldPosition());
    };
  }

  protected cb func OnLookAtObjectChangedEvent(evt: ref<LookAtObjectChangedEvent>) -> Bool {
    this.UpdateLookAtObject(evt.lookatObject);
  }

  protected cb func OnWeaponEquipEvent(evt: ref<WeaponEquipEvent>) -> Bool {
    AnimationControllerComponent.ApplyFeature(this, n"WeaponEquipType", evt.animFeature);
    AnimationControllerComponent.ApplyFeature(evt.item, n"WeaponEquipType", evt.animFeature);
  }

  protected cb func OnSetUpEquipmentOverlayEvent(evt: ref<SetUpEquipmentOverlayEvent>) -> Bool {
    this.m_equipmentMeshOverlayEffectName = evt.meshOverlayEffectName;
    this.m_equipmentMeshOverlayEffectTag = evt.meshOverlayEffectTag;
    this.m_equipmentMeshOverlaySlots = evt.meshOverlaySlots;
  }

  protected cb func OnAppearanceChangeFinishEvent(evt: ref<entAppearanceChangeFinishEvent>) -> Bool {
    let effect: ref<EffectInstance>;
    let i: Int32;
    let item: wref<ItemObject>;
    let ts: ref<TransactionSystem>;
    if IsNameValid(this.m_equipmentMeshOverlayEffectName) {
      ts = GameInstance.GetTransactionSystem(this.GetGame());
      i = 0;
      while i < ArraySize(this.m_equipmentMeshOverlaySlots) {
        item = ts.GetItemInSlot(this, this.m_equipmentMeshOverlaySlots[i]);
        if IsDefined(item) {
          effect = GameInstance.GetGameEffectSystem(this.GetGame()).CreateEffectStatic(this.m_equipmentMeshOverlayEffectName, this.m_equipmentMeshOverlayEffectTag, this);
          if IsDefined(effect) {
            EffectData.SetBool(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.enable, true);
            EffectData.SetBool(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.clearMaterialOverlayOnDetach, true);
            EffectData.SetEntity(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.entity, item);
            effect.Run();
          };
        };
        i += 1;
      };
      effect = GameInstance.GetGameEffectSystem(this.GetGame()).CreateEffectStatic(this.m_equipmentMeshOverlayEffectName, this.m_equipmentMeshOverlayEffectTag, this);
      if IsDefined(effect) {
        EffectData.SetBool(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.enable, true);
        EffectData.SetBool(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.clearMaterialOverlayOnDetach, true);
        EffectData.SetEntity(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.entity, this);
        effect.Run();
      };
    };
  }

  protected cb func OnSetSlowMoForOnePunchAttackEvent(evt: ref<SetSlowMoForOnePunchAttackEvent>) -> Bool {
    this.SetSlowMo(TDB.GetFloat(t"Items.StrongArms.slowMoAmount"), TDB.GetFloat(t"Items.StrongArms.slowDuration"));
  }

  protected final func SetSlowMo(slowMoAmount: Float, slowMoDuration: Float) -> Void {
    GameInstance.GetTimeSystem(this.GetGame()).SetTimeDilation(n"deflect", slowMoAmount, slowMoDuration);
  }

  protected cb func OnDodgeToAvoidCombatEvent(evt: ref<DodgeToAvoidCombatEvent>) -> Bool {
    let npc: ref<ScriptedPuppet> = GameInstance.FindEntityByID(this.GetGame(), evt.npcID) as ScriptedPuppet;
    let justDodged: Bool = StatusEffectHelper.HasStatusEffectWithTagConst(this, n"JustDodgedActiveBuffer");
    let playerIsVisible: Bool = npc.GetSenses().IsAgentVisible(this);
    let cooldownIsActive: Bool = StatusEffectHelper.HasStatusEffectWithTagConst(this, n"DodgeOutOfSightCooldown");
    let failureCooldownIsActive: Bool = StatusEffectHelper.HasStatusEffectWithTagConst(this, n"SecondChancePerkCooldown");
    if justDodged && !playerIsVisible && !cooldownIsActive && !failureCooldownIsActive {
      StatusEffectHelper.ApplyStatusEffect(npc, t"BaseStatusEffect.MemoryWipeOnDodge");
      GameObject.PlaySoundEvent(this, n"ui_gmpl_stealth_avoid_combat");
      GameInstance.GetDelaySystem(this.GetGame()).CancelDelay(evt.delayID);
      StatusEffectHelper.RemoveStatusEffect(this, t"BaseStatusEffect.SecondChancePerkTimeDilation");
      StatusEffectHelper.ApplyStatusEffect(this, t"BaseStatusEffect.DodgeOutOfSightCooldown");
      StatusEffectHelper.ApplyStatusEffect(this, t"BaseStatusEffect.SecondChancePerkCooldown");
    };
  }

  private final const func CanAvoidCombat() -> Bool {
    return !this.IsInCombat() || this.IsCombatStartBufferActive(TDB.GetFloat(t"AIGeneralSettings.avoidCombatBuffer"));
  }

  public final const func CanAvoidCombatWithDodge() -> Bool {
    return RPGManager.HasStatFlag(this, gamedataStatType.CanPlayerDodgeOnDetection) && this.CanAvoidCombat() && this.m_inCrouch;
  }

  public final const func CanAvoidCombatWithGag() -> Bool {
    return RPGManager.HasStatFlag(this, gamedataStatType.CanPlayerGagOnDetection) && this.CanAvoidCombat();
  }

  private final const func IsCombatStartBufferActive(buffer: Float) -> Bool {
    let bboard: ref<IBlackboard> = this.GetPlayerPerkDataBlackboard();
    let combatTimeStamp: Float = bboard.GetFloat(GetAllBlackboardDefs().PlayerPerkData.CombatStateTime);
    let currentTime: Float = EngineTime.ToFloat(GameInstance.GetSimTime(this.GetGame()));
    let combatDuration: Float = currentTime - combatTimeStamp;
    if combatDuration <= buffer {
      return true;
    };
    return false;
  }

  protected cb func OnActivateOpticalCamoToExitCombatEvent(evt: ref<ExitCombatOnOpticalCamoActivatedEvent>) -> Bool {
    let npcPuppet: wref<ScriptedPuppet>;
    if IsDefined(evt.npc) {
      npcPuppet = evt.npc as ScriptedPuppet;
      if IsDefined(npcPuppet) {
        npcPuppet.GetTargetTrackerComponent().ActivateThreat(this);
      };
      if this.IsInCombat() {
        StatusEffectHelper.ApplyStatusEffect(evt.npc, t"BaseStatusEffect.MemoryWipeExitCombat");
        GameObject.PlaySoundEvent(this, n"ui_gmpl_stealth_avoid_combat");
      };
    };
  }

  protected cb func OnEnablePlayerVisibilityEvent(evt: ref<EnablePlayerVisibilityEvent>) -> Bool {
    if this.IsInvisible() {
      this.SetInvisible(false);
    };
  }

  private final func EvaluateApplyingReplacerGameplayRestrictions() -> Void {
    let mainObj: wref<PlayerPuppet> = GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerMainGameObject() as PlayerPuppet;
    let controlledObj: wref<PlayerPuppet> = GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerControlledGameObject() as PlayerPuppet;
    let controlledObjRecordID: TweakDBID = controlledObj.GetRecordID();
    switch controlledObjRecordID {
      case t"Character.johnny_replacer":
        StatusEffectHelper.ApplyStatusEffect(mainObj, t"GameplayRestriction.BlockAllHubMenu");
        StatusEffectHelper.ApplyStatusEffect(mainObj, t"GameplayRestriction.BlockFastTravel");
        GameInstance.GetGodModeSystem(mainObj.GetGame()).AddGodMode(mainObj.GetEntityID(), gameGodModeType.Invulnerable, n"JohnnyReplacerSequence");
        break;
      case t"Character.q000_vr_replacer":
        StatusEffectHelper.ApplyStatusEffect(mainObj, t"GameplayRestriction.BlockAllHubMenu");
        StatusEffectHelper.ApplyStatusEffect(mainObj, t"GameplayRestriction.BlockFastTravel");
        break;
      case t"Character.mq304_assassin_replacer_male":
        StatusEffectHelper.ApplyStatusEffect(mainObj, t"GameplayRestriction.BlockAllHubMenu");
        StatusEffectHelper.ApplyStatusEffect(mainObj, t"GameplayRestriction.BlockFastTravel");
        break;
      case t"Character.mq304_assassin_replacer_female":
        StatusEffectHelper.ApplyStatusEffect(mainObj, t"GameplayRestriction.BlockAllHubMenu");
        StatusEffectHelper.ApplyStatusEffect(mainObj, t"GameplayRestriction.BlockFastTravel");
        break;
      case t"Character.Player_Puppet_Base":
        StatusEffectHelper.RemoveStatusEffect(mainObj, t"GameplayRestriction.BlockAllHubMenu");
        StatusEffectHelper.RemoveStatusEffect(mainObj, t"GameplayRestriction.BlockFastTravel");
        GameInstance.GetGodModeSystem(mainObj.GetGame()).RemoveGodMode(mainObj.GetEntityID(), gameGodModeType.Invulnerable, n"JohnnyReplacerSequence");
        GameInstance.GetInventoryManager(mainObj.GetGame()).RemoveEquipmentStateFlag(gameEEquipmentManagerState.InfiniteAmmo);
        break;
      case t"Character.kurt_replacer":
        StatusEffectHelper.ApplyStatusEffect(mainObj, t"GameplayRestriction.BlockAllHubMenu");
        StatusEffectHelper.ApplyStatusEffect(mainObj, t"GameplayRestriction.BlockFastTravel");
        break;
      default:
        StatusEffectHelper.RemoveStatusEffect(mainObj, t"GameplayRestriction.BlockAllHubMenu");
        StatusEffectHelper.RemoveStatusEffect(mainObj, t"GameplayRestriction.BlockFastTravel");
    };
  }

  private final func ResolveCachedGameplayRestrictions() -> Void {
    let psmBB: ref<IBlackboard> = this.GetPlayerStateMachineBlackboard();
    let i: Int32 = 0;
    while i < ArraySize(this.m_cachedGameplayRestrictions) {
      this.AddGameplayRestriction(psmBB, this.m_cachedGameplayRestrictions[i]);
      i += 1;
    };
    if IsDefined(psmBB) {
      ArrayClear(this.m_cachedGameplayRestrictions);
    };
  }

  private final func AddGameplayRestriction(psmBB: ref<IBlackboard>, actionRestrictionRecordID: TweakDBID) -> Void {
    let actionRestrictions: array<TweakDBID>;
    if IsDefined(psmBB) {
      actionRestrictions = FromVariant<array<TweakDBID>>(psmBB.GetVariant(GetAllBlackboardDefs().PlayerStateMachine.ActionRestriction));
      ArrayPush(actionRestrictions, actionRestrictionRecordID);
      psmBB.SetVariant(GetAllBlackboardDefs().PlayerStateMachine.ActionRestriction, ToVariant(actionRestrictions));
    } else {
      this.CacheGameplayRestriction(actionRestrictionRecordID);
    };
  }

  private final func RemoveGameplayRestriction(psmBB: ref<IBlackboard>, actionRestrictionRecordID: TweakDBID) -> Void {
    let actionRestrictions: array<TweakDBID>;
    if IsDefined(psmBB) {
      actionRestrictions = FromVariant<array<TweakDBID>>(psmBB.GetVariant(GetAllBlackboardDefs().PlayerStateMachine.ActionRestriction));
      ArrayRemove(actionRestrictions, actionRestrictionRecordID);
      psmBB.SetVariant(GetAllBlackboardDefs().PlayerStateMachine.ActionRestriction, ToVariant(actionRestrictions));
    };
  }

  private final func CacheGameplayRestriction(actionRestrictionRecordID: TweakDBID) -> Void {
    if !ArrayContains(this.m_cachedGameplayRestrictions, actionRestrictionRecordID) {
      ArrayPush(this.m_cachedGameplayRestrictions, actionRestrictionRecordID);
    };
  }

  private final func PlayerAttachedCallback(playerPuppet: ref<GameObject>) -> Void {
    let allBlackboardDef: ref<AllBlackboardDefinitions>;
    let attachmentSlotCallback: ref<PlayerPuppetAttachmentSlotsCallback>;
    let blackboard: ref<IBlackboard>;
    let deviceTakeControlBlackboard: ref<IBlackboard>;
    let playerDevelopmentData: ref<PlayerDevelopmentData>;
    let pmsBlackboard: ref<IBlackboard>;
    if playerPuppet == this {
      ArrayClear(this.m_callbackHandles);
      pmsBlackboard = this.GetPlayerStateMachineBlackboard();
      allBlackboardDef = GetAllBlackboardDefs();
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerFloat(allBlackboardDef.PlayerStateMachine.ZoomLevel, this, n"OnZoomLevelChange", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.Locomotion, this, n"OnLocomotionStateChanged", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.Combat, this, n"OnCombatStateChanged"));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerVariant(allBlackboardDef.PlayerStateMachine.SecurityZoneData, this, n"OnZoneChange"));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.LeftHandCyberware, this, n"OnLeftHandCyberwareStateChange", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.Vision, this, n"OnVisionStateChange", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.MeleeWeapon, this, n"OnMeleeWeaponStateChange", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.UpperBody, this, n"OnUpperBodyStateChange", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.Vehicle, this, n"OnVehicleStateChange", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.Weapon, this, n"OnWeaponStateChange", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerBool(allBlackboardDef.PlayerStateMachine.IsDriverCombatInTPP, this, n"OnDriverCombatCameraChange", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.DriverCombatWeaponType, this, n"OnDriverCombatWeaponTypeChange", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerBool(allBlackboardDef.PlayerStateMachine.IsInBodySlamState, this, n"OnBodySlamStateChange", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.CombatGadget, this, n"OnCombatGadgetStateChange", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.SceneTier, this, n"OnSceneTierChange", true));
      ArrayPush(this.m_callbackHandles, pmsBlackboard.RegisterListenerInt(allBlackboardDef.PlayerStateMachine.Takedown, this, n"OnPlayerTakedownStateChange", true));
      deviceTakeControlBlackboard = GameInstance.GetBlackboardSystem(this.GetGame()).Get(allBlackboardDef.DeviceTakeControl);
      if IsDefined(deviceTakeControlBlackboard) {
        ArrayPush(this.m_callbackHandles, deviceTakeControlBlackboard.RegisterListenerEntityID(allBlackboardDef.DeviceTakeControl.ActiveDevice, this, n"OnControllingDeviceChange", true));
      };
      blackboard = GameInstance.GetBlackboardSystem(this.GetGame()).Get(allBlackboardDef.UI_Stealth);
      if IsDefined(blackboard) {
        ArrayPush(this.m_callbackHandles, blackboard.RegisterListenerUint(allBlackboardDef.UI_Stealth.numberOfCombatants, this, n"OnNumberOfCombatantsChanged"));
      };
      this.m_allStatsListener = new PlayerPuppetAllStatListener();
      this.m_allStatsListener.player = this;
      GameInstance.GetStatsSystem(this.GetGame()).RegisterListener(Cast<StatsObjectID>(this.GetEntityID()), this.m_allStatsListener);
      this.m_hasKiroshiOpticsFragment = RPGManager.HasStatFlag(this, gamedataStatType.HasKiroshiOpticsFragment);
      attachmentSlotCallback = new PlayerPuppetAttachmentSlotsCallback();
      attachmentSlotCallback.m_player = this;
      attachmentSlotCallback.slotID = t"AttachmentSlots.WeaponRight";
      this.m_rightHandAttachmentSlotListener = GameInstance.GetTransactionSystem(this.GetGame()).RegisterAttachmentSlotListener(this, attachmentSlotCallback);
      this.UpdateWeaponRightEquippedItemInfo();
      if this.IsJohnnyReplacer() {
        GameplaySettingsSystem.SetWasEverJohnny(this, true);
      };
      if this.IsReplacer() {
        playerDevelopmentData = PlayerDevelopmentSystem.GetData(this);
        playerDevelopmentData.OnRestored(this.GetGame());
      };
      this.EvaluateApplyingReplacerGameplayRestrictions();
      this.RestoreMinigamePrograms();
      this.ResolveCachedGameplayRestrictions();
      this.RegisterInterestingFactsListeners();
      this.m_visionModeController.RegisterOwner(this);
      this.m_combatController.RegisterOwner(this);
      PlayerPuppet.ChacheQuickHackListCleanup(playerPuppet);
      this.UpdateAimAssist();
    };
  }

  private final func PlayerDetachedCallback(playerPuppet: ref<GameObject>) -> Void {
    if playerPuppet == this {
      this.UnregisterInterestingFactsListeners();
      this.m_visionModeController.UnregisterOwner();
      ArrayClear(this.m_callbackHandles);
      GameInstance.GetStatsSystem(this.GetGame()).UnregisterListener(Cast<StatsObjectID>(this.GetEntityID()), this.m_allStatsListener);
      GameInstance.GetTransactionSystem(this.GetGame()).UnregisterAttachmentSlotListener(this, this.m_rightHandAttachmentSlotListener);
    };
  }

  protected cb func OnGameAttached() -> Bool {
    let entityID: StatsObjectID;
    let evt: ref<GameLoadedFactReset>;
    let questSystem: ref<QuestsSystem>;
    let statPoolsSystem: ref<StatPoolsSystem>;
    let playerAttach: ref<PlayerAttachRequest> = new PlayerAttachRequest();
    playerAttach.owner = this;
    GameInstance.GetScriptableSystemsContainer(GetGameInstance()).QueueRequest(playerAttach);
    super.OnGameAttached();
    if this.IsControlledByLocalPeer() || IsHost() {
      this.RegisterInputListener(this, n"IconicCyberware");
      this.RegisterInputListener(this, n"CyberwareMaskInVehicle");
      this.RegisterInputListener(this, n"CallVehicle");
      this.RegisterInputListener(this, n"ToggleWalk");
      this.RegisterInputListener(this, n"SceneFastForward");
      this.RegisterInputListener(this, n"PocketRadio");
      this.m_Debug_DamageInputRec = new DEBUG_DamageInputReceiver();
      this.m_Debug_DamageInputRec.m_player = this;
      this.RegisterInputListener(this.m_Debug_DamageInputRec, n"Debug_KillAll");
      this.RegisterInputListener(this.m_Debug_DamageInputRec, n"Debug_Kill");
    } else {
      if IsClient() {
        this.RegisterRemoteMappin();
        this.RefreshCPOVisionAppearance();
        this.RegisterCPOMissionDataCallback();
      };
    };
    this.m_CPOMissionDataState = new CPOMissionDataState();
    this.isTalkingOnPhone = false;
    this.m_coverVisibilityPerkBlocked = false;
    this.m_behindCover = false;
    this.m_inCombat = false;
    this.SetIsBeingRevealed(false);
    this.m_hasBeenDetected = false;
    this.m_inCrouch = false;
    this.RegisterToFacts();
    this.EnableUIBlackboardListener(true);
    this.InitializeTweakDBRecords();
    this.DefineModifierGroups();
    this.RegisterStatListeners(this);
    this.UpdateVisibilityModifier();
    this.EnableInteraction(n"Revive", false);
    this.m_incapacitated = false;
    this.UpdatePlayerSettings();
    this.CalculateEncumbrance();
    AnimationControllerComponent.ApplyFeature(this, n"CameraGameplay", new AnimFeature_CameraGameplay());
    AnimationControllerComponent.ApplyFeature(this, n"CameraBodyOffset", new AnimFeature_CameraBodyOffset());
    this.m_playerAttachedCallbackID = GameInstance.GetPlayerSystem(GetGameInstance()).RegisterPlayerPuppetAttachedCallback(this, n"PlayerAttachedCallback");
    this.m_playerDetachedCallbackID = GameInstance.GetPlayerSystem(GetGameInstance()).RegisterPlayerPuppetDetachedCallback(this, n"PlayerDetachedCallback");
    this.UpdateSecondaryVisibilityOffset(false);
    this.EnableCombatVisibilityDistances(false);
    this.SetSenseObjectType(gamedataSenseObjectType.Player);
    this.GracePeriodAfterSpawn();
    StatusEffectHelper.RemoveStatusEffect(this, t"GameplayRestriction.FastForward");
    StatusEffectHelper.RemoveStatusEffect(this, t"GameplayRestriction.FastForwardCrouchLock");
    StatusEffectHelper.RemoveStatusEffect(this, t"BaseStatusEffect.DontShootAtMe");
    entityID = Cast<StatsObjectID>(this.GetEntityID());
    statPoolsSystem = GameInstance.GetStatPoolsSystem(this.GetGame());
    if StatusEffectSystem.ObjectHasStatusEffect(this, t"AIQuickHackStatusEffect.BeingHacked") {
      StatusEffectHelper.RemoveStatusEffect(this, t"AIQuickHackStatusEffect.BeingHacked");
      if statPoolsSystem.GetStatPoolValue(entityID, gamedataStatPoolType.QuickHackUpload) > 0.00 {
        statPoolsSystem.RequestRemovingStatPool(entityID, gamedataStatPoolType.QuickHackUpload);
      };
    };
    this.UnlockAccessPointPrograms();
    statPoolsSystem.RequestMarkingStatPoolActive(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.Stamina);
    statPoolsSystem.RequestMarkingStatPoolActive(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.HealingItemsCharges);
    statPoolsSystem.RequestMarkingStatPoolActive(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.GrenadesCharges);
    statPoolsSystem.RequestMarkingStatPoolActive(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.OpticalCamoCharges);
    statPoolsSystem.RequestMarkingStatPoolActive(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.ProjectileLauncherCharges);
    statPoolsSystem.RequestMarkingStatPoolActive(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.BerserkCharge);
    statPoolsSystem.RequestMarkingStatPoolActive(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.SandevistanCharge);
    statPoolsSystem.RequestMarkingStatPoolActive(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.CyberdeckOverclock);
    this.m_handlingModifiers = PlayerWeaponHandlingModifiers.Create(this);
    this.m_handlingModifiers.OnAttach();
    this.m_pocketRadio = new PocketRadio();
    this.m_pocketRadio.OnPlayerAttach(this);
    evt = new GameLoadedFactReset();
    GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, evt, 1.00);
    questSystem = GameInstance.GetQuestsSystem(this.GetGame());
    questSystem.SetFact(n"game_was_loaded", 1);
  }

  protected cb func OnGameLoadedFactReset(evt: ref<GameLoadedFactReset>) -> Bool {
    let questSystem: ref<QuestsSystem> = GameInstance.GetQuestsSystem(this.GetGame());
    questSystem.SetFact(n"game_was_loaded", 0);
  }

  protected cb func OnDetach() -> Bool {
    let playerDetach: ref<PlayerDetachRequest> = new PlayerDetachRequest();
    playerDetach.owner = this;
    playerDetach.ownerID = this.GetEntityID();
    GameInstance.GetScriptableSystemsContainer(GetGameInstance()).QueueRequest(playerDetach);
    this.UnregisterStatListeners(this);
    this.EnableUIBlackboardListener(false);
    if IsClient() {
      this.UnregisterRemoteMappin();
      this.UnregisterCPOMissionDataCallback();
    };
    this.CPOMissionDataOnPlayerDetach();
    this.SetEntityNoticedPlayerBBValue(false);
    if Cast<Bool>(this.m_playerAttachedCallbackID) {
      GameInstance.GetPlayerSystem(this.GetGame()).UnregisterPlayerPuppetAttachedCallback(this.m_playerAttachedCallbackID);
    };
    if Cast<Bool>(this.m_playerDetachedCallbackID) {
      GameInstance.GetPlayerSystem(this.GetGame()).UnregisterPlayerPuppetDetachedCallback(this.m_playerDetachedCallbackID);
    };
    this.m_pocketRadio.OnPlayerDetach(this);
  }

  protected const func ShouldRegisterToHUD() -> Bool {
    return false;
  }

  public final func GetCWMaskID() -> ItemID {
    return EquipmentSystem.GetData(this).FindItemInEquipAreaByTag(n"MaskCW", gamedataEquipmentArea.EyesCW);
  }

  public final func HasCWMask() -> Bool {
    return ItemID.IsValid(this.GetCWMaskID());
  }

  private final func ExecuteCWMask() -> Void {
    let dpadAction: ref<DPADActionPerformed>;
    let maskId: ItemID = this.GetCWMaskID();
    if !ItemID.IsValid(maskId) {
      return;
    };
    dpadAction = new DPADActionPerformed();
    dpadAction.action = EHotkey.RB;
    dpadAction.state = EUIActionState.COMPLETED;
    dpadAction.successful = true;
    GameInstance.GetUISystem(this.GetGame()).QueueEvent(dpadAction);
    ItemActionsHelper.UseItem(this, maskId);
  }

  protected cb func OnAction(action: ListenerAction, consumer: ListenerActionConsumer) -> Bool {
    if Equals(ListenerAction.GetName(action), n"ToggleWalk") && ListenerAction.IsButtonJustReleased(action) {
      this.ProcessToggleWalkInput();
      return true;
    };
    if Equals(ListenerAction.GetName(action), n"IconicCyberware") {
      if this.HasCWMask() && NotEquals(this.m_vehicleState, gamePSMVehicle.Default) {
        if GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_Scanner).GetBool(GetAllBlackboardDefs().UI_Scanner.UIVisible) {
          return true;
        };
        if Equals(ListenerAction.GetType(action), gameinputActionType.BUTTON_RELEASED) && this.m_CWMaskInVehicleInputHeld {
          this.m_CWMaskInVehicleInputHeld = false;
          this.ActivateIconicCyberware();
        } else {
          if Equals(ListenerAction.GetType(action), gameinputActionType.BUTTON_HOLD_COMPLETE) {
            this.m_CWMaskInVehicleInputHeld = false;
            this.ExecuteCWMask();
          } else {
            if Equals(ListenerAction.GetType(action), gameinputActionType.BUTTON_PRESSED) {
              this.m_CWMaskInVehicleInputHeld = true;
            };
          };
        };
      } else {
        if Equals(ListenerAction.GetType(action), gameinputActionType.BUTTON_PRESSED) {
          this.ActivateIconicCyberware();
        };
      };
    } else {
      if Equals(ListenerAction.GetName(action), n"CallVehicle") {
        if !GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_PlayerStats).GetBool(GetAllBlackboardDefs().UI_PlayerStats.isReplacer) && Equals(ListenerAction.GetType(action), gameinputActionType.BUTTON_RELEASED) {
          this.ProcessCallVehicleAction(ListenerAction.GetType(action));
        };
      } else {
        if Equals(ListenerAction.GetName(action), n"SceneFastForward") {
          if this.m_customFastForwardPossible {
            if Equals(ListenerAction.GetType(action), gameinputActionType.BUTTON_PRESSED) {
              GameInstance.GetTimeSystem(this.GetGame()).SetTimeDilation(n"customFFTimeDilation", 10.00);
              GameObjectEffectHelper.StartEffectEvent(this, n"transition_glitch_loop", false);
              GameInstance.GetAudioSystem(this.GetGame()).Play(n"motion_light_fast_2d");
            } else {
              if Equals(ListenerAction.GetType(action), gameinputActionType.BUTTON_RELEASED) && !DefaultTransition.IsFastForwardByLine(this) {
                GameInstance.GetTimeSystem(this.GetGame()).UnsetTimeDilation(n"customFFTimeDilation");
                GameObjectEffectHelper.StopEffectEvent(this, n"transition_glitch_loop");
              };
            };
          };
        } else {
          if Equals(ListenerAction.GetName(action), n"PocketRadio") {
            this.m_pocketRadio.HandleInputAction(action);
          };
        };
      };
    };
  }

  private final func KeybaordAndMouseControlsActive() -> Bool {
    return this.PlayerLastUsedKBM();
  }

  public func HasPrimaryOrSecondaryEquipment() -> Bool {
    return true;
  }

  private final func ProcessToggleWalkInput() -> Void {
    let psmEvent: ref<PSMPostponedParameterBool> = new PSMPostponedParameterBool();
    psmEvent.id = n"ToggleWalkInputRegistered";
    psmEvent.aspect = gamestateMachineParameterAspect.Permanent;
    psmEvent.value = true;
    this.QueueEvent(psmEvent);
  }

  public final func IconicCyberwareActivated() -> Void {
    if PlayerDevelopmentSystem.GetData(this).IsNewPerkBoughtAnyLevel(gamedataNewPerkType.Tech_Master_Perk_2) {
      if !StatusEffectSystem.ObjectHasStatusEffect(this, t"BaseStatusEffect.Tech_Master_Perk_2_Buff") {
        StatusEffectHelper.ApplyStatusEffect(this, t"BaseStatusEffect.Tech_Master_Perk_2_Buff");
      };
    };
  }

  private final func ActivateIconicCyberware() -> Void {
    let audioEvent: ref<SoundPlayEvent>;
    let dpadAction: ref<DPADActionPerformed>;
    let isInFocusMode: Bool;
    let activated: Bool = false;
    let activeItem: ItemID = EquipmentSystem.GetData(this).GetActiveItem(gamedataEquipmentArea.SystemReplacementCW);
    if !ItemID.IsValid(activeItem) {
      return;
    };
    isInFocusMode = this.GetPlayerStateMachineBlackboard().GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == 1;
    if isInFocusMode {
      QuickHackableHelper.TryToCycleOverclockedState(this);
      return;
    };
    dpadAction = new DPADActionPerformed();
    dpadAction.action = EHotkey.LBRB;
    dpadAction.state = EUIActionState.COMPLETED;
    dpadAction.successful = true;
    if GameInstance.GetStatsSystem(this.GetGame()).GetStatBoolValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.HasBerserk) {
      activated = ItemActionsHelper.UseItem(this, activeItem);
      if !activated {
        audioEvent = new SoundPlayEvent();
        audioEvent.soundName = n"ui_grenade_empty";
        this.QueueEvent(audioEvent);
      };
    } else {
      if GameInstance.GetStatsSystem(this.GetGame()).GetStatBoolValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.HasSandevistan) {
        if TimeDilationHelper.CanUseTimeDilation(this) {
          ItemActionsHelper.UseItem(this, activeItem);
          activated = false;
        };
      } else {
        if Equals(TweakDBInterface.GetCName(ItemID.GetTDBID(activeItem) + t".cyberwareType", n"None"), n"Sandevistan") && this.IsPhoneCallActive() {
          this.ShowSandevistanBlockedNotification();
          return;
        };
        if GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.HasCyberdeck) > 0.00 && GameInstance.GetStatsSystem(this.GetGame()).GetStatBoolValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.CanUseOverclock) {
          activated = QuickHackableHelper.TryToCycleOverclockedState(this);
        } else {
          dpadAction.successful = false;
        };
      };
    };
    if activated {
      this.IconicCyberwareActivated();
    };
    GameInstance.GetUISystem(this.GetGame()).QueueEvent(dpadAction);
  }

  private final func IsPhoneCallActive() -> Bool {
    let lastPhoneCallInformation: PhoneCallInformation;
    let blackboardSystem: ref<BlackboardSystem> = GameInstance.GetBlackboardSystem(GetGameInstance());
    let infoVariant: Variant = blackboardSystem.Get(GetAllBlackboardDefs().UI_ComDevice).GetVariant(GetAllBlackboardDefs().UI_ComDevice.PhoneCallInformation);
    if IsDefined(infoVariant) {
      lastPhoneCallInformation = FromVariant<PhoneCallInformation>(infoVariant);
      return Equals(lastPhoneCallInformation.callPhase, questPhoneCallPhase.StartCall) && (Equals(lastPhoneCallInformation.callMode, questPhoneCallMode.Video) || Equals(lastPhoneCallInformation.callMode, questPhoneCallMode.Audio));
    };
    return false;
  }

  private final func ShowSandevistanBlockedNotification() -> Void {
    let notificationEvent: ref<UIInGameNotificationEvent> = new UIInGameNotificationEvent();
    notificationEvent.m_notificationType = UIInGameNotificationType.SandevistanInCallRestriction;
    GameInstance.GetUISystem(this.GetGame()).QueueEvent(notificationEvent);
  }

  private final func ProcessCallVehicleAction(type: gameinputActionType) -> Void {
    let dpadAction: ref<DPADActionPerformed>;
    if !VehicleComponent.IsMountedToVehicle(this.GetGame(), this) && this.CheckRadialContextRequest() && !VehicleSystem.IsSummoningVehiclesRestricted(this.GetGame()) {
      this.SendSummonVehicleQuickSlotsManagerRequest();
    };
    dpadAction = new DPADActionPerformed();
    dpadAction.action = EHotkey.DPAD_RIGHT;
    dpadAction.successful = false;
    GameInstance.GetUISystem(this.GetGame()).QueueEvent(dpadAction);
  }

  protected cb func OnMountingEvent(evt: ref<MountingEvent>) -> Bool {
    let bikeObject: ref<BikeObject>;
    let missShotOverrideEvent: ref<OverrideMissShotOffset>;
    let allowsCombat: Bool = false;
    let mountedToVehicle: Bool = false;
    this.m_mountedVehicle = null;
    let playerStateMachineBlackboard: wref<IBlackboard> = GameInstance.GetBlackboardSystem(this.GetGame()).GetLocalInstanced(this.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    this.m_mountedVehicle = GameInstance.FindEntityByID(this.GetGame(), evt.request.lowLevelMountingInfo.parentId) as VehicleObject;
    if IsDefined(this.m_mountedVehicle) {
      mountedToVehicle = true;
      allowsCombat = VehicleComponent.GetVehicleAllowsCombat(this.GetGame(), this.m_mountedVehicle);
      bikeObject = this.m_mountedVehicle as BikeObject;
      if !IsDefined(bikeObject) {
        this.m_enviroDamageRcvComponent.Toggle(false);
      };
      if this.m_mountedVehicle.GetRecord().IsArmoredVehicle() {
        playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.InArmoredVehicle, true);
      };
    };
    playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.MountedToCombatVehicle, allowsCombat);
    playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.MountedToVehicle, mountedToVehicle);
    playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.MountedPreventFirstEquip, true);
    if Equals(evt.request.lowLevelMountingInfo.slotId.id, n"seat_front_left") {
      missShotOverrideEvent = new OverrideMissShotOffset();
      missShotOverrideEvent.overrideRecord = "vehicleMissShotOffset";
      this.QueueEvent(missShotOverrideEvent);
      if mountedToVehicle {
        this.m_pocketRadio.HandleVehicleMounted(this.m_mountedVehicle);
      };
    };
  }

  protected cb func OnUnmountingEvent(evt: ref<UnmountingEvent>) -> Bool {
    let missShotOverrideEvent: ref<OverrideMissShotOffset>;
    let playerStateMachineBlackboard: wref<IBlackboard>;
    let mountChild: ref<GameObject> = GameInstance.FindEntityByID(this.GetGame(), evt.request.lowLevelMountingInfo.childId) as GameObject;
    if IsDefined(mountChild) && mountChild.IsPlayer() {
      playerStateMachineBlackboard = this.GetPlayerStateMachineBlackboard();
      playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.MountedToCombatVehicle, false);
      playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.MountedToVehicle, false);
      playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.MountedToVehicleInDriverSeat, false);
      playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.InArmoredVehicle, false);
      playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.MountedPreventFirstEquip, false);
      if Equals(evt.request.lowLevelMountingInfo.slotId.id, n"seat_front_left") {
        this.m_pocketRadio.HandleVehicleUnmounted(this.m_mountedVehicle);
      };
      this.m_mountedVehicle = null;
    };
    this.m_enviroDamageRcvComponent.Toggle(true);
    if Equals(evt.request.lowLevelMountingInfo.slotId.id, n"seat_front_left") {
      missShotOverrideEvent = new OverrideMissShotOffset();
      this.QueueEvent(missShotOverrideEvent);
    };
  }

  public final const func GetMountedVehicle() -> ref<VehicleObject> {
    return this.m_mountedVehicle;
  }

  private final const func IsCallingVehicleRestricted() -> Bool {
    return PlayerGameplayRestrictions.IsHotkeyRestricted(this.GetGame(), EHotkey.DPAD_RIGHT);
  }

  public final func IsInPoliceVehicle() -> Bool {
    let vehicleObj: wref<VehicleObject>;
    let vehicleRecord: ref<Vehicle_Record>;
    VehicleComponent.GetVehicle(this.GetGame(), this, vehicleObj);
    if !IsDefined(vehicleObj) {
      return false;
    };
    if !VehicleComponent.GetVehicleRecord(vehicleObj, vehicleRecord) {
      return false;
    };
    return Equals(vehicleRecord.Affiliation().Type(), gamedataAffiliation.NCPD);
  }

  private final func GetUnlockedVehiclesSize() -> Int32 {
    let unlockedVehicles: array<PlayerVehicle>;
    GameInstance.GetVehicleSystem(this.GetGame()).GetPlayerUnlockedVehicles(unlockedVehicles);
    return ArraySize(unlockedVehicles);
  }

  private final func SendSummonVehicleQuickSlotsManagerRequest() -> Void {
    let evt: ref<CallAction> = new CallAction();
    evt.calledAction = QuickSlotActionType.SummonVehicle;
    this.QueueEvent(evt);
  }

  private final func CheckVehicleSystemGarageState() -> Bool {
    let blackboard: ref<IBlackboard> = GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().VehicleSummonData);
    let garageState: Uint32 = blackboard.GetUint(GetAllBlackboardDefs().VehicleSummonData.GarageState);
    return Equals(IntEnum<vehicleGarageState>(garageState), vehicleGarageState.SummonAvailable);
  }

  private final func CheckRadialContextRequest() -> Bool {
    return !GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_QuickSlotsData).GetBool(GetAllBlackboardDefs().UI_QuickSlotsData.UIRadialContextRequest);
  }

  private final func OnActionMultiplayer(action: ListenerAction, consumer: ListenerActionConsumer) -> Void {
    let isVisionModeActive: Bool;
    let pingSystem: ref<PingSystem>;
    if Equals(ListenerAction.GetName(action), n"MP_TriggerPing") {
      isVisionModeActive = this.GetPlayerStateMachineBlackboard().GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == 1;
      if !isVisionModeActive {
        if Equals(ListenerAction.GetType(action), gameinputActionType.BUTTON_RELEASED) {
          pingSystem = GameInstance.GetPingSystem(this.GetGame());
          if IsDefined(pingSystem) {
            pingSystem.TriggerPing(this);
          };
        };
      };
    };
  }

  private final func GetCPOQuickSlotID(action: ListenerAction) -> Int32 {
    if GameInstance.GetRuntimeInfo(this.GetGame()).IsMultiplayer() || GameInstance.GetPlayerSystem(this.GetGame()).IsCPOControlSchemeForced() {
      if Equals(ListenerAction.GetName(action), n"QuickSlot1") && ListenerAction.IsButtonJustReleased(action) {
        return 0;
      };
      if Equals(ListenerAction.GetName(action), n"QuickSlot2") && ListenerAction.IsButtonJustReleased(action) {
        return 1;
      };
      if Equals(ListenerAction.GetName(action), n"QuickSlot3") && ListenerAction.IsButtonJustReleased(action) {
        return 2;
      };
    };
    return -1;
  }

  private final func UpdatePlayerSettings() -> Void {
    let meleeCameraShakeWeight: Float = TweakDBInterface.GetFloat(t"player.camera.meleeCameraShakeWeight", 1.00);
    let disableHeadBobbing: Bool = TweakDBInterface.GetBool(t"player.camera.disableHeadBobbing", false);
    AnimationControllerComponent.SetInputFloat(this, n"melee_camera_shake_weight", meleeCameraShakeWeight);
    AnimationControllerComponent.SetInputBool(this, n"disable_camera_bobbing", disableHeadBobbing);
  }

  public final const func GetQuickSlotsManager() -> ref<QuickSlotsManager> {
    return this.m_quickSlotsManager;
  }

  public final const func GetInspectionComponent() -> ref<InspectionComponent> {
    return this.m_inspectionComponent;
  }

  public final const func GetFPPCameraComponent() -> ref<FPPCameraComponent> {
    return this.m_fppCameraComponent;
  }

  public final func GetBufferModifier() -> Int32 {
    return this.m_nextBufferModifier;
  }

  public final func SetBufferModifier(i: Int32) -> Void {
    this.m_nextBufferModifier = i;
  }

  public final static func GetCriticalHealthThreshold() -> Float {
    return TDB.GetFloat(t"player.hitVFX.critHealthThreshold");
  }

  public final static func GetLowHealthThreshold() -> Float {
    return TDB.GetFloat(t"player.hitVFX.lowHealthThreshold");
  }

  public final func GetPlayerWeaponHandler() -> ref<PlayerWeaponHandlingModifiers> {
    return this.m_handlingModifiers;
  }

  public final func GetPocketRadio() -> ref<PocketRadio> {
    return this.m_pocketRadio;
  }

  public final static func IsTargetFriendlyNPC(player: ref<PlayerPuppet>, target: ref<Entity>) -> Bool {
    let attitudeTowardsPlayer: EAIAttitude;
    let targetAsPuppet: ref<ScriptedPuppet>;
    let targetAsWeakspot: ref<WeakspotObject> = target as WeakspotObject;
    if IsDefined(targetAsWeakspot) {
      target = targetAsWeakspot.GetOwner();
    };
    targetAsPuppet = target as ScriptedPuppet;
    if targetAsPuppet.GetRecordID() == t"Character.Silverhand" {
      return false;
    };
    if IsDefined(player) && IsDefined(targetAsPuppet) && ScriptedPuppet.IsAlive(targetAsPuppet) && !StatusEffectSystem.ObjectHasStatusEffect(targetAsPuppet, t"BaseStatusEffect.IgnoreWeaponSafe") {
      attitudeTowardsPlayer = GameObject.GetAttitudeTowards(player, targetAsPuppet);
      if Equals(attitudeTowardsPlayer, EAIAttitude.AIA_Friendly) {
        return true;
      };
    };
    return false;
  }

  public final static func IsTargetChildNPC(player: ref<PlayerPuppet>, target: ref<Entity>) -> Bool {
    let targetAsPuppet: ref<ScriptedPuppet> = target as ScriptedPuppet;
    if IsDefined(player) && IsDefined(targetAsPuppet) && ScriptedPuppet.IsAlive(targetAsPuppet) {
      if targetAsPuppet.IsCharacterChildren() {
        return true;
      };
    };
    return false;
  }

  public final const func GetPlayerStateMachineBlackboard() -> ref<IBlackboard> {
    let psmBlackboard: ref<IBlackboard> = GameInstance.GetBlackboardSystem(this.GetGame()).GetLocalInstanced(this.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    return psmBlackboard;
  }

  public final const func GetPlayerPerkDataBlackboard() -> ref<IBlackboard> {
    return GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().PlayerPerkData);
  }

  public final const func GetHackingDataBlackboard() -> ref<IBlackboard> {
    return GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().HackingData);
  }

  public final static func GetCurrentLocomotionState(player: wref<PlayerPuppet>) -> gamePSMLocomotionStates {
    let blackboard: ref<IBlackboard> = player.GetPlayerStateMachineBlackboard();
    return IntEnum<gamePSMLocomotionStates>(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Locomotion));
  }

  public final static func GetCurrentHighLevelState(player: wref<PlayerPuppet>) -> gamePSMHighLevel {
    let blackboard: ref<IBlackboard> = player.GetPlayerStateMachineBlackboard();
    return IntEnum<gamePSMHighLevel>(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.HighLevel));
  }

  public final static func GetCurrentCombatState(player: wref<PlayerPuppet>) -> gamePSMCombat {
    let blackboard: ref<IBlackboard> = player.GetPlayerStateMachineBlackboard();
    return IntEnum<gamePSMCombat>(blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Combat));
  }

  public final static func GetCurrentVehicleState(player: wref<PlayerPuppet>) -> gamePSMVehicle {
    return player.m_vehicleState;
  }

  public final static func GetQuickMeleeCooldown() -> Float {
    return TweakDBInterface.GetFloat(t"player.quickMelee.quickMeleeCooldown", 5.00);
  }

  private final func GetDamageThresholdParams() -> Void {
    this.m_highDamageThreshold = TweakDBInterface.GetFloat(t"player.damageThresholds.highDamageThreshold", 0.40);
    this.m_medDamageThreshold = TweakDBInterface.GetFloat(t"player.damageThresholds.medDamageThreshold", 0.10);
    this.m_lowDamageThreshold = TweakDBInterface.GetFloat(t"player.damageThresholds.lowDamageThreshold", 0.10);
    this.m_meleeHighDamageThreshold = TweakDBInterface.GetFloat(t"player.damageThresholds.meleeHighDamageThreshold", 0.10);
    this.m_meleeMedDamageThreshold = TweakDBInterface.GetFloat(t"player.damageThresholds.meleeMedDamageThreshold", 0.20);
    this.m_meleeLowDamageThreshold = TweakDBInterface.GetFloat(t"player.damageThresholds.meleeLowDamageThreshold", 0.30);
    this.m_explosionHighDamageThreshold = TweakDBInterface.GetFloat(t"player.damageThresholds.explosionHighDamageThreshold", 0.00);
    this.m_explosionMedDamageThreshold = TweakDBInterface.GetFloat(t"player.damageThresholds.explosionMedDamageThreshold", 0.00);
    this.m_explosionLowDamageThreshold = TweakDBInterface.GetFloat(t"player.damageThresholds.explosionLowDamageThreshold", 0.00);
  }

  private final func EnableUIBlackboardListener(enable: Bool) -> Void {
    let blackboardSystem: ref<BlackboardSystem> = GameInstance.GetBlackboardSystem(GetGameInstance());
    let uiBlackboard: ref<IBlackboard> = blackboardSystem.Get(GetAllBlackboardDefs().UIGameData);
    let quickSlotsBlackboard: ref<IBlackboard> = blackboardSystem.Get(GetAllBlackboardDefs().UI_QuickSlotsData);
    let phoneBlackboard: wref<IBlackboard> = blackboardSystem.Get(GetAllBlackboardDefs().UI_ComDevice);
    let targetingBlackBoard: wref<IBlackboard> = blackboardSystem.Get(GetAllBlackboardDefs().UI_TargetingInfo);
    if enable {
      this.m_itemLogBlackboard = blackboardSystem.Get(GetAllBlackboardDefs().UI_ItemLog);
      this.m_interactionDataListener = uiBlackboard.RegisterListenerVariant(GetAllBlackboardDefs().UIGameData.InteractionData, this, n"OnInteractionStateChange");
      this.m_popupIsModalListener = uiBlackboard.RegisterListenerBool(GetAllBlackboardDefs().UIGameData.Popup_IsModal, this, n"OnUIContextChange");
      this.m_uiVendorContextListener = uiBlackboard.RegisterListenerBool(GetAllBlackboardDefs().UIGameData.UIVendorContextRequest, this, n"OnUIVendorContextChange");
      this.m_uiRadialContextistener = quickSlotsBlackboard.RegisterListenerBool(GetAllBlackboardDefs().UI_QuickSlotsData.UIRadialContextRequest, this, n"OnUIRadialContextChange");
      this.m_contactsActiveListener = phoneBlackboard.RegisterListenerBool(GetAllBlackboardDefs().UI_ComDevice.ContactsActive, this, n"OnUIContactListContextChanged");
      this.m_smsMessengerActiveListener = phoneBlackboard.RegisterListenerBool(GetAllBlackboardDefs().UI_ComDevice.SmsMessengerActive, this, n"OnUISmsMessengerContextChanged");
      this.m_currentVisibleTargetListener = targetingBlackBoard.RegisterListenerEntityID(GetAllBlackboardDefs().UI_TargetingInfo.CurrentVisibleTarget, this, n"OnCurrentVisibleTargetChanged");
    } else {
      uiBlackboard.UnregisterListenerVariant(GetAllBlackboardDefs().UIGameData.InteractionData, this.m_interactionDataListener);
      uiBlackboard.UnregisterListenerBool(GetAllBlackboardDefs().UIGameData.Popup_IsModal, this.m_popupIsModalListener);
      uiBlackboard.UnregisterListenerBool(GetAllBlackboardDefs().UIGameData.UIVendorContextRequest, this.m_uiVendorContextListener);
      quickSlotsBlackboard.UnregisterListenerBool(GetAllBlackboardDefs().UI_QuickSlotsData.UIRadialContextRequest, this.m_uiRadialContextistener);
      phoneBlackboard.UnregisterListenerBool(GetAllBlackboardDefs().UI_ComDevice.ContactsActive, this.m_contactsActiveListener);
      phoneBlackboard.UnregisterListenerBool(GetAllBlackboardDefs().UI_ComDevice.SmsMessengerActive, this.m_smsMessengerActiveListener);
      targetingBlackBoard.UnregisterListenerEntityID(GetAllBlackboardDefs().UI_TargetingInfo.CurrentVisibleTarget, this.m_currentVisibleTargetListener);
      this.m_itemLogBlackboard = null;
      this.m_interactionDataListener = null;
      this.m_popupIsModalListener = null;
      this.m_uiVendorContextListener = null;
      this.m_uiRadialContextistener = null;
      this.m_contactsActiveListener = null;
      this.m_smsMessengerActiveListener = null;
      this.m_currentVisibleTargetListener = null;
    };
  }

  private final func SetupInPlayerDevelopmentSystem() -> Void {
    let build: gamedataBuildType;
    let cpoStartingBuildName: String;
    let setProgressionBuildReq: ref<SetProgressionBuild>;
    let updatePDS: ref<UpdatePlayerDevelopment> = new UpdatePlayerDevelopment();
    updatePDS.Set(this);
    GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"PlayerDevelopmentSystem").QueueRequest(updatePDS);
    if GameInstance.GetRuntimeInfo(this.GetGame()).IsMultiplayer() {
      cpoStartingBuildName = TweakDBInterface.GetCharacterRecord(this.GetRecordID()).CpoCharacterBuild();
      build = IntEnum<gamedataBuildType>(Cast<Int32>(EnumValueFromString("gamedataBuildType", cpoStartingBuildName)));
      setProgressionBuildReq = new SetProgressionBuild();
      setProgressionBuildReq.Set(this, build);
      GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"PlayerDevelopmentSystem").QueueRequest(setProgressionBuildReq);
    };
  }

  private final func UpdateVisibilityModifier() -> Void {
    let detectMultEvent: ref<VisibleObjectDetectionMultEvent>;
    let statsSystem: ref<StatsSystem> = GameInstance.GetStatsSystem(this.GetGame());
    let visibilityValue: Float = statsSystem.GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.Visibility);
    if visibilityValue >= 0.00 {
      detectMultEvent = new VisibleObjectDetectionMultEvent();
      detectMultEvent.multiplier = visibilityValue;
      this.QueueEvent(detectMultEvent);
    };
  }

  public final static func SendOnBeingNoticed(player: wref<PlayerPuppet>, objectThatNoticed: wref<GameObject>) -> Void {
    let evt: ref<OnBeingNoticed>;
    let revealEvt: ref<RevealObjectEvent>;
    if !IsDefined(player) || !IsDefined(objectThatNoticed) {
      return;
    };
    evt = new OnBeingNoticed();
    evt.objectThatNoticed = objectThatNoticed;
    player.QueueEvent(evt);
    if GameInstance.GetStatsSystem(player.GetGame()).GetStatValue(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.HasAutomaticTagging) > 0.00 {
      revealEvt = new RevealObjectEvent();
      revealEvt.reveal = true;
      revealEvt.reason.reason = n"AutomaticTagging";
      revealEvt.reason.sourceEntityId = player.GetEntityID();
      revealEvt.lifetime = 15.00;
      objectThatNoticed.QueueEvent(revealEvt);
    };
  }

  protected cb func OnBeingNoticed(evt: ref<OnBeingNoticed>) -> Bool {
    if !IsMultiplayer() {
      this.SetEntityNoticedPlayerBBValue(true);
      if !this.m_inCombat && EngineTime.ToFloat(GameInstance.GetTimeSystem(this.GetGame()).GetSimTime()) - this.GetPS().GetCombatExitTimestamp() > 45.00 {
        ReactionManagerComponent.SendVOEventToSquad(this, n"detection_warning");
      };
    };
  }

  private final func SetEntityNoticedPlayerBBValue(b: Bool) -> Void {
    if this.m_inCombat || this.IsReplacer() {
      return;
    };
    if Equals(b, true) {
      this.GetPlayerPerkDataBlackboard().SetUint(GetAllBlackboardDefs().PlayerPerkData.EntityNoticedPlayer, 1u);
      this.QueueEvent(new ClearBeingNoticedBB());
    } else {
      this.GetPlayerPerkDataBlackboard().SetUint(GetAllBlackboardDefs().PlayerPerkData.EntityNoticedPlayer, 0u);
    };
  }

  protected cb func OnClearBeingNoticedBB(evt: ref<ClearBeingNoticedBB>) -> Bool {
    this.SetEntityNoticedPlayerBBValue(false);
  }

  public final func IsBeingRevealed() -> Bool {
    return this.m_isBeingRevealed;
  }

  public final func SetIsBeingRevealed(isBeingRevealed: Bool) -> Void {
    this.m_isBeingRevealed = isBeingRevealed;
  }

  protected cb func OnBeingTargetByLaserSight(evt: ref<BeingTargetByLaserSightUpdateEvent>) -> Bool {
    let dot: Float;
    let forward: Vector4;
    if IsClient() && this.IsControlledByLocalPeer() || !IsMultiplayer() {
      if Equals(evt.state, LaserTargettingState.End) {
        if this.m_bestTargettingWeapon == evt.weapon {
          this.m_bestTargettingDot = -1.00;
          this.m_bestTargettingWeapon = null;
        };
        this.m_targettingEnemies -= 1;
        if this.m_targettingEnemies == 0 {
          GameObjectEffectHelper.StopEffectEvent(this, n"laser_targetting");
          this.m_laserTargettingVfxBlackboard = null;
        };
        return true;
      };
      if Equals(evt.state, LaserTargettingState.Start) {
        this.m_targettingEnemies += 1;
      };
      if !IsDefined(this.m_laserTargettingVfxBlackboard) {
        this.m_laserTargettingVfxBlackboard = new worldEffectBlackboard();
        GameObjectEffectHelper.StartEffectEvent(this, n"laser_targetting", false, this.m_laserTargettingVfxBlackboard);
      };
      forward = Matrix.GetDirectionVector(this.GetFPPCameraComponent().GetLocalToWorld());
      dot = -Vector4.Dot(forward, evt.weapon.GetWorldForward());
      if this.m_bestTargettingWeapon != evt.weapon {
        if dot > this.m_bestTargettingDot {
          this.m_bestTargettingWeapon = evt.weapon;
        } else {
          return true;
        };
      };
      this.m_laserTargettingVfxBlackboard.SetValue(n"laser_angle", dot);
      this.m_bestTargettingDot = dot;
    };
  }

  protected cb func OnBeingTarget(evt: ref<OnBeingTarget>) -> Bool {
    let evtToSend: ref<OnBeingTarget>;
    let puppetTargetingPlayer: wref<ScriptedPuppet> = evt.objectThatTargets as ScriptedPuppet;
    let npcPuppet: ref<NPCPuppet> = puppetTargetingPlayer as NPCPuppet;
    if IsDefined(npcPuppet) {
      evtToSend = new OnBeingTarget();
      evtToSend.objectThatTargets = evt.objectThatTargets;
      evtToSend.noLongerTarget = evt.noLongerTarget;
      npcPuppet.QueueEvent(evtToSend);
    };
  }

  protected cb func OnInteractionStateChange(value: Variant) -> Bool {
    let psmEvent: ref<PSMPostponedParameterBool>;
    let interactionData: bbUIInteractionData = FromVariant<bbUIInteractionData>(value);
    if bbUIInteractionData.HasAnyInteraction(interactionData) {
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"OnInteractionStateActive";
    } else {
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"OnInteractionStateInactive";
    };
    psmEvent.value = true;
    this.QueueEvent(psmEvent);
  }

  protected cb func OnUpdateVisibilityModifierEvent(evt: ref<UpdateVisibilityModifierEvent>) -> Bool {
    this.UpdateVisibilityModifier();
  }

  protected cb func OnUpdateAutoRevealStatEvent(evt: ref<UpdateAutoRevealStatEvent>) -> Bool {
    this.GetPS().SetAutoReveal(evt.hasAutoReveal);
  }

  public final const func HasAutoReveal() -> Bool {
    return this.GetPS().HasAutoReveal();
  }

  protected cb func OnUIContextChange(value: Bool) -> Bool {
    let psmEvent: ref<PSMPostponedParameterBool>;
    if value {
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"OnUIContextActive";
    } else {
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"OnUIContextInactive";
    };
    psmEvent.value = true;
    this.QueueEvent(psmEvent);
  }

  protected cb func OnUIRadialContextChange(value: Bool) -> Bool {
    let psmEvent: ref<PSMPostponedParameterBool>;
    if value {
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"OnUIRadialContextActive";
    } else {
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"OnUIRadialContextInactive";
    };
    psmEvent.value = true;
    this.QueueEvent(psmEvent);
  }

  protected cb func OnCurrentVisibleTargetChanged(targetId: EntityID) -> Bool {
    let bbSystem: ref<BlackboardSystem>;
    if !EntityID.IsDefined(targetId) {
      bbSystem = GameInstance.GetBlackboardSystem(this.GetGame());
      bbSystem.Get(GetAllBlackboardDefs().UI_NameplateData).SetInt(GetAllBlackboardDefs().UI_NameplateData.DamageProjection, 0, true);
      GameInstance.GetDamageSystem(this.GetGame()).ClearPreviewTargetStruct();
    };
  }

  protected cb func OnUIContactListContextChanged(value: Bool) -> Bool {
    let psmEvent: ref<PSMPostponedParameterBool>;
    if value {
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"OnUIContactListContextActive";
    } else {
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"OnUIContactListContextInactive";
    };
    psmEvent.value = true;
    this.QueueEvent(psmEvent);
  }

  protected cb func OnUISmsMessengerContextChanged(value: Bool) -> Bool {
    let psmEvent: ref<PSMPostponedParameterBool>;
    if value {
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"OnUISmsMessengerContextActive";
    } else {
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"OnUISmsMessengerContextInactive";
    };
    psmEvent.value = true;
    this.QueueEvent(psmEvent);
  }

  protected cb func OnUIVendorContextChange(value: Bool) -> Bool {
    let psmEvent: ref<PSMPostponedParameterBool>;
    if value {
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"OnUIVendorContextActive";
    } else {
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"OnUIVendorContextInactive";
    };
    psmEvent.value = true;
    this.QueueEvent(psmEvent);
  }

  protected cb func OnExperienceGained(evt: ref<ExperiencePointsEvent>) -> Bool {
    let addExpRequest: ref<AddExperience> = new AddExperience();
    addExpRequest.Set(this, evt.amount, evt.type, evt.isDebug);
    GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"PlayerDevelopmentSystem").QueueRequest(addExpRequest);
  }

  protected cb func OnLevelUp(evt: ref<LevelUpdateEvent>) -> Bool;

  protected cb func OnRequestStats(evt: ref<RequestStats>) -> Bool {
    let requestStatsEvent: ref<RequestStatsBB> = new RequestStatsBB();
    requestStatsEvent.Set(this);
    GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"PlayerDevelopmentSystem").QueueRequest(requestStatsEvent);
  }

  protected cb func OnBuyAttribute(evt: ref<RequestBuyAttribute>) -> Bool {
    let attType: gamedataStatType = evt.type;
    let request: ref<BuyAttribute> = new BuyAttribute();
    request.Set(this, attType);
    GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"PlayerDevelopmentSystem").QueueRequest(request);
  }

  protected cb func OnItemAddedToSlot(evt: ref<ItemAddedToSlot>) -> Bool {
    let equipSlot: SEquipSlot;
    let equipmentBB: ref<IBlackboard>;
    let equipmentData: ref<EquipmentSystemPlayerData>;
    let itemType: gamedataItemCategory;
    let newApp: CName;
    let paperdollEquipData: SPaperdollEquipData;
    let itemID: ItemID = evt.GetItemID();
    let itemTDBID: TweakDBID = ItemID.GetTDBID(itemID);
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(itemTDBID);
    let slotID: TweakDBID = evt.GetSlotID();
    if IsDefined(itemRecord) && IsDefined(itemRecord.ItemCategory()) {
      itemType = itemRecord.ItemCategory().Type();
    };
    if Equals(itemType, gamedataItemCategory.Weapon) {
      newApp = TweakDBInterface.GetCName(itemTDBID + t".specific_player_appearance", n"None");
      if NotEquals(newApp, n"None") {
        GameInstance.GetTransactionSystem(this.GetGame()).ChangeItemAppearanceByName(this, itemID, newApp);
      };
      if slotID == t"AttachmentSlots.WeaponRight" {
        equipmentBB = GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_Equipment);
        if IsDefined(equipmentBB) {
          paperdollEquipData.equipArea.areaType = TweakDBInterface.GetItemRecord(itemTDBID).EquipArea().Type();
          paperdollEquipData.equipArea.activeIndex = 0;
          equipSlot.itemID = itemID;
          ArrayPush(paperdollEquipData.equipArea.equipSlots, equipSlot);
          paperdollEquipData.equipped = true;
          paperdollEquipData.placementSlot = EquipmentSystem.GetPlacementSlot(itemID);
          equipmentBB.SetVariant(GetAllBlackboardDefs().UI_Equipment.lastModifiedArea, ToVariant(paperdollEquipData));
        };
      };
    } else {
      if Equals(itemType, gamedataItemCategory.Clothing) {
        equipmentData = EquipmentSystem.GetData(GetPlayer(this.GetGame()));
        if IsDefined(equipmentData) && !equipmentData.IsVisualSetActive() {
          equipmentData.OnEquipProcessVisualTags(itemID);
        };
      };
    };
    if slotID == t"AttachmentSlots.WeaponRight" || slotID == t"AttachmentSlots.WeaponLeft" {
      EquipmentSystemPlayerData.UpdateArmSlot(this, evt.GetItemID(), false);
    };
    super.OnItemAddedToSlot(evt);
  }

  protected cb func OnPartAddedToSlotEvent(evt: ref<PartAddedToSlotEvent>) -> Bool {
    let i: Int32;
    let packages: array<wref<GameplayLogicPackage_Record>>;
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(evt.partID));
    if IsDefined(itemRecord) {
      itemRecord.OnAttach(packages);
    };
    i = 0;
    while i < ArraySize(packages) {
      RPGManager.ApplyGLP(this, packages[i]);
      i += 1;
    };
  }

  protected cb func OnClearItemAppearanceEvent(evt: ref<ClearItemAppearanceEvent>) -> Bool {
    let equipmentData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(GetPlayer(this.GetGame()));
    equipmentData.OnClearItemAppearance(evt.itemID);
  }

  protected cb func OnResetItemAppearanceEvent(evt: ref<ResetItemAppearanceEvent>) -> Bool {
    let equipmentData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(GetPlayer(this.GetGame()));
    equipmentData.OnResetItemAppearance(evt.itemID);
  }

  protected cb func OnUnderwearEquipFailsafeEvent(evt: ref<UnderwearEquipFailsafeEvent>) -> Bool {
    let equipmentData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(GetPlayer(this.GetGame()));
    equipmentData.OnUnderwearEquipFailsafe(evt.bottom);
  }

  protected cb func OnItemRemovedFromSlot(evt: ref<ItemRemovedFromSlot>) -> Bool {
    let equipSlot: SEquipSlot;
    let equipmentBB: ref<IBlackboard>;
    let itemType: gamedataItemCategory;
    let paperdollEquipData: SPaperdollEquipData;
    let itemID: ItemID = evt.GetItemID();
    let itemTDBID: TweakDBID = ItemID.GetTDBID(itemID);
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(itemTDBID);
    if IsDefined(itemRecord) && IsDefined(itemRecord.ItemCategory()) {
      itemType = itemRecord.ItemCategory().Type();
    };
    if Equals(itemType, gamedataItemCategory.Weapon) {
      if evt.GetSlotID() == t"AttachmentSlots.WeaponRight" {
        equipmentBB = GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_Equipment);
        if IsDefined(equipmentBB) {
          equipSlot.itemID = itemID;
          ArrayPush(paperdollEquipData.equipArea.equipSlots, equipSlot);
          paperdollEquipData.equipArea.activeIndex = 0;
          paperdollEquipData.equipArea.areaType = itemRecord.EquipArea().Type();
          paperdollEquipData.equipped = false;
          paperdollEquipData.placementSlot = EquipmentSystem.GetPlacementSlot(itemID);
          equipmentBB.SetVariant(GetAllBlackboardDefs().UI_Equipment.lastModifiedArea, ToVariant(paperdollEquipData));
        };
      };
    };
    if evt.GetSlotID() == t"AttachmentSlots.WeaponRight" || evt.GetSlotID() == t"AttachmentSlots.WeaponLeft" {
      EquipmentSystemPlayerData.UpdateArmSlot(this, evt.GetItemID(), true);
    };
    super.OnItemRemovedFromSlot(evt);
  }

  private final static func RemoveItemGameplayPackage(objectToRemoveFrom: ref<GameObject>, itemID: ItemID) -> Void {
    let i: Int32;
    let packages: array<wref<GameplayLogicPackage_Record>>;
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID));
    if IsDefined(itemRecord) {
      itemRecord.OnAttach(packages);
    };
    i = 0;
    while i < ArraySize(packages) {
      RPGManager.RemoveGLP(objectToRemoveFrom, packages[i]);
      i += 1;
    };
  }

  protected cb func OnPartRemovedFromSlotEvent(evt: ref<PartRemovedFromSlotEvent>) -> Bool {
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    if !transactionSystem.HasItemInAnySlot(this, evt.itemID) {
      PlayerPuppet.RemoveItemGameplayPackage(this, evt.removedPartID);
    };
  }

  protected cb func OnItemChangedEvent(evt: ref<ItemChangedEvent>) -> Bool {
    let assignHotkey: ref<AssignHotkeyIfEmptySlot>;
    let hotkeyRefresh: ref<HotkeyRefreshRequest>;
    let itemData: ref<gameItemData>;
    let maxAmount: Float;
    let itemType: gamedataItemType = gamedataItemType.Invalid;
    let eqSystem: wref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    if IsDefined(eqSystem) {
      itemData = evt.itemData;
      maxAmount = itemData.GetStatValueByType(gamedataStatType.Quantity);
      if RPGManager.GetItemRecord(evt.itemID).IsSingleInstance() {
        this.UpdateInventoryWeight(RPGManager.GetItemWeight(itemData) * Cast<Float>(evt.difference));
      };
      if IsDefined(itemData) {
        itemType = itemData.GetItemType();
      };
      if eqSystem.IsItemInHotkey(this, evt.itemID) {
        hotkeyRefresh = new HotkeyRefreshRequest();
        hotkeyRefresh.owner = this;
        eqSystem.QueueRequest(hotkeyRefresh);
      } else {
        if evt.currentQuantity > 0 && Hotkey.IsCompatible(EHotkey.DPAD_UP, itemType) || Hotkey.IsCompatible(EHotkey.RB, itemType) || Hotkey.IsCompatible(EHotkey.LBRB, EquipmentSystem.GetEquipAreaType(evt.itemID)) && eqSystem.IsEquippable(this, itemData) {
          if Equals(itemType, gamedataItemType.Cyb_HealingAbility) && !RPGManager.IsItemEquipped(this, evt.itemID) {
            return true;
          };
          assignHotkey = AssignHotkeyIfEmptySlot.Construct(evt.itemID, this);
          eqSystem.QueueRequest(assignHotkey);
        };
      };
      if (Equals(itemType, gamedataItemType.Gen_Junk) || Equals(itemType, gamedataItemType.Gen_Jewellery)) && evt.currentQuantity > Cast<Int32>(maxAmount) - 1 {
        ItemActionsHelper.DisassembleItem(this, evt.itemID, 1);
      };
      if (Equals(itemType, gamedataItemType.Con_Edible) || Equals(itemType, gamedataItemType.Con_LongLasting)) && evt.currentQuantity > Cast<Int32>(maxAmount) - 1 {
        if itemData.HasTag(n"Alcohol") || itemData.HasTag(n"LongLasting") {
          ItemActionsHelper.ConsumeItem(this, evt.itemID, true);
        } else {
          if itemData.HasTag(n"Drink") {
            ItemActionsHelper.DrinkItem(this, evt.itemID, true);
          } else {
            if itemData.HasTag(n"Food") {
              ItemActionsHelper.EatItem(this, evt.itemID, true);
            };
          };
        };
      };
    };
  }

  protected cb func OnPartRemovedEvent(evt: ref<PartRemovedEvent>) -> Bool {
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    if transactionSystem.HasItemInAnySlot(this, evt.itemID) {
      PlayerPuppet.RemoveItemGameplayPackage(this, evt.removedPartID);
    };
  }

  protected cb func OnPrepareForForcedVehicleCombat(evt: ref<PrepareForForcedVehicleCombat>) -> Bool {
    let equipRequest: ref<GameplayEquipRequest>;
    let equipmentSystem: ref<EquipmentSystem>;
    let weaponID: ItemID;
    if !UpperBodyTransition.HasRangedWeaponEquipped(this) {
      weaponID = EquipmentSystem.GetData(this).GetLastUsedOrFirstAvailableOneHandedRangedWeapon();
      if !ItemID.IsValid(weaponID) {
        equipmentSystem = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
        equipRequest = new GameplayEquipRequest();
        equipRequest.owner = this;
        equipRequest.itemID = ItemID.CreateQuery(t"Items.Preset_V_Unity_Cutscene");
        equipRequest.blockUpdateWeaponActiveSlots = false;
        equipRequest.forceEquipWeapon = false;
        equipmentSystem.QueueRequest(equipRequest);
      };
    };
  }

  protected cb func OnItemAddedToInventory(evt: ref<ItemAddedEvent>) -> Bool {
    let drawItemRequest: ref<DrawItemRequest>;
    let entryString: String;
    let eqSystem: wref<EquipmentSystem>;
    let gadgetItemUpdated: Bool;
    let itemData: wref<gameItemData>;
    let itemLogDataData: ItemID;
    let itemName: String;
    let itemRecord: ref<Item_Record>;
    let notification_Message: String;
    let player: ref<GameObject>;
    let price: Int32;
    let questSystem: ref<QuestsSystem>;
    let tags: array<CName>;
    let wardrobeSystem: wref<WardrobeSystem> = GameInstance.GetWardrobeSystem(this.GetGame());
    let transSystem: wref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    let itemQuality: gamedataQuality = gamedataQuality.Invalid;
    let itemCategory: gamedataItemCategory = gamedataItemCategory.Invalid;
    let itemType: gamedataItemType = gamedataItemType.Invalid;
    let weaponEvolution: gamedataWeaponEvolution = gamedataWeaponEvolution.Invalid;
    if !ItemID.IsValid(evt.itemID) {
      return false;
    };
    itemData = evt.itemData;
    questSystem = GameInstance.GetQuestsSystem(this.GetGame());
    itemRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(evt.itemID));
    itemCategory = RPGManager.GetItemCategory(evt.itemID);
    itemType = RPGManager.GetItemType(evt.itemID);
    itemLogDataData = evt.itemID;
    if IsDefined(itemData) {
      if !itemRecord.IsSingleInstance() {
        this.UpdateInventoryWeight(RPGManager.GetItemWeight(itemData), RPGManager.IsItemBroken(itemData));
      };
      this.TryScaleItemToPlayer(itemData);
      if Equals(itemCategory, gamedataItemCategory.Weapon) {
        this.SetIconicWeaponsTier(itemData);
        this.LockItemPlusOnNonIconicWeapons(itemData);
        if itemData.HasStatData(gamedataStatType.Airdropped) {
          player = GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerMainGameObject();
          StatusEffectHelper.ApplyStatusEffect(player, t"BaseStatusEffect.JustLootedAmazonIconicFromAirdrop");
        };
        if itemData.HasStatData(gamedataStatType.ItemPurchasedAtVendor) && itemData.HasTag(n"Nue_Jackie") {
          player = GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerMainGameObject();
          StatusEffectHelper.ApplyStatusEffect(player, t"BaseStatusEffect.JustPurchasedNueJackieAtBM");
        };
      };
      if Equals(itemCategory, gamedataItemCategory.Cyberware) {
        if itemData.HasStatData(gamedataStatType.Airdropped) {
          player = GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerMainGameObject();
          StatusEffectHelper.ApplyStatusEffect(player, t"BaseStatusEffect.JustLootedIconicCWFromAirdrop");
        };
        if itemData.HasStatData(gamedataStatType.IconicCWFromTreasureChestLooted) {
          player = GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerMainGameObject();
          StatusEffectHelper.ApplyStatusEffect(player, t"BaseStatusEffect.JustLootedIconicCWFromTreasureChest");
        };
      };
      itemQuality = RPGManager.GetItemDataQuality(itemData);
      if !(itemData.HasTag(n"SkipActivityLog") || itemData.HasTag(n"SkipActivityLogOnLoot") || evt.flaggedAsSilent || itemData.HasTag(n"Currency") || ItemID.HasFlag(itemData.GetID(), gameEItemIDFlag.Preview) || itemData.HasTag(n"Recipe") && !itemData.HasTag(n"IconicRecipe") || RPGManager.IsItemBroken(itemData) || GameInstance.GetWorkspotSystem(this.GetGame()).IsActorInWorkspot(this)) {
        itemName = UIItemsHelper.GetItemName(itemRecord, itemData);
        GameInstance.GetActivityLogSystem(this.GetGame()).AddLog(GetLocalizedText("UI-ScriptExports-Looted") + ": " + itemName);
      };
    };
    if IsDefined(this.m_itemLogBlackboard) {
      this.m_itemLogBlackboard.SetVariant(GetAllBlackboardDefs().UI_ItemLog.ItemLogItem, ToVariant(itemLogDataData), true);
    };
    eqSystem = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    if IsDefined(eqSystem) {
      if Equals(itemCategory, gamedataItemCategory.Weapon) && IsDefined(itemData) && itemData.HasTag(n"TakeAndEquip") {
        drawItemRequest = new DrawItemRequest();
        drawItemRequest.owner = this;
        drawItemRequest.itemID = evt.itemID;
        eqSystem.QueueRequest(drawItemRequest);
      };
    };
    if IsDefined(wardrobeSystem) && Equals(itemCategory, gamedataItemCategory.Clothing) && !wardrobeSystem.IsItemBlacklisted(evt.itemID) {
      wardrobeSystem.StoreUniqueItemIDAndMarkNew(this.GetGame(), evt.itemID);
    };
    if Equals(itemType, gamedataItemType.Con_Skillbook) {
      GameInstance.GetTelemetrySystem(this.GetGame()).LogSkillbookUsed(this, evt.itemID);
      ItemActionsHelper.LearnItem(this, evt.itemID, true);
      if itemData.HasTag(n"LargeSkillbook") {
        notification_Message = GetLocalizedText("LocKey#46534") + "\\n";
        if itemData.HasTag(n"Body") {
          notification_Message += GetLocalizedText("LocKey#93274");
        } else {
          if itemData.HasTag(n"Reflex") {
            notification_Message += GetLocalizedText("LocKey#93275");
          } else {
            if itemData.HasTag(n"Intelligence") {
              notification_Message += GetLocalizedText("LocKey#93278");
            } else {
              if itemData.HasTag(n"Cool") {
                notification_Message += GetLocalizedText("LocKey#93280");
              } else {
                if itemData.HasTag(n"Tech") {
                  notification_Message += GetLocalizedText("LocKey#51170");
                } else {
                  if itemData.HasTag(n"PerkSkillbook") {
                    notification_Message += GetLocalizedText("LocKey#2694");
                  };
                };
              };
            };
          };
        };
      } else {
        if itemData.HasTag(n"CWCapacity_Shard") {
          if GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.Humanity) < TDB.GetFloat(t"BaseStats.Humanity.max") {
            notification_Message = GetLocalizedText("LocKey#46534") + "\\n" + GetLocalizedText(LocKeyToString(TweakDBInterface.GetItemRecord(ItemID.GetTDBID(evt.itemID)).LocalizedDescription()));
          };
        } else {
          notification_Message = GetLocalizedText("LocKey#46534") + "\\n" + GetLocalizedText(LocKeyToString(TweakDBInterface.GetItemRecord(ItemID.GetTDBID(evt.itemID)).LocalizedDescription()));
        };
      };
      this.SetWarningMessage(notification_Message, SimpleMessageType.Neutral);
      if itemData.HasStatData(gamedataStatType.ItemPurchasedAtVendor) && !itemData.HasTag(n"LargeSkillbook") {
        player = GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerMainGameObject();
        if itemData.HasTag(n"SkillbookReward_Body") {
          StatusEffectHelper.ApplyStatusEffect(player, t"BaseStatusEffect.JustPurchasedBodySkillBookAtShankVendor");
        } else {
          if itemData.HasTag(n"SkillbookReward_Ref") {
            StatusEffectHelper.ApplyStatusEffect(player, t"BaseStatusEffect.JustPurchasedRefSkillBookAtShankVendor");
          } else {
            if itemData.HasTag(n"SkillbookReward_Int") {
              StatusEffectHelper.ApplyStatusEffect(player, t"BaseStatusEffect.JustPurchasedIntSkillBookAtShankVendor");
            } else {
              if itemData.HasTag(n"SkillbookReward_Tech") {
                StatusEffectHelper.ApplyStatusEffect(player, t"BaseStatusEffect.JustPurchasedTechSkillBookAtShankVendor");
              } else {
                if itemData.HasTag(n"SkillbookReward_Cool") {
                  StatusEffectHelper.ApplyStatusEffect(player, t"BaseStatusEffect.JustPurchasedCoolSkillBookAtShankVendor");
                };
              };
            };
          };
        };
      };
    } else {
      if Equals(itemType, gamedataItemType.Con_Edible) {
        if itemData.HasTag(n"PermanentFood") {
          ItemActionsHelper.ConsumeItem(this, evt.itemID, true);
          if itemData.HasTag(n"PermanentStaminaFood") {
            Cast<Int32>(this.GetPermanentFoodBonus(gamedataStatType.StaminaRegenBonusBlackmarket));
            notification_Message = GetLocalizedText("LocKey#93105") + "\\n" + GetLocalizedText("LocKey#93723");
          } else {
            if itemData.HasTag(n"PermanentMemoryFood") {
              Cast<Int32>(this.GetPermanentFoodBonus(gamedataStatType.MemoryRegenBonusBlackmarket));
              notification_Message = GetLocalizedText("LocKey#93106") + "\\n" + GetLocalizedText("LocKey#93724");
            } else {
              if itemData.HasTag(n"PermanentHealthFood") {
                Cast<Int32>(this.GetPermanentFoodBonus(gamedataStatType.HealthBonusBlackmarket));
                notification_Message = GetLocalizedText("LocKey#93104") + "\\n" + GetLocalizedText("LocKey#93725");
              };
            };
          };
          this.SetWarningMessage(notification_Message, SimpleMessageType.Neutral);
        };
      } else {
        if Equals(itemType, gamedataItemType.Gen_Readable) {
          transSystem.RemoveItem(this, evt.itemID, 1);
          entryString = ReadAction.GetJournalEntryFromAction(ItemActionsHelper.GetReadAction(evt.itemID).GetID());
          GameInstance.GetJournalManager(this.GetGame()).ChangeEntryState(entryString, "gameJournalOnscreen", gameJournalEntryState.Active, JournalNotifyOption.Notify);
        } else {
          if Equals(itemType, gamedataItemType.Gen_Junk) && GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.CanAutomaticallyDisassembleJunk) > 0.00 {
            ItemActionsHelper.DisassembleItem(this, evt.itemID, transSystem.GetItemQuantity(this, evt.itemID));
          } else {
            if Equals(itemType, gamedataItemType.Gad_Grenade) || Equals(itemType, gamedataItemType.Con_Inhaler) || Equals(itemType, gamedataItemType.Con_Injector) {
              tags = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemLogDataData)).Tags();
              gadgetItemUpdated = ConsumablesChargesHelper.LeaveTheBestQualityConsumable(this.GetGame(), ConsumablesChargesHelper.GetConsumableTag(tags));
              if gadgetItemUpdated {
                ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), ItemID.GetTDBID(evt.itemID));
                if Equals(itemType, gamedataItemType.Gad_Grenade) {
                  notification_Message = GetLocalizedText("LocKey#95690");
                } else {
                  notification_Message = GetLocalizedText("LocKey#95689");
                };
                this.SetWarningMessage(notification_Message, SimpleMessageType.Neutral);
              };
            } else {
              if Equals(itemType, gamedataItemType.Con_Ammo) {
                GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_EquipmentData).SetBool(GetAllBlackboardDefs().UI_EquipmentData.ammoLooted, true);
                GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_EquipmentData).SignalBool(GetAllBlackboardDefs().UI_EquipmentData.ammoLooted);
              } else {
                if Equals(itemType, gamedataItemType.Gen_Keycard) {
                  this.GotKeycardNotification();
                } else {
                  if Equals(itemType, gamedataItemType.Gen_MoneyShard) {
                    price = RPGManager.CalculateSellPrice(this.GetGame(), this, evt.itemID) * evt.itemData.GetQuantity();
                    transSystem.GiveMoney(this, price, n"money");
                    transSystem.RemoveItem(this, evt.itemID, transSystem.GetItemQuantity(this, evt.itemID));
                  };
                };
              };
            };
          };
        };
      };
    };
    if RPGManager.IsItemBroken(evt.itemData) && RPGManager.IsItemWeapon(evt.itemID) {
      ItemActionsHelper.DisassembleItem(this, evt.itemID, transSystem.GetItemQuantity(this, evt.itemID));
    };
    if questSystem.GetFact(n"disable_tutorials") == 0 && questSystem.GetFact(n"q001_show_sts_tut") > 0 && !RPGManager.IsItemBroken(evt.itemData) {
      weaponEvolution = RPGManager.GetWeaponEvolution(evt.itemID);
      if Equals(weaponEvolution, gamedataWeaponEvolution.Smart) && questSystem.GetFact(n"smart_weapon_tutorial") == 0 {
        questSystem.SetFact(n"smart_weapon_tutorial", 1);
      } else {
        if Equals(weaponEvolution, gamedataWeaponEvolution.Tech) && RPGManager.IsTechPierceEnabled(this.GetGame(), this, evt.itemID) && questSystem.GetFact(n"tech_weapon_tutorial") == 0 {
          questSystem.SetFact(n"tech_weapon_tutorial", 1);
        } else {
          if Equals(weaponEvolution, gamedataWeaponEvolution.Power) && RPGManager.IsRicochetChanceEnabled(this.GetGame(), this, evt.itemID) && questSystem.GetFact(n"power_weapon_tutorial") == 0 && evt.itemID != ItemID.CreateQuery(t"Items.Preset_V_Unity_Cutscene") && evt.itemID != ItemID.CreateQuery(t"Items.Preset_V_Unity") {
            questSystem.SetFact(n"power_weapon_tutorial", 1);
          };
        };
      };
      if Equals(itemCategory, gamedataItemCategory.Gadget) && questSystem.GetFact(n"grenade_inventory_tutorial") == 0 {
        questSystem.SetFact(n"grenade_inventory_tutorial", 1);
      } else {
        if Equals(itemCategory, gamedataItemCategory.Cyberware) && questSystem.GetFact(n"cyberware_inventory_tutorial") == 0 {
          questSystem.SetFact(n"cyberware_inventory_tutorial", 1);
        };
      };
      if (Equals(itemType, gamedataItemType.Con_Inhaler) || Equals(itemType, gamedataItemType.Con_Injector)) && questSystem.GetFact(n"consumable_inventory_tutorial") == 0 {
        questSystem.SetFact(n"consumable_inventory_tutorial", 1);
      };
      if RPGManager.IsItemIconic(evt.itemData) && Equals(itemCategory, gamedataItemCategory.Weapon) && questSystem.GetFact(n"iconic_item_tutorial") == 0 {
        questSystem.SetFact(n"iconic_item_tutorial", 1);
      };
    };
    if questSystem.GetFact(n"initial_gadget_picked") == 0 {
      if Equals(RPGManager.GetItemCategory(evt.itemID), gamedataItemCategory.Gadget) {
        questSystem.SetFact(n"initial_gadget_picked", 1);
      };
    };
    RPGManager.ProcessOnLootedPackages(this, evt.itemID);
    if Equals(itemQuality, gamedataQuality.Legendary) || Equals(itemQuality, gamedataQuality.Iconic) {
      GameInstance.GetAutoSaveSystem(this.GetGame()).RequestCheckpoint();
    };
  }

  protected cb func OnRefreshItemPlayerScalingEvent(evt: ref<RefreshItemPlayerScalingEvent>) -> Bool {
    let i: Int32;
    let itemList: array<wref<gameItemData>>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    transactionSystem.GetItemList(this, itemList);
    i = 0;
    while i < ArraySize(itemList) {
      if itemList[i].HasTag(n"RescalePL") {
        this.TryScaleItemToPlayer(itemList[i]);
      };
      i += 1;
    };
  }

  public final func TryScaleItemToPlayer(itemData: ref<gameItemData>) -> Void {
    let scalingBlocker: ref<gameStatModifierData>;
    let scalingMod: ref<gameStatModifierData>;
    if TweakDBInterface.GetBool(ItemID.GetTDBID(itemData.GetID()) + t".scaleToPlayer", false) && itemData.GetStatValueByType(gamedataStatType.ScalingBlocked) < 1.00 && (itemData.GetStatValueByType(gamedataStatType.PowerLevel) <= 1.00 || itemData.HasTag(n"DLCStashItem") || itemData.HasTag(n"AutoScalingItem") || itemData.HasTag(n"StashScaling_Iconic") || itemData.HasTag(n"Left_Hand_Retrofix")) {
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.LootLevel, true);
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.PowerLevel, true);
      scalingMod = RPGManager.CreateStatModifier(gamedataStatType.LootLevel, gameStatModifierType.Additive, GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.PowerLevel));
      scalingBlocker = RPGManager.CreateStatModifier(gamedataStatType.ScalingBlocked, gameStatModifierType.Additive, 1.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), scalingMod);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), scalingBlocker);
    };
  }

  protected cb func OnRestoreCybWeaponQualitiesEvent(evt: ref<RestoreCybWeaponQualitiesEvent>) -> Bool {
    let baseQuality: Float;
    let i: Int32;
    let itemData: ref<gameItemData>;
    let itemList: array<wref<gameItemData>>;
    let itemRecord: ref<Item_Record>;
    let itemType: gamedataItemType;
    let quality: gamedataQuality;
    let qualityMod: ref<gameStatModifierData>;
    let qualityPlus: Float;
    let qualityStat: Float;
    let tierPlusMod: ref<gameStatModifierData>;
    let tierPlusStat: Float;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    transactionSystem.GetItemList(this, itemList);
    i = 0;
    while i < ArraySize(itemList) {
      itemData = itemList[i];
      itemType = itemData.GetItemType();
      if NotEquals(itemType, gamedataItemType.Cyb_Launcher) && NotEquals(itemType, gamedataItemType.Cyb_MantisBlades) && NotEquals(itemType, gamedataItemType.Cyb_StrongArms) && NotEquals(itemType, gamedataItemType.Cyb_NanoWires) {
      } else {
        itemRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemData.GetID()));
        quality = itemRecord.QualityHandle().Type();
        switch quality {
          case gamedataQuality.Common:
            baseQuality = 0.00;
            qualityPlus = 0.00;
            break;
          case gamedataQuality.CommonPlus:
            baseQuality = 0.00;
            qualityPlus = 1.00;
            break;
          case gamedataQuality.Uncommon:
            baseQuality = 1.00;
            qualityPlus = 0.00;
            break;
          case gamedataQuality.UncommonPlus:
            baseQuality = 1.00;
            qualityPlus = 1.00;
            break;
          case gamedataQuality.Rare:
            baseQuality = 2.00;
            qualityPlus = 0.00;
            break;
          case gamedataQuality.RarePlus:
            baseQuality = 2.00;
            qualityPlus = 1.00;
            break;
          case gamedataQuality.Epic:
            baseQuality = 3.00;
            qualityPlus = 0.00;
            break;
          case gamedataQuality.EpicPlus:
            baseQuality = 3.00;
            qualityPlus = 1.00;
            break;
          case gamedataQuality.Legendary:
            baseQuality = 4.00;
            qualityPlus = 0.00;
            break;
          case gamedataQuality.LegendaryPlus:
            baseQuality = 4.00;
            qualityPlus = 1.00;
            break;
          case gamedataQuality.LegendaryPlusPlus:
            baseQuality = 4.00;
            qualityPlus = 2.00;
            break;
          default:
        };
        qualityStat = itemData.GetStatValueByType(gamedataStatType.Quality);
        tierPlusStat = itemData.GetStatValueByType(gamedataStatType.IsItemPlus);
        if baseQuality * 2.00 + qualityPlus != qualityStat * 2.00 + tierPlusStat {
          GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.Quality, true);
          GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.IsItemPlus, true);
          qualityMod = RPGManager.CreateStatModifier(gamedataStatType.Quality, gameStatModifierType.Additive, baseQuality);
          tierPlusMod = RPGManager.CreateStatModifier(gamedataStatType.IsItemPlus, gameStatModifierType.Additive, qualityPlus);
          if GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), qualityMod) && GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), tierPlusMod) {
            qualityStat = itemData.GetStatValueByType(gamedataStatType.Quality);
            tierPlusStat = itemData.GetStatValueByType(gamedataStatType.IsItemPlus);
          };
        };
      };
      i += 1;
    };
  }

  protected cb func OnRescaleNonIconicWeaponsEvent(evt: ref<RescaleNonIconicWeaponsEvent>) -> Bool {
    let i: Int32;
    let itemList: array<wref<gameItemData>>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    transactionSystem.GetItemList(this, itemList);
    i = 0;
    while i < ArraySize(itemList) {
      if !RPGManager.IsItemIconic(itemList[i]) && RPGManager.IsItemWeapon(itemList[i].GetID()) {
        this.RetroRescaleNonIconicWeapons(itemList[i]);
      };
      i += 1;
    };
  }

  public final func RetroRescaleNonIconicWeapons(itemData: ref<gameItemData>) -> Void {
    let isIconic: Float;
    let isItemCrafted: Float;
    let isItemPurchased: Float;
    let isScalingBlocked: Float;
    let lootLevel: Float;
    let lootLevelMod: ref<gameStatModifierData>;
    let playerLevel: Float;
    let playerMaxQualtity: Float;
    let qualityMod: ref<gameStatModifierData>;
    let randomCurveMod: ref<gameStatModifierData>;
    let randomizerMod: ref<gameStatModifierData>;
    let scalingBlocker: ref<gameStatModifierData>;
    let weaponQuality: Float;
    let itemType: gamedataItemType = itemData.GetItemType();
    if Equals(itemType, gamedataItemType.Cyb_Launcher) || Equals(itemType, gamedataItemType.Cyb_MantisBlades) || Equals(itemType, gamedataItemType.Cyb_StrongArms) || Equals(itemType, gamedataItemType.Cyb_NanoWires) {
      return;
    };
    playerLevel = GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.Level);
    playerMaxQualtity = GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.MaxQuality);
    weaponQuality = itemData.GetStatValueByType(gamedataStatType.Quality);
    isScalingBlocked = itemData.GetStatValueByType(gamedataStatType.ScalingBlocked);
    lootLevel = itemData.GetStatValueByType(gamedataStatType.LootLevel);
    isItemCrafted = itemData.GetStatValueByType(gamedataStatType.IsItemCrafted);
    isItemPurchased = itemData.GetStatValueByType(gamedataStatType.ItemPurchasedAtVendor);
    isIconic = itemData.GetStatValueByType(gamedataStatType.IsItemIconic);
    if isScalingBlocked < 1.00 && (lootLevel < 1.00 || lootLevel > playerLevel) && isItemCrafted < 1.00 && isItemPurchased < 1.00 && isIconic < 1.00 {
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.PowerLevel, true);
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.LootLevel, true);
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.RandomCurveInput, true);
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.Quality, true);
      lootLevelMod = RPGManager.CreateStatModifier(gamedataStatType.LootLevel, gameStatModifierType.Additive, playerLevel);
      scalingBlocker = RPGManager.CreateStatModifier(gamedataStatType.ScalingBlocked, gameStatModifierType.Additive, 1.00);
      randomizerMod = RPGManager.CreateStatModifier(gamedataStatType.NPCWeaponDropRandomizer, gameStatModifierType.Additive, RandRangeF(-0.80, 0.20));
      randomCurveMod = RPGManager.CreateStatModifier(gamedataStatType.RandomCurveInput, gameStatModifierType.Additive, RandRangeF(0.01, 0.99));
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), lootLevelMod);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), scalingBlocker);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), randomizerMod);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), randomCurveMod);
      lootLevelMod = RPGManager.CreateCombinedStatModifier(gamedataStatType.LootLevel, gameStatModifierType.AdditiveMultiplier, gamedataStatType.NPCWeaponDropRandomizer, gameCombinedStatOperation.Multiplication, 1.00, gameStatObjectsRelation.Self);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), lootLevelMod);
      randomCurveMod = RPGManager.CreateStatModifierUsingCurve(gamedataStatType.RandomCurveInput, gameStatModifierType.Additive, gamedataStatType.LootLevel, n"quality_curves", n"level_to_random_range_mult_new");
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), randomCurveMod);
      qualityMod = RPGManager.CreateStatModifierUsingCurve(gamedataStatType.Quality, gameStatModifierType.Additive, gamedataStatType.RandomCurveInput, n"random_distributions", n"quality_distribution_new");
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), qualityMod);
    } else {
      if isScalingBlocked < 1.00 && isItemCrafted >= 1.00 && isIconic < 1.00 && weaponQuality - playerMaxQualtity >= 2.00 {
        GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.Quality, true);
        qualityMod = RPGManager.CreateStatModifier(gamedataStatType.Quality, gameStatModifierType.Additive, playerMaxQualtity);
        GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), qualityMod);
        scalingBlocker = RPGManager.CreateStatModifier(gamedataStatType.ScalingBlocked, gameStatModifierType.Additive, 1.00);
        GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), scalingBlocker);
      };
    };
  }

  protected cb func OnConsumablesChargesRework(evt: ref<ConsumablesChargesReworkEvent>) -> Bool {
    let chargedConsumables: array<wref<gameItemData>>;
    let i: Int32;
    let playerLevel: Float = GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.Level);
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    transactionSystem.GetItemListByTag(this, n"ChargedConsumable", chargedConsumables);
    i = 0;
    while i < ArraySize(chargedConsumables) {
      if chargedConsumables[i].HasTag(n"Ozob") {
        ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeOzobsNose");
      } else {
        transactionSystem.RemoveItem(this, chargedConsumables[i].GetID(), chargedConsumables[i].GetQuantity());
      };
      i += 1;
    };
    if playerLevel <= 9.00 {
      transactionSystem.GiveItemByTDBID(this, t"Items.FirstAidWhiffV0", 1);
      transactionSystem.GiveItemByTDBID(this, t"Items.BonesMcCoy70V0", 1);
      transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeFragRegular", 1);
      transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeFlashRegular", 1);
      transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeEMPRegular", 1);
    } else {
      if playerLevel > 9.00 && playerLevel <= 17.00 {
        transactionSystem.GiveItemByTDBID(this, t"Items.FirstAidWhiffVUncommon", 1);
        transactionSystem.GiveItemByTDBID(this, t"Items.BonesMcCoy70VUncommon", 1);
        transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeFragUncommon", 1);
        transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeFlashUncommon", 1);
        transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeEMPUncommon", 1);
        transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeSmokeRegular", 1);
        transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeIncendiaryRegular", 1);
        transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeReconRegular", 1);
      } else {
        if playerLevel > 17.00 && playerLevel <= 25.00 {
          transactionSystem.GiveItemByTDBID(this, t"Items.FirstAidWhiffV1", 1);
          transactionSystem.GiveItemByTDBID(this, t"Items.BonesMcCoy70V1", 1);
          transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeFragSticky", 1);
          transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeFlashHoming", 1);
          transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeEMPRare", 1);
          transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeSmokeRare", 1);
          transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeBiohazardRegular", 1);
          transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeIncendiaryRare", 1);
          transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeReconRare", 1);
        } else {
          if playerLevel > 25.00 && playerLevel <= 33.00 {
            transactionSystem.GiveItemByTDBID(this, t"Items.FirstAidWhiffVEpic", 1);
            transactionSystem.GiveItemByTDBID(this, t"Items.BonesMcCoy70VEpic", 1);
            transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeFragEpic", 1);
            transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeFlashEpic", 1);
            transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeEMPSticky", 1);
            transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeSmokeEpic", 1);
            transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeBiohazardHoming", 1);
            transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeIncendiarySticky", 1);
            transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeReconSticky", 1);
          } else {
            if playerLevel > 33.00 {
              transactionSystem.GiveItemByTDBID(this, t"Items.FirstAidWhiffV2", 1);
              transactionSystem.GiveItemByTDBID(this, t"Items.BonesMcCoy70V2", 1);
              transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeFragLegendary", 1);
              transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeFlashLegendary", 1);
              transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeEMPLegendary", 1);
              transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeSmokeLegendary", 1);
              transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeBiohazardLegendary", 1);
              transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeIncendiaryLegendary", 1);
              transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeReconLegendary", 1);
              transactionSystem.GiveItemByTDBID(this, t"Items.GrenadeCuttingRegular", 1);
            };
          };
        };
      };
    };
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeEMPRegular");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeFragRegular");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeFlashRegular");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.FirstAidWhiffV0");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.BonesMcCoy70V0");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeReconRegular");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeIncendiaryRegular");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.FirstAidWhiffV1");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.BonesMcCoy70V1");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.FirstAidWhiffV1");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.BonesMcCoy70V1");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.RecipeGrenadeFlashHoming");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeFragSticky");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeBiohazardRegular");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeIncendiaryRare");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeFlashHoming");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeEMPSticky");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeBiohazardHoming");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeIncendiarySticky");
    ConsumablesChargesHelper.HideConsumableRecipe(this.GetGame(), t"Items.GrenadeReconSticky");
  }

  protected cb func OnBlockAndCompensateScalingEvent(evt: ref<BlockAndCompensateScalingEvent>) -> Bool {
    let i: Int32;
    let itemList: array<wref<gameItemData>>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    transactionSystem.GetItemList(this, itemList);
    i = 0;
    while i < ArraySize(itemList) {
      if itemList[i].HasTag(n"DLCStashItem") || itemList[i].HasTag(n"AutoScalingItem") {
        this.BlockScaling(itemList[i]);
        this.CompensateExceedScaling(itemList[i]);
      };
      i += 1;
    };
  }

  public final func BlockScaling(itemData: ref<gameItemData>) -> Void {
    let scalingBlocker: ref<gameStatModifierData>;
    if TweakDBInterface.GetBool(ItemID.GetTDBID(itemData.GetID()) + t".scaleToPlayer", false) {
      scalingBlocker = RPGManager.CreateStatModifier(gamedataStatType.ScalingBlocked, gameStatModifierType.Additive, 1.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), scalingBlocker);
    };
  }

  public final func CompensateExceedScaling(itemData: ref<gameItemData>) -> Void {
    let scalingCompensateIL: ref<gameStatModifierData>;
    let scalingCompensateL: ref<gameStatModifierData>;
    let playerLevel: Float = GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.Level);
    let lvlDiff: Float = itemData.GetStatValueByType(gamedataStatType.Level) - playerLevel;
    let upgradeCount: Float = itemData.GetStatValueByType(gamedataStatType.WasItemUpgraded);
    if TweakDBInterface.GetBool(ItemID.GetTDBID(itemData.GetID()) + t".scaleToPlayer", false) && lvlDiff > 0.00 {
      scalingCompensateL = RPGManager.CreateStatModifier(gamedataStatType.Level, gameStatModifierType.Additive, upgradeCount * -1.00);
      scalingCompensateIL = RPGManager.CreateStatModifier(gamedataStatType.ItemLevel, gameStatModifierType.Additive, upgradeCount * -10.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), scalingCompensateL);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), scalingCompensateIL);
    };
  }

  public final func SetIconicWeaponsTier(itemData: ref<gameItemData>) -> Void {
    let plusToUpgradeMod: ref<gameStatModifierData>;
    let qualityToUpgradeMod: ref<gameStatModifierData>;
    let upgradeToPlusMod: ref<gameStatModifierData>;
    let upgradeToQualityMod: ref<gameStatModifierData>;
    let iconicCheck: Bool = itemData.HasTag(n"IconicWeapon");
    if iconicCheck && itemData.GetStatValueByType(gamedataStatType.WasItemUpgraded) < 1.00 {
      qualityToUpgradeMod = RPGManager.CreateStatModifier(gamedataStatType.WasItemUpgraded, gameStatModifierType.Additive, itemData.GetStatValueByType(gamedataStatType.Quality) * 2.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), qualityToUpgradeMod);
      upgradeToQualityMod = RPGManager.CreateStatModifier(gamedataStatType.Quality, gameStatModifierType.Additive, itemData.GetStatValueByType(gamedataStatType.WasItemUpgraded) * -0.50);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), upgradeToQualityMod);
      plusToUpgradeMod = RPGManager.CreateStatModifier(gamedataStatType.WasItemUpgraded, gameStatModifierType.Additive, itemData.GetStatValueByType(gamedataStatType.IsItemPlus));
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), plusToUpgradeMod);
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.IsItemPlus, true);
      upgradeToPlusMod = RPGManager.CreateStatModifierUsingCurve(gamedataStatType.IsItemPlus, gameStatModifierType.Additive, gamedataStatType.WasItemUpgraded, n"quality_curves", n"iconic_upgrades_amount_to_plus");
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), upgradeToPlusMod);
    };
  }

  public final func LockItemPlusOnNonIconicWeapons(itemData: ref<gameItemData>) -> Void {
    let plusMod: ref<gameStatModifierData>;
    let iconicCheck: Bool = itemData.HasTag(n"IconicWeapon");
    if !iconicCheck {
      plusMod = RPGManager.CreateStatModifier(gamedataStatType.IsItemPlus, gameStatModifierType.Additive, itemData.GetStatValueByType(gamedataStatType.IsItemPlus));
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.IsItemPlus, true);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), plusMod);
    };
  }

  protected cb func OnIconicsReworkCompensateEvent(evt: ref<IconicsReworkCompensateEvent>) -> Bool {
    let i: Int32;
    let itemList: array<wref<gameItemData>>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    transactionSystem.GetItemList(this, itemList);
    i = 0;
    while i < ArraySize(itemList) {
      if itemList[i].HasTag(n"IconicWeapon") {
        this.RescaleOwnedIconicsToPlayerLevel(itemList[i]);
      };
      i += 1;
    };
  }

  public final func RescaleOwnedIconicsToPlayerLevel(itemData: ref<gameItemData>) -> Void {
    let itemLeveltier5LimiterMod: ref<gameStatModifierData>;
    let plusToUpgradeMod: ref<gameStatModifierData>;
    let qualityToUpgradeMod: ref<gameStatModifierData>;
    let upgradeToPlusMod: ref<gameStatModifierData>;
    let upgradeToQualityMod: ref<gameStatModifierData>;
    let playerLevel: Float = GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.Level);
    let lootLevelMod: ref<gameStatModifierData> = RPGManager.CreateStatModifier(gamedataStatType.LootLevel, gameStatModifierType.Additive, playerLevel);
    let zeroUpgradeMod: ref<gameStatModifierData> = RPGManager.CreateStatModifier(gamedataStatType.WasItemUpgraded, gameStatModifierType.Additive, itemData.GetStatValueByType(gamedataStatType.WasItemUpgraded) * -1.00);
    GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.PowerLevel, true);
    GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.LootLevel, true);
    GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.ItemLevel, true);
    GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), zeroUpgradeMod);
    GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), lootLevelMod);
    itemLeveltier5LimiterMod = RPGManager.CreateStatModifierUsingCurve(gamedataStatType.ItemLevel, gameStatModifierType.Additive, gamedataStatType.LootLevel, n"quality_curves", n"iconic_weapon_level_tier5_limiter_retrofix");
    GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), itemLeveltier5LimiterMod);
    lootLevelMod = RPGManager.CreateStatModifier(gamedataStatType.LootLevel, gameStatModifierType.Additive, itemData.GetStatValueByType(gamedataStatType.ItemLevel));
    GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), lootLevelMod);
    qualityToUpgradeMod = RPGManager.CreateStatModifier(gamedataStatType.WasItemUpgraded, gameStatModifierType.Additive, itemData.GetStatValueByType(gamedataStatType.Quality) * 2.00);
    GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), qualityToUpgradeMod);
    upgradeToQualityMod = RPGManager.CreateStatModifier(gamedataStatType.Quality, gameStatModifierType.Additive, itemData.GetStatValueByType(gamedataStatType.WasItemUpgraded) * -0.50);
    GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), upgradeToQualityMod);
    plusToUpgradeMod = RPGManager.CreateStatModifier(gamedataStatType.WasItemUpgraded, gameStatModifierType.Additive, itemData.GetStatValueByType(gamedataStatType.IsItemPlus));
    GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), plusToUpgradeMod);
    GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.IsItemPlus, true);
    upgradeToPlusMod = RPGManager.CreateStatModifierUsingCurve(gamedataStatType.IsItemPlus, gameStatModifierType.Additive, gamedataStatType.WasItemUpgraded, n"quality_curves", n"iconic_upgrades_amount_to_plus");
    GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), upgradeToPlusMod);
  }

  protected cb func OnRasetsuToPlayerScalingEvent(evt: ref<RasetsuToPlayerScalingEvent>) -> Bool {
    let i: Int32;
    let itemList: array<wref<gameItemData>>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    transactionSystem.GetItemList(this, itemList);
    i = 0;
    while i < ArraySize(itemList) {
      if itemList[i].HasTag(n"Rasetsu") {
        this.ScaleRasetsuToProperTier(itemList[i]);
      };
      i += 1;
    };
  }

  public final func ScaleRasetsuToProperTier(itemData: ref<gameItemData>) -> Void {
    let scalingBlocker: ref<gameStatModifierData>;
    let scalingMod: ref<gameStatModifierData>;
    if TweakDBInterface.GetBool(ItemID.GetTDBID(itemData.GetID()) + t".scaleToPlayer", false) && itemData.GetStatValueByType(gamedataStatType.ScalingBlocked) < 1.00 && (itemData.GetStatValueByType(gamedataStatType.PowerLevel) <= 1.00 || itemData.HasTag(n"DLCStashItem") || itemData.HasTag(n"AutoScalingItem") || itemData.HasTag(n"StashScaling_Iconic") || itemData.HasTag(n"Left_Hand_Retrofix")) {
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.LootLevel, true);
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.PowerLevel, true);
      scalingMod = RPGManager.CreateStatModifierUsingCurve(gamedataStatType.LootLevel, gameStatModifierType.Additive, gamedataStatType.WasItemUpgraded, n"quality_curves", n"iconic_upgrades_amount_to_level");
      scalingBlocker = RPGManager.CreateStatModifier(gamedataStatType.ScalingBlocked, gameStatModifierType.Additive, 1.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), scalingMod);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), scalingBlocker);
      scalingMod = RPGManager.CreateStatModifier(gamedataStatType.LootLevel, gameStatModifierType.Additive, itemData.GetStatValueByType(gamedataStatType.LootLevel));
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.LootLevel, true);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), scalingMod);
    };
  }

  protected cb func OnUnifyIconicsUpgradeCountWithEffectiveTierEvent(evt: ref<UnifyIconicsUpgradeCountWithEffectiveTierEvent>) -> Bool {
    let i: Int32;
    let itemList: array<wref<gameItemData>>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    transactionSystem.GetItemList(this, itemList);
    i = 0;
    while i < ArraySize(itemList) {
      if itemList[i].HasTag(n"IconicWeapon") {
        this.UnifyIconicWeaponsUpgradesCountWithEffectiveTier(itemList[i]);
      };
      i += 1;
    };
  }

  public final func UnifyIconicWeaponsUpgradesCountWithEffectiveTier(itemData: ref<gameItemData>) -> Void {
    let plusMod: ref<gameStatModifierData>;
    let upgradeMod: ref<gameStatModifierData>;
    let effectiveTier: Float = itemData.GetStatValueByType(gamedataStatType.EffectiveTier);
    let upgradeCount: Float = itemData.GetStatValueByType(gamedataStatType.WasItemUpgraded);
    if upgradeCount < effectiveTier {
      upgradeMod = RPGManager.CreateStatModifier(gamedataStatType.WasItemUpgraded, gameStatModifierType.Additive, itemData.GetStatValueByType(gamedataStatType.EffectiveTier));
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.WasItemUpgraded, true);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), upgradeMod);
    };
    GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.IsItemPlus, true);
    plusMod = RPGManager.CreateStatModifierUsingCurve(gamedataStatType.IsItemPlus, gameStatModifierType.Additive, gamedataStatType.WasItemUpgraded, n"quality_curves", n"iconic_upgrades_amount_to_plus");
    GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(itemData.GetStatsObjectID(), plusMod);
  }

  private final func GetItemCapacityRequirement(itemID: ItemID) -> Float {
    let itemModParams: ItemModParams;
    itemModParams.quantity = 1;
    itemModParams.itemID = itemID;
    let itemData: ref<gameItemData> = Inventory.CreateItemData(itemModParams, this);
    let reqs: array<SItemStackRequirementData> = RPGManager.GetEquipRequirements(this, itemData);
    let i: Int32 = ArraySize(reqs) - 1;
    while i >= 0 {
      if Equals(reqs[i].statType, gamedataStatType.HumanityAvailable) {
        return reqs[i].requiredValue;
      };
      i -= 1;
    };
    return 0.00;
  }

  protected cb func OnRetrofixQuickhacksEvent(evt: ref<RetrofixQuickhacksEvent>) -> Bool {
    let counterPartID: TweakDBID;
    let counterPartRecord: ref<Item_Record>;
    let i: Int32;
    let itemID: ItemID;
    let itemList: array<wref<gameItemData>>;
    let qhRecord: ref<Item_Record>;
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    ts.GetItemListByTag(this, n"SoftwareShard", itemList);
    i = 0;
    while i < ArraySize(itemList) {
      itemID = itemList[i].GetID();
      qhRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID));
      if qhRecord.Deprecated() {
        counterPartRecord = qhRecord.CounterpartHandle();
        if IsDefined(counterPartRecord) {
          counterPartID = counterPartRecord.GetRecordID();
          if !TDBID.IsValid(counterPartID) {
          } else {
            ts.GiveItemByTDBID(this, counterPartID, itemList[i].GetQuantity());
            ts.RemoveItem(this, itemID, itemList[i].GetQuantity());
          };
        };
        ts.RemoveItem(this, itemID, itemList[i].GetQuantity());
      };
      i += 1;
    };
  }

  private final func GetGlitchedEquippedCyberware(out items: [ItemID]) -> Void {
    let currentItem: ItemID;
    let j: Int32;
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    let equipmentSystemPlayerData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(this);
    let equipmentAreas: array<gamedataEquipmentArea> = equipmentSystemPlayerData.GetAllCyberwareEquipmentAreas();
    let i: Int32 = 0;
    while i < ArraySize(equipmentAreas) {
      j = 0;
      while j < equipmentSystemPlayerData.GetNumberOfSlots(equipmentAreas[i]) {
        currentItem = this.GetEquippedItemIdInArea(equipmentAreas[i], j);
        if ItemID.IsValid(currentItem) && !ts.HasItem(this, currentItem) {
          ArrayPush(items, currentItem);
        };
        j += 1;
      };
      i += 1;
    };
  }

  private final func PrioritizeCyberwareOnRetrofix(itemID: ItemID, equipmentArea: gamedataEquipmentArea, out p1: [ItemID], out p2: [ItemID], out p3: [ItemID], beforeVikVisit: Bool) -> Bool {
    if beforeVikVisit && NotEquals(equipmentArea, gamedataEquipmentArea.SystemReplacementCW) {
      return false;
    };
    if Equals(equipmentArea, gamedataEquipmentArea.SystemReplacementCW) || Equals(equipmentArea, gamedataEquipmentArea.ArmsCW) || Equals(equipmentArea, gamedataEquipmentArea.HandsCW) {
      ArrayPush(p1, itemID);
    } else {
      if Equals(equipmentArea, gamedataEquipmentArea.LegsCW) {
        ArrayPush(p2, itemID);
      } else {
        ArrayPush(p3, itemID);
      };
    };
    return true;
  }

  private final func RemoveCyberwareParts(item: ItemID) -> [ItemID] {
    let convertedItem: ItemID;
    let counterPartID: TweakDBID;
    let counterPartRecord: ref<Item_Record>;
    let installedPartId: TweakDBID;
    let qhRecord: ref<Item_Record>;
    let removedItems: array<ItemID>;
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    let itemParts: array<SPartSlots> = ItemModificationSystem.GetAllSlots(this, item);
    let i: Int32 = 0;
    while i < ArraySize(itemParts) {
      if Equals(itemParts[i].status, ESlotState.Taken) {
        installedPartId = ItemID.GetTDBID(itemParts[i].installedPart);
        if installedPartId != t"Items.GenericItemRoot" {
          qhRecord = TweakDBInterface.GetItemRecord(installedPartId);
          if qhRecord.Deprecated() {
            counterPartRecord = qhRecord.CounterpartHandle();
            counterPartID = counterPartRecord.GetRecordID();
            if IsDefined(counterPartRecord) && TDBID.IsValid(counterPartID) {
              convertedItem = ItemID.FromTDBID(counterPartID);
              ts.GiveItem(this, convertedItem, 1);
              ArrayPush(removedItems, convertedItem);
            };
            ts.RemoveItem(this, ts.RemovePart(this, item, itemParts[i].slotID), 1);
          } else {
            ArrayPush(removedItems, ts.RemovePart(this, item, itemParts[i].slotID));
          };
        };
      };
      i += 1;
    };
    return removedItems;
  }

  protected cb func OnRetrofixCyberwaresEvent(evt: ref<RetrofixCyberwaresEvent>) -> Bool {
    let counterPartID: TweakDBID;
    let counterPartRecord: ref<Item_Record>;
    let cwRecord: ref<Item_Record>;
    let equipmentArea: gamedataEquipmentArea;
    let equippedCW: array<ItemID>;
    let firstPriorityCW: array<ItemID>;
    let freeArmorCW: array<ItemID>;
    let i: Int32;
    let itemID: ItemID;
    let itemList: array<wref<gameItemData>>;
    let maxEquippedQuickhackQuality: Int32;
    let quickhackParts: array<ItemID>;
    let secondPriorityCW: array<ItemID>;
    let thirdPriorityCW: array<ItemID>;
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    let equipmentSystem: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    let equipmentData: ref<EquipmentSystemPlayerData> = equipmentSystem.GetPlayerData(this);
    let statsSystem: ref<StatsSystem> = GameInstance.GetStatsSystem(this.GetGame());
    let hasSmartLink: Bool = Cast<Bool>(statsSystem.GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.HasSmartLink));
    let maxAvailableQuality: gamedataQuality = RPGManager.ConvertPlayerLevelToCyberwareQuality(GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.Level), false);
    let maxAvailableQualityInt: Int32 = RPGManager.ConvertQualityToCombinedValue(maxAvailableQuality);
    let tutorialCapacityPrediction: Float = RipperDocGameController.GetApproximateTutorialCapacity(this, maxAvailableQuality, hasSmartLink);
    let availableCapacity: Float = statsSystem.GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.HumanityAvailable) - statsSystem.GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.HumanityOverallocated) - tutorialCapacityPrediction;
    let beforeVikVisit: Bool = GameInstance.GetQuestsSystem(this.GetGame()).GetFact(n"q001_ripperdoc_done") < 1;
    ts.GetItemListByTag(this, n"Cyberware", itemList);
    this.GetGlitchedEquippedCyberware(equippedCW);
    i = 0;
    while i < ArraySize(equippedCW) {
      cwRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(equippedCW[i]));
      counterPartRecord = cwRecord.CounterpartHandle();
      counterPartID = counterPartRecord.GetRecordID();
      counterPartRecord = this.GetAppropriateCWByQuality(maxAvailableQualityInt, counterPartID);
      if !TDBID.IsValid(counterPartRecord.GetRecordID()) {
      } else {
        equipmentArea = counterPartRecord.EquipArea().Type();
        if !this.PrioritizeCyberwareOnRetrofix(ItemID.FromTDBID(counterPartRecord.GetRecordID()), equipmentArea, firstPriorityCW, secondPriorityCW, thirdPriorityCW, beforeVikVisit) {
          ts.GiveItemByTDBID(this, counterPartRecord.GetRecordID(), 1);
        };
      };
      i += 1;
    };
    i = 0;
    while i < ArraySize(itemList) {
      itemID = itemList[i].GetID();
      cwRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID));
      if cwRecord.Deprecated() {
        if equipmentData.IsEquipped(itemID) {
          this.RemoveCyberwareParts(itemID);
        };
        ts.RemoveItem(this, itemID, itemList[i].GetQuantity());
      } else {
        counterPartRecord = cwRecord.CounterpartHandle();
        if IsDefined(counterPartRecord) {
          counterPartRecord = this.GetAppropriateCWByQuality(maxAvailableQualityInt, counterPartRecord.GetRecordID());
          counterPartID = counterPartRecord.GetRecordID();
          if !TDBID.IsValid(counterPartID) {
            this.RemoveCyberwareParts(itemID);
            ts.RemoveItem(this, itemID, itemList[i].GetQuantity());
          } else {
            equipmentArea = counterPartRecord.EquipArea().Type();
            if !equipmentData.IsEquipped(itemID) || !this.PrioritizeCyberwareOnRetrofix(ItemID.FromTDBID(counterPartID), equipmentArea, firstPriorityCW, secondPriorityCW, thirdPriorityCW, beforeVikVisit) {
              ts.GiveItemByTDBID(this, counterPartID, itemList[i].GetQuantity());
            };
            if equipmentData.IsEquipped(itemID) && EquipmentSystem.IsItemCyberdeck(itemID) {
              quickhackParts = this.RemoveCyberwareParts(itemID);
              maxEquippedQuickhackQuality = counterPartRecord.Quality().Value();
            } else {
              this.RemoveCyberwareParts(itemID);
            };
            ts.RemoveItem(this, itemID, itemList[i].GetQuantity());
          };
        };
      };
      i += 1;
    };
    if ArraySize(quickhackParts) > 0 {
      i = ArraySize(quickhackParts) - 1;
      while i >= 0 {
        if TweakDBInterface.GetItemRecord(ItemID.GetTDBID(quickhackParts[i])).Quality().Value() > maxEquippedQuickhackQuality {
          ArrayRemove(quickhackParts, quickhackParts[i]);
        };
        i -= 1;
      };
    };
    if !beforeVikVisit {
      ArrayPush(freeArmorCW, ItemID.FromTDBID(this.GetAppropriateCWByQuality(maxAvailableQualityInt, t"Items.AdvancedBoringPlatingCommon").GetRecordID()));
      ArrayPush(freeArmorCW, ItemID.FromTDBID(this.GetAppropriateCWByQuality(maxAvailableQualityInt, t"Items.AdvancedBionicJointsCommon").GetRecordID()));
      ArrayPush(freeArmorCW, ItemID.FromTDBID(this.GetAppropriateCWByQuality(maxAvailableQualityInt, t"Items.AdvancedChargeSystemCommon").GetRecordID()));
    };
    availableCapacity -= this.EquipNewCyberware(firstPriorityCW, availableCapacity, true, quickhackParts);
    availableCapacity -= this.EquipNewCyberware(secondPriorityCW, availableCapacity, true);
    availableCapacity -= this.EquipNewCyberware(thirdPriorityCW, availableCapacity, true);
    availableCapacity -= this.EquipNewCyberware(freeArmorCW, availableCapacity, false);
  }

  private final func GetAppropriateCWByQuality(quality: Int32, baseCWRecord: TweakDBID) -> ref<Item_Record> {
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(baseCWRecord);
    let currentQuality: Int32 = itemRecord.Quality().Value();
    if !TDBID.IsValid(baseCWRecord) {
      return itemRecord;
    };
    while currentQuality < quality {
      itemRecord = itemRecord.NextUpgradeItem();
      currentQuality = itemRecord.Quality().Value();
      if !IsDefined(itemRecord) || !TDBID.IsValid(itemRecord.GetRecordID()) {
        currentQuality = quality + 1;
        break;
      };
    };
    if !IsDefined(itemRecord) || currentQuality > quality {
      itemRecord = TweakDBInterface.GetItemRecord(baseCWRecord);
    };
    return itemRecord;
  }

  private final func EquipNewCyberware(items: [ItemID], availableCapacity: Float, addToInventoryIfOverallocated: Bool, opt stashedQuickhacks: [ItemID]) -> Float {
    let capacityTaken: Float;
    let equipRequest: ref<GameplayEquipRequest>;
    let itemCapacityRequirement: Float;
    let tags: array<CName>;
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    let equipmentSystem: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    let i: Int32 = 0;
    while i < ArraySize(items) {
      itemCapacityRequirement = this.GetItemCapacityRequirement(items[i]);
      if capacityTaken + itemCapacityRequirement > availableCapacity {
        if addToInventoryIfOverallocated {
          ts.GiveItemByTDBID(this, ItemID.GetTDBID(items[i]), 1);
        };
      } else {
        equipRequest = new GameplayEquipRequest();
        equipRequest.owner = this;
        equipRequest.addToInventory = true;
        equipRequest.blockUpdateWeaponActiveSlots = true;
        equipRequest.itemID = items[i];
        tags = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(items[i])).Tags();
        if ArrayContains(tags, n"Cyberdeck") && ArraySize(stashedQuickhacks) > 0 {
          equipRequest.partsToAdd = stashedQuickhacks;
        };
        equipmentSystem.QueueRequest(equipRequest);
        capacityTaken += itemCapacityRequirement;
      };
      i += 1;
    };
    return capacityTaken;
  }

  protected cb func OnRetrofixOverallocatedCyberwareEvent(evt: ref<RetrofixOverallocatedCyberwareEvent>) -> Bool {
    let cwRecord: ref<Item_Record>;
    let equipmentArea: gamedataEquipmentArea;
    let firstPriorityCW: array<ItemID>;
    let i: Int32;
    let itemID: ItemID;
    let itemList: array<wref<gameItemData>>;
    let secondPriorityCW: array<ItemID>;
    let thirdPriorityCW: array<ItemID>;
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    let equipmentSystem: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    let equipmentData: ref<EquipmentSystemPlayerData> = equipmentSystem.GetPlayerData(this);
    let statsSystem: ref<StatsSystem> = GameInstance.GetStatsSystem(this.GetGame());
    let availableCapacity: Float = statsSystem.GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.HumanityAvailable) - statsSystem.GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.HumanityOverallocated);
    if availableCapacity < 0.00 {
      ts.GetItemListByTag(this, n"Cyberware", itemList);
      i = 0;
      while i < ArraySize(itemList) {
        itemID = itemList[i].GetID();
        if !equipmentData.IsEquipped(itemID) {
        } else {
          cwRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(itemID));
          equipmentArea = cwRecord.EquipArea().Type();
          if Equals(equipmentArea, gamedataEquipmentArea.RightArm) || Equals(equipmentArea, gamedataEquipmentArea.EyesCW) || Equals(equipmentArea, gamedataEquipmentArea.SystemReplacementCW) || Equals(equipmentArea, gamedataEquipmentArea.ArmsCW) || Equals(equipmentArea, gamedataEquipmentArea.HandsCW) {
          } else {
            if Equals(equipmentArea, gamedataEquipmentArea.LegsCW) {
              ArrayPush(secondPriorityCW, itemID);
            } else {
              ArrayPush(thirdPriorityCW, itemID);
            };
          };
        };
        i += 1;
      };
      availableCapacity += this.UnequipOverallocatedCyberware(thirdPriorityCW, availableCapacity);
      if availableCapacity < 0.00 {
        availableCapacity += this.UnequipOverallocatedCyberware(secondPriorityCW, availableCapacity);
      };
      if availableCapacity < 0.00 {
        availableCapacity += this.UnequipOverallocatedCyberware(firstPriorityCW, availableCapacity);
      };
    };
  }

  private final func UnequipOverallocatedCyberware(items: [ItemID], availableCapacity: Float) -> Float {
    let capacityFreed: Float;
    let itemCapacityRequirement: Float;
    let equipmentSystem: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    let unequipRequest: ref<UnequipItemsRequest> = new UnequipItemsRequest();
    unequipRequest.owner = this;
    let i: Int32 = 0;
    while i < ArraySize(items) {
      itemCapacityRequirement = this.GetItemCapacityRequirement(items[i]);
      ArrayPush(unequipRequest.items, items[i]);
      capacityFreed += itemCapacityRequirement;
      if capacityFreed + availableCapacity >= 0.00 {
        break;
      };
      i += 1;
    };
    equipmentSystem.QueueRequest(unequipRequest);
    return capacityFreed;
  }

  public final func UpdateInventoryWeight(weightChange: Float, opt isLootBroken: Bool) -> Void {
    if weightChange == 0.00 {
      return;
    };
    this.m_curInventoryWeight += weightChange;
    this.EvaluateEncumbrance(isLootBroken);
  }

  protected cb func OnItemBeingRemovedFromInventory(evt: ref<ItemBeingRemovedEvent>) -> Bool {
    let dps: ref<DropPointSystem>;
    let equipData: ref<EquipmentSystemPlayerData>;
    let equipmentSystem: ref<EquipmentSystem>;
    let itemName: String;
    let itemRecord: ref<Item_Record>;
    let unequipRequest: ref<UnequipItemsRequest>;
    let dpsRequest: ref<DropPointRequest> = new DropPointRequest();
    dpsRequest.CreateRequest(ItemID.GetTDBID(evt.itemID), DropPointPackageStatus.COLLECTED);
    if !RPGManager.IsItemSingleInstance(evt.itemData) {
      this.UpdateInventoryWeight(-1.00 * RPGManager.GetItemWeight(evt.itemData));
    };
    if IsDefined(evt.itemData) {
      if !evt.itemData.HasTag(n"SkipActivityLog") && !evt.itemData.HasTag(n"SkipActivityLogOnRemove") && !ItemID.HasFlag(evt.itemData.GetID(), gameEItemIDFlag.Preview) && !RPGManager.IsItemBroken(evt.itemData) {
        itemRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(evt.itemID));
        itemName = UIItemsHelper.GetItemName(itemRecord, evt.itemData);
        GameInstance.GetActivityLogSystem(this.GetGame()).AddLog(GetLocalizedText("LocKey#26163") + ": " + itemName);
      };
    };
    dps = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"DropPointSystem") as DropPointSystem;
    if IsDefined(dps) {
      dps.QueueRequest(dpsRequest);
    };
    this.SendCheckRemovedItemWithSlotActiveItemRequest(evt.itemID);
    equipmentSystem = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    if IsDefined(equipmentSystem) {
      if equipmentSystem.IsItemInHotkey(this, evt.itemID) {
      };
      equipData = equipmentSystem.GetPlayerData(this);
      if equipData.IsEquipped(evt.itemID) {
        unequipRequest = new UnequipItemsRequest();
        unequipRequest.owner = this;
        ArrayPush(unequipRequest.items, evt.itemID);
        EquipmentSystem.GetInstance(this).QueueRequest(unequipRequest);
      };
    };
  }

  protected cb func OnInventoryEmpty(evt: ref<OnInventoryEmptyEvent>) -> Bool {
    this.m_curInventoryWeight = 0.00;
    this.EvaluateEncumbrance();
  }

  public final func EvaluateEncumbrance(opt isLootBroken: Bool) -> Void {
    let carryCapacity: Float;
    let hasEncumbranceEffect: Bool;
    let isApplyingRestricted: Bool;
    let overweightEffectID: TweakDBID;
    let ses: ref<StatusEffectSystem>;
    if this.m_curInventoryWeight < 0.00 {
      this.m_curInventoryWeight = 0.00;
    };
    ses = GameInstance.GetStatusEffectSystem(this.GetGame());
    overweightEffectID = t"BaseStatusEffect.Encumbered";
    hasEncumbranceEffect = ses.HasStatusEffect(this.GetEntityID(), overweightEffectID);
    isApplyingRestricted = StatusEffectSystem.ObjectHasStatusEffectWithTag(this, n"NoEncumbrance");
    carryCapacity = GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.CarryCapacity);
    if this.m_curInventoryWeight > carryCapacity && !isApplyingRestricted && !isLootBroken {
      this.SetWarningMessage(GetLocalizedText("UI-Notifications-Overburden"));
    };
    if this.m_curInventoryWeight > carryCapacity && !hasEncumbranceEffect && !isApplyingRestricted && !isLootBroken {
      ses.ApplyStatusEffect(this.GetEntityID(), overweightEffectID);
    } else {
      if this.m_curInventoryWeight <= carryCapacity && hasEncumbranceEffect || hasEncumbranceEffect && isApplyingRestricted {
        ses.RemoveStatusEffect(this.GetEntityID(), overweightEffectID);
      };
    };
    GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_PlayerStats).SetFloat(GetAllBlackboardDefs().UI_PlayerStats.currentInventoryWeight, this.m_curInventoryWeight, true);
  }

  private final func CalculateEncumbrance() -> Void {
    let i: Int32;
    let items: array<wref<gameItemData>>;
    let TS: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    TS.GetItemList(this, items);
    i = 0;
    while i < ArraySize(items) {
      if !ItemID.HasFlag(items[i].GetID(), gameEItemIDFlag.Preview) {
        this.m_curInventoryWeight += RPGManager.GetItemStackWeight(this, items[i]);
      };
      i += 1;
    };
  }

  protected cb func OnEvaluateEncumbranceEvent(evt: ref<EvaluateEncumbranceEvent>) -> Bool {
    this.EvaluateEncumbrance();
  }

  private final func SendCheckRemovedItemWithSlotActiveItemRequest(item: ItemID) -> Void {
    let request: ref<CheckRemovedItemWithSlotActiveItem> = new CheckRemovedItemWithSlotActiveItem();
    request.itemID = item;
    request.owner = this;
    GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem").QueueRequest(request);
  }

  protected cb func OnTakedownOrder(evt: ref<OrderTakedownEvent>) -> Bool {
    let squadInterface: ref<PlayerSquadInterface>;
    let takedownCommand: ref<AIFollowerTakedownCommand> = new AIFollowerTakedownCommand();
    takedownCommand.combatCommand = true;
    takedownCommand.target = evt.target;
    if AISquadHelper.GetPlayerSquadInterface(this, squadInterface) {
      squadInterface.BroadcastCommand(takedownCommand);
    };
  }

  protected cb func OnSpiderbotOrderTargetEvent(evt: ref<SpiderbotOrderDeviceEvent>) -> Bool {
    let squadInterface: ref<PlayerSquadInterface>;
    let deviceCommand: ref<AIFollowerDeviceCommand> = new AIFollowerDeviceCommand();
    deviceCommand.target = evt.target;
    deviceCommand.overrideMovementTarget = evt.overrideMovementTarget;
    if AISquadHelper.GetPlayerSquadInterface(this, squadInterface) {
      squadInterface.BroadcastCommand(deviceCommand);
    };
  }

  private final func OnHitBlockedOrDeflected(hitEvent: ref<gameHitEvent>) -> Void {
    if hitEvent.attackData.WasDeflectedAny() {
      this.PushDeflectEvent(hitEvent.attackData.WasBulletDeflected());
      GameObject.PlayVoiceOver(this, n"meleeDeflect", n"Scripts:OnHitBlockedOrDeflected");
      GameInstance.GetTelemetrySystem(this.GetGame()).LogHitDefense(telemetryHitDefenseType.Deflect);
    } else {
      if hitEvent.attackData.WasBlocked() {
        this.PushDeflectEvent(hitEvent.attackData.WasBulletBlocked());
        GameInstance.GetTelemetrySystem(this.GetGame()).LogHitDefense(telemetryHitDefenseType.Block);
      };
    };
  }

  private final func PushDeflectEvent(isBulletDeflect: Bool) -> Void {
    if isBulletDeflect {
      if !this.m_waitingForDelayEvent {
        GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, new KatanaMagFieldHitDelayEvent(), 0.40);
        this.m_waitingForDelayEvent = true;
        AnimationControllerComponent.PushEvent(this, n"MeleeBulletDeflect");
      };
    } else {
      AnimationControllerComponent.PushEvent(this, n"MeleeBlock");
    };
  }

  protected cb func OnKatanaMagFieldHitDelayEvent(evt: ref<KatanaMagFieldHitDelayEvent>) -> Bool {
    this.m_waitingForDelayEvent = false;
  }

  private final func OnHitAnimation(hitEvent: ref<gameHitEvent>) -> Void {
    let playerStateMachineBlackboard: ref<IBlackboard>;
    let playerVehicle: wref<VehicleObject>;
    let remoteControlledVehicle: wref<VehicleObject>;
    let vehicleID: EntityID;
    super.OnHitAnimation(hitEvent);
    this.GetDamageThresholdParams();
    this.PushHitDataToGraph(hitEvent);
    this.AddOnHitRumble(hitEvent);
    if this.GetZoomBlackboardValues() {
      this.SetZoomBlackboardValues(false);
    };
    TakeOverControlSystem.ReleaseControlOnHit(this);
    playerStateMachineBlackboard = GameInstance.GetBlackboardSystem(this.GetGame()).GetLocalInstanced(this.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    vehicleID = playerStateMachineBlackboard.GetEntityID(GetAllBlackboardDefs().PlayerStateMachine.EntityIDVehicleRemoteControlled);
    if EntityID.IsDefined(vehicleID) {
      if VehicleComponent.GetVehicleFromID(this.GetGame(), vehicleID, remoteControlledVehicle) && !VehicleComponent.GetVehicle(this.GetGame(), this.GetEntityID(), playerVehicle) {
        remoteControlledVehicle.SetVehicleRemoteControlled(false, false, true);
      };
    };
  }

  private final func AddOnHitRumble(hitEvent: ref<gameHitEvent>) -> Void {
    let direction: Int32;
    let soundName: CName;
    let thresholdHigh: Float;
    let thresholdMed: Float;
    let rumbleIntensityPrefix: String = "light_";
    let rumbleDirectionSuffix: String = "";
    let rumbleDuration: String = "pulse";
    let damageDealt: Float = hitEvent.attackComputed.GetTotalAttackValue(gamedataStatPoolType.Health);
    let totalHealth: Float = GameInstance.GetStatPoolsSystem(this.GetGame()).GetStatPoolMaxPointValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.Health);
    if damageDealt < 1.00 {
      return;
    };
    if AttackData.IsDoT(hitEvent.attackData) {
      soundName = TDB.GetCName(t"rumble.local.medium_slow");
      GameObject.PlaySound(this, soundName);
      return;
    };
    damageDealt = damageDealt / totalHealth;
    if AttackData.IsMelee(hitEvent.attackData.GetAttackType()) {
      thresholdMed = TDB.GetFloat(t"player.onHitRumble.meleeMedIntensityThreshold");
      thresholdHigh = TDB.GetFloat(t"player.onHitRumble.meleeHighIntensityThreshold");
      rumbleDuration = "slow";
    } else {
      thresholdMed = TDB.GetFloat(t"player.onHitRumble.medIntensityThreshold");
      thresholdHigh = TDB.GetFloat(t"player.onHitRumble.highIntensityThreshold");
    };
    if damageDealt >= thresholdHigh {
      rumbleIntensityPrefix = "heavy_";
    } else {
      if damageDealt >= thresholdMed {
        rumbleIntensityPrefix = "medium_";
      };
    };
    direction = GameObject.GetAttackAngleInInt(hitEvent);
    if direction == 1 {
      rumbleDirectionSuffix = "_left";
    } else {
      if direction == 3 {
        rumbleDirectionSuffix = "_right";
      };
    };
    soundName = TDB.GetCName(TDBID.Create("rumble.local." + rumbleIntensityPrefix + rumbleDuration + rumbleDirectionSuffix));
    GameObject.PlaySound(this, soundName);
  }

  private final func PushHitDataToGraph(hitEvent: ref<gameHitEvent>) -> Void {
    let minShakeStrength: Float;
    let shakeStrength: Float;
    let enableOnHitCameraShake: Bool = TweakDBInterface.GetBool(t"player.cameraShake.enableOnHitCameraShake", true);
    let useMinMaxRangeValues: Bool = TweakDBInterface.GetBool(t"player.cameraShake.useMinMaxRangeValues", true);
    let minShakeStrengthString: String = "player.cameraShake.";
    let maxShakeStrengthString: String = "player.cameraShake.";
    let damageDealt: Float = hitEvent.attackComputed.GetTotalAttackValue(gamedataStatPoolType.Health);
    let totalHealth: Float = GameInstance.GetStatPoolsSystem(this.GetGame()).GetStatPoolValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.Health);
    damageDealt = damageDealt / totalHealth;
    let attackSource: wref<GameObject> = hitEvent.attackData.GetSource();
    if damageDealt <= 0.00 {
      return;
    };
    if enableOnHitCameraShake {
      if IsDefined(attackSource as SecurityTurret) {
        minShakeStrengthString = minShakeStrengthString + "defaultMedMin";
        maxShakeStrengthString = maxShakeStrengthString + "defaultMedMax";
      } else {
        if AttackData.IsDoT(hitEvent.attackData) && StatusEffectSystem.ObjectHasStatusEffect(hitEvent.target, t"BaseStatusEffect.OutOfOxygen") {
          minShakeStrengthString = minShakeStrengthString + "outOfOxyMin";
          maxShakeStrengthString = maxShakeStrengthString + "outOfOxyMax";
        } else {
          if AttackData.IsExplosion(hitEvent.attackData.GetAttackType()) {
            if damageDealt >= this.m_explosionHighDamageThreshold {
              minShakeStrengthString = minShakeStrengthString + "explosionHighMin";
              maxShakeStrengthString = maxShakeStrengthString + "explosionHighMax";
            } else {
              if damageDealt >= this.m_explosionMedDamageThreshold {
                minShakeStrengthString = minShakeStrengthString + "explosionMedMin";
                maxShakeStrengthString = maxShakeStrengthString + "explosionMedMax";
              } else {
                if damageDealt >= this.m_explosionLowDamageThreshold {
                  minShakeStrengthString = minShakeStrengthString + "explosionLowMin";
                  maxShakeStrengthString = maxShakeStrengthString + "explosionLowMax";
                };
              };
            };
          } else {
            if AttackData.IsMelee(hitEvent.attackData.GetAttackType()) {
              if damageDealt >= this.m_meleeHighDamageThreshold {
                minShakeStrengthString = minShakeStrengthString + "meleeHighMin";
                maxShakeStrengthString = maxShakeStrengthString + "meleeHighMax";
              } else {
                if damageDealt >= this.m_meleeMedDamageThreshold {
                  minShakeStrengthString = minShakeStrengthString + "meleeMedMin";
                  maxShakeStrengthString = maxShakeStrengthString + "meleeMedMax";
                } else {
                  if damageDealt >= this.m_meleeLowDamageThreshold {
                    minShakeStrengthString = minShakeStrengthString + "meleeLowMin";
                    maxShakeStrengthString = maxShakeStrengthString + "meleeLowMax";
                  };
                };
              };
            } else {
              if damageDealt >= this.m_highDamageThreshold {
                minShakeStrengthString = minShakeStrengthString + "defaultLowMin";
                maxShakeStrengthString = maxShakeStrengthString + "defaultLowMax";
              } else {
                if damageDealt >= this.m_medDamageThreshold {
                  minShakeStrengthString = minShakeStrengthString + "defaultMedMin";
                  maxShakeStrengthString = maxShakeStrengthString + "defaultMedMax";
                } else {
                  if damageDealt >= this.m_lowDamageThreshold {
                    minShakeStrengthString = minShakeStrengthString + "defaultHighMin";
                    maxShakeStrengthString = maxShakeStrengthString + "defaultHighMax";
                  };
                };
              };
            };
          };
        };
      };
      minShakeStrength = TweakDBInterface.GetFloat(TDBID.Create(minShakeStrengthString), 0.00);
      shakeStrength = TweakDBInterface.GetFloat(TDBID.Create(maxShakeStrengthString), 0.00);
    } else {
      shakeStrength = 0.00;
    };
    if useMinMaxRangeValues {
      shakeStrength = RandRangeF(minShakeStrength, shakeStrength);
    };
    this.SendCameraShakeDataToGraph(hitEvent, shakeStrength);
  }

  protected cb func OnCameraShakeEvent(evt: ref<gameCameraShakeEvent>) -> Bool {
    this.SendCameraShakeDataToGraph(evt.shakeStrength);
  }

  public final func SendCameraShakeDataToGraph(opt hitEvent: ref<gameHitEvent>, shakeStrength: Float) -> Void {
    let attackRecord: ref<Attack_Melee_Record>;
    let animFeature: ref<AnimFeature_PlayerHitReactionData> = new AnimFeature_PlayerHitReactionData();
    let attackType: gamedataAttackType = hitEvent.attackData.GetAttackType();
    animFeature.isMeleeHit = AttackData.IsMelee(attackType);
    animFeature.isLightMeleeHit = AttackData.IsLightMelee(attackType);
    animFeature.isStrongMeleeHit = AttackData.IsStrongMelee(attackType);
    animFeature.isQuickMeleeHit = AttackData.IsQuickMelee(attackType);
    animFeature.isExplosion = AttackData.IsExplosion(attackType);
    animFeature.isPressureWave = AttackData.IsPressureWave(attackType);
    if animFeature.isMeleeHit {
      attackRecord = hitEvent.attackData.GetAttackDefinition().GetRecord() as Attack_Melee_Record;
      animFeature.meleeAttackDirection = EnumInt(attackRecord.AttackDirection().Direction().Type());
    };
    animFeature.hitDirection = GameObject.GetAttackAngleInFloat(hitEvent);
    animFeature.hitStrength = shakeStrength;
    AnimationControllerComponent.ApplyFeature(this, n"HitReactionData", animFeature);
    AnimationControllerComponent.PushEvent(this, n"Hit");
  }

  private final func OnHitUI(hitEvent: ref<gameHitEvent>) -> Void {
    let dmgType: gamedataDamageType;
    let effName: CName;
    let attackValues: array<Float> = hitEvent.attackComputed.GetAttackValues();
    let i: Int32 = 0;
    while i < ArraySize(attackValues) {
      if attackValues[i] > 0.00 {
        dmgType = IntEnum<gamedataDamageType>(i);
        break;
      };
      i += 1;
    };
    switch dmgType {
      case gamedataDamageType.Thermal:
        effName = n"fire_damage_indicator";
        break;
      case gamedataDamageType.Electric:
        effName = n"emp_damage_indicator";
        break;
      default:
    };
    if AttackData.IsDoT(hitEvent.attackData) {
      if hitEvent.attackData.GetAttackDefinition().GetRecord().GetID() == t"Attacks.OutOfOxygenDamageOverTime" {
        effName = n"status_pain";
      };
    };
    GameObjectEffectHelper.StartEffectEvent(this, effName);
  }

  private final func OnHitSounds(hitEvent: ref<gameHitEvent>) -> Void {
    let damagePercentage: Float;
    let damageSwitch: ref<SoundSwitchEvent>;
    let damageTypeSwitch: ref<SoundSwitchEvent>;
    let damageValue: Float;
    let forwardLocalToWorldAngle: Float;
    let hitDirection: Vector4;
    let playerOutOfOxygen: Bool;
    let soundEvent: ref<SoundPlayEvent>;
    let soundParamAxisX: ref<SoundParameterEvent>;
    let soundParamAxisY: ref<SoundParameterEvent>;
    let target: ref<GameObject>;
    let totalHealth: Float;
    super.OnHitSounds(hitEvent);
    playerOutOfOxygen = hitEvent.attackData.GetAttackDefinition().GetRecord().GetID() == t"Attacks.OutOfOxygenDamageOverTime";
    if playerOutOfOxygen {
      return;
    };
    soundEvent = new SoundPlayEvent();
    damageSwitch = new SoundSwitchEvent();
    damageTypeSwitch = new SoundSwitchEvent();
    soundParamAxisX = new SoundParameterEvent();
    soundParamAxisY = new SoundParameterEvent();
    target = hitEvent.target;
    forwardLocalToWorldAngle = Vector4.Heading(target.GetWorldForward());
    hitDirection = Vector4.RotByAngleXY(hitEvent.hitDirection, forwardLocalToWorldAngle);
    soundParamAxisX.parameterName = n"RTPC_Positioning_2D_LR_axis";
    soundParamAxisX.parameterValue = hitDirection.X * 100.00;
    soundParamAxisY.parameterName = n"RTPC_Positioning_2D_FB_axis";
    soundParamAxisY.parameterValue = hitDirection.Y * 100.00;
    target.QueueEvent(soundParamAxisX);
    target.QueueEvent(soundParamAxisY);
    damageSwitch.switchName = n"SW_Impact_Velocity";
    damageValue = hitEvent.attackComputed.GetTotalAttackValue(gamedataStatPoolType.Health);
    damageTypeSwitch.switchName = n"w_feedback_player_damage_type";
    if Equals(hitEvent.attackData.GetAttackType(), gamedataAttackType.PressureWave) {
      damageTypeSwitch.switchValue = n"falling";
    } else {
      if Equals(hitEvent.attackData.GetAttackType(), gamedataAttackType.Ranged) {
        damageTypeSwitch.switchValue = n"bullet";
      } else {
        damageTypeSwitch.switchValue = n"generic";
      };
    };
    target.QueueEvent(damageTypeSwitch);
    if damageValue <= 0.00 {
      return;
    };
    if damageValue >= this.m_highDamageThreshold {
      damageSwitch.switchValue = n"SW_Impact_Velocity_Hi";
    } else {
      if damageValue >= this.m_medDamageThreshold {
        damageSwitch.switchValue = n"SW_Impact_Velocity_Med";
      } else {
        if damageValue >= this.m_lowDamageThreshold {
          damageSwitch.switchValue = n"SW_Impact_Velocity_Low";
        };
      };
    };
    target.QueueEvent(damageSwitch);
    GameObject.PlayVoiceOver(this, n"onPlayerHit", n"Scripts:OnHitSounds");
    if !hitEvent.attackData.GetWeapon().GetItemData().HasTag(n"MeleeWeapon") {
      totalHealth = GameInstance.GetStatPoolsSystem(this.GetGame()).GetStatPoolMaxPointValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.Health);
      damagePercentage = ClampF(damageValue, 0.00, totalHealth) / totalHealth * 100.00;
      GameInstance.GetAudioSystem(this.GetGame()).GlobalParameter(n"w_feedback_player_damage", damagePercentage);
      soundEvent.soundName = n"w_feedback_player_damage";
      target.QueueEvent(soundEvent);
    };
    if IsClient() && this.IsControlledByLocalPeer() && GameInstance.GetStatPoolsSystem(this.GetGame()).GetStatPoolValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.CPO_Armor) == 0.00 {
      soundEvent.soundName = n"test_ad_emitter_2_1";
      target.QueueEvent(soundEvent);
    };
  }

  protected cb func OnDamageInflicted(evt: ref<DamageInflictedEvent>) -> Bool {
    this.m_lastDmgInflicted = GameInstance.GetSimTime(this.GetGame());
  }

  public final func GetLastDamageInflictedTime() -> EngineTime {
    return this.m_lastDmgInflicted;
  }

  protected cb func OnInteraction(choiceEvent: ref<InteractionChoiceEvent>) -> Bool {
    let clearDataEvent: ref<CPOMissionDataTransferred>;
    let currentDataOwner: ref<PlayerPuppet>;
    let receivingData: ref<PlayerPuppet>;
    let transferDataEvent: ref<CPOMissionDataTransferred>;
    let choice: String = choiceEvent.choice.choiceMetaData.tweakDBName;
    if Equals(choice, "TakeCPOMissionDataFromPlayer") {
      currentDataOwner = choiceEvent.hotspot as PlayerPuppet;
      receivingData = choiceEvent.activator as PlayerPuppet;
      clearDataEvent = new CPOMissionDataTransferred();
      clearDataEvent.dataDownloaded = false;
      clearDataEvent.isChoiceToken = currentDataOwner.m_CPOMissionDataState.m_isChoiceToken;
      currentDataOwner.QueueEvent(clearDataEvent);
      transferDataEvent = new CPOMissionDataTransferred();
      transferDataEvent.dataDownloaded = true;
      transferDataEvent.compatibleDeviceName = currentDataOwner.m_CPOMissionDataState.m_compatibleDeviceName;
      transferDataEvent.dataDamagesPresetName = currentDataOwner.m_CPOMissionDataState.m_CPOMissionDataDamagesPreset;
      transferDataEvent.ownerDecidesOnTransfer = currentDataOwner.m_CPOMissionDataState.m_ownerDecidesOnTransfer;
      transferDataEvent.isChoiceToken = currentDataOwner.m_CPOMissionDataState.m_isChoiceToken;
      transferDataEvent.choiceTokenTimeout = currentDataOwner.m_CPOMissionDataState.m_choiceTokenTimeout;
      receivingData.QueueEvent(transferDataEvent);
    } else {
      if Equals(choice, "GiveCPOMissionDataToPlayer") {
        receivingData = choiceEvent.hotspot as PlayerPuppet;
        currentDataOwner = choiceEvent.activator as PlayerPuppet;
        clearDataEvent = new CPOMissionDataTransferred();
        clearDataEvent.dataDownloaded = false;
        clearDataEvent.isChoiceToken = currentDataOwner.m_CPOMissionDataState.m_isChoiceToken;
        currentDataOwner.QueueEvent(clearDataEvent);
        transferDataEvent = new CPOMissionDataTransferred();
        transferDataEvent.dataDownloaded = true;
        transferDataEvent.compatibleDeviceName = currentDataOwner.m_CPOMissionDataState.m_compatibleDeviceName;
        transferDataEvent.dataDamagesPresetName = currentDataOwner.m_CPOMissionDataState.m_CPOMissionDataDamagesPreset;
        transferDataEvent.ownerDecidesOnTransfer = currentDataOwner.m_CPOMissionDataState.m_ownerDecidesOnTransfer;
        transferDataEvent.isChoiceToken = currentDataOwner.m_CPOMissionDataState.m_isChoiceToken;
        transferDataEvent.choiceTokenTimeout = currentDataOwner.m_CPOMissionDataState.m_choiceTokenTimeout;
        receivingData.QueueEvent(transferDataEvent);
      };
    };
    super.OnInteraction(choiceEvent);
  }

  protected cb func OnTogglePlayerFlashlightEvent(evt: ref<TogglePlayerFlashlightEvent>) -> Bool {
    let comp: wref<IComponent> = this.FindComponentByName(n"TEMP_flashlight");
    if IsDefined(comp) {
      comp.Toggle(evt.enable);
    };
  }

  protected cb func OnToggleNewPlayerFlashlightEvent(evt: ref<ToggleNewPlayerFlashlightEvent>) -> Bool {
    let comp: wref<gameLightComponent> = this.FindComponentByName(n"Flashlight") as gameLightComponent;
    if IsDefined(comp) {
      comp.ToggleLight(!comp.IsOn());
    };
  }

  private final func InitializeTweakDBRecords() -> Void {
    this.m_coverRecordID = t"Character.Player_Cover_Modifier";
    this.m_damageReductionRecordID = t"Character.Player_Workspot_DamageReduction_Modifier";
    this.m_visReductionRecordID = t"Character.Player_Workspot_VisibilityReduction_Modifier";
  }

  protected final func DefineModifierGroups() -> Void {
    let statsSystem: ref<StatsSystem> = GameInstance.GetStatsSystem(this.GetGame());
    statsSystem.DefineModifierGroupFromRecord(TDBID.ToNumber(this.m_damageReductionRecordID), this.m_damageReductionRecordID);
    statsSystem.DefineModifierGroupFromRecord(TDBID.ToNumber(this.m_visReductionRecordID), this.m_visReductionRecordID);
    statsSystem.DefineModifierGroupFromRecord(TDBID.ToNumber(this.m_coverRecordID), this.m_coverRecordID);
  }

  protected final func RegisterStatListeners(self: ref<PlayerPuppet>) -> Void {
    let statsSystem: ref<StatsSystem> = GameInstance.GetStatsSystem(this.GetGame());
    let statPoolsSystem: ref<StatPoolsSystem> = GameInstance.GetStatPoolsSystem(this.GetGame());
    let entityID: EntityID = this.GetEntityID();
    this.m_visibilityListener = new VisibilityStatListener();
    this.m_visibilityListener.SetStatType(gamedataStatType.Visibility);
    this.m_visibilityListener.m_owner = self;
    statsSystem.RegisterListener(Cast<StatsObjectID>(entityID), this.m_visibilityListener);
    this.m_secondHeartListener = new SecondHeartStatListener();
    this.m_secondHeartListener.SetStatType(gamedataStatType.HasSecondHeart);
    this.m_secondHeartListener.m_player = self;
    statsSystem.RegisterListener(Cast<StatsObjectID>(entityID), this.m_secondHeartListener);
    this.m_healthStatListener = new HealthStatListener();
    this.m_healthStatListener.m_ownerPuppet = self;
    statPoolsSystem.RequestRegisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.Health, this.m_healthStatListener);
    this.m_oxygenStatListener = new OxygenStatListener();
    this.m_oxygenStatListener.SetValue(TweakDBInterface.GetFloat(t"player.oxygenThresholds.critOxygenThreshold", 10.00));
    this.m_oxygenStatListener.m_ownerPuppet = self;
    statPoolsSystem.RequestRegisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.Oxygen, this.m_oxygenStatListener);
    this.m_autoRevealListener = new AutoRevealStatListener();
    this.m_autoRevealListener.SetStatType(gamedataStatType.AutoReveal);
    this.m_autoRevealListener.m_owner = self;
    statsSystem.RegisterListener(Cast<StatsObjectID>(entityID), this.m_autoRevealListener);
    this.m_aimAssistListener = new AimAssistSettingsListener();
    this.m_aimAssistListener.Initialize(self);
    this.m_accessibilityControlsListener = new AccessibilityControlsListener();
    this.m_accessibilityControlsListener.Initialize(self);
    if this.IsControlledByLocalPeer() {
      this.m_armorStatListener = new ArmorStatListener();
      this.m_armorStatListener.m_ownerPuppet = self;
      statPoolsSystem.RequestRegisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.CPO_Armor, this.m_armorStatListener);
    };
    this.m_memoryListener = new MemoryListener();
    this.m_memoryListener.m_player = this;
    statPoolsSystem.RequestRegisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.Memory, this.m_memoryListener);
    statPoolsSystem.RequestMarkingStatPoolActive(Cast<StatsObjectID>(entityID), gamedataStatPoolType.Memory);
    this.m_staminaListener = new StaminaListener();
    this.m_staminaListener.Init(this);
    statPoolsSystem.RequestRegisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.Stamina, this.m_staminaListener);
    this.m_HealingItemsChargeStatListener = new HealingItemsChargeStatListener();
    this.m_HealingItemsChargeStatListener.Init(this);
    statPoolsSystem.RequestRegisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.HealingItemsCharges, this.m_HealingItemsChargeStatListener);
    this.m_GrenadesChargeStatListener = new GrenadesChargeStatListener();
    this.m_GrenadesChargeStatListener.Init(this);
    statPoolsSystem.RequestRegisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.GrenadesCharges, this.m_GrenadesChargeStatListener);
    this.m_ProjectileLauncherChargeStatListener = new ProjectileLauncherChargeStatListener();
    this.m_ProjectileLauncherChargeStatListener.Init(this);
    statPoolsSystem.RequestRegisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.ProjectileLauncherCharges, this.m_ProjectileLauncherChargeStatListener);
    this.m_OpticalCamoChargeStatListener = new OpticalCamoChargeStatListener();
    this.m_OpticalCamoChargeStatListener.Init(this);
    statPoolsSystem.RequestRegisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.OpticalCamoCharges, this.m_OpticalCamoChargeStatListener);
    this.m_OverclockChargeListener = new OverclockChargeListener();
    this.m_OverclockChargeListener.Init(this);
    statPoolsSystem.RequestRegisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.CyberdeckOverclock, this.m_OverclockChargeListener);
  }

  protected final func UnregisterStatListeners(self: ref<PlayerPuppet>) -> Void {
    let statsSystem: ref<StatsSystem> = GameInstance.GetStatsSystem(this.GetGame());
    let statPoolsSystem: ref<StatPoolsSystem> = GameInstance.GetStatPoolsSystem(this.GetGame());
    let entityID: EntityID = this.GetEntityID();
    statsSystem.UnregisterListener(Cast<StatsObjectID>(entityID), this.m_visibilityListener);
    this.m_visibilityListener = null;
    statsSystem.UnregisterListener(Cast<StatsObjectID>(entityID), this.m_secondHeartListener);
    this.m_secondHeartListener = null;
    statsSystem.UnregisterListener(Cast<StatsObjectID>(entityID), this.m_autoRevealListener);
    this.m_autoRevealListener = null;
    if this.IsControlledByLocalPeer() {
      statPoolsSystem.RequestUnregisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.CPO_Armor, this.m_armorStatListener);
      this.m_armorStatListener = null;
    };
    this.m_aimAssistListener = null;
    this.m_accessibilityControlsListener = null;
    statPoolsSystem.RequestUnregisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.Health, this.m_healthStatListener);
    this.m_healthStatListener = null;
    statPoolsSystem.RequestUnregisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.Oxygen, this.m_oxygenStatListener);
    this.m_oxygenStatListener = null;
    statPoolsSystem.RequestUnregisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.Memory, this.m_memoryListener);
    this.m_memoryListener = null;
    statPoolsSystem.RequestUnregisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.Stamina, this.m_staminaListener);
    this.m_staminaListener = null;
    statPoolsSystem.RequestUnregisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.HealingItemsCharges, this.m_HealingItemsChargeStatListener);
    this.m_HealingItemsChargeStatListener = null;
    statPoolsSystem.RequestUnregisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.GrenadesCharges, this.m_GrenadesChargeStatListener);
    this.m_GrenadesChargeStatListener = null;
    statPoolsSystem.RequestUnregisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.ProjectileLauncherCharges, this.m_ProjectileLauncherChargeStatListener);
    this.m_ProjectileLauncherChargeStatListener = null;
    statPoolsSystem.RequestUnregisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.OpticalCamoCharges, this.m_OpticalCamoChargeStatListener);
    this.m_OpticalCamoChargeStatListener = null;
    statPoolsSystem.RequestUnregisteringListener(Cast<StatsObjectID>(entityID), gamedataStatPoolType.CyberdeckOverclock, this.m_OverclockChargeListener);
    this.m_OverclockChargeListener = null;
  }

  protected cb func OnCleanUpTimeDilationEvent(evt: ref<CleanUpTimeDilationEvent>) -> Bool {
    let reason: CName;
    let timeSystem: ref<TimeSystem>;
    if Equals(evt.reason, n"focusMode") && this.GetPlayerStateMachineBlackboard().GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == 1 {
      return false;
    };
    timeSystem = GameInstance.GetTimeSystem(this.GetGame());
    timeSystem.UnsetTimeDilation(evt.reason, n"None");
    timeSystem.SetTimeDilationOnLocalPlayerZero(reason, 1.00, 0.10, n"None", n"None");
    timeSystem.SetIgnoreTimeDilationOnLocalPlayerZero(false);
    timeSystem.UnsetTimeDilationOnLocalPlayerZero(reason, n"None");
  }

  protected cb func OnHealthUpdateEvent(evt: ref<HealthUpdateEvent>) -> Bool {
    this.UpdateHealthStateSFX(evt.value);
    this.UpdateHealthStateVFX(evt.value);
    this.m_lastHealthUpdate = evt.value;
    GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_PlayerStats).SetInt(GetAllBlackboardDefs().UI_PlayerStats.CurrentHealth, Cast<Int32>(evt.value), true);
  }

  private final func UpdateHealthStateSFX(healthValue: Float) -> Void {
    let lowHealthThreshold: Float = PlayerPuppet.GetLowHealthThreshold();
    let critHealthThreshold: Float = PlayerPuppet.GetCriticalHealthThreshold();
    GameInstance.GetAudioSystem(this.GetGame()).GlobalParameter(n"g_player_health", healthValue);
    if healthValue > lowHealthThreshold {
      GameInstance.GetAudioSystem(this.GetGame()).NotifyGameTone(n"InNormalHealth");
    } else {
      GameInstance.GetAudioSystem(this.GetGame()).NotifyGameTone(n"InLowHealth");
      if healthValue > TweakDBInterface.GetFloat(t"player.hitVFX.critHealthRumbleEndThreshold", 30.00) {
        this.m_critHealthRumblePlayed = false;
        this.StopCritHealthRumble();
      };
      if healthValue <= critHealthThreshold && healthValue > 0.00 {
        if !this.m_critHealthRumblePlayed {
          this.m_critHealthRumblePlayed = true;
          this.PlayCritHealthRumble();
          GameInstance.GetDelaySystem(this.GetGame()).CancelDelay(this.m_critHealthRumbleDurationID);
          this.m_critHealthRumbleDurationID = GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, new StopCritHealthRumble(), TweakDBInterface.GetFloat(t"player.hitVFX.critHealthRumbleMaxDuration", 8.00));
        };
      };
    };
  }

  protected cb func OnStopCritHealthRumble(evt: ref<StopCritHealthRumble>) -> Bool {
    this.StopCritHealthRumble();
  }

  private final func PlayCritHealthRumble() -> Void {
    GameObject.PlaySound(this, TDB.GetCName(t"rumble.local.loop_light"));
  }

  private final func StopCritHealthRumble() -> Void {
    GameObject.StopSound(this, TDB.GetCName(t"rumble.local.loop_light"));
  }

  private final func UpdateHealthStateVFX(healthValue: Float) -> Void {
    let lowHealthThreshold: Float = PlayerPuppet.GetLowHealthThreshold();
    let critHealthThreshold: Float = PlayerPuppet.GetCriticalHealthThreshold();
    if IsClient() && this.IsControlledByLocalPeer() || !IsMultiplayer() {
      if IsDefined(this.m_healthVfxBlackboard) && (healthValue >= lowHealthThreshold || StatusEffectSystem.ObjectHasStatusEffectWithTag(this, n"PreventLowHealthOverlay")) {
        GameObjectEffectHelper.BreakEffectLoopEvent(this, n"fx_health_low");
        this.m_healthVfxBlackboard = null;
        GameInstance.GetTelemetrySystem(this.GetGame()).LogPlayerReachedCriticalHealth(false);
        GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().PhotoMode).SetUint(GetAllBlackboardDefs().PhotoMode.PlayerHealthState, 0u);
      } else {
        if healthValue <= critHealthThreshold && TDB.GetBool(t"player.hitVFX.useCriticalThreshold") {
          if !IsDefined(this.m_healthVfxBlackboard) {
            if !StatusEffectSystem.ObjectHasStatusEffectWithTag(this, n"PreventLowHealthOverlay") {
              this.m_healthVfxBlackboard = new worldEffectBlackboard();
              this.m_healthVfxBlackboard.SetValue(n"health_state", healthValue / critHealthThreshold);
              GameObjectEffectHelper.StartEffectEvent(this, n"fx_health_critical", false, this.m_healthVfxBlackboard);
              GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().PhotoMode).SetUint(GetAllBlackboardDefs().PhotoMode.PlayerHealthState, 2u);
            };
          } else {
            this.m_healthVfxBlackboard.SetValue(n"health_state", healthValue / critHealthThreshold);
          };
        } else {
          if healthValue <= lowHealthThreshold {
            if !IsDefined(this.m_healthVfxBlackboard) {
              if !StatusEffectSystem.ObjectHasStatusEffectWithTag(this, n"PreventLowHealthOverlay") {
                this.m_healthVfxBlackboard = new worldEffectBlackboard();
                this.m_healthVfxBlackboard.SetValue(n"health_state", healthValue / lowHealthThreshold);
                if StatusEffectSystem.ObjectHasStatusEffect(this, t"BaseStatusEffect.CerberusGrabAttackEffect01") {
                  GameObjectEffectHelper.StartEffectEvent(this, n"eyes_closing_instant", false, this.m_healthVfxBlackboard);
                } else {
                  if StatusEffectSystem.ObjectHasStatusEffect(this, t"BaseStatusEffect.CerberusGrabAttackEffectBack") {
                    GameObjectEffectHelper.StopEffectEvent(this, n"status_bleeding");
                    GameObjectEffectHelper.StartEffectEvent(this, n"eyes_closed_loop", false, this.m_healthVfxBlackboard);
                  } else {
                    GameObjectEffectHelper.StartEffectEvent(this, n"fx_health_low", false, this.m_healthVfxBlackboard);
                  };
                };
                GameInstance.GetTelemetrySystem(this.GetGame()).LogPlayerReachedCriticalHealth(true);
                ReactionManagerComponent.SendVOEventToSquad(this, n"player_fallback");
                GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().PhotoMode).SetUint(GetAllBlackboardDefs().PhotoMode.PlayerHealthState, 1u);
              };
            } else {
              this.m_healthVfxBlackboard.SetValue(n"health_state", healthValue / lowHealthThreshold);
            };
          };
        };
      };
    };
  }

  private final func SetZoomBlackboardValues(newState: Bool) -> Void {
    let playerStateMachineBlackboard: ref<IBlackboard> = GameInstance.GetBlackboardSystem(this.GetGame()).GetLocalInstanced(this.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.IsUIZoomDevice, newState);
    playerStateMachineBlackboard.FireCallbacks();
  }

  private final func GetZoomBlackboardValues() -> Bool {
    let playerStateMachineBlackboard: ref<IBlackboard> = GameInstance.GetBlackboardSystem(this.GetGame()).GetLocalInstanced(this.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    return playerStateMachineBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsUIZoomDevice);
  }

  protected cb func OnRewardEvent(evt: ref<RewardEvent>) -> Bool {
    RPGManager.GiveReward(this.GetGame(), evt.rewardName);
  }

  protected cb func OnManagePersonalLinkChangeEvent(evt: ref<ManagePersonalLinkChangeEvent>) -> Bool {
    RPGManager.TogglePersonalLinkAppearance(this);
    RPGManager.ToggleHolsteredArmAppearance(this, evt.shouldEquip);
  }

  public final const func IsBlackwallForceEquippedOnPlayer() -> Bool {
    let weaponItem: ItemID = EquipmentSystem.GetData(this).GetActiveItem(gamedataEquipmentArea.Weapon);
    let itemRecord: wref<Item_Record> = RPGManager.GetItemRecord(weaponItem);
    let itemTags: array<CName> = itemRecord.Tags();
    return ArrayContains(itemTags, n"BlackwallForce");
  }

  public final const func GetPhoneCallFactName(contactName1: CName, contactName2: CName) -> String {
    let phoneSystem: ref<PhoneSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"PhoneSystem") as PhoneSystem;
    return phoneSystem.GetPhoneCallFactName(contactName1, contactName2);
  }

  private final func TriggerInspect(itemID: String, offset: Float, adsOffset: Float, timeToScan: Float) -> Void {
    let evt: ref<InspectionTriggerEvent>;
    let scanEvt: ref<ScanEvent>;
    GameInstance.GetTransactionSystem(this.GetGame()).AddItemToSlot(this, t"AttachmentSlots.Inspect", ItemID.FromTDBID(TDBID.Create(itemID)));
    scanEvt = new ScanEvent();
    scanEvt.isAvailable = true;
    scanEvt.clue = itemID;
    this.QueueEvent(scanEvt);
    evt = new InspectionTriggerEvent();
    evt.item = itemID;
    evt.offset = offset;
    evt.adsOffset = adsOffset;
    evt.timeToScan = timeToScan;
    this.QueueEvent(evt);
  }

  public final func SetInvisible(isInvisible: Bool) -> Void {
    this.m_visibleObjectComponent.Toggle(!isInvisible);
  }

  public final func IsInvisible() -> Bool {
    return !this.m_visibleObjectComponent.IsEnabled();
  }

  protected cb func OnHeavyFootstepEvent(evt: ref<HeavyFootstepEvent>) -> Bool {
    this.PlayFootstepCameraShakeBasedOnProximity(evt);
  }

  private final func PlayFootstepCameraShakeBasedOnProximity(evt: ref<HeavyFootstepEvent>) -> Void {
    let distanceBasedShake: Bool;
    let distanceToNPC: Float;
    let rumbleName: CName;
    let shakeStrength: Float;
    let rumbleIntensityPrefix: String = "light_";
    let rumbleDuration: String = "pulse";
    let footstepStylePrefix: String = "footstepWalk";
    let minFootstepDistanceThreshold: Float = TDB.GetFloat(t"player.onFootstepRumble.minFootstepDistanceThreshold");
    let medFootstepDistanceThreshold: Float = TDB.GetFloat(t"player.onFootstepRumble.medFootstepDistanceThreshold");
    let maxFootstepDistanceThreshold: Float = TDB.GetFloat(t"player.onFootstepRumble.maxFootstepDistanceThreshold");
    switch evt.audioEventName {
      case n"nme_boss_smasher_lcm_walk":
        rumbleIntensityPrefix = "light_";
        footstepStylePrefix = "footstepWalk";
        distanceBasedShake = true;
        break;
      case n"nme_boss_smasher_lcm_sprint":
        rumbleIntensityPrefix = "medium_";
        footstepStylePrefix = "footstepSprint";
        distanceBasedShake = true;
        break;
      case n"enm_mech_minotaur_loco_fs_heavy":
        rumbleIntensityPrefix = "medium_";
        footstepStylePrefix = "footstepWalk";
        distanceBasedShake = true;
        break;
      case n"lcm_npc_exo_":
        rumbleIntensityPrefix = "medium_";
        footstepStylePrefix = "footstepWalk";
        distanceBasedShake = true;
        break;
      case n"q302_sc_02_chimera_attack_land":
      case n"q302_sc_02_chimera_melee_front_stomp":
      case n"q302_sc_02_chimera_melee_stomp":
        rumbleIntensityPrefix = "heavy_";
        footstepStylePrefix = "footstepWalk";
        distanceBasedShake = false;
        shakeStrength = TDB.GetFloat(TDBID.Create("player.cameraShake." + footstepStylePrefix + "High"));
    };
    if distanceBasedShake {
      distanceToNPC = Vector4.Distance2D(evt.instigator.GetWorldPosition(), this.GetWorldPosition());
      if distanceToNPC > maxFootstepDistanceThreshold {
        return;
      };
      if distanceToNPC >= medFootstepDistanceThreshold {
        shakeStrength = TDB.GetFloat(TDBID.Create("player.cameraShake." + footstepStylePrefix + "Med"));
      } else {
        if distanceToNPC >= minFootstepDistanceThreshold {
          shakeStrength = TDB.GetFloat(TDBID.Create("player.cameraShake." + footstepStylePrefix + "High"));
        } else {
          shakeStrength = TDB.GetFloat(TDBID.Create("player.cameraShake." + footstepStylePrefix + "Low"));
        };
      };
    };
    rumbleName = TDB.GetCName(TDBID.Create("rumble.local." + rumbleIntensityPrefix + rumbleDuration));
    GameObject.PlaySound(this, rumbleName);
    this.SendCameraShakeDataToGraph(shakeStrength);
  }

  public final func UpdateVisibility() -> Void {
    let shouldUseCombatVisibility: Bool = false;
    let shouldCoverModiferBeActive: Bool = this.m_behindCover && !this.m_coverVisibilityPerkBlocked && !this.m_inCombat;
    shouldUseCombatVisibility = this.m_coverVisibilityPerkBlocked || this.m_inCombat;
    this.EnableCombatVisibilityDistances(shouldUseCombatVisibility);
    if shouldCoverModiferBeActive && !this.m_coverModifierActive {
      this.m_coverModifierActive = GameInstance.GetStatsSystem(this.GetGame()).ApplyModifierGroup(Cast<StatsObjectID>(this.GetEntityID()), TDBID.ToNumber(this.m_coverRecordID));
    } else {
      if !shouldCoverModiferBeActive && this.m_coverModifierActive {
        GameInstance.GetStatsSystem(this.GetGame()).RemoveModifierGroup(Cast<StatsObjectID>(this.GetEntityID()), TDBID.ToNumber(this.m_coverRecordID));
        this.m_coverModifierActive = false;
      };
    };
  }

  private final func UpdateSecondaryVisibilityOffset(isCrouching: Bool) -> Void {
    let objectOffsetEvent: ref<VisibleObjectSecondaryPositionEvent> = new VisibleObjectSecondaryPositionEvent();
    objectOffsetEvent.offset.X = 0.00;
    objectOffsetEvent.offset.Y = 0.00;
    objectOffsetEvent.offset.Z = isCrouching ? TweakDBInterface.GetFloat(t"player.stealth.crouchVisibilityZOffset", 0.60) : TweakDBInterface.GetFloat(t"player.stealth.chestVisibilityZOffset", 1.30);
    this.QueueEvent(objectOffsetEvent);
  }

  private final func EnableCombatVisibilityDistances(enable: Bool) -> Void {
    let objectDistanceEvent: ref<VisibleObjectDistanceEvent> = new VisibleObjectDistanceEvent();
    let objectSecondaryDistanceEvent: ref<VisibleObjectetSecondaryDistanceEvent> = new VisibleObjectetSecondaryDistanceEvent();
    let nearDistance: Float = TweakDBInterface.GetFloat(t"player.stealth.nearVisibilityDistance", 200.00);
    let farDistance: Float = TweakDBInterface.GetFloat(t"player.stealth.farVisibilityDistance", 5.00);
    let extraEvalDistance: Float = TweakDBInterface.GetFloat(t"player.stealth.extraEvalVisibilityDistance", 3.00);
    objectDistanceEvent.distance = enable ? farDistance : nearDistance;
    objectSecondaryDistanceEvent.distance = enable ? nearDistance : farDistance;
    objectSecondaryDistanceEvent.extraEvaluationDistance = enable ? extraEvalDistance : 0.00;
    this.QueueEvent(objectDistanceEvent);
    this.QueueEvent(objectSecondaryDistanceEvent);
  }

  protected cb func OnZoomLevelChange(newLevel: Float) -> Bool {
    this.UpdateAimAssist();
  }

  protected cb func OnLocomotionStateChanged(newState: Int32) -> Bool {
    this.m_locomotionState = newState;
    let isCrouching: Bool = newState == 1;
    if NotEquals(this.m_inCrouch, isCrouching) {
      this.UpdateSecondaryVisibilityOffset(isCrouching);
      this.m_inCrouch = isCrouching;
    };
    this.UpdateAimAssist();
  }

  protected cb func OnCombatStateChanged(newState: Int32) -> Bool {
    let bboard: ref<IBlackboard>;
    let combatTimeStamp: Float;
    let psmEvent: ref<PSMPostponedParameterBool>;
    let inCombat: Bool = newState == 1;
    if NotEquals(inCombat, this.m_inCombat) {
      if !inCombat {
        this.GetPS().SetCombatExitTimestamp(EngineTime.ToFloat(GameInstance.GetTimeSystem(this.GetGame()).GetSimTime()));
      };
      this.m_inCombat = inCombat;
      this.UpdateVisibility();
      if !this.m_inCombat {
        this.m_hasBeenDetected = false;
      } else {
        this.SetIsBeingRevealed(false);
        this.GetPlayerPerkDataBlackboard().SetUint(GetAllBlackboardDefs().PlayerPerkData.EntityNoticedPlayer, 0u);
        bboard = this.GetPlayerPerkDataBlackboard();
        combatTimeStamp = EngineTime.ToFloat(GameInstance.GetSimTime(this.GetGame()));
        bboard.SetFloat(GetAllBlackboardDefs().PlayerPerkData.CombatStateTime, combatTimeStamp);
      };
      psmEvent = new PSMPostponedParameterBool();
      psmEvent.id = n"ForceDisableToggleWalk";
      psmEvent.aspect = gamestateMachineParameterAspect.Permanent;
      psmEvent.value = true;
      this.QueueEvent(psmEvent);
      GameInstance.GetPlayerSystem(this.GetGame()).PlayerEnteredCombat(this.m_inCombat);
    };
    if inCombat {
      (this.GetTargetTrackerComponent() as TargetTrackingExtension).RemoveHostileCamerasFromThreats();
      this.GetSensorObjectComponent().RemoveForcedSensesTracing(gamedataSenseObjectType.Camera, EAIAttitude.AIA_Hostile);
    } else {
      this.GetSensorObjectComponent().SetForcedSensesTracing(gamedataSenseObjectType.Camera, EAIAttitude.AIA_Hostile);
    };
  }

  public final func OnStatChanged(ownerID: StatsObjectID, statType: gamedataStatType, diff: Float, total: Float) -> Void {
    if Equals(statType, gamedataStatType.HasKiroshiOpticsFragment) {
      this.m_hasKiroshiOpticsFragment = total > 0.00;
      this.UpdateAimAssist();
    } else {
      if Equals(statType, gamedataStatType.ZoomLevel) {
        this.UpdateAimAssist();
      };
    };
  }

  public final func UpdateWeaponRightEquippedItemInfo() -> Void {
    this.m_equippedRightHandWeapon = GameInstance.GetTransactionSystem(this.GetGame()).GetItemInSlot(this, t"AttachmentSlots.WeaponRight") as WeaponObject;
  }

  public final func OnItemEquipped(slot: TweakDBID, item: ItemID) -> Void {
    if slot == t"AttachmentSlots.WeaponRight" {
      this.UpdateWeaponRightEquippedItemInfo();
      this.UpdateAimAssist();
    };
  }

  public final func OnItemUnequipped(slot: TweakDBID, item: ItemID) -> Void {
    if slot == t"AttachmentSlots.WeaponRight" {
      this.m_handlingModifiers.RemoveHandlingModifiersFromWeapon(this.m_equippedRightHandWeapon);
      this.m_equippedRightHandWeapon = null;
      this.UpdateAimAssist();
    };
  }

  protected cb func OnLeftHandCyberwareStateChange(newState: Int32) -> Bool {
    this.m_leftHandCyberwareState = newState;
    this.UpdateAimAssist();
  }

  protected cb func OnVisionStateChange(newState: Int32) -> Bool {
    let focus: Bool = newState == 1;
    if NotEquals(focus, this.m_focusModeActive) {
      this.m_focusModeActive = focus;
      this.UpdateAimAssist();
    };
  }

  protected cb func OnMeleeWeaponStateChange(newState: Int32) -> Bool {
    this.m_meleeWeaponState = newState;
    this.UpdateAimAssist();
  }

  protected cb func OnBodySlamStateChange(newState: Bool) -> Bool {
    this.m_isInBodySlam = newState;
    this.UpdateAimAssist();
  }

  protected cb func OnUpperBodyStateChange(newState: Int32) -> Bool {
    let isAiming: Bool = newState == 6;
    if NotEquals(isAiming, this.m_isAiming) {
      this.m_isAiming = isAiming;
      this.UpdateAimAssist();
    };
    this.m_pocketRadio.HandleRestriction(PocketRadioRestrictions.UpperBodyState, newState == 5);
  }

  protected cb func OnCombatGadgetStateChange(newState: Int32) -> Bool {
    this.m_combatGadgetState = newState;
    this.UpdateAimAssist();
  }

  protected cb func OnSceneTierChange(newState: Int32) -> Bool {
    this.m_sceneTier = IntEnum<GameplayTier>(newState);
    this.m_pocketRadio.HandleRestriction(PocketRadioRestrictions.SceneTier, EnumInt(this.m_sceneTier) >= 2);
  }

  protected cb func OnPlayerTakedownStateChange(takedownState: Int32) -> Bool {
    let grappledEnemy: ref<GameObject>;
    let grappledEnemyID: EntityID;
    let workspotSystem: ref<WorkspotGameSystem>;
    let takedownStateEnum: gamePSMTakedown = IntEnum<gamePSMTakedown>(takedownState);
    if NotEquals(takedownStateEnum, gamePSMTakedown.EnteringGrapple) {
      return true;
    };
    workspotSystem = GameInstance.GetWorkspotSystem(this.GetGame());
    if !IsDefined(workspotSystem) {
      return true;
    };
    grappledEnemyID = this.GetPlayerStateMachineBlackboard().GetEntityID(GetAllBlackboardDefs().PlayerStateMachine.TakedownTargetID);
    grappledEnemy = GameInstance.FindEntityByID(this.GetGame(), grappledEnemyID) as GameObject;
    if !IsDefined(grappledEnemy) {
      return true;
    };
    if workspotSystem.IsActorInWorkspot(grappledEnemy) {
      workspotSystem.StopNpcInWorkspot(grappledEnemy);
    };
  }

  public final func OnEnterAimState() -> Void {
    if !this.m_isAiming {
      this.m_isAiming = true;
      this.UpdateAimAssistImmediate();
    };
  }

  protected cb func OnVehicleStateChange(newState: Int32) -> Bool {
    let recordID: TweakDBID;
    let vehicle: wref<VehicleObject>;
    let vehicleRecord: ref<Vehicle_Record>;
    this.m_inMountedWeaponVehicle = false;
    if NotEquals(this.m_vehicleState, IntEnum<gamePSMVehicle>(newState)) {
      this.m_vehicleState = IntEnum<gamePSMVehicle>(newState);
      this.UpdateAimAssist();
      if VehicleComponent.GetVehicle(this.GetGame(), this.GetEntityID(), vehicle) {
        recordID = vehicle.GetRecordID();
        vehicleRecord = TweakDBInterface.GetVehicleRecord(recordID);
        if IsDefined(vehicleRecord.VehDataPackage()) {
          if Equals(vehicleRecord.VehDataPackage().DriverCombat().Type(), gamedataDriverCombatType.Doors) {
            this.HandleDoorsForCombat(vehicle);
          } else {
            if Equals(vehicleRecord.VehDataPackage().DriverCombat().Type(), gamedataDriverCombatType.MountedWeapons) {
              this.m_inMountedWeaponVehicle = true;
            };
          };
        };
      };
    };
  }

  private final func HandleDoorsForCombat(vehicle: wref<VehicleObject>) -> Void {
    let ignoreAutoDoorCloseEvt: ref<SetIgnoreAutoDoorCloseEvent> = new SetIgnoreAutoDoorCloseEvent();
    if Equals(this.m_vehicleState, gamePSMVehicle.DriverCombat) {
      ignoreAutoDoorCloseEvt.set = true;
      vehicle.QueueEvent(ignoreAutoDoorCloseEvt);
      VehicleComponent.OpenDoor(vehicle, VehicleComponent.GetDriverSlotID());
      VehicleComponent.OpenDoor(vehicle, VehicleComponent.GetFrontPassengerSlotID());
    } else {
      if NotEquals(this.m_vehicleState, gamePSMVehicle.Transition) && NotEquals(this.m_vehicleState, gamePSMVehicle.Passenger) {
        ignoreAutoDoorCloseEvt.set = false;
        vehicle.QueueEvent(ignoreAutoDoorCloseEvt);
        VehicleComponent.CloseDoor(vehicle, VehicleComponent.GetDriverSlotID());
        VehicleComponent.CloseDoor(vehicle, VehicleComponent.GetFrontPassengerSlotID());
      };
    };
  }

  protected cb func OnDriverCombatCameraChange(inTPP: Bool) -> Bool {
    if NotEquals(this.m_inDriverCombatTPP, inTPP) {
      this.m_inDriverCombatTPP = inTPP;
      this.UpdateAimAssist();
    };
  }

  protected cb func OnDriverCombatWeaponTypeChange(newWeaponType: Int32) -> Bool {
    this.m_driverCombatWeaponType = IntEnum<gamedataItemType>(newWeaponType);
    this.UpdateAimAssist();
  }

  protected cb func OnWeaponStateChange(newState: Int32) -> Bool {
    let quickMelee: Bool = newState == 3;
    if NotEquals(quickMelee, this.m_doingQuickMelee) {
      this.m_doingQuickMelee = quickMelee;
      this.UpdateAimAssist();
    };
  }

  protected cb func OnControllingDeviceChange(controllingId: EntityID) -> Bool {
    this.m_controllingDeviceID = controllingId;
    this.UpdateAimAssist();
  }

  protected cb func OnNumberOfCombatantsChanged(value: Uint32) -> Bool {
    if this.m_numberOfCombatants == 0 && value > 0u && !this.m_hasBeenDetected {
      this.m_hasBeenDetected = true;
    };
    this.m_numberOfCombatants = Cast<Int32>(value);
    GameInstance.GetTelemetrySystem(this.GetGame()).LogNumberOfCombatants(this.m_numberOfCombatants);
  }

  protected cb func OnPlayerCoverStatusChangedEvent(evt: ref<PlayerCoverStatusChangedEvent>) -> Bool {
    if NotEquals(this.m_behindCover, evt.fullyBehindCover) {
      this.m_behindCover = evt.fullyBehindCover;
      this.UpdateVisibility();
    };
  }

  protected cb func OnStatusEffectApplied(evt: ref<ApplyStatusEffectEvent>) -> Bool {
    let actionRestrictionRecord: ref<ActionRestrictionGroup_Record>;
    let bioMonitorBB: ref<IBlackboard>;
    let cooldowns: array<SPlayerCooldown>;
    let cwIconCooldown: Float;
    let enableVisibilityEvt: ref<EnablePlayerVisibilityEvent>;
    let enableVisiblityDelay: Float;
    let exitCombatDelay: Float;
    let gameplayTags: array<CName>;
    let hostileTarget: wref<GameObject>;
    let hostileTargetPuppet: wref<ScriptedPuppet>;
    let hostileTargets: array<TrackedLocation>;
    let i: Int32;
    let j: Int32;
    let modifier: ref<gameStatModifierData>;
    let newCooldown: SPlayerCooldown;
    let permaMod: ref<gameStatModifierData>;
    let psmEvent: ref<PSMPostponedParameterScriptable>;
    let restrictionRecord: ref<GameplayRestrictionStatusEffect_Record>;
    let statPoolsSystem: ref<StatPoolsSystem>;
    let uiBB: ref<IBlackboard>;
    let vanishEvt: ref<ExitCombatOnOpticalCamoActivatedEvent>;
    let weaponID: ItemID;
    let weaponStatsObjectID: StatsObjectID;
    if Equals(evt.staticData.StatusEffectType().Type(), gamedataStatusEffectType.Defeated) {
      StatusEffectHelper.RemoveStatusEffect(this, evt.staticData.GetID());
      return true;
    };
    if Equals(evt.staticData.StatusEffectType().Type(), gamedataStatusEffectType.UncontrolledMovement) {
      StatusEffectHelper.RemoveStatusEffect(this, evt.staticData.GetID());
      return true;
    };
    gameplayTags = evt.staticData.GameplayTags();
    if ArrayContains(gameplayTags, n"DoNotApplyOnPlayer") {
      StatusEffectHelper.RemoveStatusEffect(this, evt.staticData.GetID());
      return true;
    };
    psmEvent = new PSMPostponedParameterScriptable();
    psmEvent.id = n"StatusEffect";
    psmEvent.value = evt.staticData;
    this.QueueEvent(psmEvent);
    super.OnStatusEffectApplied(evt);
    if evt.staticData.GetID() == t"BaseStatusEffect.BlockCoverVisibilityReduction" {
      this.m_coverVisibilityPerkBlocked = true;
      this.UpdateVisibility();
    };
    if ArrayContains(gameplayTags, n"NoScanning") {
      this.m_visionModeController.UpdateNoScanningRestriction();
    };
    if ArrayContains(gameplayTags, n"GameplayRestriction") {
      PlayerGameplayRestrictions.OnGameplayRestrictionAdded(this, evt.staticData, gameplayTags);
      restrictionRecord = evt.staticData as GameplayRestrictionStatusEffect_Record;
      if IsDefined(restrictionRecord) && evt.isNewApplication {
        actionRestrictionRecord = restrictionRecord.ActionRestriction();
        if IsDefined(actionRestrictionRecord) {
          this.AddGameplayRestriction(this.GetPlayerStateMachineBlackboard(), actionRestrictionRecord.GetID());
        };
      };
    };
    if ArrayContains(gameplayTags, n"CameraAnimation") {
      if GameplaySettingsSystem.GetAdditiveCameraMovementsSetting(this) <= 0.00 {
        StatusEffectHelper.RemoveStatusEffectsWithTag(this, n"CameraAnimation");
      } else {
        if ArrayContains(gameplayTags, n"Breathing") || ArrayContains(gameplayTags, n"JohnnySickness") {
          this.ProcessBreathingEffectApplication(evt);
        } else {
          StatusEffectHelper.RemoveAllStatusEffectsWithTagBeside(this, n"CameraAnimation", evt.staticData.GetID());
        };
      };
    };
    if ArrayContains(gameplayTags, n"CyberspacePresence") {
      this.DisableFootstepAudio(true);
      this.DisableCameraBobbing(true);
    };
    if ArrayContains(gameplayTags, n"JugglerPerkRemoveKnifeCooldowns") {
      statPoolsSystem = GameInstance.GetStatPoolsSystem(this.GetGame());
      i = 0;
      while i < 3 {
        weaponID = this.GetEquippedItemIdInArea(gamedataEquipmentArea.Weapon, i);
        if Equals(RPGManager.GetWeaponEvolution(weaponID), gamedataWeaponEvolution.Throwable) {
          weaponStatsObjectID = Cast<StatsObjectID>(weaponID);
          statPoolsSystem.RequestSettingStatPoolMaxValue(weaponStatsObjectID, gamedataStatPoolType.ThrowRecovery, this);
        };
        i += 1;
      };
      if PlayerDevelopmentSystem.GetInstance(this).IsNewPerkBought(this, gamedataNewPerkType.Cool_Right_Perk_3_4) == 1 {
        StatusEffectHelper.ApplyStatusEffect(this, t"BaseStatusEffect.JugglerCriticalDamageBonusSE");
      };
    };
    if ArrayContains(gameplayTags, n"IconicPunkKnifeRemoveCooldown") {
      statPoolsSystem = GameInstance.GetStatPoolsSystem(this.GetGame());
      i = 0;
      while i < 3 {
        weaponID = this.GetEquippedItemIdInArea(gamedataEquipmentArea.Weapon, i);
        if GameInstance.GetTransactionSystem(this.GetGame()).GetItemData(this, weaponID).HasTag(n"BountyHunterIconicKnife") {
          weaponStatsObjectID = Cast<StatsObjectID>(weaponID);
          statPoolsSystem.RequestSettingStatPoolMaxValue(weaponStatsObjectID, gamedataStatPoolType.ThrowRecovery, this);
        };
        i += 1;
      };
    };
    if ArrayContains(gameplayTags, n"KurtKnifeRemoveCooldown") {
      statPoolsSystem = GameInstance.GetStatPoolsSystem(this.GetGame());
      i = 0;
      while i < 3 {
        weaponID = this.GetEquippedItemIdInArea(gamedataEquipmentArea.Weapon, i);
        if GameInstance.GetTransactionSystem(this.GetGame()).GetItemData(this, weaponID).HasTag(n"KurtIconicKnife") {
          weaponStatsObjectID = Cast<StatsObjectID>(weaponID);
          statPoolsSystem.RequestSettingStatPoolMaxValue(weaponStatsObjectID, gamedataStatPoolType.ThrowRecovery, this);
        };
        i += 1;
      };
    };
    if ArrayContains(gameplayTags, n"ThrowMod1RemoveCooldown") {
      statPoolsSystem = GameInstance.GetStatPoolsSystem(this.GetGame());
      i = 0;
      while i < 3 {
        weaponID = this.GetEquippedItemIdInArea(gamedataEquipmentArea.Weapon, i);
        if GameInstance.GetTransactionSystem(this.GetGame()).GetItemData(this, weaponID).GetStatValueByType(gamedataStatType.ThrowMod1_CanReturn) > 0.00 {
          weaponStatsObjectID = Cast<StatsObjectID>(weaponID);
          statPoolsSystem.RequestSettingStatPoolMaxValue(weaponStatsObjectID, gamedataStatPoolType.ThrowRecovery, this);
        };
        i += 1;
      };
    };
    if ArrayContains(gameplayTags, n"ActiveCamo") && RPGManager.HasStatFlag(this, gamedataStatType.CanPlayerExitCombatWithOpticalCamo) && !RPGManager.HasStatFlag(this, gamedataStatType.BlockOpticalCamoRelicPerk) && !StatusEffectHelper.HasStatusEffectWithTagConst(this, n"OpticalCamoSlideCoolPerk") && !StatusEffectHelper.HasStatusEffectWithTagConst(this, n"OpticalCamoGrapple") {
      exitCombatDelay = TweakDBInterface.GetFloat(t"Items.AdvancedOpticalCamoCommon.exitCombatDelay", 1.50);
      this.PromoteOpticalCamoEffectorToCompletelyBlocking();
      if this.m_inCombat {
        enableVisiblityDelay = GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.OpticalCamoDuration);
        this.SetInvisible(true);
        hostileTargets = this.GetTargetTrackerComponent().GetHostileThreats(false);
        j = 0;
        while j < ArraySize(hostileTargets) {
          hostileTarget = hostileTargets[j].entity as GameObject;
          hostileTargetPuppet = hostileTarget as ScriptedPuppet;
          if IsDefined(hostileTargetPuppet) {
            hostileTargetPuppet.GetTargetTrackerComponent().DeactivateThreat(this);
          };
          vanishEvt = new ExitCombatOnOpticalCamoActivatedEvent();
          vanishEvt.npc = hostileTarget;
          GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, vanishEvt, exitCombatDelay);
          j += 1;
        };
        enableVisibilityEvt = new EnablePlayerVisibilityEvent();
        GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, enableVisibilityEvt, enableVisiblityDelay);
      };
    };
    if ArrayContains(gameplayTags, n"CamoActiveOnPlayer") || ArrayContains(gameplayTags, n"OpticalCamoSlideCoolPerk") || ArrayContains(gameplayTags, n"OpticalCamoGrapple") {
      this.SetStatPoolEnabled(gamedataStatPoolType.OpticalCamoCharges, gameStatPoolModificationTypes.Decay, true);
      this.SetStatPoolEnabled(gamedataStatPoolType.OpticalCamoCharges, gameStatPoolModificationTypes.Regeneration, false);
    };
    if ArrayContains(gameplayTags, n"Overclock") {
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.CyberdeckOverclockStatValue);
      cwIconCooldown = GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.CyberdeckOverclockDuration);
      modifier = RPGManager.CreateStatModifier(gamedataStatType.CyberdeckOverclockStatValue, gameStatModifierType.Additive, cwIconCooldown) as gameConstantStatModifierData;
      GameInstance.GetStatsSystem(this.GetGame()).AddModifier(Cast<StatsObjectID>(this.GetEntityID()), modifier);
      this.SetStatPoolEnabled(gamedataStatPoolType.CyberdeckOverclock, gameStatPoolModificationTypes.Regeneration, false);
      this.SetStatPoolEnabled(gamedataStatPoolType.CyberdeckOverclock, gameStatPoolModificationTypes.Decay, true);
    };
    if ArrayContains(gameplayTags, n"PostManiac") {
      if PlayerDevelopmentSystem.GetInstance(this).IsNewPerkBought(this, gamedataNewPerkType.Tech_Inbetween_Left_3) >= 1 {
        ConsumablesChargesHelper.RefreshAmountOfCharges(this.GetGame(), gamedataStatPoolType.ProjectileLauncherCharges, 1);
      };
      ConsumablesChargesHelper.RefreshAmountOfCharges(this.GetGame(), gamedataStatPoolType.GrenadesCharges, 1);
    };
    if Equals(evt.staticData.StatusEffectType().Type(), gamedataStatusEffectType.PlayerCooldown) {
      bioMonitorBB = GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_PlayerBioMonitor);
      cooldowns = FromVariant<array<SPlayerCooldown>>(bioMonitorBB.GetVariant(GetAllBlackboardDefs().UI_PlayerBioMonitor.Cooldowns));
      newCooldown.effectID = evt.staticData.GetID();
      newCooldown.instigatorID = StatusEffectHelper.GetStatusEffectByID(this, evt.staticData.GetID()).GetInstigatorStaticDataID();
      ArrayPush(cooldowns, newCooldown);
      bioMonitorBB.SetVariant(GetAllBlackboardDefs().UI_PlayerBioMonitor.Cooldowns, ToVariant(cooldowns));
    };
    this.ProcessTieredDrunkEffect(evt);
    this.ProcessTieredDruggedEffect(evt);
    if ArrayContains(gameplayTags, n"Exhausted") {
      this.UpdateAimAssist();
    };
    if ArrayContains(gameplayTags, n"UsedHealingItemOrCyberwareStatusEffect") {
      this.OnStatusEffectUsedHealingItemOrCyberwareApplied();
    };
    if ArrayContains(gameplayTags, n"Berserk") {
      uiBB = GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UIGameData);
      uiBB.SetBool(GetAllBlackboardDefs().UIGameData.BerserkActive, true, true);
      GameInstance.GetUISystem(this.GetGame()).PushGameContext(UIGameContext.Berserk);
    };
    if ArrayContains(gameplayTags, n"FocusedCoolPerkSE") {
      if RPGManager.HasStatFlag(this, gamedataStatType.FocusedGrenadeShootingPerk) {
        this.ToggleFocusedGrenadeShootingPerk(true);
      };
    };
    if ArrayContains(gameplayTags, n"CWshard_Tier2") {
      permaMod = RPGManager.CreateStatModifier(gamedataStatType.Humanity, gameStatModifierType.Additive, 2.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(Cast<StatsObjectID>(this.GetEntityID()), permaMod);
    };
    if ArrayContains(gameplayTags, n"CWshard_Tier3") {
      permaMod = RPGManager.CreateStatModifier(gamedataStatType.Humanity, gameStatModifierType.Additive, 3.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(Cast<StatsObjectID>(this.GetEntityID()), permaMod);
    };
    if ArrayContains(gameplayTags, n"CWshard_Tier4") {
      permaMod = RPGManager.CreateStatModifier(gamedataStatType.Humanity, gameStatModifierType.Additive, 4.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(Cast<StatsObjectID>(this.GetEntityID()), permaMod);
    };
    if ArrayContains(gameplayTags, n"CWshard_Tier5") {
      permaMod = RPGManager.CreateStatModifier(gamedataStatType.Humanity, gameStatModifierType.Additive, 6.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(Cast<StatsObjectID>(this.GetEntityID()), permaMod);
    };
    if ArrayContains(gameplayTags, n"CarryShard") {
      permaMod = RPGManager.CreateStatModifier(gamedataStatType.CarryCapacity, gameStatModifierType.Additive, 2.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(Cast<StatsObjectID>(this.GetEntityID()), permaMod);
    };
    if ArrayContains(gameplayTags, n"CWshard_debug_lvl20") {
      permaMod = RPGManager.CreateStatModifier(gamedataStatType.Humanity, gameStatModifierType.Additive, 30.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(Cast<StatsObjectID>(this.GetEntityID()), permaMod);
    };
    if ArrayContains(gameplayTags, n"CWshard_debug_lvl30") {
      permaMod = RPGManager.CreateStatModifier(gamedataStatType.Humanity, gameStatModifierType.Additive, 54.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(Cast<StatsObjectID>(this.GetEntityID()), permaMod);
    };
    if ArrayContains(gameplayTags, n"VanillaIconicOSLooted") || ArrayContains(gameplayTags, n"AllVanillaIconicOSPurchased") {
      permaMod = RPGManager.CreateStatModifier(gamedataStatType.IconicCWFromTreasureChestLooted, gameStatModifierType.Additive, 1.00);
      GameInstance.GetStatsSystem(this.GetGame()).AddSavedModifier(Cast<StatsObjectID>(this.GetEntityID()), permaMod);
    };
    if ArrayContains(gameplayTags, n"CustomFastForward") {
      this.SetCustomFastForwardEnabled(true);
    };
    if ArrayContains(gameplayTags, n"PreventLowHealthOverlay") {
      this.UpdateHealthStateVFX(this.m_lastHealthUpdate);
    };
    if ArrayContains(gameplayTags, n"Body_Reward_Skillbook_Bought") {
      AddFact(this.GetGame(), n"body_skillbook_bought_counter", 1);
    };
    if ArrayContains(gameplayTags, n"Ref_Reward_Skillbook_Bought") {
      AddFact(this.GetGame(), n"ref_skillbook_bought_counter", 1);
    };
    if ArrayContains(gameplayTags, n"Int_Reward_Skillbook_Bought") {
      AddFact(this.GetGame(), n"int_skillbook_bought_counter", 1);
    };
    if ArrayContains(gameplayTags, n"Tech_Reward_Skillbook_Bought") {
      AddFact(this.GetGame(), n"tech_skillbook_bought_counter", 1);
    };
    if ArrayContains(gameplayTags, n"Cool_Reward_Skillbook_Bought") {
      AddFact(this.GetGame(), n"cool_skillbook_bought_counter", 1);
    };
    if ArrayContains(gameplayTags, n"ForcedQHUploadAwarenessBumps") {
      GameInstance.GetDelaySystem(this.GetGame()).DelayEventNextFrame(this, new RefreshQuickhackMenuEvent());
    };
    this.m_pocketRadio.OnStatusEffectApplied(evt, gameplayTags);
    this.m_combatController.OnStatusEffectApplied(evt, gameplayTags);
  }

  public final func DisableScanningFromInput() -> Void {
    this.m_visionModeController.ForceInputOff();
  }

  private final func SetCustomFastForwardEnabled(enabled: Bool) -> Void {
    if enabled {
      ScenesFastForwardTransition.DisplayFFButtonPrompt(this);
      GameInstance.GetUISystem(this.GetGame()).SetHudEntryForcedVisibility(n"input_hint", worlduiEntryVisibility.ForceShow);
      this.m_customFastForwardPossible = true;
    } else {
      ScenesFastForwardTransition.HideFFButtonPrompt(this);
      GameInstance.GetUISystem(this.GetGame()).SetHudEntryForcedVisibility(n"input_hint", worlduiEntryVisibility.TierVisibility);
      GameInstance.GetTimeSystem(this.GetGame()).UnsetTimeDilation(n"customFFTimeDilation");
      GameObjectEffectHelper.StopEffectEvent(this, n"transition_glitch_loop");
      this.m_customFastForwardPossible = false;
    };
  }

  protected final func SetStatPoolEnabled(statpool: gamedataStatPoolType, statpooltype: gameStatPoolModificationTypes, enabled: Bool) -> Void {
    let mod: StatPoolModifier;
    let statPoolsSystem: ref<StatPoolsSystem> = GameInstance.GetStatPoolsSystem(this.GetGame());
    let entityID: StatsObjectID = Cast<StatsObjectID>(this.GetEntityID());
    statPoolsSystem.GetModifier(entityID, statpool, statpooltype, mod);
    mod.enabled = enabled;
    statPoolsSystem.RequestSettingModifier(entityID, statpool, statpooltype, mod);
  }

  public final func GetEquippedItemIdInArea(equipArea: gamedataEquipmentArea, opt slot: Int32) -> ItemID {
    let equipmentSystem: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    return equipmentSystem.GetItemInEquipSlot(this, equipArea, slot);
  }

  private final func DisableFootstepAudio(b: Bool) -> Void {
    let audioEventName: CName = b ? n"disableFootsteps" : n"enableFootsteps";
    GameObject.PlaySoundEvent(this, audioEventName);
  }

  private final func DisableCameraBobbing(b: Bool) -> Void {
    AnimationControllerComponent.SetInputBool(this, n"disable_camera_bobbing", b);
  }

  public final func OnAdditiveCameraMovementsSettingChanged() -> Void {
    if GameplaySettingsSystem.GetAdditiveCameraMovementsSetting(this) <= 0.00 {
      StatusEffectHelper.RemoveStatusEffectsWithTag(this, n"CameraAnimation");
    } else {
      PlayerPuppet.ReevaluateAllBreathingEffects(this);
    };
  }

  public final static func ReevaluateAllBreathingEffects(player: wref<PlayerPuppet>) -> Void {
    if !PlayerPuppet.CanApplyBreathingEffect(player) {
      StatusEffectHelper.RemoveStatusEffectsWithTag(player, n"Breathing");
    } else {
      if StatusEffectSystem.ObjectHasStatusEffect(player, t"BaseStatusEffect.BreathingMedium") || StatusEffectSystem.ObjectHasStatusEffect(player, t"BaseStatusEffect.BreathingHeavy") || StatusEffectSystem.ObjectHasStatusEffect(player, t"BaseStatusEffect.BreathingSick") || PlayerPuppet.IsJohnnySicknessBreathingEffectActive(player) {
        return;
      };
      if GameplaySettingsSystem.GetAdditiveCameraMovementsSetting(player) <= 0.50 {
        StatusEffectHelper.RemoveStatusEffect(player, t"BaseStatusEffect.BreathingLow");
      } else {
        StatusEffectHelper.ApplyStatusEffect(player, t"BaseStatusEffect.BreathingLow");
      };
    };
  }

  public final static func CanApplyBreathingEffect(player: wref<PlayerPuppet>) -> Bool {
    let blackboard: ref<IBlackboard>;
    if !IsDefined(player) {
      return false;
    };
    if GameplaySettingsSystem.GetAdditiveCameraMovementsSetting(player) <= 0.00 {
      return false;
    };
    if !ScriptedPuppet.IsActive(player) {
      return false;
    };
    blackboard = player.GetPlayerStateMachineBlackboard();
    if !IsDefined(blackboard) {
      return false;
    };
    if blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Combat) == 1 && !PlayerPuppet.IsJohnnySicknessBreathingEffectActive(player) {
      return false;
    };
    if blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Swimming) != 0 && !PlayerPuppet.IsJohnnySicknessBreathingEffectActive(player) {
      return false;
    };
    if VehicleComponent.IsMountedToVehicle(player.GetGame(), player) {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(player, n"CameraShake") {
      return false;
    };
    return true;
  }

  public final static func IsSwimming(player: wref<PlayerPuppet>) -> Bool {
    let blackboard: ref<IBlackboard>;
    if !IsDefined(player) {
      return false;
    };
    blackboard = player.GetPlayerStateMachineBlackboard();
    return blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Swimming) != 0;
  }

  public final static func GetSceneTier(player: wref<PlayerPuppet>) -> Int32 {
    let psmBlackboard: ref<IBlackboard>;
    if !IsDefined(player) {
      return 0;
    };
    psmBlackboard = player.GetPlayerStateMachineBlackboard();
    if IsDefined(psmBlackboard) {
      return psmBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.HighLevel);
    };
    return 0;
  }

  public final static func IsJohnnySicknessBreathingEffectActive(player: wref<PlayerPuppet>) -> Bool {
    if StatusEffectSystem.ObjectHasStatusEffect(player, t"BaseStatusEffect.JohnnySicknessLow") || StatusEffectSystem.ObjectHasStatusEffect(player, t"BaseStatusEffect.JohnnySicknessMedium") || StatusEffectSystem.ObjectHasStatusEffect(player, t"BaseStatusEffect.JohnnySicknessHeavy") || StatusEffectSystem.ObjectHasStatusEffect(player, t"BaseStatusEffect.JohnnySicknessMediumQuest") {
      return true;
    };
    return false;
  }

  private final func ProcessBreathingEffectApplication(evt: ref<StatusEffectEvent>) -> Void {
    let gameplayTags: array<CName> = evt.staticData.GameplayTags();
    if ArrayContains(gameplayTags, n"Breathing") {
      if PlayerPuppet.CanApplyBreathingEffect(this) {
        switch evt.staticData.GetID() {
          case t"BaseStatusEffect.BreathingLow":
            if GameplaySettingsSystem.GetAdditiveCameraMovementsSetting(this) <= 0.50 {
              StatusEffectHelper.RemoveStatusEffect(this, t"BaseStatusEffect.BreathingLow");
            };
            StatusEffectHelper.RemoveAllStatusEffectsWithTagBeside(this, n"Breathing", t"BaseStatusEffect.BreathingLow");
            break;
          default:
            StatusEffectHelper.RemoveAllStatusEffectsWithTagBeside(this, n"Breathing", evt.staticData.GetID());
        };
      } else {
        StatusEffectHelper.RemoveStatusEffectsWithTag(this, n"Breathing");
      };
    } else {
      if PlayerPuppet.CanApplyBreathingEffect(this) {
        StatusEffectHelper.RemoveStatusEffectsWithTag(this, n"Breathing");
        StatusEffectHelper.RemoveAllStatusEffectsWithTagBeside(this, n"JohnnySickness", evt.staticData.GetID());
      };
    };
  }

  private final func ProcessTieredDrunkEffect(evt: ref<StatusEffectEvent>) -> Void {
    let stackCount: Int32;
    let drunkID: TweakDBID = t"BaseStatusEffect.Drunk";
    if evt.staticData.GetID() == drunkID {
      stackCount = Cast<Int32>(StatusEffectHelper.GetStatusEffectByID(this, drunkID).GetStackCount());
      GameObjectEffectHelper.BreakEffectLoopEvent(this, n"status_drunk_level_1");
      GameObjectEffectHelper.BreakEffectLoopEvent(this, n"status_drunk_level_2");
      GameObjectEffectHelper.BreakEffectLoopEvent(this, n"status_drunk_level_3");
      GameObject.SetAudioParameter(this, n"vfx_fullscreen_drunk_level", 0.00);
      switch stackCount {
        case 1:
          GameObjectEffectHelper.StartEffectEvent(this, n"status_drunk_level_1");
          GameObject.SetAudioParameter(this, n"vfx_fullscreen_drunk_level", 1.00);
          break;
        case 2:
          GameObjectEffectHelper.StartEffectEvent(this, n"status_drunk_level_2");
          GameObject.SetAudioParameter(this, n"vfx_fullscreen_drunk_level", 2.00);
          break;
        case 4:
        case 3:
          GameObjectEffectHelper.StartEffectEvent(this, n"status_drunk_level_3");
          GameObject.SetAudioParameter(this, n"vfx_fullscreen_drunk_level", 3.00);
      };
    };
  }

  private final func ProcessTieredDruggedEffect(evt: ref<StatusEffectEvent>) -> Void {
    let stackCount: Int32;
    let druggedID: TweakDBID = t"BaseStatusEffect.Drugged";
    if evt.staticData.GetID() == druggedID {
      stackCount = Cast<Int32>(StatusEffectHelper.GetStatusEffectByID(this, druggedID).GetStackCount());
      GameObjectEffectHelper.BreakEffectLoopEvent(this, n"status_drugged_low");
      GameObjectEffectHelper.BreakEffectLoopEvent(this, n"status_drugged_medium");
      GameObjectEffectHelper.BreakEffectLoopEvent(this, n"status_drugged_heavy");
      GameObject.SetAudioParameter(this, n"vfx_fullscreen_drugged_level", 0.00);
      switch stackCount {
        case 1:
          GameObjectEffectHelper.StartEffectEvent(this, n"status_drugged_low");
          GameObject.SetAudioParameter(this, n"vfx_fullscreen_drugged_level", 1.00);
          break;
        case 2:
          GameObjectEffectHelper.StartEffectEvent(this, n"status_drugged_medium");
          GameObject.SetAudioParameter(this, n"vfx_fullscreen_drugged_level", 2.00);
          break;
        case 3:
          GameObjectEffectHelper.StartEffectEvent(this, n"status_drugged_heavy");
          GameObject.SetAudioParameter(this, n"vfx_fullscreen_drugged_level", 3.00);
      };
    };
  }

  private final func OnStatusEffectUsedHealingItemOrCyberwareApplied() -> Void {
    let entityID: StatsObjectID;
    let healthMax: Float;
    let overshieldAmount: Float;
    let overshieldPercentage: Float;
    let statPoolsSystem: ref<StatPoolsSystem>;
    this.GetPlayerPerkDataBlackboard().SetUint(GetAllBlackboardDefs().PlayerPerkData.UsedHealingItemOrCyberware, this.GetPlayerPerkDataBlackboard().GetUint(GetAllBlackboardDefs().PlayerPerkData.UsedHealingItemOrCyberware) + 1u);
    if PlayerDevelopmentSystem.GetInstance(this).IsNewPerkBought(this, gamedataNewPerkType.Body_Central_Milestone_3) < 3 {
      return;
    };
    entityID = Cast<StatsObjectID>(this.GetEntityID());
    statPoolsSystem = GameInstance.GetStatPoolsSystem(this.GetGame());
    healthMax = statPoolsSystem.GetStatPoolMaxPointValue(entityID, gamedataStatPoolType.Health);
    overshieldAmount = statPoolsSystem.GetStatPoolValue(entityID, gamedataStatPoolType.Overshield, false);
    overshieldPercentage = TweakDBInterface.GetFloat(t"NewPerks.Body_Central_Milestone_3.overshieldPercentage", 0.30);
    overshieldAmount += healthMax * overshieldPercentage;
    statPoolsSystem.RequestSettingStatPoolValue(entityID, gamedataStatPoolType.Overshield, overshieldAmount, this, false);
  }

  protected cb func OnNewPerkSold(evt: ref<NewPerkSoldEvent>) -> Bool {
    if Equals(evt.perkType, gamedataNewPerkType.Body_Central_Milestone_3) && evt.perkLevelSold == 3 {
      GameInstance.GetStatPoolsSystem(this.GetGame()).RequestSettingStatPoolValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.Overshield, 0.00, this);
    };
  }

  protected cb func OnStatusEffectRemoved(evt: ref<RemoveStatusEffect>) -> Bool {
    let actionRestrictionRecord: ref<ActionRestrictionGroup_Record>;
    let bioMonitorBB: ref<IBlackboard>;
    let cooldowns: array<SPlayerCooldown>;
    let cwIconCooldown: Float;
    let emptyID: EntityID;
    let focusEventUI: ref<FocusPerkTriggerd>;
    let gameplayTags: array<CName>;
    let i: Int32;
    let modifier: ref<gameStatModifierData>;
    let perkIsPurchased: Bool;
    let restrictionRecord: ref<GameplayRestrictionStatusEffect_Record>;
    let uiBB: ref<IBlackboard>;
    let psmEvent: ref<PSMPostponedParameterScriptable> = new PSMPostponedParameterScriptable();
    psmEvent.id = n"StatusEffectRemoved";
    psmEvent.value = evt.staticData;
    this.QueueEvent(psmEvent);
    super.OnStatusEffectRemoved(evt);
    if evt.staticData.GetID() == t"BaseStatusEffect.BlockCoverVisibilityReduction" {
      this.m_coverVisibilityPerkBlocked = false;
      this.UpdateVisibility();
    };
    gameplayTags = evt.staticData.GameplayTags();
    if ArrayContains(gameplayTags, n"NPCQuickhack") && EntityID.IsDefined(this.m_attackingNetrunnerID) {
      this.m_attackingNetrunnerID = emptyID;
    };
    if ArrayContains(gameplayTags, n"NoScanning") {
      this.m_visionModeController.UpdateNoScanningRestriction();
    };
    if ArrayContains(gameplayTags, n"GameplayRestriction") {
      PlayerGameplayRestrictions.OnGameplayRestrictionRemoved(this, evt, gameplayTags);
      restrictionRecord = evt.staticData as GameplayRestrictionStatusEffect_Record;
      if IsDefined(restrictionRecord) {
        actionRestrictionRecord = restrictionRecord.ActionRestriction();
        if IsDefined(actionRestrictionRecord) {
          this.RemoveGameplayRestriction(this.GetPlayerStateMachineBlackboard(), actionRestrictionRecord.GetID());
        };
      };
    };
    if ArrayContains(gameplayTags, n"CameraAnimation") && evt.staticData.GetID() != t"BaseStatusEffect.BreathingLow" && !StatusEffectSystem.ObjectHasStatusEffectWithTag(this, n"CameraShake") {
      PlayerPuppet.ReevaluateAllBreathingEffects(this);
    };
    if ArrayContains(gameplayTags, n"CyberspacePresence") {
      this.DisableFootstepAudio(false);
      this.DisableCameraBobbing(false);
    };
    if ArrayContains(gameplayTags, n"FocusedCoolPerkSE") {
      if GameInstance.GetTimeSystem(this.GetGame()).IsTimeDilationActive(n"focusedStatePerkDilation") {
        GameInstance.GetTimeSystem(this.GetGame()).UnsetTimeDilation(n"focusedStatePerkDilation", n"MeleeHitEaseOut");
      };
      GameObject.PlaySoundEvent(this, n"time_dilation_focused_exit");
      GameObjectEffectHelper.BreakEffectLoopEvent(this, n"cool_perk_focused_state_fullscreen");
      focusEventUI = new FocusPerkTriggerd();
      focusEventUI.isActive = false;
      this.QueueEvent(focusEventUI);
      perkIsPurchased = RPGManager.HasStatFlag(this, gamedataStatType.FocusedGrenadeShootingPerk);
      if perkIsPurchased {
        this.ToggleFocusedGrenadeShootingPerk(false);
      };
    };
    if Equals(evt.staticData.StatusEffectType().Type(), gamedataStatusEffectType.PlayerCooldown) {
      bioMonitorBB = GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_PlayerBioMonitor);
      cooldowns = FromVariant<array<SPlayerCooldown>>(bioMonitorBB.GetVariant(GetAllBlackboardDefs().UI_PlayerBioMonitor.Cooldowns));
      i = 0;
      while i < ArraySize(cooldowns) {
        if cooldowns[i].effectID == evt.staticData.GetID() {
          ArrayErase(cooldowns, i);
          break;
        };
        i += 1;
      };
      bioMonitorBB.SetVariant(GetAllBlackboardDefs().UI_PlayerBioMonitor.Cooldowns, ToVariant(cooldowns));
    };
    if ArrayContains(gameplayTags, n"Exhausted") {
      this.UpdateAimAssist();
    };
    if ArrayContains(gameplayTags, n"CamoActiveOnPlayer") || ArrayContains(gameplayTags, n"OpticalCamoSlideCoolPerk") || ArrayContains(gameplayTags, n"OpticalCamoGrapple") {
      this.SetStatPoolEnabled(gamedataStatPoolType.OpticalCamoCharges, gameStatPoolModificationTypes.Regeneration, true);
      this.SetStatPoolEnabled(gamedataStatPoolType.OpticalCamoCharges, gameStatPoolModificationTypes.Decay, false);
    };
    if ArrayContains(gameplayTags, n"Overclock") {
      GameInstance.GetStatsSystem(this.GetGame()).RemoveAllModifiers(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.CyberdeckOverclockStatValue);
      cwIconCooldown = GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.CyberdeckOverclockCooldown);
      modifier = RPGManager.CreateStatModifier(gamedataStatType.CyberdeckOverclockStatValue, gameStatModifierType.Additive, cwIconCooldown) as gameConstantStatModifierData;
      GameInstance.GetStatsSystem(this.GetGame()).AddModifier(Cast<StatsObjectID>(this.GetEntityID()), modifier);
      this.SetStatPoolEnabled(gamedataStatPoolType.CyberdeckOverclock, gameStatPoolModificationTypes.Regeneration, true);
      this.SetStatPoolEnabled(gamedataStatPoolType.CyberdeckOverclock, gameStatPoolModificationTypes.Decay, false);
    };
    this.ProcessTieredDrunkEffect(evt);
    this.ProcessTieredDruggedEffect(evt);
    if ArrayContains(gameplayTags, n"Berserk") {
      uiBB = GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UIGameData);
      uiBB.SetBool(GetAllBlackboardDefs().UIGameData.BerserkActive, false, true);
      GameInstance.GetUISystem(this.GetGame()).PopGameContext(UIGameContext.Berserk, true);
    };
    if ArrayContains(gameplayTags, n"CustomFastForward") {
      this.SetCustomFastForwardEnabled(false);
    };
    if ArrayContains(gameplayTags, n"PreventLowHealthOverlay") {
      this.UpdateHealthStateVFX(this.m_lastHealthUpdate);
    };
    this.m_pocketRadio.OnStatusEffectRemoved(evt, gameplayTags);
    this.m_combatController.OnStatusEffectRemoved(evt, gameplayTags);
  }

  protected cb func OnUpdateEquippedWeaponsHandling(evt: ref<UpdateEquippedWeaponsHandlingEvent>) -> Bool {
    if IsDefined(this.m_equippedRightHandWeapon) {
      this.m_handlingModifiers.UpdateEquippedWeaponsHandling(evt, this.m_equippedRightHandWeapon);
    };
  }

  public final func RegisterFriendlyDeviceHostileToEnemies(deviceID: EntityID) -> Void {
    RWLock.Acquire(this.m_friendlyDevicesHostileToEnemiesLock);
    if !ArrayContains(this.m_friendlyDevicesHostileToEnemies, deviceID) {
      ArrayPush(this.m_friendlyDevicesHostileToEnemies, deviceID);
    };
    RWLock.Release(this.m_friendlyDevicesHostileToEnemiesLock);
  }

  public final func UnregisterFriendlyDeviceHostileToEnemies(deviceID: EntityID) -> Void {
    RWLock.Acquire(this.m_friendlyDevicesHostileToEnemiesLock);
    ArrayRemove(this.m_friendlyDevicesHostileToEnemies, deviceID);
    RWLock.Release(this.m_friendlyDevicesHostileToEnemiesLock);
  }

  protected cb func OnAttitudeChanged(evt: ref<AttitudeChangedEvent>) -> Bool {
    let device: ref<SensorDevice>;
    let i: Int32;
    if Equals(evt.attitude, EAIAttitude.AIA_Hostile) {
      RWLock.AcquireShared(this.m_friendlyDevicesHostileToEnemiesLock);
      i = 0;
      while i < ArraySize(this.m_friendlyDevicesHostileToEnemies) {
        device = GameInstance.FindEntityByID(this.GetGame(), this.m_friendlyDevicesHostileToEnemies[i]) as SensorDevice;
        if IsDefined(device) {
          device.ScheduleHostileUpdateTowardsPlayerHostiles();
        };
        i += 1;
      };
      RWLock.ReleaseShared(this.m_friendlyDevicesHostileToEnemiesLock);
    };
  }

  protected cb func OnAdHocAnimationRequest(evt: ref<AdHocAnimationEvent>) -> Bool {
    let blackboardSystem: ref<BlackboardSystem> = GameInstance.GetBlackboardSystem(this.GetGame());
    let blackboard: ref<IBlackboard> = blackboardSystem.Get(GetAllBlackboardDefs().AdHocAnimation);
    blackboard.SetBool(GetAllBlackboardDefs().AdHocAnimation.IsActive, true);
    blackboard.SetBool(GetAllBlackboardDefs().AdHocAnimation.UseBothHands, evt.useBothHands);
    blackboard.SetBool(GetAllBlackboardDefs().AdHocAnimation.UnequipWeapon, evt.unequipWeapon);
    blackboard.SetInt(GetAllBlackboardDefs().AdHocAnimation.AnimationIndex, evt.animationIndex);
    blackboard.SetFloat(GetAllBlackboardDefs().AdHocAnimation.AnimationDuration, evt.animationDuration);
  }

  protected cb func OnSceneForceWeaponAimEvent(evt: ref<SceneForceWeaponAim>) -> Bool {
    let eqManipulationRequest: ref<EquipmentSystemWeaponManipulationRequest>;
    let blackboard: ref<IBlackboard> = this.GetPlayerStateMachineBlackboard();
    let tier: Int32 = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.HighLevel);
    if tier > 1 && tier <= 5 {
      blackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.SceneAimForced, true);
      blackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.SceneSafeForced, false);
      blackboard.SetFloat(GetAllBlackboardDefs().PlayerStateMachine.SceneWeaponLoweringSpeedOverride, 0.00);
      this.SendSceneOverridesAnimFeature(blackboard);
      eqManipulationRequest = new EquipmentSystemWeaponManipulationRequest();
      eqManipulationRequest.requestType = EquipmentManipulationAction.RequestLastUsedOrFirstAvailableWeapon;
      eqManipulationRequest.owner = this;
      GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem").QueueRequest(eqManipulationRequest);
    };
  }

  protected cb func OnFelledEvent(evt: ref<FelledEvent>) -> Bool {
    let blackboard: ref<IBlackboard> = this.GetPlayerStateMachineBlackboard();
    blackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.Felled, evt.m_active);
  }

  protected cb func OnClearAnimFeatureCarryEvent(evt: ref<ClearAnimFeatureCarryEvent>) -> Bool {
    let animFeatureCarry: ref<AnimFeature_Carry> = new AnimFeature_Carry();
    animFeatureCarry.state = 0;
    animFeatureCarry.pickupAnimation = 0;
    animFeatureCarry.fastMode = false;
    animFeatureCarry.useBothHands = false;
    animFeatureCarry.instant = false;
    animFeatureCarry.isCarryActive = false;
    animFeatureCarry.isFriendlyCarry = false;
    animFeatureCarry.wasThrown = false;
    AnimationControllerComponent.ApplyFeature(this, n"Carry", animFeatureCarry);
  }

  protected cb func OnSceneForceWeaponSafeEvent(evt: ref<SceneForceWeaponSafe>) -> Bool {
    let eqManipulationRequest: ref<EquipmentSystemWeaponManipulationRequest>;
    let blackboard: ref<IBlackboard> = this.GetPlayerStateMachineBlackboard();
    let tier: Int32 = blackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.HighLevel);
    if tier > 1 && tier <= 5 {
      blackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.SceneAimForced, false);
      blackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.SceneSafeForced, true);
      blackboard.SetFloat(GetAllBlackboardDefs().PlayerStateMachine.SceneWeaponLoweringSpeedOverride, evt.weaponLoweringSpeedOverride);
      this.SendSceneOverridesAnimFeature(blackboard);
      eqManipulationRequest = new EquipmentSystemWeaponManipulationRequest();
      eqManipulationRequest.requestType = EquipmentManipulationAction.RequestLastUsedOrFirstAvailableWeapon;
      eqManipulationRequest.owner = this;
      GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem").QueueRequest(eqManipulationRequest);
    };
  }

  protected cb func OnSceneFirstEquipState(evt: ref<SceneFirstEquipState>) -> Bool {
    let blackboard: ref<IBlackboard> = this.GetPlayerStateMachineBlackboard();
    blackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.ScenePreventFirstEquip, evt.prevented);
  }

  protected cb func OnEnableBraindanceActions(evt: ref<EnableBraindanceActions>) -> Bool {
    let bdEvent: ref<BraindanceInputChangeEvent>;
    let maskEvent: ref<EnableFields> = new EnableFields();
    maskEvent.actionMask = evt.actionMask;
    let bdSystem: ref<BraindanceSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"BraindanceSystem") as BraindanceSystem;
    bdSystem.QueueRequest(maskEvent);
    bdEvent = new BraindanceInputChangeEvent();
    bdEvent.bdSystem = bdSystem;
    GameInstance.GetUISystem(this.GetGame()).QueueEvent(bdEvent);
  }

  protected cb func OnDisableBraindanceActions(evt: ref<DisableBraindanceActions>) -> Bool {
    let bdEvent: ref<BraindanceInputChangeEvent>;
    let maskEvent: ref<DisableFields> = new DisableFields();
    maskEvent.actionMask = evt.actionMask;
    let bdSystem: ref<BraindanceSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"BraindanceSystem") as BraindanceSystem;
    bdSystem.QueueRequest(maskEvent);
    bdEvent = new BraindanceInputChangeEvent();
    bdEvent.bdSystem = bdSystem;
    GameInstance.GetUISystem(this.GetGame()).QueueEvent(bdEvent);
  }

  protected cb func OnForceBraindanceCameraToggle(evt: ref<ForceBraindanceCameraToggle>) -> Bool {
    let request: ref<SetBraindanceState> = new SetBraindanceState();
    request.newState = evt.editorState;
    let bdSystem: ref<BraindanceSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"BraindanceSystem") as BraindanceSystem;
    bdSystem.QueueRequest(request);
  }

  protected cb func OnPauseBraindance(evt: ref<PauseBraindance>) -> Bool {
    let request: ref<SendPauseBraindanceRequest> = new SendPauseBraindanceRequest();
    let bdSystem: ref<BraindanceSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"BraindanceSystem") as BraindanceSystem;
    bdSystem.QueueRequest(request);
  }

  protected cb func OnModifyOverlappedSecurityArease(evt: ref<ModifyOverlappedSecurityAreas>) -> Bool {
    if evt.isEntering {
      this.AddOverrlappedSecurityZone(evt.zoneID);
    } else {
      this.RemoveOverrlappedSecurityZone(evt.zoneID);
    };
  }

  public final func AddOverrlappedSecurityZone(zone: PersistentID) -> Void {
    if !ArrayContains(this.m_overlappedSecurityZones, zone) {
      ArrayPush(this.m_overlappedSecurityZones, zone);
    };
  }

  public final func RemoveOverrlappedSecurityZone(zone: PersistentID) -> Void {
    ArrayRemove(this.m_overlappedSecurityZones, zone);
  }

  protected final func SendSceneOverridesAnimFeature(sceneOverridesBlackboard: ref<IBlackboard>) -> Void {
    let animFeature: ref<AnimFeature_SceneGameplayOverrides> = new AnimFeature_SceneGameplayOverrides();
    animFeature.aimForced = sceneOverridesBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.SceneAimForced);
    animFeature.safeForced = sceneOverridesBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.SceneSafeForced);
    if sceneOverridesBlackboard.GetFloat(GetAllBlackboardDefs().PlayerStateMachine.SceneWeaponLoweringSpeedOverride) > 0.00 {
      animFeature.isAimOutTimeOverridden = true;
      animFeature.aimOutTimeOverride = sceneOverridesBlackboard.GetFloat(GetAllBlackboardDefs().PlayerStateMachine.SceneWeaponLoweringSpeedOverride);
    } else {
      animFeature.isAimOutTimeOverridden = false;
      animFeature.aimOutTimeOverride = 0.00;
    };
    AnimationControllerComponent.ApplyFeature(this, n"SceneGameplayOverrides", animFeature);
  }

  protected cb func OnWorkspotStartedEvent(evt: ref<WorkspotStartedEvent>) -> Bool {
    this.m_currentPlayerWorkspotTags = evt.tags;
    if ArrayContains(evt.tags, n"wsPlayerDamageReduction") {
      if !this.IsWorkspotDamageReductionAdded() {
        this.m_workspotDamageReductionActive = GameInstance.GetStatsSystem(this.GetGame()).ApplyModifierGroup(Cast<StatsObjectID>(this.GetEntityID()), TDBID.ToNumber(this.m_damageReductionRecordID));
      };
    };
    if ArrayContains(evt.tags, n"wsPlayerVisibilityReduction") {
      if !this.IsWorkspotVisibilityReductionActive() {
        this.m_workspotVisibilityReductionActive = GameInstance.GetStatsSystem(this.GetGame()).ApplyModifierGroup(Cast<StatsObjectID>(this.GetEntityID()), TDBID.ToNumber(this.m_visReductionRecordID));
      };
    };
    if ArrayContains(evt.tags, n"FinisherWorkspot") {
      this.m_isInFinisher = true;
      FinisherAttackEvents.SetCameraContext(this, n"WorkspotLocked");
      FinisherAttackEvents.SetGameplayCameraParameters(this, "cameraFinishers");
    };
  }

  protected cb func OnWorkspotFinishedEvent(evt: ref<WorkspotFinishedEvent>) -> Bool {
    ArrayClear(this.m_currentPlayerWorkspotTags);
    if ArrayContains(evt.tags, n"wsPlayerDamageReduction") {
      if this.IsWorkspotDamageReductionAdded() {
        if GameInstance.GetStatsSystem(this.GetGame()).RemoveModifierGroup(Cast<StatsObjectID>(this.GetEntityID()), TDBID.ToNumber(this.m_damageReductionRecordID)) {
          this.m_workspotDamageReductionActive = false;
        };
      };
    };
    if ArrayContains(evt.tags, n"wsPlayerVisibilityReduction") {
      if this.IsWorkspotVisibilityReductionActive() {
        if GameInstance.GetStatsSystem(this.GetGame()).RemoveModifierGroup(Cast<StatsObjectID>(this.GetEntityID()), TDBID.ToNumber(this.m_visReductionRecordID)) {
          this.m_workspotVisibilityReductionActive = false;
        };
      };
    };
    if ArrayContains(evt.tags, n"FinisherWorkspot") {
      this.m_isInFinisher = false;
      FinisherAttackEvents.SetCameraContext(this, n"Default");
      FinisherAttackEvents.SetGameplayCameraParameters(this, "cameraDefault");
      FinisherEndEvents.ApplyFinisherBuffs(this, true);
    };
  }

  public final const func GetPlayerCurrentWorkspotTags() -> [CName] {
    return this.m_currentPlayerWorkspotTags;
  }

  public final const func PlayerContainsWorkspotTag(tag: CName) -> Bool {
    return ArrayContains(this.m_currentPlayerWorkspotTags, tag);
  }

  public final const func IsCooldownForActionActive(actionID: TweakDBID) -> Bool {
    return this.GetPS().IsActionReady(actionID);
  }

  private final func RegisterToFacts() -> Void {
    GameInstance.GetQuestsSystem(this.GetGame()).RegisterEntity(n"q001_took_vroom_jacket", this.GetEntityID());
    GameInstance.GetQuestsSystem(this.GetGame()).RegisterEntity(n"player_allow_outerwear_clothing", this.GetEntityID());
  }

  protected cb func OnFactChangedEvent(evt: ref<FactChangedEvent>) -> Bool {
    switch evt.GetFactName() {
      case n"player_allow_outerwear_clothing":
      case n"q001_took_vroom_jacket":
        if GameInstance.GetQuestsSystem(this.GetGame()).GetFact(evt.GetFactName()) > 0 {
          this.AllowOuterwearClothing();
        } else {
          this.DisallowOuterwearClothing();
        };
        break;
      default:
    };
  }

  protected cb func OnSysDebuggerEvent(evt: ref<SysDebuggerEvent>) -> Bool {
    let req: ref<RealTimeUpdateRequest> = new RealTimeUpdateRequest();
    req.m_evt = evt;
    req.m_time = EngineTime.ToFloat(GameInstance.GetTimeSystem(this.GetGame()).GetSimTime());
    let debugger: ref<SecSystemDebugger> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"SecSystemDebugger") as SecSystemDebugger;
    debugger.QueueRequest(req);
  }

  private final func AllowOuterwearClothing() -> Void {
    AnimationControllerComponent.SetInputBool(this, n"allow_outerwear_clothing", true);
  }

  private final func DisallowOuterwearClothing() -> Void {
    AnimationControllerComponent.SetInputBool(this, n"allow_outerwear_clothing", false);
  }

  private final func InitializeFocusModeTagging() -> Void {
    let request: ref<RegisterInputListenerRequest> = new RegisterInputListenerRequest();
    request.object = this;
    this.GetTaggingSystem().QueueRequest(request);
  }

  private final func UnInitializeFocusModeTagging() -> Void {
    let request: ref<UnRegisterInputListenerRequest> = new UnRegisterInputListenerRequest();
    request.object = this;
    this.GetTaggingSystem().QueueRequest(request);
  }

  protected cb func OnRequestEquipHeavyWeapon(evt: ref<RequestEquipHeavyWeapon>) -> Bool {
    let drawItem: ref<DrawItemRequest> = new DrawItemRequest();
    let equipmentSystem: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    drawItem.itemID = evt.itemID;
    drawItem.owner = this;
    equipmentSystem.QueueRequest(drawItem);
  }

  protected cb func OnFillAnimWrapperInfoBasedOnEquippedItem(evt: ref<FillAnimWrapperInfoBasedOnEquippedItem>) -> Bool {
    if ItemID.IsValid(evt.itemID) {
      if evt.clearWrapperInfo {
        AnimationControllerComponent.SetAnimWrapperWeightOnOwnerAndItems(this, evt.itemName, 0.00);
      } else {
        AnimationControllerComponent.SetAnimWrapperWeightOnOwnerAndItems(this, evt.itemName, 1.00);
      };
    };
  }

  protected func OnIncapacitated() -> Void {
    super.OnIncapacitated();
    if this.IsDead() {
      this.EnableInteraction(n"Revive", true);
    };
    this.m_incapacitated = true;
    this.RefreshCPOVisionAppearance();
    this.SetSenseObjectType(gamedataSenseObjectType.Deadbody);
  }

  private final func RefreshCPOVisionAppearance() -> Void {
    let visionAppearance: VisionAppearance;
    if this.IsControlledByAnotherClient() {
      visionAppearance.showThroughWalls = true;
      if this.IsIncapacitated() {
        visionAppearance.fill = 1;
        if this.HasCPOMissionData() {
          visionAppearance.outline = 2;
        } else {
          visionAppearance.outline = 4;
        };
      } else {
        if this.HasCPOMissionData() {
          visionAppearance.outline = 2;
        } else {
          visionAppearance.outline = 1;
        };
      };
      GameInstance.GetVisionModeSystem(this.GetGame()).ForceVisionAppearance(this, visionAppearance);
    };
  }

  protected func OnResurrected() -> Void {
    if IsMultiplayer() {
      GameInstance.GetStatPoolsSystem(this.GetGame()).RequestResetingModifier(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.Health, gameStatPoolModificationTypes.Regeneration);
      this.Revive(20.00);
    } else {
      this.Revive(100.00);
    };
    super.OnResurrected();
    this.EnableInteraction(n"Revive", false);
    this.m_incapacitated = false;
    this.RefreshCPOVisionAppearance();
    this.CreateVendettaTimeDelayEvent();
    this.SetSenseObjectType(gamedataSenseObjectType.Player);
  }

  public const func IsIncapacitated() -> Bool {
    return this.m_incapacitated;
  }

  private final func RegisterCPOMissionDataCallback() -> Void {
    this.m_CPOMissionDataBbId = this.GetBlackboard().RegisterListenerBool(GetAllBlackboardDefs().Puppet.HasCPOMissionData, this, n"OnCPOMissionDataChanged");
  }

  private final func UnregisterCPOMissionDataCallback() -> Void {
    if IsDefined(this.m_CPOMissionDataBbId) {
      this.GetBlackboard().UnregisterListenerBool(GetAllBlackboardDefs().Puppet.HasCPOMissionData, this.m_CPOMissionDataBbId);
    };
  }

  protected cb func OnCPOMissionDataTransferred(evt: ref<CPOMissionDataTransferred>) -> Bool {
    if IsServer() {
      this.OnCPOMissionDataTransferredServer(evt);
    } else {
      this.OnCPOMissionDataTransferredClient(evt);
    };
  }

  private final func OnCPOMissionDataTransferredServer(evt: ref<CPOMissionDataTransferred>) -> Void {
    this.SetHasCPOMissionData(evt.dataDownloaded, evt.dataDamagesPresetName, evt.compatibleDeviceName, evt.ownerDecidesOnTransfer);
    if evt.dataDownloaded {
      this.m_CPOMissionDataState.m_choiceTokenTimeout = evt.choiceTokenTimeout;
      if evt.isChoiceToken {
        this.m_CPOMissionDataState.m_delayedGiveChoiceTokenEventId = MultiplayerGiveChoiceTokenEvent.CreateDelayedEvent(this, evt.compatibleDeviceName, evt.choiceTokenTimeout);
      };
    } else {
      if this.m_CPOMissionDataState.m_isChoiceToken && this.m_CPOMissionDataState.m_delayedGiveChoiceTokenEventId != new DelayID() {
        GameInstance.GetDelaySystem(this.GetGame()).CancelDelay(this.m_CPOMissionDataState.m_delayedGiveChoiceTokenEventId);
      };
    };
    this.m_CPOMissionDataState.m_isChoiceToken = evt.isChoiceToken;
    this.QueueReplicatedEvent(evt);
  }

  private final func OnCPOMissionDataTransferredClient(evt: ref<CPOMissionDataTransferred>) -> Void {
    this.m_CPOMissionDataState.m_isChoiceToken = evt.isChoiceToken;
    if evt.isChoiceToken {
      this.OnCPOMissionDataTransferredChoiceTokenClient(evt);
    };
  }

  private final func OnCPOMissionDataTransferredChoiceTokenClient(evt: ref<CPOMissionDataTransferred>) -> Void {
    this.SetCPOMissionData(evt.dataDownloaded);
    if this.m_choiceTokenTextDrawn {
      GameInstance.GetDebugVisualizerSystem(this.GetGame()).ClearLayer(this.m_choiceTokenTextLayerId);
      this.m_choiceTokenTextDrawn = false;
    };
    if evt.dataDownloaded {
      this.QueueEvent(new CPOChoiceTokenDrawTextEvent());
    };
  }

  protected cb func OnCPOChoiceTokenDrawTextEvent(evt: ref<CPOChoiceTokenDrawTextEvent>) -> Bool {
    let choiceText: String;
    if this.m_choiceTokenTextDrawn {
      GameInstance.GetDebugVisualizerSystem(this.GetGame()).ClearLayer(this.m_choiceTokenTextLayerId);
    };
    if this.HasCPOMissionData() && this.m_CPOMissionDataState.m_isChoiceToken {
      if this.IsControlledByLocalPeer() {
        choiceText = "Make a choice";
      } else {
        choiceText = "Other player is making choice";
      };
      this.m_choiceTokenTextLayerId = GameInstance.GetDebugVisualizerSystem(this.GetGame()).DrawText(new Vector4(500.00, 300.00, 0.00, 1.50), choiceText, gameDebugViewETextAlignment.Center, new Color(245u, 35u, 32u, 255u), 2.00);
      GameInstance.GetDebugVisualizerSystem(this.GetGame()).SetScale(this.m_choiceTokenTextLayerId, new Vector4(3.00, 3.00, 0.00, 0.00));
      this.m_choiceTokenTextDrawn = true;
      GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, evt, 3.00);
    };
  }

  private final func CPOMissionDataOnPlayerDetach() -> Void {
    let evt: ref<MultiplayerGiveChoiceTokenEvent>;
    if IsServer() && this.HasCPOMissionData() && this.m_CPOMissionDataState.m_isChoiceToken {
      if this.m_CPOMissionDataState.m_delayedGiveChoiceTokenEventId != new DelayID() {
        GameInstance.GetDelaySystem(this.GetGame()).CancelDelay(this.m_CPOMissionDataState.m_delayedGiveChoiceTokenEventId);
      };
      evt = MultiplayerGiveChoiceTokenEvent.CreateEvent(this.m_CPOMissionDataState.m_compatibleDeviceName, this.m_CPOMissionDataState.m_choiceTokenTimeout);
      evt.GiveChoiceToken(this);
    };
    if this.m_choiceTokenTextDrawn {
      GameInstance.GetDebugVisualizerSystem(this.GetGame()).ClearLayer(this.m_choiceTokenTextLayerId);
      this.m_choiceTokenTextDrawn = false;
    };
  }

  protected cb func OnCPOMissionPlayerVotedEvent(evt: ref<CPOMissionPlayerVotedEvent>) -> Bool {
    this.SetCPOMissionVoted(evt.compatibleDeviceName, true);
  }

  protected cb func OnPlayerDamageFromDataEvent(e: ref<PlayerDamageFromDataEvent>) -> Bool {
    this.ProcessDamageEvents(true, this.m_CPOMissionDataState.m_CPOMissionDataDamagesPreset);
  }

  protected cb func OnCPOMissionDataUpdateEvent(e: ref<CPOMissionDataUpdateEvent>) -> Bool {
    this.m_CPOMissionDataState.UpdateSounds(this);
  }

  public final const func GetCompatibleCPOMissionDeviceName() -> CName {
    return this.m_CPOMissionDataState.m_compatibleDeviceName;
  }

  protected cb func OnCPOMissionDataChanged(hasData: Bool) -> Bool {
    this.RefreshCPOVisionAppearance();
  }

  public final func SetHasCPOMissionData(setHasData: Bool, damagesPreset: CName, compatibleDeviceName: CName, ownerDecidesOnTransfer: Bool) -> Void {
    this.SetCPOMissionData(setHasData);
    this.m_CPOMissionDataState.m_CPOMissionDataDamagesPreset = damagesPreset;
    this.m_CPOMissionDataState.m_compatibleDeviceName = compatibleDeviceName;
    this.m_CPOMissionDataState.m_ownerDecidesOnTransfer = ownerDecidesOnTransfer;
    this.ProcessDamageEvents(setHasData, damagesPreset);
  }

  protected cb func OnCPOGiveChoiceTokenEvent(e: ref<MultiplayerGiveChoiceTokenEvent>) -> Bool {
    e.GiveChoiceToken(this);
  }

  private final func ProcessDamageEvents(addDamage: Bool, damagesPreset: CName) -> Void {
    let armorDPS: Float;
    let currArmor: Float;
    let currHealth: Float;
    let healthDPS: Float;
    let tickableEvent: ref<PlayerDamageFromDataEvent>;
    let delaySystem: ref<DelaySystem> = GameInstance.GetDelaySystem(this.GetGame());
    delaySystem.CancelDelay(this.m_DataDamageUpdateID);
    if addDamage {
      armorDPS = TweakDBInterface.GetFloat(TDBID.Create("player." + NameToString(damagesPreset) + ".armorDPS"), 0.00);
      healthDPS = TweakDBInterface.GetFloat(TDBID.Create("player." + NameToString(damagesPreset) + ".healthDPS"), 0.00);
      currArmor = GameInstance.GetStatPoolsSystem(this.GetGame()).GetStatPoolValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.CPO_Armor);
      currHealth = GameInstance.GetStatPoolsSystem(this.GetGame()).GetStatPoolValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.Health);
      if armorDPS > 0.00 && currArmor > 0.00 {
        GameInstance.GetStatPoolsSystem(this.GetGame()).RequestChangingStatPoolValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.CPO_Armor, -armorDPS, null, false);
      } else {
        if healthDPS > 0.00 && currHealth > 0.00 {
          if armorDPS > 0.00 {
            GameInstance.GetStatPoolsSystem(this.GetGame()).RequestChangingStatPoolValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.CPO_Armor, -armorDPS, null, false);
          };
          GameInstance.GetStatPoolsSystem(this.GetGame()).RequestChangingStatPoolValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatPoolType.Health, -healthDPS, null, false);
        };
      };
      tickableEvent = new PlayerDamageFromDataEvent();
      this.m_DataDamageUpdateID = delaySystem.DelayEvent(this, tickableEvent, 1.00);
    };
  }

  protected cb func OnDeath(evt: ref<gameDeathEvent>) -> Bool {
    this.ForceCloseRadialWheel();
    StatusEffectHelper.ApplyStatusEffect(this, t"GameplayRestriction.BlockAllHubMenu");
    super.OnDeath(evt);
    GameInstance.GetTelemetrySystem(this.GetGame()).LogPlayerDeathEvent(evt);
    GameInstance.GetAutoSaveSystem(this.GetGame()).InvalidateAutoSaveRequests();
  }

  private final func ForceCloseRadialWheel() -> Void {
    let closeEvt: ref<ForceRadialWheelShutdown> = new ForceRadialWheelShutdown();
    this.QueueEvent(closeEvt);
  }

  private final func Revive(percAmount: Float) -> Void {
    let playerID: StatsObjectID = Cast<StatsObjectID>(this.GetEntityID());
    let statPoolsSystem: ref<StatPoolsSystem> = GameInstance.GetStatPoolsSystem(this.GetGame());
    if percAmount >= 0.00 && percAmount <= 100.00 {
      statPoolsSystem.RequestSettingStatPoolValue(playerID, gamedataStatPoolType.Health, percAmount, null, true);
    };
  }

  protected cb func OnTargetNeutraliziedEvent(evt: ref<TargetNeutraliziedEvent>) -> Bool {
    let processExpReq: ref<ProcessQueuedCombatExperience>;
    let bb: ref<IBlackboard> = GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_Crosshair);
    bb.SetVariant(GetAllBlackboardDefs().UI_Crosshair.EnemyNeutralized, ToVariant(evt.type));
    processExpReq = new ProcessQueuedCombatExperience();
    processExpReq.owner = GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerControlledGameObject();
    processExpReq.m_entity = evt.targetID;
    GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"PlayerDevelopmentSystem").QueueRequest(processExpReq);
    this.CheckVForVendettaAchievement(evt);
    GameInstance.GetRazerChromaEffectsSystem(this.GetGame()).PlayAnimation(n"EnemyKill", false);
  }

  protected cb func OnRewindableSectionEvent(evt: ref<scnRewindableSectionEvent>) -> Bool {
    let psmAdd: ref<PSMAddOnDemandStateMachine>;
    let psmRem: ref<PSMRemoveOnDemandStateMachine>;
    let stateMachineIdentifierRem: StateMachineIdentifier;
    let inBD: ref<SetIsInBraindance> = new SetIsInBraindance();
    let bdSystem: ref<BraindanceSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"BraindanceSystem") as BraindanceSystem;
    if evt.active {
      psmAdd = new PSMAddOnDemandStateMachine();
      psmAdd.stateMachineName = n"BraindanceControls";
      this.QueueEvent(psmAdd);
      this.DisableCameraBobbing(true);
      inBD.newState = true;
      bdSystem.QueueRequest(inBD);
      GameInstance.GetAudioSystem(this.GetGame()).SetBDCameraListenerOverride(true);
    } else {
      psmRem = new PSMRemoveOnDemandStateMachine();
      stateMachineIdentifierRem.definitionName = n"BraindanceControls";
      psmRem.stateMachineIdentifier = stateMachineIdentifierRem;
      this.QueueEvent(psmRem);
      this.DisableCameraBobbing(false);
      inBD.newState = false;
      bdSystem.QueueRequest(inBD);
      GameInstance.GetAudioSystem(this.GetGame()).SetBDCameraListenerOverride(false);
    };
  }

  public final const func IsInCombat() -> Bool {
    return this.m_inCombat;
  }

  public final const func IsNaked() -> Bool {
    if ItemID.IsValid(EquipmentSystem.GetData(GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerMainGameObject()).GetActiveItem(gamedataEquipmentArea.Legs)) || ItemID.IsValid(EquipmentSystem.GetData(GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerMainGameObject()).GetActiveItem(gamedataEquipmentArea.OuterChest)) || ItemID.IsValid(EquipmentSystem.GetData(GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerMainGameObject()).GetActiveItem(gamedataEquipmentArea.InnerChest)) {
      return false;
    };
    return true;
  }

  public final const func IsMoving() -> Bool {
    return this.IsMovingHorizontally() || this.IsMovingVertically();
  }

  public final const func IsMovingHorizontally() -> Bool {
    return this.GetPlayerStateMachineBlackboard().GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsMovingHorizontally);
  }

  public final const func IsMovingVertically() -> Bool {
    return this.GetPlayerStateMachineBlackboard().GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsMovingVertically);
  }

  protected cb func OnZoneChange(value: Variant) -> Bool {
    let securityZoneData: SecurityAreaData = FromVariant<SecurityAreaData>(value);
    GameInstance.GetTelemetrySystem(this.GetGame()).LogPlayerInDangerousArea(Equals(securityZoneData.securityAreaType, ESecurityAreaType.RESTRICTED) || Equals(securityZoneData.securityAreaType, ESecurityAreaType.DANGEROUS));
  }

  private final func SetWarningMessage(const message: script_ref<String>, opt msgType: SimpleMessageType) -> Void {
    let warningMsg: SimpleScreenMessage;
    warningMsg.isShown = true;
    warningMsg.duration = 5.00;
    warningMsg.message = Deref(message);
    if NotEquals(msgType, SimpleMessageType.Undefined) {
      warningMsg.type = msgType;
    };
    GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_Notifications).SetVariant(GetAllBlackboardDefs().UI_Notifications.WarningMessage, ToVariant(warningMsg), true);
  }

  private final func StartProcessingVForVendettaAchievement(deathInstigator: ref<GameObject>) -> Void {
    if GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.HasSecondHeart) > 0.00 && IsDefined(deathInstigator as NPCPuppet) && this.m_NPCDeathInstigator == null {
      this.m_NPCDeathInstigator = deathInstigator as NPCPuppet;
    };
  }

  private final func CreateVendettaTimeDelayEvent() -> Void {
    let vendettaTimeDelayEvent: ref<FinishedVendettaTimeEvent>;
    if IsDefined(this.m_NPCDeathInstigator) {
      vendettaTimeDelayEvent = new FinishedVendettaTimeEvent();
      GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, vendettaTimeDelayEvent, 5.00);
    };
  }

  protected cb func OnFinishedVendettaTimeEvent(evt: ref<FinishedVendettaTimeEvent>) -> Bool {
    this.m_NPCDeathInstigator = null;
  }

  private final const func CheckVForVendettaAchievement(evt: ref<TargetNeutraliziedEvent>) -> Void {
    let achievementRequest: ref<AddAchievementRequest>;
    let dataTrackingSystem: ref<DataTrackingSystem> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"DataTrackingSystem") as DataTrackingSystem;
    let achievement: gamedataAchievement = gamedataAchievement.VForVendetta;
    if !IsDefined(this.m_NPCDeathInstigator) || this.m_NPCDeathInstigator.GetEntityID() != evt.targetID {
      return;
    };
    achievementRequest = new AddAchievementRequest();
    achievementRequest.achievement = achievement;
    dataTrackingSystem.QueueRequest(achievementRequest);
  }

  protected cb func OnProcessVendettaAchievementEvent(evt: ref<ProcessVendettaAchievementEvent>) -> Bool {
    this.StartProcessingVForVendettaAchievement(evt.deathInstigator);
  }

  public const func GetNetworkLinkSlotName() -> CName {
    return n"Chest";
  }

  public const func IsNetworkLinkDynamic() -> Bool {
    return true;
  }

  private final func RegisterRemoteMappin() -> Void {
    let data: MappinData;
    data.mappinType = t"Mappins.CPO_RemotePlayerMappinDefinition";
    data.variant = gamedataMappinVariant.CPO_RemotePlayerVariant;
    data.active = true;
    this.m_remoteMappinId = GameInstance.GetMappinSystem(this.GetGame()).RegisterRemotePlayerMappin(data, this);
  }

  private final func UnregisterRemoteMappin() -> Void {
    if this.m_remoteMappinId.value != 0u {
      GameInstance.GetMappinSystem(this.GetGame()).UnregisterMappin(this.m_remoteMappinId);
      this.m_remoteMappinId.value = 0u;
    };
  }

  protected cb func OnRegisterFastTravelPoints(evt: ref<RegisterFastTravelPointsEvent>) -> Bool {
    let request: ref<RegisterFastTravelPointRequest>;
    let i: Int32 = 0;
    while i < ArraySize(evt.fastTravelNodes) {
      request = new RegisterFastTravelPointRequest();
      request.pointData = evt.fastTravelNodes[i];
      request.requesterID = this.GetEntityID();
      this.GetFastTravelSystem().QueueRequest(request);
      i += 1;
    };
  }

  public const func ShouldShowScanner() -> Bool {
    if IsMultiplayer() {
      return true;
    };
    return false;
  }

  protected cb func OnWoundedInstigated(evt: ref<WoundedInstigated>) -> Bool {
    let value: Uint32 = this.GetPlayerPerkDataBlackboard().GetUint(GetAllBlackboardDefs().PlayerPerkData.WoundedInstigated);
    this.GetPlayerPerkDataBlackboard().SetUint(GetAllBlackboardDefs().PlayerPerkData.WoundedInstigated, value + 1u);
  }

  protected cb func OnDismembermentInstigated(evt: ref<DismembermentInstigated>) -> Bool {
    let info: DismembermentInstigatedInfo;
    let oldInfo: DismembermentInstigatedInfo = FromVariant<DismembermentInstigatedInfo>(this.GetPlayerPerkDataBlackboard().GetVariant(GetAllBlackboardDefs().PlayerPerkData.DismembermentInstigated));
    info.value = oldInfo.value + 1u;
    info.targetPosition = evt.targetPosition;
    info.attackPosition = evt.attackPosition;
    info.wasTargetAlreadyDead = evt.targetIsDead;
    info.wasTargetAlreadyDefeated = evt.targetIsDefeated;
    info.weaponRecord = evt.weaponRecord;
    info.bodyPart = evt.bodyPart;
    info.attackType = evt.attackType;
    info.attackSubtype = evt.attackSubtype;
    info.attackIsExplosion = evt.attackIsExplosion;
    info.target = evt.target;
    info.timeSinceDeath = evt.timeSinceDeath;
    info.timeSinceDefeat = evt.timeSinceDefeat;
    this.GetPlayerPerkDataBlackboard().SetVariant(GetAllBlackboardDefs().PlayerPerkData.DismembermentInstigated, ToVariant(info));
    if PlayerDevelopmentSystem.GetInstance(this).IsNewPerkBought(this, gamedataNewPerkType.Espionage_Central_Milestone_1) == 1 && Equals(evt.weaponRecord.ItemType().Type(), gamedataItemType.Cyb_MantisBlades) && !evt.targetIsDead {
      StatusEffectHelper.ApplyStatusEffect(this, t"BaseStatusEffect.Espionage_Central_Milestone_1_Buff_MantisBlades");
    };
  }

  private final func ToggleFocusedGrenadeShootingPerk(activate: Bool) -> Void {
    let aimCandidateRadiusSquared: Float;
    let asBaseGrenade: ref<BaseGrenade>;
    let asWeaponGrenade: ref<WeaponGrenade>;
    let bestAimCandidate: ref<WeaponGrenade>;
    let cameraForward: Vector4;
    let playerToGrenade: Vector4;
    let projectileEntity: wref<Entity>;
    let projectionLength: Float;
    let projectionVector: Vector4;
    let projectileSystem: ref<ProjectileSystem> = GameInstance.GetProjectileSystem(this.GetGame());
    let aimCandidateMaxRadiusSquared: Float = 6.00;
    let bestAimCandidateRadiusSquared: Float = aimCandidateMaxRadiusSquared;
    let projectileComponents: array<ref<ProjectileComponent>> = projectileSystem.GetRegisteredComponents();
    let i: Int32 = 0;
    while i < ArraySize(projectileComponents) {
      projectileEntity = projectileComponents[i].GetEntity();
      if !IsDefined(projectileEntity) {
      } else {
        asWeaponGrenade = projectileEntity as WeaponGrenade;
        if !IsDefined(asWeaponGrenade) {
        } else {
          if !activate {
            asWeaponGrenade.DisableFocusedShootingHighlight();
            if asWeaponGrenade.IsGrenadeTargetedWithFocusedShootingPerk() {
              asWeaponGrenade.DeactivateFocusedShootingAim();
            };
          } else {
            asWeaponGrenade.EnableFocusedShootingHighlight();
            playerToGrenade = projectileEntity.GetWorldPosition() - this.GetWorldPosition();
            cameraForward = Matrix.GetDirectionVector(this.GetFPPCameraComponent().GetLocalToWorld());
            projectionLength = Vector4.Dot(cameraForward, playerToGrenade) / Vector4.Length(cameraForward);
            if projectionLength <= 0.00 {
            } else {
              projectionVector = cameraForward * projectionLength;
              aimCandidateRadiusSquared = Vector4.LengthSquared(playerToGrenade) - Vector4.LengthSquared(projectionVector);
              if aimCandidateRadiusSquared < bestAimCandidateRadiusSquared && aimCandidateRadiusSquared <= aimCandidateMaxRadiusSquared {
                bestAimCandidateRadiusSquared = aimCandidateRadiusSquared;
                bestAimCandidate = asWeaponGrenade;
              };
            };
          };
        };
      };
      i += 1;
    };
    if !activate {
      return;
    };
    if !IsDefined(bestAimCandidate) {
      return;
    };
    bestAimCandidate.TriggerLookAtThisGrenade();
    asBaseGrenade = asWeaponGrenade as BaseGrenade;
    if IsDefined(asBaseGrenade) {
      asBaseGrenade.SetCanBeShot(true);
    };
  }

  public final func GetPrimaryTargetingComponent() -> ref<TargetingComponent> {
    return this.m_primaryTargetingComponent;
  }

  public final func GetBreachFinderComponent() -> ref<BreachFinderComponent> {
    return this.m_breachFinderComponent;
  }

  public final static func SetLevel(inst: GameInstance, const stringType: script_ref<String>, const stringVal: script_ref<String>, levelGainReason: telemetryLevelGainReason) -> Void {
    let i: Int32;
    let inventory: array<wref<gameItemData>>;
    let itemData: wref<gameItemData>;
    let statMod: ref<gameStatModifierData>;
    let profType: gamedataProficiencyType = IntEnum<gamedataProficiencyType>(Cast<Int32>(EnumValueFromString("gamedataProficiencyType", stringType)));
    let newLevel: Int32 = StringToInt(stringVal);
    let request: ref<SetProficiencyLevel> = new SetProficiencyLevel();
    request.Set(GetPlayer(inst), newLevel, profType, levelGainReason);
    GameInstance.GetScriptableSystemsContainer(inst).Get(n"PlayerDevelopmentSystem").QueueRequest(request);
    if Equals(profType, gamedataProficiencyType.Level) {
      GameInstance.GetTransactionSystem(inst).GetItemList(GetPlayer(inst), inventory);
      statMod = RPGManager.CreateStatModifier(gamedataStatType.PowerLevel, gameStatModifierType.Additive, StringToFloat(stringVal));
      i = 0;
      while i < ArraySize(inventory) {
        itemData = inventory[i];
        if IsDefined(RPGManager.GetItemRecord(itemData.GetID())) {
          GameInstance.GetStatsSystem(inst).RemoveAllModifiers(itemData.GetStatsObjectID(), gamedataStatType.PowerLevel, true);
          GameInstance.GetStatsSystem(inst).AddSavedModifier(itemData.GetStatsObjectID(), statMod);
        };
        i += 1;
      };
    };
  }

  public final static func SetBuild(inst: GameInstance, const stringType: script_ref<String>, opt isDebug: Bool) -> Void {
    let buildRequest: ref<questSetProgressionBuildRequest>;
    let buildType: gamedataBuildType;
    let buildTypeRequest: ref<SetProgressionBuild>;
    let buildInt: Int32 = Cast<Int32>(EnumValueFromString("gamedataBuildType", stringType));
    let player: ref<PlayerPuppet> = GetPlayer(inst);
    if buildInt >= 0 {
      buildType = IntEnum<gamedataBuildType>(buildInt);
      buildTypeRequest = new SetProgressionBuild();
      buildTypeRequest.m_isDebug = isDebug;
      buildTypeRequest.Set(player, buildType);
      GameInstance.GetScriptableSystemsContainer(inst).Get(n"PlayerDevelopmentSystem").QueueRequest(buildTypeRequest);
    } else {
      buildRequest = new questSetProgressionBuildRequest();
      buildRequest.buildID = TDBID.Create(Deref(stringType));
      buildRequest.owner = player;
      GameInstance.GetScriptableSystemsContainer(inst).Get(n"PlayerDevelopmentSystem").QueueRequest(buildRequest);
    };
  }

  private final func ApplyNPCLevelAndProgressionBuild(npc: wref<GameObject>, actionName: CName) -> Void {
    let NPCLevel: Int32;
    let buildName: String;
    let buildSpacing: Int32;
    let presetBuildLevel: Int32;
    let statsSystem: ref<StatsSystem>;
    let gameInstance: GameInstance = this.GetGame();
    if IsDefined(npc) {
      statsSystem = GameInstance.GetStatsSystem(gameInstance);
      NPCLevel = Cast<Int32>(statsSystem.GetStatValue(Cast<StatsObjectID>(npc.GetEntityID()), gamedataStatType.PowerLevel));
      buildSpacing = this.FindBuildSpacing("gamedataBuildType", "RangedCombat");
      if buildSpacing <= 0 {
        return;
      };
      presetBuildLevel = NPCLevel - NPCLevel % buildSpacing;
      switch actionName {
        case n"ApplyNPCLevelToPlayerRanged":
          buildName = "RangedCombat";
          break;
        case n"ApplyNPCLevelToPlayerMelee":
          buildName = "MeleeCombat";
          break;
        case n"ApplyNPCLevelToPlayerNetrunner":
          buildName = "CombatNetrunner";
          break;
        default:
      };
      if Equals(buildName, "") {
        return;
      };
      buildName = buildName + presetBuildLevel;
      AddFact(gameInstance, n"full_rpg_progression_on");
      PlayerPuppet.SetBuild(gameInstance, buildName);
      PlayerPuppet.SetLevel(gameInstance, "Level", IntToString(NPCLevel), telemetryLevelGainReason.Ignore);
    };
  }

  protected cb func OnMeleeHitEvent(evt: ref<MeleeHitEvent>) -> Bool {
    let bodySlamBump: Int32;
    let isBodySlamming: Bool;
    let isExhuasted: Bool;
    let slowMoDelay: Float;
    let slowMoEvent: ref<MeleeHitSlowMoEvent>;
    let targetAsPuppet: ref<ScriptedPuppet>;
    let validTarget: Bool;
    let slowMoEnabled: Bool = TweakDBInterface.GetBool(t"timeSystem.meleeHit.enabled", false);
    if evt.isStrongAttack {
      slowMoEnabled = TweakDBInterface.GetBool(t"timeSystem.meleeHitStrong.enabled", false);
      slowMoDelay = TweakDBInterface.GetFloat(t"timeSystem.meleeHitStrong.delay", 0.10);
    } else {
      slowMoEnabled = TweakDBInterface.GetBool(t"timeSystem.meleeHit.enabled", false);
      slowMoDelay = TweakDBInterface.GetFloat(t"timeSystem.meleeHit.delay", 0.10);
    };
    isBodySlamming = this.GetPlayerStateMachineBlackboard().GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsInBodySlamState);
    if slowMoEnabled || isBodySlamming {
      targetAsPuppet = evt.target as ScriptedPuppet;
    };
    if slowMoEnabled {
      validTarget = IsDefined(targetAsPuppet) && ScriptedPuppet.IsAlive(targetAsPuppet) || IsDefined(evt.target as WeakspotObject);
      isExhuasted = StatusEffectSystem.ObjectHasStatusEffect(this, t"BaseStatusEffect.PlayerExhausted");
      if !evt.hitBlocked && evt.instigator == this && !isExhuasted && validTarget {
        slowMoEvent = new MeleeHitSlowMoEvent();
        slowMoEvent.isStrongAttack = evt.isStrongAttack;
        GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, slowMoEvent, slowMoDelay);
      };
    };
    if isBodySlamming && IsDefined(targetAsPuppet) {
      bodySlamBump = this.GetPlayerStateMachineBlackboard().GetInt(GetAllBlackboardDefs().PlayerStateMachine.BodySlamBump);
      bodySlamBump += 1;
      this.GetPlayerStateMachineBlackboard().SetInt(GetAllBlackboardDefs().PlayerStateMachine.BodySlamBump, bodySlamBump, true);
    };
  }

  protected cb func OnMeleeHitSloMo(evt: ref<MeleeHitSlowMoEvent>) -> Bool {
    let dilation: Float;
    let duration: Float;
    let easeInCurve: CName;
    let easeOutCurve: CName;
    if evt.isStrongAttack {
      dilation = TweakDBInterface.GetFloat(t"timeSystem.meleeHitStrong.timeDilation", 0.10);
      duration = TweakDBInterface.GetFloat(t"timeSystem.meleeHitStrong.duration", 0.10);
      easeInCurve = TweakDBInterface.GetCName(t"timeSystem.meleeHitStrong.easeInCurve", n"None");
      easeOutCurve = TweakDBInterface.GetCName(t"timeSystem.meleeHitStrong.easeOutCurve", n"None");
    } else {
      dilation = TweakDBInterface.GetFloat(t"timeSystem.meleeHit.timeDilation", 0.10);
      duration = TweakDBInterface.GetFloat(t"timeSystem.meleeHit.duration", 0.10);
      easeInCurve = TweakDBInterface.GetCName(t"timeSystem.meleeHit.easeInCurve", n"None");
      easeOutCurve = TweakDBInterface.GetCName(t"timeSystem.meleeHit.easeOutCurve", n"None");
    };
    if duration < 0.00 {
      duration = 0.10;
    };
    GameInstance.GetTimeSystem(this.GetGame()).SetTimeDilation(n"meleeHit", dilation, duration, easeInCurve, easeOutCurve);
  }

  private final func FindBuildSpacing(const enumType: script_ref<String>, const buildNameStringPart: script_ref<String>) -> Int32 {
    let buildInt: Int32;
    let fullEnumString: String;
    let i: Int32 = 1;
    while i <= 20 {
      fullEnumString = buildNameStringPart + i;
      buildInt = Cast<Int32>(EnumValueFromString(enumType, fullEnumString));
      if buildInt >= 0 {
        return i;
      };
      i += 1;
    };
    return -1;
  }

  private final func GotKeycardNotification() -> Void {
    let notify: ref<AuthorisationNotificationEvent> = new AuthorisationNotificationEvent();
    notify.type = gameuiAuthorisationNotificationType.GotKeycard;
    this.QueueEvent(notify);
  }

  protected cb func OnHackTargetEvent(evt: ref<HackTargetEvent>) -> Bool {
    super.OnHackTargetEvent(evt);
    this.SetIsBeingRevealed(evt.settings.isRevealPositionAction);
    this.m_attackingNetrunnerID = evt.netrunnerID;
  }

  protected cb func OnCarHitPlayer(evt: ref<OnCarHitPlayer>) -> Bool {
    let attack: ref<IAttack>;
    let attackContext: AttackInitContext;
    let broadcaster: ref<StimBroadcasterComponent>;
    let currTime: Float;
    let hitEvent: ref<gameHitEvent>;
    let hornEvt: ref<VehicleHornProbsEvent>;
    let seperationImpulseEvt: ref<PSMImpulse>;
    let soundEvent: ref<SoundPlayEvent>;
    let vehicleObject: ref<VehicleObject>;
    if StatusEffectSystem.ObjectHasStatusEffect(this, t"BaseStatusEffect.VehicleKnockdown") || StatusEffectSystem.ObjectHasStatusEffect(this, t"BaseStatusEffect.BikeKnockdown") {
      return false;
    };
    currTime = EngineTime.ToFloat(GameInstance.GetTimeSystem(this.GetGame()).GetSimTime());
    if currTime < this.m_vehicleKnockdownTimestamp + 5.00 {
      return false;
    };
    this.m_vehicleKnockdownTimestamp = currTime;
    hitEvent = new gameHitEvent();
    hitEvent.attackData = new AttackData();
    hitEvent.target = this;
    attackContext.record = TweakDBInterface.GetAttackRecord(t"Attacks.CarHitPlayer");
    attackContext.instigator = this;
    attackContext.source = this;
    attack = IAttack.Create(attackContext);
    hitEvent.attackData.SetAttackDefinition(attack);
    hitEvent.attackData.AddFlag(hitFlag.FriendlyFire, n"vehicle_collision");
    hitEvent.attackData.AddFlag(hitFlag.CanDamageSelf, n"vehicle_collision");
    hitEvent.attackData.SetSource(this);
    hitEvent.attackData.SetInstigator(this);
    hitEvent.attackData.SetAttackDefinition(attack);
    hitEvent.hitDirection = evt.hitDirection;
    GameInstance.GetDamageSystem(this.GetGame()).QueueHitEvent(hitEvent, this);
    if NotEquals(evt.seperationImpulse, new Vector4(0.00, 0.00, 0.00, 0.00)) {
      seperationImpulseEvt = new PSMImpulse();
      seperationImpulseEvt.id = n"impulse";
      seperationImpulseEvt.impulse = evt.seperationImpulse;
      this.QueueEvent(seperationImpulseEvt);
    };
    soundEvent = new SoundPlayEvent();
    soundEvent.soundName = n"v_col_player_impact";
    hitEvent.target.QueueEvent(soundEvent);
    broadcaster = this.GetStimBroadcasterComponent();
    broadcaster.TriggerSingleBroadcast(this, gamedataStimType.CrowdIllegalAction, 4.00);
    vehicleObject = GameInstance.FindEntityByID(this.GetGame(), evt.carId) as VehicleObject;
    if vehicleObject.HasPassengers() && !vehicleObject.IsVehicleRemoteControlled() {
      hornEvt = new VehicleHornProbsEvent();
      hornEvt.honkMinTime = 1.00;
      hornEvt.honkMaxTime = 2.00;
      hornEvt.probability = 0.80;
      vehicleObject.QueueEvent(hornEvt);
    };
  }

  protected cb func OnDistrictChanged(evt: ref<PlayerEnteredNewDistrictEvent>) -> Bool {
    this.m_gunshotRange = evt.gunshotRange;
    this.m_explosionRange = evt.explosionRange;
  }

  public final const func GetGunshotRange() -> Float {
    return this.m_gunshotRange;
  }

  public final const func GetExplosionRange() -> Float {
    return this.m_explosionRange;
  }

  public final const func GetMinigamePrograms() -> [MinigameProgramData] {
    return this.GetPS().GetMinigamePrograms();
  }

  protected cb func OnUpdateMiniGameProgramsEvent(evt: ref<UpdateMiniGameProgramsEvent>) -> Bool {
    this.UpdateMinigamePrograms(evt.program, evt.add);
  }

  private final func UpdateMinigamePrograms(program: MinigameProgramData, add: Bool) -> Void {
    let evt: ref<StoreMiniGameProgramEvent> = new StoreMiniGameProgramEvent();
    evt.program = program;
    evt.add = add;
    this.SendEventToDefaultPS(evt);
  }

  private final func RestoreMinigamePrograms() -> Void {
    let programs: array<MinigameProgramData> = this.GetPS().GetMinigamePrograms();
    this.GetMinigameBlackboard().SetVariant(GetAllBlackboardDefs().HackingMinigame.PlayerPrograms, ToVariant(programs));
  }

  private final func GetMinigameBlackboard() -> ref<IBlackboard> {
    return GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().HackingMinigame);
  }

  private final func InitInterestingFacts() -> Void {
    this.m_interestingFacts.m_zone = n"CityAreaType";
    this.m_interestingFactsListenersFunctions.m_zone = n"OnZoneFactChanged";
  }

  private final func UpdateAimAssist() -> Void {
    if !this.m_aimAssistUpdateQueued {
      this.m_aimAssistUpdateQueued = true;
      GameInstance.GetDelaySystem(this.GetGame()).QueueTask(this, null, n"UpdateAimAssistDelayedTask", gameScriptTaskExecutionStage.Any);
    };
  }

  protected final func UpdateAimAssistDelayedTask(data: ref<ScriptTaskData>) -> Void {
    this.UpdateAimAssistImmediate();
  }

  private final func UpdateAimAssistImmediate() -> Void {
    let inLefthandCW: Bool;
    let inMeleeAssistState: Bool;
    let inSprint: Bool;
    let isExhuasted: Bool;
    this.m_aimAssistUpdateQueued = false;
    if this.m_focusModeActive {
      this.ApplyAimAssistSettings(AimAssistSettingConfig.Scanning);
      return;
    };
    if EntityID.IsDefined(this.m_controllingDeviceID) {
      this.ApplyAimAssistSettings(AimAssistSettingConfig.Default);
      return;
    };
    if this.m_leftHandCyberwareState == 5 {
      this.ApplyAimAssistSettings(AimAssistSettingConfig.LeftHandCyberwareCharge);
      return;
    };
    inLefthandCW = this.m_leftHandCyberwareState != 0 && this.m_leftHandCyberwareState != 12 && this.m_leftHandCyberwareState != 11;
    if inLefthandCW {
      this.ApplyAimAssistSettings(AimAssistSettingConfig.LeftHandCyberware);
      return;
    };
    inSprint = this.m_locomotionState == 2;
    if NotEquals(this.m_vehicleState, gamePSMVehicle.Default) {
      if Equals(this.m_vehicleState, gamePSMVehicle.Combat) {
        this.ApplyAimAssistSettings(AimAssistSettingConfig.VehicleCombat);
        return;
      };
      if Equals(this.m_vehicleState, gamePSMVehicle.DriverCombat) {
        if this.m_equippedRightHandWeapon.IsMelee() {
          if this.m_meleeWeaponState == 19 {
            this.ApplyAimAssistSettings(AimAssistSettingConfig.Vehicle);
            return;
          };
          if this.m_inDriverCombatTPP {
            this.ApplyAimAssistSettings(AimAssistSettingConfig.DriverCombatMeleeTPP);
            return;
          };
          this.ApplyAimAssistSettings(AimAssistSettingConfig.Vehicle);
          return;
        };
        if this.m_inMountedWeaponVehicle {
          if Equals(this.m_driverCombatWeaponType, gamedataItemType.Wea_VehicleMissileLauncher) {
            if this.m_isAiming {
              this.ApplyAimAssistSettings(AimAssistSettingConfig.DriverCombatMissilesAiming);
              return;
            };
            this.ApplyAimAssistSettings(AimAssistSettingConfig.DriverCombatMissiles);
            return;
          };
          this.ApplyAimAssistSettings(AimAssistSettingConfig.Vehicle);
          return;
        };
        if this.m_isAiming {
          this.ApplyAimAssistSettings(AimAssistSettingConfig.DriverCombatAiming);
          return;
        };
        if this.m_inDriverCombatTPP {
          this.ApplyAimAssistSettings(AimAssistSettingConfig.DriverCombatTPP);
          return;
        };
        this.ApplyAimAssistSettings(AimAssistSettingConfig.DriverCombat);
        return;
      };
      this.ApplyAimAssistSettings(AimAssistSettingConfig.Vehicle);
      return;
    };
    if this.m_isInBodySlam || this.m_combatGadgetState != 0 {
      this.ApplyAimAssistSettings(AimAssistSettingConfig.Off);
      return;
    };
    if !inSprint && IsDefined(this.m_equippedRightHandWeapon) && this.m_equippedRightHandWeapon.IsMelee() && (!this.m_isAiming || !MeleeTransition.CanThrowWeaponObject(this, this.m_equippedRightHandWeapon)) && this.m_meleeWeaponState != 19 {
      inMeleeAssistState = this.m_meleeWeaponState == 8 || this.m_meleeWeaponState == 10 || this.m_meleeWeaponState == 20;
      this.ApplyAimAssistSettings(inMeleeAssistState ? AimAssistSettingConfig.MeleeCombat : AimAssistSettingConfig.MeleeCombatIdle);
      return;
    };
    if this.m_doingQuickMelee {
      this.ApplyAimAssistSettings(AimAssistSettingConfig.QuickMelee);
      return;
    };
    isExhuasted = StatusEffectSystem.ObjectHasStatusEffect(this, t"BaseStatusEffect.PlayerExhausted");
    if isExhuasted {
      this.ApplyAimAssistSettings(AimAssistSettingConfig.Exhausted);
      return;
    };
    if this.m_isAiming {
      this.m_weaponZoomLevel = GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.ZoomLevel);
      if this.m_weaponZoomLevel <= 1.30 {
        this.ApplyAimAssistSettings(this.m_hasKiroshiOpticsFragment ? AimAssistSettingConfig.AimingLimbCyber : AimAssistSettingConfig.Aiming);
      } else {
        if this.m_weaponZoomLevel <= 2.50 {
          this.ApplyAimAssistSettings(this.m_hasKiroshiOpticsFragment ? AimAssistSettingConfig.AimingLimbCyberZoomLevel1 : AimAssistSettingConfig.ZoomLevel1);
        } else {
          this.ApplyAimAssistSettings(this.m_hasKiroshiOpticsFragment ? AimAssistSettingConfig.AimingLimbCyberZoomLevel2 : AimAssistSettingConfig.ZoomLevel2);
        };
      };
      return;
    };
    if inSprint {
      this.ApplyAimAssistSettings(AimAssistSettingConfig.Sprinting);
      return;
    };
    this.ApplyAimAssistSettings(this.m_hasKiroshiOpticsFragment ? AimAssistSettingConfig.LimbCyber : AimAssistSettingConfig.Default);
  }

  public final func GetAimAssistLevel() -> EAimAssistLevel {
    return this.m_aimAssistListener.GetAimAssistLevel();
  }

  public final func IsAimSnapEnabled() -> Bool {
    return this.m_aimAssistListener.GetAimSnapEnabled();
  }

  public final func ApplyAimAssistSettings(config: AimAssistSettingConfig) -> Void {
    let configRecord: wref<AimAssistConfigPreset_Record>;
    let settingsRecord: wref<AimAssistSettings_Record>;
    let aimAssistLevel: EAimAssistLevel = EAimAssistLevel.Standard;
    if Equals(config, this.m_aimAssistListener.m_currentConfig) {
      return;
    };
    if NotEquals(config, AimAssistSettingConfig.Count) {
      this.m_aimAssistListener.m_currentConfig = config;
    };
    if NotEquals(this.m_aimAssistListener.m_currentConfig, AimAssistSettingConfig.Count) {
      settingsRecord = this.m_aimAssistListener.m_settingsRecords[EnumInt(this.m_aimAssistListener.m_currentConfig)];
      if IsDefined(settingsRecord) {
        if Equals(this.m_aimAssistListener.m_currentConfig, AimAssistSettingConfig.MeleeCombat) || Equals(this.m_aimAssistListener.m_currentConfig, AimAssistSettingConfig.MeleeCombatIdle) {
          aimAssistLevel = this.m_aimAssistListener.GetAimAssistMeleeLevel();
        } else {
          if Equals(this.m_aimAssistListener.m_currentConfig, AimAssistSettingConfig.DriverCombat) || Equals(this.m_aimAssistListener.m_currentConfig, AimAssistSettingConfig.DriverCombatTPP) || Equals(this.m_aimAssistListener.m_currentConfig, AimAssistSettingConfig.DriverCombatMissiles) || Equals(this.m_aimAssistListener.m_currentConfig, AimAssistSettingConfig.DriverCombatMissilesAiming) || Equals(this.m_aimAssistListener.m_currentConfig, AimAssistSettingConfig.DriverCombatMeleeTPP) {
            if this.m_aimAssistListener.GetAimAssistDriverCombatEnabled() {
              aimAssistLevel = EAimAssistLevel.Light;
            } else {
              aimAssistLevel = EAimAssistLevel.Off;
            };
          } else {
            aimAssistLevel = this.m_aimAssistListener.GetAimAssistLevel();
          };
        };
        if Equals(aimAssistLevel, EAimAssistLevel.Off) {
          configRecord = settingsRecord.Off();
        } else {
          if Equals(aimAssistLevel, EAimAssistLevel.Light) {
            configRecord = settingsRecord.Light();
          } else {
            if Equals(aimAssistLevel, EAimAssistLevel.Heavy) {
              configRecord = settingsRecord.Heavy();
            } else {
              configRecord = settingsRecord.Standard();
            };
          };
        };
        GameInstance.GetTargetingSystem(this.GetGame()).SetAimAssistConfig(this, configRecord.GetID());
      };
    };
  }

  public final const func ShouldAllowCycleToFistCyberware() -> Bool {
    return this.m_accessibilityControlsListener.GetAllowCycleToFistCyberware();
  }

  private final func RegisterInterestingFactsListeners() -> Void {
    this.InitInterestingFacts();
    this.m_interestingFactsListenersIds.m_zone = GameInstance.GetQuestsSystem(this.GetGame()).RegisterListener(this.m_interestingFacts.m_zone, this, this.m_interestingFactsListenersFunctions.m_zone);
    this.InvalidateZone();
  }

  private final func UnregisterInterestingFactsListeners() -> Void {
    GameInstance.GetQuestsSystem(this.GetGame()).UnregisterListener(this.m_interestingFacts.m_zone, this.m_interestingFactsListenersIds.m_zone);
  }

  public final func SetBlackboardIntVariable(id: BlackboardID_Int, value: Int32) -> Void {
    let blackboardSystem: ref<BlackboardSystem> = GameInstance.GetBlackboardSystem(this.GetGame());
    let blackboard: ref<IBlackboard> = blackboardSystem.GetLocalInstanced(this.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    if IsDefined(blackboard) {
      blackboard.SetInt(id, value);
    };
  }

  private final func InvalidateZone() -> Void {
    this.OnZoneFactChanged(GameInstance.GetQuestsSystem(this.GetGame()).GetFact(this.m_interestingFacts.m_zone));
  }

  public final func GetStaminaValueUnsafe() -> Float {
    return this.m_staminaListener.GetStaminaValue();
  }

  public final func GetStaminaPercUnsafe() -> Float {
    return this.m_staminaListener.GetStaminaPerc();
  }

  public final func IsExhausted() -> Bool {
    return StatusEffectSystem.ObjectHasStatusEffect(this, t"BaseStatusEffect.PlayerExhausted");
  }

  public final func GetHealingItemCharges() -> Int32 {
    return this.m_HealingItemsChargeStatListener.GetCharges();
  }

  public final func GetHealingItemUseCost() -> Int32 {
    return this.m_HealingItemsChargeStatListener.GetRechargeDuration();
  }

  public final func HealingItemChargesAreOnMax() -> Bool {
    return this.GetHealingItemUseCost() * this.GetHealingItemCharges() == this.m_HealingItemsChargeStatListener.MaxStatPoolValue();
  }

  public final func GetGrenadeCharges() -> Int32 {
    return this.m_GrenadesChargeStatListener.GetCharges();
  }

  public final func GetGrenadeCharges(item: ref<Grenade_Record>) -> Int32 {
    return this.m_GrenadesChargeStatListener.GetCharges(item);
  }

  public final func GrenadeChargesAreOnMax() -> Bool {
    return this.GetGrenadeThrowCostClean() * this.GetGrenadeCharges() == this.m_GrenadesChargeStatListener.MaxStatPoolValue();
  }

  public final func GetGrenadeThrowCost() -> Int32 {
    return this.m_GrenadesChargeStatListener.GetRechargeDuration();
  }

  public final func GetGrenadeThrowCostClean() -> Int32 {
    return this.m_GrenadesChargeStatListener.GetRechargeDurationClean();
  }

  public final func GetProjectileLauncherCharges() -> Int32 {
    return this.m_ProjectileLauncherChargeStatListener.GetCharges();
  }

  public final func GetProjectileLauncherShootCost() -> Int32 {
    return this.m_ProjectileLauncherChargeStatListener.GetRechargeDuration();
  }

  public final func OnZoneFactChanged(val: Int32) -> Void {
    let zoneType: gameCityAreaType = this.GetCurrentZoneType(val);
    switch zoneType {
      case gameCityAreaType.Undefined:
        this.OnExitPublicZone();
        this.OnExitSafeZone();
        this.OnEnterUndefinedZone();
        break;
      case gameCityAreaType.PublicZone:
        this.OnExitSafeZone();
        this.OnEnterPublicZone();
        break;
      case gameCityAreaType.SafeZone:
        this.OnExitPublicZone();
        this.OnEnterSafeZone();
        break;
      case gameCityAreaType.RestrictedZone:
        this.OnExitPublicZone();
        this.OnExitSafeZone();
        this.OnEnterRestrictedZone();
        break;
      case gameCityAreaType.DangerousZone:
        this.OnExitPublicZone();
        this.OnExitSafeZone();
        this.OnEnterDangerousZone();
    };
  }

  public final func SetSecurityAreaTypeE3HACK(securityAreaType: ESecurityAreaType) -> Void {
    this.m_securityAreaTypeE3HACK = securityAreaType;
    this.InvalidateZone();
  }

  private final func OnEnterUndefinedZone() -> Void;

  private final func OnEnterPublicZone() -> Void {
    let psmEvent: ref<PSMPostponedParameterBool> = new PSMPostponedParameterBool();
    psmEvent.id = n"InPublicZone";
    psmEvent.value = true;
    psmEvent.aspect = gamestateMachineParameterAspect.Permanent;
    this.QueueEvent(psmEvent);
    this.SetBlackboardIntVariable(GetAllBlackboardDefs().PlayerStateMachine.Zones, 1);
    GameInstance.GetAudioSystem(this.GetGame()).NotifyGameTone(n"EnterPublic");
  }

  private final func OnExitPublicZone() -> Void {
    let psmEvent: ref<PSMPostponedParameterBool> = new PSMPostponedParameterBool();
    psmEvent.id = n"InPublicZone";
    psmEvent.value = false;
    psmEvent.aspect = gamestateMachineParameterAspect.Permanent;
    this.QueueEvent(psmEvent);
  }

  private final func OnEnterSafeZone() -> Void {
    let psmEvent: ref<PSMPostponedParameterBool> = new PSMPostponedParameterBool();
    psmEvent.id = n"ForceEmptyHandsByZone";
    psmEvent.value = true;
    psmEvent.aspect = gamestateMachineParameterAspect.Permanent;
    this.QueueEvent(psmEvent);
    this.SetBlackboardIntVariable(GetAllBlackboardDefs().PlayerStateMachine.Zones, 2);
    GameInstance.GetAudioSystem(this.GetGame()).NotifyGameTone(n"EnterSafe");
  }

  private final func OnExitSafeZone() -> Void {
    let psmEvent: ref<PSMPostponedParameterBool> = new PSMPostponedParameterBool();
    psmEvent.id = n"ForceEmptyHandsByZone";
    psmEvent.value = false;
    psmEvent.aspect = gamestateMachineParameterAspect.Permanent;
    this.QueueEvent(psmEvent);
  }

  private final func OnEnterRestrictedZone() -> Void {
    this.SetBlackboardIntVariable(GetAllBlackboardDefs().PlayerStateMachine.Zones, 3);
    GameInstance.GetAudioSystem(this.GetGame()).NotifyGameTone(n"EnterRestricted");
  }

  private final func OnEnterDangerousZone() -> Void {
    GameInstance.GetAudioSystem(this.GetGame()).NotifyGameTone(n"EnterDangerous");
    this.SetBlackboardIntVariable(GetAllBlackboardDefs().PlayerStateMachine.Zones, 4);
  }

  protected final const func GetCurrentZoneType(factValue: Int32) -> gameCityAreaType {
    let questZoneType: gameCityAreaType = IntEnum<gameCityAreaType>(factValue);
    if Equals(questZoneType, gameCityAreaType.SafeZone) || Equals(questZoneType, gameCityAreaType.RestrictedZone) || Equals(questZoneType, gameCityAreaType.DangerousZone) {
      return questZoneType;
    };
    return this.GetCurrentSecurityZoneType(this);
  }

  protected final const func GetCurrentSecurityZoneType(owner: ref<GameObject>) -> gameCityAreaType {
    switch this.m_securityAreaTypeE3HACK {
      case ESecurityAreaType.SAFE:
        return gameCityAreaType.PublicZone;
      case ESecurityAreaType.RESTRICTED:
        return gameCityAreaType.RestrictedZone;
      case ESecurityAreaType.DANGEROUS:
        return gameCityAreaType.DangerousZone;
      default:
        return gameCityAreaType.PublicZone;
    };
    return gameCityAreaType.PublicZone;
  }

  protected cb func OnInvalidateVisionModeController(evt: ref<PlayerVisionModeControllerInvalidateEvent>) -> Bool {
    this.m_visionModeController.OnInvalidateActiveState(evt);
  }

  protected cb func OnInvalidateCombatController(evt: ref<PlayerCombatControllerInvalidateEvent>) -> Bool {
    this.m_combatController.OnInvalidateActiveState(evt);
  }

  protected cb func OnStartedBeingTrackedAsHostile(evt: ref<StartedBeingTrackedAsHostile>) -> Bool {
    this.m_combatController.OnStartedBeingTrackedAsHostile(evt);
  }

  protected cb func OnStoppedBeingTrackedAsHostile(evt: ref<StoppedBeingTrackedAsHostile>) -> Bool {
    this.m_combatController.OnStoppedBeingTrackedAsHostile();
  }

  protected cb func OnCrouchDelayEvent(evt: ref<CrouchDelayEvent>) -> Bool {
    this.m_combatController.OnCrouchDelayEvent(evt);
  }

  public final const func GetCachedQuickHackList() -> [PlayerQuickhackData] {
    let QHList: array<PlayerQuickhackData> = FromVariant<array<PlayerQuickhackData>>(GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().PlayerQuickHackData).GetVariant(GetAllBlackboardDefs().PlayerQuickHackData.CachedQuickHackList));
    return QHList;
  }

  public final static func ChacheQuickHackList(self: wref<PlayerPuppet>, QHList: [PlayerQuickhackData]) -> Void {
    if !IsDefined(self) {
      return;
    };
    GameInstance.GetBlackboardSystem(self.GetGame()).Get(GetAllBlackboardDefs().PlayerQuickHackData).SetVariant(GetAllBlackboardDefs().PlayerQuickHackData.CachedQuickHackList, ToVariant(QHList), true);
  }

  public final static func ChacheQuickHackListCleanup(object: wref<GameObject>) -> Void {
    let QHList: array<PlayerQuickhackData>;
    if !IsDefined(object) {
      return;
    };
    GameInstance.GetBlackboardSystem(object.GetGame()).Get(GetAllBlackboardDefs().PlayerQuickHackData).SetVariant(GetAllBlackboardDefs().PlayerQuickHackData.CachedQuickHackList, ToVariant(QHList), true);
  }

  public final static func GetPlayerQuickHackInCyberDeck(player: wref<PlayerPuppet>) -> [ItemID] {
    let hacksInDeck: array<ItemID>;
    let i: Int32;
    let installedPartId: TweakDBID;
    let parts: array<SPartSlots>;
    let systemReplacementID: ItemID = EquipmentSystem.GetData(player).GetActiveItem(gamedataEquipmentArea.SystemReplacementCW);
    if EquipmentSystem.IsCyberdeckEquipped(player) {
      parts = ItemModificationSystem.GetAllSlots(player, systemReplacementID);
      i = 0;
      while i < ArraySize(parts) {
        if Equals(parts[i].status, ESlotState.Taken) {
          installedPartId = ItemID.GetTDBID(parts[i].installedPart);
          if installedPartId != t"Items.GenericItemRoot" {
            ArrayPush(hacksInDeck, parts[i].installedPart);
          };
        };
        i += 1;
      };
    };
    return hacksInDeck;
  }

  public final static func GetPlayerQuickHackInCyberDeckTweakDBID(player: wref<PlayerPuppet>, quality: gamedataQuality) -> [TweakDBID] {
    let hacksInDeckTweakDBID: array<TweakDBID>;
    let tweakId: TweakDBID;
    let hacksInDeck: array<ItemID> = PlayerPuppet.GetPlayerQuickHackInCyberDeck(player);
    let i: Int32 = 0;
    while i < ArraySize(hacksInDeck) {
      tweakId = ItemID.GetTDBID(hacksInDeck[i]);
      if Equals(TweakDBInterface.GetItemRecord(tweakId).Quality().Type(), quality) {
        ArrayPush(hacksInDeckTweakDBID, tweakId);
      };
      i += 1;
    };
    return hacksInDeckTweakDBID;
  }

  public final static func GetPlayerQuickHackInCyberDeckTweakDBID(player: wref<PlayerPuppet>) -> [TweakDBID] {
    let hacksInDeckTweakDBID: array<TweakDBID>;
    let tweakId: TweakDBID;
    let hacksInDeck: array<ItemID> = PlayerPuppet.GetPlayerQuickHackInCyberDeck(player);
    let i: Int32 = 0;
    while i < ArraySize(hacksInDeck) {
      tweakId = ItemID.GetTDBID(hacksInDeck[i]);
      ArrayPush(hacksInDeckTweakDBID, tweakId);
      i += 1;
    };
    return hacksInDeckTweakDBID;
  }

  protected cb func OnDisableVisualOverride(evt: ref<DisableVisualOverride>) -> Bool {
    let request: ref<QuestDisableWardrobeSetRequest> = new QuestDisableWardrobeSetRequest();
    request.owner = this;
    request.blockReequipping = evt.blockReequipping;
    let equipSys: ref<EquipmentSystem> = this.GetEquipmentSystem();
    equipSys.QueueRequest(request);
  }

  protected cb func OnRestoreVisualOverride(evt: ref<RestoreVisualOverride>) -> Bool {
    let request: ref<QuestRestoreWardrobeSetRequest> = new QuestRestoreWardrobeSetRequest();
    request.owner = this;
    let equipSys: ref<EquipmentSystem> = this.GetEquipmentSystem();
    equipSys.QueueRequest(request);
  }

  protected cb func OnEnableVisualOverride(evt: ref<EnableVisualOverride>) -> Bool {
    let request: ref<QuestEnableWardrobeSetRequest> = new QuestEnableWardrobeSetRequest();
    request.owner = this;
    let equipSys: ref<EquipmentSystem> = this.GetEquipmentSystem();
    equipSys.QueueRequest(request);
  }

  private final const func GetEquipmentSystem() -> ref<EquipmentSystem> {
    return GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
  }

  protected cb func OnHideVisualSlot(evt: ref<HideVisualSlot>) -> Bool {
    let request: ref<QuestHideSlotRequest> = new QuestHideSlotRequest();
    let equipSys: ref<EquipmentSystem> = this.GetEquipmentSystem();
    request.slot = this.GetAreaFromEnum(evt.slot);
    request.owner = this;
    equipSys.QueueRequest(request);
  }

  protected cb func OnRestoreVisualSlot(evt: ref<RestoreVisualSlot>) -> Bool {
    let request: ref<QuestRestoreSlotRequest> = new QuestRestoreSlotRequest();
    let equipSys: ref<EquipmentSystem> = this.GetEquipmentSystem();
    request.slot = this.GetAreaFromEnum(evt.slot);
    request.owner = this;
    equipSys.QueueRequest(request);
  }

  private final const func GetAreaFromEnum(slot: TransmogSlots) -> gamedataEquipmentArea {
    switch slot {
      case TransmogSlots.Head:
        return gamedataEquipmentArea.Head;
      case TransmogSlots.Face:
        return gamedataEquipmentArea.Face;
      case TransmogSlots.InnerChest:
        return gamedataEquipmentArea.InnerChest;
      case TransmogSlots.OuterChest:
        return gamedataEquipmentArea.OuterChest;
      case TransmogSlots.Legs:
        return gamedataEquipmentArea.Legs;
      case TransmogSlots.Feet:
        return gamedataEquipmentArea.Feet;
      default:
        return gamedataEquipmentArea.Invalid;
    };
  }

  protected cb func OnRefreshQuickhackMenuEvent(evt: ref<RefreshQuickhackMenuEvent>) -> Bool {
    let hudManager: ref<HUDManager> = GameInstance.GetScriptableSystemsContainer(this.GetGame()).Get(n"HUDManager") as HUDManager;
    if IsDefined(hudManager) {
      QuickhackModule.RequestRefreshQuickhackMenu(this.GetGame(), hudManager.GetCurrentTargetID());
    };
  }

  protected cb func OnHalloweenEvent(evt: ref<HalloweenEvent>) -> Bool {
    let broadcaster: ref<StimBroadcasterComponent>;
    if Equals(this.m_sceneTier, GameplayTier.Tier1_FullGameplay) || Equals(this.m_sceneTier, GameplayTier.Tier2_StagedGameplay) {
      broadcaster = this.GetStimBroadcasterComponent();
      if IsDefined(broadcaster) {
        broadcaster.TriggerSingleBroadcast(this, gamedataStimType.UndeadCall);
      };
    };
  }

  protected cb func OnVehicleRadioEvent(evt: ref<VehicleRadioEvent>) -> Bool {
    this.m_pocketRadio.HandleVehicleRadioEvent(evt);
  }

  protected cb func OnVehicleRadioStationChanged(evt: ref<VehicleRadioStationChanged>) -> Bool {
    this.m_pocketRadio.HandleVehicleRadioStationChanged(evt);
  }

  protected cb func OnRadioToggleEvent(evt: ref<RadioToggleEvent>) -> Bool {
    this.m_pocketRadio.HandleRadioToggleEvent(evt);
  }

  public final func PSIsInDriverCombat() -> Bool {
    return this.GetPS().IsInDriverCombat();
  }

  public final func SetPSIsInDriverCombat(isInDriverCombat: Bool) -> Void {
    this.GetPS().SetIsInDriverCombat(isInDriverCombat);
  }

  public final func PSGetPocketRadioStation() -> Int32 {
    return this.GetPS().GetPocketRadioStation();
  }

  public final func PSSetPocketRadioStation(value: Int32) -> Void {
    this.GetPS().SetPocketRadioStation(value);
  }

  public final func OnExplosiveDeviceExploded() -> Void {
    if GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.HasGrenadeHack) == 1.00 && GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.ExplosionKillsRecudeUltimateHacksCost) == 1.00 {
      StatusEffectHelper.ApplyStatusEffect(this, t"BaseStatusEffect.ReduceUltimateSuicideWithGrenadeCostPlusPlus");
      StatusEffectHelper.RemoveStatusEffect(this, t"BaseStatusEffect.ReduceUltimateSuicideWithGrenadeCost");
    } else {
      if GameInstance.GetStatsSystem(this.GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetEntityID()), gamedataStatType.HasGrenadeHack) == 1.00 {
        StatusEffectHelper.ApplyStatusEffect(this, t"BaseStatusEffect.ReduceUltimateSuicideWithGrenadeCost");
        StatusEffectHelper.RemoveStatusEffect(this, t"BaseStatusEffect.ReduceUltimateSuicideWithGrenadeCostPlusPlus");
      };
    };
  }

  private final func PromoteOpticalCamoEffectorToCompletelyBlocking() -> Void {
    let appliedEffectors: array<wref<Effector>>;
    let i: Int32;
    let opticalCamoEffector: ref<AttachOpticalCamoVisionBlockerEffector>;
    GameInstance.GetEffectorSystem(this.GetGame()).GetEffectorsList(this.GetEntityID(), appliedEffectors);
    i = 0;
    while i < ArraySize(appliedEffectors) {
      opticalCamoEffector = appliedEffectors[i] as AttachOpticalCamoVisionBlockerEffector;
      if !IsDefined(opticalCamoEffector) {
      } else {
        opticalCamoEffector.SetBlockingCompletely(true);
      };
      i += 1;
    };
  }
}
