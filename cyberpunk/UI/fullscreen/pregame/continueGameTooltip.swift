
public class ContinueGameTooltip extends inkLogicController {

  private edit let m_mainContainer: inkWidgetRef;

  private edit let m_imageReplacement: inkImageRef;

  private edit let m_networkStatusError: inkWidgetRef;

  private edit let m_networkSyncingIndicator: inkWidgetRef;

  private edit let m_label: inkTextRef;

  private edit let m_labelDate: inkTextRef;

  private edit let m_location: inkTextRef;

  private edit let m_quest: inkTextRef;

  private edit let m_level: inkTextRef;

  private edit let m_lifepath: inkImageRef;

  private edit let m_cloudStatus: inkImageRef;

  private edit let m_playTime: inkTextRef;

  private let m_saveFileStatus: inkSaveStatus;

  private let m_cloudSaveStatus: CloudSavesQueryStatus;

  private let m_metaDataLoaded: Bool;

  private let m_isOffline: Bool;

  @default(ContinueGameTooltip, base\gameplay\gui\fullscreen\load_game\save_game.inkatlas)
  private const let m_defaultAtlasPath: ResRef;

  protected cb func OnInitialize() -> Bool {
    inkWidgetRef.SetVisible(this.m_networkSyncingIndicator, true);
    inkWidgetRef.SetVisible(this.m_networkStatusError, false);
    inkWidgetRef.SetVisible(this.m_mainContainer, false);
  }

  public final func SetMetadata(metadata: ref<SaveMetadataInfo>, opt isEp1Enabled: Bool) -> Void {
    let finalString: String;
    let hrs: Int32;
    let mins: Int32;
    let playthroughTime: Float;
    let shrs: String;
    let smins: String;
    let isEp1Save: Bool = metadata.IsEp1Save();
    if !isEp1Enabled && isEp1Save {
      inkWidgetRef.SetVisible(this.m_label, false);
      inkTextRef.SetLocalizedTextScript(this.m_quest, "LocKey#92500");
      inkWidgetRef.SetVisible(this.m_location, false);
    } else {
      inkTextRef.SetText(this.m_label, metadata.internalName);
      inkTextRef.SetText(this.m_quest, metadata.trackedQuest);
      inkTextRef.SetText(this.m_location, metadata.locationName);
    };
    playthroughTime = MaxF(Cast<Float>(metadata.playthroughTime), Cast<Float>(metadata.playTime));
    hrs = RoundF(playthroughTime / 3600.00);
    mins = RoundF((playthroughTime % 3600.00) / 60.00);
    if hrs > 9 {
      shrs = ToString(hrs);
    } else {
      shrs = ToString(hrs);
    };
    if mins > 9 {
      smins = ToString(mins);
    } else {
      smins = ToString(mins);
    };
    if hrs != 0 {
      finalString = shrs + GetLocalizedText("UI-Labels-Units-Hours");
    };
    if mins != 0 {
      finalString = hrs != 0 ? finalString + " " : finalString;
      finalString = finalString + smins + GetLocalizedText("UI-Labels-Units-Minutes");
    };
    inkWidgetRef.SetVisible(this.m_playTime, true);
    inkTextRef.SetText(this.m_playTime, finalString);
    inkWidgetRef.SetVisible(this.m_labelDate, true);
    inkTextRef.SetDateTimeByTimestamp(this.m_labelDate, metadata.timestamp);
    if Equals(metadata.lifePath, inkLifePath.Corporate) {
      inkImageRef.SetTexturePart(this.m_lifepath, n"LifepathCorpo1");
      inkTextRef.SetText(this.m_level, "Gameplay-LifePaths-Corporate");
    };
    if Equals(metadata.lifePath, inkLifePath.Nomad) {
      inkImageRef.SetTexturePart(this.m_lifepath, n"LifepathNomad1");
      inkTextRef.SetText(this.m_level, "Gameplay-LifePaths-Nomad");
    };
    if Equals(metadata.lifePath, inkLifePath.StreetKid) {
      inkImageRef.SetTexturePart(this.m_lifepath, n"LifepathStreetKid1");
      inkTextRef.SetText(this.m_level, "Gameplay-LifePaths-Streetkid");
    };
    this.m_saveFileStatus = metadata.saveStatus;
    switch metadata.saveStatus {
      case inkSaveStatus.Local:
      case inkSaveStatus.Invalid:
        inkWidgetRef.SetVisible(this.m_cloudStatus, false);
        break;
      case inkSaveStatus.Upload:
        inkImageRef.SetTexturePart(this.m_cloudStatus, n"icon_cloud_upload");
        inkWidgetRef.SetVisible(this.m_cloudStatus, true);
        break;
      case inkSaveStatus.Cloud:
        inkImageRef.SetTexturePart(this.m_cloudStatus, n"icon_cloud");
        inkWidgetRef.SetVisible(this.m_cloudStatus, true);
        break;
      case inkSaveStatus.InSync:
        inkImageRef.SetTexturePart(this.m_cloudStatus, n"icon_cloud_insync");
        inkWidgetRef.SetVisible(this.m_cloudStatus, true);
    };
    this.m_metaDataLoaded = true;
    inkWidgetRef.SetVisible(this.m_networkSyncingIndicator, false);
    inkWidgetRef.SetVisible(this.m_mainContainer, true);
  }

  public final func UpdateNetworkStatus(status: CloudSavesQueryStatus) -> Void {
    this.m_cloudSaveStatus = status;
    this.UpdateStatus();
  }

  public final func SetOfflineStatus(value: Bool) -> Void {
    this.m_isOffline = value;
    this.UpdateStatus();
  }

  public final func SetInvalid(metadata: ref<SaveMetadataInfo>) -> Void {
    inkWidgetRef.SetVisible(this.m_labelDate, false);
    inkWidgetRef.SetVisible(this.m_playTime, false);
    inkWidgetRef.SetVisible(this.m_label, true);
    inkWidgetRef.SetVisible(this.m_quest, true);
    inkTextRef.SetText(this.m_label, "UI-Menus-Saving-CorruptedSaveTitle");
    inkTextRef.SetText(this.m_quest, metadata.internalName);
    switch metadata.saveStatus {
      case inkSaveStatus.Upload:
      case inkSaveStatus.Local:
      case inkSaveStatus.Invalid:
        inkWidgetRef.SetVisible(this.m_cloudStatus, false);
        break;
      case inkSaveStatus.Cloud:
        inkImageRef.SetTexturePart(this.m_cloudStatus, n"icon_cloud");
        inkWidgetRef.SetVisible(this.m_cloudStatus, true);
        break;
      case inkSaveStatus.InSync:
        inkImageRef.SetTexturePart(this.m_cloudStatus, n"icon_cloud_insync");
        inkWidgetRef.SetVisible(this.m_cloudStatus, true);
    };
    this.m_metaDataLoaded = false;
    inkWidgetRef.SetVisible(this.m_networkSyncingIndicator, false);
    inkWidgetRef.SetVisible(this.m_mainContainer, true);
  }

  public final func DisplayDataSyncIndicator(value: Bool) -> Void {
    inkWidgetRef.SetVisible(this.m_networkStatusError, false);
    inkWidgetRef.SetVisible(this.m_networkSyncingIndicator, value);
  }

  public final func IsBusy() -> Bool {
    return inkWidgetRef.IsVisible(this.m_networkSyncingIndicator);
  }

  public final func GetPreviewImageWidget() -> wref<inkImage> {
    return inkWidgetRef.Get(this.m_imageReplacement) as inkImage;
  }

  public final func CheckThumbnailCensorship(IsBuildCensored: Bool) -> Void {
    if IsBuildCensored && this.IsCloudSave() {
      inkImageRef.SetAtlasResource(this.m_imageReplacement, this.m_defaultAtlasPath);
      inkImageRef.SetActiveTextureType(this.m_imageReplacement, inkTextureType.StaticTexture);
      inkImageRef.SetTexturePart(this.m_imageReplacement, n"cross_prog_icon");
    };
  }

  public final func IsCloudSave() -> Bool {
    return Equals(this.m_saveFileStatus, inkSaveStatus.Cloud) || Equals(this.m_saveFileStatus, inkSaveStatus.InSync);
  }

  private final func UpdateStatus() -> Void {
    if Equals(this.m_cloudSaveStatus, CloudSavesQueryStatus.FetchFailed) || this.m_isOffline {
      inkWidgetRef.SetVisible(this.m_networkStatusError, true);
      inkWidgetRef.SetVisible(this.m_networkSyncingIndicator, false);
    } else {
      if this.m_metaDataLoaded {
        inkWidgetRef.SetVisible(this.m_networkSyncingIndicator, false);
      };
    };
  }
}
