
public abstract class BaseScriptableAction extends DeviceAction {

  protected let m_requesterID: EntityID;

  protected let m_executor: wref<GameObject>;

  protected let m_proxyExecutor: wref<GameObject>;

  protected let m_costComponents: [wref<ObjectActionCost_Record>];

  protected let m_objectActionID: TweakDBID;

  protected let m_objectActionRecord: wref<ObjectAction_Record>;

  protected let m_inkWidgetID: TweakDBID;

  protected let interactionChoice: InteractionChoice;

  protected let m_interactionLayer: CName;

  protected let m_isActionRPGCheckDissabled: Bool;

  protected let m_canSkipPayCost: Bool;

  protected let m_calculatedBaseCost: Int32;

  public let m_deviceActionQueue: ref<DeviceActionQueue>;

  public let m_isActionQueueingUsed: Bool;

  public let m_isQueuedAction: Bool;

  public let m_isInactive: Bool;

  public let m_isTargetDead: Bool;

  public let m_activationTimeReduction: Float;

  public let m_IsAppliedByMonowire: Bool;

  protected func GetOwnerPS(game: GameInstance) -> ref<PersistentState> {
    let psID: PersistentID = this.GetPersistentID();
    if PersistentID.IsDefined(psID) {
      return GameInstance.GetPersistencySystem(game).GetConstAccessToPSObject(psID, this.GetDeviceClassName());
    };
    return null;
  }

  public final func RegisterAsRequester(id: EntityID) -> Void {
    this.m_requesterID = id;
  }

  public final func GetRequesterID() -> EntityID {
    return this.m_requesterID;
  }

  public final func SetExecutor(executor: wref<GameObject>) -> Void {
    this.m_executor = executor;
  }

  public final const func GetExecutor() -> wref<GameObject> {
    return this.m_executor;
  }

  public final func SetProxyExecutor(proxy: wref<GameObject>) -> Void {
    this.m_proxyExecutor = proxy;
  }

  public final const func GetProxyExecutor() -> wref<GameObject> {
    return this.m_proxyExecutor;
  }

  public final const func GetDeviceActionQueue() -> wref<DeviceActionQueue> {
    return this.m_deviceActionQueue;
  }

  public final const func GetDeviceActionQueueSize() -> Int32 {
    if IsDefined(this.m_deviceActionQueue) {
      return this.m_deviceActionQueue.GetQueueSize();
    };
    return 0;
  }

  public final const func GetDeviceActionMaxQueueSize() -> Int32 {
    if IsDefined(this.m_deviceActionQueue) {
      return this.m_deviceActionQueue.GetMaxQueueSize();
    };
    return 1;
  }

  public final const func GetDeviceActionQueueNames() -> [CName] {
    let actionNames: array<CName>;
    this.m_deviceActionQueue.GetAllQueuedActionNames(actionNames);
    return actionNames;
  }

  public final const func IsFirstUniqueCategoryInQueue(targetID: EntityID, category: gamedataHackCategory) -> Bool {
    let currentlyUploadingAction: wref<ScriptableDeviceAction>;
    let deviceActionQueue: wref<DeviceActionQueue>;
    let hackCategory: gamedataHackCategory;
    let i: Int32;
    let objectActionRecords: array<ref<ObjectAction_Record>>;
    let targetPuppet: ref<ScriptedPuppet> = GameInstance.FindEntityByID(this.GetExecutor().GetGame(), targetID) as ScriptedPuppet;
    let isNewCategory: Bool = true;
    if !IsDefined(targetPuppet) {
      return false;
    };
    currentlyUploadingAction = targetPuppet.GetCurrentlyUploadingAction();
    if currentlyUploadingAction == null || currentlyUploadingAction.m_isInactive {
      return false;
    };
    hackCategory = currentlyUploadingAction.GetObjectActionRecord().HackCategory().Type();
    if Equals(hackCategory, category) {
      return false;
    };
    deviceActionQueue = currentlyUploadingAction.GetDeviceActionQueue();
    if IsDefined(deviceActionQueue) {
      deviceActionQueue.GetAllQueuedActionObjectRecords(objectActionRecords);
      i = 0;
      while i < ArraySize(objectActionRecords) {
        hackCategory = objectActionRecords[i].HackCategory().Type();
        if Equals(hackCategory, category) {
          isNewCategory = false;
          break;
        };
        i += 1;
      };
    };
    return isNewCategory;
  }

  public final const func GetActionID() -> CName {
    let id: CName;
    if TDBID.IsValid(this.GetObjectActionID()) {
      id = this.GetObjectActionRecord().ActionName();
    } else {
      if NotEquals(this.GetClassName(), this.actionName) {
        id = this.actionName;
      } else {
        id = this.GetClassName();
      };
    };
    return id;
  }

  public const func GetObjectActionRecord() -> wref<ObjectAction_Record> {
    if IsDefined(this.m_objectActionRecord) {
      return this.m_objectActionRecord;
    };
    return TweakDBInterface.GetObjectActionRecord(this.m_objectActionID);
  }

  public const func GetObjectActionID() -> TweakDBID {
    let tweakDBID: TweakDBID;
    let record: wref<ObjectAction_Record> = this.GetObjectActionRecord();
    if IsDefined(record) {
      tweakDBID = record.GetID();
    };
    return tweakDBID;
  }

  public final const func GetGameplayCategoryID() -> TweakDBID {
    let returnValue: TweakDBID;
    let record: wref<ObjectAction_Record> = this.GetObjectActionRecord();
    if IsDefined(record) && IsDefined(this.GetObjectActionRecord().GameplayCategory()) {
      returnValue = this.GetObjectActionRecord().GameplayCategory().GetID();
    };
    return returnValue;
  }

  public final const func GetGameplayCategoryRecord() -> wref<ObjectActionGameplayCategory_Record> {
    let returnValue: wref<ObjectActionGameplayCategory_Record>;
    let record: wref<ObjectAction_Record> = this.GetObjectActionRecord();
    if IsDefined(record) {
      returnValue = this.GetObjectActionRecord().GameplayCategory();
    };
    return returnValue;
  }

  public func SetObjectActionID(id: TweakDBID) -> Void {
    this.m_objectActionID = id;
    this.m_objectActionRecord = null;
    this.m_objectActionRecord = this.GetObjectActionRecord();
    ArrayClear(this.m_costComponents);
    if IsDefined(this.m_objectActionRecord) {
      this.actionName = this.m_objectActionRecord.ActionName();
    };
    this.ProduceInteractionPart();
  }

  public func GetTweakDBChoiceRecord() -> String {
    let recordName: String;
    if IsDefined(this.GetObjectActionRecord()) && IsDefined(this.GetObjectActionRecord().ObjectActionUI()) && TDBID.IsValid(this.m_objectActionID) {
      recordName = this.GetObjectActionRecord().ObjectActionUI().Name();
    };
    if IsStringValid(recordName) {
      return recordName;
    };
    return NameToString(this.actionName);
  }

  public func GetTweakDBChoiceID() -> TweakDBID {
    let id: TweakDBID;
    return id;
  }

  public final func SetIsActionRPGCheckDissabled(value: Bool) -> Void {
    if value && this.IsInactive() {
      this.SetActive();
    };
    this.m_isActionRPGCheckDissabled = value;
  }

  public final const func GetIsActionRPGCheckDissabled() -> Bool {
    return this.m_isActionRPGCheckDissabled;
  }

  public final func SetInactive() -> Void {
    if !this.GetIsActionRPGCheckDissabled() {
      ChoiceTypeWrapper.SetType(this.interactionChoice.choiceMetaData.type, gameinteractionsChoiceType.Inactive);
    };
  }

  public final func SetActive() -> Void {
    ChoiceTypeWrapper.ClearType(this.interactionChoice.choiceMetaData.type, gameinteractionsChoiceType.Inactive);
  }

  public final const func IsInactive() -> Bool {
    return ChoiceTypeWrapper.IsType(this.interactionChoice.choiceMetaData.type, gameinteractionsChoiceType.Inactive);
  }

  public final const func IsInteractionChoiceValid() -> Bool {
    if !IsStringValid(this.interactionChoice.choiceMetaData.tweakDBName) && !TDBID.IsValid(this.interactionChoice.choiceMetaData.tweakDBID) {
      return false;
    };
    return true;
  }

  protected final func ProduceInteractionPart() -> Void {
    let cost: Int32;
    let costPart: ref<InteractionChoiceCaptionQuickhackCostPart>;
    this.interactionChoice.choiceMetaData.tweakDBName = this.GetTweakDBChoiceRecord();
    if !this.IsInteractionChoiceValid() {
      return;
    };
    InteractionChoiceCaption.Clear(this.interactionChoice.captionParts);
    if IsDefined(this.GetObjectActionRecord()) && IsDefined(this.GetObjectActionRecord().ObjectActionUI()) {
      InteractionChoiceCaption.AddPartFromRecord(this.interactionChoice.captionParts, this.GetObjectActionRecord().ObjectActionUI().CaptionIcon());
    };
    cost = this.GetCost();
    if cost > 0 {
      costPart = new InteractionChoiceCaptionQuickhackCostPart();
      costPart.cost = cost;
      InteractionChoiceCaption.AddScriptPart(this.interactionChoice.captionParts, costPart);
    };
    ChoiceTypeWrapper.SetType(this.interactionChoice.choiceMetaData.type, gameinteractionsChoiceType.CheckSuccess);
  }

  public func IsPossible(target: wref<GameObject>, opt actionRecord: wref<ObjectAction_Record>, opt objectActionsCallbackController: wref<gameObjectActionsCallbackController>) -> Bool {
    let targetPrereqs: array<wref<IPrereq_Record>>;
    if !IsDefined(actionRecord) {
      actionRecord = this.GetObjectActionRecord();
    };
    if IsDefined(objectActionsCallbackController) && objectActionsCallbackController.HasObjectAction(actionRecord) {
      return objectActionsCallbackController.IsObjectActionTargetPrereqFulfilled(actionRecord);
    };
    actionRecord.TargetPrereqs(targetPrereqs);
    return RPGManager.CheckPrereqs(targetPrereqs, target);
  }

  public func CanInterrupt(target: wref<GameObject>, opt actionRecord: wref<ObjectAction_Record>) -> Bool {
    let interruptionPrereqs: array<wref<IPrereq_Record>>;
    if !IsDefined(actionRecord) {
      actionRecord = this.GetObjectActionRecord();
    };
    actionRecord.InterruptionPrereqs(interruptionPrereqs);
    return RPGManager.CheckPrereqs(interruptionPrereqs, target);
  }

  public func IsVisible(const context: script_ref<GetActionsContext>, opt objectActionsCallbackController: wref<gameObjectActionsCallbackController>) -> Bool {
    let actionRecord: wref<ObjectAction_Record>;
    let instigatorPrereqs: array<wref<IPrereq_Record>>;
    if !IsNameValid(Deref(context).interactionLayerTag) {
      return false;
    };
    actionRecord = this.GetObjectActionRecord();
    if Equals(actionRecord.InteractionLayer(), n"any") || Equals(Deref(context).interactionLayerTag, actionRecord.InteractionLayer()) {
      if IsDefined(objectActionsCallbackController) && objectActionsCallbackController.HasObjectAction(actionRecord) {
        return objectActionsCallbackController.IsObjectActionInstigatorPrereqFulfilled(actionRecord);
      };
      actionRecord.InstigatorPrereqs(instigatorPrereqs);
      return RPGManager.CheckPrereqs(instigatorPrereqs, Deref(context).processInitiatorObject);
    };
    return false;
  }

  public func IsVisible(player: wref<GameObject>, opt objectActionsCallbackController: wref<gameObjectActionsCallbackController>) -> Bool {
    let instigatorPrereqs: array<wref<IPrereq_Record>>;
    let actionRecord: wref<ObjectAction_Record> = this.GetObjectActionRecord();
    if IsDefined(objectActionsCallbackController) && objectActionsCallbackController.HasObjectAction(actionRecord) {
      return objectActionsCallbackController.IsObjectActionInstigatorPrereqFulfilled(actionRecord);
    };
    actionRecord.InstigatorPrereqs(instigatorPrereqs);
    return RPGManager.CheckPrereqs(instigatorPrereqs, player);
  }

  public func ProcessRPGAction(gameInstance: GameInstance, opt gameplayRoleComponent: ref<GameplayRoleComponent>) -> Void {
    this.SetProxyExecutor(this.GetExecutor().TryGetControlledProxy());
    if this.GetActivationTime() > 0.00 {
      if this.PutActionInQuickhackQueue(gameInstance, gameplayRoleComponent) {
        return;
      };
    };
    if this.CanSkipPayCost(false) || this.PayCost(true) {
      this.StartAction(gameInstance);
      if this.GetActivationTime() > 0.00 {
        this.StartUpload(gameInstance);
      } else {
        this.CompleteAction(gameInstance);
      };
      this.m_calculatedBaseCost = this.GetBaseCost();
    };
  }

  public func StartAction(gameInstance: GameInstance) -> Void {
    let actionEffects: array<wref<ObjectActionEffect_Record>>;
    let player: ref<PlayerPuppet>;
    let objectActionRecord: ref<ObjectAction_Record> = this.GetObjectActionRecord();
    if IsDefined(objectActionRecord) {
      objectActionRecord.StartEffects(actionEffects);
    };
    this.ProcessStatusEffects(actionEffects, gameInstance);
    this.ProcessEffectors(actionEffects, gameInstance);
    if IsDefined(objectActionRecord) && IsDefined(objectActionRecord.Cooldown()) && IsDefined(this.GetExecutor()) {
      if TDBID.IsValid(objectActionRecord.Cooldown().GetID()) && this.GetExecutor().IsPlayer() {
        player = this.GetExecutor() as PlayerPuppet;
        if IsDefined(player) {
          player.GetCooldownStorage().StartSimpleCooldown(this);
        };
      };
    };
  }

  public func CompleteAction(gameInstance: GameInstance) -> Void {
    let actionEffects: array<wref<ObjectActionEffect_Record>>;
    let i: Int32;
    let rewards: array<wref<RewardBase_Record>>;
    let actionRecord: ref<ObjectAction_Record> = this.GetObjectActionRecord();
    if IsDefined(actionRecord) {
      actionRecord.Rewards(rewards);
    };
    i = 0;
    while i < ArraySize(rewards) {
      RPGManager.GiveReward(gameInstance, rewards[i].GetID(), Cast<StatsObjectID>(this.GetRequesterID()));
      i += 1;
    };
    if IsDefined(actionRecord) {
      actionRecord.CompletionEffects(actionEffects);
      if NotEquals(actionRecord.HackCategory().Type(), gamedataHackCategory.NotAHack) {
        RPGManager.AwardExperienceFromQuickhack(this.GetExecutor() as PlayerPuppet, Cast<Float>(this.m_calculatedBaseCost), this.m_requesterID, actionRecord.HackCategory().Type());
      };
    };
    this.ProcessStatusEffects(actionEffects, gameInstance);
    this.ProcessEffectors(actionEffects, gameInstance);
  }

  private func StartUpload(gameInstance: GameInstance) -> Void {
    return;
  }

  private final func PutActionInQuickhackQueue(gameInstance: GameInstance, gameplayRoleComponent: ref<GameplayRoleComponent>) -> Bool {
    let requesterObject: ref<GameObject>;
    let slotName: CName;
    let isActionQueued: Bool = false;
    let sAction: ref<ScriptableDeviceAction> = this as ScriptableDeviceAction;
    if IsDefined(sAction) && sAction.IsQuickHack() {
      requesterObject = GameInstance.FindEntityByID(gameInstance, this.m_requesterID) as GameObject;
      if IsDefined(requesterObject) {
        slotName = this.GetExecutor().GetQuickHackIndicatorSlotName();
        isActionQueued = QuickHackableQueueHelper.PutActionInQuickhackQueue(sAction, gameplayRoleComponent, gameInstance, slotName, requesterObject);
      };
      if !sAction.m_isQueuedAction && sAction.GetExecutor().IsPlayer() {
        RPGManager.IncrementQuickHackBlackboard(gameInstance, sAction.GetObjectActionID());
      };
    };
    return isActionQueued;
  }

  protected func ProcessStatusEffects(const actionEffects: script_ref<[wref<ObjectActionEffect_Record>]>, gameInstance: GameInstance) -> Void {
    let i: Int32;
    let proxyId: EntityID;
    let instigator: ref<GameObject> = this.GetExecutor();
    let proxy: ref<GameObject> = this.GetProxyExecutor();
    if IsDefined(proxy) {
      proxyId = proxy.GetEntityID();
    };
    i = 0;
    while i < ArraySize(Deref(actionEffects)) {
      switch Deref(actionEffects)[i].Recipient().Type() {
        case gamedataObjectActionReference.Instigator:
          this.ResetStatusEffectIfActionIsQueued(instigator.GetEntityID(), Deref(actionEffects)[i].StatusEffect(), gameInstance);
          StatusEffectHelper.ApplyStatusEffect(instigator, Deref(actionEffects)[i].StatusEffect().GetID(), instigator.GetEntityID(), proxyId);
          break;
        case gamedataObjectActionReference.Target:
          this.ResetStatusEffectIfActionIsQueued(this.m_requesterID, Deref(actionEffects)[i].StatusEffect(), gameInstance);
          GameInstance.GetStatusEffectSystem(gameInstance).ApplyStatusEffect(this.m_requesterID, Deref(actionEffects)[i].StatusEffect().GetID(), GameObject.GetTDBID(instigator), instigator.GetEntityID(), 1u, new Vector4(0.00, 0.00, 0.00, 0.00), true, proxyId);
          break;
        case gamedataObjectActionReference.Source:
      };
      i += 1;
    };
  }

  protected final func ProcessEffectors(const actionEffects: script_ref<[wref<ObjectActionEffect_Record>]>, gameInstance: GameInstance) -> Void {
    let i: Int32;
    let proxyId: EntityID;
    let instigator: ref<GameObject> = this.GetExecutor();
    let proxy: ref<GameObject> = this.GetProxyExecutor();
    if IsDefined(proxy) {
      proxyId = proxy.GetEntityID();
    };
    i = 0;
    while i < ArraySize(Deref(actionEffects)) {
      switch Deref(actionEffects)[i].Recipient().Type() {
        case gamedataObjectActionReference.Instigator:
          GameInstance.GetEffectorSystem(gameInstance).ApplyEffector(this.GetExecutor().GetEntityID(), instigator, Deref(actionEffects)[i].EffectorToTrigger().GetID(), TDBID.None(), proxyId);
          break;
        case gamedataObjectActionReference.Target:
          GameInstance.GetEffectorSystem(gameInstance).ApplyEffector(this.m_requesterID, instigator, Deref(actionEffects)[i].EffectorToTrigger().GetID(), TDBID.None(), proxyId);
          break;
        case gamedataObjectActionReference.Source:
      };
      i += 1;
    };
  }

  protected final func ResetStatusEffectIfActionIsQueued(entityID: EntityID, statusEffectRecord: ref<StatusEffect_Record>, gameInstance: GameInstance) -> Void {
    if this.m_isActionQueueingUsed && !QuickHackableQueueHelper.IsStatusEffectStackable(statusEffectRecord) {
      GameInstance.GetStatusEffectSystem(gameInstance).RemoveStatusEffect(entityID, statusEffectRecord.GetID());
    };
  }

  public func GetActivationTime() -> Float {
    let executor: ref<GameObject>;
    let timeMods: array<wref<StatModifier_Record>>;
    let uploadTime: Float;
    if IsDefined(this.GetObjectActionRecord()) {
      this.GetObjectActionRecord().ActivationTime(timeMods);
    };
    executor = this.GetExecutor();
    if IsDefined(executor) && ArraySize(timeMods) > 0 {
      if executor.IsPlayer() && ArraySize(timeMods) > 1 {
        uploadTime = RPGManager.CalculateStatModifiers(GameInstance.GetStatsDataSystem(this.GetExecutor().GetGame()).GetValueFromCurve(n"puppet_dynamic_scaling", this.GetPowerLevelDiff(), n"pl_diff_to_upload_time_modifier"), 1.00, 0.00, timeMods, executor.GetGame(), executor, Cast<StatsObjectID>(this.GetRequesterID()), Cast<StatsObjectID>(executor.GetEntityID()));
      } else {
        uploadTime = RPGManager.CalculateStatModifiers(timeMods, executor.GetGame(), executor, Cast<StatsObjectID>(this.GetRequesterID()), Cast<StatsObjectID>(executor.GetEntityID()));
      };
    };
    if this.m_activationTimeReduction > 0.00 && this.m_activationTimeReduction < 1.00 {
      uploadTime -= uploadTime * this.m_activationTimeReduction;
    };
    return uploadTime;
  }

  public final func GetCooldownDuration() -> Float {
    if IsDefined(this.GetObjectActionRecord()) {
      return this.GetObjectActionRecord().Cooldown().Duration();
    };
    return 0.00;
  }

  public final func IsEyesInTheSkyPerk() -> Bool {
    let hackCategory: gamedataHackCategory;
    let playerBlackboard: ref<IBlackboard>;
    let isControlling: Bool = false;
    let isCategory: Bool = false;
    let isPerk: Bool = false;
    let playerPuppet: ref<PlayerPuppet> = GetPlayer(this.GetExecutor().GetGame());
    if !IsDefined(playerPuppet) {
      return false;
    };
    hackCategory = this.GetObjectActionRecord().HackCategory().Type();
    playerBlackboard = GameInstance.GetBlackboardSystem(this.GetExecutor().GetGame()).GetLocalInstanced(playerPuppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    isCategory = Equals(hackCategory, gamedataHackCategory.CovertHack) || Equals(hackCategory, gamedataHackCategory.ControlHack) || Equals(hackCategory, gamedataHackCategory.DeviceHack);
    isControlling = playerBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsControllingDevice);
    isPerk = Cast<Bool>(PlayerDevelopmentSystem.GetData(this.GetExecutor()).IsNewPerkBought(gamedataNewPerkType.Intelligence_Left_Milestone_1));
    return isCategory && isControlling && isPerk;
  }

  public final func GetModifiedDurationTime(baseValue: Float) -> Float {
    if this.IsEyesInTheSkyPerk() {
      return baseValue + baseValue * TweakDBInterface.GetFloat(t"NewPerks.Intelligence_Left_Milestone_1.durationIncease", 0.00);
    };
    return baseValue;
  }

  public func GetDurationTime() -> Float {
    let durationTime: Float;
    let executor: ref<GameObject>;
    let timeMods: array<wref<StatModifier_Record>>;
    if IsDefined(this.GetObjectActionRecord()) {
      this.GetObjectActionRecord().DurationTime(timeMods);
    };
    executor = this.GetExecutor();
    if IsDefined(executor) && ArraySize(timeMods) > 0 {
      durationTime = RPGManager.CalculateStatModifiers(timeMods, executor.GetGame(), executor, Cast<StatsObjectID>(this.GetRequesterID()), Cast<StatsObjectID>(executor.GetEntityID()));
    };
    return this.GetModifiedDurationTime(durationTime);
  }

  public final func CanPayCost(opt user: ref<GameObject>, opt checkForOverclockedState: Bool) -> Bool {
    let executor: ref<GameObject>;
    let executorQuantity: Int32;
    let i: Int32;
    let itemCost: wref<ItemCost_Record>;
    let overclockIsActivated: Bool;
    let quantity: Int32;
    let statPoolCost: wref<StatPoolCost_Record>;
    let statPoolSys: ref<StatPoolsSystem>;
    let statPoolType: gamedataStatPoolType;
    let transactionSys: ref<TransactionSystem>;
    if IsDefined(user) {
      executor = user;
    } else {
      executor = this.GetExecutor();
    };
    if ArraySize(this.m_costComponents) == 0 {
      this.GetObjectActionRecord().Costs(this.m_costComponents);
    };
    i = 0;
    while i < ArraySize(this.m_costComponents) {
      quantity = this.GetCost();
      itemCost = this.m_costComponents[i] as ItemCost_Record;
      statPoolCost = this.m_costComponents[i] as StatPoolCost_Record;
      if IsDefined(itemCost) {
        transactionSys = GameInstance.GetTransactionSystem(executor.GetGame());
        executorQuantity = transactionSys.GetItemQuantity(executor, ItemID.CreateQuery(itemCost.Item().GetID()));
      } else {
        if IsDefined(statPoolCost) {
          statPoolSys = GameInstance.GetStatPoolsSystem(executor.GetGame());
          statPoolType = statPoolCost.StatPool().StatPoolType();
          executorQuantity = FloorF(statPoolSys.GetStatPoolValue(Cast<StatsObjectID>(executor.GetEntityID()), statPoolType, false));
        };
      };
      if executorQuantity < quantity {
        overclockIsActivated = QuickHackableHelper.IsOverclockedStateActive(this.GetExecutor());
        if checkForOverclockedState && overclockIsActivated {
          if !QuickHackableHelper.CanPayWithHealthInOverclockedState(this.GetExecutor(), quantity, 0.00) {
            return false;
          };
        } else {
          return false;
        };
      };
      i += 1;
    };
    return true;
  }

  public final func CanSkipPayCost(isJustConsulting: Bool) -> Bool {
    let amountOfFreeSimultaneousActions: Int32;
    let playerBlackboard: ref<IBlackboard>;
    let playerFreeCostActionIDName: TweakDBID;
    let playerPuppet: ref<PlayerPuppet>;
    if this.m_canSkipPayCost {
      return true;
    };
    playerPuppet = this.GetExecutor() as PlayerPuppet;
    if !IsDefined(playerPuppet) {
      return false;
    };
    playerBlackboard = GameInstance.GetBlackboardSystem(playerPuppet.GetGame()).GetLocalInstanced(playerPuppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    playerFreeCostActionIDName = FromVariant<TweakDBID>(playerBlackboard.GetVariant(GetAllBlackboardDefs().PlayerStateMachine.CostFreeActionID));
    amountOfFreeSimultaneousActions = playerBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.AmountOfCostFreeActions);
    if this.GetObjectActionID() == playerFreeCostActionIDName {
      if amountOfFreeSimultaneousActions > 0 {
        amountOfFreeSimultaneousActions -= 1;
        playerBlackboard.SetInt(GetAllBlackboardDefs().PlayerStateMachine.AmountOfCostFreeActions, amountOfFreeSimultaneousActions);
        if amountOfFreeSimultaneousActions > 0 {
          return true;
        };
      };
      if !isJustConsulting {
        playerBlackboard.SetVariant(GetAllBlackboardDefs().PlayerStateMachine.CostFreeActionID, ToVariant(TDBID.None()));
      };
      return true;
    };
    return false;
  }

  public func PayCost(opt checkForOverclockedState: Bool) -> Bool {
    let currValue: Float;
    let executorQuantity: Int32;
    let itemCost: wref<ItemCost_Record>;
    let newValue: Float;
    let overclockIsActivated: Bool;
    let quantity: Int32;
    let statPoolCost: wref<StatPoolCost_Record>;
    let statPoolSys: ref<StatPoolsSystem>;
    let statPoolType: gamedataStatPoolType;
    let transactionSys: ref<TransactionSystem>;
    if IsDefined(this.GetObjectActionRecord()) {
      if ArraySize(this.m_costComponents) == 0 {
        this.GetObjectActionRecord().Costs(this.m_costComponents);
      };
    };
    if IsDefined(this.m_costComponents[0]) {
      quantity = this.GetCost();
      this.paymentQuantity = quantity;
      itemCost = this.m_costComponents[0] as ItemCost_Record;
      statPoolCost = this.m_costComponents[0] as StatPoolCost_Record;
      overclockIsActivated = QuickHackableHelper.IsOverclockedStateActive(this.GetExecutor());
      if IsDefined(itemCost) {
        transactionSys = GameInstance.GetTransactionSystem(this.GetExecutor().GetGame());
        executorQuantity = transactionSys.GetItemQuantity(this.GetExecutor(), ItemID.CreateQuery(itemCost.Item().GetID()));
        if executorQuantity < quantity {
          if checkForOverclockedState && overclockIsActivated {
            return QuickHackableHelper.PayWithHealthInOverclockedState(this.GetExecutor(), quantity);
          };
          return false;
        };
        transactionSys.RemoveItem(this.GetExecutor(), ItemID.CreateQuery(itemCost.Item().GetID()), quantity);
      } else {
        if IsDefined(statPoolCost) {
          statPoolSys = GameInstance.GetStatPoolsSystem(this.GetExecutor().GetGame());
          statPoolType = statPoolCost.StatPool().StatPoolType();
          currValue = statPoolSys.GetStatPoolValue(Cast<StatsObjectID>(this.GetExecutor().GetEntityID()), statPoolType, false);
          newValue = currValue - Cast<Float>(quantity);
          if newValue < 0.00 {
            if checkForOverclockedState && overclockIsActivated {
              return QuickHackableHelper.PayWithHealthInOverclockedState(this.GetExecutor(), quantity);
            };
            return false;
          };
          statPoolSys.RequestSettingStatPoolValue(Cast<StatsObjectID>(this.GetExecutor().GetEntityID()), statPoolType, newValue, this.GetExecutor(), false);
        };
      };
      GameObject.PlaySoundEvent(this.GetExecutor(), n"ui_focus_mode_quickhack");
      return true;
    };
    return true;
  }

  public func GetCost() -> Int32 {
    let availableMemory: Float;
    let cost: Float;
    let costMods: array<wref<StatModifier_Record>>;
    let currentlyUploadingAction: wref<ScriptableDeviceAction>;
    let device: ref<ScriptableDeviceComponentPS>;
    let deviceActionQueue: wref<DeviceActionQueue>;
    let distance: Float;
    let extraCost: Float;
    let hackCategory: gamedataHackCategory;
    let i: Int32;
    let instigatorPos: Vector4;
    let objectActionRecords: array<ref<ObjectAction_Record>>;
    let shouldReduceCost: Bool;
    let stacks: Uint32;
    let statPoolCost: ref<StatPoolCost_Record>;
    let statsDataSystem: ref<StatsDataSystem>;
    let targetID: EntityID;
    let targetPos: Vector4;
    let targetPuppet: ref<ScriptedPuppet>;
    let hackID: TweakDBID = this.GetObjectActionRecord().GetID();
    if IsDefined(this.GetExecutor()) && this.GetObjectActionRecord().GetCostsCount() > 0 {
      device = this.GetOwnerPS(this.GetExecutor().GetGame()) as ScriptableDeviceComponentPS;
      if IsDefined(device) && this.GetObjectActionID() == t"DeviceAction.TakeControlCameraClassHack" && device.WasActionPerformed(this.GetActionID(), EActionContext.QHack) {
        return 0;
      };
      if ArraySize(this.m_costComponents) == 0 {
        this.GetObjectActionRecord().Costs(this.m_costComponents);
      };
      if IsDefined(this.m_costComponents[0]) {
        BaseScriptableAction.GetCostMods(this.m_costComponents, costMods);
        if EntityID.IsDefined(this.GetRequesterID()) {
          targetID = this.GetRequesterID();
        } else {
          targetID = PersistentID.ExtractEntityID(this.GetPersistentID());
        };
        cost = RPGManager.CalculateStatModifiers(costMods, this.GetExecutor().GetGame(), this.GetExecutor(), Cast<StatsObjectID>(targetID), Cast<StatsObjectID>(this.GetExecutor().GetEntityID()));
        statPoolCost = this.m_costComponents[0] as StatPoolCost_Record;
        if Equals(statPoolCost.StatPool().StatPoolType(), gamedataStatPoolType.Memory) {
          hackCategory = this.GetObjectActionRecord().HackCategory().Type();
          if Equals(hackCategory, gamedataHackCategory.VehicleHack) {
            if this.GetObjectActionID() == t"DeviceAction.TakeControlVehicleClassHack" {
              cost *= GameInstance.GetStatsDataSystem(this.GetExecutor().GetGame()).GetValueFromCurve(n"puppet_dynamic_scaling", this.GetExecutorLevel(), n"vehicle_quickhack_remotecontrol_memory_cost_multiplier");
            } else {
              if this.GetObjectActionID() == t"DeviceAction.VehicleForceBrakesClassHack" {
                cost *= GameInstance.GetStatsDataSystem(this.GetExecutor().GetGame()).GetValueFromCurve(n"puppet_dynamic_scaling", this.GetExecutorLevel(), n"vehicle_quickhack_forcebrakes_memory_cost_multiplier");
              } else {
                if this.GetObjectActionID() == t"DeviceAction.VehicleExplodeClassHack" {
                  cost *= GameInstance.GetStatsDataSystem(this.GetExecutor().GetGame()).GetValueFromCurve(n"puppet_dynamic_scaling", this.GetExecutorLevel(), n"vehicle_quickhack_explode_memory_cost_multiplier");
                } else {
                  if this.GetObjectActionID() == t"DeviceAction.VehicleAccelerateClassHack" {
                    cost *= GameInstance.GetStatsDataSystem(this.GetExecutor().GetGame()).GetValueFromCurve(n"puppet_dynamic_scaling", this.GetExecutorLevel(), n"vehicle_quickhack_accelerate_memory_cost_multiplier");
                  };
                };
              };
            };
          } else {
            extraCost = GameInstance.GetStatsDataSystem(this.GetExecutor().GetGame()).GetValueFromCurve(n"puppet_dynamic_scaling", this.GetPowerLevelDiff(), n"pl_diff_to_memory_cost_modifier");
            cost += extraCost;
            if Equals(hackCategory, gamedataHackCategory.UltimateHack) {
              cost += extraCost;
            };
          };
          if Cast<Bool>(PlayerDevelopmentSystem.GetData(this.GetExecutor()).IsNewPerkBought(gamedataNewPerkType.Intelligence_Left_Perk_3_1)) && this.IsFirstUniqueCategoryInQueue(targetID, hackCategory) {
            cost -= GameInstance.GetStatsSystem(this.GetExecutor().GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetExecutor().GetEntityID()), gamedataStatType.FirstHackOfTypeInQueueRAMDecrease);
          };
          if Equals(hackCategory, gamedataHackCategory.DamageHack) && Cast<Bool>(PlayerDevelopmentSystem.GetData(this.GetExecutor()).IsNewPerkBought(gamedataNewPerkType.Intelligence_Central_Perk_2_2)) {
            shouldReduceCost = false;
            if GameInstance.GetStatusEffectSystem(this.GetExecutor().GetGame()).HasStatusEffectWithTag(targetID, n"CovertQuickhacked") || GameInstance.GetStatusEffectSystem(this.GetExecutor().GetGame()).HasStatusEffectWithTag(targetID, n"ControlQuickhacked") || GameInstance.GetStatusEffectSystem(this.GetExecutor().GetGame()).HasStatusEffect(targetID, t"BaseStatusEffect.DistractionDuration") {
              shouldReduceCost = true;
            };
            if !shouldReduceCost {
              targetPuppet = GameInstance.FindEntityByID(this.GetExecutor().GetGame(), targetID) as ScriptedPuppet;
              if IsDefined(targetPuppet) {
                currentlyUploadingAction = targetPuppet.GetCurrentlyUploadingAction();
                if Equals(currentlyUploadingAction.GetObjectActionRecord().HackCategory().Type(), gamedataHackCategory.CovertHack) || Equals(currentlyUploadingAction.GetObjectActionRecord().HackCategory().Type(), gamedataHackCategory.ControlHack) {
                  shouldReduceCost = true;
                };
                deviceActionQueue = currentlyUploadingAction.GetDeviceActionQueue();
                deviceActionQueue.GetAllQueuedActionObjectRecords(objectActionRecords);
                i = 0;
                while i < ArraySize(objectActionRecords) {
                  if Equals(objectActionRecords[i].HackCategory().Type(), gamedataHackCategory.CovertHack) || Equals(objectActionRecords[i].HackCategory().Type(), gamedataHackCategory.ControlHack) {
                    shouldReduceCost = true;
                    break;
                  };
                  i += 1;
                };
              };
            };
            if shouldReduceCost {
              cost -= TweakDBInterface.GetFloat(t"NewPerks.Intelligence_Central_Perk_2_2.memoryCostReduction", 0.00);
            };
          };
          if this.IsEyesInTheSkyPerk() {
            cost -= TweakDBInterface.GetFloat(t"NewPerks.Intelligence_Left_Milestone_1.memoryCostReduction", 0.00);
          };
          if Cast<Bool>(PlayerDevelopmentSystem.GetData(this.GetExecutor()).IsNewPerkBought(gamedataNewPerkType.Intelligence_Master_Perk_1)) {
            targetPuppet = GameInstance.FindEntityByID(this.GetExecutor().GetGame(), targetID) as ScriptedPuppet;
            if IsDefined(targetPuppet) && targetPuppet.CanNewActionBeQueued() && targetPuppet.GetDeviceActionQueueSize() >= targetPuppet.GetDeviceActionMaxQueueSize() - 1 {
              cost *= 1.00 - TweakDBInterface.GetFloat(t"NewPerks.Intelligence_Master_Perk_1.memoryCostReduction", 0.00);
            };
          };
          if GameInstance.GetStatPoolsSystem(this.GetExecutor().GetGame()).GetStatPoolValue(Cast<StatsObjectID>(this.GetExecutor().GetEntityID()), gamedataStatPoolType.QuickHackUpload, true) > 0.00 && GameInstance.GetStatsSystem(this.GetExecutor().GetGame()).GetStatValue(Cast<StatsObjectID>(targetID), gamedataStatType.IsNetrunnerArchetype) > 0.00 {
            if GameInstance.GetStatsSystem(this.GetExecutor().GetGame()).GetStatValue(Cast<StatsObjectID>(targetID), gamedataStatType.RevealNetrunnerWhenHacked) > 0.00 {
              if Cast<Bool>(PlayerDevelopmentSystem.GetData(this.GetExecutor()).IsNewPerkBought(gamedataNewPerkType.Intelligence_Left_Perk_2_1)) {
                cost -= TweakDBInterface.GetFloat(t"NewPerks.Intelligence_Left_Perk_2_1.memoryCostReduction", 0.00);
              };
            };
          };
          if hackID == t"QuickHack.MadnessLvl3Hack" || hackID == t"QuickHack.MadnessLvl4Hack" || hackID == t"QuickHack.MadnessLvl4PlusPlusHack" || hackID == t"QuickHack.MadnessSetFriendlyHack" {
            cost -= this.GetMadnessLvl3ProgramCostReduction(targetID);
          };
          if hackID == t"QuickHack.BrainMeltLvl3Hack" || hackID == t"QuickHack.BrainMeltLvl4Hack" || hackID == t"QuickHack.BrainMeltLvl4PlusPlusHack" {
            stacks = StatusEffectHelper.GetStatusEffectByID(GetPlayer(this.GetExecutor().GetGame()), t"BaseStatusEffect.BrainMeltCostReductionSE").GetStackCount();
            cost -= Cast<Float>(stacks) * TweakDBInterface.GetFloat(t"EquipmentGLP.BrainMeltProgramLvl3Passive.memoryCostReductionPerStack", 0.00);
          };
          if hackID == t"QuickHack.SystemCollapseLvl3Hack" || hackID == t"QuickHack.SystemCollapseLvl4Hack" || hackID == t"QuickHack.SystemCollapseLvl4PlusPlusHack" {
            stacks = StatusEffectHelper.GetStatusEffectByID(GetPlayer(this.GetExecutor().GetGame()), t"BaseStatusEffect.SystemCollapseMemoryCostReduction").GetStackCount();
            cost -= Cast<Float>(stacks) * TweakDBInterface.GetFloat(t"EquipmentGLP.SystemCollapseLvl3Program.memoryCostReductionPerStack", 0.00);
          };
          if this.GetObjectActionRecord().GetID() == t"QuickHack.GrenadeLvl3Hack" || this.GetObjectActionRecord().GetID() == t"QuickHack.GrenadeLvl4Hack" {
            cost -= this.GetDetonateGranadeCostReduction(false);
          } else {
            if this.GetObjectActionRecord().GetID() == t"QuickHack.GrenadeLvl4PlusPlusHack" {
              cost -= this.GetDetonateGranadeCostReduction(true);
            };
          };
          if Equals(this.actionName, n"Suicide") {
            cost -= GameInstance.GetStatsSystem(this.GetExecutor().GetGame()).GetStatValue(Cast<StatsObjectID>(this.GetExecutor().GetEntityID()), gamedataStatType.SuicideHackMemoryCostReduction);
          };
          if Cast<Bool>(PlayerDevelopmentSystem.GetData(this.GetExecutor()).IsNewPerkBought(gamedataNewPerkType.Intelligence_Central_Perk_1_2)) {
            targetPos = GameInstance.FindEntityByID(this.GetExecutor().GetGame(), targetID).GetWorldPosition();
            instigatorPos = this.GetExecutor().GetWorldPosition();
            distance = Vector4.Distance(targetPos, instigatorPos);
            statsDataSystem = GameInstance.GetStatsDataSystem(this.GetExecutor().GetGame());
            cost *= 1.00 - statsDataSystem.GetValueFromCurve(n"hacking_passives", distance, n"distance_to_hacking_cost_reduction");
          };
          if QuickHackableHelper.IsOverclockedStateActive(this.GetExecutor()) {
            if StrEndsWith(NameToString(this.actionName), "BlackWall") {
              availableMemory = GameInstance.GetStatPoolsSystem(this.GetExecutor().GetGame()).GetStatPoolValue(Cast<StatsObjectID>(this.GetExecutor().GetEntityID()), gamedataStatPoolType.Memory, false);
              availableMemory = MinF(cost, Cast<Float>(FloorF(availableMemory)));
              cost = availableMemory + (cost - availableMemory) * TweakDBInterface.GetFloat(t"QuickHack.BaseBlackWallHack.memoryCostReductionInOverclock", 1.00);
            };
          };
        };
        if ArraySize(costMods) > 0 {
          return Max(2, CeilF(cost));
        };
        return Max(0, CeilF(cost));
      };
    };
    return 0;
  }

  public func GetBaseCost() -> Int32 {
    let constantCostMods: array<wref<StatModifier_Record>>;
    let cost: Float;
    let costMods: array<wref<StatModifier_Record>>;
    let extraCost: Float;
    let i: Int32;
    let statPoolCost: wref<StatPoolCost_Record>;
    let targetID: EntityID;
    if IsDefined(this.GetExecutor()) && this.GetObjectActionRecord().GetCostsCount() > 0 {
      if ArraySize(this.m_costComponents) == 0 {
        this.GetObjectActionRecord().Costs(this.m_costComponents);
      };
      BaseScriptableAction.GetCostMods(this.m_costComponents, costMods);
      if IsDefined(this.m_costComponents[0]) {
        if EntityID.IsDefined(this.GetRequesterID()) {
          targetID = this.GetRequesterID();
        } else {
          targetID = PersistentID.ExtractEntityID(this.GetPersistentID());
        };
        i = 0;
        while i < ArraySize(costMods) {
          if IsDefined(costMods[i] as ConstantStatModifier_Record) {
            ArrayPush(constantCostMods, costMods[i]);
          };
          i += 1;
        };
        cost = RPGManager.CalculateStatModifiers(constantCostMods, this.GetExecutor().GetGame(), this.GetExecutor(), Cast<StatsObjectID>(targetID), Cast<StatsObjectID>(this.GetExecutor().GetEntityID()));
        statPoolCost = this.m_costComponents[0] as StatPoolCost_Record;
        if Equals(statPoolCost.StatPool().StatPoolType(), gamedataStatPoolType.Memory) {
          extraCost = GameInstance.GetStatsDataSystem(this.GetExecutor().GetGame()).GetValueFromCurve(n"puppet_dynamic_scaling", this.GetPowerLevelDiff(), n"pl_diff_to_memory_cost_modifier");
          cost += extraCost;
          if Equals(this.GetObjectActionRecord().HackCategory().Type(), gamedataHackCategory.UltimateHack) {
            cost += extraCost;
          };
        };
        if ArraySize(costMods) > 0 {
          return Max(1, CeilF(cost));
        };
        return Max(0, CeilF(cost));
      };
    };
    return 0;
  }

  public final static func GetBaseCostStatic(executor: wref<GameObject>, actionRecord: wref<ObjectAction_Record>) -> Int32 {
    let constantCostMods: array<wref<StatModifier_Record>>;
    let cost: Float;
    let costComponents: array<wref<ObjectActionCost_Record>>;
    let costMods: array<wref<StatModifier_Record>>;
    let i: Int32;
    let targetID: EntityID;
    if IsDefined(executor) && actionRecord.GetCostsCount() > 0 {
      actionRecord.Costs(costComponents);
      BaseScriptableAction.GetCostMods(costComponents, costMods);
      if IsDefined(costComponents[0]) {
        i = 0;
        while i < ArraySize(costMods) {
          if IsDefined(costMods[i] as ConstantStatModifier_Record) {
            ArrayPush(constantCostMods, costMods[i]);
          };
          i += 1;
        };
        cost += RPGManager.CalculateStatModifiers(constantCostMods, executor.GetGame(), executor, Cast<StatsObjectID>(targetID), Cast<StatsObjectID>(executor.GetEntityID()));
        if ArraySize(costMods) > 0 {
          return Max(1, CeilF(cost));
        };
        return Max(0, CeilF(cost));
      };
    };
    return 0;
  }

  public final static func GetCostMods(costComponents: script_ref<[wref<ObjectActionCost_Record>]>, out costMods: [wref<StatModifier_Record>]) -> Void {
    let i: Int32 = 0;
    while i < ArraySize(Deref(costComponents)) {
      Deref(costComponents)[i].CostMods(costMods);
      i += 1;
    };
  }

  private final func GetPowerLevelDiff() -> Float {
    let executorLevel: Float;
    let powerLevelDiff: Float;
    let statsSystem: ref<StatsSystem>;
    let targetID: EntityID;
    let targetLevel: Float;
    if !IsDefined(this.GetExecutor()) {
      return 0.00;
    };
    targetID = this.GetRequesterID();
    if !EntityID.IsDefined(targetID) {
      targetID = PersistentID.ExtractEntityID(this.GetPersistentID());
    };
    if !EntityID.IsDefined(targetID) {
      return 0.00;
    };
    statsSystem = GameInstance.GetStatsSystem(this.GetExecutor().GetGame());
    executorLevel = statsSystem.GetStatValue(Cast<StatsObjectID>(this.GetExecutor().GetEntityID()), gamedataStatType.PowerLevel);
    targetLevel = statsSystem.GetStatValue(Cast<StatsObjectID>(targetID), gamedataStatType.PowerLevel);
    powerLevelDiff = Cast<Float>(RoundMath(executorLevel) - RoundF(targetLevel));
    return powerLevelDiff;
  }

  private final func GetExecutorLevel() -> Float {
    let executorLevel: Float;
    let statsSystem: ref<StatsSystem>;
    if !IsDefined(this.GetExecutor()) {
      return 0.00;
    };
    statsSystem = GameInstance.GetStatsSystem(this.GetExecutor().GetGame());
    executorLevel = statsSystem.GetStatValue(Cast<StatsObjectID>(this.GetExecutor().GetEntityID()), gamedataStatType.PowerLevel);
    return executorLevel;
  }

  private final func GetMadnessLvl3ProgramCostReduction(targetID: EntityID) -> Float {
    let appliedStatusEffects: array<ref<StatusEffect>>;
    let costReductor: gamedataStatusEffectType;
    let j: Int32;
    let maxReduction: Float = TweakDBInterface.GetFloat(t"Items.MadnessLvl3Program.maxCostReduction", 0.00);
    let stackReduction: Float = TweakDBInterface.GetFloat(t"Items.MadnessLvl3Program.costReductionPerReductorStack", 0.00);
    let costReductors: array<TweakDBID> = TweakDBInterface.GetForeignKeyArray(t"Items.MadnessLvl3Program.statusEffectsTypeCostReductors");
    let stacks: Uint32 = 0u;
    let i: Int32 = 0;
    while i < ArraySize(costReductors) {
      ArrayClear(appliedStatusEffects);
      costReductor = TweakDBInterface.GetStatusEffectTypeRecord(costReductors[i]).Type();
      GameInstance.GetStatusEffectSystem(this.GetExecutor().GetGame()).GetAppliedEffectsOfType(targetID, costReductor, appliedStatusEffects);
      j = 0;
      while j < ArraySize(appliedStatusEffects) {
        stacks += appliedStatusEffects[j].GetStackCount();
        j += 1;
      };
      i += 1;
    };
    return MinF(maxReduction, Cast<Float>(stacks) * stackReduction);
  }

  private final func GetDetonateGranadeCostReduction(legendaryPlusPlus: Bool) -> Float {
    let stackSE: TweakDBID = legendaryPlusPlus ? t"BaseStatusEffect.ReduceUltimateSuicideWithGrenadeCostPlusPlus" : t"BaseStatusEffect.ReduceUltimateSuicideWithGrenadeCost";
    let stackReduction: Float = TweakDBInterface.GetFloat(legendaryPlusPlus ? t"BaseStatusEffect.ReduceUltimateSuicideWithGrenadeCostPlusPlus.costReductionPerStack" : t"BaseStatusEffect.ReduceUltimateSuicideWithGrenadeCost.costReductionPerStack", 0.00);
    let stacks: Uint32 = StatusEffectHelper.GetStatusEffectByID(GetPlayer(this.GetExecutor().GetGame()), stackSE).GetStackCount();
    return Cast<Float>(stacks) * stackReduction;
  }
}

public abstract class ScriptableDeviceAction extends BaseScriptableAction {

  public let prop: ref<DeviceActionProperty>;

  protected let m_actionWidgetPackage: SActionWidgetPackage;

  protected let m_spiderbotActionLocationOverride: NodeRef;

  @default(BeginArcadeMinigameUI, 3.967)
  @default(TogglePersonalLink, 2.733)
  protected let m_duration: Float;

  @default(ScriptableDeviceAction, true)
  private let m_canTriggerStim: Bool;

  private let m_wasPerformedOnOwner: Bool;

  private let m_shouldActivateDevice: Bool;

  private let m_disableSpread: Bool;

  @default(QuickHackToggleOpen, true)
  protected let m_isQuickHack: Bool;

  protected let m_isSpiderbotAction: Bool;

  protected let m_attachedProgram: TweakDBID;

  protected let m_activeStatusEffect: TweakDBID;

  protected let m_interactionIconType: TweakDBID;

  protected let m_hasInteraction: Bool;

  protected let m_inactiveReason: String;

  protected let m_widgetStyle: gamedataComputerUIStyle;

  protected func GetOwnerPS(game: GameInstance) -> ref<ScriptableDeviceComponentPS> {
    let psID: PersistentID = this.GetPersistentID();
    if PersistentID.IsDefined(psID) {
      return GameInstance.GetPersistencySystem(game).GetConstAccessToPSObject(psID, this.GetDeviceClassName()) as ScriptableDeviceComponentPS;
    };
    return null;
  }

  public func ResolveAction(data: ref<ResolveActionData>) -> Bool {
    return true;
  }

  public final const func ShouldActivateDevice() -> Bool {
    return this.m_shouldActivateDevice;
  }

  public final func SetShouldActivateDevice(value: Bool) -> Void {
    this.m_shouldActivateDevice = value;
  }

  public final func SetDisableSpread(value: Bool) -> Void {
    this.m_disableSpread = value;
  }

  public final func SetCanSkipPayCost(value: Bool) -> Void {
    this.m_canSkipPayCost = value;
  }

  public final const func IsSpreadDisabled() -> Bool {
    return this.m_disableSpread;
  }

  public final const func CanTriggerStim() -> Bool {
    return this.m_canTriggerStim;
  }

  public final func SetCanTriggerStim(canTrigger: Bool) -> Void {
    this.m_canTriggerStim = canTrigger;
  }

  public final const func GetDurationValue() -> Float {
    return this.m_duration;
  }

  public final func SetCompleted() -> Void {
    this.m_duration = 0.00;
  }

  public final const func IsCompleted() -> Bool {
    return this.m_duration <= 0.00;
  }

  public final const func IsStarted() -> Bool {
    return this.m_duration > 0.00;
  }

  public final func SetDurationValue(duration: Float) -> Void {
    this.m_duration = duration;
  }

  public final func GetActionName() -> CName {
    if IsNameValid(this.actionName) {
      return this.actionName;
    };
    return this.GetDefaultActionName();
  }

  protected func GetDefaultActionName() -> CName {
    return n"ScriptableDeviceAction";
  }

  public const func GetObjectActionRecord() -> wref<ObjectAction_Record> {
    if IsDefined(this.m_objectActionRecord) {
      return this.m_objectActionRecord;
    };
    if TDBID.IsValid(this.m_objectActionID) {
      return super.GetObjectActionRecord();
    };
    return TweakDBInterface.GetObjectActionRecord(TDBID.Create("DeviceAction." + NameToString(this.GetClassName())));
  }

  public const func CanSpiderbotCompleteThisAction(const device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.IsDisabled() {
      return false;
    };
    return true;
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if ScriptableDeviceAction.IsAvailable(device) && ScriptableDeviceAction.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    return true;
  }

  public final func AddDeviceName(const deviceName: script_ref<String>) -> Void {
    this.localizedObjectName = Deref(deviceName);
  }

  public final func GetDeviceName() -> String {
    return this.localizedObjectName;
  }

  public func GetInkWidgetLibraryPath() -> ResRef {
    return r"base\\movies\\misc\\distraction_generic.bk2";
  }

  public func GetInkWidgetLibraryID() -> CName {
    return n"None";
  }

  public func SetInkWidgetTweakDBID(id: TweakDBID) -> Void {
    this.m_inkWidgetID = id;
  }

  public final func SetWidgetStyle(style: gamedataComputerUIStyle) -> Void {
    this.m_widgetStyle = style;
  }

  public func GetInkWidgetTweakDBID() -> TweakDBID {
    if TDBID.IsValid(this.m_inkWidgetID) {
      return this.m_inkWidgetID;
    };
    switch this.m_widgetStyle {
      case gamedataComputerUIStyle.Orange:
        return t"DevicesUIDefinitions.GenericDeviceActionWidgetOA";
      default:
        return t"DevicesUIDefinitions.GenericDeviceActionWidget";
    };
  }

  public func SetActiveStatusEffectTweakDBID(effectID: TweakDBID) -> Void {
    this.m_activeStatusEffect = effectID;
  }

  public func GetActiveStatusEffectTweakDBID() -> TweakDBID {
    return this.m_activeStatusEffect;
  }

  public func SetAttachedProgramTweakDBID(programID: TweakDBID) -> Void {
    this.m_attachedProgram = programID;
  }

  public func GetAttachedProgramTweakDBID() -> TweakDBID {
    return this.m_attachedProgram;
  }

  public final func SetIllegal(isIllegal: Bool) -> Void {
    if isIllegal {
      ChoiceTypeWrapper.SetType(this.interactionChoice.choiceMetaData.type, gameinteractionsChoiceType.Illegal);
    };
  }

  public final func ClearIllegal() -> Void {
    ChoiceTypeWrapper.ClearType(this.interactionChoice.choiceMetaData.type, gameinteractionsChoiceType.Illegal);
  }

  public final func IsIllegal() -> Bool {
    return ChoiceTypeWrapper.IsType(this.interactionChoice.choiceMetaData.type, gameinteractionsChoiceType.Illegal);
  }

  public final func GetInteractionLayer() -> CName {
    return this.m_interactionLayer;
  }

  public final func SetInteractionLayer(layer: CName) -> Void {
    this.m_interactionLayer = layer;
  }

  public final func GetRequestType() -> gamedeviceRequestType {
    if Equals(this.m_interactionLayer, n"direct") {
      return gamedeviceRequestType.Direct;
    };
    if Equals(this.m_interactionLayer, n"remote") {
      return gamedeviceRequestType.Remote;
    };
    return gamedeviceRequestType.None;
  }

  public func SetObjectActionID(id: TweakDBID) -> Void {
    this.m_objectActionID = id;
    this.m_objectActionRecord = TweakDBInterface.GetObjectActionRecord(id);
    ArrayClear(this.m_costComponents);
    if IsDefined(this.m_objectActionRecord) {
      this.actionName = this.m_objectActionRecord.ActionName();
    };
    this.ProduceInteractionPart();
  }

  public final func SetAsQuickHack(opt wasExecutedAtLeastOnce: Bool) -> Void {
    this.m_isQuickHack = true;
    this.m_wasPerformedOnOwner = wasExecutedAtLeastOnce;
    this.ProduceInteractionParts();
  }

  public final func GetAwarenessCost(gameInstance: GameInstance) -> Float {
    let incrementModifiers: array<wref<StatModifier_Record>>;
    let awarenessCost: Float = 0.00;
    let hackActionRecord: wref<ObjectAction_Record> = this.GetObjectActionRecord();
    let hackStatRecord: wref<StatModifierGroup_Record> = TweakDBInterface.GetStatModifierGroupRecord(TDB.GetForeignKey(hackActionRecord.GetID() + t".awarenessCost"));
    if TDBID.IsValid(hackStatRecord.GetID()) {
      hackStatRecord.StatModifiers(incrementModifiers);
      awarenessCost = RPGManager.CalculateStatModifiers(incrementModifiers, gameInstance, this.GetExecutor(), Cast<StatsObjectID>(this.GetRequesterID()), Cast<StatsObjectID>(this.GetExecutor().GetEntityID()));
    };
    return awarenessCost;
  }

  private final func ProduceInteractionParts() -> Void {
    let costPart: ref<InteractionChoiceCaptionQuickhackCostPart>;
    let iconRecord: wref<ChoiceCaptionIconPart_Record>;
    if !this.IsInteractionChoiceValid() {
      return;
    };
    InteractionChoiceCaption.Clear(this.interactionChoice.captionParts);
    iconRecord = this.GetInteractionIcon();
    if IsDefined(iconRecord) {
      InteractionChoiceCaption.AddPartFromRecord(this.interactionChoice.captionParts, iconRecord);
    };
    if this.m_isQuickHack && this.GetCost() >= 0 || this.GetCost() > 0 {
      costPart = new InteractionChoiceCaptionQuickhackCostPart();
      costPart.cost = this.GetCost();
      InteractionChoiceCaption.AddScriptPart(this.interactionChoice.captionParts, costPart);
    };
    InteractionChoiceCaption.AddTextPart(this.interactionChoice.captionParts, LocKeyToString(TweakDBInterface.GetInteractionBaseRecord(TDBID.Create("Interactions." + this.interactionChoice.choiceMetaData.tweakDBName)).Caption()));
  }

  private func StartUpload(gameInstance: GameInstance) -> Void {
    let actionUploadListener: ref<QuickHackUploadListener>;
    let hackingMinigameBB: ref<IBlackboard>;
    let setQuickHackAttempt: ref<SetQuickHackAttemptEvent>;
    let statMod: ref<gameStatModifierData>;
    let progressBarBB: wref<IBlackboard> = GameInstance.GetBlackboardSystem(gameInstance).Get(GetAllBlackboardDefs().UI_HUDProgressBar);
    let statPoolSys: ref<StatPoolsSystem> = GameInstance.GetStatPoolsSystem(gameInstance);
    progressBarBB.SetFloat(GetAllBlackboardDefs().UI_HUDProgressBar.ProgressBump, 0.00);
    hackingMinigameBB = GameInstance.GetBlackboardSystem(gameInstance).Get(GetAllBlackboardDefs().HackingMinigame);
    hackingMinigameBB.SetVector4(GetAllBlackboardDefs().HackingMinigame.LastPlayerHackPosition, GetPlayer(gameInstance).GetWorldPosition());
    statMod = RPGManager.CreateStatModifier(gamedataStatType.QuickHackUpload, gameStatModifierType.Additive, 1.00);
    GameInstance.GetStatsSystem(gameInstance).RemoveAllModifiers(Cast<StatsObjectID>(this.m_requesterID), gamedataStatType.QuickHackUpload);
    GameInstance.GetStatsSystem(gameInstance).AddModifier(Cast<StatsObjectID>(this.m_requesterID), statMod);
    actionUploadListener = new QuickHackUploadListener();
    actionUploadListener.m_action = this;
    actionUploadListener.m_gameInstance = gameInstance;
    statPoolSys.RequestRegisteringListener(Cast<StatsObjectID>(this.m_requesterID), gamedataStatPoolType.QuickHackUpload, actionUploadListener);
    statPoolSys.RequestAddingStatPool(Cast<StatsObjectID>(this.m_requesterID), t"BaseStatPools.BaseQuickHackUpload", true);
    if this.IsQuickHack() {
      setQuickHackAttempt = new SetQuickHackAttemptEvent();
      setQuickHackAttempt.wasQuickHackAttempt = true;
      GameInstance.GetPersistencySystem(gameInstance).QueuePSEvent(this.GetPersistentID(), this.GetDeviceClassName(), setQuickHackAttempt);
    };
  }

  public func CompleteAction(gameInstance: GameInstance) -> Void {
    let setQuickHack: ref<SetQuickHackEvent>;
    if this.m_isTargetDead {
      return;
    };
    GameInstance.GetPersistencySystem(gameInstance).QueuePSDeviceEvent(this);
    super.CompleteAction(gameInstance);
    if this.IsQuickHack() {
      setQuickHack = new SetQuickHackEvent();
      setQuickHack.wasQuickHacked = true;
      setQuickHack.quickHackName = this.GetActionName();
      GameInstance.GetPersistencySystem(gameInstance).QueuePSEvent(this.GetPersistentID(), this.GetDeviceClassName(), setQuickHack);
      QuickhackModule.RequestRefreshQuickhackMenu(gameInstance, this.GetRequesterID());
      RPGManager.HealPuppetAfterQuickhack(gameInstance, this.m_executor);
    };
  }

  public func GetCost() -> Int32 {
    return super.GetCost();
  }

  public func SetInteractionIcon(iconType: TweakDBID) -> Void {
    this.m_interactionIconType = iconType;
  }

  public const func GetInteractionIcon() -> wref<ChoiceCaptionIconPart_Record> {
    let iconType: wref<ChoiceCaptionIconPart_Record>;
    if TDBID.IsValid(this.m_objectActionID) {
      iconType = this.GetObjectActionRecord().ObjectActionUI().CaptionIcon();
    };
    if IsDefined(iconType) {
      return iconType;
    };
    if TDBID.IsValid(this.m_interactionIconType) {
      iconType = TweakDBInterface.GetChoiceCaptionIconPartRecord(this.m_interactionIconType);
    } else {
      iconType = InteractionChoiceMetaData.GetTweakData(this.interactionChoice.choiceMetaData).CaptionIcon();
    };
    return iconType;
  }

  public final func SetAsSpiderbotAction() -> Void {
    this.m_isSpiderbotAction = true;
  }

  public final const func IsRemoteHack() -> Bool {
    let actionType: gamedataObjectActionType;
    let typeRecord: wref<ObjectActionType_Record>;
    let actionRecord: wref<ObjectAction_Record> = this.GetObjectActionRecord();
    if IsDefined(actionRecord) {
      typeRecord = actionRecord.ObjectActionType();
    };
    if IsDefined(typeRecord) {
      actionType = typeRecord.Type();
      return Equals(actionType, gamedataObjectActionType.Remote);
    };
    return false;
  }

  public final const func IsQuickHack() -> Bool {
    let actionRecord: wref<ObjectAction_Record>;
    let actionType: gamedataObjectActionType;
    let typeRecord: wref<ObjectActionType_Record>;
    if this.m_isQuickHack {
      return true;
    };
    actionRecord = this.GetObjectActionRecord();
    if IsDefined(actionRecord) {
      typeRecord = actionRecord.ObjectActionType();
    };
    if IsDefined(typeRecord) {
      actionType = typeRecord.Type();
      return Equals(actionType, gamedataObjectActionType.DeviceQuickHack) || Equals(actionType, gamedataObjectActionType.PuppetQuickHack) || Equals(actionType, gamedataObjectActionType.VehicleQuickHack);
    };
    return false;
  }

  public final const func IsSpiderbotAction() -> Bool {
    return this.m_isSpiderbotAction;
  }

  public final func SetSpiderbotLocationOverrideReference(targetLocationReference: NodeRef) -> Void {
    this.m_spiderbotActionLocationOverride = targetLocationReference;
  }

  public final func GetSpiderbotLocationOverrideReference() -> NodeRef {
    return this.m_spiderbotActionLocationOverride;
  }

  public final func GetInteractionChoice() -> InteractionChoice {
    let choice: InteractionChoice;
    let i: Int32;
    if this.m_hasInteraction {
      ArrayInsert(choice.data, 0, ToVariant(this));
      choice.caption = this.interactionChoice.caption;
      choice.captionParts = this.interactionChoice.captionParts;
      choice.choiceMetaData.tweakDBID = this.interactionChoice.choiceMetaData.tweakDBID;
      choice.choiceMetaData.type = this.interactionChoice.choiceMetaData.type;
      if !TDBID.IsValid(this.interactionChoice.choiceMetaData.tweakDBID) {
        choice.choiceMetaData.tweakDBID = this.GetTweakDBChoiceID();
      };
      if Equals(StringToName(this.interactionChoice.choiceMetaData.tweakDBName), this.GetActionName()) {
        choice.choiceMetaData.tweakDBName = this.GetTweakDBChoiceRecord();
      } else {
        choice.choiceMetaData.tweakDBName = this.interactionChoice.choiceMetaData.tweakDBName;
      };
      i = 0;
      while i < ArraySize(this.interactionChoice.data) {
        ArrayPush(choice.data, ToVariant(FromVariant<ref<DeviceAction>>(this.interactionChoice.data[i])));
        i += 1;
      };
      if ArraySize(choice.captionParts.parts) == 0 {
        this.ProduceInteractionParts();
        choice.captionParts = this.interactionChoice.captionParts;
      };
    };
    return choice;
  }

  public final func GetActionWidgetPackage() -> SActionWidgetPackage {
    let actionWidgetPackage: SActionWidgetPackage;
    actionWidgetPackage.action = this;
    if !TDBID.IsValid(this.m_actionWidgetPackage.widgetTweakDBID) {
      actionWidgetPackage.widgetTweakDBID = this.GetInkWidgetTweakDBID();
      this.ResolveActionWidgetTweakDBData();
    } else {
      actionWidgetPackage.widgetTweakDBID = this.m_actionWidgetPackage.widgetTweakDBID;
    };
    actionWidgetPackage.wasInitalized = this.m_actionWidgetPackage.wasInitalized;
    actionWidgetPackage.dependendActions = this.m_actionWidgetPackage.dependendActions;
    actionWidgetPackage.libraryPath = this.m_actionWidgetPackage.libraryPath;
    actionWidgetPackage.libraryID = this.m_actionWidgetPackage.libraryID;
    actionWidgetPackage.widgetName = this.m_actionWidgetPackage.widgetName;
    actionWidgetPackage.displayName = this.m_actionWidgetPackage.displayName;
    actionWidgetPackage.iconID = this.m_actionWidgetPackage.iconID;
    actionWidgetPackage.isWidgetInactive = this.m_actionWidgetPackage.isWidgetInactive;
    actionWidgetPackage.widgetState = this.m_actionWidgetPackage.widgetState;
    actionWidgetPackage.isValid = ResRef.IsValid(actionWidgetPackage.libraryPath) || IsNameValid(actionWidgetPackage.libraryID) || TDBID.IsValid(actionWidgetPackage.widgetTweakDBID);
    return actionWidgetPackage;
  }

  public final func CreateInteraction(opt actions: [ref<DeviceAction>], opt alternativeMainChoiceRecord: String, opt alternativeMainChoiceTweakDBID: TweakDBID) -> Void {
    let defaultChoiceID: TweakDBID;
    this.m_hasInteraction = true;
    if TDBID.IsValid(alternativeMainChoiceTweakDBID) {
      this.interactionChoice.choiceMetaData.tweakDBID = alternativeMainChoiceTweakDBID;
    } else {
      if IsStringValid(alternativeMainChoiceRecord) {
        this.interactionChoice.choiceMetaData.tweakDBName = alternativeMainChoiceRecord;
      } else {
        defaultChoiceID = this.GetTweakDBChoiceID();
        if TDBID.IsValid(defaultChoiceID) {
          this.interactionChoice.choiceMetaData.tweakDBID = defaultChoiceID;
        } else {
          if IsStringValid(alternativeMainChoiceRecord) {
            this.interactionChoice.choiceMetaData.tweakDBName = alternativeMainChoiceRecord;
          } else {
            this.interactionChoice.choiceMetaData.tweakDBName = this.GetTweakDBChoiceRecord();
          };
        };
      };
    };
    DeviceHelper.PushActionsIntoInteractionChoice(this.interactionChoice, actions);
  }

  public func HasUI() -> Bool {
    return Equals(this.m_actionWidgetPackage.wasInitalized, true) && IsStringValid(this.m_actionWidgetPackage.widgetName);
  }

  public func CreateActionWidgetPackage(opt actions: [ref<DeviceAction>]) -> Void {
    this.m_actionWidgetPackage.wasInitalized = true;
    this.m_actionWidgetPackage.dependendActions = actions;
    this.m_actionWidgetPackage.libraryPath = this.GetInkWidgetLibraryPath();
    this.m_actionWidgetPackage.libraryID = this.GetInkWidgetLibraryID();
    this.m_actionWidgetPackage.widgetName = ToString(this.GetActionName());
    this.m_actionWidgetPackage.displayName = this.GetCurrentDisplayString();
    this.m_actionWidgetPackage.iconID = this.GetActionName();
    this.m_actionWidgetPackage.widgetTweakDBID = this.GetInkWidgetTweakDBID();
    this.ResolveActionWidgetTweakDBData();
  }

  public func CreateActionWidgetPackage(widgetTweakDBID: TweakDBID, opt actions: [ref<DeviceAction>]) -> Void {
    this.CreateActionWidgetPackage(actions);
    if TDBID.IsValid(widgetTweakDBID) {
      this.m_actionWidgetPackage.widgetTweakDBID = widgetTweakDBID;
      this.ResolveActionWidgetTweakDBData();
    };
  }

  protected final func ResolveActionWidgetTweakDBData() -> Void {
    let record: ref<WidgetDefinition_Record>;
    if TDBID.IsValid(this.m_actionWidgetPackage.widgetTweakDBID) {
      record = TweakDBInterface.GetWidgetDefinitionRecord(this.m_actionWidgetPackage.widgetTweakDBID);
      if record != null {
        this.m_actionWidgetPackage.libraryPath = record.LibraryPath();
        this.m_actionWidgetPackage.libraryID = StringToName(record.LibraryID());
      };
    };
  }

  public func CreateCustomInteraction(opt actions: [ref<DeviceAction>], const customName1: script_ref<String>, const customName2: script_ref<String>, opt customID1: TweakDBID, opt customID2: TweakDBID) -> Void;

  public final func SetInactiveWithReason(isActiveIf: Bool, const reason: script_ref<String>) -> Void {
    if !isActiveIf {
      this.SetInactive();
      this.SetInactiveReason(reason);
    };
  }

  public final func SetInactiveReason(const reasonStr: script_ref<String>) -> Void {
    if NotEquals(reasonStr, "") {
      this.m_inactiveReason = Deref(reasonStr);
    };
  }

  public final const func GetInactiveReason() -> String {
    return this.m_inactiveReason;
  }

  public final func SetInactiveReasonAsCaption() -> Void {
    if this.IsInactive() {
      this.interactionChoice.caption = this.m_inactiveReason;
    };
  }

  public final const func GetDurationFromTDBRecord(record: TweakDBID) -> Float {
    let minigameActionRecord: ref<MinigameAction_Record> = TweakDBInterface.GetMinigameActionRecord(record);
    let duration: Float = minigameActionRecord.Duration();
    return duration;
  }
}

public abstract class ActionBool extends ScriptableDeviceAction {

  public func GetProperties() -> [ref<DeviceActionProperty>] {
    let arr: array<ref<DeviceActionProperty>>;
    ArrayPush(arr, this.prop);
    return arr;
  }

  public const func GetCurrentDisplayString() -> String {
    let str: String;
    if !FromVariant<Bool>(this.prop.first) {
      str = NameToString(FromVariant<CName>(this.prop.second));
    } else {
      str = NameToString(FromVariant<CName>(this.prop.third));
    };
    return str;
  }

  public final func GetValue() -> Bool {
    return FromVariant<Bool>(this.prop.first);
  }

  public final func OverrideInteractionRecord(newRecordforTrue: TweakDBID, newRecordForFalse: TweakDBID) -> Void {
    let isTrue: Bool;
    let newRecord: TweakDBID;
    DeviceActionPropertyFunctions.GetProperty_Bool(this.prop, isTrue);
    if isTrue {
      newRecord = newRecordforTrue;
    } else {
      newRecord = newRecordForFalse;
    };
    if TDBID.IsValid(newRecord) {
      this.m_hasInteraction = true;
      this.interactionChoice.choiceMetaData.tweakDBID = newRecord;
    };
  }

  public func CreateCustomInteraction(opt actions: [ref<DeviceAction>], const customName1: script_ref<String>, const customName2: script_ref<String>, opt customID1: TweakDBID, opt customID2: TweakDBID) -> Void {
    let value: Bool;
    let useTweakDB: Bool = TDBID.IsValid(customID1) && TDBID.IsValid(customID2);
    if IsStringValid(customName1) && IsStringValid(customName2) || useTweakDB {
      this.m_hasInteraction = true;
      DeviceActionPropertyFunctions.GetProperty_Bool(this.prop, value);
      if !value {
        if useTweakDB {
          this.interactionChoice.choiceMetaData.tweakDBID = customID1;
        } else {
          this.interactionChoice.choiceMetaData.tweakDBName = Deref(customName1);
        };
      } else {
        if useTweakDB {
          this.interactionChoice.choiceMetaData.tweakDBID = customID2;
        } else {
          this.interactionChoice.choiceMetaData.tweakDBName = Deref(customName2);
        };
      };
      DeviceHelper.PushActionsIntoInteractionChoice(this.interactionChoice, actions);
    } else {
      this.CreateInteraction(actions);
    };
  }

  public func CreateActionWidgetPackage(opt actions: [ref<DeviceAction>]) -> Void {
    let value: Bool;
    super.CreateActionWidgetPackage(actions);
    DeviceActionPropertyFunctions.GetProperty_Bool(this.prop, value);
    if !value {
      this.m_actionWidgetPackage.widgetState = EWidgetState.OFF;
    } else {
      this.m_actionWidgetPackage.widgetState = EWidgetState.ON;
    };
  }
}

public abstract class ActionInt extends ScriptableDeviceAction {

  public func GetProperties() -> [ref<DeviceActionProperty>] {
    let arr: array<ref<DeviceActionProperty>>;
    ArrayPush(arr, this.prop);
    return arr;
  }

  public const func GetCurrentDisplayString() -> String {
    let str: String = NameToString(this.prop.name) + " " + IntToString(FromVariant<Int32>(this.prop.first));
    return str;
  }
}

public abstract class ActionFloat extends ScriptableDeviceAction {

  public func GetProperties() -> [ref<DeviceActionProperty>] {
    let arr: array<ref<DeviceActionProperty>>;
    ArrayPush(arr, this.prop);
    return arr;
  }

  public const func GetCurrentDisplayString() -> String {
    let str: String = NameToString(this.prop.name) + " " + FloatToString(FromVariant<Float>(this.prop.first));
    return str;
  }
}

public abstract class ActionName extends ScriptableDeviceAction {

  public func GetProperties() -> [ref<DeviceActionProperty>] {
    let arr: array<ref<DeviceActionProperty>>;
    ArrayPush(arr, this.prop);
    return arr;
  }

  public const func GetCurrentDisplayString() -> String {
    let str: String = NameToString(this.prop.name) + " " + NameToString(FromVariant<CName>(this.prop.first));
    return str;
  }
}

public abstract class ActionNodeRef extends ScriptableDeviceAction {

  public func GetProperties() -> [ref<DeviceActionProperty>] {
    let arr: array<ref<DeviceActionProperty>>;
    ArrayPush(arr, this.prop);
    return arr;
  }

  public const func GetCurrentDisplayString() -> String {
    let str: String = NameToString(this.prop.name) + " NodeRef (conversion to string not supported yet)";
    return str;
  }
}

public abstract class ActionEntityReference extends ScriptableDeviceAction {

  public func GetProperties() -> [ref<DeviceActionProperty>] {
    let arr: array<ref<DeviceActionProperty>>;
    ArrayPush(arr, this.prop);
    return arr;
  }

  public const func GetCurrentDisplayString() -> String {
    let str: String = NameToString(this.prop.name) + " EntityReference (conversion to string not supported yet)";
    return str;
  }
}

public abstract class ActionWorkSpot extends ActionBool {

  private let m_workspotTarget: wref<gamePuppet>;

  public final func SetUp(owner: ref<DeviceComponentPS>, workspotTarget: wref<gamePuppet>) -> Void {
    this.SetUp(owner);
    this.m_workspotTarget = workspotTarget;
  }

  public final func GetWorkspotTarget() -> wref<gamePuppet> {
    return this.m_workspotTarget;
  }
}

public abstract class ActionSkillCheck extends ActionBool {

  protected let m_skillCheck: ref<SkillCheckBase>;

  @default(ActionDemolition, EDeviceChallengeSkill.Athletics)
  @default(ActionEngineering, EDeviceChallengeSkill.Engineering)
  @default(ActionHacking, EDeviceChallengeSkill.Hacking)
  protected let m_skillCheckName: EDeviceChallengeSkill;

  @default(ActionDemolition, LocKey#22271)
  @default(ActionEngineering, LocKey#22276)
  @default(ActionHacking, LocKey#22278)
  protected let m_localizedName: String;

  protected let m_skillcheckDescription: UIInteractionSkillCheck;

  protected let m_wasPassed: Bool;

  protected let m_availableUnpowered: Bool;

  protected func GetDefaultActionName() -> CName {
    return n"ActionSkillCheck";
  }

  public final func SetProperties(skillCheck: ref<SkillCheckBase>) -> Void {
    this.actionName = this.GetDefaultActionName();
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
    this.m_skillCheck = skillCheck;
  }

  public final func CreateInteraction(requester: ref<GameObject>, opt actions: [ref<DeviceAction>], opt alternativeMainChoiceRecord: String, opt alternativeMainChoiceRecordID: TweakDBID) -> Void {
    let choiceType: gameinteractionsChoiceType;
    this.m_hasInteraction = true;
    if IsStringValid(alternativeMainChoiceRecord) {
      this.interactionChoice.choiceMetaData.tweakDBName = alternativeMainChoiceRecord;
    } else {
      if TDBID.IsValid(alternativeMainChoiceRecordID) {
        this.interactionChoice.choiceMetaData.tweakDBID = alternativeMainChoiceRecordID;
      } else {
        if TDBID.IsValid(this.m_skillCheck.GetAlternativeName()) {
          this.interactionChoice.choiceMetaData.tweakDBID = this.m_skillCheck.GetAlternativeName();
        } else {
          this.interactionChoice.choiceMetaData.tweakDBName = this.GetTweakDBChoiceRecord();
        };
      };
    };
    this.m_skillCheck.GetBaseSkill().SetEntityID(this.GetRequesterID());
    this.m_skillcheckDescription = this.CreateSkillcheckInfo(requester);
    choiceType = this.m_wasPassed ? gameinteractionsChoiceType.CheckSuccess : gameinteractionsChoiceType.CheckFailed;
    ChoiceTypeWrapper.SetType(this.interactionChoice.choiceMetaData.type, choiceType);
    if this.m_wasPassed {
      DeviceHelper.PushActionsIntoInteractionChoice(this.interactionChoice, actions);
    };
  }

  public final func CreateSkillcheckInfo(requester: ref<GameObject>) -> UIInteractionSkillCheck {
    let requiredSkill: Int32;
    this.m_wasPassed = this.m_skillCheck.Evaluate(requester);
    this.m_skillcheckDescription.isValid = true;
    this.m_skillcheckDescription.skillCheck = this.m_skillCheckName;
    this.m_skillcheckDescription.skillName = this.m_localizedName;
    if Equals(this.m_skillCheck.GetDifficulty(), EGameplayChallengeLevel.TRIVIAL) {
      requiredSkill = 3;
    } else {
      requiredSkill = this.m_skillCheck.GetBaseSkill().GetRequiredLevel(requester.GetGame());
    };
    this.m_skillcheckDescription.requiredSkill = requiredSkill;
    this.m_skillcheckDescription.playerSkill = this.m_skillCheck.GetBaseSkill().GetPlayerSkill(requester);
    this.m_skillcheckDescription.actionDisplayName = this.interactionChoice.caption;
    this.m_skillcheckDescription.isPassed = this.m_wasPassed;
    this.m_skillcheckDescription.ownerID = this.GetRequesterID();
    if this.m_skillCheck.GetAdditionalRequirements().HasAdditionalRequirements() {
      this.m_skillcheckDescription.hasAdditionalRequirements = true;
      this.m_skillcheckDescription.additionalReqOperator = this.m_skillCheck.GetAdditionalRequirements().GetOperator();
      this.m_skillcheckDescription.additionalRequirements = this.m_skillCheck.GetAdditionalRequirements().CreateDescription(requester, this.GetRequesterID());
    };
    return this.m_skillcheckDescription;
  }

  public final const func GetPlayerStateMachine(requester: ref<GameObject>) -> ref<IBlackboard> {
    let playerPuppet: ref<PlayerPuppet> = GameInstance.GetPlayerSystem(requester.GetGame()).GetLocalPlayerMainGameObject() as PlayerPuppet;
    let playerStateMachineBlackboard: ref<IBlackboard> = GameInstance.GetBlackboardSystem(requester.GetGame()).GetLocalInstanced(playerPuppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    return playerStateMachineBlackboard;
  }

  public final const func GetSkillcheckInfo() -> UIInteractionSkillCheck {
    return this.m_skillcheckDescription;
  }

  public final const func SetSkillCheckReadyToPresentOnScreen() -> Void {
    this.m_skillCheck.GetBaseSkill().TrySetRequiredLevel(this.m_skillcheckDescription.requiredSkill);
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>, availableUnpowered: Bool) -> Bool {
    if ActionSkillCheck.IsAvailable(device, availableUnpowered) && ScriptableDeviceAction.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>, availableUnpowered: Bool) -> Bool {
    if device.IsDisabled() || device.IsUnpowered() && !availableUnpowered || device.IsBroken() {
      return false;
    };
    return true;
  }

  public func GetTweakDBChoiceRecord() -> String {
    let str: String = NameToString(this.GetDefaultActionName());
    return str;
  }

  public final func WasPassed() -> Bool {
    return this.m_wasPassed;
  }

  public final func AvailableOnUnpowered() -> Bool {
    return this.m_availableUnpowered;
  }

  public final func SetAvailableOnUnpowered() -> Void {
    this.m_availableUnpowered = true;
  }

  public func GetAttributeCheckType() -> EDeviceChallengeSkill {
    return this.m_skillCheckName;
  }

  public final func ResetCaption() -> Void {
    this.interactionChoice.caption = "";
  }
}

public class RemoteBreach extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"RemoteBreach";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}

public class PingDevice extends ActionBool {

  @default(PingDevice, true)
  private let m_shouldForward: Bool;

  public final func SetProperties() -> Void {
    this.actionName = n"Ping";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }

  public final const func ShouldForward() -> Bool {
    return this.m_shouldForward;
  }

  public final func SetShouldForward(shouldForward: Bool) -> Void {
    this.m_shouldForward = shouldForward;
  }

  public func CompleteAction(gameInstance: GameInstance) -> Void {
    super.CompleteAction(gameInstance);
    if this.m_shouldForward {
      this.GetExecutor().GetDeviceLink().PingDevicesNetwork();
    };
  }
}

public class ActionHacking extends ActionSkillCheck {

  protected func GetDefaultActionName() -> CName {
    return n"ActionHacking";
  }

  public const func GetInteractionIcon() -> wref<ChoiceCaptionIconPart_Record> {
    return TweakDBInterface.GetChoiceCaptionIconPartRecord(t"ChoiceCaptionParts.JackInIcon");
  }
}

public class ActionEngineering extends ActionSkillCheck {

  protected func GetDefaultActionName() -> CName {
    return n"ActionEngineering";
  }

  public const func GetInteractionIcon() -> wref<ChoiceCaptionIconPart_Record> {
    return TweakDBInterface.GetChoiceCaptionIconPartRecord(t"ChoiceCaptionParts.UseIcon");
  }
}

public class ActionDemolition extends ActionSkillCheck {

  public let slotID: MountingSlotId;

  protected func GetDefaultActionName() -> CName {
    return n"ActionDemolition";
  }

  public const func GetInteractionIcon() -> wref<ChoiceCaptionIconPart_Record> {
    return TweakDBInterface.GetChoiceCaptionIconPartRecord(t"ChoiceCaptionParts.UseIcon");
  }
}

public class ActionScavenge extends ActionInt {

  public final func SetProperties(amoutOfScraps: Int32) -> Void {
    this.actionName = n"ActionScavenge";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Int(this.actionName, amoutOfScraps);
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if ActionScavenge.IsAvailable(device) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.CanBeScavenged() {
      return true;
    };
    return false;
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "ActionScavenge";
  }
}

public class BaseDeviceStatus extends ActionEnum {

  public let m_isRestarting: Bool;

  public func SetProperties(const deviceRef: ref<ScriptableDeviceComponentPS>) -> Void {
    this.m_isRestarting = deviceRef.IsRestarting();
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Int(n"STATUS", EnumInt(deviceRef.GetDeviceState()));
  }

  public const func GetCurrentDisplayString() -> String {
    let str: String;
    let baseStateValue: Int32 = 0;
    if IsDefined(this.prop.first) {
      baseStateValue = FromVariant<Int32>(this.prop.first);
    };
    if this.m_isRestarting {
      return "LocKey#17797";
    };
    switch baseStateValue {
      case -2:
        str = "LocKey#17796";
        break;
      case -1:
        str = "LocKey#17793";
        break;
      case 0:
        str = "LocKey#17794";
        break;
      case 1:
        str = "LocKey#17795";
        break;
      default:
        str = "Unknown Status - DEBUG";
    };
    return str;
  }

  public final const func GetScannerStatusRecord() -> TweakDBID {
    let ending: String;
    let recordID: TweakDBID;
    let recordbase: String = "scanning_devices.";
    let baseStateValue: Int32 = FromVariant<Int32>(this.prop.first);
    if this.m_isRestarting {
      ending = "booting";
    } else {
      switch baseStateValue {
        case -2:
          ending = "disabled";
          break;
        case -1:
          ending = "unpowered";
          break;
        case 0:
          ending = "off";
          break;
        case 1:
          ending = "on";
          break;
        default:
      };
    };
    recordID = TDBID.Create(recordbase + ending);
    return recordID;
  }

  public const func GetStatusValue() -> Int32 {
    return FromVariant<Int32>(this.prop.first);
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if BaseDeviceStatus.IsAvailable(device) && BaseDeviceStatus.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.IsDisabled() {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(requesterClearancer: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(requesterClearancer, 1) {
      return true;
    };
    return false;
  }

  public final static func IsContextValid(const context: script_ref<GetActionsContext>) -> Bool {
    if Equals(Deref(context).requestType, gamedeviceRequestType.External) {
      return true;
    };
    return false;
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "wrong_action";
  }
}

public class QuestForceDestructible extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceDestructible";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceDestructible", true, n"QuestForceDestructible", n"QuestForceDestructible");
  }
}

public class QuestForceIndestructible extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestForceIndestructible";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}

public class QuestForceInvulnerable extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestForceInvulnerable";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}

public class QuestForceEnabled extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceEnabled";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceEnabled", true, n"QuestForceEnabled", n"QuestForceEnabled");
  }
}

public class QuestForceDisabled extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceDisabled";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceDisabled", true, n"QuestForceDisabled", n"QuestForceDisabled");
  }
}

public class QuestForcePower extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForcePower";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForcePower", true, n"QuestForcePower", n"QuestForcePower");
  }
}

public class QuestForceUnpower extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceUnpower";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceUnpower", true, n"QuestForceUnpower", n"QuestForceUnpower");
  }
}

public class QuestForceON extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceON";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceON", true, n"QuestForceON", n"QuestForceON");
  }
}

public class QuestForceOFF extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceOFF";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceOFF", true, n"QuestForceOFF", n"QuestForceOFF");
  }
}

public class QuestForceAuthorizationEnabled extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"AuthorizationEnable";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceAuthorizationEnabled", true, n"QuestForceAuthorizationEnabled", n"QuestForceAuthorizationEnabled");
  }
}

public class QuestForceAuthorizationDisabled extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"AuthorizationDisable";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceAuthorizationDisabled", true, n"QuestForceAuthorizationDisabled", n"QuestForceAuthorizationDisabled");
  }
}

public class QuestForceDisconnectPersonalLink extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestForceDisconnectPersonalLink";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceDisconnectPersonalLink", true, n"QuestForceDisconnectPersonalLink", n"QuestForceDisconnectPersonalLink");
  }
}

public class QuestForcePersonalLinkUnderStrictQuestControl extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestForcePersonalLinkUnderStrictQuestControl";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }
}

public class QuestEnableFixing extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"EnableFixing";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestEnableFixing", true, n"QuestEnableFixing", n"QuestEnableFixing");
  }
}

public class QuestDisableFixing extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"DisableFixing";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestDisableFixing", true, n"QuestDisableFixing", n"QuestDisableFixing");
  }
}

public class QuestForceJuryrigTrapArmed extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"JuryrigTrapArmed";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceJuryrigTrapArmed", true, n"QuestForceJuryrigTrapArmed", n"QuestForceJuryrigTrapArmed");
  }
}

public class QuestForceJuryrigTrapDeactivated extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"JuryrigTrapDeactivate";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceJuryrigTrapDeactivated", true, n"QuestForceJuryrigTrapDeactivated", n"QuestForceJuryrigTrapDeactivated");
  }
}

public class QuestForceSecuritySystemSafe extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceSecuritySystemSafe";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceSecuritySystemSafe", true, n"QuestForceSecuritySystemSafe", n"QuestForceSecuritySystemSafe");
  }
}

public class QuestForceSecuritySystemAlarmed extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceSecuritySystemAlarmed";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceSecuritySystemAlarmed", true, n"QuestForceSecuritySystemAlarmed", n"QuestForceSecuritySystemAlarmed");
  }
}

public class QuestForceSecuritySystemArmed extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceSecuritySystemArmed";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceSecuritySystemArmed", true, n"QuestForceSecuritySystemArmed", n"QuestForceSecuritySystemArmed");
  }
}

public class QuestStartGlitch extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestStartGlitch";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestStartGlitch", true, n"QuestStartGlitch", n"QuestStartGlitch");
  }
}

public class QuestStopGlitch extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestStopGlitch";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestStopGlitch", true, n"QuestStopGlitch", n"QuestStopGlitch");
  }
}

public class QuestEnableInteraction extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"EnableInteraction";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestEnableInteraction", true, n"QuestEnableInteraction", n"QuestEnableInteraction");
  }
}

public class QuestDisableInteraction extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"DisableInteraction";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestDisableInteraction", true, n"QuestDisableInteraction", n"QuestDisableInteraction");
  }
}

public class SetDeviceON extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"SetDeviceON";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"SetDeviceON", true, n"LocKey#255", n"LocKey#255");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if SetDeviceON.IsAvailable(device) && SetDeviceON.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.IsUnpowered() || device.IsDisabled() {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 29) {
      return true;
    };
    return false;
  }
}

public class SetDeviceOFF extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"SetDeviceOFF";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"SetDeviceOFF", true, n"LocKey#256", n"LocKey#256");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if SetDeviceOFF.IsAvailable(device) && SetDeviceOFF.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.IsUnpowered() || device.IsDisabled() {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 29) {
      return true;
    };
    return false;
  }
}

public class SetDeviceUnpowered extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"SetDeviceUnpowered";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"SetDeviceUnpowered", true, n"LocKey#258", n"LocKey#258");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if SetDeviceUnpowered.IsAvailable(device) && SetDeviceUnpowered.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.GetDeviceStatusAction().GetStatusValue() == -2 {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 29) {
      return true;
    };
    return false;
  }
}

public class SetDevicePowered extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"SetDevicePowered";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"SetDevicePowered", true, n"LocKey#257", n"LocKey#257");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if SetDevicePowered.IsAvailable(device) && SetDevicePowered.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.GetDeviceStatusAction().GetStatusValue() == -2 {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 29) {
      return true;
    };
    return false;
  }
}

public class DisassembleDevice extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"DisassembleDevice";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"DisassembleDevice", true, n"LocKey#264", n"LocKey#264");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if device.CanBeDisassembled() {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.CanBeDisassembled() {
      return true;
    };
    return false;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 2) {
      return true;
    };
    return false;
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "ExtractParts";
  }
}

public class FixDevice extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"FixDevice";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"FixDevice", true, n"LocKey#266", n"LocKey#266");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if device.CanBeFixed() {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.CanBeFixed() {
      return true;
    };
    return false;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 2) {
      return true;
    };
    return false;
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "FixDevice";
  }
}

public class ToggleJuryrigTrap extends ActionBool {

  public final func SetProperties(state: EJuryrigTrapState) -> Void {
    this.actionName = n"ToggleJuryrigTrap";
    let isArmed: Bool = Equals(state, EJuryrigTrapState.ARMED);
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ToggleJuryrigTrap", isArmed, n"LocKey#270", n"LocKey#270");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    return ToggleJuryrigTrap.IsAvailable(device) && ToggleJuryrigTrap.IsContextValid(context);
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    return device.CanBeTrapped() && Equals(device.GetJuryrigTrapState(), EJuryrigTrapState.UNARMED);
  }

  public final static func IsContextValid(const context: script_ref<GetActionsContext>) -> Bool {
    return NotEquals(Deref(context).requestType, gamedeviceRequestType.External);
  }

  public func GetTweakDBChoiceRecord() -> String {
    if !FromVariant<Bool>(this.prop.first) {
      return "JuryrigTrap";
    };
    return "DisableJuryrigTrap";
  }
}

public class ToggleActivation extends ActionBool {

  public final func SetProperties(status: EDeviceStatus) -> Void {
    this.actionName = n"ToggleActivation";
    let disabled: Bool = false;
    if Equals(status, EDeviceStatus.DISABLED) {
      disabled = true;
    };
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ToggleActivation", disabled, n"LocKey#247", n"LocKey#245");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if ToggleActivation.IsAvailable(device) && ToggleActivation.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 98) {
      return true;
    };
    return false;
  }
}

public class TogglePower extends ActionBool {

  @default(TogglePower, Power)
  protected let m_TrueRecordName: String;

  @default(TogglePower, Unpower)
  protected let m_FalseRecordName: String;

  public final func SetProperties(status: EDeviceStatus) -> Void {
    let unpowered: Bool;
    this.actionName = n"TogglePower";
    if Equals(status, EDeviceStatus.UNPOWERED) {
      unpowered = true;
    };
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"TogglePower", unpowered, n"LocKey#258", n"LocKey#257");
  }

  public final func SetProperties(status: EDeviceStatus, nameOnTrue: TweakDBID, nameOnFalse: TweakDBID) -> Void {
    let displayNameOnFalse: CName;
    let displayNameOnTrue: CName;
    let record: wref<InteractionBase_Record>;
    let unpowered: Bool;
    if Equals(status, EDeviceStatus.UNPOWERED) {
      unpowered = true;
    };
    this.actionName = n"TogglePower";
    if !TDBID.IsValid(nameOnTrue) {
      displayNameOnTrue = n"LocKey#258";
    } else {
      record = TweakDBInterface.GetInteractionBaseRecord(nameOnTrue);
      if IsDefined(record) {
        this.m_TrueRecordName = record.Name();
        displayNameOnTrue = StringToName(LocKeyToString(record.Caption()));
      };
    };
    if !TDBID.IsValid(nameOnFalse) {
      displayNameOnFalse = n"LocKey#257";
    } else {
      record = TweakDBInterface.GetInteractionBaseRecord(nameOnFalse);
      if IsDefined(record) {
        this.m_FalseRecordName = record.Name();
        displayNameOnFalse = StringToName(LocKeyToString(record.Caption()));
      };
    };
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, unpowered, displayNameOnTrue, displayNameOnFalse);
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if TogglePower.IsAvailable(device) && TogglePower.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.GetDeviceStatusAction().GetStatusValue() == -2 {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 19) {
      return true;
    };
    return false;
  }

  public func GetTweakDBChoiceRecord() -> String {
    if !FromVariant<Bool>(this.prop.first) {
      return "Unpower";
    };
    return "Power";
  }
}

public class ToggleON extends ActionBool {

  @default(ToggleON, On)
  protected let m_TrueRecordName: String;

  @default(ToggleON, Off)
  protected let m_FalseRecordName: String;

  public func GetBaseCost() -> Int32 {
    if this.m_isQuickHack {
      return super.GetBaseCost();
    };
    return 0;
  }

  public final func SetProperties(status: EDeviceStatus) -> Void {
    let isOn: Bool;
    this.actionName = n"ToggleON";
    if Equals(status, EDeviceStatus.ON) {
      isOn = true;
    };
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ToggleOn", isOn, n"LocKey#255", n"LocKey#256");
  }

  public final func SetProperties(status: EDeviceStatus, nameOnTrue: TweakDBID, nameOnFalse: TweakDBID) -> Void {
    let displayNameOnFalse: CName;
    let displayNameOnTrue: CName;
    let isOn: Bool;
    let record: wref<InteractionBase_Record>;
    this.actionName = n"ToggleON";
    if Equals(status, EDeviceStatus.ON) {
      isOn = true;
    };
    if !TDBID.IsValid(nameOnTrue) {
      displayNameOnTrue = n"LocKey#255";
    } else {
      record = TweakDBInterface.GetInteractionBaseRecord(nameOnTrue);
      if IsDefined(record) {
        this.m_TrueRecordName = record.Name();
        displayNameOnTrue = StringToName(LocKeyToString(record.Caption()));
      };
    };
    if !TDBID.IsValid(nameOnFalse) {
      displayNameOnFalse = n"LocKey#256";
    } else {
      record = TweakDBInterface.GetInteractionBaseRecord(nameOnFalse);
      if IsDefined(record) {
        this.m_FalseRecordName = record.Name();
        displayNameOnFalse = StringToName(LocKeyToString(record.Caption()));
      };
    };
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ToggleOn", isOn, displayNameOnTrue, displayNameOnFalse);
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if ToggleON.IsAvailable(device) && ToggleON.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.IsDisabled() || device.IsUnpowered() {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 5) {
      return true;
    };
    return false;
  }

  public func GetTweakDBChoiceRecord() -> String {
    if !FromVariant<Bool>(this.prop.first) {
      return this.m_TrueRecordName;
    };
    return this.m_FalseRecordName;
  }

  public func GetInkWidgetTweakDBID() -> TweakDBID {
    switch this.m_widgetStyle {
      case gamedataComputerUIStyle.Orange:
        return t"DevicesUIDefinitions.ToggleDeviceActionWidgetOA";
      default:
        return t"DevicesUIDefinitions.ToggleDeviceActionWidget";
    };
  }

  public func GetActivationTime() -> Float {
    if this.IsQuickHack() {
      return super.GetActivationTime();
    };
    return 0.00;
  }
}

public class QuickHackToggleON extends ActionBool {

  public let Repeat: Bool;

  public func GetBaseCost() -> Int32 {
    if this.m_isQuickHack {
      return super.GetBaseCost();
    };
    return 0;
  }

  public const func GetInteractionIcon() -> wref<ChoiceCaptionIconPart_Record> {
    return TweakDBInterface.GetChoiceCaptionIconPartRecord(t"ChoiceCaptionParts.OnOff");
  }

  public final func SetProperties(status: EDeviceStatus) -> Void {
    let isOn: Bool;
    this.actionName = n"QuickHackToggleON";
    if Equals(status, EDeviceStatus.ON) {
      isOn = true;
    };
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ToggleOn", isOn, n"LocKey#256", n"LocKey#256");
  }

  public func GetTweakDBChoiceRecord() -> String {
    if TDBID.IsValid(this.m_objectActionID) {
      return super.GetTweakDBChoiceRecord();
    };
    if !FromVariant<Bool>(this.prop.first) {
      return "On";
    };
    return "Off";
  }
}

public class ToggleBlockade extends ActionBool {

  @default(ToggleBlockade, Raise)
  protected let m_TrueRecordName: String;

  @default(ToggleBlockade, Lower)
  protected let m_FalseRecordName: String;

  public final func SetProperties(isActive: Bool, nameOnTrue: TweakDBID, nameOnFalse: TweakDBID) -> Void {
    let displayNameOnFalse: CName;
    let displayNameOnTrue: CName;
    let record: wref<InteractionBase_Record>;
    this.actionName = n"ToggleBlockade";
    if !TDBID.IsValid(nameOnTrue) {
      nameOnTrue = t"Interactions.Raise";
    };
    if !TDBID.IsValid(nameOnFalse) {
      nameOnFalse = nameOnTrue = t"Interactions.Lower";
    };
    record = TweakDBInterface.GetInteractionBaseRecord(nameOnTrue);
    this.m_FalseRecordName = record.Name();
    displayNameOnTrue = StringToName(LocKeyToString(record.Caption()));
    record = TweakDBInterface.GetInteractionBaseRecord(nameOnFalse);
    this.m_TrueRecordName = record.Name();
    displayNameOnFalse = StringToName(LocKeyToString(record.Caption()));
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ToggleBlockade", isActive, displayNameOnTrue, displayNameOnFalse);
  }

  public final static func IsDefaultConditionMet(device: ref<RoadBlockControllerPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if ToggleBlockade.IsAvailable(device) && ToggleBlockade.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<RoadBlockControllerPS>) -> Bool {
    if device.IsON() {
      return true;
    };
    return false;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 2) {
      return true;
    };
    return false;
  }

  public func GetTweakDBChoiceRecord() -> String {
    if !FromVariant<Bool>(this.prop.first) {
      return this.m_TrueRecordName;
    };
    return this.m_FalseRecordName;
  }
}

public class QuickHackToggleBlockade extends ToggleBlockade {

  public func GetTweakDBChoiceRecord() -> String {
    let recordName: String;
    if TDBID.IsValid(this.m_objectActionID) {
      recordName = this.GetObjectActionRecord().ObjectActionUI().Name();
    };
    if IsStringValid(recordName) {
      return recordName;
    };
    return super.GetTweakDBChoiceRecord();
  }
}

public class QuestForceRoadBlockadeActivate extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceRoadBlockadeActivate";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceRoadBlockadeActivate", true, n"QuestForceRoadBlockadeActivate", n"QuestForceRoadBlockadeActivate");
  }
}

public class QuestForceRoadBlockadeDeactivate extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceRoadBlockadeDeactivate";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceRoadBlockadeDeactivate", true, n"QuestForceRoadBlockadeDeactivate", n"QuestForceRoadBlockadeDeactivate");
  }
}

public class QuestForceActivate extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceActivate";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceActivate", true, n"QuestForceActivate", n"QuestForceActivate");
  }
}

public class QuestForceDeactivate extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ForceDeactivate";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestForceDeactivate", true, n"QuestForceDeactivate", n"QuestForceDeactivate");
  }
}

public class QuestPickUpCall extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"PickUpCall";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestPickUpCall", true, n"QuestPickUpCall", n"QuestPickUpCall");
  }
}

public class QuestHangUpCall extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"HangUpCall";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestHangUpCall", true, n"QuestHangUpCall", n"QuestHangUpCall");
  }
}

public class ToggleActivate extends ActionBool {

  protected let m_TrueRecordName: String;

  protected let m_FalseRecordName: String;

  public final func SetProperties(activationStatus: EActivationState) -> Void {
    let isActivated: Bool;
    if Equals(activationStatus, EActivationState.DEACTIVATED) {
      isActivated = false;
    } else {
      isActivated = true;
    };
    this.actionName = n"ToggleActivate";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ToggleActivate", isActivated, n"LocKey#233", n"LocKey#234");
  }

  public final func SetProperties(isActive: Bool, nameOnTrue: TweakDBID, nameOnFalse: TweakDBID) -> Void {
    let displayNameOnFalse: CName;
    let displayNameOnTrue: CName;
    let record: wref<InteractionBase_Record>;
    this.actionName = n"ToggleActivate";
    if !TDBID.IsValid(nameOnTrue) {
      nameOnTrue = t"Interactions.Activate";
    };
    if !TDBID.IsValid(nameOnFalse) {
      nameOnFalse = t"Interactions.Deactivate";
    };
    record = TweakDBInterface.GetInteractionBaseRecord(nameOnTrue);
    this.m_FalseRecordName = record.Name();
    displayNameOnTrue = StringToName(LocKeyToString(record.Caption()));
    record = TweakDBInterface.GetInteractionBaseRecord(nameOnFalse);
    this.m_TrueRecordName = record.Name();
    displayNameOnFalse = StringToName(LocKeyToString(record.Caption()));
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ToggleActivate", isActive, displayNameOnTrue, displayNameOnFalse);
  }

  public func GetTweakDBChoiceRecord() -> String {
    if TDBID.IsValid(this.m_objectActionID) {
      return super.GetTweakDBChoiceRecord();
    };
    if !FromVariant<Bool>(this.prop.first) {
      return this.m_TrueRecordName;
    };
    return this.m_FalseRecordName;
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    return true;
  }
}

public class ActivateDevice extends ActionBool {

  public let m_tweakDBChoiceName: String;

  public final func SetProperties(opt action_name: CName) -> Void {
    let displayName: CName;
    this.actionName = n"ActivateDevice";
    if IsNameValid(action_name) {
      displayName = action_name;
    } else {
      displayName = n"LocKey#233";
    };
    this.m_tweakDBChoiceName = NameToString(action_name);
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ActivateDevice", true, displayName, displayName);
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    return true;
  }

  public func GetTweakDBChoiceRecord() -> String {
    if TDBID.IsValid(this.m_objectActionID) {
      return super.GetTweakDBChoiceRecord();
    };
    return this.m_tweakDBChoiceName;
  }
}

public class DeactivateDevice extends ActionBool {

  public final func SetProperties(opt action_name: CName) -> Void {
    if NotEquals(action_name, n"None") {
      this.actionName = action_name;
    } else {
      this.actionName = n"LocKey#234";
    };
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"DeactivateDevice", true, this.actionName, this.actionName);
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    return true;
  }
}

public class AuthorizeUser extends ActionBool {

  private let m_enteredPassword: CName;

  private let m_validPasswords: [CName];

  private let m_libraryName: CName;

  private let m_isforced: Bool;

  public final func SetProperties(const validPasswords: script_ref<[CName]>, opt isforced: Bool) -> Void {
    this.actionName = n"AuthorizeUser";
    this.m_validPasswords = Deref(validPasswords);
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, n"LocKey#17813", n"LocKey#17813");
    this.m_isforced = isforced;
    if this.m_isforced {
      if ArraySize(this.m_validPasswords) > 0 {
        this.m_enteredPassword = this.m_validPasswords[0];
      };
    };
  }

  public final func IsForced() -> Bool {
    return this.m_isforced;
  }

  public final func GetEnteredPassword() -> CName {
    return this.m_enteredPassword;
  }

  public func ResolveAction(data: ref<ResolveActionData>) -> Bool {
    this.m_enteredPassword = StringToName(data.m_password);
    return true;
  }

  public final func GetValidPasswords() -> [CName] {
    return this.m_validPasswords;
  }

  public final func CreateActionWidgetPackage(authorizationWidgetName: CName, const authorizationDisplayNameOverride: script_ref<String>) -> Void {
    this.m_libraryName = authorizationWidgetName;
    this.CreateActionWidgetPackage();
    if NotEquals(authorizationDisplayNameOverride, "") {
      this.m_actionWidgetPackage.displayName = Deref(authorizationDisplayNameOverride);
    } else {
      this.m_actionWidgetPackage.displayName = "LocKey#210";
    };
  }

  public final func CreateActionWidgetPackage(const authorizationDisplayNameOverride: script_ref<String>) -> Void {
    this.CreateActionWidgetPackage();
    if IsStringValid(authorizationDisplayNameOverride) {
      this.m_actionWidgetPackage.displayName = Deref(authorizationDisplayNameOverride);
    };
  }

  public func GetInkWidgetTweakDBID() -> TweakDBID {
    if Equals(this.m_libraryName, n"elevator") {
      return t"DevicesUIDefinitions.AuthorizationBlockedActionWidget";
    };
    return super.GetInkWidgetTweakDBID();
  }
}

public class FactQuickHack extends ActionBool {

  private let m_factProperties: ComputerQuickHackData;

  public func GetTweakDBChoiceRecord() -> String {
    if TDBID.IsValid(this.m_objectActionID) {
      return super.GetTweakDBChoiceRecord();
    };
    return "DownloadCPOMissionData";
  }

  public func GetTweakDBChoiceID() -> TweakDBID {
    let id: TweakDBID = this.m_factProperties.alternativeName;
    return id;
  }

  public final func GetFactProperties() -> ComputerQuickHackData {
    return this.m_factProperties;
  }

  public final func SetProperties(properties: ComputerQuickHackData) -> Void {
    this.m_factProperties = properties;
  }
}

public class QuickHackAuthorization extends ActionBool {

  public func GetTweakDBChoiceRecord() -> String {
    if TDBID.IsValid(this.m_objectActionID) {
      return super.GetTweakDBChoiceRecord();
    };
    return "QuickHackAuthorization";
  }
}

public class SetAuthorizationModuleON extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"SetAuthorizationModuleON";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, n"LocKey#262", n"LocKey#262");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if SetAuthorizationModuleON.IsAvailable(device) && SetAuthorizationModuleON.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.HasAuthorizationModule() && !device.IsAuthorizationModuleOn() {
      return true;
    };
    return false;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 15) {
      return true;
    };
    return false;
  }
}

public class SetAuthorizationModuleOFF extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"SetAuthorizationModuleOFF";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, n"LocKey#263", n"LocKey#263");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if SetAuthorizationModuleOFF.IsAvailable(device) && SetAuthorizationModuleOFF.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.HasAuthorizationModule() && device.IsAuthorizationModuleOn() {
      return true;
    };
    return false;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 15) {
      return true;
    };
    return false;
  }
}

public class InstallKeylogger extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"InstallKeylogger";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, n"LocKey#376", n"LocKey#376");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if InstallKeylogger.IsAvailable(device) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.IsON() {
      return true;
    };
    return false;
  }
}

public class SetExposeQuickHacks extends ActionBool {

  @default(SetExposeQuickHacks, true)
  public let isRemote: Bool;

  public final func SetProperties() -> Void {
    this.actionName = n"SetExposeQuickHacks";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, n"SetExposeQuickHacks", n"SetExposeQuickHacks");
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "SetExposeQuickHacks";
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    return device.IsPowered();
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 2) {
      return true;
    };
    return false;
  }

  public final static func IsContextValid(const context: script_ref<GetActionsContext>) -> Bool {
    if Equals(Deref(context).requestType, gamedeviceRequestType.Direct) {
      return true;
    };
    return false;
  }
}

public class TogglePersonalLink extends ActionBool {

  public let m_cachedStatus: EPersonalLinkConnectionStatus;

  public let m_shouldSkipMiniGame: Bool;

  public final func SetProperties(personalLinkStatus: EPersonalLinkConnectionStatus, shouldSkipMinigame: Bool) -> Void {
    let isPersonalLinkConnected: Bool;
    this.m_cachedStatus = personalLinkStatus;
    this.m_shouldSkipMiniGame = shouldSkipMinigame;
    if Equals(this.m_cachedStatus, EPersonalLinkConnectionStatus.NOT_CONNECTED) {
      isPersonalLinkConnected = false;
    } else {
      isPersonalLinkConnected = true;
    };
    this.actionName = n"TogglePersonalLink";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"Personal Link", isPersonalLinkConnected, n"LocKey#284", n"LocKey#285");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if TogglePersonalLink.IsAvailable(device) && TogglePersonalLink.IsClearanceValid(Deref(context).clearance) && TogglePersonalLink.IsContextValid(context) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    return device.IsON() && device.HasPersonalLinkSlot();
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 2) {
      return true;
    };
    return false;
  }

  public final static func IsContextValid(const context: script_ref<GetActionsContext>) -> Bool {
    if Equals(Deref(context).requestType, gamedeviceRequestType.Direct) {
      return true;
    };
    return false;
  }

  public final func ShouldConnect() -> Bool {
    let value: Bool;
    DeviceActionPropertyFunctions.GetProperty_Bool(this.prop, value);
    return value;
  }

  public func GetTweakDBChoiceRecord() -> String {
    let value: Bool;
    DeviceActionPropertyFunctions.GetProperty_Bool(this.prop, value);
    if Equals(this.m_cachedStatus, EPersonalLinkConnectionStatus.NOT_CONNECTED) {
      if this.m_shouldSkipMiniGame {
        return "ConnectPersonalLinkNoMinigame";
      };
      return "ConnectPersonalLink";
    };
    return "DisconnectPersonalLink";
  }

  public const func GetInteractionIcon() -> wref<ChoiceCaptionIconPart_Record> {
    return TweakDBInterface.GetChoiceCaptionIconPartRecord(t"ChoiceCaptionParts.JackInIcon");
  }
}

public class OpenFullscreenUI extends ActionBool {

  public final func SetProperties(isZoomInteraction: Bool) -> Void {
    this.actionName = n"OpenFullscreenUI";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"Zoom Interaction", isZoomInteraction, n"LocKey#288", n"LocKey#289");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    return OpenFullscreenUI.IsAvailable(device) && OpenFullscreenUI.IsClearanceValid(Deref(context).clearance) && OpenFullscreenUI.IsContextValid(context);
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    return device.IsPowered();
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    return Clearance.IsInRange(clearance, 2);
  }

  public final static func IsContextValid(const context: script_ref<GetActionsContext>) -> Bool {
    return Equals(Deref(context).requestType, gamedeviceRequestType.Direct);
  }

  public final func ShouldConnect() -> Bool {
    let value: Bool;
    DeviceActionPropertyFunctions.GetProperty_Bool(this.prop, value);
    return !value;
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "OpenFullscreenUI";
  }
}

public class SpiderbotDistraction extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"SpiderbotDistraction";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, n"LocKey#596", n"LocKey#596");
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "SpiderbotDistraction";
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if !AIActionHelper.CheckFlatheadStatPoolRequirements(device.GetGameInstance(), "DeviceAction") {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 2) {
      return true;
    };
    return false;
  }

  public final static func IsContextValid(const context: script_ref<GetActionsContext>) -> Bool {
    if Equals(Deref(context).requestType, gamedeviceRequestType.Remote) {
      return true;
    };
    return false;
  }
}

public class SpiderbotBoolAction extends ActionBool {

  @default(SpiderbotBoolAction, SpiderbotToggleOn)
  protected let m_TrueRecord: String;

  @default(SpiderbotBoolAction, SpiderbotToggleOff)
  protected let m_FalseRecord: String;

  public final func SetProperties(status: EDeviceStatus) -> Void {
    let isOn: Bool;
    this.actionName = n"SpiderbotToggleON";
    if Equals(status, EDeviceStatus.ON) {
      isOn = true;
    };
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, isOn, n"LocKey#255", n"LocKey#256");
  }

  public final func SetProperties(status: EDeviceStatus, nameOnTrue: CName, nameOnFalse: CName) -> Void {
    let isOn: Bool;
    this.actionName = n"SpiderbotToggleON";
    if Equals(status, EDeviceStatus.ON) {
      isOn = true;
    };
    if !IsNameValid(nameOnTrue) {
      nameOnTrue = n"LocKey#255";
    } else {
      this.m_TrueRecord = NameToString(nameOnTrue);
    };
    if !IsNameValid(nameOnFalse) {
      nameOnFalse = n"LocKey#256";
    } else {
      this.m_FalseRecord = NameToString(nameOnFalse);
    };
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, isOn, nameOnTrue, nameOnFalse);
  }

  public func GetTweakDBChoiceRecord() -> String {
    if !FromVariant<Bool>(this.prop.first) {
      return this.m_TrueRecord;
    };
    return this.m_FalseRecord;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if !AIActionHelper.CheckFlatheadStatPoolRequirements(device.GetGameInstance(), "DeviceAction") {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 2) {
      return true;
    };
    return false;
  }

  public final static func IsContextValid(const context: script_ref<GetActionsContext>) -> Bool {
    if Equals(Deref(context).requestType, gamedeviceRequestType.Remote) {
      return true;
    };
    return false;
  }
}

public class ToggleZoomInteraction extends ActionBool {

  public final func SetProperties(isZoomInteraction: Bool) -> Void {
    this.actionName = n"ToggleZoomInteraction";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"Zoom Interaction", isZoomInteraction, n"LocKey#288", n"LocKey#289");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    return ToggleZoomInteraction.IsAvailable(device) && ToggleZoomInteraction.IsClearanceValid(Deref(context).clearance) && ToggleZoomInteraction.IsContextValid(context);
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    return device.IsPowered();
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    return Clearance.IsInRange(clearance, 2);
  }

  public final static func IsContextValid(const context: script_ref<GetActionsContext>) -> Bool {
    return Equals(Deref(context).requestType, gamedeviceRequestType.Direct);
  }

  public final func ShouldConnect() -> Bool {
    let value: Bool;
    DeviceActionPropertyFunctions.GetProperty_Bool(this.prop, value);
    return !value;
  }

  public func GetTweakDBChoiceRecord() -> String {
    let value: Bool;
    DeviceActionPropertyFunctions.GetProperty_Bool(this.prop, value);
    if !value {
      return "EnterZoomInteraction";
    };
    return "ExitZoomInteraction";
  }
}

public class SetDeviceAttitude extends ActionBool {

  public let Repeat: Bool;

  public let IgnoreHostiles: Bool;

  public let Attitude: EAIAttitude;

  public func GetBaseCost() -> Int32 {
    if this.m_isQuickHack {
      return super.GetBaseCost();
    };
    return 0;
  }

  public final func SetProperties() -> Void {
    this.actionName = n"SetDeviceAttitude";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, n"LocKey#362", n"LocKey#362");
  }

  public func GetTweakDBChoiceRecord() -> String {
    if TDBID.IsValid(this.m_objectActionID) {
      return super.GetTweakDBChoiceRecord();
    };
    return "SetDeviceAttitude";
  }

  public const func GetInteractionIcon() -> wref<ChoiceCaptionIconPart_Record> {
    return TweakDBInterface.GetChoiceCaptionIconPartRecord(t"ChoiceCaptionParts.ChangeToFriendlyIcon");
  }
}

public class ThumbnailUI extends ActionBool {

  protected let m_thumbnailWidgetPackage: SThumbnailWidgetPackage;

  public final func SetProperties() -> Void {
    this.actionName = n"ThumbnailUI";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ThumbnailUI", true, n"ThumbnailUI", n"ThumbnailUI");
  }

  public func CreateThumbnailWidgetPackage(opt status: String) -> Void {
    this.m_thumbnailWidgetPackage.libraryPath = this.GetInkWidgetLibraryPath();
    this.m_thumbnailWidgetPackage.libraryID = this.GetInkWidgetLibraryID();
    this.m_thumbnailWidgetPackage.widgetName = ToString(this.GetActionName());
    this.m_thumbnailWidgetPackage.displayName = this.GetDeviceName();
    this.m_thumbnailWidgetPackage.deviceStatus = status;
    this.m_thumbnailWidgetPackage.widgetTweakDBID = this.GetInkWidgetTweakDBID();
    this.ResolveThumbnailWidgetTweakDBData();
  }

  public func CreateThumbnailWidgetPackage(widgetTweakDBID: TweakDBID, opt status: String) -> Void {
    this.CreateThumbnailWidgetPackage(status);
    if TDBID.IsValid(widgetTweakDBID) {
      this.m_thumbnailWidgetPackage.widgetTweakDBID = widgetTweakDBID;
      this.ResolveThumbnailWidgetTweakDBData();
    };
  }

  public func GetInkWidgetLibraryPath() -> ResRef {
    return r"base\\movies\\misc\\distraction_generic.bk2";
  }

  public func GetInkWidgetLibraryID() -> CName {
    return n"None";
  }

  public func GetInkWidgetTweakDBID() -> TweakDBID {
    switch this.m_widgetStyle {
      case gamedataComputerUIStyle.Orange:
        return t"DevicesUIDefinitions.GenericDeviceThumnbnailWidgetOA";
      default:
        return t"DevicesUIDefinitions.GenericDeviceThumnbnailWidget";
    };
  }

  public final func GetThumbnailWidgetPackage() -> SThumbnailWidgetPackage {
    let widgetPackage: SThumbnailWidgetPackage;
    widgetPackage.thumbnailAction = this;
    if !TDBID.IsValid(this.m_thumbnailWidgetPackage.widgetTweakDBID) {
      widgetPackage.widgetTweakDBID = this.GetInkWidgetTweakDBID();
      this.ResolveThumbnailWidgetTweakDBData();
    } else {
      widgetPackage.widgetTweakDBID = this.m_thumbnailWidgetPackage.widgetTweakDBID;
    };
    widgetPackage.libraryID = this.m_thumbnailWidgetPackage.libraryID;
    widgetPackage.widgetName = this.m_thumbnailWidgetPackage.widgetName;
    widgetPackage.displayName = this.m_thumbnailWidgetPackage.displayName;
    widgetPackage.deviceStatus = this.m_thumbnailWidgetPackage.deviceStatus;
    widgetPackage.widgetTweakDBID = this.m_thumbnailWidgetPackage.widgetTweakDBID;
    widgetPackage.libraryPath = this.m_thumbnailWidgetPackage.libraryPath;
    widgetPackage.isValid = ResRef.IsValid(widgetPackage.libraryPath) || IsNameValid(widgetPackage.libraryID);
    return widgetPackage;
  }

  private final func ResolveThumbnailWidgetTweakDBData() -> Void {
    let record: ref<WidgetDefinition_Record>;
    if TDBID.IsValid(this.m_thumbnailWidgetPackage.widgetTweakDBID) {
      record = TweakDBInterface.GetWidgetDefinitionRecord(this.m_thumbnailWidgetPackage.widgetTweakDBID);
      if record != null {
        this.m_thumbnailWidgetPackage.libraryPath = record.LibraryPath();
        this.m_thumbnailWidgetPackage.libraryID = StringToName(record.LibraryID());
      };
    };
  }
}

public class QuestResetDeviceToInitialState extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestResetDeviceToInitialState";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, n"QuestResetDeviceToInitialState", n"QuestResetDeviceToInitialState");
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "QuestResetDeviceToInitialState";
  }
}

public class QuestForceCameraZoom extends ActionBool {

  @default(QuestForceCameraZoom, true)
  private let m_useWorkspot: Bool;

  private let m_instant: Bool;

  public final func SetProperties(enable: Bool, opt instant: Bool) -> Void {
    if enable {
      this.actionName = n"QuestForceEnableCameraZoom";
    } else {
      this.actionName = n"QuestForceDisableCameraZoom";
    };
    this.m_instant = instant;
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, enable, n"QuestForceEnableCameraZoom", n"QuestForceDisableCameraZoom");
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "QuestForceCameraZoom";
  }

  public final const func UseWorkspot() -> Bool {
    return this.m_useWorkspot;
  }

  public final func SetUseWorkspot(useWorkspot: Bool) -> Void {
    this.m_useWorkspot = useWorkspot;
  }

  public final const func IsInstant() -> Bool {
    return this.m_instant;
  }
}

public class PlayDeafeningMusic extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"HackVolume";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, this.actionName, this.actionName);
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if PlayDeafeningMusic.IsAvailable(device) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.IsON() || device.IsOFF() {
      return true;
    };
    return false;
  }

  public const func GetInteractionIcon() -> wref<ChoiceCaptionIconPart_Record> {
    return TweakDBInterface.GetChoiceCaptionIconPartRecord(t"ChoiceCaptionParts.DistractIcon");
  }
}

public class ChangeMusicAction extends ActionBool {

  @default(ChangeMusicAction, NextStation)
  protected let m_interactionRecordName: String;

  public let m_settings: ref<MusicSettings>;

  public final func SetProperties(settings: ref<MusicSettings>) -> Void {
    this.actionName = n"NextStation";
    this.m_settings = settings;
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"Next Station", true, n"LocKey#252", n"LocKey#252");
  }

  public final func SetProperties(settings: ref<MusicSettings>, nameOnTrue: TweakDBID) -> Void {
    let displayName: CName;
    let record: wref<InteractionBase_Record>;
    this.m_settings = settings;
    if !TDBID.IsValid(nameOnTrue) {
      displayName = n"LocKey#252";
      this.actionName = n"NextStation";
    } else {
      record = TweakDBInterface.GetInteractionBaseRecord(nameOnTrue);
      this.m_interactionRecordName = record.Name();
      this.actionName = StringToName(this.m_interactionRecordName);
      displayName = StringToName(LocKeyToString(record.Caption()));
    };
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, displayName, displayName);
  }

  public func GetTweakDBChoiceRecord() -> String {
    if TDBID.IsValid(this.m_objectActionID) {
      return super.GetTweakDBChoiceRecord();
    };
    return this.m_interactionRecordName;
  }

  public final func GetMusicSettings() -> ref<MusicSettings> {
    return this.m_settings;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if !AIActionHelper.CheckFlatheadStatPoolRequirements(device.GetGameInstance(), "DeviceAction") {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 2) {
      return true;
    };
    return false;
  }

  public final static func IsContextValid(const context: script_ref<GetActionsContext>) -> Bool {
    if Equals(Deref(context).requestType, gamedeviceRequestType.Remote) {
      return true;
    };
    return false;
  }
}

public class StartCall extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"StartCall";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"StartCall", true, n"LocKey#279", n"LocKey#279");
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "Call";
  }

  public func GetInkWidgetTweakDBID() -> TweakDBID {
    return t"DevicesUIDefinitions.IntercomCallActionWidget";
  }
}

public class Flush extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"Flush";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"Flush", true, n"LocKey#50672", n"LocKey#50672");
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "Flush";
  }
}

public class ToggleGlassTint extends ActionBool {

  @default(ToggleGlassTint, TintGlass)
  protected let m_TrueRecord: String;

  @default(ToggleGlassTint, ClearGlass)
  protected let m_FalseRecord: String;

  public final func SetProperties(isActive: Bool) -> Void {
    let record: TweakDBID = TDBID.Create("Interactions." + this.m_TrueRecord);
    let nameOnTrue: CName = StringToName(TweakDBInterface.GetInteractionBaseRecord(record).Name());
    record = TDBID.Create("Interactions." + this.m_FalseRecord);
    let nameOnFalse: CName = StringToName(TweakDBInterface.GetInteractionBaseRecord(record).Name());
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, isActive, nameOnTrue, nameOnFalse);
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if ToggleGlassTint.IsAvailable(device) && ToggleGlassTint.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.IsON() {
      return true;
    };
    return false;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 2) {
      return true;
    };
    return false;
  }

  public func GetTweakDBChoiceRecord() -> String {
    if TDBID.IsValid(this.m_objectActionID) {
      return super.GetTweakDBChoiceRecord();
    };
    if !FromVariant<Bool>(this.prop.first) {
      return this.m_TrueRecord;
    };
    return this.m_FalseRecord;
  }
}

public class QuestForceTintGlass extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestForceTintGlass";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, n"QuestForceTintGlass", n"QuestForceTintGlass");
  }
}

public class QuestForceClearGlass extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestForceClearGlass";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, n"QuestForceClearGlass", n"QuestForceClearGlass");
  }
}

public class PresetAction extends ActionBool {

  protected let m_preset: ref<SmartHousePreset>;

  public final func SetProperties(preset: ref<SmartHousePreset>) -> Void {
    this.actionName = preset.GetClassName();
    this.m_preset = preset;
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"SmartHousePreset", true, this.GetDisplayName(), this.GetDisplayName());
  }

  public final func GetPreset() -> ref<SmartHousePreset> {
    return this.m_preset;
  }

  protected final func GetDisplayName() -> CName {
    return this.m_preset.GetPresetName();
  }

  public func CreateActionWidgetPackage(opt actions: [ref<DeviceAction>]) -> Void {
    super.CreateActionWidgetPackage(actions);
    this.m_actionWidgetPackage.iconID = this.m_preset.GetIconName();
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if PresetAction.IsAvailable(device) && PresetAction.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if !device.IsON() {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 99) {
      return true;
    };
    return false;
  }

  public func GetInkWidgetTweakDBID() -> TweakDBID {
    return t"DevicesUIDefinitions.SmartHousePresetWidget";
  }
}

public class ToggleAlarm extends ActionBool {

  public final func SetProperties(status: ESecuritySystemState) -> Void {
    let isOn: Bool;
    this.actionName = n"ToggleAlarm";
    if NotEquals(status, ESecuritySystemState.SAFE) {
      isOn = true;
    };
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ToggleAlarm", isOn, n"LocKey#346", n"LocKey#345");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if ToggleAlarm.IsAvailable(device) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.IsON() {
      return true;
    };
    return false;
  }

  public func GetTweakDBChoiceRecord() -> String {
    if TDBID.IsValid(this.m_objectActionID) {
      return super.GetTweakDBChoiceRecord();
    };
    if !FromVariant<Bool>(this.prop.first) {
      return "TurnOnCarAlarm";
    };
    return "TurnOffCarAlarm";
  }
}

public class SecurityAlarmBreachResponse extends ActionBool {

  private let m_currentSecurityState: ESecuritySystemState;

  public final func SetProperties(currentSecuritySystemState: ESecuritySystemState) -> Void {
    this.actionName = n"SecurityAlarmBreachResponse";
    this.m_currentSecurityState = currentSecuritySystemState;
  }

  public final const func GetSecurityState() -> ESecuritySystemState {
    return this.m_currentSecurityState;
  }
}

public class SecurityAlarmEscalate extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"StartAlarm";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"StartAlarm", true, n"LocKey#340", n"LocKey#344");
  }

  public func GetTweakDBChoiceRecord() -> String {
    if TDBID.IsValid(this.m_objectActionID) {
      return super.GetTweakDBChoiceRecord();
    };
    return "StartAlarm";
  }
}

public class MasterDeviceDestroyed extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"MasterDeviceDestroyed";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"MasterDeviceDestroyed", true, n"MasterDeviceDestroyed", n"MasterDeviceDestroyed");
  }
}

public class DelayEvent extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"DelayEvent";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"DelayEvent", true, n"DelayEvent", n"DelayEvent");
  }
}

public class Distraction extends ActionBool {

  public final func SetProperties(action_name: CName) -> Void {
    this.actionName = n"Distraction";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(action_name, true, action_name, action_name);
  }

  public final func SetProperties() -> Void {
    this.actionName = n"Distraction";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"LocKey#336", true, n"LocKey#336", n"LocKey#336");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if device.IsPowered() {
      return true;
    };
    return false;
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "Distract";
  }
}

public class TogglePlay extends ActionBool {

  public final func SetProperties(isPlaying: Bool) -> Void {
    this.actionName = n"TogglePlay";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"Play", isPlaying, n"LocKey#280", n"LocKey#281");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    if TogglePlay.IsAvailable(device) && TogglePlay.IsClearanceValid(Deref(context).clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.IsDisabled() {
      return false;
    };
    if device.IsUnpowered() {
      return false;
    };
    if device.IsDeviceSecured() {
      return false;
    };
    if !device.IsON() {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 2) {
      return true;
    };
    return false;
  }

  public func GetInkWidgetTweakDBID() -> TweakDBID {
    return t"DevicesUIDefinitions.JukeboxPlayActionWidget";
  }
}

public class OpenInteriorManager extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"OpenInteriorManager";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"OpenInteriorManager", true, n"LocKey#27969", n"LocKey#27969");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    return true;
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "InteriorManager";
  }
}

public class EnterLadder extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"EnterLadder";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"EnterLadder", true, n"EnterLadder", n"EnterLadder");
  }

  public final static func IsPlayerInAcceptableState(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    let playerSMBlackboard: ref<IBlackboard> = EnterLadder.GetPlayerStateMachine(Deref(context).processInitiatorObject);
    let isUsingLadder: Bool = playerSMBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LocomotionDetailed) == 10 || playerSMBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LocomotionDetailed) == 11 || playerSMBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LocomotionDetailed) == 12 || playerSMBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LocomotionDetailed) == 13;
    let isJumping: Bool = playerSMBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Locomotion) == 5;
    if isUsingLadder {
      return false;
    };
    if isJumping {
      return false;
    };
    if playerSMBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.HighLevel) != 1 {
      return false;
    };
    if playerSMBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.Carrying) {
      return false;
    };
    if playerSMBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Takedown) != 0 {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(Deref(context).processInitiatorObject, n"NoWorldInteractions") {
      return false;
    };
    return true;
  }

  public final static func PushOnEnterLadderEventToPSM(requester: ref<GameObject>) -> Void {
    let psmEvent: ref<PSMPostponedParameterBool> = new PSMPostponedParameterBool();
    psmEvent.id = n"actionEnterLadder";
    psmEvent.value = true;
    requester.QueueEvent(psmEvent);
  }

  public final static func GetPlayerStateMachine(requester: ref<GameObject>) -> ref<IBlackboard> {
    let playerPuppet: ref<PlayerPuppet> = GameInstance.GetPlayerSystem(requester.GetGame()).GetLocalPlayerMainGameObject() as PlayerPuppet;
    let playerStateMachineBlackboard: ref<IBlackboard> = GameInstance.GetBlackboardSystem(requester.GetGame()).GetLocalInstanced(playerPuppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    return playerStateMachineBlackboard;
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    return true;
  }

  public func GetTweakDBChoiceRecord() -> String {
    return "EnterLadder";
  }
}

public class ProgramSetDeviceOff extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ProgramSetDeviceOff";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ProgramSetDeviceOff", true, n"LocKey#256", n"LocKey#256");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    return true;
  }
}

public class ProgramSetDeviceAttitude extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"ProgramSetDeviceAttitude";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"ProgramSetDeviceAttitude", true, n"LocKey#362", n"LocKey#362");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, const context: script_ref<GetActionsContext>) -> Bool {
    return true;
  }
}

public class QuestResetPerformedActionsStorage extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"QuestResetPerformedActionsStorage";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(n"QuestResetPerformedActionsStorage", true, n"QuestResetPerformedActionsStorage", n"QuestResetPerformedActionsStorage");
  }
}
