
public class TimeDilationHelper extends IScriptable {

  public final static func GetTimeDilationParameters(out timeDilationParameters: ref<TimeDilationParameters>, const profileName: script_ref<String>) -> Void {
    timeDilationParameters = new TimeDilationParameters();
    timeDilationParameters.reason = TimeDilationHelper.GetCNameFromTimeSystemTweak(profileName, "reason");
    timeDilationParameters.timeDilation = TimeDilationHelper.GetFloatFromTimeSystemTweak(profileName, "timeDilation");
    timeDilationParameters.playerTimeDilation = TimeDilationHelper.GetFloatFromTimeSystemTweak(profileName, "playerTimeDilation");
    timeDilationParameters.duration = TimeDilationHelper.GetFloatFromTimeSystemTweak(profileName, "duration");
    timeDilationParameters.easeInCurve = TimeDilationHelper.GetCNameFromTimeSystemTweak(profileName, "easeInCurve");
    timeDilationParameters.easeOutCurve = TimeDilationHelper.GetCNameFromTimeSystemTweak(profileName, "easeOutCurve");
  }

  public final static func SetTimeDilationWithProfile(requester: wref<GameObject>, const profileName: script_ref<String>, enable: Bool, allowMultipleTimeDilationSimultaneously: Bool) -> Bool {
    let duration: Float;
    let easeInCurve: CName;
    let easeOutCurve: CName;
    let isTimeDilationActive: Bool;
    let playerTimeDilation: Float;
    let reason: CName;
    let timeDilation: Float;
    let timeDilationParameters: ref<TimeDilationParameters>;
    let timeSystem: ref<TimeSystem>;
    if !IsDefined(requester) {
      return false;
    };
    timeSystem = GameInstance.GetTimeSystem(requester.GetGame());
    if !IsDefined(timeSystem) {
      return false;
    };
    isTimeDilationActive = timeSystem.IsTimeDilationActive();
    if !enable && !isTimeDilationActive || !allowMultipleTimeDilationSimultaneously && enable && isTimeDilationActive || IsMultiplayer() {
      return false;
    };
    TimeDilationHelper.GetTimeDilationParameters(timeDilationParameters, profileName);
    reason = timeDilationParameters.reason;
    timeDilation = timeDilationParameters.timeDilation;
    playerTimeDilation = timeDilationParameters.playerTimeDilation;
    duration = timeDilationParameters.duration;
    easeInCurve = timeDilationParameters.easeInCurve;
    easeOutCurve = timeDilationParameters.easeOutCurve;
    if enable {
      TimeDilationHelper.SetTimeDilation(requester, reason, timeDilation, duration, easeInCurve, easeOutCurve, allowMultipleTimeDilationSimultaneously);
      TimeDilationHelper.SetTimeDilationOnPlayer(requester, reason, playerTimeDilation, duration, easeInCurve, easeOutCurve, allowMultipleTimeDilationSimultaneously);
    } else {
      TimeDilationHelper.UnSetTimeDilation(requester, reason, easeOutCurve);
      TimeDilationHelper.UnSetTimeDilationOnPlayer(requester, reason, easeOutCurve);
    };
    return true;
  }

  public final static func SetTimeDilation(requester: wref<GameObject>, reason: CName, timeDilation: Float, opt duration: Float, easeInCurve: CName, easeOutCurve: CName, allowMultipleTimeDilationSimultaneously: Bool, opt listener: ref<TimeDilationListener>) -> Bool {
    let timeSystem: ref<TimeSystem> = GameInstance.GetTimeSystem(requester.GetGame());
    if !IsDefined(timeSystem) || !allowMultipleTimeDilationSimultaneously && timeSystem.IsTimeDilationActive() || IsMultiplayer() {
      return false;
    };
    timeSystem.SetTimeDilation(reason, timeDilation, duration, easeInCurve, easeOutCurve, listener);
    return true;
  }

  public final static func SetTimeDilationOnPlayer(requester: wref<GameObject>, reason: CName, timeDilation: Float, opt duration: Float, easeInCurve: CName, easeOutCurve: CName, allowMultipleTimeDilationSimultaneously: Bool, opt listener: ref<TimeDilationListener>) -> Bool {
    let timeSystem: ref<TimeSystem> = GameInstance.GetTimeSystem(requester.GetGame());
    if !IsDefined(timeSystem) || !allowMultipleTimeDilationSimultaneously && timeSystem.IsTimeDilationActive() || IsMultiplayer() {
      return false;
    };
    TimeDilationHelper.SetIgnoreTimeDilationOnLocalPlayerZero(requester, false);
    timeSystem.SetTimeDilationOnLocalPlayerZero(reason, timeDilation, duration, easeInCurve, easeOutCurve);
    return true;
  }

  public final static func UnSetTimeDilation(requester: wref<GameObject>, opt reason: CName, opt easeOutCurve: CName) -> Bool {
    let timeSystem: ref<TimeSystem> = GameInstance.GetTimeSystem(requester.GetGame());
    if !IsDefined(timeSystem) || !timeSystem.IsTimeDilationActive() || IsMultiplayer() {
      return false;
    };
    if !IsNameValid(easeOutCurve) {
      timeSystem.UnsetTimeDilation(reason, n"None");
    } else {
      timeSystem.UnsetTimeDilation(reason, easeOutCurve);
    };
    return true;
  }

  public final static func UnSetTimeDilationOnPlayer(requester: wref<GameObject>, opt reason: CName, opt easeOutCurve: CName) -> Bool {
    let timeSystem: ref<TimeSystem> = GameInstance.GetTimeSystem(requester.GetGame());
    if !IsDefined(timeSystem) || IsMultiplayer() {
      return false;
    };
    if !IsNameValid(easeOutCurve) {
      timeSystem.UnsetTimeDilationOnLocalPlayerZero(reason, n"None");
    } else {
      timeSystem.UnsetTimeDilationOnLocalPlayerZero(reason, easeOutCurve);
    };
    TimeDilationHelper.RestorePreviousIgnoreTimeDilationOnLocalPlayerZero(requester);
    return true;
  }

  public final static func SetIgnoreTimeDilationOnLocalPlayerZero(requester: wref<GameObject>, ignore: Bool) -> Bool {
    let timeSystem: ref<TimeSystem> = GameInstance.GetTimeSystem(requester.GetGame());
    if !IsDefined(timeSystem) || IsMultiplayer() {
      return false;
    };
    timeSystem.SetIgnoreTimeDilationOnLocalPlayerZero(ignore);
    return true;
  }

  public final static func RestorePreviousIgnoreTimeDilationOnLocalPlayerZero(requester: wref<GameObject>) -> Bool {
    let timeSystem: ref<TimeSystem> = GameInstance.GetTimeSystem(requester.GetGame());
    if !IsDefined(timeSystem) || IsMultiplayer() {
      return false;
    };
    timeSystem.RestorePreviousIgnoreTimeDilationOnLocalPlayerZero();
    return true;
  }

  public final static func SetIndividualTimeDilation(target: wref<GameObject>, reason: CName, timeDilation: Float, opt duration: Float, opt easeInCurve: CName, opt easeOutCurve: CName) -> Bool {
    if IsMultiplayer() {
      return false;
    };
    (target as gamePuppet).SetIndividualTimeDilation(reason, timeDilation, duration, easeInCurve, easeOutCurve);
    return true;
  }

  public final static func UnsetIndividualTimeDilation(target: wref<GameObject>, opt easeOutCurve: CName) -> Bool {
    if IsMultiplayer() {
      return false;
    };
    (target as gamePuppet).UnsetIndividualTimeDilation(easeOutCurve);
    return true;
  }

  public final static func CanUseTimeDilation(playerGameObject: wref<GameObject>) -> Bool {
    let playerVehicleState: Int32;
    let player: ref<PlayerPuppet> = playerGameObject as PlayerPuppet;
    if !IsDefined(player) {
      return false;
    };
    playerVehicleState = player.GetPlayerStateMachineBlackboard().GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vehicle);
    if playerVehicleState != 0 {
      if playerVehicleState != 1 && playerVehicleState != 6 && playerVehicleState != 4 {
        return false;
      };
      if !PlayerDevelopmentSystem.GetData(player).IsNewPerkBoughtAnyLevel(gamedataNewPerkType.Cool_Left_Milestone_1) {
        return false;
      };
    };
    if GameInstance.GetStatusEffectSystem(playerGameObject.GetGame()).HasStatusEffect(playerGameObject.GetEntityID(), t"BaseStatusEffect.PlayerInFinisherWorkspot") {
      return false;
    };
    return true;
  }

  public final static func GetFloatFromTimeSystemTweak(const tweakDBPath: script_ref<String>, const paramName: script_ref<String>) -> Float {
    return TweakDBInterface.GetFloat(TDBID.Create("timeSystem." + tweakDBPath + "." + paramName), 0.00);
  }

  public final static func GetCNameFromTimeSystemTweak(const tweakDBPath: script_ref<String>, const paramName: script_ref<String>) -> CName {
    return TweakDBInterface.GetCName(TDBID.Create("timeSystem." + tweakDBPath + "." + paramName), n"None");
  }

  public final static func GetTimeDilationKey() -> CName {
    return n"timeDilation";
  }

  public final static func GetSandevistanKey() -> CName {
    return n"sandevistan";
  }

  public final static func GetSandevistanVersusSandevistanKey() -> CName {
    return n"sandevistanVersusSandevistan";
  }

  public final static func GetKerenzikovKey() -> CName {
    return n"kereznikov";
  }

  public final static func GetFocusModeKey() -> CName {
    return n"focusMode";
  }

  public final static func GetFocusedStateKey() -> CName {
    return n"focusedStatePerkDilation";
  }
}
