
public class CursorRootController extends inkLogicController {

  public edit let m_mainCursor: inkWidgetRef;

  public edit let m_cursorPattern: inkWidgetRef;

  public edit let m_progressBar: inkWidgetRef;

  public edit let m_progressBarFrame: inkWidgetRef;

  protected let m_animProxy: ref<inkAnimProxy>;

  protected cb func OnInitialize() -> Bool {
    inkWidgetRef.SetVisible(this.m_progressBar, false);
  }

  public final func PlayAnim(context: CName, animationOverride: CName) -> Void {
    let animation: CName;
    if IsDefined(this.m_animProxy) {
      this.m_animProxy.GotoEndAndStop(true);
      this.m_animProxy = null;
    };
    if NotEquals(animationOverride, n"None") {
      animation = this.GetAnimNameFromContext(animationOverride);
    } else {
      animation = this.GetAnimNameFromContext(context);
    };
    if NotEquals(animation, n"None") {
      this.m_animProxy = this.PlayLibraryAnimation(animation);
    };
    if IsDefined(this.m_animProxy) {
      this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, n"OnAnimationFinished");
    };
  }

  protected func GetAnimNameFromContext(context: CName) -> CName {
    return n"None";
  }

  protected cb func OnAnimationFinished(proxy: ref<inkAnimProxy>) -> Bool {
    this.m_animProxy = null;
  }
}

public class GamepadCursorRootController extends CursorRootController {

  protected func GetAnimNameFromContext(context: CName) -> CName {
    let animation: CName;
    switch context {
      case n"Show":
        animation = n"show";
        break;
      case n"Hide":
        animation = n"hide";
        break;
      case n"Default":
        animation = n"default";
        break;
      case n"Hover":
        animation = n"hover";
        break;
      case n"hoverOnHoldToComplete":
        animation = n"hoverOnHoldToComplete";
        break;
      case n"InvalidAction":
        animation = n"invalid";
    };
    return animation;
  }
}

public class MouseCursorRootController extends CursorRootController {

  protected func GetAnimNameFromContext(context: CName) -> CName {
    let animation: CName;
    switch context {
      case n"Show":
        animation = n"show_mouse";
        break;
      case n"Hide":
        animation = n"hide_mouse";
        break;
      case n"Default":
        animation = n"default_mouse";
        break;
      case n"Hover":
        animation = n"hover_mouse";
        break;
      case n"Pan":
        animation = n"pan_mouse";
        break;
      case n"hoverOnHoldToComplete":
        animation = n"hoverOnHoldToComplete_mouse";
        break;
      case n"InvalidAction":
        animation = n"invalid_mouse";
    };
    return animation;
  }
}

public class WorldMapCursorRootController extends CursorRootController {

  protected func GetAnimNameFromContext(context: CName) -> CName {
    let animation: CName;
    return animation;
  }
}

public class CursorGameController extends inkGameController {

  private let m_cursorRoot: wref<CursorRootController>;

  private let m_currentContext: CName;

  private let m_margin: inkMargin;

  private let m_data: ref<MenuCursorUserData>;

  @default(CursorGameController, false)
  private let m_isCursorVisible: Bool;

  @default(CursorGameController, default)
  private let m_cursorType: CName;

  private let m_cursorForDevice: CName;

  private let m_dpadAnimProxy: ref<inkAnimProxy>;

  private let m_clickAnimProxy: ref<inkAnimProxy>;

  protected cb func OnInitialize() -> Bool {
    let cursorInfo: ref<inkCursorInfo>;
    let root: ref<inkWidget> = this.GetRootWidget();
    root.RegisterToCallback(n"OnSetCursorVisibility", this, n"OnSetCursorVisibility");
    root.RegisterToCallback(n"OnSetCursorPosition", this, n"OnSetCursorPosition");
    root.RegisterToCallback(n"OnSetCursorScale", this, n"OnSetCursorScale");
    root.RegisterToCallback(n"OnSetCursorContext", this, n"OnSetCursorContext");
    root.RegisterToCallback(n"OnSetCursorType", this, n"OnSetCursorType");
    root.RegisterToCallback(n"OnSetCursorForDevice", this, n"OnSetCursorForDevice");
    root.RegisterToCallback(n"OnDpadCursorMoved", this, n"OnDpadCursorMoved");
    this.RegisterToGlobalInputCallback(n"OnPostOnHold", this, n"OnHold");
    this.RegisterToGlobalInputCallback(n"OnPostOnRelease", this, n"OnRelease");
    this.m_isCursorVisible = false;
    cursorInfo = root.GetUserData(n"inkCursorInfo") as inkCursorInfo;
    if IsDefined(cursorInfo) && NotEquals(cursorInfo.cursorForDevice, n"None") {
      this.OnSetCursorForDevice(cursorInfo.cursorForDevice);
    };
  }

  protected cb func OnUnitialize() -> Bool {
    let root: ref<inkWidget> = this.GetRootWidget();
    root.UnregisterFromCallback(n"OnSetCursorVisibility", this, n"OnSetCursorVisibility");
    root.UnregisterFromCallback(n"OnSetCursorPosition", this, n"OnSetCursorPosition");
    root.UnregisterFromCallback(n"OnSetCursorScale", this, n"OnSetCursorScale");
    root.UnregisterFromCallback(n"OnSetCursorContext", this, n"OnSetCursorContext");
    root.UnregisterFromCallback(n"OnSetCursorType", this, n"OnSetCursorType");
    root.UnregisterFromCallback(n"OnSetCursorForDevice", this, n"OnSetCursorForDevice");
    root.UnregisterFromCallback(n"OnDpadCursorMoved", this, n"OnDpadCursorMoved");
    this.UnregisterFromGlobalInputCallback(n"OnPostOnHold", this, n"OnHold");
    this.UnregisterFromGlobalInputCallback(n"OnPostOnRelease", this, n"OnRelease");
  }

  protected cb func OnSetCursorVisibility(isVisible: Bool) -> Bool {
    if NotEquals(this.m_isCursorVisible, isVisible) {
      this.m_isCursorVisible = isVisible;
      if this.m_isCursorVisible {
        this.ProcessCursorContext(n"Show", null);
      } else {
        this.ProcessCursorContext(n"Hide", null);
      };
    };
  }

  protected cb func OnSetCursorPosition(const pos: Vector2) -> Bool {
    this.m_margin.left = pos.X;
    this.m_margin.top = pos.Y;
    if IsDefined(this.m_cursorRoot) {
      inkWidgetRef.SetMargin(this.m_cursorRoot.m_mainCursor, this.m_margin);
    };
  }

  protected cb func OnSetCursorScale(const scale: Vector2) -> Bool {
    let cursorVisibilityInfo: ref<inkCursorInfo>;
    let mainCursorScale: Vector2;
    let mainCursorSize: Vector2;
    let root: ref<inkCompoundWidget>;
    let size: Vector2;
    let type: CName;
    if IsDefined(this.m_cursorRoot) {
      mainCursorScale = inkWidgetRef.GetScale(this.m_cursorRoot.m_mainCursor);
      if NotEquals(scale, mainCursorScale) {
        inkWidgetRef.SetScale(this.m_cursorRoot.m_mainCursor, scale);
        root = this.GetRootWidget() as inkCompoundWidget;
        cursorVisibilityInfo = root.GetUserData(n"inkCursorInfo") as inkCursorInfo;
        if IsDefined(cursorVisibilityInfo) {
          type = this.GetCursorType();
          if NotEquals(type, n"mouse") && NotEquals(type, n"world_map") {
            mainCursorSize = inkWidgetRef.GetSize(this.m_cursorRoot.m_mainCursor);
            size.X = mainCursorSize.X * scale.X;
            size.Y = mainCursorSize.Y * scale.Y;
          };
          cursorVisibilityInfo.SetSize(size);
        };
      };
    };
  }

  protected cb func OnSetCursorContext(const context: CName, data: ref<inkUserData>) -> Bool {
    this.ProcessCursorContext(context, data);
  }

  protected cb func OnSetCursorType(const type: CName) -> Bool {
    if NotEquals(this.m_cursorType, type) {
      this.m_cursorType = type;
      this.SpawnCursor();
    };
  }

  protected cb func OnSetCursorForDevice(const type: CName) -> Bool {
    if NotEquals(this.m_cursorForDevice, type) {
      this.m_cursorForDevice = type;
      this.SpawnCursor();
    };
  }

  protected cb func OnDpadCursorMoved(angle: Float) -> Bool {
    let cursorVisibilityInfo: ref<inkCursorInfo>;
    let scale: Vector2;
    let size: Vector2;
    let root: ref<inkCompoundWidget> = this.GetRootWidget() as inkCompoundWidget;
    if IsDefined(this.m_dpadAnimProxy) {
      this.m_dpadAnimProxy.Stop(true);
      this.m_dpadAnimProxy = null;
    };
    if IsDefined(this.m_cursorRoot) {
      cursorVisibilityInfo = root.GetUserData(n"inkCursorInfo") as inkCursorInfo;
      if IsDefined(cursorVisibilityInfo) {
        scale = inkWidgetRef.GetScale(this.m_cursorRoot.m_mainCursor);
        size = inkWidgetRef.GetSize(this.m_cursorRoot.m_mainCursor);
        size.X *= scale.X;
        size.Y *= scale.Y;
        cursorVisibilityInfo.SetSize(size);
      };
      inkWidgetRef.SetRotation(this.m_cursorRoot.m_cursorPattern, angle);
      this.m_dpadAnimProxy = this.m_cursorRoot.PlayLibraryAnimation(n"wipe");
    };
  }

  protected func GetCursorType() -> CName {
    if Equals(this.m_cursorType, n"default") || Equals(this.m_cursorForDevice, n"mouse") {
      if NotEquals(this.m_cursorType, n"dpad") {
        return this.m_cursorForDevice;
      };
    };
    return this.m_cursorType;
  }

  public final func SpawnCursor() -> Void {
    let type: CName = this.GetCursorType();
    this.AsyncSpawnFromLocal(this.GetRootWidget(), type, this, n"OnCursorSpawned");
  }

  protected cb func OnCursorSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    let cursorVisibilityInfo: ref<inkCursorInfo>;
    let size: Vector2;
    let type: CName;
    let root: ref<inkCompoundWidget> = this.GetRootWidget() as inkCompoundWidget;
    if IsDefined(this.m_cursorRoot) {
      root.RemoveChild(this.m_cursorRoot.GetRootWidget());
    };
    this.m_cursorRoot = widget.GetController() as CursorRootController;
    cursorVisibilityInfo = root.GetUserData(n"inkCursorInfo") as inkCursorInfo;
    if IsDefined(cursorVisibilityInfo) {
      cursorVisibilityInfo.SetResizableWidget(inkWidgetRef.Get(this.m_cursorRoot.m_mainCursor));
      type = this.GetCursorType();
      if IsDefined(this.m_cursorRoot) && Equals(type, n"dpad") {
        inkWidgetRef.SetSize(this.m_cursorRoot.m_mainCursor, cursorVisibilityInfo.GetResizableWidgetSize());
      };
      if IsDefined(this.m_cursorRoot) && NotEquals(type, n"mouse") && NotEquals(type, n"world_map") {
        size = inkWidgetRef.GetSize(this.m_cursorRoot.m_mainCursor);
      };
      cursorVisibilityInfo.SetSize(size);
      this.OnSetCursorVisibility(cursorVisibilityInfo.isVisible);
      this.OnSetCursorPosition(cursorVisibilityInfo.pos);
    };
    this.ProcessCursorContext(this.m_currentContext, null, true);
  }

  protected cb func OnHold(evt: ref<inkPointerEvent>) -> Bool {
    let progress: Float = evt.GetHoldProgress();
    if progress >= 1.00 {
      return false;
    };
    if this.m_data == null {
      return false;
    };
    if this.DoesActionMatch(evt, this.m_data.GetActions()) {
      this.PlaySound(n"Attributes", n"OnStart");
      this.UpdateFillPercent(progress);
    };
  }

  protected cb func OnRelease(evt: ref<inkPointerEvent>) -> Bool {
    let actionsList: array<CName>;
    if (Equals(this.m_currentContext, n"HoldToComplete") || Equals(this.m_currentContext, n"Hover")) && this.isClickAction(evt) {
      if IsDefined(this.m_clickAnimProxy) {
        this.m_clickAnimProxy.GotoStartAndStop();
        this.m_clickAnimProxy = null;
      };
      if Equals(this.m_cursorType, n"dpad") {
        this.m_clickAnimProxy = this.m_cursorRoot.PlayLibraryAnimation(n"click_feedback_dpad");
      } else {
        this.m_clickAnimProxy = this.m_cursorRoot.PlayLibraryAnimation(n"click_feedback_" + this.m_cursorForDevice);
      };
    };
    if IsDefined(this.m_data) {
      actionsList = this.m_data.GetActions();
    };
    if this.DoesActionMatch(evt, actionsList) {
      this.UpdateFillPercent(0.00);
    };
  }

  public final func UpdateFillPercent(percent: Float) -> Void {
    let newScale: Vector2;
    newScale.X = percent;
    newScale.Y = 1.00;
    if IsDefined(this.m_cursorRoot) {
      if inkWidgetRef.IsValid(this.m_cursorRoot.m_progressBarFrame) {
        inkWidgetRef.SetVisible(this.m_cursorRoot.m_progressBarFrame, percent > 0.00);
      };
      inkWidgetRef.SetVisible(this.m_cursorRoot.m_progressBar, percent > 0.00);
      if Equals(this.m_cursorType, n"dpad") {
        inkWidgetRef.Get(this.m_cursorRoot.m_progressBar).SetEffectParamValue(inkEffectType.LinearWipe, n"LinearWipe_0", n"transition", percent);
      } else {
        inkWidgetRef.SetScale(this.m_cursorRoot.m_progressBar, newScale);
      };
    };
  }

  private final func ProcessCursorContext(const context: CName, data: ref<inkUserData>, opt force: Bool) -> Void {
    let animationOverride: CName;
    if NotEquals(this.m_currentContext, context) || this.m_data != data || force {
      this.m_currentContext = context;
      this.m_data = data as MenuCursorUserData;
      if IsDefined(this.m_data) {
        animationOverride = this.m_data.GetAnimationOverride();
      };
      this.UpdateFillPercent(0.00);
      if IsDefined(this.m_cursorRoot) && NotEquals(this.m_cursorType, n"dpad") {
        this.m_cursorRoot.PlayAnim(context, animationOverride);
      };
      if Equals(context, n"Hover") {
        GameInstance.GetAudioSystem(this.GetPlayerControlledObject().GetGame()).Play(n"ui_menu_hover");
      };
    };
  }

  public final func isClickAction(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"click") || evt.IsAction(n"activate") || evt.IsAction(n"proceed") || evt.IsAction(n"one_click_confirm") {
      return true;
    };
    return false;
  }

  private final func DoesActionMatch(evt: ref<inkPointerEvent>, const actionsList: script_ref<[CName]>) -> Bool {
    let i: Int32;
    let count: Int32 = ArraySize(Deref(actionsList));
    if Cast<Bool>(count) {
      i = 0;
      while i < count {
        if evt.IsAction(Deref(actionsList)[i]) {
          return true;
        };
        i += 1;
      };
    };
    return false;
  }
}

public class MenuCursorUserData extends inkUserData {

  private let animationOverride: CName;

  private let actions: [CName];

  public final func SetAnimationOverride(anim: CName) -> Void {
    this.animationOverride = anim;
  }

  public final func GetAnimationOverride() -> CName {
    return this.animationOverride;
  }

  public final func AddAction(action: CName) -> Void {
    ArrayPush(this.actions, action);
  }

  public final func AddUniqueAction(action: CName) -> Void {
    if !ArrayContains(this.actions, action) {
      ArrayPush(this.actions, action);
    };
  }

  public final func RemoveAction(action: CName) -> Void {
    ArrayRemove(this.actions, action);
  }

  public final func GetActions() -> [CName] {
    return this.actions;
  }

  public final func GetActionsListSize() -> Int32 {
    return ArraySize(this.actions);
  }
}
