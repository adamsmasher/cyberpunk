
public class TestScriptableComponent extends ScriptableComponent {

  private final func OnGameAttach() -> Void;

  private final func OnGameDetach() -> Void;

  private final func OnEditorAttach() -> Void;

  private final func OnEditorDetach() -> Void;

  private final func OnUpdate(deltaTime: Float) -> Void;

  protected cb func OnHit(evt: ref<gameHitEvent>) -> Bool;

  protected cb func OnTakeControl(ri: EntityResolveComponentsInterface) -> Bool;
}
