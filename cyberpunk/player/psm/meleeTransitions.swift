
public class DriverCombatListener extends IScriptable {

  public let m_mountedCallback: ref<CallbackHandle>;

  public let m_tppCallback: ref<CallbackHandle>;

  public let m_isMounted: Bool;

  public let m_isInTPP: Bool;

  public final func Init(const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let allBlackboardDef: ref<AllBlackboardDefinitions> = GetAllBlackboardDefs();
    this.m_mountedCallback = scriptInterface.localBlackboard.RegisterListenerBool(allBlackboardDef.PlayerStateMachine.MountedToVehicleInDriverSeat, this, n"OnMountedInDriverSeatChanged");
    this.m_tppCallback = scriptInterface.localBlackboard.RegisterListenerBool(allBlackboardDef.PlayerStateMachine.IsDriverCombatInTPP, this, n"OnDriveCombatTPPChanged");
    this.m_isMounted = scriptInterface.localBlackboard.GetBool(allBlackboardDef.PlayerStateMachine.MountedToVehicleInDriverSeat);
    this.m_isInTPP = scriptInterface.localBlackboard.GetBool(allBlackboardDef.PlayerStateMachine.IsDriverCombatInTPP);
  }

  public final func UnInit(const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let allBlackboardDef: ref<AllBlackboardDefinitions> = GetAllBlackboardDefs();
    scriptInterface.localBlackboard.UnregisterListenerBool(allBlackboardDef.PlayerStateMachine.MountedToVehicleInDriverSeat, this.m_mountedCallback);
    scriptInterface.localBlackboard.UnregisterListenerBool(allBlackboardDef.PlayerStateMachine.IsDriverCombatInTPP, this.m_tppCallback);
    this.m_mountedCallback = null;
    this.m_tppCallback = null;
  }

  protected cb func OnMountedInDriverSeatChanged(value: Bool) -> Bool {
    this.m_isMounted = value;
  }

  protected cb func OnDriveCombatTPPChanged(value: Bool) -> Bool {
    this.m_isInTPP = value;
  }

  public final const func IsMounted() -> Bool {
    return this.m_isMounted;
  }

  public final const func IsMountedInTPP() -> Bool {
    return this.m_isMounted && this.m_isInTPP;
  }
}

public abstract class MeleeTransition extends DefaultTransition {

  public let m_stateNameString: String;

  protected let m_driverCombatListener: ref<DriverCombatListener>;

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_stateNameString = NameToString(this.GetStateName());
    DefaultTransition.UppercaseFirstChar(this.m_stateNameString);
    this.m_driverCombatListener = new DriverCombatListener();
    this.m_driverCombatListener.Init(scriptInterface);
  }

  protected func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_driverCombatListener.UnInit(scriptInterface);
    this.m_driverCombatListener = null;
  }

  public final static func GetMeleeAttackCooldownName() -> CName {
    return n"MeleeAttackCooldown";
  }

  public final static func GetHoldEnterDuration(const scriptInterface: ref<StateGameScriptInterface>) -> Float {
    return scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.HoldEnterDuration);
  }

  protected final static func GetGorillaArmsSpyTreeStatusEffectID() -> TweakDBID {
    return t"BaseStatusEffect.GorillaArmsSpyBuff";
  }

  protected final static func GetMantisBladesSpecialAttackSpyTreeStatusEffectID() -> TweakDBID {
    return t"BaseStatusEffect.MantisBladesSpecialAttackSpyBuff";
  }

  protected final static func GetMantisBladesInvulnerableLeapRelicBufffStatusEffectID() -> TweakDBID {
    return t"BaseStatusEffect.MantisBladesInvulnerableLeapRelicBuff";
  }

  protected final static func GetMantisBladesLeapDismembermentSpyTreeDebuffStatusEffectID() -> TweakDBID {
    return t"BaseStatusEffect.MantisBladesDismembermentSpyDebuff";
  }

  protected final static func GetGorillaArmsOnePunchNPCMarkStatusEffectID() -> TweakDBID {
    return t"BaseStatusEffect.GorillaArmsOnePunchNPCMark";
  }

  protected final static func HasOnePunchManStatusEffect(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"OnePunchMan");
  }

  protected final static func HasGrandFinaleStatusEffect(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"GrandFinale");
  }

  protected final static func PlayerLeapedToTargetCheck(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"JustLeaped");
  }

  protected final func ConsumeStamina(scriptInterface: ref<StateGameScriptInterface>, attackData: ref<MeleeAttackData>) -> Void {
    if attackData.staminaCost > 0.00 {
      PlayerStaminaHelpers.ModifyStamina(scriptInterface.executionOwner as PlayerPuppet, -attackData.staminaCost);
    };
  }

  protected final func IsCloseEnoughForOnePunch(const scriptInterface: ref<StateGameScriptInterface>, target: wref<GameObject>) -> Bool {
    let cameraWorldTransform: Transform = scriptInterface.GetCameraWorldTransform();
    let targetPosition: Vector4 = target.GetWorldPosition();
    let distance: Float = Vector4.Length(targetPosition - cameraWorldTransform.position);
    return distance <= this.GetStaticFloatParameterDefault("dashOnePunchAttackEnemyMaxRange", 8.00);
  }

  protected final const func CanPerformRelicLeap(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let hasMantisBlades: Bool = Equals(MeleeTransition.GetWeaponType(scriptInterface), gamedataItemType.Cyb_MantisBlades);
    let hasPerkPurchased: Bool = MeleeTransition.HasNewSpyAttackStatFlag(scriptInterface);
    return hasPerkPurchased && hasMantisBlades && MeleeTransition.WeaponIsCharged(scriptInterface);
  }

  protected final const func GetSlotTransformToTarget(const scriptInterface: ref<StateGameScriptInterface>, target: ref<GameObject>, leapDuration: Float, isTargetKnockedOver: Bool, out targetPos: Vector4) -> Void {
    let rotationDuration: Float;
    this.GetSlotTransformToTarget(scriptInterface, target, leapDuration, isTargetKnockedOver, targetPos, rotationDuration);
  }

  protected final const func GetSlotTransformToTarget(const scriptInterface: ref<StateGameScriptInterface>, target: ref<GameObject>, leapDuration: Float, isTargetKnockedOver: Bool, out targetPos: Vector4, out rotationDuration: Float) -> Void {
    let lerpAlpha: Float;
    let playerPos: Vector4;
    let slotComponent: ref<SlotComponent>;
    let slotTransform: WorldTransform;
    let slotTransformPos: Vector4;
    let scriptedPuppet: ref<ScriptedPuppet> = target as ScriptedPuppet;
    if !IsDefined(scriptedPuppet) {
      if IsDefined(target as WeakspotObject) {
        targetPos = target.GetWorldPosition();
        targetPos.Z -= 0.50;
      };
      return;
    };
    slotComponent = scriptedPuppet.GetSlotComponent();
    if !IsDefined(slotComponent) {
      return;
    };
    if slotComponent.GetSlotTransform(n"Center", slotTransform) {
      slotTransformPos = WorldPosition.ToVector4(WorldTransform.GetWorldPosition(slotTransform));
      if target.IsDrone() {
        slotTransformPos.Z -= 1.50;
      };
      targetPos = slotTransformPos;
    } else {
      if this.GetClosestSlotTransform(scriptInterface, isTargetKnockedOver, slotComponent, slotTransform) {
        slotTransformPos = WorldPosition.ToVector4(WorldTransform.GetWorldPosition(slotTransform));
        targetPos = target.GetWorldPosition();
        playerPos = scriptInterface.executionOwner.GetWorldPosition();
        if slotTransformPos.Z < playerPos.Z {
          lerpAlpha = (playerPos.Z - targetPos.Z) / Vector4.Distance(playerPos, targetPos);
          targetPos.Z = LerpF(lerpAlpha, targetPos.Z, slotTransformPos.Z, true);
        };
        targetPos.X = slotTransformPos.X;
        targetPos.Y = slotTransformPos.Y;
        rotationDuration = leapDuration;
      };
    };
  }

  protected final const func ToMeleeChargedHold(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if MeleeTransition.WantsToStrongAttack(stateContext, scriptInterface) {
      return true;
    };
    if scriptInterface.GetActionValue(n"MeleeStrongAttack") > 0.00 {
      return true;
    };
    if this.GetInStateTime() > MeleeTransition.GetHoldEnterDuration(scriptInterface) {
      return true;
    };
    return false;
  }

  protected final static func HasNewSpyAttackStatFlag(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return RPGManager.HasStatFlag(scriptInterface.executionOwner, gamedataStatType.CanUseNewMeleewareAttackSpyTree);
  }

  protected final static func WeaponIsCharged(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let weaponObject: ref<WeaponObject> = MeleeTransition.GetWeaponObject(scriptInterface);
    return weaponObject.IsCharged();
  }

  protected final static func HasMonowireWithQuickhackSelected(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let weaponObject: ref<WeaponObject> = MeleeTransition.GetWeaponObject(scriptInterface);
    let quickhackRecord: ref<Item_Record> = RPGManager.GetMonoWireQuickhackRecord(weaponObject);
    return quickhackRecord != null && TDBID.IsValid(quickhackRecord.GetID());
  }

  protected final static func GetWeaponType(const scriptInterface: ref<StateGameScriptInterface>) -> gamedataItemType {
    let weaponType: gamedataItemType;
    let weaponObject: ref<WeaponObject> = MeleeTransition.GetWeaponObject(scriptInterface);
    return weaponType = WeaponObject.GetWeaponType(weaponObject.GetItemID());
  }

  protected final const func IsChoice1Released(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.IsActionJustReleased(n"Choice1");
  }

  protected final const func IsBlockPressed(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.IsActionJustPressed(n"MeleeBlock");
  }

  protected final const func IsBlockHeld(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.GetActionValue(n"MeleeBlock") > 0.50;
  }

  public final static func LightMeleeAttackPressed(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.IsActionJustPressed(n"MeleeLightAttack");
  }

  protected final const func LightMeleeAttackReleased(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.IsActionJustReleased(n"MeleeLightAttack");
  }

  public final static func MeleeAttackPressed(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.IsActionJustPressed(n"MeleeAttack");
  }

  public final static func MeleeAttackReleased(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.IsActionJustReleased(n"MeleeAttack");
  }

  protected final static func QuickMeleePressed(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.IsActionJustPressed(n"QuickMelee");
  }

  protected final const func QuickMeleeHeld(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.IsActionJustHeld(n"QuickMelee");
  }

  protected final static func QuickMeleeReleased(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.IsActionJustReleased(n"QuickMelee");
  }

  protected final static func QuickMeleeTapped(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.IsActionJustTapped(n"QuickMelee");
  }

  public final static func StrongMeleeAttackPressed(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.IsActionJustPressed(n"MeleeStrongAttack");
  }

  public final static func StrongMeleeAttackReleased(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.IsActionJustReleased(n"MeleeStrongAttack");
  }

  public final static func AnyMeleeAttack(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.GetActionValue(n"MeleeAttack") > 0.00 {
      return true;
    };
    if scriptInterface.GetActionValue(n"MeleeLightAttack") > 0.00 {
      return true;
    };
    if scriptInterface.GetActionValue(n"MeleeHeavyAttack") > 0.00 {
      return true;
    };
    if scriptInterface.GetActionValue(n"QuickMelee") > 0.00 {
      return true;
    };
    return false;
  }

  public final static func GetAimAssistMeleeRecord(const scriptInterface: ref<StateGameScriptInterface>) -> ref<AimAssistMelee_Record> {
    let aimAssistRecord: ref<AimAssistConfigPreset_Record> = null;
    let record: ref<AimAssistMelee_Record> = null;
    let aimAsisstRecordId: TweakDBID = scriptInterface.GetTargetingSystem().GetAimAssistConfig(scriptInterface.executionOwner);
    aimAssistRecord = TweakDBInterface.GetAimAssistConfigPresetRecord(aimAsisstRecordId);
    if IsDefined(aimAssistRecord) {
      record = aimAssistRecord.MeleeParams();
    };
    return record;
  }

  public final static func AnyMeleeAttackPressed(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return MeleeTransition.MeleeAttackPressed(scriptInterface) || MeleeTransition.LightMeleeAttackPressed(scriptInterface) || MeleeTransition.StrongMeleeAttackPressed(scriptInterface) || MeleeTransition.QuickMeleeTapped(scriptInterface);
  }

  public final static func NoMeleeAttack(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.GetActionValue(n"MeleeAttack") > 0.00 {
      return false;
    };
    if scriptInterface.GetActionValue(n"MeleeLightAttack") > 0.00 {
      return false;
    };
    if scriptInterface.GetActionValue(n"MeleeStrongAttack") > 0.00 {
      return false;
    };
    if scriptInterface.GetActionValue(n"QuickMelee") > 0.00 {
      return false;
    };
    return true;
  }

  protected final const func NoStrongAttackPressed(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.GetActionValue(n"MeleeAttack") > 0.00 {
      return false;
    };
    if scriptInterface.GetActionValue(n"MeleeStrongAttack") > 0.00 {
      return false;
    };
    return true;
  }

  protected final const func ShouldHold(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>, opt skipDurationCheck: Bool, opt skipPressCount: Bool) -> Bool {
    if stateContext.GetConditionBool(n"StrongMeleeAttackPressed") {
      return true;
    };
    if skipPressCount || MeleeTransition.CheckMeleeAttackPressCount(stateContext, scriptInterface) {
      if scriptInterface.GetActionValue(n"MeleeStrongAttack") > 0.00 {
        return true;
      };
      if scriptInterface.GetActionValue(n"MeleeAttack") > 0.50 && (skipDurationCheck || scriptInterface.GetActionStateTime(n"MeleeAttack") >= MeleeTransition.GetHoldEnterDuration(scriptInterface)) {
        return true;
      };
    };
    return false;
  }

  public final static func CheckMeleeAttackPressCount(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let lastChargePressCount: StateResultInt;
    let actionPressCount: Uint32 = scriptInterface.GetActionPressCount(n"MeleeAttack");
    actionPressCount += scriptInterface.GetActionPressCount(n"MeleeLightAttack");
    actionPressCount += scriptInterface.GetActionPressCount(n"MeleeStrongAttack");
    actionPressCount += scriptInterface.GetActionPressCount(n"QuickMelee");
    lastChargePressCount = stateContext.GetPermanentIntParameter(n"LastMeleePressCount");
    if lastChargePressCount.valid && lastChargePressCount.value == Cast<Int32>(actionPressCount) {
      return false;
    };
    return true;
  }

  protected final func SetMeleeAttackPressCount(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let actionPressCount: Uint32 = scriptInterface.GetActionPressCount(n"MeleeAttack");
    actionPressCount += scriptInterface.GetActionPressCount(n"MeleeLightAttack");
    actionPressCount += scriptInterface.GetActionPressCount(n"MeleeStrongAttack");
    actionPressCount += scriptInterface.GetActionPressCount(n"QuickMelee");
    stateContext.SetPermanentIntParameter(n"LastMeleePressCount", Cast<Int32>(actionPressCount), true);
  }

  protected final func ClearMeleePressCount(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    stateContext.SetPermanentIntParameter(n"LastMeleePressCount", 0, true);
  }

  public final static func WantsToStrongAttack(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if MeleeTransition.IsPlayingSyncedAnimation(scriptInterface) {
      return false;
    };
    if stateContext.GetConditionBool(n"StrongMeleeAttackPressed") {
      return true;
    };
    if MeleeTransition.CheckMeleeAttackPressCount(stateContext, scriptInterface) {
      if MeleeTransition.StrongMeleeAttackReleased(scriptInterface) {
        return true;
      };
      if MeleeTransition.MeleeAttackReleased(scriptInterface) && scriptInterface.GetActionPrevStateTime(n"MeleeAttack") > MeleeTransition.GetHoldEnterDuration(scriptInterface) {
        return true;
      };
    };
    return false;
  }

  public final static func WantsToLightAttack(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if MeleeTransition.IsPlayingSyncedAnimation(scriptInterface) {
      return false;
    };
    if stateContext.GetConditionBool(n"LightMeleeAttackPressed") {
      return true;
    };
    if MeleeTransition.CheckMeleeAttackPressCount(stateContext, scriptInterface) {
      if MeleeTransition.MeleeAttackReleased(scriptInterface) {
        return true;
      };
      if MeleeTransition.LightMeleeAttackPressed(scriptInterface) {
        return true;
      };
    };
    return false;
  }

  public final static func WantsToQuickMelee(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if MeleeTransition.IsPlayingSyncedAnimation(scriptInterface) {
      return false;
    };
    if stateContext.GetConditionBool(n"QuickMeleeAttackTapped") {
      return true;
    };
    if MeleeTransition.CheckMeleeAttackPressCount(stateContext, scriptInterface) {
      if MeleeTransition.QuickMeleeTapped(scriptInterface) {
        return true;
      };
    };
    return false;
  }

  public final static func IsThrownWeaponReloading(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.GetStatPoolsSystem().HasStatPoolValueReachedMax(Cast<StatsObjectID>(MeleeTransition.GetWeaponObject(scriptInterface).GetEntityID()), gamedataStatPoolType.ThrowRecovery) {
      return false;
    };
    return true;
  }

  protected final const func ShouldInterruptHoldStates(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let interruptEvent: StateResultBool;
    let finisherTargetID: EntityID = scriptInterface.localBlackboard.GetEntityID(GetAllBlackboardDefs().PlayerStateMachine.FinisherTarget);
    if EntityID.IsDefined(finisherTargetID) {
      return true;
    };
    if !this.IsWeaponReady(stateContext, scriptInterface) {
      return true;
    };
    if this.IsAttackParried(stateContext, scriptInterface) {
      return true;
    };
    if this.IsSafeStateForced(stateContext, scriptInterface) {
      return true;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == 1 && !DefaultTransition.IsInRpgContext(scriptInterface) {
      return true;
    };
    interruptEvent = stateContext.GetPermanentBoolParameter(n"InterruptMelee");
    if interruptEvent.value {
      return true;
    };
    return false;
  }

  public final static func UpdateMeleeInputBuffer(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, opt onlyLightMeleeAttack: Bool) -> Void {
    if MeleeTransition.IsPlayingSyncedAnimation(scriptInterface) {
      MeleeTransition.ClearInputBuffer(stateContext);
      return;
    };
    if onlyLightMeleeAttack {
      if MeleeTransition.WantsToLightAttack(stateContext, scriptInterface) || MeleeTransition.WantsToStrongAttack(stateContext, scriptInterface) {
        stateContext.SetConditionBoolParameter(n"StrongMeleeAttackPressed", false, true);
        stateContext.SetConditionBoolParameter(n"LightMeleeAttackPressed", true, true);
        stateContext.SetConditionBoolParameter(n"QuickMeleeAttackTapped", false, true);
      };
      return;
    };
    if MeleeTransition.WantsToStrongAttack(stateContext, scriptInterface) {
      stateContext.SetConditionBoolParameter(n"StrongMeleeAttackPressed", true, true);
      stateContext.SetConditionBoolParameter(n"LightMeleeAttackPressed", false, true);
      stateContext.SetConditionBoolParameter(n"QuickMeleeAttackTapped", false, true);
    } else {
      if MeleeTransition.WantsToLightAttack(stateContext, scriptInterface) {
        stateContext.SetConditionBoolParameter(n"StrongMeleeAttackPressed", false, true);
        stateContext.SetConditionBoolParameter(n"LightMeleeAttackPressed", true, true);
        stateContext.SetConditionBoolParameter(n"QuickMeleeAttackTapped", false, true);
      } else {
        if MeleeTransition.WantsToQuickMelee(stateContext, scriptInterface) {
          stateContext.SetConditionBoolParameter(n"StrongMeleeAttackPressed", false, true);
          stateContext.SetConditionBoolParameter(n"LightMeleeAttackPressed", false, true);
          stateContext.SetConditionBoolParameter(n"QuickMeleeAttackTapped", true, true);
        };
      };
    };
  }

  public final static func ClearInputBuffer(stateContext: ref<StateContext>) -> Void {
    stateContext.SetConditionBoolParameter(n"LightMeleeAttackPressed", false, true);
    stateContext.SetConditionBoolParameter(n"StrongMeleeAttackPressed", false, true);
    stateContext.SetConditionBoolParameter(n"QuickMeleeAttackTapped", false, true);
  }

  protected final const func CheckItemType(const scriptInterface: ref<StateGameScriptInterface>, const itemType: gamedataItemType) -> Bool {
    let currentItemType: gamedataItemType;
    if !DefaultTransition.GetWeaponItemType(scriptInterface, MeleeTransition.GetWeaponObject(scriptInterface), currentItemType) || NotEquals(currentItemType, itemType) {
      return false;
    };
    return true;
  }

  public final static func MeleeSprintStateCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Melee) == 2 && (PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Body_Right_Milestone_2) < 2 || !WeaponObject.IsBlunt(GameObject.GetActiveWeapon(scriptInterface.executionOwner).GetItemID()) || scriptInterface.GetStatPoolsSystem().GetStatPoolValue(Cast<StatsObjectID>(scriptInterface.executionOwner.GetEntityID()), gamedataStatPoolType.Stamina, true) <= 0.00) {
      return false;
    };
    if !stateContext.GetBoolParameter(n"canSprintWhileCharging", true) && Equals(stateContext.GetStateMachineCurrentState(n"Melee"), n"meleeChargedHold") {
      return false;
    };
    if stateContext.GetBoolParameter(n"isAttacking", true) && !stateContext.IsStateActive(n"Melee", n"meleeBodySlamAttack") {
      return false;
    };
    return true;
  }

  public final static func MeleeUseExplorationCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Melee) == 2 {
      return false;
    };
    if Equals(stateContext.GetStateMachineCurrentState(n"Melee"), n"meleeChargedHold") {
      return false;
    };
    if stateContext.GetBoolParameter(n"isAttacking", true) {
      return false;
    };
    if !DefaultTransition.HasMeleeWeaponEquipped(scriptInterface) {
      return true;
    };
    return true;
  }

  protected final func IncrementAttackNumber(scriptInterface: ref<StateGameScriptInterface>, stateContext: ref<StateContext>) -> Void {
    let attacksNumber: Int32;
    let currentValue: StateResultInt = stateContext.GetPermanentIntParameter(n"meleeAttackNumber");
    let value: Int32 = currentValue.value;
    value += 1;
    attacksNumber = Cast<Int32>(scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.AttacksNumber));
    if value >= attacksNumber {
      if this.CheckIfInfiniteCombo(stateContext, scriptInterface) {
        value = 1;
      } else {
        value = 0;
      };
    };
    this.SetAttackNumber(stateContext, value);
  }

  protected final func IncrementTotalComboAttackNumber(scriptInterface: ref<StateGameScriptInterface>, stateContext: ref<StateContext>) -> Void {
    let currentValue: StateResultInt = stateContext.GetPermanentIntParameter(n"totalMeleeAttacksInCombo");
    let value: Int32 = currentValue.value;
    value += 1;
    stateContext.SetPermanentIntParameter(n"totalMeleeAttacksInCombo", value, true);
  }

  protected final const func CheckIfFinalAttack(const scriptInterface: ref<StateGameScriptInterface>, const stateContext: ref<StateContext>) -> Bool {
    let attacksNumber: Int32;
    let currentValue: StateResultInt;
    let value: Int32;
    if this.CheckIfInfiniteCombo(stateContext, scriptInterface) {
      return false;
    };
    currentValue = stateContext.GetPermanentIntParameter(n"meleeAttackNumber");
    value = currentValue.value + 1;
    attacksNumber = Cast<Int32>(scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.AttacksNumber));
    return value >= attacksNumber;
  }

  protected final const func CheckIfInfiniteCombo(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.HasWeaponStatFlag(scriptInterface, gamedataStatType.CanWeaponInfinitlyCombo) {
      return false;
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.CanMeleeInfinitelyCombo) {
      return false;
    };
    return true;
  }

  protected final func ResetAttackNumber(stateContext: ref<StateContext>) -> Void {
    stateContext.SetPermanentIntParameter(n"meleeAttackNumber", 0, true);
    stateContext.SetPermanentIntParameter(n"totalMeleeAttacksInCombo", 0, true);
  }

  protected final func SetAttackNumber(stateContext: ref<StateContext>, value: Int32) -> Void {
    stateContext.SetPermanentIntParameter(n"meleeAttackNumber", value, true);
  }

  protected final func SetCanSprintWhileCharging(stateContext: ref<StateContext>, value: Bool) -> Void {
    stateContext.SetPermanentBoolParameter(n"canSprintWhileCharging", value, true);
  }

  protected final func SetIsAttacking(stateContext: ref<StateContext>, value: Bool) -> Void {
    stateContext.SetPermanentBoolParameter(n"isAttacking", value, true);
  }

  protected final func SetIsBlocking(stateContext: ref<StateContext>, value: Bool) -> Void {
    stateContext.SetPermanentBoolParameter(n"isBlocking", value, true);
  }

  protected final func SetIsParried(stateContext: ref<StateContext>, value: Bool) -> Void {
    stateContext.SetPermanentBoolParameter(n"isParried", value, true);
  }

  protected final func SetIsThrowReloading(stateContext: ref<StateContext>, value: Bool) -> Void {
    stateContext.SetPermanentBoolParameter(n"isThrowReloading", value, true);
  }

  protected final func SetThrowReloadTime(stateContext: ref<StateContext>, value: Float) -> Void {
    stateContext.SetPermanentFloatParameter(n"throwReloadTime", value, true);
  }

  protected final func SetIsTargeting(stateContext: ref<StateContext>, value: Bool) -> Void {
    stateContext.SetPermanentBoolParameter(n"isTargeting", value, true);
  }

  protected final func SetIsHolding(stateContext: ref<StateContext>, value: Bool) -> Void {
    stateContext.SetPermanentBoolParameter(n"isHolding", value, true);
  }

  protected final func SetIsSafe(stateContext: ref<StateContext>, value: Bool) -> Void {
    stateContext.SetPermanentBoolParameter(n"isSafe", value, true);
  }

  protected final const func ApplyThrowAttackGameplayRestrictions(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"GameplayRestriction.NoRadialMenus");
    StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"GameplayRestriction.FirearmsNoUnequipNoSwitch");
  }

  protected final const func RemoveAllMeleeGameplayRestrictions(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.RemoveThrowAttackGameplayRestrictions(stateContext, scriptInterface);
  }

  protected final const func RemoveThrowAttackGameplayRestrictions(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    StatusEffectHelper.RemoveStatusEffect(scriptInterface.executionOwner, t"GameplayRestriction.NoRadialMenus");
    StatusEffectHelper.RemoveStatusEffect(scriptInterface.executionOwner, t"GameplayRestriction.FirearmsNoUnequipNoSwitch");
  }

  protected final const func IsWeaponReady(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let isTakedown: Bool = scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Takedown) == 2 || scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Takedown) == 3 || scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Takedown) == 4;
    let isInFocusMode: Bool = scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Vision) == 1;
    let isUsingCombatGadget: Bool = stateContext.IsStateMachineActive(n"CombatGadget");
    if this.IsNoCombatActionsForced(scriptInterface) {
      return false;
    };
    if stateContext.IsStateMachineActive(n"Consumable") || stateContext.IsStateMachineActive(n"CombatGadget") {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffectOfType(scriptInterface.executionOwner, gamedataStatusEffectType.Stunned) {
      return false;
    };
    if this.IsRightHandInUnequippingState(stateContext) {
      return false;
    };
    if MeleeTransition.IsPlayingSyncedAnimation(scriptInterface) {
      return false;
    };
    if isInFocusMode && !DefaultTransition.IsInRpgContext(scriptInterface) {
      return false;
    };
    if isUsingCombatGadget {
      return false;
    };
    if isTakedown {
      return false;
    };
    return true;
  }

  protected final const func HasWeaponStatFlag(const scriptInterface: ref<StateGameScriptInterface>, flag: gamedataStatType) -> Bool {
    let flagOn: Bool = scriptInterface.GetStatsSystem().GetStatBoolValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), flag);
    return flagOn;
  }

  protected final func GetPlayerAimingStatusEffectID() -> TweakDBID {
    return t"BaseStatusEffect.PlayerAiming";
  }

  protected final func DrawDebugText(scriptInterface: ref<StateGameScriptInterface>, out textLayerId: Uint32, const text: script_ref<String>) -> Void {
    textLayerId = GameInstance.GetDebugVisualizerSystem(scriptInterface.GetGame()).DrawText(new Vector4(500.00, 550.00, 0.00, 0.00), Deref(text), gameDebugViewETextAlignment.Left, new Color(255u, 255u, 0u, 255u));
    GameInstance.GetDebugVisualizerSystem(scriptInterface.GetGame()).SetScale(textLayerId, new Vector4(1.00, 1.00, 0.00, 0.00));
  }

  protected final func ClearDebugText(scriptInterface: ref<StateGameScriptInterface>, textLayerId: Uint32) -> Void {
    GameInstance.GetDebugVisualizerSystem(scriptInterface.GetGame()).ClearLayer(textLayerId);
  }

  protected final const func CheckLeapCollision(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let geometryDescription: ref<GeometryDescriptionQuery>;
    let geometryDescriptionResult: ref<GeometryDescriptionResult>;
    let queryFilter: QueryFilter;
    let cameraWorldTransform: Transform = scriptInterface.GetCameraWorldTransform();
    QueryFilter.AddGroup(queryFilter, n"Static");
    QueryFilter.AddGroup(queryFilter, n"PlayerBlocker");
    geometryDescription = new GeometryDescriptionQuery();
    geometryDescription.refPosition = Transform.GetPosition(cameraWorldTransform);
    geometryDescription.refDirection = Transform.GetForward(cameraWorldTransform);
    geometryDescription.filter = queryFilter;
    geometryDescription.primitiveDimension = new Vector4(0.10, 0.10, 0.10, 0.00);
    geometryDescription.maxDistance = 5.00;
    geometryDescription.maxExtent = 5.00;
    geometryDescription.probingPrecision = 0.05;
    geometryDescription.probingMaxDistanceDiff = 5.00;
    geometryDescription.AddFlag(worldgeometryDescriptionQueryFlags.DistanceVector);
    geometryDescriptionResult = scriptInterface.GetSpatialQueriesSystem().GetGeometryDescriptionSystem().QueryExtents(geometryDescription);
    if Equals(geometryDescriptionResult.queryStatus, worldgeometryDescriptionQueryStatus.NoGeometry) {
      return true;
    };
    return false;
  }

  protected final func GetPerfectAimSnapParams() -> AimRequest {
    let aimSnapParams: AimRequest;
    aimSnapParams.duration = 0.33;
    aimSnapParams.adjustPitch = true;
    aimSnapParams.adjustYaw = true;
    aimSnapParams.endOnAimingStopped = true;
    aimSnapParams.precision = 0.10;
    aimSnapParams.easeIn = true;
    aimSnapParams.easeOut = true;
    aimSnapParams.checkRange = true;
    aimSnapParams.processAsInput = true;
    aimSnapParams.bodyPartsTracking = true;
    aimSnapParams.bptMaxDot = 0.50;
    aimSnapParams.bptMaxSwitches = -1.00;
    aimSnapParams.bptMinInputMag = 0.50;
    aimSnapParams.bptMinResetInputMag = 0.10;
    return aimSnapParams;
  }

  protected final func GetBlockLookAtParams() -> AimRequest {
    let aimSnapParams: AimRequest;
    aimSnapParams.duration = 30.00;
    aimSnapParams.adjustPitch = true;
    aimSnapParams.adjustYaw = true;
    aimSnapParams.endOnAimingStopped = false;
    aimSnapParams.precision = 0.10;
    aimSnapParams.easeIn = true;
    aimSnapParams.easeOut = true;
    aimSnapParams.checkRange = true;
    aimSnapParams.processAsInput = true;
    aimSnapParams.bodyPartsTracking = false;
    aimSnapParams.bptMaxDot = 0.50;
    aimSnapParams.bptMaxSwitches = -1.00;
    aimSnapParams.bptMinInputMag = 0.50;
    aimSnapParams.bptMinResetInputMag = 0.10;
    return aimSnapParams;
  }

  protected final func SendAnimFeatureData(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let animFeature: ref<AnimFeature_MeleeData> = new AnimFeature_MeleeData();
    animFeature.attackType = stateContext.GetIntParameter(n"attackType", true);
    animFeature.attackNumber = stateContext.GetIntParameter(n"meleeAttackNumber", true);
    animFeature.attackSpeed = stateContext.GetFloatParameter(n"attackSpeed", true);
    animFeature.hasDeflectAnim = stateContext.GetBoolParameter(n"hasDeflectAnim", true);
    animFeature.hasHitAnim = stateContext.GetBoolParameter(n"hasHitAnim", true);
    let weaponObject: ref<WeaponObject> = MeleeTransition.GetWeaponObject(scriptInterface);
    animFeature.isAttacking = stateContext.GetBoolParameter(n"isAttacking", true);
    animFeature.isTargeting = stateContext.GetBoolParameter(n"isTargeting", true);
    animFeature.isBlocking = stateContext.GetBoolParameter(n"isBlocking", true);
    animFeature.isParried = stateContext.GetBoolParameter(n"isParried", true);
    animFeature.isHolding = stateContext.GetBoolParameter(n"isHolding", true);
    animFeature.shouldHandsDisappear = weaponObject.HasTag(n"Cyberware");
    animFeature.keepRenderPlane = weaponObject.HasTag(n"KeepRenderPlane");
    animFeature.isSafe = stateContext.GetBoolParameter(n"isSafe", true);
    animFeature.isThrowReloading = stateContext.GetBoolParameter(n"isThrowReloading", true);
    animFeature.throwReloadTime = stateContext.GetFloatParameter(n"throwReloadTime", true);
    animFeature.isMeleeWeaponEquipped = true;
    scriptInterface.SetAnimationParameterFeature(n"MeleeData", animFeature);
  }

  protected final func DisableNanoWireIK(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.UpdateNanoWireEndPositionAnimFeature(scriptInterface, n"ikRightNanoWire", false);
    this.UpdateNanoWireEndPositionAnimFeature(scriptInterface, n"ikLeftNanoWire", false);
  }

  protected final func UpdateNanoWireEndPositionAnimFeature(scriptInterface: ref<StateGameScriptInterface>, animFeatureName: CName, enable: Bool, opt setPosition: Bool, opt slotPosition: Vector4) -> Void {
    let animFeature: ref<AnimFeature_SimpleIkSystem> = new AnimFeature_SimpleIkSystem();
    animFeature.isEnable = enable;
    animFeature.setPosition = setPosition;
    animFeature.position = slotPosition;
    scriptInterface.SetAnimationParameterFeature(animFeatureName, animFeature);
  }

  protected final func GetMeleeMovementDirection(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> meleeMoveDirection {
    let direction: meleeMoveDirection;
    let currentYaw: Float = DefaultTransition.GetYawMovementDirection(stateContext, scriptInterface);
    if currentYaw >= -45.00 && currentYaw <= 45.00 {
      direction = meleeMoveDirection.Forward;
    } else {
      if currentYaw > 45.00 && currentYaw < 135.00 {
        direction = meleeMoveDirection.Right;
      } else {
        if currentYaw >= 135.00 && currentYaw <= 180.00 || currentYaw <= -135.00 && currentYaw >= -180.00 {
          direction = meleeMoveDirection.Back;
        } else {
          if currentYaw > -135.00 && currentYaw < -45.00 {
            direction = meleeMoveDirection.Left;
          };
        };
      };
    };
    return direction;
  }

  public final static func GetWeaponObject(const scriptInterface: ref<StateGameScriptInterface>) -> ref<WeaponObject> {
    let owner: ref<GameObject> = scriptInterface.owner;
    let weapon: ref<WeaponObject> = owner as WeaponObject;
    return weapon;
  }

  public final static func CanThrowWeaponObject(const owner: ref<GameObject>, const weapon: ref<WeaponObject>) -> Bool {
    if !RPGManager.HasStatFlag(weapon, gamedataStatType.HasMeleeTargeting) {
      return false;
    };
    if weapon.WeaponHasTag(n"Throwable") && !RPGManager.HasStatFlag(owner, gamedataStatType.CanThrowWeapon) {
      return false;
    };
    return true;
  }

  protected final const func GetAttackDataFromCurrentState(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, attackNumber: Int32, out outgoingStruct: ref<MeleeAttackData>) -> Bool {
    return this.GetAttackDataFromState(stateContext, scriptInterface, this.m_stateNameString, attackNumber, outgoingStruct);
  }

  protected final const func GetAttackDataFromState(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, stateName: String, attackNumber: Int32, out outgoingStruct: ref<MeleeAttackData>) -> Bool {
    let attackRecord: wref<Attack_Melee_Record>;
    let attackSpeed: Float;
    let attackSpeedMult: Float;
    let effectToPlay: CName;
    let ownerID: EntityID;
    let recordID: TweakDBID;
    let staminaCostMods: array<wref<StatModifier_Record>>;
    let statsSystem: ref<StatsSystem>;
    if !this.GetAttackRecord(scriptInterface, stateName, attackNumber, attackRecord) {
      return false;
    };
    recordID = attackRecord.GetID();
    if !TDBID.IsValid(recordID) {
      return false;
    };
    attackSpeedMult = 1.00;
    ownerID = scriptInterface.ownerEntityID;
    statsSystem = scriptInterface.GetStatsSystem();
    if attackRecord.DontScaleWithAttackSpeed() {
      attackSpeed = 1.00;
    } else {
      attackSpeed = statsSystem.GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.AttackSpeed);
      if scriptInterface.HasStatFlag(gamedataStatType.CanMeleeBerserk) {
        attackSpeedMult *= LerpF(Cast<Float>(stateContext.GetIntParameter(n"totalMeleeAttacksInCombo", true)) / Cast<Float>(this.GetStaticIntParameterDefault("maxBerserkASAttack", 1)), 1.00, this.GetStaticFloatParameterDefault("maxBerserkAS", 1.00), true);
      };
      if this.IsPlayerTired(scriptInterface) && !scriptInterface.HasStatFlag(gamedataStatType.CanIgnoreWeaponStaminaPenaties) {
        attackSpeedMult *= this.GetStaticFloatParameterDefault("lowStaminaAttackSpeedMult", 0.60);
      };
      attackSpeed *= attackSpeedMult;
    };
    attackRecord.StaminaCost(staminaCostMods);
    outgoingStruct = scriptInterface.GetMeleeAttackData(attackRecord, RPGManager.CalculateStatModifiers(staminaCostMods, scriptInterface.GetGame(), scriptInterface.owner, Cast<StatsObjectID>(ownerID)), attackSpeed);
    MeleeTransition.GetWeaponObject(scriptInterface).SetAttack(recordID);
    stateContext.SetPermanentFloatParameter(n"idleTransitionTime", outgoingStruct.idleTransitionTime, true);
    stateContext.SetPermanentFloatParameter(n"attackSpeed", attackSpeed, true);
    effectToPlay = attackRecord.VfxName();
    GameObjectEffectHelper.StartEffectEvent(scriptInterface.owner, effectToPlay, false);
    return true;
  }

  protected final const func HasAttackRecord(const scriptInterface: ref<StateGameScriptInterface>, const opt attackNumber: Int32) -> Bool {
    let attackRecord: wref<Attack_Melee_Record>;
    if this.GetAttackRecord(scriptInterface, this.m_stateNameString, attackNumber, attackRecord) {
      return true;
    };
    return false;
  }

  protected final const func GetAttackRecord(const scriptInterface: ref<StateGameScriptInterface>, const stateName: String, const attackNumber: Int32, out attackRecord: wref<Attack_Melee_Record>) -> Bool {
    attackRecord = MeleeTransition.GetWeaponObject(scriptInterface).GetAttack(StringToName(stateName + IntToString(attackNumber))).GetRecord() as Attack_Melee_Record;
    return attackRecord != null;
  }

  public final func SpawnMeleeWeaponProjectile(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let appearanceName: CName;
    let projectileTemplateName: CName;
    let useAppearance: Bool;
    let transactionSystem: ref<TransactionSystem> = scriptInterface.GetTransactionSystem();
    let itemObj: ref<ItemObject> = transactionSystem.GetItemInSlot(scriptInterface.executionOwner, t"AttachmentSlots.WeaponRight");
    let weaponObj: ref<WeaponObject> = MeleeTransition.GetWeaponObject(scriptInterface);
    let isThrowable: Bool = weaponObj.WeaponHasTag(n"Throwable");
    if isThrowable {
      useAppearance = weaponObj.GetBoolPropertyFromWeaponDefinition(t".useProjectileAppearance");
      if useAppearance {
        appearanceName = weaponObj.GetAppearanceNameFromComponent(n"Grip");
      };
    };
    projectileTemplateName = weaponObj.GetNamePropertyFromWeaponDefinition(t".projectileTemplateName");
    if IsDefined(itemObj) && IsNameValid(projectileTemplateName) {
      if this.m_driverCombatListener.IsMountedInTPP() {
        ProjectileLaunchHelper.SpawnProjectileFromRightHand(scriptInterface.executionOwner, projectileTemplateName, appearanceName, itemObj);
      } else {
        ProjectileLaunchHelper.SpawnProjectileFromScreenCenter(scriptInterface.executionOwner, projectileTemplateName, appearanceName, itemObj);
      };
    };
  }

  protected final const func GetMeleeWeaponFriendlyName(scriptInterface: ref<StateGameScriptInterface>) -> CName {
    return StringToName(TweakDBInterface.GetItemRecord(ItemID.GetTDBID(MeleeTransition.GetWeaponObject(scriptInterface).GetItemID())).FriendlyName());
  }

  public final static func IsPlayingSyncedAnimation(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.GetWorkspotSystem().IsActorInWorkspot(scriptInterface.executionOwner) && !scriptInterface.localBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.MountedToVehicle) {
      return true;
    };
    return false;
  }

  protected final func AdjustAttackPosition(scriptInterface: ref<StateGameScriptInterface>, stateContext: ref<StateContext>, attackData: ref<MeleeAttackData>) -> Bool {
    let adjustPosition: Vector4;
    let impulseVector: Vector4;
    if !attackData.useAdjustmentInsteadOfImpulse {
      return false;
    };
    impulseVector = this.AddCameraSpaceImpulse(scriptInterface, stateContext, attackData);
    impulseVector += this.AddForwardImpulse(scriptInterface, stateContext, attackData);
    impulseVector += this.AddUpImpulse(scriptInterface, stateContext, attackData);
    adjustPosition = scriptInterface.executionOwner.GetWorldPosition() + impulseVector;
    this.RequestPlayerPositionAdjustment(stateContext, scriptInterface, null, attackData.attackEffectDelay, this.GetStaticFloatParameterDefault("distanceRadiusToTarget", 0.90), this.GetStaticFloatParameterDefault("rotationDuration", -1.00), adjustPosition, false);
    return true;
  }

  protected final func AddAttackImpulse(scriptInterface: ref<StateGameScriptInterface>, stateContext: ref<StateContext>, attackData: ref<MeleeAttackData>) -> Void {
    let impulseEvent: ref<PSMImpulse>;
    let impulseVector: Vector4;
    let targetTooCloseRange: Float = this.GetStaticFloatParameterDefault("minDistToTarget", 5.00);
    if attackData.forwardImpulse > 0.00 || attackData.cameraSpaceImpulse > 0.00 {
      if IsDefined(DefaultTransition.GetTargetObject(scriptInterface, targetTooCloseRange)) {
        return;
      };
    };
    impulseVector = this.AddCameraSpaceImpulse(scriptInterface, stateContext, attackData);
    impulseVector += this.AddForwardImpulse(scriptInterface, stateContext, attackData);
    impulseVector += this.AddUpImpulse(scriptInterface, stateContext, attackData);
    impulseEvent = new PSMImpulse();
    impulseEvent.id = n"impulse";
    impulseEvent.impulse = impulseVector;
    scriptInterface.executionOwner.QueueEvent(impulseEvent);
  }

  protected final func AddCameraSpaceImpulse(scriptInterface: ref<StateGameScriptInterface>, stateContext: ref<StateContext>, attackData: ref<MeleeAttackData>) -> Vector4 {
    let cameraWorldTransform: Transform;
    let impulseValue: Float;
    let impulseVector: Vector4;
    if attackData.cameraSpaceImpulse == 0.00 {
      return Vector4.EmptyVector();
    };
    impulseValue = attackData.cameraSpaceImpulse;
    if !scriptInterface.IsOnGround() {
      impulseValue *= this.GetStaticFloatParameterDefault("inAirImpulseMultiplier", 1.00);
    };
    cameraWorldTransform = scriptInterface.GetCameraWorldTransform();
    impulseVector = Transform.GetForward(cameraWorldTransform);
    impulseVector = impulseVector * impulseValue;
    return impulseVector;
  }

  protected final func AddForwardImpulse(scriptInterface: ref<StateGameScriptInterface>, stateContext: ref<StateContext>, attackData: ref<MeleeAttackData>) -> Vector4 {
    let impulseValue: Float;
    let impulseVector: Vector4;
    if attackData.forwardImpulse == 0.00 {
      return Vector4.EmptyVector();
    };
    impulseValue = attackData.forwardImpulse;
    if !scriptInterface.IsOnGround() {
      impulseValue *= this.GetStaticFloatParameterDefault("inAirImpulseMultiplier", 1.00);
    };
    impulseVector = scriptInterface.executionOwner.GetWorldForward();
    impulseVector = impulseVector * impulseValue;
    return impulseVector;
  }

  protected final func AddUpImpulse(scriptInterface: ref<StateGameScriptInterface>, stateContext: ref<StateContext>, attackData: ref<MeleeAttackData>) -> Vector4 {
    let impulseValue: Float;
    let impulseVector: Vector4;
    if attackData.upImpulse == 0.00 {
      return Vector4.EmptyVector();
    };
    impulseValue = attackData.upImpulse;
    if !scriptInterface.IsOnGround() {
      impulseValue *= this.GetStaticFloatParameterDefault("inAirImpulseMultiplier", 1.00);
    };
    impulseVector = scriptInterface.executionOwner.GetWorldUp();
    impulseVector = impulseVector * impulseValue;
    return impulseVector;
  }

  protected final func GetMovementInput(scriptInterface: ref<StateGameScriptInterface>) -> Float {
    let x: Float = scriptInterface.GetActionValue(n"MoveX");
    let y: Float = scriptInterface.GetActionValue(n"MoveY");
    let res: Float = SqrtF(SqrF(x) + SqrF(y));
    return res;
  }

  protected final func IsPlayerInputDirectedForward(scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if AbsF(scriptInterface.GetInputHeading()) < 45.00 {
      return true;
    };
    return false;
  }

  protected final const func GetNanoWireTargetObject(const scriptInterface: ref<StateGameScriptInterface>) -> ref<GameObject> {
    let angleOut: EulerAngles;
    let targetingSystem: ref<TargetingSystem> = scriptInterface.GetTargetingSystem();
    let targetObject: ref<GameObject> = targetingSystem.GetObjectClosestToCrosshair(scriptInterface.executionOwner, angleOut, TSQ_NPC());
    let wireAttackRange: Float = scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.Range);
    wireAttackRange *= 2.00;
    if targetObject.IsPuppet() && ScriptedPuppet.IsActive(targetObject) && (Equals(GameObject.GetAttitudeTowards(targetObject, scriptInterface.executionOwner), EAIAttitude.AIA_Neutral) || Equals(GameObject.GetAttitudeTowards(targetObject, scriptInterface.executionOwner), EAIAttitude.AIA_Hostile)) {
      if wireAttackRange <= 0.00 || Vector4.Distance(scriptInterface.executionOwner.GetWorldPosition(), targetObject.GetWorldPosition()) <= wireAttackRange {
        return targetObject;
      };
    };
    return null;
  }

  protected final func IsTargetAPuppet(scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return DefaultTransition.GetTargetObject(scriptInterface).IsPuppet();
  }

  protected final func IsTargetOfficer(scriptInterface: ref<StateGameScriptInterface>, object: wref<GameObject>) -> Bool {
    let puppet: ref<NPCPuppet> = object as NPCPuppet;
    let isOfficer: Bool = Equals(puppet.GetNPCRarity(), gamedataNPCRarity.Officer);
    return isOfficer;
  }

  protected final const func IsAttackParried(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.GetStatusEffectSystem().HasStatusEffect(scriptInterface.executionOwnerEntityID, t"BaseStatusEffect.Parry");
  }

  protected final const func HasMeleeTargeting(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.HasWeaponStatFlag(scriptInterface, gamedataStatType.HasMeleeTargeting);
  }

  protected final const func CanWeaponBlock(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.HasWeaponStatFlag(scriptInterface, gamedataStatType.CanWeaponBlock);
  }

  protected final const func CanWeaponDeflect(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.HasWeaponStatFlag(scriptInterface, gamedataStatType.CanWeaponDeflect);
  }

  protected final const func CanThrowWeapon(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return scriptInterface.HasStatFlag(gamedataStatType.CanThrowWeapon);
  }

  protected final func ResetFlags(stateContext: ref<StateContext>) -> Void {
    this.SetIsBlocking(stateContext, false);
    this.SetIsTargeting(stateContext, false);
    this.SetIsAttacking(stateContext, false);
    this.SetIsHolding(stateContext, false);
    this.SetIsParried(stateContext, false);
    this.SetIsSafe(stateContext, false);
    this.SetIsThrowReloading(stateContext, false);
  }
}

public abstract class MeleeEventsTransition extends MeleeTransition {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SendAnimFeatureData(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SendAnimFeatureData(stateContext, scriptInterface);
    this.ToggleWireVisualEffect(stateContext, scriptInterface, n"monowire_idle", false);
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void;

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let animFeature: ref<AnimFeature_MeleeData> = new AnimFeature_MeleeData();
    scriptInterface.SetAnimationParameterFeature(n"MeleeData", animFeature);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee, 0);
    this.ResetFlags(stateContext);
    this.ToggleWireVisualEffect(stateContext, scriptInterface, n"monowire_idle", false);
    this.RemoveAllMeleeGameplayRestrictions(stateContext, scriptInterface);
    this.MeleeTransitionRemoveTriggerEffects(GameInstance.GetAudioSystem(scriptInterface.owner.GetGame()));
  }

  protected final func ToggleWireVisualEffect(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, effectName: CName, b: Bool) -> Void {
    if Equals(this.GetMeleeWeaponFriendlyName(scriptInterface), n"mono_wires") {
      if Equals(b, true) {
        GameObjectEffectHelper.StartEffectEvent(scriptInterface.owner, effectName);
      } else {
        GameObjectEffectHelper.StopEffectEvent(scriptInterface.owner, effectName);
      };
    };
  }

  protected final func MeleeTransitionRemoveTriggerEffects(audioSystem: ref<AudioSystem>) -> Void {
    audioSystem.RemoveTriggerEffect(n"PSM_MeleeTargetingOnEnter_trigger");
    audioSystem.RemoveTriggerEffect(n"PSM_MeleeReloadOnEnter_feedback");
    audioSystem.RemoveTriggerEffect(n"PSM_MeleeAttackGeneric");
  }

  protected final func TargetPrediction(targetPosition: Vector4, targetPuppet: ref<ScriptedPuppet>, deltaTime: Float, effectStrength: Float) -> Vector4 {
    let targetVelocity: Vector4 = targetPuppet.GetVelocity();
    let distanceLimit: Float = 2.00;
    let targetPos: Vector4 = targetPosition;
    let predictedPos: Vector4 = targetPos + targetVelocity * deltaTime * effectStrength;
    if Vector4.DistanceSquared(targetPos, predictedPos) >= distanceLimit * distanceLimit {
      predictedPos = targetPos + Vector4.Normalize(targetVelocity) * distanceLimit;
    };
    targetPos = predictedPos;
    return targetPos;
  }

  protected final func CheckThrowableCooldown(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if !MeleeTransition.GetWeaponObject(scriptInterface).WeaponHasTag(n"Throwable") {
      return;
    };
    if MeleeTransition.IsThrownWeaponReloading(stateContext, scriptInterface) {
      this.SetIsThrowReloading(stateContext, true);
    };
  }
}

public class MeleeNotReadyDecisions extends MeleeTransition {

  protected final const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return !this.IsWeaponReady(stateContext, scriptInterface);
  }

  protected final const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsWeaponReady(stateContext, scriptInterface);
  }
}

public class MeleeNotReadyEvents extends MeleeEventsTransition {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let weapon: wref<WeaponObject> = scriptInterface.owner as WeaponObject;
    this.ResetFlags(stateContext);
    this.ResetAttackNumber(stateContext);
    scriptInterface.PushAnimationEvent(n"MeleeNotReady");
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 0);
    stateContext.RemovePermanentFloatParameter(n"meleeRecoveryDuration");
    this.TutorialSetFact(scriptInterface, n"melee_combat_tutorial");
    this.CheckThrowableCooldown(stateContext, scriptInterface);
    GameInstance.GetAudioSystem(scriptInterface.owner.GetGame()).AddTriggerEffect(weapon.GetTriggerEffectName(), n"PSM_MeleeAttackGeneric");
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.PlayerExhausted") {
      GameInstance.GetAudioSystem(scriptInterface.owner.GetGame()).ReplaceTriggerEffect(n"te_wea_melee_swing_exhausted", n"PSM_MeleeAttackGeneric", false);
    };
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
  }
}

public class MeleeParriedDecisions extends MeleeTransition {

  protected final const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsAttackParried(stateContext, scriptInterface);
  }

  protected final const func ToMeleeIdle(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return !this.IsAttackParried(stateContext, scriptInterface);
  }

  protected final const func ToMeleeDeflect(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsBlockPressed(stateContext, scriptInterface);
  }
}

public class MeleeParriedEvents extends MeleeEventsTransition {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    MeleeTransition.ClearInputBuffer(stateContext);
    this.ResetFlags(stateContext);
    this.ResetAttackNumber(stateContext);
    scriptInterface.PushAnimationEvent(n"MeleeParried");
    this.SetIsParried(stateContext, true);
    DefaultTransition.PlayRumble(scriptInterface, "heavy_fast");
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 5);
    stateContext.RemovePermanentFloatParameter(n"meleeRecoveryDuration");
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetIsParried(stateContext, false);
    super.OnExit(stateContext, scriptInterface);
  }
}

public class MeleeRecoveryDecisions extends MeleeTransition {

  protected final const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let paramResult: StateResultFloat = stateContext.GetPermanentFloatParameter(n"meleeRecoveryDuration");
    if paramResult.valid {
      return true;
    };
    return false;
  }

  protected final const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let paramResult: StateResultFloat = stateContext.GetPermanentFloatParameter(n"meleeRecoveryDuration");
    return this.GetInStateTime() >= paramResult.value;
  }
}

public class MeleeRecoveryEvents extends MeleeNotReadyEvents {

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    stateContext.RemovePermanentFloatParameter(n"meleeRecoveryDuration");
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    stateContext.RemovePermanentFloatParameter(n"meleeRecoveryDuration");
  }
}

public class MeleeEquippingDecisions extends MeleeIdleDecisions {

  public let m_hasEquipAttack: Bool;

  protected func OnAttach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let attackRecord: wref<Attack_Melee_Record>;
    this.m_hasEquipAttack = this.GetAttackRecord(scriptInterface, "MeleeEquipAttack", 0, attackRecord);
  }

  protected final const func ToMeleeIdle(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsRightHandInEquippedState(stateContext);
  }

  protected const func ToMeleeHold(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.m_hasEquipAttack {
      return false;
    };
    return super.ToMeleeHold(stateContext, scriptInterface);
  }

  protected final const func ToMeleeEquipAttack(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.m_hasEquipAttack {
      return false;
    };
    return scriptInterface.IsActionJustHeld(n"MeleeAttack");
  }
}

public class MeleeEquippingEvents extends MeleeRumblingEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.ResetFlags(stateContext);
    this.ResetAttackNumber(stateContext);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee, 0);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 1);
    stateContext.SetPermanentBoolParameter(n"isSafe", false, true);
    GameInstance.GetAudioSystem(scriptInterface.owner.GetGame()).RemoveTriggerEffect(n"PSM_MeleeTargetingOnEnter_trigger");
    this.CheckThrowableCooldown(stateContext, scriptInterface);
    super.OnEnter(stateContext, scriptInterface);
  }

  protected final func CleanupFirstEquipFX(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let weapon: ref<ItemObject>;
    if !this.CheckItemType(scriptInterface, gamedataItemType.Wea_Chainsword) {
      return;
    };
    if !this.IsInFirstEquip(stateContext) {
      return;
    };
    weapon = scriptInterface.owner as ItemObject;
    if IsDefined(weapon) {
      GameInstance.GetAudioSystem(weapon.GetGame()).Stop(n"w_melee_cut_o_matic_first_equip", weapon.GetEntityID(), n"firearm_emitter");
    };
  }

  protected func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.CleanupFirstEquipFX(stateContext, scriptInterface);
    super.OnExit(stateContext, scriptInterface);
  }

  protected func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.CleanupFirstEquipFX(stateContext, scriptInterface);
    super.OnForcedExit(stateContext, scriptInterface);
  }
}

public class MeleeIdleDecisions extends MeleeTransition {

  protected final const func ToMeleePublicSafe(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Combat) == 1 {
      return false;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Zones) == 4 {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"Fists") {
      return false;
    };
    if stateContext.IsStateMachineActive(n"Vehicle") {
      return false;
    };
    if stateContext.GetBoolParameter(n"InPublicZone", true) {
      if this.GetInStateTime() > this.GetStaticFloatParameterDefault("safeTransition", 1.00) {
        return true;
      };
      return false;
    };
    return false;
  }

  protected const func ToMeleeHold(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.ShouldHold(stateContext, scriptInterface, true) {
      return true;
    };
    return false;
  }
}

public class MeleeIdleEvents extends MeleeRumblingEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.ResetFlags(stateContext);
    this.ResetAttackNumber(stateContext);
    this.ClearMeleePressCount(stateContext, scriptInterface);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee, 0);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 2);
    stateContext.SetPermanentBoolParameter(n"isSafe", false, true);
    this.SetFlags(stateContext);
    this.ToggleWireVisualEffect(stateContext, scriptInterface, n"monowire_idle", true);
    GameInstance.GetAudioSystem(scriptInterface.owner.GetGame()).RemoveTriggerEffect(n"PSM_MeleeTargetingOnEnter_trigger");
    super.OnEnter(stateContext, scriptInterface);
  }

  protected func SetFlags(stateContext: ref<StateContext>) -> Void;
}

public abstract class MeleeRumblingEvents extends MeleeEventsTransition {

  public func GetIntensity() -> String {
    return "light";
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let weaponItem: ItemID = EquipmentSystem.GetData(GameInstance.GetPlayerSystem(scriptInterface.owner.GetGame()).GetLocalPlayerMainGameObject()).GetActiveItem(gamedataEquipmentArea.Weapon);
    let itemRecord: wref<Item_Record> = RPGManager.GetItemRecord(weaponItem);
    let itemTags: array<CName> = itemRecord.Tags();
    let hasRumblingWeaponTag: Bool = ArrayContains(itemTags, n"MeleeRumblingWeapon");
    if hasRumblingWeaponTag {
      DefaultTransition.PlayRumbleLoop(scriptInterface, this.GetIntensity());
    };
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    DefaultTransition.StopRumbleLoop(scriptInterface, this.GetIntensity());
    super.OnExit(stateContext, scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    DefaultTransition.StopRumbleLoop(scriptInterface, this.GetIntensity());
    this.OnExit(stateContext, scriptInterface);
  }
}

public class MeleePublicSafeDecisions extends MeleeTransition {

  protected final const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Combat) == 1 {
      return false;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Zones) == 4 {
      return false;
    };
    if stateContext.IsStateActive(n"Locomotion", n"sprint") {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"Fists") {
      return false;
    };
    return false;
  }

  protected final const func ToMeleeIdle(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if stateContext.IsStateActive(n"Locomotion", n"sprint") {
      return true;
    };
    if scriptInterface.IsActionJustPressed(n"Reload") {
      return true;
    };
    if StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"Fists") {
      return true;
    };
    return false;
  }
}

public class MeleePublicSafeEvents extends MeleeRumblingEvents {

  public let m_unequipTime: Float;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.ResetFlags(stateContext);
    this.ResetAttackNumber(stateContext);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee, 0);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 4);
    this.SetIsSafe(stateContext, true);
    if this.GetWeaponItemTag(stateContext, scriptInterface, n"Meleeware") {
      this.m_unequipTime = this.GetStaticFloatParameterDefault("timeToUnequipMeleeware", 15.00);
    } else {
      if stateContext.GetBoolParameter(n"InPublicZone", true) {
        this.m_unequipTime = this.GetStaticFloatParameterDefault("timeToAutoUnequipWeapon", 15.00);
      } else {
        this.m_unequipTime = -1.00;
      };
    };
    super.OnEnter(stateContext, scriptInterface);
  }

  protected final func OnTick(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if this.m_unequipTime > 0.00 && this.GetInStateTime() >= this.m_unequipTime {
      this.SendEquipmentSystemWeaponManipulationRequest(scriptInterface, EquipmentManipulationAction.UnequipWeapon);
    };
  }
}

public class MeleeSafeDecisions extends MeleeTransition {

  protected final const func IsFriendlyTargetInMeleeRange(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>, const attackRangeSquared: Float) -> Bool {
    let isTargeting: StateResultBool;
    let player: ref<PlayerPuppet> = scriptInterface.executionOwner as PlayerPuppet;
    if !IsDefined(player) {
      return false;
    };
    if !player.IsAimingAtFriendly() {
      return false;
    };
    isTargeting = stateContext.GetPermanentBoolParameter(n"isTargeting");
    if !isTargeting.value && player.DistanceFromTargetSquared() > attackRangeSquared {
      return false;
    };
    return true;
  }

  protected final const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let isTargeting: StateResultBool = stateContext.GetPermanentBoolParameter(n"isTargeting");
    let attackRangeSquared: Float = scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.Range);
    attackRangeSquared = attackRangeSquared * attackRangeSquared;
    if this.IsFriendlyTargetInMeleeRange(stateContext, scriptInterface, attackRangeSquared) {
      return true;
    };
    if this.ShouldEnterSafe(stateContext, scriptInterface, isTargeting.value ? 0.00 : attackRangeSquared) {
      return true;
    };
    return false;
  }

  protected final const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.EnterCondition(stateContext, scriptInterface) {
      return true;
    };
    return false;
  }
}

public class MeleeSafeEvents extends MeleePublicSafeEvents {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 3);
  }
}

public class MeleeHoldDecisions extends MeleeTransition {

  protected final const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.ShouldHold(stateContext, scriptInterface) && !MeleeTransition.IsPlayingSyncedAnimation(scriptInterface) {
      return true;
    };
    return false;
  }

  protected final const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.ShouldInterruptHoldStates(stateContext, scriptInterface) {
      return true;
    };
    if this.ToMeleeChargedHold(stateContext, scriptInterface) && !MeleeTransition.IsPlayingSyncedAnimation(scriptInterface) {
      return true;
    };
    if scriptInterface.GetActionValue(n"MeleeStrongAttack") <= 0.00 && scriptInterface.GetActionValue(n"MeleeAttack") <= 0.00 {
      return true;
    };
    if this.IsBlockHeld(stateContext, scriptInterface) {
      if !MeleeTransition.GetWeaponObject(scriptInterface).IsThrowable() || this.CanThrowWeapon(stateContext, scriptInterface) {
        return true;
      };
    };
    return false;
  }

  protected final const func ToMeleeFinalAttack(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if GameInstance.GetPhotoModeSystem(scriptInterface.executionOwner.GetGame()).IsPhotoModeActive() {
      return false;
    };
    return this.CheckIfFinalAttack(scriptInterface, stateContext) && MeleeTransition.WantsToLightAttack(stateContext, scriptInterface);
  }

  protected final const func ToMeleeMountedFinalAttack(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.m_driverCombatListener.IsMountedInTPP() {
      return false;
    };
    return this.ToMeleeFinalAttack(stateContext, scriptInterface);
  }
}

public class MeleeHoldEvents extends MeleeEventsTransition {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetIsHolding(stateContext, true);
    this.SetIsBlocking(stateContext, false);
    this.SetIsAttacking(stateContext, false);
    this.SetIsTargeting(stateContext, false);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 6);
    stateContext.SetTemporaryBoolParameter(n"InterruptHoldForLeap", false, true);
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetIsHolding(stateContext, false);
    super.OnExit(stateContext, scriptInterface);
  }
}

public class MeleeChargedHoldDecisions extends MeleeTransition {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.ShouldHold(stateContext, scriptInterface) {
      return true;
    };
    return false;
  }

  protected final const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.ShouldInterruptHoldStates(stateContext, scriptInterface) {
      return true;
    };
    if !this.ShouldHold(stateContext, scriptInterface) {
      return true;
    };
    if this.ToMeleeStrongAttack(stateContext, scriptInterface) && !MeleeTransition.IsPlayingSyncedAnimation(scriptInterface) {
      return true;
    };
    if this.IsBlockHeld(stateContext, scriptInterface) {
      return true;
    };
    return false;
  }

  protected final const func ToMeleeStrongAttack(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let timeoutDuration: Float;
    if GameInstance.GetPhotoModeSystem(scriptInterface.executionOwner.GetGame()).IsPhotoModeActive() {
      return false;
    };
    if this.NoStrongAttackPressed(scriptInterface) {
      return true;
    };
    timeoutDuration = scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.HoldTimeoutDuration);
    if timeoutDuration > 0.00 && this.GetInStateTime() >= timeoutDuration {
      return true;
    };
    return false;
  }

  protected final const func ToMeleeMountedStrongAttack(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.m_driverCombatListener.IsMountedInTPP() {
      return false;
    };
    return this.ToMeleeStrongAttack(stateContext, scriptInterface);
  }
}

public class MeleeChargedHoldEvents extends MeleeRumblingEvents {

  public let m_clearWeaponCharge: Bool;

  public let m_effectiveRangeMod: ref<gameStatModifierData>;

  public func GetIntensity() -> String {
    return "heavy";
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let chargeValuePerSec: Float;
    let executionOwner: wref<GameObject> = scriptInterface.executionOwner;
    this.m_clearWeaponCharge = true;
    DefaultTransition.PlayRumble(scriptInterface, "light_fast");
    this.SetIsHolding(stateContext, true);
    this.SetIsBlocking(stateContext, false);
    this.SetIsAttacking(stateContext, false);
    this.SetIsTargeting(stateContext, false);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 7);
    stateContext.SetPermanentBoolParameter(n"VisionToggled", false, true);
    this.ForceDisableVisionMode(stateContext);
    if this.CheckItemType(scriptInterface, gamedataItemType.Cyb_MantisBlades) {
      this.SetCanSprintWhileCharging(stateContext, true);
    } else {
      stateContext.SetTemporaryBoolParameter(n"InterruptSprint", true, true);
      this.SetCanSprintWhileCharging(stateContext, false);
    };
    if scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID), gamedataStatType.CanMeleeLeap) > 0.00 || scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID), gamedataStatType.CanMeleeLeapInAir) > 0.00 {
      this.UpdateEffectiveRange(scriptInterface, TweakDBInterface.GetFloat(t"playerStateMachineMelee.meleeLeap.maxDistToTarget", 0.00));
    } else {
      if Equals(MeleeTransition.GetWeaponType(scriptInterface), gamedataItemType.Cyb_StrongArms) && MeleeTransition.HasNewSpyAttackStatFlag(scriptInterface) && MeleeTransition.WeaponIsCharged(scriptInterface) {
        this.UpdateEffectiveRange(scriptInterface, TweakDBInterface.GetFloat(t"playerStateMachineLocomotion.dodge.dashOnePunchAttackEnemyMaxRange", 0.00));
      } else {
        if this.CanPerformRelicLeap(scriptInterface) {
          this.UpdateEffectiveRange(scriptInterface, TweakDBInterface.GetFloat(t"playerStateMachineMelee.meleeLeap.maxDistToTargetMantisBladesRelic", 0.00));
        };
      };
    };
    chargeValuePerSec = this.GetChargeValuePerSec(scriptInterface);
    if chargeValuePerSec > 0.00 {
      if !this.CheckItemType(scriptInterface, gamedataItemType.Cyb_NanoWires) || this.IsMonowireQuickhackChargedAttack(scriptInterface) {
        this.StartPool(scriptInterface.GetStatPoolsSystem(), MeleeTransition.GetWeaponObject(scriptInterface).GetEntityID(), gamedataStatPoolType.WeaponCharge, 0.00, 100.00, chargeValuePerSec);
      };
    };
    GameInstance.GetAudioSystem(scriptInterface.owner.GetGame()).AddTriggerEffect(n"te_wea_melee_charge", n"PSM_MeleeChargeOnEnter_charge");
    GameInstance.GetStatusEffectSystem(scriptInterface.owner.GetGame()).ApplyStatusEffect(executionOwner.GetEntityID(), this.GetPlayerAimingStatusEffectID());
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitCommon(stateContext, scriptInterface);
    super.OnForcedExit(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitCommon(stateContext, scriptInterface);
    super.OnExit(stateContext, scriptInterface);
  }

  protected final func OnExitToMeleeStrongAttack(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if !this.CheckItemType(scriptInterface, gamedataItemType.Cyb_NanoWires) {
      this.m_clearWeaponCharge = false;
    };
    this.OnExit(stateContext, scriptInterface);
  }

  private final func OnExitCommon(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let executionOwner: wref<GameObject> = scriptInterface.executionOwner;
    this.SetIsHolding(stateContext, false);
    this.StopPool(scriptInterface.GetStatPoolsSystem(), MeleeTransition.GetWeaponObject(scriptInterface).GetEntityID(), gamedataStatPoolType.WeaponCharge, false);
    if this.m_clearWeaponCharge && !this.CheckItemType(scriptInterface, gamedataItemType.Cyb_MantisBlades) && !this.IsMonowireQuickhackChargedAttack(scriptInterface) {
      this.ChangeStatPoolValue(scriptInterface, scriptInterface.ownerEntityID, gamedataStatPoolType.WeaponCharge, -100.00, true);
    };
    GameInstance.GetAudioSystem(scriptInterface.owner.GetGame()).RemoveTriggerEffect(n"PSM_MeleeChargeOnEnter_charge");
    GameInstance.GetStatusEffectSystem(scriptInterface.owner.GetGame()).RemoveStatusEffect(executionOwner.GetEntityID(), this.GetPlayerAimingStatusEffectID());
    if IsDefined(this.m_effectiveRangeMod) {
      scriptInterface.GetStatsSystem().RemoveModifier(Cast<StatsObjectID>(MeleeTransition.GetWeaponObject(scriptInterface).GetEntityID()), this.m_effectiveRangeMod);
      this.m_effectiveRangeMod = null;
    };
  }

  private final func IsMonowireQuickhackChargedAttack(scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.CheckItemType(scriptInterface, gamedataItemType.Cyb_NanoWires) && MeleeTransition.HasNewSpyAttackStatFlag(scriptInterface) && MeleeTransition.HasMonowireWithQuickhackSelected(scriptInterface);
  }

  private final func GetChargeValuePerSec(scriptInterface: ref<StateGameScriptInterface>) -> Float {
    let chargeDuration: Float;
    let weapon: ref<WeaponObject>;
    let statsSystem: ref<StatsSystem> = scriptInterface.GetStatsSystem();
    if !IsDefined(statsSystem) {
      return -1.00;
    };
    weapon = MeleeTransition.GetWeaponObject(scriptInterface);
    if !IsDefined(weapon) {
      return -1.00;
    };
    chargeDuration = statsSystem.GetStatValue(Cast<StatsObjectID>(weapon.GetEntityID()), gamedataStatType.ChargeTime);
    if chargeDuration <= 0.00 {
      return -1.00;
    };
    return 100.00 / chargeDuration;
  }

  private final func UpdateEffectiveRange(scriptInterface: ref<StateGameScriptInterface>, effectiveRange: Float) -> Void {
    effectiveRange -= scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(MeleeTransition.GetWeaponObject(scriptInterface).GetEntityID()), gamedataStatType.EffectiveRange);
    this.m_effectiveRangeMod = RPGManager.CreateStatModifier(gamedataStatType.EffectiveRange, gameStatModifierType.Additive, effectiveRange);
    scriptInterface.GetStatsSystem().AddModifier(Cast<StatsObjectID>(MeleeTransition.GetWeaponObject(scriptInterface).GetEntityID()), this.m_effectiveRangeMod);
  }
}

public abstract class MeleeAttackGenericDecisions extends MeleeTransition {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let uninterruptableHealingEvent: StateResultBool;
    if !this.HasAttackRecord(scriptInterface, stateContext.GetIntParameter(n"meleeAttackNumber", true)) {
      return false;
    };
    if GameObject.IsCooldownActive(scriptInterface.owner, n"MeleeAttackCooldown") {
      return false;
    };
    uninterruptableHealingEvent = stateContext.GetPermanentBoolParameter(n"UninterruptableHealing");
    if uninterruptableHealingEvent.valid && uninterruptableHealingEvent.value {
      return false;
    };
    return true;
  }

  protected const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let interruptEvent: StateResultBool = stateContext.GetPermanentBoolParameter(n"InterruptMelee");
    let attackData: ref<MeleeAttackData> = this.GetAttackData(stateContext);
    let inStateTime: Float = this.GetInStateTime();
    if interruptEvent.value {
      if inStateTime >= attackData.attackWindowClosed {
        return true;
      };
      return false;
    };
    if this.IsBlockHeld(stateContext, scriptInterface) {
      if attackData.blockTransitionTime > 0.00 && inStateTime >= attackData.blockTransitionTime {
        return true;
      };
      if inStateTime >= attackData.attackWindowClosed {
        if !this.HasMeleeTargeting(stateContext, scriptInterface) && this.IsBlockPressed(stateContext, scriptInterface) {
          return true;
        };
      };
    };
    if inStateTime >= attackData.attackWindowClosed && !MeleeTransition.IsPlayingSyncedAnimation(scriptInterface) {
      if stateContext.GetConditionBool(n"LightMeleeAttackPressed") {
        return true;
      };
      if stateContext.GetConditionBool(n"StrongMeleeAttackPressed") {
        return true;
      };
      if this.ShouldHold(stateContext, scriptInterface, false, true) {
        return true;
      };
      if stateContext.GetConditionBool(n"QuickMeleeAttackTapped") {
        return true;
      };
    };
    if inStateTime >= attackData.idleTransitionTime {
      return true;
    };
    if this.IsAttackParried(stateContext, scriptInterface) {
      return true;
    };
    return false;
  }

  protected final const func GetAttackData(const stateContext: ref<StateContext>) -> ref<MeleeAttackData> {
    return stateContext.GetConditionScriptableParameter(n"MeleeAttackData") as MeleeAttackData;
  }
}

public abstract class MeleeAttackGenericEvents extends MeleeEventsTransition {

  public let m_effect: ref<EffectInstance>;

  public let m_attackCreated: Bool;

  public let m_blockImpulseCreation: Bool;

  public let m_standUpSend: Bool;

  public let m_trailCreated: Bool;

  public let m_finisherTarget: wref<ScriptedPuppet>;

  public let m_finisherCameraRotReseted: Bool;

  public let m_textLayer: Uint32;

  public let m_rumblePlayed: Bool;

  public let m_shouldBlockImpulseUpdate: Bool;

  public let m_enteredFromMeleeLeap: Bool;

  public let m_effectPositionUpdated: Bool;

  public let m_tppYawOverride: Float;

  protected func GetAttackType() -> EMeleeAttackType {
    return EMeleeAttackType.Combo;
  }

  protected const func IsMountedTPPAttack() -> Bool {
    return false;
  }

  protected func SetTPPYawOverride(stateContext: ref<StateContext>) -> Void {
    let yawOverride: StateResultFloat = stateContext.GetPermanentFloatParameter(n"TPPVehiclePlayerYaw");
    if yawOverride.valid {
      this.m_tppYawOverride = yawOverride.value;
    } else {
      this.m_tppYawOverride = 0.00;
    };
  }

  protected func IsMoveToTargetEnabled(attackData: ref<MeleeAttackData>, assistRecord: ref<AimAssistMelee_Record>) -> Bool {
    let assistLevel: EMoveAssistLevel;
    let attackType: EMeleeAttackType;
    if !attackData.enableAdjustingPlayerPositionToTarget {
      return false;
    };
    if IsDefined(assistRecord) {
      assistLevel = IntEnum<EMoveAssistLevel>(assistRecord.MoveToTargetEnabledAttacks());
      if Equals(assistLevel, EMoveAssistLevel.AllAttacks) {
        return true;
      };
      if Equals(assistLevel, EMoveAssistLevel.Off) {
        return false;
      };
      if Equals(assistLevel, EMoveAssistLevel.SpecialAttacks) {
        attackType = this.GetAttackType();
        return Equals(attackType, EMeleeAttackType.Strong);
      };
    };
    return false;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let adjustRadius: Float;
    let adjustRadiusParam: Float;
    let adjustmentDistParam: Float;
    let adjustmentTargetRadius: Float;
    let attackData: ref<MeleeAttackData>;
    let attackRange: Float;
    let broadcaster: ref<StimBroadcasterComponent>;
    let canMeleeLeap: Bool;
    let canPerformLeap: Bool;
    let distanceToTarget: Float;
    let finisherTargetID: EntityID;
    let healingItem: ref<gameItemData>;
    let isBloodPumpEquipped: Bool;
    let isMeleeThrow: Bool;
    let leapAngle: EulerAngles;
    let leapTargetWithinAngle: Float;
    let leapingWasSuccessful: Bool;
    let playerPerkDataBB: ref<IBlackboard>;
    let shouldCheckForWeakspots: Bool;
    let slotTransformPos: Vector4;
    let targetObject: ref<GameObject>;
    let ts: ref<TransactionSystem>;
    let uninterruptableHealingEvent: StateResultBool;
    let vecToTarget: Vector4;
    let aimAssistRecord: ref<AimAssistMelee_Record> = MeleeTransition.GetAimAssistMeleeRecord(scriptInterface);
    if uninterruptableHealingEvent.valid && uninterruptableHealingEvent.value {
      return;
    };
    this.m_attackCreated = false;
    this.m_blockImpulseCreation = false;
    this.m_standUpSend = false;
    this.m_effect = null;
    this.m_trailCreated = false;
    this.m_rumblePlayed = false;
    this.m_shouldBlockImpulseUpdate = false;
    isMeleeThrow = Equals(this.GetAttackType(), EMeleeAttackType.Throw);
    if IsDefined(broadcaster = scriptInterface.executionOwner.GetStimBroadcasterComponent()) {
      broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.MeleeAttack, isMeleeThrow);
    };
    this.SetMeleeAttackPressCount(stateContext, scriptInterface);
    MeleeTransition.ClearInputBuffer(stateContext);
    stateContext.SetPermanentBoolParameter(n"InterruptMelee", false, true);
    this.SetIsAttacking(stateContext, true);
    this.SetIsBlocking(stateContext, false);
    if !isMeleeThrow && IsDefined(aimAssistRecord) && aimAssistRecord.AimSnapOnAttack() {
      scriptInterface.GetTargetingSystem().AimSnap(scriptInterface.executionOwner);
    };
    GameObject.PlayVoiceOver(scriptInterface.executionOwner, n"meleeAttack", n"Scripts:MeleeAttackGenericEvents");
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee, 1);
    this.GetAttackDataFromCurrentState(stateContext, scriptInterface, stateContext.GetIntParameter(n"meleeAttackNumber", true), attackData);
    stateContext.SetPermanentBoolParameter(n"hasDeflectAnim", attackData.hasDeflectAnim, true);
    stateContext.SetPermanentBoolParameter(n"hasHitAnim", attackData.hasHitAnim, true);
    stateContext.SetConditionScriptableParameter(n"MeleeAttackData", attackData, true);
    stateContext.SetPermanentBoolParameter(n"VisionToggled", false, true);
    this.ForceDisableVisionMode(stateContext);
    stateContext.SetTemporaryBoolParameter(n"InterruptSprint", true, true);
    canPerformLeap = !this.m_enteredFromMeleeLeap;
    if canPerformLeap {
      uninterruptableHealingEvent = stateContext.GetPermanentBoolParameter(n"UninterruptableHealing");
      ts = scriptInterface.GetTransactionSystem();
      healingItem = ts.GetItemData(scriptInterface.owner, EquipmentSystem.GetData(scriptInterface.owner).GetActiveConsumable());
      isBloodPumpEquipped = Equals(n"BloodPump", TweakDBInterface.GetCName(ItemID.GetTDBID(healingItem.GetID()) + t".cyberwareType", n"None"));
      if !isBloodPumpEquipped && uninterruptableHealingEvent.valid && uninterruptableHealingEvent.value {
        canPerformLeap = false;
      };
    };
    this.m_finisherTarget = null;
    this.m_finisherCameraRotReseted = false;
    finisherTargetID = scriptInterface.localBlackboard.GetEntityID(GetAllBlackboardDefs().PlayerStateMachine.FinisherTarget);
    if EntityID.IsDefined(finisherTargetID) {
      this.m_finisherTarget = GameInstance.FindEntityByID(scriptInterface.executionOwner.GetGame(), finisherTargetID) as ScriptedPuppet;
    };
    if canPerformLeap && this.IsMoveToTargetEnabled(attackData, aimAssistRecord) {
      attackRange = scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.Range);
      adjustmentDistParam = aimAssistRecord.MoveToTargetSearchDistance();
      adjustmentTargetRadius = attackRange + adjustmentDistParam;
      shouldCheckForWeakspots = WeaponObject.IsBlade(MeleeTransition.GetWeaponObject(scriptInterface).GetItemID()) && PlayerDevelopmentSystem.GetData(scriptInterface.executionOwner).IsNewPerkBoughtAnyLevel(gamedataNewPerkType.Reflexes_Inbetween_Right_2);
      adjustRadiusParam = aimAssistRecord.MoveToTargetDistanceIntoAttackRange();
      adjustRadius = MaxF(attackRange - adjustRadiusParam, 1.50);
      leapTargetWithinAngle = this.GetStaticFloatParameterDefault("leapTargetWithinAngle", 10.00);
      targetObject = DefaultTransition.GetTargetObject(scriptInterface, adjustmentTargetRadius, shouldCheckForWeakspots, leapTargetWithinAngle);
      if IsDefined(targetObject) {
        vecToTarget = targetObject.GetWorldPosition() - scriptInterface.executionOwner.GetWorldPosition();
        leapAngle = Vector4.ToRotation(vecToTarget);
        this.GetSlotTransformToTarget(scriptInterface, targetObject, attackData.attackEffectDelay, false, slotTransformPos);
        if -leapAngle.Pitch <= this.GetStaticFloatParameterDefault("leapMaxPitch", 45.00) {
          leapingWasSuccessful = this.AdjustPlayerPosition(stateContext, scriptInterface, targetObject, attackData.attackEffectDelay, adjustRadius, -1.00, n"None", false, slotTransformPos);
        };
        if leapingWasSuccessful {
          this.m_blockImpulseCreation = true;
          playerPerkDataBB = GameInstance.GetBlackboardSystem(scriptInterface.executionOwner.GetGame()).Get(GetAllBlackboardDefs().PlayerPerkData);
          if IsDefined(playerPerkDataBB) {
            distanceToTarget = Vector4.Distance(targetObject.GetWorldPosition(), scriptInterface.executionOwner.GetWorldPosition());
            playerPerkDataBB.SetFloat(GetAllBlackboardDefs().PlayerPerkData.LeapedDistance, distanceToTarget, true);
            canMeleeLeap = scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.CanMeleeLeap) > 0.00;
            if !canMeleeLeap && shouldCheckForWeakspots {
              PlayerStaminaHelpers.ModifyStaminaBasedOnLeapAttackDistance(scriptInterface.executionOwner as PlayerPuppet, scriptInterface.IsOnGround(), distanceToTarget, adjustmentTargetRadius);
            };
          };
        };
      };
    };
    this.SendAnimationSlotData(stateContext, scriptInterface, attackData);
    scriptInterface.PushAnimationEvent(n"Attack");
    stateContext.SetPermanentIntParameter(n"attackType", EnumInt(this.GetAttackType()), true);
    super.OnEnter(stateContext, scriptInterface);
    if attackData.standUpDelay == 0.00 {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
      this.m_standUpSend = true;
    };
    if attackData.incrementsCombo {
      this.IncrementAttackNumber(scriptInterface, stateContext);
    };
    this.SetIsSafe(stateContext, false);
    this.SendDataTrackingRequest(scriptInterface, ETelemetryData.MeleeAttacksMade, 1);
    GameInstance.GetTelemetrySystem(scriptInterface.owner.GetGame()).LogWeaponAttackPerformed(MeleeTransition.GetWeaponObject(scriptInterface));
  }

  protected final func SendAnimationSlotData(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, const attackData: ref<MeleeAttackData>) -> Bool {
    let slotData: ref<AnimFeature_MeleeSlotData> = new AnimFeature_MeleeSlotData();
    slotData.attackType = EnumInt(this.GetAttackType());
    slotData.comboNumber = stateContext.GetIntParameter(n"meleeAttackNumber", true);
    slotData.startupDuration = attackData.startupDuration;
    slotData.activeDuration = attackData.activeDuration;
    slotData.recoverDuration = attackData.recoverDuration;
    slotData.activeHitDuration = attackData.activeHitDuration;
    slotData.recoverHitDuration = attackData.recoverHitDuration;
    scriptInterface.SetAnimationParameterFeature(n"MeleeSlotData", slotData);
    return true;
  }

  protected final func ConsumeWeaponCharge(scriptInterface: ref<StateGameScriptInterface>, attackData: ref<MeleeAttackData>) -> Void {
    if !scriptInterface.GetStatPoolsSystem().IsStatPoolAdded(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatPoolType.WeaponCharge) {
      return;
    };
    if attackData.weaponChargeCost > 0.00 {
      this.ChangeStatPoolValue(scriptInterface, scriptInterface.ownerEntityID, gamedataStatPoolType.WeaponCharge, -attackData.weaponChargeCost, true);
    };
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let attackData: ref<MeleeAttackData>;
    let duration: Float;
    let interruptEvent: StateResultBool;
    let weapon: ref<WeaponObject> = MeleeTransition.GetWeaponObject(scriptInterface);
    let uninterruptableHealingEvent: StateResultBool = stateContext.GetPermanentBoolParameter(n"UninterruptableHealing");
    if uninterruptableHealingEvent.valid && uninterruptableHealingEvent.value {
      return;
    };
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    interruptEvent = stateContext.GetPermanentBoolParameter(n"InterruptMelee");
    attackData = this.GetAttackData(stateContext);
    duration = this.GetInStateTime();
    if !this.m_standUpSend && attackData.standUpDelay > 0.00 && duration > attackData.standUpDelay {
      stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
      this.m_standUpSend = true;
    };
    this.UpdateIKData(scriptInterface, attackData);
    if duration >= attackData.attackEffectDelay && !this.m_attackCreated {
      if (!interruptEvent.valid || !interruptEvent.value) && (!uninterruptableHealingEvent.valid || !uninterruptableHealingEvent.value) {
        stateContext.SetPermanentBoolParameter(n"MeleeAttackDone", true, true);
        if IsDefined(this.m_finisherTarget) {
          if WeaponObject.IsOfType(weapon.GetItemID(), gamedataItemType.Wea_Knife) {
            this.m_attackCreated = this.CreateMeleeAttackForFinisher(stateContext, scriptInterface, attackData);
          };
          this.RotateCameraToFinisherTarget(scriptInterface, attackData.attackEffectDuration);
        };
        if !this.m_attackCreated {
          this.CreateMeleeAttack(stateContext, scriptInterface, attackData);
        };
        this.m_attackCreated = true;
        this.ConsumeStamina(scriptInterface, attackData);
      };
    };
    if IsDefined(this.m_effect) {
      this.UpdateEffectPosition(stateContext, scriptInterface, attackData, duration);
      if duration >= attackData.attackEffectDelay + attackData.attackEffectDuration + 0.10 {
        this.m_effect = null;
        this.ConsumeWeaponCharge(scriptInterface, attackData);
      };
      if weapon.WeaponHasTag(n"StrongArms") || weapon.WeaponHasTag(n"MantisBlades") {
        this.ConsumeWeaponCharge(scriptInterface, attackData);
      };
    };
    if IsDefined(this.m_finisherTarget) && !this.m_finisherCameraRotReseted && duration >= attackData.attackEffectDelay + attackData.attackEffectDuration {
      this.m_finisherCameraRotReseted = true;
      this.ResetCameraRotation(scriptInterface);
    };
    if this.m_trailCreated && duration >= attackData.trailStopDelay {
      MeleeTransition.GetWeaponObject(scriptInterface).StopCurrentMeleeTrailEffect(attackData.trailAttackSide);
    } else {
      if duration >= attackData.trailStartDelay && !this.m_trailCreated {
        MeleeTransition.GetWeaponObject(scriptInterface).StartCurrentMeleeTrailEffect(attackData.trailAttackSide);
        this.m_trailCreated = true;
      };
    };
    if this.ShouldBlockMovementImpulseUpdate(timeDelta, attackData, stateContext, scriptInterface) {
      this.m_shouldBlockImpulseUpdate = true;
    };
    if this.UpdateMovementImpulse(timeDelta, attackData, stateContext, scriptInterface) {
      this.m_blockImpulseCreation = true;
    };
    if duration >= attackData.attackEffectDelay {
      if !this.m_rumblePlayed {
        DefaultTransition.PlayRumble(scriptInterface, this.GetStaticStringParameterDefault("rumbleStrength", "light_fast"));
        this.m_rumblePlayed = true;
      };
    };
    if Equals(this.GetAttackType(), EMeleeAttackType.Final) {
      MeleeTransition.ClearInputBuffer(stateContext);
    } else {
      if this.IsAttackWindowOpen(stateContext, scriptInterface) {
        MeleeTransition.UpdateMeleeInputBuffer(stateContext, scriptInterface);
      };
    };
    if duration >= attackData.attackWindowClosed {
      stateContext.SetPermanentBoolParameter(n"MeleeAttackDone", false, true);
      this.SetIsAttacking(stateContext, false);
    };
  }

  protected final func UpdateIKData(scriptInterface: ref<StateGameScriptInterface>, const attackData: ref<MeleeAttackData>) -> Void {
    let slotPosition: Vector4;
    let animFeature: ref<AnimFeature_MeleeIKData> = new AnimFeature_MeleeIKData();
    let target: ref<GameObject> = DefaultTransition.GetTargetObject(scriptInterface);
    if IsDefined(target) {
      if AIActionHelper.GetTargetSlotPosition(target, n"Head", slotPosition) {
        animFeature.headPosition = slotPosition;
        animFeature.isValid = true;
      };
      if AIActionHelper.GetTargetSlotPosition(target, n"Chest", slotPosition) {
        animFeature.chestPosition = slotPosition;
        animFeature.isValid = true;
      };
      animFeature.ikOffset.X = attackData.ikOffset.X;
      animFeature.ikOffset.Y = attackData.ikOffset.Y;
      animFeature.ikOffset.Z = attackData.ikOffset.Z;
    };
    scriptInterface.SetAnimationParameterFeature(n"MeleeIKData", animFeature);
  }

  protected final func ShouldBlockMovementImpulseUpdate(timeDelta: Float, attackData: ref<MeleeAttackData>, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !scriptInterface.IsOnGround() && (attackData.forwardImpulse < 0.00 || attackData.forwardImpulse > 0.00) {
      return true;
    };
    if scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) >= 10.00 {
      return true;
    };
    if scriptInterface.localBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideMovingElevator) {
      return true;
    };
    return false;
  }

  protected final func UpdateMovementImpulse(timeDelta: Float, attackData: ref<MeleeAttackData>, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.m_blockImpulseCreation {
      return true;
    };
    if this.IsPlayerExhausted(scriptInterface) {
      return true;
    };
    if this.m_shouldBlockImpulseUpdate {
      return false;
    };
    if scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LocomotionDetailed) == 5 || scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LocomotionDetailed) == 6 || scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.LocomotionDetailed) == 31 {
      return false;
    };
    if this.GetInStateTime() < attackData.impulseDelay {
      return false;
    };
    if !this.CheckItemType(scriptInterface, gamedataItemType.Cyb_MantisBlades) {
      if (attackData.cameraSpaceImpulse > 0.00 || attackData.forwardImpulse > 0.00) && !this.IsPlayerInputDirectedForward(scriptInterface) {
        return true;
      };
      if this.IsCameraPitchAcceptable(stateContext, scriptInterface, this.GetStaticFloatParameterDefault("cameraPitchThreshold", -30.00)) {
        return true;
      };
    };
    if !this.AdjustAttackPosition(scriptInterface, stateContext, attackData) {
      this.AddAttackImpulse(scriptInterface, stateContext, attackData);
    };
    return true;
  }

  protected final func UpdateEffectPosition(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, attackData: ref<MeleeAttackData>, duration: Float) -> Void {
    let attackDirectionWorld: Vector4;
    let attackProgress: Float;
    let attackRange: Float;
    let bikeVelMag: Float;
    let colliderBox: Vector4;
    let mountedBike: ref<BikeObject>;
    let playerPuppet: ref<PlayerPuppet>;
    let startPosition: Vector4;
    let cameraWorldTransform: Transform = this.GetCameraTransformForMelee(scriptInterface);
    EffectData.GetVector(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.box, colliderBox);
    attackRange = scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.Range);
    attackProgress = (duration - attackData.attackEffectDelay) / attackData.attackEffectDuration;
    if attackProgress > 0.50 && attackData.useMiddlePosition {
      startPosition = attackData.middlePosition - attackData.endPosition - attackData.middlePosition;
      if !this.m_effectPositionUpdated {
        this.m_effectPositionUpdated = false;
        attackDirectionWorld = Transform.TransformPoint(cameraWorldTransform, attackData.endPosition) - Transform.TransformPoint(cameraWorldTransform, attackData.middlePosition);
        attackDirectionWorld *= 2.00;
        attackDirectionWorld.W = 0.00;
        EffectData.SetVector(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.forward, Vector4.Normalize(attackDirectionWorld));
        EffectData.SetFloat(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.range, Vector4.Length(attackDirectionWorld));
      };
    } else {
      startPosition = attackData.startPosition;
    };
    playerPuppet = scriptInterface.executionOwner as PlayerPuppet;
    mountedBike = playerPuppet.GetMountedVehicle() as BikeObject;
    if IsDefined(mountedBike) {
      attackRange = MaxF(this.GetStaticFloatParameterDefault("mountedMinBaseAttackRange", 2.00), attackRange);
      bikeVelMag = Vector4.Length(mountedBike.GetLinearVelocity());
      attackRange *= ProportionalClampF(0.00, 20.00, bikeVelMag, 1.00, 1.50);
      colliderBox.Y = attackRange;
      EffectData.SetVector(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.box, colliderBox);
    };
    if !attackData.isThrust {
      startPosition.Y += attackRange * 0.50;
    };
    EffectData.SetVector(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, Transform.TransformPoint(cameraWorldTransform, startPosition));
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let attackData: ref<MeleeAttackData> = this.GetAttackData(stateContext);
    let interruptEvent: StateResultBool = stateContext.GetPermanentBoolParameter(n"InterruptMelee");
    scriptInterface.SetAnimationParameterFloat(n"safe", 0.00);
    stateContext.SetPermanentBoolParameter(n"safe", false, true);
    if interruptEvent.value {
      this.GetAttackDataFromCurrentState(stateContext, scriptInterface, stateContext.GetIntParameter(n"meleeAttackNumber", true), attackData);
      stateContext.SetPermanentFloatParameter(n"meleeRecoveryDuration", attackData.idleTransitionTime - this.GetInStateTime(), true);
    };
    stateContext.SetPermanentBoolParameter(n"InterruptMelee", false, true);
    stateContext.SetPermanentBoolParameter(n"IsInMeleeLeapState", false, true);
    this.m_enteredFromMeleeLeap = false;
    super.OnExit(stateContext, scriptInterface);
    this.ClearDebugText(scriptInterface, this.m_textLayer);
    this.ClearLeapedDistanceBlackboardValue(scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_enteredFromMeleeLeap = false;
    this.ClearDebugText(scriptInterface, this.m_textLayer);
    this.ClearLeapedDistanceBlackboardValue(scriptInterface);
    super.OnForcedExit(stateContext, scriptInterface);
  }

  private final func ClearLeapedDistanceBlackboardValue(const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let playerPerkDataBB: ref<IBlackboard> = GameInstance.GetBlackboardSystem(scriptInterface.executionOwner.GetGame()).Get(GetAllBlackboardDefs().PlayerPerkData);
    if IsDefined(playerPerkDataBB) {
      playerPerkDataBB.SetFloat(GetAllBlackboardDefs().PlayerPerkData.LeapedDistance, 0.00, true);
    };
  }

  private final func RotateCameraToFinisherTarget(scriptInterface: ref<StateGameScriptInterface>, rotateDuration: Float) -> Void {
    let aimRequest: AimRequest;
    let slotTransform: WorldTransform;
    let maxDuration: Float = rotateDuration;
    if IsDefined(this.m_finisherTarget) && this.GetClosestSlotTransform(scriptInterface, false, this.m_finisherTarget.GetSlotComponent(), slotTransform) {
      maxDuration += TDB.GetFloat(t"playerStateMachineMelee.meleeLeap.attackStartupDuration");
      aimRequest = this.FillLookAtRequestData(WorldPosition.ToVector4(WorldTransform.GetWorldPosition(slotTransform)), rotateDuration, maxDuration * 2.00);
      GameInstance.GetTargetingSystem(scriptInterface.executionOwner.GetGame()).LookAt(scriptInterface.executionOwner, aimRequest);
    };
  }

  private final func ResetCameraRotation(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let aimRequest: AimRequest = this.FillLookAtRequestData(new Vector4(), 0.00, 0.00);
    GameInstance.GetTargetingSystem(scriptInterface.executionOwner.GetGame()).LookAt(scriptInterface.executionOwner, aimRequest);
  }

  private final func CreateMeleeAttackForFinisher(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, out attackData: ref<MeleeAttackData>) -> Bool {
    let cameraWorldTransform: Transform;
    let colliderBox: Vector4;
    let newForward: Vector4;
    let slotTransform: WorldTransform;
    let slotTransformPos: Vector4;
    let time: Float;
    let vecToTarget: Vector4;
    let worldToLocal: Transform;
    let sweepBoxColliderSize: Float = 0.40;
    if IsDefined(this.m_finisherTarget) && this.GetClosestSlotTransform(scriptInterface, false, this.m_finisherTarget.GetSlotComponent(), slotTransform) {
      cameraWorldTransform = this.GetCameraTransformForMelee(scriptInterface);
      slotTransformPos = WorldPosition.ToVector4(WorldTransform.GetWorldPosition(slotTransform));
      cameraWorldTransform.position += attackData.startPosition;
      vecToTarget = slotTransformPos - cameraWorldTransform.position;
      vecToTarget = 2.00 * Vector4.Normalize(vecToTarget);
      worldToLocal = Transform.GetInverse(cameraWorldTransform);
      newForward = Transform.TransformPoint(worldToLocal, cameraWorldTransform.position + vecToTarget);
      attackData.endPosition = newForward;
      time = attackData.attackEffectDuration;
      colliderBox.X = sweepBoxColliderSize;
      colliderBox.Y = sweepBoxColliderSize;
      colliderBox.Z = sweepBoxColliderSize;
      this.SpawnAttackGameEffect(stateContext, scriptInterface, attackData.startPosition, attackData.middlePosition, attackData.endPosition, time, colliderBox, attackData);
      return true;
    };
    return false;
  }

  protected final func CreateMeleeAttack(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, attackData: ref<MeleeAttackData>) -> Void {
    let colliderBox: Vector4;
    let time: Float;
    let weaponType: gamedataItemType;
    let sweepBoxColliderSize: Float = 0.25;
    let startPosition: Vector4 = attackData.startPosition;
    let middlePosition: Vector4 = attackData.middlePosition;
    let endPosition: Vector4 = attackData.endPosition;
    let attackRange: Float = scriptInterface.GetStatsSystem().GetStatValue(Cast<StatsObjectID>(scriptInterface.ownerEntityID), gamedataStatType.Range);
    colliderBox.X = sweepBoxColliderSize;
    colliderBox.Y = sweepBoxColliderSize;
    colliderBox.Z = sweepBoxColliderSize;
    if attackData.isThrust {
      endPosition.Y = attackRange;
      weaponType = MeleeTransition.GetWeaponType(scriptInterface);
      switch weaponType {
        case gamedataItemType.Cyb_StrongArms:
          if MeleeTransition.HasOnePunchManStatusEffect(scriptInterface) {
            colliderBox.X = TDB.GetFloat(t"Items.StrongArms.colliderBoxWidth");
            colliderBox.Z = TDB.GetFloat(t"Items.StrongArms.colliderBoxHeight");
          };
          break;
        case gamedataItemType.Cyb_MantisBlades:
          if MeleeTransition.HasGrandFinaleStatusEffect(scriptInterface) {
            colliderBox.X = TDB.GetFloat(t"Items.MantisBlades.colliderBoxWidth");
            colliderBox.Z = TDB.GetFloat(t"Items.MantisBlades.colliderBoxHeight");
          };
          break;
        default:
      };
    } else {
      startPosition.Y += attackRange * 0.50;
      middlePosition.Y += attackRange * 0.50;
      endPosition.Y += attackRange * 0.50;
      colliderBox.Y = attackRange;
    };
    time = attackData.attackEffectDuration;
    this.SpawnAttackGameEffect(stateContext, scriptInterface, startPosition, middlePosition, endPosition, time, colliderBox, attackData);
    GameObject.StartCooldown(scriptInterface.owner, n"MeleeAttackCooldown", attackData.attackWindowClosed - attackData.attackEffectDelay, true);
  }

  protected final func GetCameraTransformForMelee(scriptInterface: ref<StateGameScriptInterface>) -> Transform {
    let bikeEulerAngles: EulerAngles;
    let bikeVelMag: Float;
    let cameraEulerAngles: EulerAngles;
    let mountedBike: ref<BikeObject>;
    let pitchConstraint: Float;
    let targetComponent: wref<TargetingComponent>;
    let toTarget: Vector4;
    let playerPuppet: ref<PlayerPuppet> = scriptInterface.executionOwner as PlayerPuppet;
    let fppL2W: Matrix = playerPuppet.GetFPPCameraComponent().GetLocalToWorld();
    let cameraWorldTransform: Transform = scriptInterface.GetCameraWorldTransform();
    Transform.SetPosition(cameraWorldTransform, Matrix.GetTranslation(fppL2W));
    targetComponent = GameInstance.GetTargetingSystem(playerPuppet.GetGame()).GetTrackedTargetComponent(playerPuppet);
    if IsDefined(targetComponent) {
      toTarget = Matrix.GetTranslation(targetComponent.GetLocalToWorld()) - cameraWorldTransform.position;
    };
    if this.IsMountedTPPAttack() {
      if IsDefined(targetComponent) {
        cameraEulerAngles = Quaternion.ToEulerAngles(Quaternion.BuildFromDirectionVector(toTarget));
      } else {
        cameraEulerAngles = Quaternion.ToEulerAngles(cameraWorldTransform.orientation);
      };
      playerPuppet = scriptInterface.executionOwner as PlayerPuppet;
      mountedBike = playerPuppet.GetMountedVehicle() as BikeObject;
      if IsDefined(mountedBike) {
        bikeEulerAngles = Vector4.ToRotation(mountedBike.GetWorldForward());
      };
      playerPuppet = scriptInterface.executionOwner as PlayerPuppet;
      mountedBike = playerPuppet.GetMountedVehicle() as BikeObject;
      if IsDefined(mountedBike) {
        bikeVelMag = Vector4.Length(mountedBike.GetLinearVelocity());
        pitchConstraint = ProportionalClampF(0.00, 20.00, bikeVelMag, 20.00, 12.00);
      } else {
        pitchConstraint = 20.00;
      };
      if mountedBike.IsLeaningOnOneWheel() {
        cameraEulerAngles.Pitch = (cameraEulerAngles.Pitch + 3.00) / 2.00;
      } else {
        cameraEulerAngles.Pitch = ClampF((cameraEulerAngles.Pitch + bikeEulerAngles.Pitch) / 2.00, -pitchConstraint, pitchConstraint) - bikeEulerAngles.Pitch;
      };
      if IsDefined(mountedBike) {
        cameraWorldTransform.position.Z -= ProportionalClampF(5.00, 20.00, bikeVelMag, 0.00, 0.25) * ProportionalClampF(-pitchConstraint, -pitchConstraint / 2.00, cameraEulerAngles.Pitch, 1.00, 0.00);
      };
      cameraEulerAngles.Yaw = ClampF(this.m_tppYawOverride, this.GetStaticFloatParameterDefault("mountedMinYaw", -120.00), this.GetStaticFloatParameterDefault("mountedMaxYaw", 120.00));
      cameraEulerAngles.Yaw += playerPuppet.GetWorldYaw();
      if cameraEulerAngles.Yaw > 180.00 {
        cameraEulerAngles.Yaw -= 360.00;
      } else {
        if cameraEulerAngles.Yaw < -180.00 {
          cameraEulerAngles.Yaw += 360.00;
        };
      };
      Transform.SetOrientation(cameraWorldTransform, EulerAngles.ToQuat(cameraEulerAngles));
    } else {
      if IsDefined(targetComponent) {
        Transform.SetOrientation(cameraWorldTransform, Quaternion.BuildFromDirectionVector(toTarget));
      };
    };
    return cameraWorldTransform;
  }

  protected final func SpawnAttackGameEffect(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, startPosition: Vector4, middlePosition: Vector4, endPosition: Vector4, time: Float, colliderBox: Vector4, attackData: ref<MeleeAttackData>) -> Bool {
    let attackDirectionWorld: Vector4;
    let effect: ref<EffectInstance>;
    let meleeAttack: ref<Attack_GameEffect>;
    let weapon: ref<WeaponObject>;
    let success: Bool = false;
    let cameraWorldTransform: Transform = this.GetCameraTransformForMelee(scriptInterface);
    let attackStartPositionWorld: Vector4 = Transform.TransformPoint(cameraWorldTransform, startPosition);
    attackStartPositionWorld.W = 0.00;
    this.m_effectPositionUpdated = !attackData.useMiddlePosition;
    if attackData.useMiddlePosition {
      attackDirectionWorld = Transform.TransformPoint(cameraWorldTransform, middlePosition) - attackStartPositionWorld;
      attackDirectionWorld *= 2.00;
    } else {
      attackDirectionWorld = Transform.TransformPoint(cameraWorldTransform, endPosition) - attackStartPositionWorld;
    };
    attackDirectionWorld.W = 0.00;
    weapon = scriptInterface.owner as WeaponObject;
    if IsDefined(weapon) {
      meleeAttack = weapon.GetCurrentAttack() as Attack_GameEffect;
      if IsDefined(meleeAttack) {
        effect = meleeAttack.PrepareAttack(scriptInterface.executionOwner);
        EffectData.SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.box, colliderBox);
        EffectData.SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.duration, time);
        EffectData.SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, attackStartPositionWorld);
        EffectData.SetQuat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.rotation, Transform.GetOrientation(cameraWorldTransform));
        EffectData.SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.forward, Vector4.Normalize(attackDirectionWorld));
        EffectData.SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.range, Vector4.Length(attackDirectionWorld));
        EffectData.SetInt(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attackNumber, stateContext.GetIntParameter(n"meleeAttackNumber", true));
        EffectData.SetName(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.impactOrientationSlot, attackData.impactFxSlot);
        EffectData.SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.charge, WeaponObject.GetWeaponChargeNormalized(weapon));
        EffectData.SetBool(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.ignoreMountedVehicleCollision, true);
        if Equals(this.GetAttackType(), EMeleeAttackType.Strong) || scriptInterface.localBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.MountedToVehicle) {
          EffectData.SetBool(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.meleeCleave, true);
        };
        if this.IsMountedTPPAttack() {
          EffectData.SetBool(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.inTPP, true);
        };
        EffectData.SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.fxPackage, ToVariant((scriptInterface.owner as WeaponObject).GetFxPackage()));
        EffectData.SetBool(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.playerOwnedWeapon, true);
        this.m_effect = effect;
        success = meleeAttack.StartAttack();
      };
    };
    return success;
  }

  protected final func BroadcastStimuli(scriptInterface: ref<StateGameScriptInterface>, radius: Float) -> Void {
    let position: Vector4 = scriptInterface.executionOwner.GetWorldPosition();
    let stimuliEvent: ref<StimuliEvent> = new StimuliEvent();
    stimuliEvent.sourcePosition = position;
    stimuliEvent.name = n"run";
    let effect: ref<EffectInstance> = GameInstance.GetGameEffectSystem(scriptInterface.GetGame()).CreateEffectStatic(n"stimuli", n"stimuli_range", scriptInterface.executionOwner, scriptInterface.owner);
    EffectData.SetVariant(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.stimuliEvent, ToVariant(stimuliEvent));
    EffectData.SetVector(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, position);
    EffectData.SetFloat(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.radius, radius);
    EffectData.SetBool(effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.stimuliRaycastTest, true);
    GameInstance.GetStimuliSystem(scriptInterface.owner.GetGame()).BroadcastStimuli(effect);
  }

  protected final const func GetAttackData(const stateContext: ref<StateContext>) -> ref<MeleeAttackData> {
    return stateContext.GetConditionScriptableParameter(n"MeleeAttackData") as MeleeAttackData;
  }

  protected final const func IsAttackWindowOpen(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let attackData: ref<MeleeAttackData> = this.GetAttackData(stateContext);
    let inStateTime: Float = this.GetInStateTime();
    if inStateTime >= attackData.attackWindowOpen {
      return true;
    };
    return false;
  }
}

public class MeleeComboAttackDecisions extends MeleeAttackGenericDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !MeleeTransition.WantsToLightAttack(stateContext, scriptInterface) {
      return false;
    };
    if !super.EnterCondition(stateContext, scriptInterface) {
      return false;
    };
    if this.m_driverCombatListener.IsMountedInTPP() && GameInstance.GetPhotoModeSystem(scriptInterface.executionOwner.GetGame()).IsPhotoModeActive() {
      return false;
    };
    return true;
  }
}

public class MeleeComboAttackEvents extends MeleeAttackGenericEvents {

  protected final func GetAttackType() -> EMeleeAttackType {
    return EMeleeAttackType.Combo;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 11);
    super.OnEnter(stateContext, scriptInterface);
    this.IncrementTotalComboAttackNumber(scriptInterface, stateContext);
  }
}

public class MeleeMountedComboAttackDecisions extends MeleeComboAttackDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.m_driverCombatListener.IsMountedInTPP() {
      return false;
    };
    if !super.EnterCondition(stateContext, scriptInterface) {
      return false;
    };
    return true;
  }
}

public class MeleeMountedComboAttackEvents extends MeleeComboAttackEvents {

  protected final const func IsMountedTPPAttack() -> Bool {
    return true;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetTPPYawOverride(stateContext);
    super.OnEnter(stateContext, scriptInterface);
  }
}

public class MeleeFinalAttackDecisions extends MeleeAttackGenericDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.CheckIfFinalAttack(scriptInterface, stateContext) {
      return false;
    };
    if MeleeTransition.WantsToStrongAttack(stateContext, scriptInterface) {
      return false;
    };
    if !MeleeTransition.WantsToLightAttack(stateContext, scriptInterface) {
      return false;
    };
    if !super.EnterCondition(stateContext, scriptInterface) {
      return false;
    };
    return true;
  }
}

public class MeleeFinalAttackEvents extends MeleeAttackGenericEvents {

  protected final func GetAttackType() -> EMeleeAttackType {
    return EMeleeAttackType.Final;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    stateContext.SetPermanentBoolParameter(n"finalAttack", true, true);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 12);
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    stateContext.SetPermanentBoolParameter(n"finalAttack", false, true);
    super.OnExit(stateContext, scriptInterface);
  }
}

public class MeleeMountedFinalAttackDecisions extends MeleeFinalAttackDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.m_driverCombatListener.IsMountedInTPP() {
      return false;
    };
    if !super.EnterCondition(stateContext, scriptInterface) {
      return false;
    };
    return true;
  }
}

public class MeleeMountedFinalAttackEvents extends MeleeFinalAttackEvents {

  protected final const func IsMountedTPPAttack() -> Bool {
    return true;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetTPPYawOverride(stateContext);
    super.OnEnter(stateContext, scriptInterface);
  }
}

public class MeleeSafeAttackDecisions extends MeleeAttackGenericDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !MeleeTransition.WantsToLightAttack(stateContext, scriptInterface) {
      return false;
    };
    if !this.HasAttackRecord(scriptInterface) {
      return false;
    };
    return true;
  }
}

public class MeleeSafeAttackEvents extends MeleeAttackGenericEvents {

  protected final func GetAttackType() -> EMeleeAttackType {
    return EMeleeAttackType.Safe;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.ResetAttackNumber(stateContext);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 14);
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
  }
}

public class MeleeStrongAttackDecisions extends MeleeAttackGenericDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if stateContext.GetConditionBool(n"StrongMeleeAttackPressed") {
      return true;
    };
    return false;
  }

  protected const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let isBossFinisher: StateResultBool = stateContext.GetPermanentBoolParameter(n"bossFinisherAttack");
    let finisherTargetID: EntityID = scriptInterface.localBlackboard.GetEntityID(GetAllBlackboardDefs().PlayerStateMachine.FinisherTarget);
    if (!isBossFinisher.valid || !isBossFinisher.value) && EntityID.IsDefined(finisherTargetID) {
      return true;
    };
    return super.ExitCondition(stateContext, scriptInterface);
  }
}

public class MeleeStrongAttackEvents extends MeleeAttackGenericEvents {

  public let m_slowMoSet: Bool;

  public let m_crouchedAfterLeapAttack: Bool;

  protected final func GetAttackType() -> EMeleeAttackType {
    return EMeleeAttackType.Strong;
  }

  protected final func LeapToTarget(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, target: wref<GameObject>) -> Void {
    let slideDuration: Float;
    let targetPos: Vector4;
    let cameraWorldTransform: Transform = scriptInterface.GetCameraWorldTransform();
    let leapAngle: EulerAngles = Transform.ToEulerAngles(cameraWorldTransform);
    if leapAngle.Pitch > this.GetStaticFloatParameterDefault("noTargetMaxPitch", 45.00) {
      leapAngle.Pitch = this.GetStaticFloatParameterDefault("noTargetMaxPitch", 45.00);
      Transform.SetOrientationEuler(cameraWorldTransform, leapAngle);
    };
    slideDuration = this.GetStaticFloatParameterDefault("dashAttackSlideDuration", 0.30);
    targetPos = target.GetWorldPosition();
    if Equals(MeleeTransition.GetWeaponType(scriptInterface), gamedataItemType.Cyb_StrongArms) {
      targetPos = this.TargetPrediction(targetPos, target as ScriptedPuppet, slideDuration, 1.00);
    };
    this.RequestPlayerPositionAdjustment(stateContext, scriptInterface, null, slideDuration, 0.00, -1.00, targetPos);
  }

  public final func ApplyRelicMeleewareDamageToTarget(scriptInterface: ref<StateGameScriptInterface>, playerPuppet: wref<GameObject>, target: ref<NPCPuppet>) -> Void {
    let applyDamageEvt: ref<ApplyRelicMeleewareDamageOnNPCEvent>;
    let attack: ref<IAttack>;
    let attackContext: AttackInitContext;
    let attackDelay: Float;
    let weapon: ref<WeaponObject> = GameObject.GetActiveWeapon(playerPuppet);
    let attackComputed: ref<gameAttackComputed> = new gameAttackComputed();
    let newHitEvent: ref<gameHitEvent> = new gameHitEvent();
    newHitEvent.attackData = new AttackData();
    newHitEvent.target = target;
    newHitEvent.attackComputed = attackComputed;
    if this.CheckItemType(scriptInterface, gamedataItemType.Cyb_StrongArms) {
      attackDelay = TDB.GetFloat(t"Items.StrongArms.onePunchAttackDelay");
      attackContext.record = TweakDBInterface.GetAttackRecord(t"StrongArmsAttacks.OnePunchAttackTargetDamage");
    } else {
      attackDelay = TDB.GetFloat(t"Items.MantisBlades.grandFinaleAttackDelay");
      attackContext.record = TweakDBInterface.GetAttackRecord(t"MantisBladesAttacks.GrandFinaleAttackTargetDamage");
    };
    attackContext.instigator = playerPuppet;
    attackContext.source = playerPuppet;
    attackContext.weapon = weapon;
    attack = IAttack.Create(attackContext);
    newHitEvent.attackData.SetAttackDefinition(attack);
    if this.CheckItemType(scriptInterface, gamedataItemType.Cyb_StrongArms) {
      newHitEvent.attackData.AddFlag(hitFlag.OnePunch, n"OnePunch");
    } else {
      newHitEvent.attackData.AddFlag(hitFlag.GrandFinale, n"GrandFinale");
    };
    newHitEvent.attackData.AddFlag(hitFlag.RelicGoldenNumbers, n"RelicGoldenNumbers");
    newHitEvent.attackData.SetSource(playerPuppet);
    newHitEvent.attackData.SetInstigator(playerPuppet);
    newHitEvent.attackData.SetWeapon(weapon);
    applyDamageEvt = new ApplyRelicMeleewareDamageOnNPCEvent();
    applyDamageEvt.newHitEvent = newHitEvent;
    applyDamageEvt.target = target;
    applyDamageEvt.weapon = MeleeTransition.GetWeaponObject(scriptInterface);
    applyDamageEvt.weaponType = MeleeTransition.GetWeaponType(scriptInterface);
    GameInstance.GetDelaySystem(target.GetGame()).DelayEvent(target, applyDamageEvt, attackDelay, true);
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let crossHairNPC: NPCNextToTheCrosshair;
    let crosshairTarget: ref<NPCPuppet>;
    let isFinisher: Bool;
    let maxDistToTarget: Float;
    let npcdata: ref<IBlackboard>;
    let player: wref<GameObject>;
    let target: ref<NPCPuppet>;
    let targetSearchQuery: TargetSearchQuery;
    let weaponType: gamedataItemType;
    let weapon: ref<WeaponObject> = GameObject.GetActiveWeapon(scriptInterface.executionOwner);
    let isBossFinisher: StateResultBool = stateContext.GetPermanentBoolParameter(n"bossFinisherAttack");
    let playerPerkDataBB: ref<IBlackboard> = GameInstance.GetBlackboardSystem(scriptInterface.executionOwner.GetGame()).Get(GetAllBlackboardDefs().PlayerPerkData);
    let finisherTarget: EntityID = scriptInterface.localBlackboard.GetEntityID(GetAllBlackboardDefs().PlayerStateMachine.FinisherTarget);
    let leapTarget: EntityID = playerPerkDataBB.GetEntityID(GetAllBlackboardDefs().PlayerPerkData.LeapTarget);
    let LeapPosition: Vector4 = playerPerkDataBB.GetVector4(GetAllBlackboardDefs().PlayerPerkData.LeapPosition);
    stateContext.SetPermanentBoolParameter(n"strongAttack", true, true);
    isFinisher = EntityID.IsDefined(finisherTarget);
    if MeleeTransition.HasNewSpyAttackStatFlag(scriptInterface) && MeleeTransition.WeaponIsCharged(scriptInterface) && !isFinisher {
      weaponType = MeleeTransition.GetWeaponType(scriptInterface);
      player = scriptInterface.executionOwner;
      target = GameInstance.GetTargetingSystem(player.GetGame()).GetLookAtObject(player, true) as NPCPuppet;
      if !IsDefined(target) {
        maxDistToTarget = this.GetStaticFloatParameterDefault("maxDistToTarget", 5.00);
        targetSearchQuery = TSQ_NPC();
        targetSearchQuery.testedSet = TargetingSet.ClearlyVisible;
        targetSearchQuery.maxDistance = maxDistToTarget;
        targetSearchQuery.filterObjectByDistance = true;
        target = DefaultTransition.GetTargetObject(scriptInterface, targetSearchQuery, maxDistToTarget) as NPCPuppet;
      };
      switch weaponType {
        case gamedataItemType.Cyb_StrongArms:
          if this.IsCloseEnoughForOnePunch(scriptInterface, target) {
            this.LeapToTarget(stateContext, scriptInterface, target);
            this.ApplyRelicMeleewareDamageToTarget(scriptInterface, player, target);
            StatusEffectHelper.ApplyStatusEffect(player, t"BaseStatusEffect.GorillaArmsSpyBuff");
            this.SetAttackNumber(stateContext, TDB.GetInt(t"Items.StrongArms.specialAttackNumber"));
          };
          break;
        case gamedataItemType.Cyb_MantisBlades:
          if MeleeTransition.PlayerLeapedToTargetCheck(scriptInterface) {
            weapon = MeleeTransition.GetWeaponObject(scriptInterface);
            npcdata = GameInstance.GetBlackboardSystem(scriptInterface.GetGame()).Get(GetAllBlackboardDefs().UI_NPCNextToTheCrosshair);
            crossHairNPC = FromVariant<NPCNextToTheCrosshair>(npcdata.GetVariant(GetAllBlackboardDefs().UI_NPCNextToTheCrosshair.NameplateData));
            crosshairTarget = crossHairNPC.npc as NPCPuppet;
            target = GameInstance.FindEntityByID(scriptInterface.executionOwner.GetGame(), leapTarget) as NPCPuppet;
            if IsDefined(crosshairTarget) && (!IsDefined(target) || !GameInstance.GetTargetingSystem(player.GetGame()).IsVisibleTarget(player, target) || Vector4.DistanceSquared(crosshairTarget.GetWorldPosition(), LeapPosition) < Vector4.DistanceSquared(target.GetWorldPosition(), LeapPosition)) {
              target = crosshairTarget;
            };
            this.ApplyRelicMeleewareDamageToTarget(scriptInterface, player, target);
            GameObjectEffectHelper.StartEffectEvent(weapon, n"RelicTree_Meleeware_Attack_MantisBlades");
            this.SetAttackNumber(stateContext, TDB.GetInt(t"Items.MantisBlades.specialAttackNumber"));
            StatusEffectHelper.ApplyStatusEffect(player, t"BaseStatusEffect.MantisBladesSpecialAttackSpyBuff");
          };
          break;
        case gamedataItemType.Cyb_NanoWires:
          if !MeleeTransition.HasMonowireWithQuickhackSelected(scriptInterface) {
            break;
          };
          this.SetAttackNumber(stateContext, TDB.GetInt(t"Items.NanoWires.specialAttackNumber"));
          GameObject.PlaySound(player, n"w_cyb_nanowire_spy_perk_attack");
          break;
        default:
      };
    } else {
      this.SetAttackNumber(stateContext, stateContext.GetIntParameter(n"meleeAttackNumber", true) % 2);
    };
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 13);
    if isBossFinisher.valid && isBossFinisher.value {
      this.m_slowMoSet = false;
      this.m_blockImpulseCreation = true;
      GameObject.PlaySound(scriptInterface.executionOwner, this.GetStaticCNameParameterDefault("slowMoStartSound", n"None"));
      DefaultTransition.PlayRumble(scriptInterface, this.GetStaticStringParameterDefault("rumbleOnStartStrength", "light_fast"));
      GameInstance.GetRazerChromaEffectsSystem(scriptInterface.GetGame()).PlayAnimation(n"SlowMotion", true);
    };
    if this.m_enteredFromMeleeLeap && stateContext.GetBoolParameter(n"crouchAfterLeaping", true) {
      this.m_crouchedAfterLeapAttack = true;
      scriptInterface.SetAnimationParameterFloat(n"crouch", 1.00);
    };
    super.OnEnter(stateContext, scriptInterface);
  }

  public final func OnEnterFromMeleeLeap(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_enteredFromMeleeLeap = true;
    this.OnEnter(stateContext, scriptInterface);
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let slowMoStart: Float = this.GetStaticFloatParameterDefault("slowMoStart", 0.10);
    let isBossFinisher: StateResultBool = stateContext.GetPermanentBoolParameter(n"bossFinisherAttack");
    if !this.m_slowMoSet && isBossFinisher.valid && isBossFinisher.value && this.GetInStateTime() > slowMoStart && !this.IsTimeDilationActive(stateContext, scriptInterface, n"None") {
      scriptInterface.GetTimeSystem().SetTimeDilation(n"deflect", this.GetStaticFloatParameterDefault("slowMoAmount", 0.10), this.GetStaticFloatParameterDefault("slowDuration", 0.10), this.GetStaticCNameParameterDefault("slowMoEaseIn", n"Linear"), this.GetStaticCNameParameterDefault("slowMoEaseOut", n"Linear"));
      this.m_slowMoSet = true;
    };
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let isBossFinisher: StateResultBool = stateContext.GetPermanentBoolParameter(n"bossFinisherAttack");
    let playerPuppet: ref<PlayerPuppet> = scriptInterface.executionOwner as PlayerPuppet;
    if isBossFinisher.value && IsDefined(playerPuppet) {
      FinisherEndEvents.ApplyFinisherBuffs(playerPuppet, false);
    };
    stateContext.SetPermanentBoolParameter(n"strongAttack", false, true);
    stateContext.SetPermanentBoolParameter(n"enableVaultFromleapAttack", false, true);
    stateContext.SetPermanentBoolParameter(n"crouchAfterLeaping", false, true);
    this.SetBlackboardBoolVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeLeap, false);
    stateContext.SetPermanentBoolParameter(n"bossFinisherAttack", false, true);
    GameInstance.GetRazerChromaEffectsSystem(scriptInterface.GetGame()).StopAnimation(n"SlowMotion");
    if this.m_enteredFromMeleeLeap {
      scriptInterface.localBlackboard.SetEntityID(GetAllBlackboardDefs().PlayerStateMachine.FinisherTarget, EMPTY_ENTITY_ID());
    };
    if this.m_crouchedAfterLeapAttack {
      scriptInterface.SetAnimationParameterFloat(n"crouch", 0.00);
    };
    this.m_slowMoSet = false;
    this.m_crouchedAfterLeapAttack = false;
    super.OnExit(stateContext, scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExit(stateContext, scriptInterface);
    super.OnForcedExit(stateContext, scriptInterface);
  }

  protected final func OnExitToMeleeComboAttack(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExit(stateContext, scriptInterface);
  }
}

public class MeleeMountedStrongAttackDecisions extends MeleeStrongAttackDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.m_driverCombatListener.IsMountedInTPP() {
      return false;
    };
    if super.EnterCondition(stateContext, scriptInterface) {
      return true;
    };
    return false;
  }
}

public class MeleeMountedStrongAttackEvents extends MeleeAttackGenericEvents {

  protected final func GetAttackType() -> EMeleeAttackType {
    return EMeleeAttackType.Strong;
  }

  protected final const func IsMountedTPPAttack() -> Bool {
    return true;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetAttackNumber(stateContext, stateContext.GetIntParameter(n"meleeAttackNumber", true) % 2);
    this.SetTPPYawOverride(stateContext);
    super.OnEnter(stateContext, scriptInterface);
  }
}

public class MeleeDeflectDecisions extends MeleeTransition {

  protected final const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.IsBlockPressed(stateContext, scriptInterface) {
      if !this.CanWeaponDeflect(stateContext, scriptInterface) {
        return false;
      };
      if this.HasMeleeTargeting(stateContext, scriptInterface) {
        return false;
      };
      if this.GetStaticBoolParameterDefault("disabled", false) {
        return false;
      };
      if GameObject.IsCooldownActive(scriptInterface.owner, n"Deflect") {
        return false;
      };
      return true;
    };
    return false;
  }

  protected final const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.IsDeflectSuccessful(stateContext, scriptInterface) {
      return true;
    };
    if this.ShouldInterruptHoldStates(stateContext, scriptInterface) {
      return true;
    };
    if this.GetInStateTime() >= this.GetStaticFloatParameterDefault("duration", 0.40) {
      return true;
    };
    if this.IsBlockHeld(stateContext, scriptInterface) {
      return MeleeTransition.AnyMeleeAttackPressed(scriptInterface) || stateContext.GetConditionBool(n"LightMeleeAttackPressed");
    };
    return false;
  }

  public final func ToMeleeHold(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.ShouldHold(stateContext, scriptInterface) && this.ToMeleeComboAttack(stateContext, scriptInterface) && !MeleeTransition.IsPlayingSyncedAnimation(scriptInterface) {
      return true;
    };
    return false;
  }

  protected final const func ToMeleeComboAttack(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.IsBlockHeld(stateContext, scriptInterface) {
      if stateContext.GetConditionBool(n"LightMeleeAttackPressed") && !MeleeTransition.IsPlayingSyncedAnimation(scriptInterface) {
        return true;
      };
    };
    return false;
  }

  public final func ToMeleeBlock(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return true;
  }

  protected final func ToMeleeDeflectAttack(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.IsDeflectSuccessful(stateContext, scriptInterface);
  }

  protected final const func IsDeflectSuccessful(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let deflectEvent: StateResultBool = stateContext.GetTemporaryBoolParameter(n"successfulDeflect");
    return deflectEvent.valid;
  }
}

public class MeleeDeflectEvents extends MeleeEventsTransition {

  public let deflectStatFlag: ref<gameStatModifierData>;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let ownerID: StatsObjectID = Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID);
    this.SetIsBlocking(stateContext, true);
    scriptInterface.PushAnimationEvent(n"Deflect");
    scriptInterface.GetTargetingSystem().AimSnap(scriptInterface.executionOwner);
    this.deflectStatFlag = RPGManager.CreateStatModifier(gamedataStatType.IsDeflecting, gameStatModifierType.Additive, 1.00);
    scriptInterface.GetStatsSystem().AddModifier(ownerID, this.deflectStatFlag);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee, 2);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 10);
    stateContext.SetTemporaryBoolParameter(n"InterruptSprint", true, true);
    MeleeTransition.ClearInputBuffer(stateContext);
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let ownerID: StatsObjectID = Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID);
    scriptInterface.GetStatsSystem().RemoveModifier(ownerID, this.deflectStatFlag);
    GameObject.StartCooldown(scriptInterface.owner, n"Deflect", this.GetStaticFloatParameterDefault("cooldown", -1.00), true);
    super.OnExit(stateContext, scriptInterface);
  }
}

public class MeleeDeflectAttackDecisions extends MeleeAttackGenericDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.HasAttackRecord(scriptInterface) {
      return false;
    };
    return true;
  }
}

public class MeleeDeflectAttackEvents extends MeleeAttackGenericEvents {

  public let m_slowMoSet: Bool;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.ResetAttackNumber(stateContext);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 20);
    this.TutorialAddFact(scriptInterface, n"melee_deflect_tutorial", 1);
    DefaultTransition.PlayRumble(scriptInterface, this.GetStaticStringParameterDefault("rumbleOnStartStrength", "light_fast"));
    GameObject.PlaySound(scriptInterface.executionOwner, this.GetStaticCNameParameterDefault("slowMoStartSound", n"None"));
    this.m_slowMoSet = false;
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let slowMoStart: Float = this.GetStaticFloatParameterDefault("slowMoStart", 0.10);
    if !this.m_slowMoSet && this.GetInStateTime() > slowMoStart && !this.IsTimeDilationActive(stateContext, scriptInterface, n"None") {
      scriptInterface.GetTimeSystem().SetTimeDilation(n"deflect", this.GetStaticFloatParameterDefault("slowMoAmount", 0.10), this.GetStaticFloatParameterDefault("slowDuration", 0.10), this.GetStaticCNameParameterDefault("slowMoEaseIn", n"Linear"), this.GetStaticCNameParameterDefault("slowMoEaseOut", n"Linear"));
      this.m_slowMoSet = true;
    };
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
  }
}

public class MeleeBlockDecisions extends MeleeTransition {

  protected final const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.IsBlockHeld(stateContext, scriptInterface) {
      if this.IsAttackParried(stateContext, scriptInterface) {
        return false;
      };
      if !this.CanWeaponBlock(stateContext, scriptInterface) {
        return false;
      };
      if this.HasMeleeTargeting(stateContext, scriptInterface) && this.CanThrowWeapon(stateContext, scriptInterface) {
        return false;
      };
      if GameObject.IsCooldownActive(scriptInterface.owner, n"Block") {
        return false;
      };
      if GameObject.IsCooldownActive(scriptInterface.owner, n"Deflect") {
        return false;
      };
      if !this.IsBlockHeld(stateContext, scriptInterface) {
        return false;
      };
      return true;
    };
    return false;
  }

  protected final const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if this.ShouldEnterSafe(stateContext, scriptInterface) {
      return true;
    };
    if this.ShouldInterruptHoldStates(stateContext, scriptInterface) {
      return true;
    };
    if MeleeTransition.AnyMeleeAttackPressed(scriptInterface) {
      return true;
    };
    if this.IsBlockHeld(stateContext, scriptInterface) && !stateContext.IsStateActive(n"Locomotion", n"sprint") {
      return false;
    };
    if this.GetInStateTime() >= this.GetStaticFloatParameterDefault("minDuration", -1.00) {
      return true;
    };
    return false;
  }
}

public class MeleeBlockEvents extends MeleeRumblingEvents {

  public let blockStatFlag: ref<gameStatModifierData>;

  public func GetIntensity() -> String {
    return "medium";
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let ownerID: StatsObjectID = Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID);
    MeleeTransition.ClearInputBuffer(stateContext);
    this.SetMeleeAttackPressCount(stateContext, scriptInterface);
    this.SetIsAttacking(stateContext, false);
    this.SetIsBlocking(stateContext, true);
    scriptInterface.PushAnimationEvent(n"Block");
    stateContext.SetTemporaryBoolParameter(n"InterruptSprint", true, true);
    stateContext.SetTemporaryBoolParameter(n"InterruptBlockForLeap", false, true);
    this.blockStatFlag = RPGManager.CreateStatModifier(gamedataStatType.IsBlocking, gameStatModifierType.Additive, 1.00);
    scriptInterface.GetStatsSystem().AddModifier(ownerID, this.blockStatFlag);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee, 2);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 8);
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitCommon(stateContext, scriptInterface);
    super.OnExit(stateContext, scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitCommon(stateContext, scriptInterface);
    this.OnExit(stateContext, scriptInterface);
  }

  private final func OnExitCommon(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let ownerID: StatsObjectID = Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID);
    scriptInterface.GetStatsSystem().RemoveModifier(ownerID, this.blockStatFlag);
    GameObject.StartCooldown(scriptInterface.owner, n"Block", this.GetStaticFloatParameterDefault("cooldown", -1.00), true);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee, 0);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 22);
  }
}

public class MeleeTargetingDecisions extends MeleeTransition {

  protected final const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !MeleeTransition.CanThrowWeaponObject(scriptInterface.executionOwner, MeleeTransition.GetWeaponObject(scriptInterface)) {
      return false;
    };
    if !this.IsBlockHeld(stateContext, scriptInterface) {
      return false;
    };
    return true;
  }

  protected final const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if GameInstance.GetPhotoModeSystem(scriptInterface.executionOwner.GetGame()).IsPhotoModeActive() {
      return false;
    };
    if this.ShouldInterruptHoldStates(stateContext, scriptInterface) {
      return true;
    };
    if !this.EnterCondition(stateContext, scriptInterface) {
      return true;
    };
    if MeleeTransition.MeleeAttackPressed(scriptInterface) {
      return true;
    };
    return false;
  }
}

public class MeleeTargetingEvents extends MeleeEventsTransition {

  private let m_aimInTimeRemaining: Float;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let attackData: ref<MeleeAttackData>;
    let aimAssistRecord: ref<AimAssistMelee_Record> = MeleeTransition.GetAimAssistMeleeRecord(scriptInterface);
    this.SetIsTargeting(stateContext, true);
    scriptInterface.PushAnimationEvent(n"Targeting");
    this.ResetAttackNumber(stateContext);
    this.GetAttackDataFromState(stateContext, scriptInterface, "MeleeThrowAttack", stateContext.GetIntParameter(n"meleeAttackNumber", true), attackData);
    stateContext.SetTemporaryBoolParameter(n"InterruptSprint", true, true);
    this.m_aimInTimeRemaining = 0.25;
    this.SetBlackboardFloatVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.AimInTimeRemaining, this.m_aimInTimeRemaining);
    this.SetBlackboardFloatVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.AimInTime, this.m_aimInTimeRemaining);
    if IsDefined(aimAssistRecord) && aimAssistRecord.AimSnapOnAim() {
      scriptInterface.GetTargetingSystem().AimSnap(scriptInterface.executionOwner);
    };
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee, 2);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 9);
    super.OnEnter(stateContext, scriptInterface);
    GameInstance.GetAudioSystem(scriptInterface.owner.GetGame()).AddTriggerEffect(n"te_wea_melee_throw", n"PSM_MeleeTargetingOnEnter_trigger");
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let weaponObject: ref<WeaponObject> = MeleeTransition.GetWeaponObject(scriptInterface);
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    this.ShowAttackPreview(true, weaponObject, scriptInterface, stateContext);
    this.HandleDamagePreview(weaponObject, scriptInterface, stateContext);
    if this.m_aimInTimeRemaining > 0.00 {
      this.m_aimInTimeRemaining -= timeDelta;
      this.SetBlackboardFloatVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.AimInTimeRemaining, this.m_aimInTimeRemaining);
    };
  }

  public final func OnCommonExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_aimInTimeRemaining = 0.00;
    this.SetBlackboardFloatVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.AimInTimeRemaining, this.m_aimInTimeRemaining);
    this.ActivateDamageProjection(false, MeleeTransition.GetWeaponObject(scriptInterface), scriptInterface, stateContext);
    if !MeleeTransition.MeleeAttackPressed(scriptInterface) {
      stateContext.SetTemporaryBoolParameter(n"requestKerenzikovDeactivation", true, true);
    };
    stateContext.SetTemporaryBoolParameter(n"requestKerenzikovDeactivationWithEaseOut", true, true);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnCommonExit(stateContext, scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnCommonExit(stateContext, scriptInterface);
  }
}

public class MeleeThrowAttackDecisions extends MeleeAttackGenericDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.HasAttackRecord(scriptInterface) {
      return false;
    };
    if !this.IsBlockHeld(stateContext, scriptInterface) {
      return false;
    };
    if !MeleeTransition.MeleeAttackPressed(scriptInterface) {
      return false;
    };
    if MeleeTransition.GetWeaponObject(scriptInterface).WeaponHasTag(n"Throwable") && !this.CanThrowWeapon(stateContext, scriptInterface) {
      return false;
    };
    return true;
  }
}

public class MeleeThrowAttackEvents extends MeleeAttackGenericEvents {

  @default(MeleeThrowAttackEvents, false)
  public let m_projectileThrown: Bool;

  public let m_targetObject: wref<GameObject>;

  protected final func GetAttackType() -> EMeleeAttackType {
    return EMeleeAttackType.Throw;
  }

  protected final func EnableLockOnTarget(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let aimRequestData: AimRequest = this.GetBlockLookAtParams();
    scriptInterface.GetTargetingSystem().LookAt(scriptInterface.executionOwner, aimRequestData);
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let aimAssistRecord: ref<AimAssistMelee_Record> = MeleeTransition.GetAimAssistMeleeRecord(scriptInterface);
    let locomotionState: CName = stateContext.GetStateMachineCurrentState(n"Locomotion");
    this.m_projectileThrown = false;
    if this.CheckItemType(scriptInterface, gamedataItemType.Cyb_NanoWires) {
      this.m_targetObject = this.GetNanoWireTargetObject(scriptInterface);
    };
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 19);
    if IsDefined(aimAssistRecord) && aimAssistRecord.AimSnapOnThrow() {
      scriptInterface.GetTargetingSystem().AimSnap(scriptInterface.executionOwner);
    };
    super.OnEnter(stateContext, scriptInterface);
    if Cast<Bool>(PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Cool_Master_Perk_4)) {
      if this.IsInSlidingState(stateContext) || Equals(locomotionState, n"crouchSprint") || Equals(locomotionState, n"coolExitJump") || stateContext.IsStateActive(n"Locomotion", n"sprint") || StatusEffectSystem.ObjectHasStatusEffectWithTag(scriptInterface.executionOwner, n"JustDodged") || StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.DriverCombatVehicleManeuvers") {
        StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.ForceCritOnNextThrowSE");
      };
    };
    stateContext.SetTemporaryBoolParameter(n"requestKerenzikovDeactivationWithEaseOut", true, true);
    scriptInterface.GetTargetingSystem().SetPlayerAimWeaponPositionProvider(scriptInterface.executionOwner, IPositionProvider.CreateSlotPositionProvider(scriptInterface.executionOwner, n"RightHand"));
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let attackData: ref<MeleeAttackData>;
    let isItemNanowire: Bool;
    let isThrowable: Bool;
    let isValidNanowireAttack: Bool;
    super.OnUpdate(timeDelta, stateContext, scriptInterface);
    attackData = this.GetAttackData(stateContext);
    isThrowable = MeleeTransition.GetWeaponObject(scriptInterface).WeaponHasTag(n"Throwable");
    isItemNanowire = this.CheckItemType(scriptInterface, gamedataItemType.Cyb_NanoWires);
    isValidNanowireAttack = isItemNanowire && IsDefined(this.m_targetObject);
    if this.GetInStateTime() > attackData.attackEffectDelay && !this.m_projectileThrown && (isValidNanowireAttack || isThrowable) {
      this.m_projectileThrown = true;
      if isThrowable {
        this.SetIsThrowReloading(stateContext, true);
        this.SendAnimFeatureData(stateContext, scriptInterface);
        this.SpawnMeleeWeaponProjectile(scriptInterface);
        this.ApplyThrowableCooldown(stateContext, scriptInterface);
        MeleeTransition.GetWeaponObject(scriptInterface).SetVisible(false);
      };
    };
  }

  protected final func UpdateNanoWireIKState(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if this.GetInStateTime() >= this.GetStaticFloatParameterDefault("timeToEnableWireIK", 0.54) {
      this.EnableNanoWireIK(scriptInterface, true);
    };
    if this.GetInStateTime() >= this.GetStaticFloatParameterDefault("timeToDisableWireIK", 1.20) {
      this.DisableNanoWireIK(scriptInterface);
    };
  }

  protected final func EnableNanoWireIK(scriptInterface: ref<StateGameScriptInterface>, enable: Bool, opt setPosition: Bool) -> Void {
    let slotPosition: Vector4;
    let targetPosition: Vector4;
    let wireTargetSlot: CName;
    if IsDefined(this.m_targetObject) {
      wireTargetSlot = this.GetStaticCNameParameterDefault("wireTargetSlot", n"wireTargetSlot");
      AIActionHelper.GetTargetSlotPosition(this.m_targetObject, wireTargetSlot, slotPosition);
      targetPosition = slotPosition;
    } else {
      targetPosition = new Vector4(0.00, 0.00, 0.00, 1.00);
    };
    this.UpdateNanoWireEndPositionAnimFeature(scriptInterface, this.GetStaticCNameParameterDefault("ikAnimFeatureName", n"ikLeftNanoWire"), enable, setPosition, targetPosition);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.DisableNanoWireIK(scriptInterface);
    super.OnExit(stateContext, scriptInterface);
    scriptInterface.GetTargetingSystem().SetPlayerAimWeaponPositionProvider(scriptInterface.executionOwner, IPositionProvider.CreateEntityPositionProvider(MeleeTransition.GetWeaponObject(scriptInterface)));
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    scriptInterface.GetTargetingSystem().SetPlayerAimWeaponPositionProvider(scriptInterface.executionOwner, IPositionProvider.CreateEntityPositionProvider(MeleeTransition.GetWeaponObject(scriptInterface)));
    super.OnForcedExit(stateContext, scriptInterface);
  }

  protected final func ApplyThrowableCooldown(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let reloadTime: Float = GameInstance.GetStatsSystem(scriptInterface.GetGame()).GetStatValue(Cast<StatsObjectID>(scriptInterface.owner.GetEntityID()), gamedataStatType.ThrowRecovery);
    this.SetThrowReloadTime(stateContext, reloadTime);
  }
}

public class MeleeThrowReloadDecisions extends MeleeTransition {

  protected final const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let throwReloading: StateResultBool = stateContext.GetPermanentBoolParameter(n"isThrowReloading");
    if MeleeTransition.GetWeaponObject(scriptInterface).WeaponHasTag(n"Throwable") && throwReloading.value {
      return true;
    };
    return false;
  }

  protected final const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if MeleeTransition.IsThrownWeaponReloading(stateContext, scriptInterface) || this.GetInStateTime() <= TweakDBInterface.GetFloat(t"Items.MeleeWeapon.minimumReloadTime", 2.00) {
      return false;
    };
    return true;
  }
}

public class MeleeThrowReloadEvents extends MeleeEventsTransition {

  public let m_isSwitchingWeapon: Bool;

  protected final func OnEnterFromMeleeEquipping(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    MeleeTransition.GetWeaponObject(scriptInterface).SetVisible(false);
    this.OnEnter(stateContext, scriptInterface);
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.SetIsThrowReloading(stateContext, true);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee, 0);
    MeleeTransition.ClearInputBuffer(stateContext);
    super.OnEnter(stateContext, scriptInterface);
    GameInstance.GetAudioSystem(scriptInterface.owner.GetGame()).AddTriggerEffect(n"te_wea_melee_reload", n"PSM_MeleeReloadOnEnter_feedback");
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if MeleeTransition.MeleeAttackPressed(scriptInterface) {
      if !this.m_isSwitchingWeapon && this.GetInStateTime() > TweakDBInterface.GetFloat(t"Items.MeleeWeapon.weaponSwapOnAttackDelay", 0.40) {
        this.m_isSwitchingWeapon = true;
        this.EquipNextWeapon(scriptInterface.executionOwner);
      };
    };
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    MeleeTransition.GetWeaponObject(scriptInterface).SetVisible(true);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let weaponEquipAnimFeature: ref<AnimFeature_EquipType> = new AnimFeature_EquipType();
    this.SetIsThrowReloading(stateContext, false);
    weaponEquipAnimFeature.firstEquip = false;
    scriptInterface.SetAnimationParameterFeature(n"WeaponEquipType", weaponEquipAnimFeature);
    if GameInstance.GetStatsSystem(scriptInterface.owner.GetGame()).GetStatBoolValue(Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID), gamedataStatType.HasKnifeSharpener) {
      StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.KnifeSharpenerBuff");
    };
    super.OnExit(stateContext, scriptInterface);
    GameInstance.GetAudioSystem(scriptInterface.owner.GetGame()).RemoveTriggerEffect(n"PSM_MeleeTargetingOnEnter_trigger");
    GameInstance.GetAudioSystem(scriptInterface.owner.GetGame()).RemoveTriggerEffect(n"PSM_MeleeReloadOnEnter_feedback");
    MeleeTransition.GetWeaponObject(scriptInterface).SetVisible(true);
  }

  private final func EquipNextWeapon(owner: ref<GameObject>) -> Void {
    let equipmentManipulationRequest: ref<EquipmentSystemWeaponManipulationRequest> = new EquipmentSystemWeaponManipulationRequest();
    let eqSystem: wref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(owner.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    equipmentManipulationRequest.requestType = EquipmentManipulationAction.RequestNextThrowableWeapon;
    equipmentManipulationRequest.owner = owner;
    eqSystem.QueueRequest(equipmentManipulationRequest);
  }
}

public class MeleeLeapDecisions extends MeleeTransition {

  protected final const func EnterCondition(stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let checkForLeapableTarget: Bool;
    let performCheck: Bool;
    let target: ref<ScriptedPuppet>;
    let finisherTarget: EntityID = scriptInterface.localBlackboard.GetEntityID(GetAllBlackboardDefs().PlayerStateMachine.FinisherTarget);
    let maxLeapLength: Float = this.GetStaticFloatParameterDefault("maxDistToTarget", 5.00);
    if this.m_driverCombatListener.IsMounted() {
      return false;
    };
    if EntityID.IsDefined(finisherTarget) {
      return true;
    };
    if this.IsInMeleeState(stateContext, n"meleeChargedHold") && MeleeTransition.MeleeAttackReleased(scriptInterface) {
      performCheck = true;
    };
    if stateContext.IsStateActive(n"Locomotion", n"sprint") || scriptInterface.GetOwnerStateVectorParameterFloat(physicsStateValue.LinearSpeed) > 5.00 {
      if scriptInterface.IsOnGround() && this.IsInMeleeState(stateContext, n"meleeChargedHold") && MeleeTransition.MeleeAttackReleased(scriptInterface) {
        performCheck = true;
      };
    };
    if !performCheck {
      return false;
    };
    if !scriptInterface.IsOnGround() && !scriptInterface.HasStatFlag(gamedataStatType.CanMeleeLeapInAir) {
      return false;
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.CanMeleeLeap) {
      return false;
    };
    if this.CanPerformRelicLeap(scriptInterface) {
      target = DefaultTransition.GetTargetObject(scriptInterface) as ScriptedPuppet;
      if IsDefined(target) && !target.IsCivilian() {
        maxLeapLength = this.GetStaticFloatParameterDefault("maxDistToTargetMantisBladesRelic", 40.00);
      };
    };
    checkForLeapableTarget = DefaultTransition.ShouldCheckForLeapableTarget(scriptInterface, MeleeTransition.GetWeaponObject(scriptInterface));
    if !this.GetStaticBoolParameterDefault("canLeapWithoutTarget", false) && !IsDefined(DefaultTransition.GetTargetObject(scriptInterface, maxLeapLength, checkForLeapableTarget)) {
      return false;
    };
    if IsDefined(DefaultTransition.GetTargetObject(scriptInterface, this.GetStaticFloatParameterDefault("minDistToTarget", 2.00), checkForLeapableTarget)) {
      return false;
    };
    stateContext.SetPermanentBoolParameter(n"IsInMeleeLeapState", true, true);
    return true;
  }

  protected final const func ToMeleeStrongAttack(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let leapExitTimeResult: StateResultFloat = stateContext.GetConditionFloatParameter(n"LeapExitTime");
    let enableExitingToStrongAttack: StateResultBool = stateContext.GetConditionBoolParameter(n"enableLeapExitToStrongAttack");
    if leapExitTimeResult.valid && enableExitingToStrongAttack.valid {
      return enableExitingToStrongAttack.value && this.GetInStateTime() >= leapExitTimeResult.value;
    };
    return false;
  }

  protected final const func ToMeleeIdle(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let result: StateResultFloat = stateContext.GetConditionFloatParameter(n"LeapExitTime");
    if result.valid {
      return this.GetInStateTime() >= result.value;
    };
    return false;
  }
}

public class MeleeLeapEvents extends MeleeEventsTransition {

  public let m_enableVaultFromLeapAttack: Bool;

  public let m_exitingToMeleeStrongAttack: Bool;

  public let m_isFinisher: Bool;

  public let m_isTargetKnockedOver: Bool;

  public let m_textLayerId: Uint32;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let leapingWasSuccessful: Bool;
    let finisherTarget: EntityID = scriptInterface.localBlackboard.GetEntityID(GetAllBlackboardDefs().PlayerStateMachine.FinisherTarget);
    this.m_isFinisher = EntityID.IsDefined(finisherTarget);
    this.m_enableVaultFromLeapAttack = false;
    this.m_exitingToMeleeStrongAttack = false;
    this.m_isTargetKnockedOver = false;
    stateContext.SetConditionBoolParameter(n"enableLeapExitToStrongAttack", !this.m_isFinisher, true);
    stateContext.SetPermanentBoolParameter(n"enableVaultFromleapAttack", false, true);
    stateContext.SetPermanentBoolParameter(n"crouchAfterLeaping", false, true);
    scriptInterface.localBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.MeleeLeap, false);
    leapingWasSuccessful = this.LeapToTarget(stateContext, scriptInterface);
    if !leapingWasSuccessful {
      stateContext.SetConditionFloatParameter(n"LeapExitTime", 0.00, true);
      if this.GetStaticBoolParameterDefault("canLeapWithoutTarget", false) {
        this.Leap(stateContext, scriptInterface);
      };
    };
    if this.m_isFinisher {
      stateContext.SetPermanentBoolParameter(n"bossFinisherAttack", true, true);
    };
    if MeleeTransition.HasNewSpyAttackStatFlag(scriptInterface) && MeleeTransition.WeaponIsCharged(scriptInterface) && Equals(MeleeTransition.GetWeaponType(scriptInterface), gamedataItemType.Cyb_MantisBlades) {
      StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.MantisBladesInvulnerableLeapRelicBuff");
    };
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitCommon(stateContext, scriptInterface);
    super.OnExit(stateContext, scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitCommon(stateContext, scriptInterface);
    super.OnForcedExit(stateContext, scriptInterface);
  }

  protected final func OnExitToMeleeStrongAttack(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_exitingToMeleeStrongAttack = true;
    this.OnExit(stateContext, scriptInterface);
  }

  protected final func OnExitCommon(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.ClearDebugText(scriptInterface, this.m_textLayerId);
    StatusEffectHelper.RemoveStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.MantisBladesInvulnerableLeapRelicBuff");
    stateContext.SetConditionBoolParameter(n"enableLeapExitToStrongAttack", true, true);
    if !this.m_exitingToMeleeStrongAttack {
      stateContext.SetPermanentBoolParameter(n"enableVaultFromleapAttack", false, true);
      stateContext.SetPermanentBoolParameter(n"crouchAfterLeaping", false, true);
      scriptInterface.localBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.MeleeLeap, false);
      if this.m_isFinisher {
        scriptInterface.localBlackboard.SetEntityID(GetAllBlackboardDefs().PlayerStateMachine.FinisherTarget, EMPTY_ENTITY_ID());
      };
    };
  }

  private final func LeapToTarget(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let distanceRadiusToTarget: Float;
    let distanceToTarget: Float;
    let exitTime: Float;
    let leapAngle: EulerAngles;
    let leapDuration: Float;
    let leapTargetWithinAngle: Float;
    let playerPerkDataBB: ref<IBlackboard>;
    let playerPos: Vector4;
    let rotationDuration: Float;
    let target: ref<GameObject>;
    let targetAbsPos: Vector4;
    let targetIsOnTheSameLevel: Bool;
    let targetPos: Vector4;
    let targetPuppet: ref<ScriptedPuppet>;
    let vecToTarget: Vector4;
    let shouldCheckForLeapableTarget: Bool = false;
    let maxLeapLength: Float = this.GetStaticFloatParameterDefault("maxDistToTarget", 5.00);
    let finisherTargetId: EntityID = scriptInterface.localBlackboard.GetEntityID(GetAllBlackboardDefs().PlayerStateMachine.FinisherTarget);
    let isWeaponMantisBlades: Bool = Equals(MeleeTransition.GetWeaponType(scriptInterface), gamedataItemType.Cyb_MantisBlades);
    shouldCheckForLeapableTarget = DefaultTransition.ShouldCheckForLeapableTarget(scriptInterface, MeleeTransition.GetWeaponObject(scriptInterface));
    if this.CanPerformRelicLeap(scriptInterface) {
      maxLeapLength = this.GetStaticFloatParameterDefault("maxDistToTargetMantisBladesRelic", 40.00);
    };
    if !isWeaponMantisBlades {
      leapTargetWithinAngle = this.GetStaticFloatParameterDefault("leapTargetWithinAngle", 10.00);
    };
    target = DefaultTransition.GetTargetObject(scriptInterface, maxLeapLength, shouldCheckForLeapableTarget, leapTargetWithinAngle);
    if !IsDefined(target) || IsDefined(target) && EntityID.IsDefined(finisherTargetId) && finisherTargetId != target.GetEntityID() {
      target = GameInstance.FindEntityByID(scriptInterface.executionOwner.GetGame(), finisherTargetId) as ScriptedPuppet;
      if !IsDefined(target) || !ScriptedPuppet.IsActive(target) {
        return false;
      };
    };
    targetPuppet = target as ScriptedPuppet;
    if IsDefined(targetPuppet) {
      targetPuppet.SetFinisherSoundPlayed(false);
    };
    vecToTarget = target.GetWorldPosition() - scriptInterface.executionOwner.GetWorldPosition();
    leapAngle = Vector4.ToRotation(vecToTarget);
    if -leapAngle.Pitch > this.GetStaticFloatParameterDefault("leapMaxPitch", 45.00) {
      return false;
    };
    leapDuration = this.CalculateAdjustmentDuration(Vector4.Length(vecToTarget));
    exitTime = this.GetExitTime(scriptInterface, leapDuration);
    stateContext.SetConditionFloatParameter(n"LeapExitTime", exitTime, true);
    distanceRadiusToTarget = this.GetStaticFloatParameterDefault("distanceRadiusToTarget", 0.90);
    rotationDuration = this.GetStaticFloatParameterDefault("rotationDuration", -1.00);
    targetAbsPos = target.GetWorldPosition();
    playerPos = scriptInterface.executionOwner.GetWorldPosition();
    targetIsOnTheSameLevel = targetAbsPos.Z - playerPos.Z < 0.20;
    if targetIsOnTheSameLevel && StatusEffectSystem.ObjectHasStatusEffectOfType(target, gamedataStatusEffectType.Knockdown) {
      this.m_isTargetKnockedOver = true;
    };
    this.GetSlotTransformToTarget(scriptInterface, target, leapDuration, this.m_isTargetKnockedOver, targetPos, rotationDuration);
    if isWeaponMantisBlades {
      targetPos = this.TargetPrediction(targetPos, target as ScriptedPuppet, leapDuration, 1.00);
    };
    if !this.m_isFinisher {
      rotationDuration = this.GetStaticFloatParameterDefault("rotationDuration", -1.00);
    };
    if SpatialQueriesHelper.IsTargetReachable(scriptInterface.executionOwner, target, targetPos, false, this.m_enableVaultFromLeapAttack) {
      scriptInterface.localBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.MeleeLeap, true);
      if this.m_isFinisher {
        stateContext.SetConditionBoolParameter(n"enableLeapExitToStrongAttack", true, true);
      };
      if this.m_enableVaultFromLeapAttack {
        stateContext.SetPermanentBoolParameter(n"enableVaultFromleapAttack", true, true);
      };
      if this.m_isTargetKnockedOver {
        stateContext.SetPermanentBoolParameter(n"crouchAfterLeaping", true, true);
      };
      if MeleeTransition.HasNewSpyAttackStatFlag(scriptInterface) && isWeaponMantisBlades {
        StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.LeapWithMantisBladesCheck");
        StatusEffectHelper.ApplyStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.LeapCheck");
      };
      if Equals(MeleeTransition.GetWeaponType(scriptInterface), gamedataItemType.Wea_Katana) || Equals(MeleeTransition.GetWeaponType(scriptInterface), gamedataItemType.Wea_Sword) {
        GameObject.PlaySound(scriptInterface.executionOwner, n"lcm_dash_katana");
      };
      playerPerkDataBB = GameInstance.GetBlackboardSystem(scriptInterface.executionOwner.GetGame()).Get(GetAllBlackboardDefs().PlayerPerkData);
      if isWeaponMantisBlades {
        playerPerkDataBB.SetEntityID(GetAllBlackboardDefs().PlayerPerkData.LeapTarget, target.GetEntityID());
        playerPerkDataBB.SetVector4(GetAllBlackboardDefs().PlayerPerkData.LeapPosition, targetPos);
      };
      this.AdjustPlayerPosition(stateContext, scriptInterface, target, leapDuration, distanceRadiusToTarget, rotationDuration, n"None", true, targetPos);
      if shouldCheckForLeapableTarget {
        if IsDefined(playerPerkDataBB) {
          distanceToTarget = Vector4.Distance(target.GetWorldPosition(), scriptInterface.executionOwner.GetWorldPosition());
          playerPerkDataBB.SetFloat(GetAllBlackboardDefs().PlayerPerkData.LeapedDistance, distanceToTarget, true);
        };
      };
      PlayerStaminaHelpers.ModifyStaminaBasedOnLeapAttackDistance(scriptInterface.executionOwner as PlayerPuppet, scriptInterface.IsOnGround(), Vector4.Length(vecToTarget), maxLeapLength);
      return true;
    };
    return false;
  }

  private final func Leap(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let adjustPosition: Vector4;
    let exitTime: Float;
    let slideDuration: Float;
    let vecToTarget: Vector4;
    let cameraWorldTransform: Transform = scriptInterface.GetCameraWorldTransform();
    let leapAngle: EulerAngles = Transform.ToEulerAngles(cameraWorldTransform);
    if leapAngle.Pitch > this.GetStaticFloatParameterDefault("noTargetMaxPitch", 45.00) {
      leapAngle.Pitch = this.GetStaticFloatParameterDefault("noTargetMaxPitch", 45.00);
      Transform.SetOrientationEuler(cameraWorldTransform, leapAngle);
    };
    vecToTarget = Transform.GetForward(cameraWorldTransform) * this.GetStaticFloatParameterDefault("noTargetLeapDistance", 5.00);
    adjustPosition = scriptInterface.executionOwner.GetWorldPosition() + vecToTarget;
    slideDuration = this.CalculateAdjustmentDuration(this.GetStaticFloatParameterDefault("noTargetLeapDistance", 5.00));
    exitTime = slideDuration - this.GetStaticFloatParameterDefault("attackStartupDuration", 0.00);
    stateContext.SetConditionFloatParameter(n"LeapExitTime", exitTime, true);
    this.RequestPlayerPositionAdjustment(stateContext, scriptInterface, null, slideDuration, this.GetStaticFloatParameterDefault("distanceRadius", 0.00), this.GetStaticFloatParameterDefault("rotationDuration", -1.00), adjustPosition, true);
  }

  private final func CalculateAdjustmentDuration(distance: Float) -> Float {
    let duration: Float;
    let minDist: Float = this.GetStaticFloatParameterDefault("minDistToTarget", 1.00);
    let maxDist: Float = this.GetStaticFloatParameterDefault("maxDistToTarget", 1.00);
    let minDur: Float = this.GetStaticFloatParameterDefault("minAdjustmentDuration", 1.00);
    let maxDur: Float = this.GetStaticFloatParameterDefault("maxAdjustmentDuration", 1.00);
    distance -= minDist;
    maxDist -= minDist;
    duration = LerpF(distance / maxDist, minDur, maxDur, true);
    return duration;
  }

  private final func GetExitTime(scriptInterface: ref<StateGameScriptInterface>, leapDuration: Float) -> Float {
    let exitTime: Float;
    if Equals(MeleeTransition.GetWeaponType(scriptInterface), gamedataItemType.Wea_Knife) {
      if this.m_isFinisher {
        exitTime = leapDuration - TDB.GetFloat(t"playerStateMachineFinisher.finisherLeapToTarget.attackStartupDurationKnives");
      } else {
        exitTime = leapDuration - this.GetStaticFloatParameterDefault("attackStartupDurationKnives", 0.00);
      };
    } else {
      if this.m_isFinisher {
        exitTime = leapDuration - TDB.GetFloat(t"playerStateMachineFinisher.finisherLeapToTarget.attackStartupDuration");
      } else {
        exitTime = leapDuration - this.GetStaticFloatParameterDefault("attackStartupDuration", 0.00);
      };
    };
    return exitTime;
  }
}

public class MeleeDashDecisions extends MeleeTransition {

  protected final const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let performCheck: Bool;
    if stateContext.IsStateActive(n"Locomotion", n"sprint") {
      performCheck = true;
    };
    if !scriptInterface.IsOnGround() && DefaultTransition.Get2DLinearSpeed(scriptInterface) > 5.00 {
      performCheck = true;
    };
    if !MeleeTransition.AnyMeleeAttackPressed(scriptInterface) {
      performCheck = false;
    };
    if !performCheck {
      return false;
    };
    if !scriptInterface.IsOnGround() {
      return false;
    };
    if !this.HasWeaponStatFlag(scriptInterface, gamedataStatType.CanWeaponDash) {
      return false;
    };
    if !scriptInterface.HasStatFlag(gamedataStatType.CanMeleeDash) {
      return false;
    };
    if !IsDefined(MeleeTransition.GetWeaponObject(scriptInterface).GetAttack(n"MeleeSprintAttack0")) {
      return false;
    };
    if IsDefined(DefaultTransition.GetTargetObject(scriptInterface, this.GetStaticFloatParameterDefault("minTargetDistanceToDash", 2.00))) {
      return false;
    };
    if !this.CheckDashCollision(stateContext, scriptInterface) {
      return false;
    };
    return true;
  }

  private final static func ConvertArray4ToVector4(const arr: script_ref<[Float]>) -> Vector4 {
    let tempVector4: Vector4 = new Vector4(0.00, 0.00, 0.00, 0.00);
    let i: Int32 = 0;
    while i < ArraySize(Deref(arr)) {
      if i == 0 {
        tempVector4.X = Deref(arr)[i];
      } else {
        if i == 1 {
          tempVector4.Y = Deref(arr)[i];
        } else {
          if i == 2 {
            tempVector4.Z = Deref(arr)[i];
          } else {
            if i == 3 {
              tempVector4.W = Deref(arr)[i];
            };
          };
        };
      };
      i += 1;
    };
    return tempVector4;
  }

  protected final const func CheckDashCollision(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let geometryDescription: ref<GeometryDescriptionQuery>;
    let geometryDescriptionResult: ref<GeometryDescriptionResult>;
    let staticQueryFilter: QueryFilter;
    let cameraWorldTransform: Transform = scriptInterface.GetCameraWorldTransform();
    QueryFilter.AddGroup(staticQueryFilter, n"Static");
    geometryDescription = new GeometryDescriptionQuery();
    geometryDescription.refPosition = Transform.GetPosition(cameraWorldTransform);
    geometryDescription.refDirection = Transform.GetForward(cameraWorldTransform);
    geometryDescription.filter = staticQueryFilter;
    geometryDescription.primitiveDimension = MeleeDashDecisions.ConvertArray4ToVector4(this.GetStaticFloatArrayParameter("primitiveDimensionArr"));
    geometryDescription.maxDistance = this.GetStaticFloatParameterDefault("maxDistance", 5.00);
    geometryDescription.maxExtent = this.GetStaticFloatParameterDefault("maxExtent", 5.00);
    geometryDescription.probingPrecision = this.GetStaticFloatParameterDefault("probingPrecision", 0.05);
    geometryDescription.probingMaxDistanceDiff = this.GetStaticFloatParameterDefault("probingMaxDistanceDiff", 5.00);
    geometryDescription.AddFlag(worldgeometryDescriptionQueryFlags.DistanceVector);
    geometryDescriptionResult = scriptInterface.GetSpatialQueriesSystem().GetGeometryDescriptionSystem().QueryExtents(geometryDescription);
    if Equals(geometryDescriptionResult.queryStatus, worldgeometryDescriptionQueryStatus.NoGeometry) {
      return true;
    };
    return false;
  }

  protected final const func ToMeleeSprintAttack(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let attackData: ref<MeleeAttackData>;
    let duration: Float;
    if IsDefined(MeleeTransition.GetWeaponObject(scriptInterface).GetAttack(n"MeleeSprintAttack0")) {
      duration = this.GetStaticFloatParameterDefault("slideDuration", 1.00) - attackData.attackEffectDelay;
    } else {
      duration = this.GetStaticFloatParameterDefault("timeToStartAttack", 1.00);
    };
    if this.GetInStateTime() >= duration {
      return true;
    };
    return false;
  }

  protected final const func ToMeleeIdle(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    return this.GetInStateTime() >= this.GetStaticFloatParameterDefault("timeout", 1.00);
  }
}

public class MeleeDashEvents extends MeleeEventsTransition {

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if !this.DashToTarget(stateContext, scriptInterface) {
      this.Dash(stateContext, scriptInterface);
    };
    super.OnEnter(stateContext, scriptInterface);
  }

  private final func DashToTarget(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let additionalHorizontalDistance: Float;
    let adjustPosition: Vector4;
    let horizontalDistanceFromTarget: Float;
    let leapAngle: EulerAngles;
    let playerPuppetOrientation: Quaternion;
    let safetyDisplacement: Vector4;
    let scaledSafetyDisplacement: Vector4;
    let slideDuration: Float;
    let vecToTarget: Vector4;
    let target: ref<GameObject> = DefaultTransition.GetTargetObject(scriptInterface, this.GetStaticFloatParameterDefault("maxDistToAquireTarget", 5.00));
    if !IsDefined(target) {
      return false;
    };
    vecToTarget = target.GetWorldPosition() - scriptInterface.executionOwner.GetWorldPosition();
    playerPuppetOrientation = scriptInterface.executionOwner.GetWorldOrientation();
    leapAngle = Vector4.ToRotation(vecToTarget);
    if -leapAngle.Pitch > this.GetStaticFloatParameterDefault("dashMaxPitch", 45.00) {
      return false;
    };
    safetyDisplacement.Y = 2.00;
    if vecToTarget.Z > 0.00 {
      safetyDisplacement.Y = safetyDisplacement.Y * -1.00;
    };
    horizontalDistanceFromTarget = Vector4.Length2D(vecToTarget);
    additionalHorizontalDistance = MaxF(safetyDisplacement.Y - horizontalDistanceFromTarget, 0.00);
    scaledSafetyDisplacement = safetyDisplacement * additionalHorizontalDistance;
    adjustPosition = Quaternion.Transform(playerPuppetOrientation, scaledSafetyDisplacement);
    slideDuration = this.GetStaticFloatParameterDefault("slideDuration", 0.30);
    this.RequestPlayerPositionAdjustment(stateContext, scriptInterface, target, slideDuration, this.GetStaticFloatParameterDefault("distanceRadiusToTarget", 0.90), this.GetStaticFloatParameterDefault("rotationDuration", -1.00), adjustPosition);
    return true;
  }

  private final func Dash(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let adjustPosition: Vector4;
    let slideDuration: Float;
    let vecToTarget: Vector4;
    let cameraWorldTransform: Transform = scriptInterface.GetCameraWorldTransform();
    let leapAngle: EulerAngles = Transform.ToEulerAngles(cameraWorldTransform);
    if leapAngle.Pitch > this.GetStaticFloatParameterDefault("noTargetMaxPitch", 45.00) {
      leapAngle.Pitch = this.GetStaticFloatParameterDefault("noTargetMaxPitch", 45.00);
      Transform.SetOrientationEuler(cameraWorldTransform, leapAngle);
    };
    vecToTarget = Transform.GetForward(cameraWorldTransform) * this.GetStaticFloatParameterDefault("noTargetDashDistance", 5.00);
    adjustPosition = scriptInterface.executionOwner.GetWorldPosition() + vecToTarget;
    slideDuration = this.GetStaticFloatParameterDefault("slideDuration", 0.30);
    this.RequestPlayerPositionAdjustment(stateContext, scriptInterface, null, slideDuration, this.GetStaticFloatParameterDefault("distanceRadius", 0.00), this.GetStaticFloatParameterDefault("rotationDuration", -1.00), adjustPosition);
  }
}

public class MeleeBlockAttackDecisions extends MeleeAttackGenericDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !this.IsBlockHeld(stateContext, scriptInterface) {
      return false;
    };
    if !(MeleeTransition.AnyMeleeAttackPressed(scriptInterface) || stateContext.GetConditionBool(n"LightMeleeAttackPressed")) {
      return false;
    };
    if !this.HasAttackRecord(scriptInterface) {
      return false;
    };
    return true;
  }

  protected const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let attackData: ref<MeleeAttackData> = this.GetAttackData(stateContext);
    let inStateTime: Float = this.GetInStateTime();
    if this.IsBlockHeld(stateContext, scriptInterface) {
      if attackData.blockTransitionTime > 0.00 && inStateTime >= attackData.blockTransitionTime {
        return true;
      };
    };
    if stateContext.GetConditionBool(n"LightMeleeAttackPressed") && inStateTime >= attackData.attackWindowClosed {
      return true;
    };
    if inStateTime >= attackData.idleTransitionTime {
      return true;
    };
    return false;
  }
}

public class MeleeBlockAttackEvents extends MeleeAttackGenericEvents {

  protected final func GetAttackType() -> EMeleeAttackType {
    return EMeleeAttackType.Block;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.ResetAttackNumber(stateContext);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 15);
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
  }
}

public class MeleeBodySlamAttackDecisions extends MeleeAttackGenericDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if PlayerDevelopmentSystem.GetInstance(scriptInterface.executionOwner).IsNewPerkBought(scriptInterface.executionOwner, gamedataNewPerkType.Body_Right_Milestone_2) < 2 {
      return false;
    };
    if !WeaponObject.IsBlunt(GameObject.GetActiveWeapon(scriptInterface.executionOwner).GetItemID()) {
      return false;
    };
    if !this.IsBlockHeld(stateContext, scriptInterface) {
      return false;
    };
    if !stateContext.IsStateActive(n"Locomotion", n"sprint") {
      return false;
    };
    if scriptInterface.GetStatPoolsSystem().GetStatPoolValue(Cast<StatsObjectID>(scriptInterface.executionOwner.GetEntityID()), gamedataStatPoolType.Stamina, true) <= 0.00 {
      return false;
    };
    return true;
  }

  protected const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let exitBodySlam: StateResultBool;
    let inAirDuration: StateResultFloat;
    let locomotionState: CName;
    if !this.IsBlockHeld(stateContext, scriptInterface) {
      return true;
    };
    if MeleeTransition.QuickMeleeTapped(scriptInterface) {
      return true;
    };
    locomotionState = stateContext.GetStateMachineCurrentState(n"Locomotion");
    if Equals(locomotionState, n"fall") || Equals(locomotionState, n"bodySlamJump") {
      inAirDuration = stateContext.GetPermanentFloatParameter(n"InAirDuration");
      if inAirDuration.valid && inAirDuration.value > this.GetStaticFloatParameterDefault("maxAirTime", 0.50) {
        return true;
      };
    } else {
      if NotEquals(locomotionState, n"sprint") && NotEquals(locomotionState, n"regularLand") {
        return true;
      };
    };
    exitBodySlam = stateContext.GetTemporaryBoolParameter(n"exitBodySlam");
    if exitBodySlam.value {
      return true;
    };
    return false;
  }
}

public class MeleeBodySlamAttackEvents extends MeleeEventsTransition {

  public let m_effect: ref<EffectInstance>;

  public let m_speedModifier: ref<gameStatModifierData>;

  public let m_stunModifier: ref<gameStatModifierData>;

  public let m_chargeStage: Int32;

  public let m_attackSpawnDelay: Float;

  public let m_timeToFullAttack: Float;

  public let m_nextAttackRefresh: Float;

  public let m_playBumpSFX: Bool;

  public let m_bumpCallback: ref<CallbackHandle>;

  public let m_delayBetweenBumpSFX: Float;

  public let m_bumpSFXCooldown: Float;

  public let m_staminaCost: Float;

  @default(MeleeBodySlamAttackEvents, 2)
  public const let m_fullAttackIndex: Int32;

  @default(MeleeBodySlamAttackEvents, 1)
  public const let m_weakAttackIndex: Int32;

  protected func OnDetach(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.RemoveStatModifiers(scriptInterface);
  }

  protected final func AddSpeedModifier(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let modifierVal: Float = 0.00;
    let speedParam: String = "initialSpeed";
    if this.m_chargeStage > 1 {
      speedParam = "finalSpeed";
    };
    modifierVal = this.GetStaticFloatParameterDefault(speedParam, 0.00);
    this.m_speedModifier = RPGManager.CreateStatModifier(gamedataStatType.MaxSpeed, gameStatModifierType.Additive, modifierVal);
    scriptInterface.GetStatsSystem().AddModifier(Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID), this.m_speedModifier);
  }

  protected final func RemoveSpeedModifier(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if IsDefined(this.m_speedModifier) {
      scriptInterface.GetStatsSystem().RemoveModifier(Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID), this.m_speedModifier);
      this.m_speedModifier = null;
    };
  }

  protected final func AddStunModifier(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_stunModifier = RPGManager.CreateStatModifier(gamedataStatType.StunApplicationRate, gameStatModifierType.Multiplier, 0.00);
    scriptInterface.GetStatsSystem().AddModifier(Cast<StatsObjectID>(scriptInterface.ownerEntityID), this.m_stunModifier);
  }

  protected final func RemoveStunModifier(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if IsDefined(this.m_stunModifier) {
      scriptInterface.GetStatsSystem().RemoveModifier(Cast<StatsObjectID>(scriptInterface.ownerEntityID), this.m_stunModifier);
      this.m_stunModifier = null;
    };
  }

  protected final func RemoveStatModifiers(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.RemoveSpeedModifier(scriptInterface);
    this.RemoveStunModifier(scriptInterface);
  }

  protected final func SpawnBodySlamAttack(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>, attackStage: Int32) -> Void {
    let attack: ref<Attack_GameEffect>;
    let attackData: ref<MeleeAttackData>;
    let weapon: ref<WeaponObject>;
    let colliderBox: Vector4 = new Vector4(0.25, 0.25, 0.25, 0.00);
    this.GetAttackDataFromCurrentState(stateContext, scriptInterface, attackStage, attackData);
    weapon = scriptInterface.owner as WeaponObject;
    attack = weapon.GetCurrentAttack() as Attack_GameEffect;
    this.m_staminaCost = attackData.staminaCost;
    if IsDefined(attack) {
      this.m_effect = attack.PrepareAttack(scriptInterface.executionOwner);
      colliderBox.X = this.GetStaticFloatParameterDefault("hitboxWidth", 0.25);
      colliderBox.Z = this.GetStaticFloatParameterDefault("hitboxHeight", 1.00);
      EffectData.SetVector(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.box, colliderBox);
      EffectData.SetFloat(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.duration, attackData.attackEffectDuration);
      EffectData.SetVector(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, Transform.TransformPoint(scriptInterface.GetCameraWorldTransform(), new Vector4(0.00, this.GetStaticFloatParameterDefault("hitboxForwardOffset", 1.00), 0.00, 0.00)));
      EffectData.SetQuat(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.rotation, Transform.GetOrientation(scriptInterface.GetCameraWorldTransform()));
      EffectData.SetVector(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.forward, Transform.GetForward(scriptInterface.GetCameraWorldTransform()));
      EffectData.SetFloat(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.range, this.GetStaticFloatParameterDefault("range", 0.25));
      EffectData.SetBool(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.meleeCleave, true);
      EffectData.SetInt(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.attackNumber, attackStage);
      EffectData.SetName(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.impactOrientationSlot, attackData.impactFxSlot);
      this.m_nextAttackRefresh = attackData.attackEffectDuration;
      EffectData.SetVariant(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.fxPackage, ToVariant(weapon.GetFxPackage()));
      EffectData.SetBool(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.playerOwnedWeapon, true);
      this.m_attackSpawnDelay = attackData.attackEffectDuration;
      attack.StartAttack();
    };
  }

  protected final func UpdateEffectPosition(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let position: Vector4 = new Vector4(0.00, this.GetStaticFloatParameterDefault("hitboxForwardOffset", 1.00), 0.00, 0.00);
    EffectData.SetVector(this.m_effect.GetSharedData(), GetAllBlackboardDefs().EffectSharedData.position, Transform.TransformPoint(scriptInterface.GetCameraWorldTransform(), position));
  }

  protected final func SetBodySlamAnimFeature(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let animFeature: ref<AnimFeature_BodySlam> = new AnimFeature_BodySlam();
    if this.m_chargeStage == 1 {
      animFeature.chargeLevel = 3;
    } else {
      if this.m_chargeStage == 0 {
        animFeature.chargeLevel = 0;
      } else {
        animFeature.chargeLevel = 2;
      };
    };
    scriptInterface.SetAnimationParameterFeature(n"BodySlam", animFeature);
  }

  protected final func SetChargeStage(scriptInterface: ref<StateGameScriptInterface>, chargeStage: Int32) -> Void {
    this.RemoveSpeedModifier(scriptInterface);
    this.m_chargeStage = chargeStage;
    this.AddSpeedModifier(scriptInterface);
    this.SetBodySlamAnimFeature(scriptInterface);
  }

  protected final func UpdateChargeStage(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    if this.m_chargeStage == 1 {
      if this.GetInStateTime() > this.GetStaticFloatParameterDefault("timeToFinalSpeed", 1.00) {
        this.SetChargeStage(scriptInterface, 2);
      };
    };
  }

  protected cb func OnBodySlamBump(value: Int32) -> Bool {
    if this.m_bumpSFXCooldown <= 0.00 {
      this.m_playBumpSFX = true;
      this.m_bumpSFXCooldown = this.m_delayBetweenBumpSFX;
    };
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let isExhausted: Bool;
    this.m_chargeStage = 1;
    this.SetBodySlamAnimFeature(scriptInterface);
    this.ResetAttackNumber(stateContext);
    this.SetIsAttacking(stateContext, true);
    this.SetIsBlocking(stateContext, true);
    stateContext.SetConditionBoolParameter(n"SprintToggled", true, true);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee, 2);
    this.SetMeleeAttackPressCount(stateContext, scriptInterface);
    MeleeTransition.ClearInputBuffer(stateContext);
    this.SpawnBodySlamAttack(stateContext, scriptInterface, this.m_weakAttackIndex);
    isExhausted = StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.PlayerExhausted");
    if !isExhausted {
      this.AddSpeedModifier(scriptInterface);
    };
    this.m_timeToFullAttack = this.GetStaticFloatParameterDefault("timeToFullAttack", 1.00);
    this.AddStunModifier(scriptInterface);
    this.m_bumpCallback = scriptInterface.localBlackboard.RegisterListenerInt(GetAllBlackboardDefs().PlayerStateMachine.BodySlamBump, this, n"OnBodySlamBump");
    scriptInterface.localBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.IsInBodySlamState, true, true);
    this.m_playBumpSFX = false;
    this.m_delayBetweenBumpSFX = this.GetStaticFloatParameterDefault("delayBetweenBumpSFX", 1.00);
    this.m_bumpSFXCooldown = 0.00;
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let nextAttackType: Int32 = this.m_weakAttackIndex;
    let fullAttackStartFrame: Bool = false;
    this.UpdateChargeStage(stateContext, scriptInterface);
    MeleeTransition.UpdateMeleeInputBuffer(stateContext, scriptInterface);
    if this.m_timeToFullAttack > 0.00 {
      this.m_timeToFullAttack = this.m_timeToFullAttack - timeDelta;
      if this.m_timeToFullAttack <= 0.00 {
        this.m_effect.Terminate();
        this.SpawnBodySlamAttack(stateContext, scriptInterface, this.m_fullAttackIndex);
        fullAttackStartFrame = true;
      };
    };
    if !fullAttackStartFrame {
      this.m_nextAttackRefresh = this.m_nextAttackRefresh - timeDelta;
      if this.m_nextAttackRefresh <= 0.00 {
        if this.m_timeToFullAttack <= 0.00 {
          nextAttackType = this.m_fullAttackIndex;
        };
        this.SpawnBodySlamAttack(stateContext, scriptInterface, nextAttackType);
      };
    };
    this.UpdateEffectPosition(stateContext, scriptInterface);
    if this.m_playBumpSFX {
      this.m_playBumpSFX = false;
      if this.m_timeToFullAttack <= 0.00 {
        GameObject.PlaySoundEvent(scriptInterface.executionOwner, n"lcm_additional_bump_perk_body_slam_strong");
      } else {
        GameObject.PlaySoundEvent(scriptInterface.executionOwner, n"lcm_additional_bump_perk_body_slam_mid");
      };
      if scriptInterface.GetStatPoolsSystem().GetStatPoolValue(Cast<StatsObjectID>(scriptInterface.executionOwner.GetEntityID()), gamedataStatPoolType.Stamina, true) <= this.GetStaticFloatParameterDefault("staminaCostPerHit", 20.00) {
        stateContext.SetTemporaryBoolParameter(n"exitBodySlam", true, true);
      };
      PlayerStaminaHelpers.ModifyStamina(scriptInterface.executionOwner as PlayerPuppet, -this.m_staminaCost, true);
    };
    this.m_bumpSFXCooldown -= timeDelta;
  }

  protected final func OnExitCommon(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.m_effect.Terminate();
    this.RemoveStatModifiers(scriptInterface);
    this.m_chargeStage = 0;
    this.SetBodySlamAnimFeature(scriptInterface);
    this.SetIsAttacking(stateContext, false);
    this.SetIsBlocking(stateContext, false);
    scriptInterface.localBlackboard.UnregisterListenerInt(GetAllBlackboardDefs().PlayerStateMachine.BodySlamBump, this.m_bumpCallback);
    scriptInterface.localBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.IsInBodySlamState, false, true);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitCommon(stateContext, scriptInterface);
    super.OnExit(stateContext, scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnExitCommon(stateContext, scriptInterface);
    super.OnForcedExit(stateContext, scriptInterface);
  }
}

public class MeleeCrouchAttackDecisions extends MeleeAttackGenericDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !(stateContext.IsStateActive(n"Locomotion", n"crouch") || stateContext.IsStateActive(n"Locomotion", n"slide")) {
      return false;
    };
    if !MeleeTransition.WantsToLightAttack(stateContext, scriptInterface) {
      return false;
    };
    if !this.HasAttackRecord(scriptInterface) {
      return false;
    };
    return true;
  }
}

public class MeleeCrouchAttackEvents extends MeleeAttackGenericEvents {

  protected final func GetAttackType() -> EMeleeAttackType {
    return EMeleeAttackType.Crouch;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.ResetAttackNumber(stateContext);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 17);
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
  }
}

public class MeleeJumpAttackDecisions extends MeleeAttackGenericDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if !(scriptInterface.localBlackboard.GetInt(GetAllBlackboardDefs().PlayerStateMachine.Locomotion) == 5 || stateContext.IsStateActive(n"Locomotion", n"fall")) {
      return false;
    };
    if !MeleeTransition.WantsToLightAttack(stateContext, scriptInterface) {
      return false;
    };
    if !super.EnterCondition(stateContext, scriptInterface) {
      return false;
    };
    return true;
  }
}

public class MeleeJumpAttackEvents extends MeleeAttackGenericEvents {

  protected final func GetAttackType() -> EMeleeAttackType {
    return EMeleeAttackType.Jump;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.ResetAttackNumber(stateContext);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 18);
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
  }
}

public class MeleeSprintAttackDecisions extends MeleeAttackGenericDecisions {

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let performCheck: Bool;
    if stateContext.IsStateActive(n"Locomotion", n"sprint") {
      performCheck = true;
    };
    if !scriptInterface.IsOnGround() && DefaultTransition.Get2DLinearSpeed(scriptInterface) > 5.00 {
      performCheck = true;
    };
    if !performCheck {
      return false;
    };
    if !MeleeTransition.WantsToLightAttack(stateContext, scriptInterface) {
      return false;
    };
    if !this.HasAttackRecord(scriptInterface) {
      return false;
    };
    return true;
  }
}

public class MeleeSprintAttackEvents extends MeleeAttackGenericEvents {

  protected final func GetAttackType() -> EMeleeAttackType {
    return EMeleeAttackType.Sprint;
  }

  public final func OnEnterFromMeleeDash(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.OnEnter(stateContext, scriptInterface);
    this.m_blockImpulseCreation = true;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.ResetAttackNumber(stateContext);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 16);
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
  }
}

public class MeleeEquipAttackEvents extends MeleeAttackGenericEvents {

  protected final func GetAttackType() -> EMeleeAttackType {
    return EMeleeAttackType.Equip;
  }

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    this.ResetAttackNumber(stateContext);
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.MeleeWeapon, 21);
    super.OnEnter(stateContext, scriptInterface);
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
  }
}

public class MeleeGroundSlamAttackDecisions extends MeleeAttackGenericDecisions {

  protected final const func IsGroundSlamming(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let curState: CName = stateContext.GetStateMachineCurrentState(n"Locomotion");
    if Equals(curState, n"airHover") {
      return true;
    };
    if Equals(curState, n"superheroFall") {
      return true;
    };
    if Equals(curState, n"superheroLand") {
      return true;
    };
    if Equals(curState, n"superheroLandRecovery") {
      return true;
    };
    return false;
  }

  protected final const func IsValidLocomotionState(const state: CName) -> Bool {
    if Equals(state, n"stand") || Equals(state, n"sprint") || Equals(state, n"crouch") || Equals(state, n"crouchSprint") || Equals(state, n"aimWalk") || Equals(state, n"jump") || Equals(state, n"doubleJump") || Equals(state, n"chargeJump") || Equals(state, n"hoverJump") || Equals(state, n"coolExitJump") || Equals(state, n"bodySlamJump") || Equals(state, n"ladderJump") || Equals(state, n"fall") || Equals(state, n"slideFall") || Equals(state, n"dodgeAir") || Equals(state, n"dodge") {
      return true;
    };
    return false;
  }

  protected final const func CanFit(const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let capsuleHeight: Float = this.GetStaticFloatParameterDefault("capsuleHeight", 1.00);
    let capsuleRadius: Float = this.GetStaticFloatParameterDefault("capsuleRadius", 1.00);
    return scriptInterface.CanCapsuleFit(capsuleHeight, capsuleRadius);
  }

  protected const func EnterCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    if scriptInterface.IsOnGround() {
      if !scriptInterface.GetStatsSystem().GetStatBoolValue(Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID), gamedataStatType.CanGroundSlamOnGround) {
        return false;
      };
    } else {
      if !scriptInterface.GetStatsSystem().GetStatBoolValue(Cast<StatsObjectID>(scriptInterface.executionOwnerEntityID), gamedataStatType.CanGroundSlamInAir) {
        return false;
      };
    };
    if !MeleeTransition.WantsToQuickMelee(stateContext, scriptInterface) {
      return false;
    };
    if !WeaponObject.IsBlunt(GameObject.GetActiveWeapon(scriptInterface.executionOwner).GetItemID()) {
      return false;
    };
    if StatusEffectSystem.ObjectHasStatusEffect(scriptInterface.executionOwner, t"BaseStatusEffect.GroundSlamCooldown") {
      stateContext.SetConditionBoolParameter(n"QuickMeleeAttackTapped", false, true);
      return false;
    };
    if !this.IsValidLocomotionState(stateContext.GetStateMachineCurrentState(n"Locomotion")) {
      return false;
    };
    if !this.CanFit(scriptInterface) {
      return false;
    };
    if scriptInterface.localBlackboard.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsPlayerInsideMovingElevator) {
      return false;
    };
    return true;
  }

  protected const func ExitCondition(const stateContext: ref<StateContext>, const scriptInterface: ref<StateGameScriptInterface>) -> Bool {
    let justEnteredGroundSlam: StateResultBool = stateContext.GetTemporaryBoolParameter(n"groundSlam");
    if justEnteredGroundSlam.value {
      return false;
    };
    if this.IsGroundSlamming(stateContext, scriptInterface) {
      return false;
    };
    return true;
  }
}

public class MeleeGroundSlamAttackEvents extends MeleeAttackGenericEvents {

  public let m_knockdownImmunityModifier: ref<gameStatModifierData>;

  public let m_stunImmunityModifier: ref<gameStatModifierData>;

  public func OnEnter(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let attackData: ref<MeleeAttackData>;
    this.m_attackCreated = false;
    this.m_effect = null;
    this.m_trailCreated = false;
    this.ResetAttackNumber(stateContext);
    stateContext.SetTemporaryBoolParameter(n"groundSlam", true, true);
    MeleeTransition.ClearInputBuffer(stateContext);
    stateContext.SetPermanentBoolParameter(n"InterruptMelee", false, true);
    this.SetIsAttacking(stateContext, true);
    this.SetIsBlocking(stateContext, false);
    GameObject.PlayVoiceOver(scriptInterface.executionOwner, n"meleeAttack", n"Scripts:MeleeAttackGenericEvents");
    this.SetBlackboardIntVariable(scriptInterface, GetAllBlackboardDefs().PlayerStateMachine.Melee, 1);
    this.GetAttackDataFromCurrentState(stateContext, scriptInterface, stateContext.GetIntParameter(n"meleeAttackNumber", true), attackData);
    this.ConsumeStamina(scriptInterface, attackData);
    stateContext.SetPermanentBoolParameter(n"hasDeflectAnim", attackData.hasDeflectAnim, true);
    stateContext.SetPermanentBoolParameter(n"hasHitAnim", attackData.hasHitAnim, true);
    stateContext.SetConditionScriptableParameter(n"MeleeAttackData", attackData, true);
    stateContext.SetPermanentBoolParameter(n"VisionToggled", false, true);
    this.ForceDisableVisionMode(stateContext);
    stateContext.SetTemporaryBoolParameter(n"InterruptSprint", true, true);
    stateContext.SetConditionBoolParameter(n"SprintToggled", false, true);
    stateContext.SetConditionBoolParameter(n"SprintHoldCanStartWithoutNewInput", false, true);
    this.ClearDebugText(scriptInterface, this.m_textLayer);
    stateContext.SetPermanentIntParameter(n"attackType", EnumInt(this.GetAttackType()), true);
    stateContext.SetConditionBoolParameter(n"CrouchToggled", false, true);
    this.SetIsSafe(stateContext, false);
    this.SendDataTrackingRequest(scriptInterface, ETelemetryData.MeleeAttacksMade, 1);
    GameInstance.GetTelemetrySystem(scriptInterface.owner.GetGame()).LogWeaponAttackPerformed(MeleeTransition.GetWeaponObject(scriptInterface));
    this.AddStatModifiers(scriptInterface);
  }

  public func OnUpdate(timeDelta: Float, stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let broadcaster: ref<StimBroadcasterComponent>;
    let spawnAttack: StateResultBool;
    let attackData: ref<MeleeAttackData> = this.GetAttackData(stateContext);
    if !this.m_attackCreated {
      spawnAttack = stateContext.GetTemporaryBoolParameter(n"spawnGroundSlamSwing");
      if spawnAttack.value {
        this.CreateMeleeAttack(stateContext, scriptInterface, attackData);
        this.m_attackCreated = true;
        if IsDefined(broadcaster = scriptInterface.executionOwner.GetStimBroadcasterComponent()) {
          broadcaster.TriggerSingleBroadcast(scriptInterface.executionOwner, gamedataStimType.MeleeAttack);
        };
      };
    };
    if IsDefined(this.m_effect) {
      this.UpdateEffectPosition(stateContext, scriptInterface, attackData, 0.00);
    };
  }

  public func OnExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnExit(stateContext, scriptInterface);
    this.RemoveStatModifiers(scriptInterface);
  }

  public func OnForcedExit(stateContext: ref<StateContext>, scriptInterface: ref<StateGameScriptInterface>) -> Void {
    super.OnForcedExit(stateContext, scriptInterface);
    this.RemoveStatModifiers(scriptInterface);
  }

  private final func AddStatModifiers(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let statSys: ref<StatsSystem> = scriptInterface.GetStatsSystem();
    this.m_knockdownImmunityModifier = RPGManager.CreateStatModifier(gamedataStatType.KnockdownImmunity, gameStatModifierType.Additive, 1.00);
    this.m_stunImmunityModifier = RPGManager.CreateStatModifier(gamedataStatType.StunImmunity, gameStatModifierType.Additive, 1.00);
    statSys.AddModifier(Cast<StatsObjectID>(scriptInterface.ownerEntityID), this.m_knockdownImmunityModifier);
    statSys.AddModifier(Cast<StatsObjectID>(scriptInterface.ownerEntityID), this.m_stunImmunityModifier);
  }

  private final func RemoveStatModifiers(scriptInterface: ref<StateGameScriptInterface>) -> Void {
    let statSys: ref<StatsSystem> = scriptInterface.GetStatsSystem();
    if IsDefined(this.m_knockdownImmunityModifier) {
      statSys.RemoveModifier(Cast<StatsObjectID>(scriptInterface.ownerEntityID), this.m_knockdownImmunityModifier);
      this.m_knockdownImmunityModifier = null;
    };
    if IsDefined(this.m_stunImmunityModifier) {
      statSys.RemoveModifier(Cast<StatsObjectID>(scriptInterface.ownerEntityID), this.m_stunImmunityModifier);
      this.m_stunImmunityModifier = null;
    };
  }
}
